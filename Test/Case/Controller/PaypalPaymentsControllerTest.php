<?php
App::uses('PaypalPaymentsController', 'Controller');

/**
 * PaypalPaymentsController Test Case
 *
 */
class PaypalPaymentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.drilling_type',
		'app.order',
		'app.user',
		'app.promo_code',
		'app.order_status',
		'app.order_date',
		'app.delivery_option',
		'app.order_item',
		'app.product',
		'app.paper_size',
		'app.paypal_payment'
	);

	public function testPaypalPost() {

		$post = array(
			'payment_type' => 'instant',
		    'payment_date' => '2015-04-24 13:21:49',
		    'payment_status' => 'Completed',
		    'payer_status' => 'verified',
		    'first_name' => 'John',
		    'last_name' => 'Smith',
		    'payer_email' => 'buyer@paypalsandbox.com',
		    'payer_id' => 'TESTBUYERID01',
		    'address_name' => 'John Smith',
		    'address_country' => 'United States',
		    'address_country_code' => 'US',
		    'address_zip' => '95131',
		    'address_state' => 'CA',
		    'address_city' => 'San Jose',
		    'address_street' => '123 any street',
		    'business' => 'seller@paypalsandbox.com',
		    'receiver_email' => 'seller@paypalsandbox.com',
		    'receiver_id' => 'seller@paypalsandbox.com',
		    'residence_country' => 'US',
		    'item_name1' => 'something',
		    'item_number1' => 'order-1',
		    'quantity' => 1,
		    'shipping' => 2.50,
		    'tax' => 0.00,
		    'mc_currency' => 'USD',
		    'mc_fee' => 0.44,
		    'mc_gross' => 3.51,
		    'mc_gross1' => 3.51,
		    'mc_handling' => 2.06,
		    'mc_handling1' => 1.67,
		    'mc_shipping' => 3.02,
		    'mc_shipping1' => 1.02,
		    'txn_type' => 'cart',
		    'txn_id' => 899327589,
		    'notify_version' => 2.4,
		    'custom' => 'order-1',
		    'invoice' => 'abc1234',
		    'test_ipn' => 1,
		    'verify_sign' => 'AoJkn68KR6tdjo1Retf2nk0bXTaCA9onJcCRzBSQbwDVSZjbPDPU5PL4'
	    );

	    $result = $this->testAction(
			'/paypal_payments/process',
			array(
				'data' => $post,
				'method' => 'post',
				'return' => 'vars'
			)
		);

		die(debug($result));


	}

}
