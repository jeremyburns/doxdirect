<?php
App::uses('OrdersController', 'Controller');

/**
 * OrdersController Test Case
 *
 */
class OrdersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.drilling_type',
		'app.order',
		'app.user',
		'app.promo_code',
		'app.order_status',
		'app.order_date',
		'app.delivery_option',
		'app.order_item',
		'app.product',
		'app.paper_size'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testQuote method
 *
 * @return void
 */
	public function testQuote() {

		$data = array(
			'Order' => array(
				'product_id' => '',
				'pages' => '1',
				'paper_size_id' => '',
				'copies' => '1',
				'sides' => '1',
				'colour' => '1',
				'tabs' => '0'
			)
		);

		$result = $this->testAction(
			'/orders/quote',
			array(
				'data' => $data,
				'method' => 'post',
				'return' => 'vars'
			)
		);

		die(debug($result));
		$this->markTestIncomplete('testDelete not implemented.');
	}

	public function testCheckout() {

		$data = array(
			'Order' => array(
				'id' => 'order-1',
				'billing_same_as_shipping' => '1',
				'accept_terms' => '1',
				'keep_me_up_to_date' => '0',
				'source_id' => ''
			),
			'User' => array(
				'password' => '',
				'id' => '',
				'first_name' => 'First',
				'last_name' => 'Name',
				'username' => 'newuser@doxdirect.com',
				'phone' => '1234567890'
			),
			'ShippingAddress' => array(
				'id' => '',
				'house_number' => '1',
				'address_1' => 'The Road',
				'address_2' => '',
				'city' => 'Slough',
				'county' => 'Berkshire',
				'post_code' => 'A12 3BC',
				'country_id' => '226'
			),
			'BillingAddress' => array(
				'id' => '',
				'first_name' => '',
				'last_name' => '',
				'email' => '',
				'house_number' => '',
				'address_1' => '',
				'address_2' => '',
				'city' => '',
				'county' => '',
				'post_code' => '',
				'country_id' => '226'
			)
		);

		$result = $this->testAction(
			'/orders/checkout',
			array(
				'data' => $data,
				'method' => 'post',
				'return' => 'vars'
			)
		);

		die(debug($result));

	}

}
