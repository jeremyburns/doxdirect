<?php
App::uses('OrderItem', 'Model');

/**
 * OrderItem Test Case
 *
 */
class OrderItemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.binding',
		'app.binding_edge_type',
		'app.binding_side_type',
		'app.binding_type',
		'app.click',
		'app.cover_type',
		'app.delivery_option',
		'app.drilling_type',
		'app.job_status',
		'app.media',
		'app.side',
		'app.order',
		'app.order_item',
		'app.order_item_file',
		'app.order_item_file_page',
		'app.order_item_file_page_process',
		'app.order_item_tab',
		'app.order_status',
		'app.paper_size',
		'app.payment_method',
		'app.paypal_payment',
		'app.product',
		'app.promo_code',
		'app.source',
		'app.turnaround_option',
		'app.user',
		'app.user_address'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OrderItem = ClassRegistry::init('OrderItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OrderItem);

		parent::tearDown();
	}

	public function testCollatePages() {

		$orderItemId = 'order-item-2';

		$result = $this->OrderItem->collatePages($orderItemId);

// 		die(debug($result));

	}

}
