<?php
App::uses('DeliveryOption', 'Model');

/**
 * DeliveryOption Test Case
 *
 */
class DeliveryOptionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.delivery_option',
		'app.order',
		'app.user',
		'app.promo_code',
		'app.order_status',
		'app.order_date',
		'app.order_item',
		'app.product',
		'app.paper_size',
		'app.drilling_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DeliveryOption = ClassRegistry::init('DeliveryOption');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DeliveryOption);

		parent::tearDown();
	}

	public function testRefreshFromPace() {

		$result = $this->DeliveryOption->refresh(true);

		$this->assertTrue($result['success']);

	}

	public function testDeliveryDate() {

		$royalMail = '00';
		$upsExpress = '07';
		$upsStandard = '11';
		$upsWorldWideExtraPlus = '54';
		$upsSaver = '65';

		$testCases = array(
			array(
				'description' => 'Royal Mail - despatch on a Thursday',
				'despatch_date' => '2015-04-23',
				'delivery_option_id' => $royalMail,
				'expected' => array(
					'delivery_option_name' => 'Royal Mail 1st Class',
					'is_guaranteed' => false,
					'latest_delivery_time' => null,
					'delivery_date' => '2015-04-25',
					'delivery_date_formatted' => 'Saturday 25 April'
				)
			),
			array(
				'description' => 'Royal Mail - despatch on a Friday',
				'despatch_date' => '2015-04-24',
				'delivery_option_id' => $royalMail,
				'expected' => array(
					'delivery_option_name' => 'Royal Mail 1st Class',
					'is_guaranteed' => false,
					'latest_delivery_time' => null,
					'delivery_date' => '2015-04-27',
					'delivery_date_formatted' => 'Monday 27 April'
				)
			)
		);

		foreach ($testCases as $testCase) {

			$despatchDate = new DateTime($testCase['despatch_date']);

			$result = $this->DeliveryOption->deliveryDetails(
				$testCase['delivery_option_id'],
				$despatchDate
			);

			unset ($result['delivery_date_obj']);

			$this->assertEqual(
				$result,
				$testCase['expected'],
				$testCase['description']
			);

		}

	}

}
