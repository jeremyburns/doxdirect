<?php
App::uses('Process', 'Model');

/**
 * Process Test Case
 *
 */
class ProcessTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.process'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Process = ClassRegistry::init('Process');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Process);

		parent::tearDown();
	}

/*
	public function testGetIdByCode() {

		// Get the click for A5 unbound
		$result = $this->Process->getIdByCode('cut-sheet-A5');

		$expected = 10;

		$this->assertEqual($result, $expected);

	}
*/

	public function testGetIdByInvalidCode() {

		// Get the click for A5 unbound
		$result = $this->Process->getIdByCode('invalid');

		$expected = false;

		$this->assertEqual($result, $expected);

	}

}