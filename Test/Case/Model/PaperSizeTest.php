<?php
App::uses('PaperSize', 'Model');

/**
 * PaperSize Test Case
 *
 */
class PaperSizeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.delivery_option',
		'app.drilling_type',
		'app.paper_size',
		'app.promo_code',
		'app.order',
		'app.order_item',
		'app.order_status',
		'app.order_date',
		'app.product',
		'app.user'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PaperSize = ClassRegistry::init('PaperSize');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PaperSize);

		parent::tearDown();
	}

	public function testMatch() {

		$testCases = [
			'a4_p_mm' => [
				'width' => 210,
				'height' => 297,
				'measurement' => 'mm',
				'expected' => [
					'success' => true,
					'paper_size_match' => 'exact',
					'paper_size_id' => 'A4',
					'orientation' => 'p',
					'width' => 210.0,
					'height' => 297.0
				]
			],
			'a4_p_pts' => [
				'width' => 595,
				'height' => 842,
				'measurement' => 'points',
				'expected' => [
					'success' => true,
					'paper_size_match' => 'exact',
					'paper_size_id' => 'A4',
					'orientation' => 'p',
					'width' => 210,
					'height' => 297
				]
			],
			'a4_l_mm' => [  // provide A4 landscape
				'width' => 297,
				'height' => 210,
				'measurement' => 'mm',
				'expected' => [
					'success' => true,
					'paper_size_match' => 'exact',
					'paper_size_id' => 'A4',
					'orientation' => 'l',
					'width' => 297,
					'height' => 210
				]
			],
			'a4_l_pts' => [  // provide A4 landscape in points
				'width' => 842,
				'height' => 595,
				'measurement' => 'points',
				'expected' => [
					'success' => true,
					'paper_size_match' => 'exact',
					'paper_size_id' => 'A4',
					'orientation' => 'l',
					'width' => 297,
					'height' => 210
				]
			],
			'missing_parameter_p_mm' => [
				'width' => null,
				'height' => 297,
				'measurement' => 'mm',
				'expected' => [
					'success' => false,
					'code' => 'missing-parameter',
					'paper_size_match' => 'unmatched',
					'paper_size_id' => 'NS',
					'orientation' => 'p',
					'width' => 0,
					'height' => 297
				]
			],
			'a5_l_mm' => [ // A5 landscape
				'width' => 210,
				'height' => 148,
				'measurement' => 'mm',
				'expected' => [
					'success' => true,
					'paper_size_match' => 'exact',
					'paper_size_id' => 'A5',
					'orientation' => 'l',
					'width' => 210,
					'height' => 148
				]
			],
			'a5_l_pts' => [ // A5 landscape in points
				'width' => 595,
				'height' => 420,
				'measurement' => 'points',
				'expected' => [
					'success' => true,
					'paper_size_match' => 'exact',
					'paper_size_id' => 'A5',
					'orientation' => 'l',
					'width' => 210,
					'height' => 148
				]
			],
			'a4_us_p_mm' => [
				'width' => 215,
				'height' => 148,
				'measurement' => 'mm',
				'expected' => [
					'success' => true,
					'paper_size_match' => 'upscaled',
					'paper_size_id' => 'A4',
					'orientation' => 'l',
					'width' => 215,
					'height' => 148,
				]
			],
			'ns_us_a3_p_mm' => [
				'width' => 200,
				'height' => 300,
				'measurement' => 'mm',
				'expected' => [
					'success' => true,
					'paper_size_match' => 'upscaled',
					'paper_size_id' => 'A3',
					'orientation' => 'p',
					'width' => 200,
					'height' => 300
				]
			]
		];

		$singleTestCaseKey = '';

		foreach ($testCases as $testCaseKey => $testCase) {

			if (!$singleTestCaseKey || $singleTestCaseKey == $testCaseKey) {

				$result = $this->PaperSize->matchBySize(
					$testCase['width'],
					$testCase['height'],
					$testCase['measurement']
				);

				$this->assertEqual(
					$result,
					$testCase['expected'],
					$testCaseKey . ': ' . $testCase['width'] . '/' . $testCase['height'] . ' <> ' . $testCase['expected']['paper_size_id']
				);

			}

		}
	}

	public function testSmaller() {

		$testCases = [
			0 => [
				'paper_size_id' => 'A0',
				'expected' => 'A1'
			],
			1 => [
				'paper_size_id' => 'A1',
				'expected' => 'A2'
			],
			2 => [
				'paper_size_id' => 'A2',
				'expected' => 'A3'
			],
			3 => [
				'paper_size_id' => 'A3',
				'expected' => 'A4'
			],
			4 => [
				'paper_size_id' => 'A4',
				'expected' => 'A5'
			],
			5 => [
				'paper_size_id' => 'A5',
				'expected' => false
			],
			6 => [
				'paper_size_id' => 'A6',
				'expected' => false
			]
		];

		$singleTestCaseKey = null;

		foreach ($testCases as $testCaseKey => $testCase) {

			if (!$singleTestCaseKey || $singleTestCaseKey == $testCaseKey) {

				$result = $this->PaperSize->nextSmaller($testCase['paper_size_id']);
				$expected = $testCase['expected'];

				$this->assertEqual(
					$result,
					$expected,
					$testCaseKey . ': ' . $testCase['paper_size_id'] . ' <> ' . $testCase['expected']
				);

			}

		}

	}

	public function testLarger() {

		$testCases = [
			0 => [
				'paper_size_id' => 'A0',
				'expected' => false
			],
			1 => [
				'paper_size_id' => 'A1',
				'expected' => 'A0'
			],
			2 => [
				'paper_size_id' => 'A2',
				'expected' => 'A1'
			],
			3 => [
				'paper_size_id' => 'A3',
				'expected' => 'A2'
			],
			4 => [
				'paper_size_id' => 'A4',
				'expected' => 'A3'
			],
			5 => [
				'paper_size_id' => 'A5',
				'expected' => 'A4'
			],
			6 => [
				'paper_size_id' => 'A6',
				'expected' => false
			]
		];

		$singleTestCaseKey = 2;

		foreach ($testCases as $testCaseKey => $testCase) {

			if (!$singleTestCaseKey || $singleTestCaseKey == $testCaseKey) {

				$result = $this->PaperSize->nextLarger($testCase['paper_size_id']);
				$expected = $testCase['expected'];

				$this->assertEqual(
					$result,
					$expected,
					$testCaseKey . ': ' . $testCase['paper_size_id'] . ' <> ' . $testCase['expected']
				);

			}

		}

	}

	public function testPaperSizeScale() {

		$testCases = [
			0 => [
				'page_paper_size_id' => 'A0',
				'order_item_paper_size_id' => 'A0',
				'expected' => 0
			],
			1 => [
				'page_paper_size_id' => 'A4',
				'order_item_paper_size_id' => 'A5',
				'expected' => -100
			],
			2 => [
				'page_paper_size_id' => 'A5',
				'order_item_paper_size_id' => 'A0',
				'expected' => 500
			],
			3 => [
				'page_paper_size_id' => 'A0',
				'order_item_paper_size_id' => 'A3',
				'expected' => -300
			]
		];

		$singleTestCaseKey = null;

		foreach ($testCases as $testCaseKey => $testCase) {

			if (!$singleTestCaseKey || $singleTestCaseKey == $testCaseKey) {

				$result = $this->PaperSize->paperSizeScale(
					$testCase['page_paper_size_id'],
					$testCase['order_item_paper_size_id']
				);
				$expected = $testCase['expected'];

				$this->assertEqual(
					$result,
					$expected,
					$testCaseKey . ': ' . $testCase['page_paper_size_id'] . ' + ' . $testCase['order_item_paper_size_id'] . ' <> ' . $testCase['expected'] . ' (' . $result . ')'
				);

			}

		}

	}

}
