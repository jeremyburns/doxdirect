<?php
App::uses('OrderItemFile', 'Model');

/**
 * OrderItemFile Test Case
 *
 */
class OrderItemFileTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.binding',
		'app.binding_edge_type',
		'app.binding_side_type',
		'app.binding_type',
		'app.click',
		'app.cover_type',
		'app.delivery_option',
		'app.drilling_type',
		'app.media',
		'app.order',
		'app.order_item',
		'app.order_item_file',
		'app.order_item_file_page',
		'app.order_item_file_page_process',
		'app.order_item_tab',
		'app.order_status',
		'app.paper_size',
		'app.paypal_payment',
		'app.payment_method',
		'app.product',
		'app.promo_code',
		'app.side',
		'app.source',
		'app.turnaround_option',
		'app.user',
		'app.user_address'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OrderItemFile = ClassRegistry::init('OrderItemFile');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OrderItemFile);

		parent::tearDown();
	}

	public function testExtractPages() {

		$result = $this->OrderItemFile->extractPages('order-item-file-1');

		$expected = [
			'success' => true
		];

		$this->assertEqual($result, $expected);

		$orderItemFile = $this->OrderItemFile->find(
			'first',
			[
				'conditions' => [
					'OrderItemFile.id' => 'order-item-file-1'
				],
				'fields' => [
					'id',
					'order_item_id',
					'sequence',
					'name',
					'type',
					'error',
					'size',
					'paper_size_id',
					'paper_size_consistency',
					'paper_size_match_consistency',
					'paper_size_scale_consistency',
					'orientation',
					'orientation_consistency',
					'orientation_count_p',
					'orientation_count_l',
					'colour',
					'file_type_icon',
					'file_type_title',
					'callas_filename',
					'readable_size',
					'result',
					'extension',
					'message',
					'pages',
					'sent_to_callas',
					'count_order_item_file_page'
				],
				'contain' => [
					'OrderItemFilePage' => [
						'fields' => [
							'OrderItemFilePage.callas_id',
							'OrderItemFilePage.callas_nr',
							'OrderItemFilePage.callas_rotate',
							'OrderItemFilePage.size_box',
							'OrderItemFilePage.tl_x',
							'OrderItemFilePage.tl_y',
							'OrderItemFilePage.br_x',
							'OrderItemFilePage.br_y',
							'OrderItemFilePage.width',
							'OrderItemFilePage.height',
							'OrderItemFilePage.paper_size_id',
							'OrderItemFilePage.paper_size_match',
							'OrderItemFilePage.paper_size_scale',
							'OrderItemFilePage.orientation',
							'OrderItemFilePage.plates',
							'OrderItemFilePage.colour'
						]
					]
				]
			]
		);

		$expected = [
			'OrderItemFile' => [
				'id' => 'order-item-file-1',
				'order_item_id' => 'order-item-1',
				'sequence' => '10',
				'name' => '2page_A4_mixed_orientation.pdf',
				'type' => 'application/pdf',
				'error' => '0',
				'size' => '14128',
				'paper_size_id' => 'A4',
				'paper_size_consistency' => true,
				'paper_size_match_consistency' => true,
				'paper_size_scale_consistency' => true,
				'orientation' => 'm',
				'orientation_consistency' => false,
				'orientation_count_p' => '1',
				'orientation_count_l' => '1',
				'colour' => 'col',
				'file_type_icon' => 'fa fa-file-pdf-o',
				'file_type_title' => 'Adobe PDF',
				'callas_filename' => '2015-04-21-12-27-51-order-1-2page_A4_mixed_orientation.pdf',
				'readable_size' => '14 KB',
				'result' => 'success',
				'extension' => 'pdf',
				'message' => 'The file was successfully uploaded.',
				'pages' => 1,
				'sent_to_callas' => '2015-04-21 12:27:51',
				'count_order_item_file_page' => '2'
			],
			'OrderItemFilePage' => [
				(int) 0 => [
					'callas_id' => 'PAG2',
					'callas_nr' => '2',
					'callas_rotate' => false,
					'size_box' => '@mediabox',
					'tl_x' => null,
					'tl_y' => null,
					'br_x' => null,
					'br_y' => null,
					'width' => '210.0',
					'height' => '297.0',
					'paper_size_id' => 'A4',
					'paper_size_match' => 'exact',
					'paper_size_scale' => '0',
					'orientation' => 'p',
					'plates' => '4',
					'colour' => 'col',
					'order_item_file_id' => 'order-item-file-1'
				],
				(int) 1 => [
					'callas_id' => 'PAG1',
					'callas_nr' => '1',
					'callas_rotate' => false,
					'size_box' => '@mediabox',
					'tl_x' => null,
					'tl_y' => null,
					'br_x' => null,
					'br_y' => null,
					'width' => '297.0',
					'height' => '210.0',
					'paper_size_id' => 'A4',
					'paper_size_match' => 'exact',
					'paper_size_scale' => '0',
					'orientation' => 'l',
					'plates' => '4',
					'colour' => 'col',
					'order_item_file_id' => 'order-item-file-1'
				]
			]
		];

		$this->assertEqual($orderItemFile, $expected);

	}

	public function testSetOrderItemFilePageConsistency() {

		$orderItemFileId = 'order-item-file-1';

		$result = $this->OrderItemFile->setOrderItemFilePageConsistency($orderItemFileId);

		$expected = ['success' => true];

		$this->assertEqual($result, $expected);

		$orderItemFile = $this->OrderItemFile->find(
			'first',
			[
				'conditions' => [
					'OrderItemFile.id' => $orderItemFileId
				],
				'fields' => [
					'OrderItemFile.paper_size_consistency',
					'OrderItemFile.paper_size_match_consistency',
					'OrderItemFile.paper_size_scale_consistency',
					'OrderItemFile.orientation',
					'OrderItemFile.orientation_consistency',
					'OrderItemFile.orientation_count_p',
					'OrderItemFile.orientation_count_l'
				]
			]
		);

		$expected = [
			'OrderItemFile' => [
				'paper_size_consistency' => true,
				'paper_size_match_consistency' => true,
				'paper_size_scale_consistency' => true,
				'orientation' => 'm',
				'orientation_consistency' => false,
				'orientation_count_p' => '1',
				'orientation_count_l' => '1'
			]
		];

		$this->assertEqual($orderItemFile, $expected);

	}

/*
	public function testPaperSizeMatch() {

		$result = $this->OrderItemFile->paperSizeMatch('order-item-file-1');
		die(debug($result));
	}
*/

	public function testOrientation() {

		$orderItemFileId = 'order-item-file-1';
		$result = $this->OrderItemFile->orientation($orderItemFileId);

		$expected = [
			'success' => true,
			'consistency' => false,
			'orientation' => 'm',
			'pageOrientations' => [
				'l' => '1',
				'p' => '1'
			]
		];

		$this->assertEqual($result, $expected);

	}

	public function testProcessCallas() {

		$orderItemFile = $this->OrderItemFile->find(
			'first'
		);

		if (!$orderItemFile) {
			die(debug('No order item files to test with.'));
		}

		$orderItemFileId = $orderItemFile['OrderItemFile']['id'];

		$result = $this->OrderItemFile->processCallas($orderItemFileId);

		debug($result);
	}

	public function testRotateImage() {

		$fileName = WWW_ROOT . 'img' . DS . 'test_rotate.png';

		$rotateAngle = 90;

		$this->OrderItemFile->rotateImage($fileName, $rotateAngle);


	}

	public function testUpload() {

		$order = $this->OrderItemFile->OrderItem->Order->getOrder('order-1');

		$orderItemId = $order['OrderItem'][0]['id'];

		$files = [
			'files' => [
				'name' => [
					(int) 0 => 'a5.pdf'
				],
				'type' => [
					(int) 0 => 'application/pdf'
				],
				'tmp_name' => [
					(int) 0 => '/Applications/MAMP/tmp/php/phpjzCz20'
				],
				'error' => [
					(int) 0 => (int) 0
				],
				'size' => [
					(int) 0 => (int) 14448
				]
			]
		];

		$result = $this->OrderItemFile->upload(
			$order,
			$orderItemId,
			$files
		);

		$this->assertTrue($result['success']);

		$this->assertArrayHasKey('orderItemFileIds', $result);
	}

	public function testResequence() {

		$orderItemFileIds = $this->OrderItemFile->find(
			'list',
			[
				'conditions' => [
					'OrderItemFile.order_item_id' => 'order-item-2'
				],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.id'
				]
			]
		);

		$orderItemFileIds = array_values($orderItemFileIds);

		$result = $this->OrderItemFile->resequence($orderItemFileIds);

		$orderItemFileIds = $this->OrderItemFile->find(
			'list',
			[
				'conditions' => [
					'OrderItemFile.order_item_id' => 'order-item-2'
				],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.sequence'
				]
			]
		);

		$expected = [
			'order-item-file-2' => 1,
			'order-item-file-3' => 2
		];

		$this->assertEqual($result, $expected);

	}

}
