<?php
App::uses('ProductMapCategory', 'Model');

/**
 * ProductMapCategory Test Case
 *
 */
class ProductMapCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_map_category',
		'app.product_map_category_extra_field'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductMapCategory = ClassRegistry::init('ProductMapCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductMapCategory);

		parent::tearDown();
	}

	public function testDisplayProductCategoryFileRefreshAndDie() {

		$result = $this->ProductMapCategory->getProductMapCategories();

	}

}