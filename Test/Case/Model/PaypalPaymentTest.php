<?php
App::uses('PaypalPayment', 'Model');

/**
* PaypalPayment Test Case
*
*/
class PaypalPaymentTest extends CakeTestCase {

	/**
* Fixtures
*
* @var array
*/
	public $fixtures = array(
		'app.click',
		'app.cover_type',
		'app.job_status',
		'app.media',
		'app.delivery_option',
		'app.order',
		'app.order_item',
		'app.order_status',
		'app.payment_method',
		'app.paypal_payment',
		'app.product',
		'app.promo_code',
		'app.side',
		'app.source',
		'app.turnaround_option',
		'app.user',
		'app.user_address'
	);

	/**
* setUp method
*
* @return void
*/
	public function setUp() {
		parent::setUp();
		$this->PaypalPayment = ClassRegistry::init('PaypalPayment');
	}

	/**
* tearDown method
*
* @return void
*/
	public function tearDown() {
		unset($this->PaypalPayment);

		parent::tearDown();
	}

	private $testCases = [
		0 => [
			'mc_gross' => 3.34,
			'protection_eligibility' => 'Eligible',
			'address_status' => 'confirmed',
			'payer_id' => '275ATW85NM746',
			'tax' => 0.00,
			'address_street' => '1 Main Terrace',
			'payment_date' => '04:19:55 Jul 17, 2015 PDT',
			'payment_status' => 'Completed',
			'charset' => 'windows-1252',
			'address_zip' => 'W12 4LQ',
			'first_name' => 'Laurie',
			'mc_fee' => 0.31,
			'address_country_code' => 'GB',
			'address_name' => 'Laurie Cansfield',
			'notify_version' => 3.8,
			'custom' => '',
			'payer_status' => 'verified',
			'business' => 'jeremyb@doxdirect.com',
			'address_country' => 'United Kingdom',
			'address_city' => 'Wolverhampton',
			'quantity' => 1,
			'verify_sign' => 'ADAuA-yqjWI8Di4jgFrojD1z-NoWAmVD5sTy6mU0JAZjsZF682EvgahL',
			'payer_email' => 'nataliew@doxdirect.com',
			'txn_id' => '6SD62244JW820301W',
			'payment_type' => 'instant',
			'last_name' => 'Cansfield',
			'address_state' => 'West Midlands',
			'receiver_email' => 'jeremyb@doxdirect.com',
			'payment_fee' => 0,
			'receiver_id' => 'WE3AVA5HNVAAE',
			'txn_type' => 'web_accept',
			'item_name' => 'Printing of Documents',
			'mc_currency' => 'GBP',
			'item_number' => '55a8e327-b664-4399-bb25-4becac1f0ccd',
			'residence_country' => 'GB',
			'test_ipn' => 1,
			'handling_amount' => 0.00,
			'transaction_subject' => '',
			'payment_gross' => 0,
			'shipping' => 0.00,
			'ipn_track_id' => 'cc5a9a451e810',
		]
	];

	public function testBuildAssociationsFromIPN() {

		$data = $this->testCases[0];

		$result = $this->PaypalPayment->buildAssociationsFromIPN($data);

		$this->assertArrayHasKey('PaypalPayment', $result);

	}

	public function testReceivePayment() {

		$defaultOrder = $this->PaypalPayment->Order->defaultQuote();

		$defaultOrder['Order']['amount_net'] = 10;
		$defaultOrder['Order']['amount_due'] = 10;
		$defaultOrder['Order']['amount_total'] = 10;

		$this->PaypalPayment->Order->create();
		$result = $this->PaypalPayment->Order->save($defaultOrder);

		$orderId = $this->PaypalPayment->Order->id;

		$paypalPayment = $this->testCases[0];

		$paypalPayment['item_number'] = $orderId;
		$paypalPayment['mc_gross'] = 10;

		$paypalPayment = $this->PaypalPayment->buildAssociationsFromIPN($paypalPayment);

		$existingPaypalPaymentId = $this->PaypalPayment->searchPaypalPaymentTxn($paypalPayment);

		$this->assertFalse($existingPaypalPaymentId);

		$result = $this->PaypalPayment->saveAll($paypalPayment);

		$this->assertTrue($result);

		$paypalPaymentId = $this->PaypalPayment->id;

		$result = $this->PaypalPayment->afterPaypalNotification($paypalPaymentId);

		$expected = [
			'success' => true,
			'amount_due' => 0
		];

		$this->assertEqual($result, $expected);

	}

	public function testAfterPaypalNotification() {
die(debug('Here'));
		$paypalPayment = $this->PaypalPayment->find(
			'first',
			[
				'conditions' => [
					'PaypalPayment.order_id' => null
				]
			]
		);

		die(debug($paypalPayment));

	}

}
