<?php
App::uses('TurnaroundOption', 'Model');

/**
 * TurnaroundOption Test Case
 *
 */
class TurnaroundOptionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.binding_type',
		'app.holiday',
		'app.product',
		'app.turnaround_option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {

		$this->TurnaroundOption = ClassRegistry::init('TurnaroundOption');

		// Get a binding type that requires an extra day for processing (e.g. a book)

		$this->bindingTypeExtraDay = $this->TurnaroundOption->Order->OrderItem->Product->BindingType->field(
			'id',
			[
				'extra_processing_day >'=> 0
			]
		);

		// Get a binding type that DOES NOT require an extra day for processing (e.g. unbound)

		$this->bindingTypeNoExtraDay = $this->TurnaroundOption->Order->OrderItem->Product->BindingType->field(
			'id',
			[
				'extra_processing_day' => 0
			]
		);

		// Get a product that has a binding type that requires an extra day for processing (e.g. a book)

		$this->productExtraDay = $this->TurnaroundOption->Order->OrderItem->Product->field(
			'id',
			[
				'Product.binding_type_id'=> $this->bindingTypeExtraDay
			]
		);

		// Get a product that has a binding type that DOES NOT require an extra day for processing (e.g. a book)

		$this->productNoExtraDay = $this->TurnaroundOption->Order->OrderItem->Product->field(
			'id',
			[
				'Product.binding_type_id'=> $this->bindingTypeNoExtraDay
			]
		);

		$this->dateTimes = [
			'before' => '2015-09-09 09:00:00',
			'after' => '2015-09-09 12:00:00'
		];

		parent::setUp();

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TurnaroundOption);

		parent::tearDown();
	}

	/**
	 * This function tests the getTimestamp function in AppModel
	 */
	public function testGetTimeStamp() {

		// Not actually testing this result as we don't know what time the test is run - but it ought to not fail
		$timeStamp = $this->TurnaroundOption->getTimestamp();

		$timeStamp = $this->TurnaroundOption->getTimestamp(['format' => true]);

		$timeStamp = $this->TurnaroundOption->getTimestamp([
			'format' => true,
			'formatString' => 'H'
		]);

		$timeStamp = $this->TurnaroundOption->getTimestamp([
			'dateTime' => $this->dateTimes['before'],
			'format' => true,
		]);

		$timeStamp = $this->TurnaroundOption->getTimestamp([
			'dateTime' => $this->dateTimes['before'],
			'format' => true,
			'formatString' => 'H'
		]);

	}

	public function testDespatchDate() {

		$sameDay = 1;
		$nextDay = 2;
		$normal = 3;

		$testCases = array(
			array(
				'description' => 'Same day, Christmas eve, a Thursday, before 11, paperback',
				'order_time' => '2015-12-24 10:45:00',
				'turnaround_option_id' => $sameDay,
				'binding_type_id' => $this->bindingTypeExtraDay,
				'expected' => '2015-12-28'
			),
			array(
				'description' => 'Same day, Christmas day, a Friday, before 11, paperback',
				'order_time' => '2015-12-25 10:45:00',
				'turnaround_option_id' => $sameDay,
				'binding_type_id' => $this->bindingTypeExtraDay,
				'expected' => '2015-12-29'
			),
			array(
				'description' => 'Same day, a Monday, before 11, paperback',
				'order_time' => '2015-04-20 10:45:00',
				'turnaround_option_id' => $sameDay,
				'binding_type_id' => $this->bindingTypeExtraDay,
				'expected' => '2015-04-21'
			),
			array(
				'description' => 'Same day, a Monday, before 11, unbound',
				'order_time' => '2015-04-20 10:45:00',
				'turnaround_option_id' => $sameDay,
				'binding_type_id' => $this->bindingTypeNoExtraDay,
				'expected' => '2015-04-20'
			),
			array(
				'description' => 'Same day, a Monday, after 11, unbound',
				'order_time' => '2015-04-20 11:45:00',
				'turnaround_option_id' => $sameDay,
				'binding_type_id' => $this->bindingTypeNoExtraDay,
				'expected' => '2015-04-21'
			),
			array(
				'description' => 'Same day, a Friday, after 11, unbound',
				'order_time' => '2015-04-24 11:45:00',
				'turnaround_option_id' => $sameDay,
				'binding_type_id' => $this->bindingTypeNoExtraDay,
				'expected' => '2015-04-27'
			),
			array(
				'description' => 'Same day, a Friday, after 11, paperback',
				'order_time' => '2015-04-24 11:45:00',
				'turnaround_option_id' => $sameDay,
				'binding_type_id' => $this->bindingTypeExtraDay,
				'expected' => '2015-04-28'
			),
			array(
				'description' => 'Normal t/a, a Monday, before 11, unbound',
				'order_time' => '2015-04-20 10:45:00',
				'turnaround_option_id' => $normal,
				'binding_type_id' => $this->bindingTypeNoExtraDay,
				'expected' => '2015-04-22'
			),
			array(
				'description' => 'Normal t/a, a Thursday, before 11, unbound',
				'order_time' => '2015-04-23 10:45:00',
				'turnaround_option_id' => $normal,
				'binding_type_id' => $this->bindingTypeNoExtraDay,
				'expected' => '2015-04-27'
			)
		);

		foreach ($testCases as $testCase) {

			$orderTime = new DateTime($testCase['order_time']);

			$result = $this->TurnaroundOption->despatchDate(
				$testCase['turnaround_option_id'],
				$orderTime,
				$testCase['binding_type_id']
			);

			$this->assertEqual(
				$result['despatch_date'],
				$testCase['expected'],
				$testCase['description']
			);

		}

	}

	public function testSameDayIsAvailableAll() {

		// Set up a grid of all possible permutations
		$testCases = [
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => false,
					'dateTime' => 'before',
					'product' => 'ok'
				],
				'expected' => true
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'before',
					'product' => 'ok'
				],
				'expected' => true
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => false,
					'dateTime' => 'after',
					'product' => 'ok'
				],
				'expected' => true
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'after',
					'product' => 'ok'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => false,
					'dateTime' => 'before',
					'product' => 'bad'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'before',
					'product' => 'bad'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => false,
					'dateTime' => 'after',
					'product' => 'bad'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'after',
					'product' => 'bad'
				],
				'expected' => false
			],

			//NOW setting the qualifies_for_same_day_turnaround flag to 0 for orders only
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'before',
					'qualifies_for_same_day_turnaround' => false
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'after',
					'qualifies_for_same_day_turnaround' => false
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'before',
					'qualifies_for_same_day_turnaround' => false
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'after',
					'qualifies_for_same_day_turnaround' => false
				],
				'expected' => false
			],

			//NOW setting the qualifies_for_same_day_turnaround flag to 1 for orders only
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'before',
					'qualifies_for_same_day_turnaround' => true
				],
				'expected' => true
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'after',
					'qualifies_for_same_day_turnaround' => true
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'before',
					'qualifies_for_same_day_turnaround' => true
				],
				'expected' => true
			],
			[
				'data' => [
					'is_holiday' => false,
					'is_order' => true,
					'dateTime' => 'after',
					'qualifies_for_same_day_turnaround' => true
				],
				'expected' => false
			],



			//NOW on a holiday - have to run these last as the holiday will already be there
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => false,
					'dateTime' => 'before',
					'product' => 'ok'
				],
				'expected' => true
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'before',
					'product' => 'ok'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => false,
					'dateTime' => 'after',
					'product' => 'ok'
				],
				'expected' => true
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'after',
					'product' => 'ok'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => false,
					'dateTime' => 'before',
					'product' => 'bad'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'before',
					'product' => 'bad'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => false,
					'dateTime' => 'after',
					'product' => 'bad'
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'after',
					'product' => 'bad'
				],
				'expected' => false
			],


			//NOW setting the qualifies_for_same_day_turnaround flag to 0 for orders only on a holiday
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'before',
					'qualifies_for_same_day_turnaround' => false
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'after',
					'qualifies_for_same_day_turnaround' => false
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'before',
					'qualifies_for_same_day_turnaround' => false
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'after',
					'qualifies_for_same_day_turnaround' => false
				],
				'expected' => false
			],

			//NOW setting the qualifies_for_same_day_turnaround flag to 1 for orders only on a holiday
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'before',
					'qualifies_for_same_day_turnaround' => true
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'after',
					'qualifies_for_same_day_turnaround' => true
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'before',
					'qualifies_for_same_day_turnaround' => true
				],
				'expected' => false
			],
			[
				'data' => [
					'is_holiday' => true,
					'is_order' => true,
					'dateTime' => 'after',
					'qualifies_for_same_day_turnaround' => true
				],
				'expected' => false
			],


		];
		debug($testCases);
		// Then run through them
		foreach ($testCases as $testCaseId => $testCase) {

			if ($testCase['data']['is_holiday']) {
				// Create a holiday for a specified date
				$this->createHoliday();
			}

			$data = [
				'test_case_id' => $testCaseId,
				'dateTime' => $this->dateTimes[$testCase['data']['dateTime']],
				'is_order' => $testCase['data']['is_order']
			];

			if (isset($testCase['data']['product'])) {
				$data['product_id'] = $testCase['data']['product'] == 'ok'
					? $this->productNoExtraDay // an OK propduct
					: $this->productExtraDay // a not OK product
				;
			}

			if (isset($testCase['data']['qualifies_for_same_day_turnaround'])) {
				$data['qualifies_for_same_day_turnaround'] = $testCase['data']['qualifies_for_same_day_turnaround'];
			}

			$result = $this->TurnaroundOption->sameDayIsAvailable($data);

			debug($result);
			debug($testCase['expected']);

			$this->assertEqual($result['available'], $testCase['expected']);

		}

	}

//	public function testSameDayIsAvailableInvalid() {
//
//		// Test sending no product id - should fail
//		$result = $this->TurnaroundOption->sameDayIsAvailable();
//
//		$expected = [
//			'success' => false,
//			'allowed' => false,
//			'code' => 'no-product-or-qualifier',
//			'message' => 'That is an invalid product or order.'
//		];
//
//		$this->assertEqual($result, $expected);
//
//	}

	private function createHoliday() {

		App::uses('Holiday', 'Model');
		$this->Holiday = ClassRegistry::init('Holiday');

		$holiday = $this->Holiday->create();

		// Get our sample date
		$date = $this->dateTimes['before'];

		$date = $this->TurnaroundOption->getTimeStamp(['dateTime' => $date]);

		$data = [
			'name' => 'Test holiday',
			'day' => $date->format('d'),
			'month' => $date->format('m'),
			'year' => $date->format('Y'),
			'will_despatch' => false
		];

		$holiday = $this->Holiday->save($data);

	}

}
