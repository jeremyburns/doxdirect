<?php
App::uses('Product', 'Model');

/**
 * Product Test Case
 *
 */
class ProductTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.binding',
		'app.binding_type',
		'app.click',
		'app.cover_type',
		'app.drilling_type',
		'app.media',
		'app.order_item',
		'app.order_item_file',
		'app.paper_size',
		'app.product',
		'app.product_binding',
		'app.product_click',
		'app.product_cover_type',
		'app.product_media',
		'app.product_drilling_type',
		'app.product_side',
		'app.product_tab_type',
		'app.side',
		'app.tab_type'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Product = ClassRegistry::init('Product');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Product);

		parent::tearDown();
	}

	public function testDisplayProductCategoryFileRefreshAndDie() {

		$productCatalogue = $this->Product->getProductCatalogueFile(true);

		$this->assertArrayHasKey(0, $productCatalogue);

	}

	public function testDisplayProductCategoryFileNoRefresh() {

		$productCatalogue = $this->Product->getProductCatalogueFile(false);

		$this->assertArrayHasKey(0, $productCatalogue);

	}

	public function testDisplayProductCategoryFileNoRefreshAndDie() {

		$productCatalogue = $this->Product->getProductCatalogueFile(false);

		$this->assertArrayHasKey(0, $productCatalogue);

	}

	public function testRefreshProductCategory() {

		$result = $this->Product->refreshProductCatalogue(false);

		$expected = ['success' => true];

		$this->assertEqual($expected, $result);

	}

	public function testquoteOptions() {

		$result = $this->Product->quoteOptions();

		$expected = $this->Product->find(
			'list',
			[
				'fields' => [
					'Product.id',
					'Product.name'
				],
				'conditions' => [
					'Product.is_active' => true
				]
			]
		);

		$this->assertEqual($result, $expected);

	}

	public function testProductMap() {

		$result = $this->Product->productMap(true);

		die(debug($result));

	}

	public function testPageCountValidation() {

		$orderItemId = 'order-item-3';

		$orderItem = $this->Product->OrderItem->find(
			'first',
			[
				'conditions' => [
					'OrderItem.id' => $orderItemId
				],
				'contain' => [
					'Side',
					'OrderItemFile'
				]
			]
		);

		$data = $orderItem['OrderItem'];
		$data['Side'] = $orderItem['Side'];
		$data['OrderItemFile'] = $orderItem['OrderItemFile'];

		$result = $this->Product->pageCountValidation($data);

		$expected = [
			'success' => false,
			'code' => 'page-count-divisor',
			'message' => 'The number of pages in a saddlestitch document must be divisible by 4. As your file has 10 pages we need to insert 2 blank pages to bring the total to 12 pages.',
			'data' => [
				'insert_page_count' => (int) 2
			]
		];

		$this->assertEqual($result, $expected);


	}

}