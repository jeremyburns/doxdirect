<?php
App::uses('HolidayAlert', 'Model');

/**
 * HolidayAlert Test Case
 *
 */
class HolidayAlertTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.HolidayAlert'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->HolidayAlert = ClassRegistry::init('HolidayAlert');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->HolidayAlert);

		parent::tearDown();
	}

	public function testChristmasAlert() {

		$testCases = [
			[
				'description' => 'Christmas period',
				'date' => '2015-12-23',
				'expected' => [
					'HolidayAlert' => [
						'heading' => 'Orders placed over the Christmas period',
						'text' => 'Please note that orders placed over the Christmas period may take longer to prepare and deliver.'
					]
				]
			],
			[
				'description' => 'Just after Christmas period',
				'date' => '2016-01-03',
				'expected' => false
			],
			[
				'description' => 'Bank holiday Monday',
				'date' => '2015-04-30',
				'expected' => false
			]
		];

		foreach ($testCases as $testCase) {

			$date = new DateTime($testCase['date']);

			$result = $this->HolidayAlert->message($date);

			$this->assertEqual(
				$result,
				$testCase['expected'],
				$testCase['description']
			);

		}
	}

}