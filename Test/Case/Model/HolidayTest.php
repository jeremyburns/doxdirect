<?php
App::uses('Holiday', 'Model');

/**
 * Holiday Test Case
 *
 */
class HolidayTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.holiday'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Holiday = ClassRegistry::init('Holiday');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Holiday);

		parent::tearDown();
	}

	public function testIsChristmasDayAHoliday() {

		$date = new DateTime('2015-12-25');

		$result = $this->Holiday->isHoliday($date);

		$expected = array(
			'Holiday' => array(
				'name' => 'Christmas Day',
				'will_despatch' => false,
				'will_collect' => false,
				'will_deliver' => false
			)
		);

		$this->assertEqual($result, $expected);

	}

	public function testIsNotAHoliday() {

		$date = new DateTime('2015-10-25');

		$result = $this->Holiday->isHoliday($date);

		$expected = false;

		$this->assertEqual($result, $expected);

	}

/*
	public function testIsDateAHoliday() {

		$date = new DateTime('2015-05-04');

		$result = $this->Holiday->isHoliday($date);

		$expected = array(
			'Holiday' => array(
				'name' => 'May Bank Holiday Monday',
				'will_despatch' => false,
				'will_collect' => false,
				'will_deliver' => false
			)
		);

		$this->assertEqual($result, $expected);

	}
*/

	public function testSundayIsAWeekend() {

		$date = new DateTime('2015-04-26'); // A Sunday

		$result = $this->Holiday->isWeekend($date);

		$this->assertTrue($result);

	}

	public function testSaturdayIsAWeekend() {

		$date = new DateTime('2015-04-25'); // A Saturday

		$result = $this->Holiday->isWeekend($date);

		$this->assertTrue($result);

	}

	public function testThursdayIsNotAWeekend() {

		$date = new DateTime('2015-04-30'); // A Thursday

		$result = $this->Holiday->isWeekend($date);

		$this->assertFalse($result);

	}

	public function testWillNotDeliverOnChristmasDay() {

		$date = new DateTime('2015-12-25');

		$result = $this->Holiday->will('deliver', $date);

		$this->assertFalse($result);

	}

	public function testWillDeliverOnMondayAfterChristmasDay() {

		$date = new DateTime('2015-12-28');

		$result = $this->Holiday->will('deliver', $date);

		$this->assertTrue($result);

	}

	public function testWillDespatchOnChristmasEve() {

		$date = new DateTime('2015-12-24');

		$result = $this->Holiday->will('despatch', $date);

		$this->assertTrue($result);

	}

}
