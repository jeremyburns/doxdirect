<?php
App::uses('User', 'Model');

/**
 * User Test Case
 *
 */
class UserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user',
		'app.order',
		'app.promo_code',
		'app.order_status',
		'app.order_date',
		'app.delivery_option',
		'app.order_item',
		'app.product',
		'app.paper_size',
		'app.drilling_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->User = ClassRegistry::init('User');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->User);

		parent::tearDown();
	}

	public function testRegisterOK() {

		$newUser = array(
			'User' => array(
				'password' => 'password',
				'id' => '',
				'first_name' => 'First',
				'last_name' => 'Name',
				'username' => 'user@random.com',
				'phone' => '1234567890'
			)
		);

		$result = $this->User->register($newUser);

		$this->assertEqual($result['success'], true);

	}

	public function testRegisterDuplicate() {

		$newUser = array(
			'User' => array(
				'password' => 'password',
				'id' => '',
				'first_name' => 'First',
				'last_name' => 'Name',
				'username' => 'user@random.com',
				'phone' => '1234567890'
			)
		);

		// Register the user once
		$this->User->register($newUser);

		// Then try to register it again
		$result = $this->User->register($newUser);

		// It should fail
		$this->assertEqual($result['code'], 'user-already-exists');

	}

	public function testRegisterInvalid() {

		$newUser = array(
			'User' => array(
				'password' => 'password',
				'id' => '',
				'first_name' => 'First',
				'username' => 'user@random.com',
				'phone' => '1234567890'
			)
		);

		$result = $this->User->register($newUser);

		$expected = [
			'success' => false,
			'code' => 'user-create-failed',
			'errors' => [
				'last_name' => [
					(int) 0 => 'Please enter your last name.'
				]
			]
		];

		$this->assertEqual($result, $expected);

	}

	public function testRegisterNoPassword() {

		// The register function will add a random password

		$newUser = [
			'User' => [
				'id' => '',
				'first_name' => 'First',
				'last_name' => 'Last',
				'username' => 'user@random.com',
				'phone' => '1234567890'
			]
		];

		$result = $this->User->register($newUser);

		unset($result['User']['password']);
		unset($result['User']['modified']);
		unset($result['User']['created']);
		unset($result['User']['id']);

		$expected = [
			'success' => true,
			'User' => [
				'user_supplied_password' => false,
				'failed_login_attempts' => '0',
				'role' => 'user',
				'count_user_address' => '0',
				'count_order' => '0',
				'first_name' => 'First',
				'last_name' => 'Last',
				'username' => 'user@random.com',
				'phone' => '1234567890',
				'full_name' => 'First Last'
			]
		];

		$this->assertEqual($result, $expected);

	}

	public function testSendMail() {

		App::uses('CakeEmail', 'Network/Email');
		$Email = new CakeEmail('default');
		$Email->from(['jeremyb@doxdirect.com' => 'Doxdirect'])
			->sender('jeremyb@doxdirect.com', 'Doxdirect')
			->to('jeremyburns@me.com')
			->subject('Password reset')
			->send('Here is your link.')
		;

	}

}
