<?php
App::uses('DrillingType', 'Model');

/**
 * DrillingType Test Case
 *
 */
class DrillingTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.drilling_type',
		'app.order_item',
		'app.order',
		'app.user',
		'app.promo_code',
		'app.order_status',
		'app.order_date',
		'app.delivery_option',
		'app.product',
		'app.paper_size'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DrillingType = ClassRegistry::init('DrillingType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DrillingType);

		parent::tearDown();
	}

	public function testConvertToPaceCode() {

		// pace_code	pace_name	holes	LEAVES

		// 5789	Drilling not required	0	0

		// 5787	2 Hole Drill up to 10 leaves	2	1
		// 5852	2 Hole Drill 11 to 49 leaves	2	11
		// 5853	2 Hole Drill 50 to 125 leaves	2	50
		// 5854	2 Hole Drill 126 to 250 leaves	2	126
		// 5855	2 Hole Drill over 250 leaves	2	250

		// 5788	4 Hole Drill up to 10 leaves	4	1
		// 5856	4 Hole Drill 11 to 49 leaves	4	11
		// 5857	4 Hole Drill 50 to 125 leaves	4	50
		// 5858	4 Hole Drill 126 to 250 leaves	4	126
		// 5860	4 Hole Drill more than 250 leaves	4	250

		// params ($holes = 0, $pages = 0, $sides = 1)

		// Test with no leaves

		$result = $this->DrillingType->convertToPaceCode(0, 1, 1);
		$this->assertNull($result);

		// Test each breakpoint with 2 holes

		$result = $this->DrillingType->convertToPaceCode(2, 1, 2);
		$this->assertEqual($result, 5787);

		$result = $this->DrillingType->convertToPaceCode(2, 10, 1);
		$this->assertEqual($result, 5787);

		$result = $this->DrillingType->convertToPaceCode(2, 11, 1);
		$this->assertEqual($result, 5852);

		$result = $this->DrillingType->convertToPaceCode(2, 50, 1);
		$this->assertEqual($result, 5853);

		$result = $this->DrillingType->convertToPaceCode(2, 126, 1);
		$this->assertEqual($result, 5854);

		$result = $this->DrillingType->convertToPaceCode(2, 250, 1);
		$this->assertEqual($result, 5855);

		$result = $this->DrillingType->convertToPaceCode(2, 1000, 1);
		$this->assertEqual($result, 5855);

		// Test each breakpoint with 4 holes

		$result = $this->DrillingType->convertToPaceCode(4, 1, 2);
		$this->assertEqual($result, 5788);

		$result = $this->DrillingType->convertToPaceCode(4, 10, 1);
		$this->assertEqual($result, 5788);

		$result = $this->DrillingType->convertToPaceCode(4, 11, 1);
		$this->assertEqual($result, 5856);

		$result = $this->DrillingType->convertToPaceCode(4, 50, 1);
		$this->assertEqual($result, 5857);

		$result = $this->DrillingType->convertToPaceCode(4, 126, 1);
		$this->assertEqual($result, 5858);

		$result = $this->DrillingType->convertToPaceCode(4, 250, 1);
		$this->assertEqual($result, 5860);

		$result = $this->DrillingType->convertToPaceCode(4, 1000, 1);
		$this->assertEqual($result, 5860);

	}

}
