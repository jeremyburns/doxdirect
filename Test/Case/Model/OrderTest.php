<?php

App::uses('Order', 'Model');

/**
 * Order Test Case
 *
 */
class OrderTest extends CakeTestCase {

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = [
		'app.binding',
		'app.binding_edge_type',
		'app.binding_side_type',
		'app.binding_type',
		'app.click',
		'app.country',
		'app.cover_type',
		'app.currency',
		'app.delivery_option',
		'app.drilling_type',
		'app.holiday',
		'app.job_status',
		'app.media',
		'app.order',
		'app.order_status',
		'app.order_date',
		'app.order_item',
		'app.order_item_file',
		'app.order_item_file_page',
		'app.order_item_file_page_process',
		'app.order_item_process',
		'app.order_item_tab',
		'app.paper_size',
		'app.payment_method',
		'app.paypal_payment',
		'app.process',
		'app.product',
		'app.product_binding',
		'app.product_binding_edge_type',
		'app.product_binding_side_type',
		'app.product_click',
		'app.product_cover_type',
		'app.product_media',
		'app.product_drilling_type',
		'app.product_side',
		'app.product_tab_type',
		'app.promo_code',
		'app.side',
		'app.source',
		'app.tab_type',
		'app.test_quote',
		'app.turnaround_option',
		'app.user',
		'app.user_address'
	];

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->Order = ClassRegistry::init('Order');
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown() {
		unset($this->Order);
		unset($this->TestQuote);
		parent::tearDown();
	}

	public $sampleOrders = [
		'single_unbound' => [
			'Order' => [
				'turnaround_option_id' => '3',
				'delivery_option_id' => '00'
			],
			'OrderItem' => [
				'a4_unbound'
			]
		],
		'multiple_unbound' => [
			'Order' => [
				'turnaround_option_id' => '3',
				'delivery_option_id' => '00'
			],
			'OrderItem' => [
				'a4_unbound',
				'a3_unbound'
			]
		],
		'single_hardback' => [
			'Order' => [
				'turnaround_option_id' => '3',
				'delivery_option_id' => '00'
			],
			'OrderItem' => [
				'a4_hardback'
			]
		],
		'multiple_mixed' => [
			'Order' => [
				'turnaround_option_id' => '3',
				'delivery_option_id' => '00'
			],
			'OrderItem' => [
				'a4_hardback',
				'a5_unbound',
			]
		],
		'single_wirebound' => [
			'Order' => [
				'turnaround_option_id' => '3',
				'delivery_option_id' => '00'
			],
			'OrderItem' => [
				'a4_wirebound'
			]
		]
	];

	public $sampleOrderItems = [
		'a5_unbound' => [
			'product_id' => 5026, // a5 unbound
			'pages' => 27,
			'copies' => 1,
			'side_id' => 5727,
			'click_id' => 5720, // B&W
			'media_id' => 5778, // 100gsm White Silk
			'drilling_type_id' => 5789, // No drilling
			'tab_count' => 0
		],
		'a4_unbound' => [
			'product_id' => 5029, // A4 unbound
			'pages' => 1,
			'copies' => 1,
			'side_id' => 5727,
			'click_id' => 5712,
			'media_id' => 5708, // 120gsm White Matt
			'drilling_type_id' => 5789, // No drilling
			'tab_count' => 0
		],
		'a3_unbound' => [
			'product_id' => 5031, // A3 unbound
			'pages' => 1,
			'copies' => 1,
			'side_id' => 5727,
			'click_id' => 5706, // b&w
			'media_id' => 5782, // 120gsm White Matt
			'drilling_type_id' => 5789, // No drilling
			'tab_count' => 0
		],
		'a4_hardback' => [
			'product_id' => 5041, // A4 hardback
			'binding_id' => 5861, // Portrait
			'pages' => 27,
			'copies' => 1,
			'side_id' => 5727, // single
			'click_id' => 5705, // b&w
			'media_id' => 5713, // 100gsm White Silk
			'tab_count' => 0
		],
		'a5_hardback' => [
			'product_id' => 5040, // A5 hardback
			'binding_id' => 5862, // Portrait
			'pages' => 27,
			'copies' => 1,
			'side_id' => 5727,
			'click_id' => 5720,
			'media_id' => 5778, // 100gsm White Silk
			'tab_count' => 0
		],
		'a4_wirebound' => [
			'product_id' => 5033, // A4 wirebound
			'binding_id' => 5932, // Portrait
			'pages' => 30,
			'copies' => 1,
			'side_id' => 5727, // single
			'click_id' => 5705, // b&w
			'media_id' => 5713, // 100gsm White Silk
			'tab_count' => 0
		]
	];

	public $sampleOrderItemFiles = [
		'a4_portrait_pdf' => [
			'name' => 'A4 Portrait PDF',
			'type' => 'application/pdf',
			'error' => 0,
			'size' => 10000,
			'paper_size_id' => 'A4',
			'paper_size_consistency' => 1,
			'paper_size_match_consistency' => 1,
			'orientation' => 'p',
			'orientation_consistency' => 1,
			'orientation_count_p' => 1,
			'orientation_count_l' => 0,
			'has_trim_marks' => 0,
			'callas_filename' => 'callas_file_name',
			'result' => 'success',
			'extension' => 'pdf',
			'pages' => 1,
			'sent_to_callas' => '2015-06-01',
			'callas_info' => '{}'
		],
		'a4_landscape_pdf' => [
			'name' => 'A4 Portrait PDF',
			'type' => 'application/pdf',
			'error' => 0,
			'size' => 10000,
			'paper_size_id' => 'A4',
			'paper_size_consistency' => 1,
			'paper_size_match_consistency' => 1,
			'orientation' => 'l',
			'orientation_consistency' => 1,
			'orientation_count_p' => 0,
			'orientation_count_l' => 1,
			'has_trim_marks' => 0,
			'callas_filename' => 'callas_file_name',
			'result' => 'success',
			'extension' => 'pdf',
			'pages' => 1,
			'sent_to_callas' => '2015-06-01',
			'callas_info' => '{}'
		],
		'a3_portrait_pdf' => [
			'name' => 'A3 Portrait PDF',
			'type' => 'application/pdf',
			'error' => 0,
			'size' => 10000,
			'paper_size_id' => 'A3',
			'paper_size_consistency' => 1,
			'paper_size_match_consistency' => 1,
			'orientation' => 'p',
			'orientation_consistency' => 1,
			'orientation_count_p' => 1,
			'orientation_count_l' => 0,
			'has_trim_marks' => 0,
			'callas_filename' => 'callas_file_name',
			'result' => 'success',
			'extension' => 'pdf',
			'pages' => 1,
			'sent_to_callas' => '2015-06-01',
			'callas_info' => '{}'
		],
		'a3_landscape_pdf' => [
			'name' => 'A3 Portrait PDF',
			'type' => 'application/pdf',
			'error' => 0,
			'size' => 10000,
			'paper_size_id' => 'A3',
			'paper_size_consistency' => 1,
			'paper_size_match_consistency' => 1,
			'orientation' => 'l',
			'orientation_consistency' => 1,
			'orientation_count_p' => 0,
			'orientation_count_l' => 1,
			'has_trim_marks' => 0,
			'callas_filename' => 'callas_file_name',
			'result' => 'success',
			'extension' => 'pdf',
			'pages' => 1,
			'sent_to_callas' => '2015-06-01',
			'callas_info' => '{}'
		]
	];

	public $sampleOrderItemFilePages = [
		'a5_portrait' => [
			'size_box' => '@mediabox',
			'tl_x' => 0,
			'tl_y' => 0,
			'br_x' => 420.0,
			'br_y' => 595.0,
			'width' => 148.0,
			'height' => 210.0,
			'paper_size_id' => 'A5',
			'orientation' => 'p'
		],
		'a5_landscape' => [
			'size_box' => '@mediabox',
			'tl_x' => 0,
			'tl_y' => 0,
			'br_x' => 595.2,
			'br_y' => 841.8,
			'width' => 210.0,
			'height' => 148.0,
			'paper_size_id' => 'A4',
			'orientation' => 'l'
		],
		'a4_portrait' => [
			'size_box' => '@mediabox',
			'tl_x' => 0,
			'tl_y' => 0,
			'br_x' => 595.2,
			'br_y' => 841.8,
			'width' => 210.0,
			'height' => 297.0,
			'paper_size_id' => 'A4',
			'orientation' => 'p'
		],
		'a4_landscape' => [
			'size_box' => '@mediabox',
			'tl_x' => 0,
			'tl_y' => 0,
			'br_x' => 841.8,
			'br_y' => 595.2,
			'width' => 297.0,
			'height' => 210.0,
			'paper_size_id' => 'A4',
			'orientation' => 'l'
		],
		'a3_portrait' => [
			'size_box' => '@mediabox',
			'tl_x' => 0,
			'tl_y' => 0,
			'br_x' => 595.2,
			'br_y' => 841.8,
			'width' => 297.0,
			'height' => 420.0,
			'paper_size_id' => 'A3',
			'orientation' => 'p'
		],
		'a3_landscape' => [
			'size_box' => '@mediabox',
			'tl_x' => 0,
			'tl_y' => 0,
			'br_x' => 595.2,
			'br_y' => 841.8,
			'width' => 420.0,
			'height' => 297.0,
			'paper_size_id' => 'A3',
			'orientation' => 'l'
		]
	];

	public function testPaceAddToQuote() {

		$data = $this->prepareSampleOrder('single_unbound');

		$orderItemKey = 0;

		$order = $this->Order->quote($data, $orderItemKey);

		$this->assertEquals($order['Order']['amount_total'], 3.55);

		$orderId = $order['Order']['id'];

		$result = $this->Order->OrderItem->addNewOrderItemToOrder($orderId);

		$order = $this->Order->getOrder(
			$orderId,
			[
				'includeOrderContains' => false,
				'includeOrderItemContains' => false
			]
		);

		$orderItemKey = 1;

		$data = $this->prepareSampleOrder('single_hardback');

		$order['OrderItem'][$orderItemKey] = array_merge(
			$order['OrderItem'][$orderItemKey],
			$data['OrderItem'][0]
		);

		$result = $this->Order->quote($order, $orderItemKey);

		$this->assertEquals($result['Order']['amount_total'], 33.29);
		$this->assertEquals(count($result['OrderItem']), 2);
	}

	public function testDefaultQuoteData() {

		$result = $this->Order->defaultQuote();

		$expected = [
			'Order' => [
				'pace_quote_id' => null,
				'order_status_id' => 'quote',
				'job_status_id' => 'incomplete',
				'turnaround_option_id' => '3',
				'delivery_option_id' => '00',
				'count_order_item' => 0,
				'amount_total' => 0
			],
			'success' => true,
			'OrderItem' => [
				0 => [
					'order_id' => null,
					'pace_quote_product_id' => null,
					'name' => null,
					'pages' => 1,
					'copies' => 1,
					'click_id' => '5705',
					'side_id' => '5727',
					'tab_count' => 0,
					'product_id' => '5029',
					'media_id' => '5708',
					'cover_type_inner_front_id' => '5792',
					'cover_type_inner_back_id' => '5839',
					'cover_type_outer_front_id' => '5844',
					'cover_type_wrap_around_id' => '7220'
				]
			]
		];

		$this->assertEquals($result, $expected);
	}

	public function testCompareQuotes() {

		$this->TestQuote = ClassRegistry::init('TestQuote');
		$this->TestQuoteSource = ClassRegistry::init('TestQuote');
		$this->TestQuoteSource->setDatasource('default');

		$testQuotes = $this->TestQuote->find('all');

		foreach ($testQuotes as $testQuoteId => $testQuote) {

			$data = [
				'Order' => [
					'turnaround_option_id' => $testQuote['TestQuote']['turnaround_option_id'],
					'delivery_option_id' => $testQuote['TestQuote']['delivery_option_id'],
					'session_id' => 'test'
				],
				'OrderItem' => [
					0 => [
						'product_id' => $testQuote['TestQuote']['product_id'],
						'pages' => $testQuote['TestQuote']['pages'],
						'copies' => $testQuote['TestQuote']['copies'],
						'side_id' => $testQuote['TestQuote']['side_id'],
						'click_id' => $testQuote['TestQuote']['click_id'],
						'media_id' => $testQuote['TestQuote']['media_id'],
						// 'holes' => $testQuote['TestQuote']['holes'],
						'cover_type_inner_front_id' => $testQuote['TestQuote']['cover_type_inner_front_id'],
						'cover_type_inner_back_id' => $testQuote['TestQuote']['cover_type_inner_back_id'],
						'cover_type_outer_front_id' => $testQuote['TestQuote']['cover_type_outer_front_id'],
						'cover_type_wrap_around_id' => $testQuote['TestQuote']['cover_type_wrap_around_id'],
						'tab_count' => $testQuote['TestQuote']['tab_count']
					]
				]
			];

			$expectedPrice = $testQuote['TestQuote']['expected_price'];

			$quote = $this->Order->quote($data);

			$this->assertArrayHasKey('Order', $quote, 'The quote failed. No order key (' . $testQuoteId . ').');

			if (!empty($quote['Order']['amount_net'])) {

				$pacePrice = $quote['Order']['amount_net'];

				$this->TestQuoteSource->id = $testQuote['TestQuote']['id'];
				$this->TestQuoteSource->saveField('pace_price', $pacePrice);

// 		$this->assertEquals($expectedPrice, $pacePrice, 'Expected: ' . $expectedPrice . '; Pace price: ' . $pacePrice);
			}
		}
	}

	private function prepareSampleOrder($sampleOrderCode) {

		$sampleOrder = $this->sampleOrders[$sampleOrderCode];

		foreach ($sampleOrder['OrderItem'] as $sampleProductCode) {
			$sampleOrderItems[] = $this->sampleOrderItems[$sampleProductCode];
		}

		$sampleOrder['OrderItem'] = $sampleOrderItems;

		return $sampleOrder;
	}

	private function prepareFullOrder($testCase = []) {

		foreach ($testCase['order_item'] as $orderItem) {

			// Start with a new simple quote
			$data = $this->prepareSampleOrder($orderItem['type']);

			// Do the quote to create an order
			$order = $this->Order->quote($data, 0);

			// Get the order id
			$orderId = $order['Order']['id'];

			// Extract the order
			$order = $this->Order->getOrder($orderId);

			// Get the new order item id
			$orderItemId = $order['OrderItem'][0]['id'];

			$orderItemPaperSizeId = $order['OrderItem'][0]['Product']['paper_size_id'];

			foreach ($orderItem['order_item_file'] as $orderItemFile) {

				// Add a sample file to the order item
				$orderItemFileId = $this->addFileToSampleOrder($orderItemId, $orderItemFile['type']);

				$orderItemFilePages = $orderItemFile['order_item_file_page'];

				$pageNumber = 0;

				foreach ($orderItemFilePages as $orderItemFilePage) {

					$quantity = $orderItemFilePage['quantity'];

					for ($n = 1; $n <= $quantity; $n++) {

						$orderItemFilePageId = $this->addPageToSampleOrder(
							$orderItemFileId,
							$orderItemFilePage['type'],
							$pageNumber
						);
						$pageNumber++;

						$page = $this->Order->OrderItem->OrderItemFile->OrderItemFilePage->find(
							'first',
							[
								'OrderItemFilePage.id' => $orderItemFilePageId
							]
						);

						// Is this page scaled compared with the size chosen in the order?
						$paperSizeScale = $this->Order->OrderItem->OrderItemFile->OrderItemFilePage->PaperSize->paperSizeScale(
							$page['OrderItemFilePage']['paper_size_id'],
							$orderItemPaperSizeId
						);

						$this->Order->OrderItem->OrderItemFile->OrderItemFilePage->id = $orderItemFilePageId;
						$this->Order->OrderItem->OrderItemFile->OrderItemFilePage->set('paper_size_scale', $paperSizeScale);
						$this->Order->OrderItem->OrderItemFile->OrderItemFilePage->save();

					}
				}

				$this->Order->OrderItem->OrderItemFile->analysePages($orderItemFileId);

/*
				$this->Order->OrderItem->OrderItemFile->setOrientationConsistency($orderItemFileId);
				$this->Order->OrderItem->setOrientationConsistency($orderItemId);
*/
			}

			$this->Order->OrderItem->collatePages($orderItemId);

		}

		$order = $this->Order->getOrder($orderId);

		return $order;


	}

	private function addFileToSampleOrder($orderItemId = null, $fileType = '', $options = []) {

		if (!$orderItemId || !$fileType) {
			return [
				'success' => false,
				'code' => 'missing-parameter',
				'from' => 'OrderTest.addFileToSampleOrder'
			];
		}

		$file = $this->sampleOrderItemFiles[$fileType];

		if (!$file) {
			return [
				'success' => false,
				'code' => 'invalid_filetype',
				'from' => 'OrderTest.addFileToSampleOrder',
				'data' => $fileType
			];
		}

		$file['order_item_id'] = $orderItemId;

		if ($options) {
			foreach ($options as $fieldName => $value) {
				$file[$fieldName] = $value;
			}
		}

		$this->Order->OrderItem->OrderItemFile->create();
		$this->Order->OrderItem->OrderItemFile->set($file);
		if ($this->Order->OrderItem->OrderItemFile->save()) {
			return $this->Order->OrderItem->OrderItemFile->id;
		} else {
			return false;
		}
	}

	private function addPageToSampleOrder($orderItemFileId = null, $pageType = '', $pageNumber = 1, $options = []) {

		if (!$orderItemFileId || !$pageType) {
			return [
				'success' => false,
				'code' => 'missing-parameter',
				'from' => 'OrderTest.addPagesToSampleOrder'
			];
		}

		$page = $this->sampleOrderItemFilePages[$pageType];

		if (!$page) {
			return [
				'success' => false,
				'code' => 'invalid_pagetype',
				'from' => 'OrderTest.addPagesToSampleOrder',
				'data' => $pageType
			];
		}

		$page['order_item_file_id'] = $orderItemFileId;
		$page['callas_id'] = 'PAG' . $pageNumber;
		$page['callas_nr'] = $pageNumber;

		if ($options) {
			foreach ($options as $fieldName => $value) {
				$page[$fieldName] = $value;
			}
		}

		$this->Order->OrderItem->OrderItemFile->OrderItemFilePage->create();
		$this->Order->OrderItem->OrderItemFile->OrderItemFilePage->set($page);
		if ($this->Order->OrderItem->OrderItemFile->OrderItemFilePage->save()) {
			return $this->Order->OrderItem->OrderItemFile->OrderItemFilePage->id;
		} else {
			return false;
		}
	}

	public function testLatestDespatchDate() {

		$data = $this->prepareSampleOrder('single_unbound');

		$orderItemKey = 0;

		$order = $this->Order->quote($data, $orderItemKey);

		$orderId = $order['Order']['id'];

		$this->Order->OrderItem->addNewOrderItemToOrder($orderId);

		$order = $this->Order->getOrder($orderId);

		$orderItemKey = 1;

		$data = $this->prepareSampleOrder('single_hardback');

		$order['OrderItem'][$orderItemKey] = array_merge(
				$order['OrderItem'][$orderItemKey], $data['OrderItem'][0]
		);

		$result = $this->Order->quote($order, $orderItemKey);

		/*
		  $this->assertEquals($result['Order']['amount_total'], 33.29);
		  $this->assertEquals(count($result['OrderItem']), 2);
		 */
	}

	public function testQuoteVanilla() {

		$data = $this->prepareSampleOrder('single_unbound');

		$result = $this->Order->quote($data);

		$this->assertEquals($result['Order']['order_status_id'], 'quote');
		$this->assertEquals($result['Order']['latest_quote_result'], true);
		$this->assertEquals($result['Order']['amount_net'], 0.30);
		$this->assertEquals($result['Order']['amount_delivery'], 3.25);
		$this->assertEquals($result['Order']['amount_total'], 3.55);
		$this->assertArrayHasKey(0, $result['OrderItem']);
	}

	public function testQuoteFail() {

		$paceQuoteResult = array(
			'success' => false,
			'data' => array(
				'Order' => array(
					'pace_quote_id' => '',
					'id' => '',
					'turnaround_option_id' => '3',
					'delivery_option_id' => '00',
					'turnaround' => '2',
					'ip_address' => '::1',
					'session_id' => '4d790dc3e65804ae777ca2fda530fb3d'
				),
				'OrderItem' => array(
					(int) 0 => array(
						'id' => '',
						'pace_quote_product_id' => '',
						'product_id' => '5044',
						'binding_id' => '5892',
						'pages' => '1',
						'copies' => '1',
						'side_id' => '5727',
						'click_id' => '5720',
						'media_id' => '5759',
						'drilling_type_id' => '5852',
						'cover_type_inner_front_id' => '',
						'name' => 'A5 Ring Binder'
					)
				)
			),
			'paceData' => array(
				'source' => 'DOX',
				'ipAddress' => '::1',
				'productType' => '5044',
				'copies' => '1',
				'pagesPerCopy' => '1',
				'turnaround' => '2',
				'delivery' => '00',
				'items' => '5759,5892,5852,5727,5720'
			),
			'response' => array(
				'status' => (int) 0,
				'message' => 'The document does not appear to fit within any of the ring binder options.'
			),
			'code' => 'pace-status-0',
			'message' => 'The document does not appear to fit within any of the ring binder options.',
			'functionName' => 'new_quote',
			'from' => 'Order.getQuote'
		);

		// TODO make the above fail gracefully.
	}

	/*
	  public function testGetPaceQuote() {

	  // 5720, // A5_BW
	  // 5759, // A5_White_plain_120
	  // 5726, // Double Sided
	  // 5768, // A5 Double Sided BW 120gsm Unbound
	  // 5789 // Drilling not required

	  $data = array(
	  'source' => 'DOX',
	  'productType' => 5026,
	  'copies' => 1,
	  'pagesPerCopy' => 1,
	  'turnaround' => 2,
	  'delivery' => '00',
	  'items' => '5720,5759,5726,5768,5789'
	  );

	  $result = $this->Order->getQuote($data);

	  unset($result['response']['output']['quote_id']);
	  unset($result['response']['output']['products'][0]['quote_product_id']);
	  unset($result['response']['execution_time']);
	  unset($result['execution_time']);

	  $expectedQuote = array(
	  'result' => 'success',
	  'data' => array(
	  'source' => 'DOX',
	  'productType' => '5026',
	  'copies' => '1',
	  'pagesPerCopy' => '1',
	  'turnaround' => '2',
	  'items' => '5720,5759,5726,5768,5789'
	  ),
	  'response' => array(
	  'status' => 1,
	  'output' => array(
	  'products' => array(
	  0 => array(
	  'price' => '0.06',
	  'turnaround' => 0
	  )
	  )
	  )
	  ),
	  'price' => '0.06',
	  'turnaround' => 0
	  );

	  $this->assertEquals($result, $expectedQuote);

	  }
	 */

	function testQuoteParameters() {

		$result = $this->Order->quoteParameters();

		$this->assertTrue(is_array($result));
	}

	function testGetQuoteOptions() {

		$result = $this->Order->quoteParameters(false);

		$expected = [
			'deliveryOptions' => [
				'00' => 'Royal Mail 1st Class',
				'07' => 'UPS Express',
				'11' => 'UPS Standard',
				'54' => 'UPS Worldwide Express Plus',
				'65' => 'UPS Saver'
			],
			'turnaroundOptions' => [
				1 => 'Same day processing (before 11am)',
				2 => 'Next day processing',
				3 => 'Two day processing'
			],
			'products' => [
				'Corner Stapled' => [
					5046 => 'A4 Corner Stapled',
					5053 => 'A5 Corner Stapled'
				],
				'Hardback' => [
					5041 => 'A4 Hardback',
					5040 => 'A5 Hardback'
				],
				'Paperback' => [
					5043 => 'A4 Paperback',
					5042 => 'A5 Paperback'
				],
				'Poster' => [
					5039 => 'A0 Poster',
					5038 => 'A1 Poster',
					5037 => 'A2 Poster'
				],
				'Ring Binder' => [
					5030 => 'A4 Ring Binder',
					5044 => 'A5 Ring Binder'
				],
				'Saddle Stitched' => [
					5036 => 'A4 Saddle Stitched',
					5035 => 'A5 Saddle Stitched'
				],
				'Spiral Bound' => [
					5034 => 'A3 Spiral Bound',
					5033 => 'A4 Spiral Bound',
					5032 => 'A5 Spiral Bound'
				],
				'Unbound' => [
					5031 => 'A3 Unbound',
					5029 => 'A4 Unbound',
					5026 => 'A5 Unbound'
				]
			],
			'sides' => [
				5727 => 'Single Sided',
				5726 => 'Double Sided'
			],
			'clicks' => [
				5705 => 'Black & White',
				5706 => 'Black & White',
				5711 => 'Colour',
				5712 => 'Colour',
				5719 => 'Colour',
				5720 => 'Black & White',
				5735 => 'Black & White',
				5736 => 'Black & White',
				5737 => 'Black & White',
				5738 => 'Colour',
				5739 => 'Colour',
				5740 => 'Colour'
			],
			'drillingTypes' => [
				5789 => 'No Punching',
				5852 => '2-Hole Punching',
				5856 => '4-Hole Punching'
			]
		];

		$this->assertEquals($result, $expected);
	}

	private $testCheckoutOrders = [
		'new_user' => [
			'Order' => [
				'user_id' => null,
				'accept_terms' => 1,
				'keep_me_up_to_date' => 0,
				'source_id' => 8,
				'billing_same_as_shipping' => 1,
				'delivery_option_id' => '00',
				'turnaround_option_id' => 3
			],
			'User' => [
				'id' => null,
				'first_name' => 'New',
				'last_name' => 'User',
				'username' => 'new_user@doxdirect.com'
			],
			'ShippingAddress' => [
				'id' => '',
				'house_number' => '31',
				'address_1' => 'Dover Road',
				'address_2' => '',
				'city' => 'Wanstead',
				'county' => 'London',
				'post_code' => 'E12 5DZ',
				'country_id' => 'United Kingdom'
			],
			'BillingAddress' => [
				'id' => '',
				'house_number' => '',
				'address_1' => '',
				'address_2' => '',
				'city' => '',
				'county' => '',
				'post_code' => '',
				'country_id' => ''
			]
		],
		'existing_user' => [
			'Order' => [
				'user_id' => 'user-2',
				'accept_terms' => 1,
				'keep_me_up_to_date' => 0,
				'source_id' => 8,
				'billing_same_as_shipping' => 1,
				'delivery_option_id' => '00',
				'turnaround_option_id' => 3
			],
			'User' => [
				'id' => 'user-2',
				'first_name' => 'Jeremy',
				'last_name' => 'Burns',
				'username' => 'jeremyburns@classoutfit.com'
			],
			'ShippingAddress' => [
				'id' => '',
				'house_number' => '31',
				'address_1' => 'Dover Road',
				'address_2' => '',
				'city' => 'Wanstead',
				'county' => 'London',
				'post_code' => 'E12 5DZ',
				'country_id' => 'United Kingdom'
			],
			'BillingAddress' => [
				'id' => '',
				'house_number' => '',
				'address_1' => '',
				'address_2' => '',
				'city' => '',
				'county' => '',
				'post_code' => '',
				'country_id' => ''
			]
		]
	];

	private function createPromoCode($data) {

		$promoCode = $this->Order->PromoCode->findByCode('test');

		if ($promoCode) {
			$this->Order->PromoCode->id = $promoCode['PromoCode']['id'];
		} else {
			$this->Order->PromoCode->create();
		}

		$promoCode = $this->Order->PromoCode->save($data);

		return $promoCode;

	}

	public function testCheckoutTermsNotAccepted() {

		$order = $this->prepareCheckoutOrder('existing_user');

		$order['Order']['accept_terms'] = false;

		$result = $this->Order->checkout($order);

		$expected = [
			'success' => false,
			'code' => 'failed-validation',
			'message' => 'Please address the following issues.',
			'errors' => [
				'accept_terms' => [
					0 => 'Please indicate that you accept the terms and conditions.'
				]
			]
		];

		$this->assertEquals($result, $expected);
	}

	public function testCheckoutOKNewUser() {

		$order = $this->prepareCheckoutOrder('new_user');

		$result = $this->Order->checkout($order);

		$this->assertTrue($result['success']);
	}

	public function testCheckoutOKExistingUser() {

		$order = $this->prepareCheckoutOrder('existing_user');

		$result = $this->Order->checkout($order);

		$this->assertTrue($result['success']);
	}

	public function testCheckoutOKWithPromoCodeInvalid() {

		$order = $this->prepareCheckoutOrder('new_user');

		$order['PromoCode']['code'] = 'invalid';

		$result = $this->Order->checkout($order);

		$expected = [
			'success' => false,
			'code' => 'invalid-promo-code',
			'message' => 'That is an invalid promo code',
			'flash_key' => 'promo-code'
		];

		$this->assertEqual($result, $expected);

	}

	public function testCheckoutOKWithPromoCodeValidThatFails() {

		$order = $this->prepareCheckoutOrder('new_user');

		$promoCode = $this->createPromoCode([
			'name' => 'Test',
			'code' => 'test',
			'description' => 'Test',
			'minimum_spend' => 100,
			'discount_type' => 'percent',
			'discount_amount' => 10,
			'apply_discount_to' => 'amount_net',
			'is_active' => true,
			'user_id' => null
		]);

		$promoCodeId = $promoCode['PromoCode']['id'];

		$order['PromoCode']['code'] = 'test';

		$result = $this->Order->checkout($order);

		$expected = [
			'success' => false,
			'code' => 'below-minimum-spend',
			'message' => 'You cannot use that promo code as your order is below the minimum spend (£100.00).'
		];

		$this->assertEqual($result, $expected);

	}

	public function testCheckoutOKWithPromoCodeThatPassesValid() {

		$order = $this->prepareCheckoutOrder('new_user');

		$promoCode = $this->createPromoCode([
			'name' => 'Test',
			'code' => 'test',
			'description' => 'Test',
			'minimum_spend' => 0,
			'discount_type' => 'percent',
			'discount_amount' => 10,
			'apply_discount_to' => 'amount_net',
			'is_active' => true,
			'user_id' => null
		]);

		$promoCodeId = $promoCode['PromoCode']['id'];

		$order['PromoCode']['code'] = 'test';

		$result = $this->Order->checkout($order);

		$this->assertTrue($result['success']);

		$this->assertEqual($result['order']['Order']['amount_discount'], 0.01);
		$this->assertEqual($result['order']['Order']['promo_code_id'], $promoCodeId);

	}

	private function prepareCheckoutOrder($type = 'new_user') {

		$order = $this->Order->defaultQuote();

		$order = $this->Order->quote($order);

		$orderId = $order['Order']['id'];

		$order = $this->Order->getOrder(
			$orderId,
			[
				'includeOrderContains' => false,
				'includeOrderItemContains' => false
			]
		);

		$checkoutOrder = $this->testCheckoutOrders[$type];

		$order = $this->mergeCheckoutOrder($order, $checkoutOrder);

		return $order;

	}

	private function mergeCheckoutOrder($order, $checkoutOrder) {

		foreach ($checkoutOrder as $orderKey => $orderKeyData) {

			if (!isset($order[$orderKey])) {
				$order[$orderKey] = $checkoutOrder[$orderKey];
			} else {
				$order[$orderKey] = array_merge(
					$order[$orderKey],
					$checkoutOrder[$orderKey]
				);
			}
		}

		return $order;

	}

	public function testCheckoutWithChangeTurnaroundOption() {

		$order = $this->prepareCheckoutOrder('new_user');

		$order['Order']['turnaround_option_id'] = 2;

		$result = $this->Order->checkout($order);

		$this->assertEqual($result['order']['Order']['turnaround_option_id'], 2);
		$this->assertEqual($result['order']['Order']['amount_turnaround'], 0.02);

	}

	public function testReceivePaymentPart() {

		$order = $this->prepareCheckoutOrder('new_user');

		$orderId = $order['Order']['id'];

		$result = $this->Order->receivePayment($orderId, 2.00);

		$expected = [
			'success' => true,
			'amount_due' => 1.34
		];

		$this->assertEquals($result, $expected);
	}

	public function testReceivePaymentFull() {

		$order = $this->prepareCheckoutOrder('new_user');

		$orderId = $order['Order']['id'];

		$result = $this->Order->receivePayment($orderId, 3.34);

		$expected = [
			'success' => true,
			'amount_due' => 0
		];

		$this->assertEquals($result, $expected);
	}

	public function testReceivePaymentOver() {

		$order = $this->prepareCheckoutOrder('new_user');

		$orderId = $order['Order']['id'];

		$result = $this->Order->receivePayment($orderId, 4.00);

		$expected = [
			'success' => true,
			'amount_due' => -0.66
		];

		$this->assertEquals($result, $expected);
	}

	public function testRequoteChangePageCount() {

		$data = $this->prepareSampleOrder('single_unbound');

		$result = $this->Order->quote($data);

		$orderId = $result['Order']['id'];
		$orderItemId = $result['OrderItem'][0]['id'];
		$amountNet = $result['Order']['amount_net'];

		$this->Order->OrderItem->id = $orderItemId;
		$this->Order->OrderItem->set('pages', 100);
		$this->Order->OrderItem->save();

		$result = $this->Order->requote($orderId, $orderItemId);

		$revisedAmountNet = $result['Order']['amount_net'];

		$this->assertEquals($amountNet, 0.30);
		$this->assertEquals($revisedAmountNet, 30.45);
	}

	public function testRequoteChangeOrderOptions() {

		$data = $this->prepareSampleOrder('single_unbound');

		$result = $this->Order->quote($data);

		$orderId = $result['Order']['id'];
		$orderItemId = $result['OrderItem'][0]['id'];
		$amountTurnaround = $result['Order']['amount_turnaround'];
		$amountTotal = $result['Order']['amount_total'];

		debug('Turnaround: ' . $amountTurnaround);
		debug('Total: ' . $amountTotal);

		$this->Order->id = $orderId;
		$this->Order->set('turnaround_option_id', 1);
		$this->Order->save();

		$result = $this->Order->requote($orderId, $orderItemId);

		$revisedAmountTurnaround = $result['Order']['amount_turnaround'];
		$revisedAmountTotal = $result['Order']['amount_total'];

		debug('Turnaround: ' . $revisedAmountTurnaround);
		debug('Total: ' . $revisedAmountTotal);

		$this->assertEquals($amountTurnaround, 0.00);
		$this->assertEquals($amountTotal, 3.55);
		$this->assertEquals($revisedAmountTurnaround, 0.15);
		$this->assertEquals($revisedAmountTotal, 3.70);

	}

	/**
	 *
	 */
	public function testGetOrderFixFailuresSingle() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_unbound',
					'order_item_file' => [
						0 => [
							'type' => 'a4_portrait_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_portrait',
									'quantity' => 3
								],
								1 => [
									'type' => 'a4_landscape',
									'quantity' => 3
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);

		$orderId = $order['Order']['id'];

		$expectedKey = 'checkMixedOrientation';

		$elements = [
			'descriptionCode' => 'rotate',
			'options' => [
				0 => 'rotate_to_landscape',
				1 => 'rotate_to_portrait'
			]
		];

		$failures = $this->Order->getOrderFixFailures($orderId);

		$returnedElements = $failures['failure']['elements'];

		$returnedElementOptions = Hash::extract($returnedElements['options'], '{n}.name');


		$this->assertArrayHasKey('failure', $failures);

		$this->assertEquals($failures['failure']['checkName'], $expectedKey);

		$this->assertEquals($returnedElements['descriptionCode'], $elements['descriptionCode']);

		$this->assertEquals($returnedElementOptions, $elements['options']);

	}

	public function testGetOrderFixFailuresBelowPageLimits() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_hardback',
					'order_item_file' => [
						0 => [
							'type' => 'a4_portrait_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_portrait',
									'quantity' => 3
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);

		$orderId = $order['Order']['id'];

		$expectedKey = 'checkPageCounts';

		$failures = $this->Order->getOrderFixFailures($orderId);

		$this->assertArrayHasKey('failure', $failures);
// Need to revisit this
//		$this->assertEquals($failures['failure']['checkName'], 'checkPageCounts');
//
//		$this->assertEquals($failures['failure']['action'], 'upload');

	}

  /**
   * This test passes in a portrait wirebound document before it's bindging edge has been set.
   * It must have a long edge, so this tests that the code sets it to long
   */
	public function testGetOrderFixFailuresWireboundPortraitNoBindingEdge() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_wirebound',
					'order_item_file' => [
						0 => [
							'type' => 'a4_portrait_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_portrait',
									'quantity' => 30
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);

		$orderId = $order['Order']['id'];

		$failures = $this->Order->getOrderFixFailures($orderId);

		$this->assertArrayHasKey('failure', $failures);


		$this->assertEquals($failures['failure']['checkName'], 'checkBindingSides');

		$this->assertEquals($failures['failure']['elements']['descriptionCode'], 'set_binding_side');

		$returnedOptions = Hash::extract($failures['failure']['elements']['options'], '{n}.name');

		$expectedOptions = [
			0 => 'set_binding_side_left',
			1 => 'set_binding_side_right'
		];

		$this->assertEquals($returnedOptions, $expectedOptions);

	}

   /**
   * This test passes in a portrait wirebound document where the short binding edge has been set.
   * It must have a long edge, so this tests that the code sets it to long
   */
	public function testGetOrderFixFailuresWireboundPortraitShortBindingEdge() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_wirebound',
					'order_item_file' => [
						0 => [
							'type' => 'a4_portrait_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_portrait',
									'quantity' => 30
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);
		debug($order);
		$orderId = $order['Order']['id'];

		$orderItemId = $order['OrderItem'][0]['id'];

		$this->Order->OrderItem->setBindingEdgeTypeId($orderItemId, 'long');

		$failures = $this->Order->getOrderFixFailures($orderId);
		die(debug($failures));
		$this->assertArrayHasKey('failure', $failures);

		$this->assertEquals($failures['failure']['checkName'], 'checkPaperSizeMatchASize');

		$this->assertEquals($failures['failure']['elements']['descriptionCode'], 'page_size_mismatch');

		$returnedOptions = Hash::extract($failures['failure']['elements']['options'], '{n}.name');

		$expectedOptions = [
			0 => 'scale_up_to_paper_size',
			1 => 'add_crop_marks_and_trim'
		];

		$this->assertEquals($returnedOptions, $expectedOptions);


//		$this->assertEquals($failures['failure']['checkName'], 'checkBindingSides');
//
//		$elements = [
//			'descriptionCode' => 'set_binding_side',
//			'options' => [
//				0 => [
//					'heading' => 'Bind on the left edge',
//					'message' => 'Bind on the left edge (typically for left to right reading)',
//					'image' => 'pages/portrait_binding_side_left.png',
//					'actions' => [
//						0 => [
//							'label' => 'Select',
//							'fix_code' => 'fixSetBindingSide',
//							'data' => [
//								'binding_side' => 'left'
//							]
//						]
//					],
//					'name' => 'set_binding_side_left'
//				],
//				1 => [
//					'heading' => 'Bind on the right side',
//					'message' => 'Bind on the right side (typically for right to left reading)',
//					'image' => 'pages/portrait_binding_side_right.png',
//					'actions' => [
//						0 => [
//							'label' => 'Select',
//							'fix_code' => 'fixSetBindingSide',
//							'data' => [
//								'binding_side' => 'right'
//							]
//						]
//					],
//					'name' => 'set_binding_side_right'
//				]
//			]
//		];
//
//		$this->assertEquals($failures['failure']['elements'], $elements);
//
//		$order = $this->Order->getOrder($orderId);
//
//		$this->assertEquals(strtolower($order['OrderItem'][0]['BindingEdgeType']['pace_name']), 'long edge');

	}

/**
 * This test passes in a landscape wirebound document before it's bindging edge has been set.
 * It tests that the user is offered a choice of long or short edge binding.
 */
	public function testGetOrderFixFailuresWireboundLandscapeNoBindingEdge() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_wirebound',
					'order_item_file' => [
						0 => [
							'type' => 'a4_landscape_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_landscape',
									'quantity' => 30
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);

		$orderId = $order['Order']['id'];

		$failures = $this->Order->getOrderFixFailures($orderId);
		die(debug($failures));
		$this->assertArrayHasKey('failure', $failures);

		$this->assertEquals($failures['failure']['checkName'], 'checkBindingEdges');

		$elements = [
			'description' => 'wirebound_landscape_set_binding_edge',
			'options' => [
				0 => 'wirebound_landscape_set_binding_edge_long',
				1 => 'wirebound_landscape_set_binding_edge_short'
			]
		];

		$this->assertEquals($failures['failure']['elements'], $elements);

	}

/**
 * This test passes in a landscape wirebound document where the user has chosen a long binding edge.
 * It tests that the user is then asked to choose head/head or head/foot binding sides.
 */
	public function testGetOrderFixFailuresWireboundLandscapeLong() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_wirebound',
					'order_item_file' => [
						0 => [
							'type' => 'a4_landscape_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_landscape',
									'quantity' => 30
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);

		$orderId = $order['Order']['id'];
		$orderItemId = $order['OrderItem'][0]['id'];

		$this->Order->OrderItem->setBindingEdgeTypeId($orderItemId, 'long');

		$failures = $this->Order->getOrderFixFailures($orderId);

		$this->assertArrayHasKey('failure', $failures);

		$this->assertEquals($failures['failure']['checkName'], 'checkBindingSides');

		$this->assertEquals($failures['failure']['elements']['descriptionCode'], 'set_binding_side');

		$returnedOptions = Hash::extract($failures['failure']['elements']['options'], '{n}.name');

		$expectedOptions = [
			0 => 'set_binding_side_head_to_head',
			1 => 'set_binding_side_head_to_foot'
		];

		$this->assertEquals($returnedOptions, $expectedOptions);

	}

	/**
	 * This test passes in a landscape wirebound document where the user has chosen a short binding edge.
	 * It tests that the user is then asked to choose left or right binding sides.
	 */
	public function testGetOrderFixFailuresWireboundLandscapeShort() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_wirebound',
					'order_item_file' => [
						0 => [
							'type' => 'a4_landscape_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_landscape',
									'quantity' => 30
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);

		$orderId = $order['Order']['id'];
		$orderItemId = $order['OrderItem'][0]['id'];

		$this->Order->OrderItem->setBindingEdgeTypeId($orderItemId, 'short');

		$failures = $this->Order->getOrderFixFailures($orderId);

		$this->assertArrayHasKey('failure', $failures);

		$this->assertEquals($failures['failure']['checkName'], 'checkBindingSides');

		$this->assertEquals($failures['failure']['elements']['descriptionCode'], 'set_binding_side');

		$returnedOptions = Hash::extract($failures['failure']['elements']['options'], '{n}.name');

		$expectedOptions = [
			0 => 'set_binding_side_left',
			1 => 'set_binding_side_right'
		];

		$this->assertEquals($returnedOptions, $expectedOptions);

	}

	public function testGetOrderFixFailuresHasTrimMarks() {

		$orderId = "order-8";

		$expectedKey = 'checkHasTrimMarks';

		$failures = $this->Order->getOrderFixFailures($orderId);

		$this->assertArrayHasKey('failure', $failures);

		$this->assertEquals($failures['failure']['checkName'], 'checkHasTrimMarks');

		$this->assertEquals($failures['failure']['elements']['descriptionCode'], 'has_trim_marks');

		$returnedOptions = Hash::extract($failures['failure']['elements']['options'], '{n}.name');

		$expectedOptions = [
			0 => 'print_on_sra',
			1 => 'print_as_is'
		];

		$this->assertEquals($returnedOptions, $expectedOptions);

	}

	/**
	 *
	 */
	public function testGetOrderFixFailuresMultipleMixedOrientation() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_unbound',
					'order_item_file' => [
						0 => [
							'type' => 'a4_portrait_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_portrait',
									'quantity' => 3
								],
								1 => [
									'type' => 'a4_landscape',
									'quantity' => 3
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);

		$orderId = $order['Order']['id'];

		$expectedKey = 'checkMixedOrientation';

		$failures = $this->Order->getOrderFixFailures($orderId);

		$this->assertArrayHasKey('failure', $failures);

		$this->assertEquals($failures['failure']['checkName'], 'checkMixedOrientation');

		$this->assertEquals($failures['failure']['elements']['descriptionCode'], 'rotate');

		$returnedOptions = Hash::extract($failures['failure']['elements']['options'], '{n}.name');

		$expectedOptions = [
			0 => 'rotate_to_landscape',
			1 => 'rotate_to_portrait'
		];

		$this->assertEquals($returnedOptions, $expectedOptions);

		$orderItemId = $this->Order->OrderItem->field(
			'id',
			['OrderItem.order_id' => $orderId]
		);

		$fixOptions = [
			'rotate_to' => 'portrait',
			'direction' => 'clockwise'
		];

		$result = $this->Order->OrderItem->applyFix('fixRotatePages', $orderItemId, $failures, $fixOptions);

		$failures = $this->Order->getOrderFixFailures($orderId);

		$this->assertArrayHasKey('failure', $failures);


		$this->assertEquals($failures['failure']['checkName'], 'checkPaperSizeMatchASize');

		$this->assertEquals($failures['failure']['elements']['descriptionCode'], 'page_size_mismatch');

		$returnedOptions = Hash::extract($failures['failure']['elements']['options'], '{n}.name');

		$expectedOptions = [
			0 => 'scale_up_to_paper_size',
			1 => 'add_crop_marks_and_trim'
		];

		$this->assertEquals($returnedOptions, $expectedOptions);

	}

	public function testGetOrderFixFailuresMixedPaperSize() {

		$testCase = [
			'order_item' => [
				0 => [
					'type' => 'single_unbound',
					'order_item_file' => [
						0 => [
							'type' => 'a4_portrait_pdf',
							'order_item_file_page' => [
								0 => [
									'type' => 'a4_portrait',
									'quantity' => 1
								],
								1 => [
									'type' => 'a5_portrait',
									'quantity' => 1
								]
							]
						]
					]
				]
			]
		];

		// Start with a new simple quote
		$order = $this->prepareFullOrder($testCase);

		$orderId = $order['Order']['id'];

		$order = $this->Order->getOrder($orderId);

		$orderItemId = $this->Order->OrderItem->field(
			'id',
			['OrderItem.order_id' => $orderId]
		);

		$result = $this->Order->OrderItem->collatePages($orderItemId);

		$order = $this->Order->getOrder($orderId);

		$failures = $this->Order->getOrderFixFailures($orderId);

		$orderItemId = $this->Order->OrderItem->field(
			'id',
			['OrderItem.order_id' => $orderId]
		);

		$result = $this->Order->OrderItem->fixScaleToCutSheet($orderItemId);


//		$result = $this->Order->OrderItem->fixChangePaperUp($orderItemId);

		$result = $this->Order->getOrderFixFailures($orderId);

// This test will fail until I come up with a way of faking the Callas info file.
		$expected = [
			'success' => false,
			'code' => 'no-failures'
		];

		$this->assertArrayHasKey('success', $result);

	}

	public function testCreateCustomerWithJustBillingAddress() {

		$user = [
			'User' => [
				'username' => 'jeremyb@doxdirect.com',
				'first_name' => 'Jeremy',
				'last_name' => 'Burns'
			],
			'BillingAddress' => [
				'company_name' => 'No company',
				'address_1' => '14 Greville Street',
				'address_2' => 'Hatton Garden',
				'address_3' => 'None',
				'city' => 'London',
				'county' => 'London',
				'post_code' => 'EC1N 8SB',
				'country_id' => 76,
				'billing_same_as_shipping' => 1
			]
		];

		$result = $this->Order->createCustomer($user);

		$this->assertTrue($result['success']);

		$this->assertEquals($result['response']['status'], 1);

		$this->assertArrayHasKey('customer', $result['response']);

		$this->assertArrayHasKey('customer_id', $result['response']['customer']);

		$this->assertArrayHasKey('billing_contact_id', $result['response']['customer']);

		$this->assertArrayHasKey('shipping_contact_id', $result['response']['customer']);

	}

	public function testCreateCustomerWithBillingAndShippingAddress() {

		$user = [
			'User' => [
				'username' => 'jeremyb@doxdirect.com',
				'first_name' => 'Jeremy',
				'last_name' => 'Burns'
			],
			'BillingAddress' => [
				'company_name' => 'No company',
				'address_1' => '14 Greville Street',
				'address_2' => 'Hatton Garden',
				'address_3' => 'None',
				'city' => 'London',
				'county' => 'London',
				'post_code' => 'EC1N 8SB',
				'country_id' => 76,
				'billing_same_as_shipping' => 0
			],
			'ShippingAddress' => [
				'company_name' => '',
				'address_1' => '1 The High Street',
				'address_2' => 'Small Town',
				'address_3' => '',
				'city' => 'London',
				'county' => 'London',
				'post_code' => 'EC1N 8SB',
				'country_id' => 76
			]
		];

		$result = $this->Order->createCustomer($user);

		$this->assertTrue($result['success']);

		$this->assertEquals($result['response']['status'], 1);

		$this->assertArrayHasKey('customer', $result['response']);

		$this->assertArrayHasKey('customer_id', $result['response']['customer']);

		$this->assertArrayHasKey('billing_contact_id', $result['response']['customer']);

		$this->assertArrayHasKey('shipping_contact_id', $result['response']['customer']);

	}

	public function testConvertPaceQuoteToJob() {

		$order = $this->prepareCheckoutOrder('new_user');

		$order['Order']['billing_same_as_shipping'] = false;

		$this->Order->save($order['Order']);

		$user = [
			'User' => [
				'password' => 'password',
				'id' => '',
				'first_name' => 'First',
				'last_name' => 'Name',
				'username' => 'user@random.com',
				'phone' => '1234567890'
			]
		];

		$user = $this->Order->User->register($user);

		$userId = $user['User']['id'];

		$deliveryAddress = [
			'ShippingAddress' => [
				'user_id'=> $user['User']['id'],
				'address_1' => '1 The High Street',
				'address_2' => 'Smallcity',
				'address_3' => '',
				'city' => 'London',
				'county' => 'London',
				'post_code' => 'EC1N 8SB',
				'country_id' => 76
			]
		];

		$deliveryAddress = $this->Order->ShippingAddress->add($deliveryAddress, true);

		$deliveryAddressId = $deliveryAddress['userAddress']['ShippingAddress']['id'];

		$billingAddress = [
			'BillingAddress' => [
				'user_id'=> $user['User']['id'],
				'company_name' => 'Page Visions',
				'address_1' => '14 Greville Street',
				'address_2' => 'Hatton Garden',
				'city' => 'London',
				'county' => 'London',
				'post_code' => 'TEST PC',
				'country_id' => 76
			]
		];

		$billingAddress = $this->Order->BillingAddress->add($billingAddress, true);

		$billingAddressId = $billingAddress['userAddress']['BillingAddress']['id'];

		$order['Order'] = array_merge(
			$order['Order'],
			[
				'user_id' => $userId,
				'shipping_address_id' => $deliveryAddressId,
				'billing_address_id' => $billingAddressId
			]
		);

		$this->Order->set($order);
		$order = $this->Order->save();

		$orderId = $order['Order']['id'];

		$this->Order->convertPaceQuoteToJob($orderId);

		$paceJobNumber = $this->Order->field(
			'pace_job_number',
			['Order.id' => $orderId]
		);

		$this->assertNotEmpty($paceJobNumber);

	}

}
