<?php
App::uses('OrderStatus', 'Model');

/**
 * OrderStatus Test Case
 *
 */
class OrderStatusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.order_status',
		'app.order_date',
		'app.order'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OrderStatus = ClassRegistry::init('OrderStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OrderStatus);

		parent::tearDown();
	}

	public function testTabs() {

		$result = $this->OrderStatus->steps();

		$expected = [
			'upload' => [
				'id' => 'upload',
				'icon' => 'fa-cloud-upload',
				'name' => 'Upload',
				'description' => 'Upload your document',
				'link' => '/upload',
				'class' => 'step active'
			],
			'process_files' => [
				'id' => 'process_files',
				'icon' => 'fa fa-cog',
				'name' => 'Process file',
				'description' => 'Process your uploaded files',
				'link' => '/process-files',
				'class' => 'step'
			],
			'configure' => [
				'id' => 'configure',
				'icon' => 'fa-list-ul',
				'name' => 'Configure',
				'description' => 'Choose your options',
				'link' => '/configure',
				'class' => 'step'
			],
			'checkout' => [
				'id' => 'checkout',
				'icon' => 'fa-shopping-cart',
				'name' => 'Checkout',
				'description' => 'Choose your payment method',
				'link' => '/checkout',
				'class' => 'step'
			],
			'complete' => [
				'id' => 'complete',
				'icon' => 'fa-truck fa-flip-horizontal',
				'name' => 'Complete',
				'description' => 'Confirmation of your order details',
				'link' => '/complete',
				'class' => 'step'
			]
		];

		$this->assertEqual($result, $expected);

	}

}