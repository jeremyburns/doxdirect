<?php
/**
 * MatrixProductMediaFixture
 *
 */
class MatrixProductMediaFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'MatrixProductMedia', 'records' => true);

}
