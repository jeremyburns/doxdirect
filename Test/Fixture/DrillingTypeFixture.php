<?php
/**
 * DrillingTypeFixture
 *
 */
class DrillingTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'DrillingType', 'records' => true);

}
