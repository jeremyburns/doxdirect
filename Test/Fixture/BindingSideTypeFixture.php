<?php
/**
 * BindingSideTypeFixture
 *
 */
class BindingSideTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'BindingSideType', 'records' => true);

}
