<?php
/**
 * ProductMapCategoryFixture
 *
 */
class ProductMapCategoryFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductMapCategory', 'records' => true);

}
