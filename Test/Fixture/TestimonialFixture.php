<?php
/**
 * TestimonialFixture
 *
 */
class TestimonialFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Testimonial', 'records' => true);

}
