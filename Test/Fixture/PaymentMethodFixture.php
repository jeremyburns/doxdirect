<?php
/**
 * PaymentMethodFixture
 *
 */
class PaymentMethodFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'PaymentMethod', 'records' => true);

}
