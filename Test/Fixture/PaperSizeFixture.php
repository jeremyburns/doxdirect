<?php
/**
 * PaperSizeFixture
 *
 */
class PaperSizeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'PaperSize', 'records' => true);

}
