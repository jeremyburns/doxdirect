<?php
/**
 * HolidayFixture
 *
 */
class HolidayFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Holiday', 'records' => true);

}
