<?php
/**
 * UserFixture
 *
 */
class UserFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'User');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 'user-1',
			'username' => 'jeremyburns@me.com',
			'pace_customer_id' => null,
			'password' => '210caed9ea7ef89d74baccf2ee1c4a9a16559a7a',
			'password_reset_code' => null,
			'password_reset_code_expires' => null,
			'failed_login_attempts' => '0',
			'role' => 'admin',
			'salutation' => null,
			'first_name' => 'Jeremy',
			'last_name' => 'Burns',
			'full_name' => 'Jeremy Burns',
			'phone' => null,
			'count_user_address' => '3',
			'count_order' => '0',
			'created' => '2015-05-02 10:56:38',
			'modified' => '2015-05-06 17:00:30'
		),
		array(
			'id' => 'user-2',
			'username' => 'jeremyburns@classoutfit.com',
			'pace_customer_id' => null,
			'password' => '216ad32f381edd8195e7fcefa645d6df2385a90b',
			'password_reset_code' => null,
			'password_reset_code_expires' => null,
			'failed_login_attempts' => '0',
			'role' => 'user',
			'salutation' => null,
			'first_name' => 'Jeremy',
			'last_name' => 'Burns',
			'full_name' => 'Jeremy Burns',
			'phone' => '7973481949',
			'count_user_address' => '0',
			'count_order' => '0',
			'created' => '2015-05-07 12:11:26',
			'modified' => '2015-05-07 12:11:26'
		),
		array(
			'id' => 'guest',
			'username' => 'guest',
			'pace_customer_id' => null,
			'password' => 'password',
			'password_reset_code' => null,
			'password_reset_code_expires' => null,
			'failed_login_attempts' => '0',
			'role' => 'guest',
			'salutation' => null,
			'first_name' => 'Guest',
			'last_name' => 'User',
			'full_name' => 'Guest User',
			'phone' => null,
			'count_user_address' => '1',
			'count_order' => '0',
			'created' => null,
			'modified' => null
		),
	);

}
