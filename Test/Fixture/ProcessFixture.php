<?php
/**
 * ProcessFixture
 *
 */
class ProcessFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Process', 'records' => true);

}
