<?php
/**
 * BindingFixture
 *
 */
class BindingFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Binding', 'records' => true);

}
