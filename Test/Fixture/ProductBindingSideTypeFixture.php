<?php
/**
 * ProductBindingSideTypeFixture
 *
 */
class ProductBindingSideTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductBindingSideType', 'records' => true);

}
