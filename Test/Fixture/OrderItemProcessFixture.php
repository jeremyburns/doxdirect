<?php
/**
 * OrderItemProcessFixture
 *
 */
class OrderItemProcessFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'OrderItemProcess');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
	);

}
