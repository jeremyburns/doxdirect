<?php
/**
 * OrderDateFixture
 *
 */
class OrderDateFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'OrderDate');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 'order-date-id-1',
			'order_id' => 'order-1',
			'order_status_id' => 'upload',
			'created' => '2015-04-21 12:27:43'
		),
	);

}
