<?php
/**
 * ProductCoverTypeFixture
 *
 */
class ProductCoverTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductCoverType', 'records' => true);

}
