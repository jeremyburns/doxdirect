<?php
/**
 * ProductDrillingTypeFixture
 *
 */
class ProductDrillingTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductDrillingType', 'records' => true);

}
