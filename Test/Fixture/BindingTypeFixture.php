<?php
/**
 * BindingTypeFixture
 *
 */
class BindingTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'BindingType', 'records' => true);

}
