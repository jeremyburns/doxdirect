<?php
/**
 * PaypalPaymentFixture
 *
 */
class PaypalPaymentFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'PaypalPayment', 'records' => true);

}
