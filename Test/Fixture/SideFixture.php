<?php
/**
 * SideFixture
 *
 */
class SideFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Side', 'records' => true);

}
