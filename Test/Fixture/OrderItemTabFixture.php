<?php
/**
 * OrderItemTabFixture
 *
 */
class OrderItemTabFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'OrderItemTab', 'records' => true);

}
