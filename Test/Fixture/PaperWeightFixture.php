<?php
/**
 * PaperWeightFixture
 *
 */
class PaperWeightFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'PaperWeight', 'records' => true);

}
