<?php
/**
 * DeliveryOptionFixture
 *
 */
class DeliveryOptionFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'DeliveryOption', 'records' => true);

}
