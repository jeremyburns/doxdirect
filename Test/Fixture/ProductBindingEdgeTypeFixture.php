<?php
/**
 * ProductBindingEdgeTypeFixture
 *
 */
class ProductBindingEdgeTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductBindingEdgeType', 'records' => true);

}
