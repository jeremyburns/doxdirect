<?php
/**
 * TurnaroundOptionFixture
 *
 */
class TurnaroundOptionFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'TurnaroundOption', 'records' => true);

}
