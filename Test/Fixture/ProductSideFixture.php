<?php
/**
 * ProductSideFixture
 *
 */
class ProductSideFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductSide', 'records' => true);

}
