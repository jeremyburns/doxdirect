<?php
/**
 * ProductMapCategoryExtraFieldFixture
 *
 */
class ProductMapCategoryExtraFieldFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductMapCategoryExtraField', 'records' => true);

}
