<?php
/**
 * CurrencyFixture
 *
 */
class CurrencyFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Currency', 'records' => true);

}
