<?php
/**
 * OrderStatusFixture
 *
 */
class OrderStatusFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'OrderStatus', 'records' => true);

}
