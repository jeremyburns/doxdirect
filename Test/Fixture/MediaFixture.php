<?php
/**
 * MediaFixture
 *
 */
class MediaFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Media', 'records' => true);

}
