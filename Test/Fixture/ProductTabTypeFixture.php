<?php
/**
 * ProductTabTypeFixture
 *
 */
class ProductTabTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductTabType', 'records' => true);

}
