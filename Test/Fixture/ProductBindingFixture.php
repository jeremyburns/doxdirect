<?php
/**
 * ProductBindingFixture
 *
 */
class ProductBindingFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductBinding', 'records' => true);

}
