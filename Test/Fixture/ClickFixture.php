<?php
/**
 * ClickFixture
 *
 */
class ClickFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Click', 'records' => true);

}
