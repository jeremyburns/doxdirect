<?php
/**
 * ConfigurationFixture
 *
 */
class ConfigurationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Configuration', 'records' => true);

}
