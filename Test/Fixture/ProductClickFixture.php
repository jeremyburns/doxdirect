<?php
/**
 * ProductClickFixture
 *
 */
class ProductClickFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductClick', 'records' => true);

}
