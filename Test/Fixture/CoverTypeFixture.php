<?php
/**
 * CoverTypeFixture
 *
 */
class CoverTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'CoverType', 'records' => true);

}
