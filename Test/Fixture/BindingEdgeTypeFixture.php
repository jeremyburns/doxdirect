<?php
/**
 * BindingEdgeTypeFixture
 *
 */
class BindingEdgeTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'BindingEdgeType', 'records' => true);

}
