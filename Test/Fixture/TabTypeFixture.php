<?php
/**
 * TabTypeFixture
 *
 */
class TabTypeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'TabType', 'records' => true);

}
