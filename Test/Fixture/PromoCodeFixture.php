<?php
/**
 * PromoCodeFixture
 *
 */
class PromoCodeFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'PromoCode', 'records' => true);

}
