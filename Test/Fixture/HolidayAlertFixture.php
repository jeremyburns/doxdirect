<?php
/**
 * HolidayAlertFixture
 *
 */
class HolidayAlertFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'HolidayAlert', 'records' => true);

}
