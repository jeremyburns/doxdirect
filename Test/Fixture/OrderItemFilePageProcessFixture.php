<?php
/**
 * OrderItemFilePageProcessFixture
 *
 */
class OrderItemFilePageProcessFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'OrderItemFilePageProcess');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
	);

}
