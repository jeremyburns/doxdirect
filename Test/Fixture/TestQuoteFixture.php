<?php
/**
 * TestQuoteFixture
 *
 */
class TestQuoteFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'TestQuote', 'records' => true);

}
