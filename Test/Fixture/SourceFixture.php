<?php
/**
 * SourceFixture
 *
 */
class SourceFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Source', 'records' => true);

}
