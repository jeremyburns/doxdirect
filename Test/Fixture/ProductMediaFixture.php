<?php
/**
 * ProductMediaFixture
 *
 */
class ProductMediaFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ProductMedia', 'records' => true);

}
