<?php
App::uses('ModelBehavior', 'Model');

/**
 * Behavior to manage defaults for certain models.
 */
class DefaultableBehavior extends ModelBehavior {

	public function setup(Model $Model, $settings = array()) {

		$this->modelAlias = $Model->alias;

		if (!isset($this->settings[$this->modelAlias])) {
			$this->settings[$this->modelAlias] = array(
				'recursive' => true,
				'notices' => true,
				'autoFields' => true
			);
		}
		$this->settings[$this->modelAlias] = array_merge($this->settings[$this->modelAlias], $settings);
	}

/**
 * Set the default for a model
 *
 * @param array $id - the id to set as the default
 * @return boolean
 */
	public function setDefault(Model $Model, $newDefaultId = null) {

		if (!$newDefaultId) {
			return false;
		}

		$record = $Model->findById($newDefaultId);

		if ($record) {

			if ($this->unsetDefault($Model)) {

				$data[$this->modelAlias] = array(
					'id' => $newDefaultId,
					'is_default' => true
				);

				if ($Model->save($data)) {
					return true;
				}

			}

		}

		return false;

	}

/**
 * Get the default for a model
 *
 * @return $id
 */
	public function getDefault(Model $Model, $options = []) {

		$cacheKey = 'quote_defaults';

		$defaults = [
			'refresh' => false,
			'fieldName' => 'id'
		];

		$options = array_merge(
			$defaults,
			$options
		);

		if ($options['refresh']) {
			Cache::delete($Model->alias, $cacheKey);
		}

		return Cache::remember(
			$Model->alias,
			function() use ($Model, $options){

				$criteria[] = array($Model->alias . '.is_default' => true);

				if (isset($options['criteria'])) {
					$criteria[] = $options['criteria'];
				}

				return $Model->field(
					$options['fieldName'],
					$criteria
				);
			},
			$cacheKey
		);

	}

/**
 * Unset the current default
 *
 * @return boolean
 */
	public function unsetDefault(Model $Model) {

		$defaultId = $this->getDefault($Model);

		if (!$defaultId) {
			return true;
		}

		$data[$this->modelAlias] = array(
			'id' => $defaultId,
			'is_default' => false
		);

		if ($Model->save($data)) {
			return true;
		}

		return false;

	}


}
