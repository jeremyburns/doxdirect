<?php

App::uses('ModelBehavior', 'Model');

/**
 * Behavior to bring together all of the fix identifications for docs
 */
class OrderItemFixableBehavior extends ModelBehavior {

	/**
	 * @param Model $model
	 * @param array $settings
	 */
	public function setup(Model $model, $settings = []) {

		$this->OrderItem = $model;

	}

	public function needsRequote() {

		if ($currentPaperSizeId !== $filePaperSizeId) {
			// As the uploaded file paper size does not match the quoted product's size we need to change the product

			$productId = $orderItem['Product']['id'];
			$bindingTypeId = $orderItem['Product']['binding_type_id'];

			$newProductId = $this->Product->field(
				'id',
				[
					'Product.binding_type_id' => $bindingTypeId,
					'Product.paper_size_id' => $filePaperSizeId
				]
			);

			if ($newProductId) {
				$data['product_id'] = $newProductId;
			} else {

				// TODO waiting for the results of ticket #103
				//				return [
				//					'success' => false,
				//					'code' => 'no-product-for-paper-size',
				//					'message' => [
				//						'header' => 'We cannot print that file',
				//						'message' => 'We cannot print the file you have uploaded as the '
				//					]
				//				];
			}

		}

	}

	/**
	 * @param Model $model
	 * @param array $model
	 * @param $modelKey
	 * @return array
	 */
	public function getOrderItemFixFailures(Model $model, $orderItem = [], $orderItemKey = null) {

		if (!$orderItem) {
			return [
				'success' => false,
				'code' => 'no-order-item',
				'from' => 'OrderItemFixable.getOrderItemFixFailures'
			];
		}

		if ($orderItemKey === null) {
			return [
				'success' => false,
				'code' => 'no-order-item-key',
				'from' => 'OrderItemFixable.getOrderItemFixFailures'
			];
		}

		// Set some local variables
		$this->orderItem = $orderItem;
		$this->orderItemId = $this->orderItem['id'];
		$this->orderItemKey = $orderItemKey;

		// Move the id of the OrderItem model to the chosen id
		$this->OrderItem->id = $this->orderItemId;

		// Check it exists
		if (!$this->OrderItem->exists()) {

			// If not bounce it out
			return [
				'success' => false,
				'code' => 'invalid-order-item-id',
				'from' => 'OrderItemFixable.getOrderItemFixFailures'
			];

		}

		$failure = $this->doChecks();

		if (!$failure) {
			return [
				'success' => true,
				'code' => 'no-failures'
			];
		}

		return [
			'success' => false,
			'code' => 'failures',
			'failure' => $failure
		];

	}

	/**
	 * This array contains the name of the fixes to do in the order they should be performed
	 */
	private $checkNames = [
		'checkPageCounts',
		'checkMixedOrientation',
		'checkBindingEdges',
		'checkBindingSides',
		'checkMixedPaperSize',
		'checkHasTrimMarks',
		'checkPaperSizeMatchASize',
		'checkFilePaperSizeMatchesOrderItem'
	];

	private function doChecks() {

		$failure = [];

		$checkNames = $this->checkNames;

		foreach ($checkNames as $checkName) {

			$checkResult = $this->{$checkName}();

			if (!$checkResult['success']) {

				$descriptionCode = !empty($checkResult['elements']['descriptionCode'])
					? $checkResult['elements']['descriptionCode']
					: 'default'
						;

				$data = !empty($checkResult['data'])
					? $checkResult['data']
					: []
				;

				$checkResult['alert'] = $this->getFixAlert($descriptionCode, $data);

				if (!empty($checkResult['elements']['options'])) {

					foreach ($checkResult['elements']['options'] as $configurationOption => $optionName) {

						$option = $this->getFixOption($optionName);

						$checkResult['elements']['options'][$configurationOption] = $option;

					}

				}

				$failure = array_merge(
					$checkResult,
					[
						'checkName' => $checkName,
						'invalid_order_item_key' => $this->orderItemKey,
						'invalid_order_item' => $this->orderItem
					]
				);

				if (Configure::read('debug') && !empty($failure['elements']['descriptionCode'])) {

					$codeText = '<span class="font-size-small">Code : ' . $failure['elements']['descriptionCode'] . '</span>';
					if (!empty($failure['alert']['message'])) {
						$failure['alert']['message'] .= '<br>' . $codeText;
					} else {
						$failure['alert']['message'] = $codeText;
					}

				}

				return $failure;

			}
		}

		return $failure;

	}

	private function getFixImage($imageName) {

		$orientation = $this->orderItem['file_orientation'];

		if (!in_array($orientation, ['l', 'p'])) {
			$orientation = 'p';
		}

		$images = [
			'l' => [
				'default' => 'landscape_default.png',
				'scale_up_to_paper_size' => 'landscape_scale_up_to_paper_size.png',
				'xprint_to_trim_marks' => 'landscape_print_to_trim_marks.png',
				'increase_paper' => 'landscape_increase_paper.png',
				'insert_blank_pages' => 'landscape_insert_blank_pages.png',
				'print_as_is' => 'landscape_print_as_is.png',
				'scale_down_to_paper_size' => 'landscape_scale_down_to_paper_size.png',
				'reduce_paper' => 'landscape_reduce_paper.png',
				'binding_edge_long' => 'landscape_binding_edge_long.png',
				'binding_edge_short' => 'landscape_binding_edge_short.png',
				'binding_side_head_to_foot' => 'landscape_binding_side_head_to_foot.png',
				'binding_side_head_to_head' => 'landscape_binding_side_head_to_head.png',
				'binding_side_left' => 'landscape_binding_side_left.png',
				'binding_side_right' => 'landscape_binding_side_right.png'
			],
			'p' => [
				'default' => 'portrait_default.png',
				'scale_up_to_paper_size' => 'portrait_scale_up_to_paper_size.png',
				'xprint_to_trim_marks' => 'portrait_print_to_trim_marks.png',
				'increase_paper' => 'portrait_increase_paper.png',
				'insert_blank_pages' => 'portrait_insert_blank_pages.png',
				'print_as_is' => 'portrait_print_as_is.png',
				'scale_down_to_paper_size' => 'portrait_scale_down_to_paper_size.png',
				'reduce_paper' => 'portrait_reduce_paper.png',
				'binding_edge_long' => 'portrait_binding_edge_long.png',
				'binding_edge_short' => 'portrait_binding_edge_short.png',
				'binding_side_head_to_foot' => 'portrait_binding_side_head_to_foot.png',
				'binding_side_head_to_head' => 'portrait_binding_side_head_to_head.png',
				'binding_side_left' => 'portrait_binding_side_left.png',
				'binding_side_right' => 'portrait_binding_side_right.png'
			],
			'generic' => [
				'add_crop_marks_and_trim',
				'rotate_l_to_p',
				'rotate_p_to_l',
				'print_to_trim_marks'
			]
		];

		if (!empty($images[$orientation][$imageName])) {
			$imageName = $images[$orientation][$imageName];
		} else {
			$imageName .= '.png';
		}

		return 'pages' . DS . $imageName;

	}

	private function getFixAlert($descriptionName = '', $data = []) {

		switch ($descriptionName) {

			case 'print_to_trim_marks':
				return [
					'class' => 'info',
					'heading' => 'We\'ve identified that your file has crop marks.',
					'message' => 'How would you like us to print it?'
				];
				break;

			case 'inconsistent_paper_size':

				if (!$data) {
					return [
						'heading' => 'Your file contains a mix of paper sizes; they need to all be the same.',
						'message' => 'Please choose one of the solutions before continuing.'
					];
				}

				foreach ($data as $paperSizeId => $pageCount) {
					$pageSizes[] = $pageCount . ' ' . $paperSizeId . ' pages';
				}
				App::uses('String', 'Utility');
				$pageSizeDescription = String::toList($pageSizes);

				return [
					'class' => 'warning',
					'heading' => 'Your file contains a mix of paper sizes.',
					'message' => 'It has ' . $pageSizeDescription . '; they need to all be the same. Please choose one of the solutions before continuing.'
				];

				break;

			case 'oversized_image':
				return [
					'class' => 'warning',
					'heading' => 'You have chosen to print this document on ' . $this->orderItem['Media']['PaperSize']['id'] . ' paper, but the document (or parts of it) is ' . $this->orderItem['file_paper_size_id'] . '.',
					'message' => 'Please choose one of the solutions before continuing.'
				];
				break;

			case 'page_divisor':
				return [
					'class' => 'warning',
					'heading' => 'The number of pages for the binding type you have picked has to be divisible by 4.',
					'message' => 'We need to insert some blank pages to correct the page count.'
				];
				break;

			case 'page_size_mismatch':
				return [
					'class' => 'warning',
					'heading' => 'You have chosen to print this document on ' . $this->orderItem['Media']['PaperSize']['id'] . ' paper, but the uploaded document doesn\'t exactly match that paper size.',
					'message' => 'Please choose one of the solutions before continuing.'
				];
				break;

			case 'oversized_file_has_trim_marks':
				return [
					'class' => 'info',
					'heading' => 'We\'ve identified that your file has crop marks.',
					'message' => 'How would you like us to print it?'
				];
				break;
				break;

			case 'rotate':

				$portraitPage = $this->orderItem['file_orientation_count_p'] == 1
					? 'page'
					: 'pages'
						;

				$landscapePage = $this->orderItem['file_orientation_count_l'] == 1
					? 'page'
					: 'pages'
						;

				return [
					'class' => 'warning',
					'heading' => 'Your file has mixed orientation (page rotation) and they all need to be the same.',
					'message' => 'Your file has ' . $this->orderItem['file_orientation_count_p'] . ' portrait ' . $portraitPage . ' and ' . $this->orderItem['file_orientation_count_l'] . ' landscape ' . $landscapePage . '. Please choose one of the solutions before continuing.'
				];
				break;

			case 'undersized_image':
				return [
					'class' => 'warning',
					'heading' => 'You have chosen to print this document on ' . $this->orderItem['Media']['PaperSize']['id'] . ' paper, but the uploaded document is smaller.',
					'message' => 'Please choose one of the solutions before continuing.'
				];
				break;

			case 'set_binding_edge':
				return [
					'class' => 'info',
					'heading' => 'Which edge would you like us to bind?'
				];
				break;

			case 'set_binding_side':
				return [
					'class' => 'info',
					'heading' => 'Which side would you like us to bind?'
				];
				break;

			case 'default':
				return [
					'class' => 'warning',
					'heading' => 'We have detected that your file doesn\'t quite match what we expected.',
					'message' => 'Please choose one of the following options before continuing with your order.'
				];
				break;

		}

	}

	/**
	 * This function takes the name of a fix option and returns the data needed to populate the option on the screen
	 * @param  string $optionName The name of the option presented to the user
	 * @return array  The data needed to generate the option form
	 */
	private function getFixOption($optionName) {

		switch ($optionName) {

			case 'scale_up_to_paper_size':

				$option = [
					'heading' => 'Scale your document to fit on ' . $this->orderItem['Product']['paper_size_id'] . ' paper',
					'message' => 'We\'ll resize your file to fit the printable area of your chosen paper size (' . $this->orderItem['Product']['paper_size_id'] . '), leaving white space above/below or left/right.<br><span class="label label-warning">Attention <i class="fa fa-exclamation-circle"></i></span> Depending on your artwork, enlarging your document could lead to a blurred final result.',
					'image' => 'scale_up_to_paper_size',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixScaleToPaperSize',
							'data' => [
								'paper_size_id' => $this->orderItem['Product']['paper_size_id']
							]
						]
					]
				];
				break;

			case 'scale_down_to_paper_size':

				$option = [
					'heading' => 'Scale your document to fit on ' . $this->orderItem['Product']['paper_size_id'] . ' paper',
					'message' => 'We\'ll resize your file to fit on the order\'s paper size (' . $this->orderItem['Product']['paper_size_id'] . ').',
					'image' => 'scale_down_to_paper_size',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixScaleToPaperSize',
							'data' => [
								'paper_size_id' => $this->orderItem['Product']['paper_size_id']
							]
						]
					]
				];
				break;

			case 'increase_paper':

				$option = [
					'heading' => 'Print your document on ' . $this->orderItem['file_paper_size_id'] . ' paper to match your file',
					'message' => 'We\'ll leave your file as you uploaded it but print on ' . $this->orderItem['file_paper_size_id'] . ' paper to match it.',
					'image' => 'increase_paper',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixScaleToPaperSize',
							'data' => [
								'paper_size_id' => $this->orderItem['file_paper_size_id']
							]
						]
					]
				];
				break;

			case 'reduce_paper':

				$option = [
					'heading' => 'Print on best fit paper (' . $this->orderItem['file_paper_size_id'] . ')',
					'message' => 'We\'ll leave your document exactly as you uploaded it and print it on smaller best fit paper (' . $this->orderItem['file_paper_size_id'] . ').',
					'image' => 'reduce_paper',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixScaleToPaperSize',
							'data' => [
								'paper_size_id' => $this->orderItem['file_paper_size_id']
							]
						]
					]
				];
				break;

			case 'insert_blank_pages':

				$option = [
					'heading' => 'Insert blank pages',
					'message' => 'We\'ll insert blank pages at the end of your file to correct the page count.',
					'image' => 'insert_blank_pages',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixInsertBlankPages'
						]
					]
				];
				break;

			case 'print_to_trim_marks':

				$option = [
					'heading' => 'Print and trim to your crop marks',
					'message' => 'We\'ll print your document on best fit paper and trim according to your crop marks.',
					'image' => 'print_to_trim_marks',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixScaleToPaperSize',
							'data' => [
								'paper_size_id' => $this->orderItem['file_paper_size_id']
							]
						]
					]
				];
				break;

			case 'print_as_is':

				$option = [
					'heading' => 'Print on your chosen paper (' . $this->orderItem['file_paper_size_id'] . ')',
					'message' => 'We\'ll leave your document exactly as you uploaded it and print it on your chosen paper (' . $this->orderItem['file_paper_size_id'] . ') leaving a border to the top/bottom and/or sides.',
					'image' => 'print_as_is',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixPrintAsIs'
						]
					]
				];
				break;

			case 'print_on_sra':

				$option = [
					'heading' => 'Print on SR' . $this->orderItem['file_paper_size_id'] .' paper',
					'message' => 'Print your document on SR' . $this->orderItem['file_paper_size_id'] .' paper and trim to your crop marks.',
					'image' => 'sra',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixPrintOnSra',
							'data' => [
								'print_on_sra' => true
							]
						]
					]
				];
				break;

			case 'rotate_to_landscape':

				$option = [
					'heading' => 'Rotate all portrait pages to landscape',
					'message' => 'We\'ll rotate all portrait pages so the document prints in landscape.',
					'image' => 'rotate_p_to_l',
					'actions' => [
						[
							'label' => '<i class="fa fa-repeat"></i> Clockwise',
							'fix_code' => 'fixRotatePages',
							'data' => [
								'rotate_to' => 'landscape',
								'direction' => 'clockwise'
							]
						],
						[
							'label' => '<i class="fa fa-repeat fa-flip-horizontal"></i> Anti-clockwise',
							'fix_code' => 'fixRotatePages',
							'data' => [
								'rotate_to' => 'landscape',
								'direction' => 'counter'
							]
						]
					]
				];
				break;

			case 'rotate_to_portrait':

				$option = [
					'heading' => 'Rotate all landscape pages to portrait',
					'message' => 'We\'ll rotate all landscape pages so the document prints in portrait.',
					'image' => 'rotate_l_to_p',
					'actions' => [
						[
							'label' => '<i class="fa fa-repeat"></i> Clockwise',
							'fix_code' => 'fixRotatePages',
							'data' => [
								'rotate_to' => 'portrait',
								'direction' => 'clockwise'
							]
						],
						[
							'label' => '<i class="fa fa-repeat fa-flip-horizontal"></i> Anti-clockwise',
							'fix_code' => 'fixRotatePages',
							'data' => [
								'rotate_to' => 'portrait',
								'direction' => 'counter'
							]
						]
					]
				];
				break;

			case 'add_crop_marks_and_trim':

				$option = [
					'heading' => 'Add crop marks and trim to size',
					'message' => 'We\'ll add crop marks, print your document on best fit paper and trim it to size. Your finished item will be the exact size of your uploaded file.',
					'image' => 'add_crop_marks_and_trim',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixAddCropMarksAndTrim'
						]
					]
				];
				break;

			case 'upload_without_bleed':

				$option = [
					'heading' => 'No bleed? Save money and print on ' . $this->orderItem['Product']['paper_size_id'] . ' paper',
					'message' => 'If no pages in your document require bleed, why not export your file as an  ' . $this->orderItem['Product']['paper_size_id'] . ' file without crop marks and we\'ll print it directly onto  ' . $this->orderItem['Product']['paper_size_id'] . ' paper (no need for trimming).',
					'image' => '',
					'actions' => []
				];
				break;

			case 'set_binding_edge_long':

				$option = [
					'heading' => 'Bind on the long edge',
					'message' => 'We\'ll bind your document along the longer edge',
					'image' => 'binding_edge_long',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixSetBindingEdge',
							'data' => [
								'binding_edge' => 'long'
							]
						]
					]
				];
				break;

			case 'set_binding_edge_short':

				$option = [
					'heading' => 'Bind on the short edge',
					'message' => 'We\'ll bind your document along the shorter edge',
					'image' => 'binding_edge_short',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixSetBindingEdge',
							'data' => [
								'binding_edge' => 'short'
							]
						]
					]
				];
				break;

			case 'set_binding_side_head_to_foot':

				$option = [
					'heading' => 'Head to foot binding (like a calendar)',
					'message' => 'Head to foot binding (like a calendar)',
					'image' => 'binding_side_head_to_foot',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixSetBindingSide',
							'data' => [
								'binding_side' => 'head_to_foot'
							]
						]
					]
				];
				break;

			case 'set_binding_side_head_to_head':

				$option = [
					'heading' => 'Head to head binding (like a presenter)',
					'message' => 'Head to head binding (like a presenter)',
					'image' => 'binding_side_head_to_head',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixSetBindingSide',
							'data' => [
								'binding_side' => 'head_to_head'
							]
						]
					]
				];
				break;

			case 'set_binding_side_left':

				$option = [
					'heading' => 'Bind on the left edge',
					'message' => 'Bind on the left edge (typically for left to right reading)',
					'image' => 'binding_side_left',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixSetBindingSide',
							'data' => [
								'binding_side' => 'left'
							]
						]
					]
				];
				break;

			case 'set_binding_side_right':

				$option = [
					'heading' => 'Bind on the right side',
					'message' => 'Bind on the right side (typically for right to left reading)',
					'image' => 'binding_side_right',
					'actions' => [
						[
							'label' => 'Select',
							'fix_code' => 'fixSetBindingSide',
							'data' => [
								'binding_side' => 'right'
							]
						]
					]
				];
				break;

		}

		$option['name'] = $optionName;

		// Now get the right image based on orientation
		if ($option['image']) {
			$option['image'] = $this->getFixImage($option['image']);
		}

		return $option;

	}

	/**
	 * @return mixed
	 */
	private function checkPageCounts() {

		// See if the page limits are being met - including page divisors
		$failure = $this->OrderItem->Product->pageCountValidation($this->orderItem);

		if (!$failure['success'])
		{

			if ($failure['code'] == 'page-count-divisor') {
				return [
					'success' => false,
					'checkName' => 'checkPageCounts',
					'elements' => [
						'descriptionCode' => 'page_divisor',
						'options' => [
							'insert_blank_pages'
						],
						'data' => $failure['data']
					],
					'alert' => [
						'class' => 'warning'
					]
				];

			} else {
				$failure['alert'] = [
					'class' => 'warning'
				];
			}
		}

		return $failure;
	}

	/**
	 * @return array
	 */
	private function checkFilePaperSizeMatchesOrderItem() {

		$filePaperSizeOrderMatch = $this->orderItem['file_paper_size_order_item_match'];

		if ($filePaperSizeOrderMatch !== '=') {

			// The files do not match the order item

			// If the $filePaperSizeOrderMatch variable is -1 it means the image is smaller than the paper (e.g.) A5 file uploaded to an A4 order item

			// If the $filePaperSizeOrderMatch variable is 1 it means the image is larger than the paper (e.g.) A3 file uploaded to an A4 order item

			if ($filePaperSizeOrderMatch === 'o') {

				return [
					'success' => false,
					'checkName' => 'checkFilePaperSizeMatchesOrderItem',
					'elements' => [
						'descriptionCode' => 'oversized_image',
						'options' => [
							'scale_down_to_paper_size',
							'increase_paper'
						]
					],
					'alert' => [
						'class' => 'warning'
					]
				];

			} else {

				return [
					'success' => false,
					'checkName' => 'checkFilePaperSizeMatchesOrderItem',
					'elements' => [
						'descriptionCode' => 'undersized_image',
						'options' => [
							'reduce_paper',
							'scale_up_to_paper_size'
						]
					],
					'alert' => [
						'class' => 'warning'
					]
				];

			}

		}

		return ['success' => true];
	}

	/**
	 * @return array
	 */
	private function checkMixedPaperSize() {

		if ($this->orderItem['file_paper_size_consistency']) {

			return ['success' => true];

		}

		// The paper sizes are not consistent across the order item

		$paperSizeConsistency = $this->OrderItem->paperSizeConsistency($this->orderItemId);

		$selectedPaperNumber = $this->OrderItem->FilePaperSize->pageSizeRank($paperSizeConsistency['data']['paper_size_id']);

		$biggerOrSmallerThanSelected = "smaller";

		foreach ($paperSizeConsistency['data']['paperSizes'] as $paperSize => $pageCount) {

			$paperNumber = $this->OrderItem->FilePaperSize->pageSizeRank($paperSize);

			if ($paperNumber < $selectedPaperNumber) {

				$biggerOrSmallerThanSelected = "smaller";

			} elseif($paperNumber < $selectedPaperNumber) {

				$biggerOrSmallerThanSelected = "bigger";

			}
			// Do nothing if they're the same.

		}


		$options = [
			'scale_up_to_paper_size',
			'scale_down_to_paper_size'
		];

		return [
			'success' => false,
			'requote' => true,
			'checkName' => 'checkMixedPaperSize',
			'data' => $paperSizeConsistency['data']['paperSizes'],
			'elements' => [
				'descriptionCode' => 'inconsistent_paper_size',
				'options' => $options
			],
			'alert' => [
				'class' => 'warning'
			]
		];

		return ['success' => true];

	}

	/**
	 * @return array
	 */
	private function checkMixedOrientation() {

		if (!$this->orderItem['file_orientation_consistency']) {

			// Orientation is not consistent

			return [
				'success' => false,
				'checkName' => 'checkMixedOrientation',
				'elements' => [
					'descriptionCode' => 'rotate',
					'options' => [
						'rotate_to_landscape',
						'rotate_to_portrait'
					]
				],
				'alert' => [
					'class' => 'warning'
				]
			];
		}

		return ['success' => true];

	}

	/**
	 * Examines the order_item_file.binding_edge_criteria field
	 * @return array An arry with a success key and - if false - the ekys needed to show the user
	 */
	private function checkBindingEdges() {

		if ($this->orderItem['binding_edge_criteria'] !== '?') {

			return ['success' => true];
		}

		// We haven't offered or checked the binding edge criteria yet

		$productHasBindingEdgeTypes = $this->OrderItem->Product->hasRelatedRows(
			'ProductBindingEdgeType',
			$this->orderItem['product_id']
		);

		if (!$productHasBindingEdgeTypes) {

			// The product doesn't accommodate binding edges so set the binding_edge_criteria field to 'n'
			$this->OrderItem->setBindingEdgeCriteria($this->orderItemId, 'n');

			return ['success' => true];
		}

		if ($this->orderItem['file_orientation'] == 'p') {

			// Binding edge must be long

			$this->OrderItem->setBindingEdgeTypeId($this->orderItemId, 'long');
			$this->OrderItem->setBindingEdgeCriteria($this->orderItemId, 'y');

			return [
				'success' => true,
				'action' => 'configure'
			];

		} else {
			// TODO - only offer those set up

			$availableOptions = $this->availableOptions(
				'BindingEdgeType',
				'set_binding_edge_'
			);

			return [
				'success' => false,
				'checkName' => 'checkBindingEdges',
				'elements' => [
					'descriptionCode' => 'set_binding_edge',
					'options' => [
						'set_binding_edge_long',
						'set_binding_edge_short'
					]
				],
				'alert' => [
					'class' => 'info',
					'message' => 'Please choose an option to configure your document.'
				],
				'action' => 'configure'
			];
		}
	}

	/**
	 * Examines the order_item_file.binding_side_criteria field
	 * @return array An arry with a success key and - if false - the ekys needed to show the user
	 */
	private function checkBindingSides() {

		if ($this->orderItem['binding_side_criteria'] !== '?') {
			return ['success' => true];
		}

		// We haven't offered or checked the binding side criteria yet

		$availableOptions = $this->availableOptions(
			'BindingSideType',
			'set_binding_side_'
		);

		if (empty($availableOptions['options'])) {

			// The product doesn't accommodate binding sides so set the binding_side_criteria field to 'n'
			$this->OrderItem->setBindingSideCriteria($this->orderItemId, 'n');

			return ['success' => true];

		}

		if (count($availableOptions['options']) === 1) {

			// There's only one option, so set it

			$this->options['binding_side'] = $availableOptions['options'][0]['BindingSideType']['code'];

			$result = $this->fixSetBindingSide();

			return ['success' => true];

		}

		if ($this->orderItem['file_orientation'] == 'p') {

			// These are the only options we want to offer
			$possibleOptions = [
				'set_binding_side_left',
				'set_binding_side_right'
			];

			$options = [];

			// Loop through them and make sure each one is available for this product
			foreach ($possibleOptions as $possibleOption) {

				// Is the possible option in the available options?
				if (in_array($possibleOption, array_keys($availableOptions['codes']))) {
					// Yes, so add it to the options array
					$options[] = $possibleOption;
				}

			}

			if (!$options) {

				// None of the possible options is available for this product, so return a fail
				return [
					'success' => false,
					'code' => 'no-options'
				];

			}

			// If there's only a single match, just apply it
			if (count($options) === 1) {

				// Extract the code
				$code = $availableOptions['codes'][$options[0]];

				// Set the option
				$this->options['binding_side'] = $code;

				// Run the fix
				$result = $this->fixSetBindingSide();

				// Return success
				return ['success' => true];

			}

			// If we are here it's because there are many possible options
			// Binding side
			return [
				'success' => false,
				'checkName' => 'checkBindingSides',
				'elements' => [
					'descriptionCode' => 'set_binding_side',
					'options' => $options
				],
				'alert' => [
					'class' => 'info',
					'message' => 'Please choose an option to configure your document.'
				]
			];

		} else {

			$bindingEdgeType = null;

			if ($this->orderItem['binding_edge_type_id']) {

				$bindingEdgeType = $this->OrderItem->BindingEdgeType->shortLong($this->orderItem['binding_edge_type_id']);
			}

			if (!$bindingEdgeType) {

				return [
					'success' => false,
					'checkName' => 'checkBindingSides',
					'elements' => [
						'descriptionCode' => 'set_binding_side',
						'options' => [
							'set_binding_edge_long',
							'set_binding_edge_short'
						]
					],
					'alert' => [
						'class' => 'info',
						'message' => 'Please choose an option to configure your document.'
					],
					'action' => 'configure'
				];
			}

			if ($bindingEdgeType == 'long') {

				return [
					'success' => false,
					'checkName' => 'checkBindingSides',
					'elements' => [
						'descriptionCode' => 'set_binding_side',
						'options' => [
							'set_binding_side_head_to_head', //1
							'set_binding_side_head_to_foot'
						]
					],
					'alert' => [
						'class' => 'info',
						'message' => 'Please choose an option to configure your document.'
					]
				];

			} else {

				return [
					'success' => false,
					'checkName' => 'checkBindingSides',
					'elements' => [
						'descriptionCode' => 'set_binding_side',
						'options' => [
							'set_binding_side_left',
							'set_binding_side_right'
						]
					],
					'alert' => [
						'class' => 'info',
						'message' => 'Please choose an option to configure your document.'
					]
				];
			}

		}
	}

	private function checkHasTrimMarks() {

		if (!$this->orderItem['file_has_trim_marks'] || $this->orderItem['file_paper_size_match'] === 'exact') {
			return ['success' => true];
		}

		// The file doesn't match an exact A size, but It has trim marks - it's a pro job
		return [
			'success' => false,
			'checkName' => 'checkHasTrimMarks',
			'elements' => [
				'descriptionCode' => 'print_to_trim_marks',
				'options' => [
					'print_to_trim_marks',
					'upload_without_bleed'
				]
			],
			'alert' => [
				'class' => 'info'
			]
		];

	}

	/**
	 * @return mixed
	 */
	private function checkPaperSizeMatchASize() {

		if ($this->orderItem['file_paper_size_match'] == 'exact') {
			return ['success' => true];
		}

		// The paper size is not an exact A size - it will have been placed into the next larger size

		// No trim marks, so assume it just doesn't fit on an A size.
		return [
			'success' => false,
			'checkName' => 'checkPaperSizeMatchASize',
			'elements' => [
				'descriptionCode' => 'page_size_mismatch',
				'options' => [
					'scale_up_to_paper_size',
					'add_crop_marks_and_trim'
				]
			],
			'alert' => [
				'class' => 'warning'
			]
		];

	}


	/**
	 * This function looks at the Product*** models to see what options are available
	 * @param  mixed  [$productId                = null]        The product of the order item
	 * @param  string [$productModelName         = null] The name of the related model (e.g. ProductBindingEdgeType)
	 * @param  string [$optionCode               = null]       An option code to use as a prefix for the returned values
	 * @return array  The results
	 */
	public function availableOptions($productModelName = null, $codePrefix = null) {

		if (!$productModelName) {
			return [
				'succes' => false,
				'code' => 'missing-parameters'
			];
		}

		$productOptionModelName = 'Product' . $productModelName;

		App::uses($productOptionModelName, 'Model');
		$productOptionModel = ClassRegistry::init($productOptionModelName);

		$availableOptions = $productOptionModel->find(
			'all',
			[
				'conditions' => [
					$productOptionModelName . '.product_id' => $this->orderItem['product_id']
				],
				'contain' => [
					$productModelName => [
						'fields' => [
							$productModelName . '.id',
							$productModelName . '.code',
							$productModelName . '.name'
						]
					]
				]
			]
		);

		$codes = Hash::extract($availableOptions, '{n}.' . $productModelName);

		if ($codes && $codePrefix) {
			foreach ($codes as $code) {
				$prefixedCodes[$codePrefix . $code['code']] = $code['code'];
			}

			$codes = $prefixedCodes;
		}

		return [
			'options' => $availableOptions,
			'codes' => $codes
		];

	}


	/**
	 * This function applies any fix chosen by the user and passed to the applyFix function of the OrderItems controller
	 * @param  Object Model                 $model - The model attached to the behaviour - OrderItem
	 * @param  string [$fixName             = null] - The name of the fix to apply
	 * @param  string [$orderItemId         = null] - The id of the order item to apply the fix to
	 * @param  array  [$fixFailure          = []] - The details of the fix we are addressing
	 * @param  array  [$options             = []] - The options passed in - generated by the form the user submits
	 * @return array  Retirns a simple array to record if the fix was successful or not
	 */
	public function applyFix(Model $model, $fixName = null, $orderItemId = null, $fixFailure = [], $options = []) {

		// Check we have a fix name
		if (!$fixName) {

			return [
				'success' => false,
				'code' => 'no-fix-name'
			];
		}

		// Check we have an order item id
		if (!$orderItemId) {

			return [
				'success' => false,
				'code' => 'no-order-item-id'
			];
		}

		// Set some local variables
		$this->orderItemId = $orderItemId;
		$this->fixFailure = $fixFailure;
		$this->options = $options;

		// Move the id of the OrderItem model to the chosen id
		$this->OrderItem->id = $orderItemId;

		// Check it exists
		if (!$this->OrderItem->exists()) {

			// If not bounce it out
			return [
				'success' => false,
				'code' => 'invalid-order-item-id',
				'from' => 'OrderItemFixable.applyFix'
			];

		}

		$orderItem = $this->OrderItem->findById($orderItemId);

		$this->orderItem = $orderItem['OrderItem'];

		// Assume we don't need to quote again
		$requote = false;

		// Now we can run the fix
		$result = $this->{$fixName}();

		if (!empty($result['requote'])) {
			$orderId = $this->OrderItem->field(
				'order_id',
				['OrderItem.id' => $orderItemId]
			);
			$this->OrderItem->Order->requote($orderId, $orderItemId);
		}

		// Return the result
		return $result;

	}

	private function fixPrintToTrimMarks() {

		$this->OrderItem->set([
			'file_paper_size_match' => 'exact',
			'file_paper_size_match_consistency' => true
		]);
		$this->OrderItem->save();

		$this->OrderItem->OrderItemProcess->record(
			$this->orderItemId,
			'print-to-trims'
		);

		return [
			'success' => true,
			'message' => 'We\'ll print and trim to your crop marks.'
		];

	}

	// TODO - finish the crop function
	private function fixCrop() {

		$this->OrderItem->set('print_on_sra', $printOnSra);
		$this->OrderItem->save();

		$this->OrderItem->OrderItemProcess->record(
			$this->orderItemId,
			'change-paper-size',
			[
				'paper-size' => $targetPaperSizeId
			]
		);

		return [
			'success' => true,
			'requote' => true,
			'message' => 'We\'ll print to your crop marks.'
		];

	}

	// TODO - finish the crop function
	private function fixInsertBlankPages() {

		if (empty($this->fixFailure['elements']['data']['insert_page_count'])) {

			return [
				'success' => false,
				'code' => 'no-blank-page-count',
				'message' => 'Please specify how many blank pages to insert.'
			];

		}

		$blankPageCount = $this->fixFailure['elements']['data']['insert_page_count'];

		$this->OrderItem->insertBlankPages($this->orderItemId, $blankPageCount);

		return [
			'success' => true,
			'requote' => true,
			'message' => sprintf('We\'ll insert %d blank pages.', $blankPageCount)
		];

	}

	private function fixPrintAsIs() {

		switch ($this->fixFailure['check']) {
			case 'paper_size_match':
				$data = [
					'file_paper_size_match' => 'exact'
				];
				break;
		}

		$this->OrderItem->set($data);
		$this->OrderItem->save();

		return [
			'success' => true,
			'requote' => false,
			'message' => [
				'message' => 'We\'ll print your document exactly as you have specified.',
				'element' => 'info'
			]
		];

	}

	private function fixPrintOnSra() {

		$this->OrderItem->set('print_on_sra', $options['print_on_sra']);
		$this->OrderItem->save();

		return ['success' => true];

	}

	private function fixTrimToSize() {

		// Set the order item:
		// - to print on SRA paper
		// - show the paper size matches as exact
		// - the paper size match consistency is all true
		$this->OrderItem->set([
			'file_paper_size_match' => 'exact',
			'file_paper_size_match_consistency' => true
		]);
		$this->OrderItem->save();

		$this->OrderItem->OrderItemProcess->record(
			$this->orderItemId,
			'add-crop-marks-and-trim'
		);

		return [
			'success' => true,
			'requote' => true,
			'message' => 'We\'ll add crop marks and trim to size.'
		];

	}

	private function fixAddCropMarksAndTrim() {

		// Set the order item:
		// - to print on SRA paper
		// - show the paper size matches as exact
		// - the paper size match consistency is all true
		$this->OrderItem->set([
			'file_paper_size_match' => 'exact',
			'file_paper_size_match_consistency' => true
		]);
		$this->OrderItem->save();

		$this->OrderItem->OrderItemProcess->record(
			$this->orderItemId,
			'add-crop-marks-and-trim'
		);

		return [
			'success' => true,
			'requote' => false,
			'message' => 'We\'ll add crop marks and trim your document.'
		];

	}

	private function fixRotatePages() {

		$rotateTo = $this->options['rotate_to'];
		$direction = $this->options['direction'];

		if (!$rotateTo) {

			return [
				'success' => false,
				'code' => 'no-target-rotation',
				'requote' => false,
				'message' => 'Please tell us how we should rotate your pages.'
			];
		}

		$rotateToCode = substr($rotateTo, 0, 1);

		$orderItemFileIds = $this->OrderItem->orderItemFileIds($this->orderItemId);

		$orderItemFilePages = $this->OrderItem->OrderItemFile->OrderItemFilePage->find(
			'all',
			[
				'conditions' => [
					'OrderItemFilePage.order_item_file_id' => $orderItemFileIds,
					'OrderItemFilePage.orientation <> ' => $rotateToCode
				]
			]
		);

		if (!$orderItemFilePages) {

			foreach ($orderItemFileIds as $orderItemFileId) {
				$this->OrderItem->OrderItemFile->setOrientationConsistency($orderItemFileId);
			}

			return [
				'success' => true,
				'code' => 'no-pages-to-rotate',
				'requote' => false,
				'message' => 'There are no pages to rotate.'
			];

		}

		// This quotes the target value for SQL purposes
		$strTargetOrientationCode = $this->OrderItem->sqlize($rotateToCode, 'string');

		if ($direction == 'clockwise') {
			$rotateAngle = 90;
			$rotateCode = 'clockwise-90';
		} else {
			$rotateAngle = 270;
			$rotateCode = 'counter-90';
		}

		$processCode = 'rotate-to-' . $rotateTo . '-' . $rotateCode;

		foreach ($orderItemFilePages as $orderItemFilePage) {

			$orderItemFilePageId = $orderItemFilePage['OrderItemFilePage']['id'];

			try {
				$this->OrderItem->OrderItemFile->rotateImage($orderItemFilePage['OrderItemFilePage']['preview_image_path'], $rotateAngle);
			} catch (Exception $e) {

				return [
					'success' => false,
					'code' => 'rotation-failed',
					'message' => $e->getMessage(),
					'requote' => false
				];
			}

		}

		$result = $this->OrderItem->OrderItemFile->OrderItemFilePage->updateAll(
			[
				'OrderItemFilePage.orientation' => $strTargetOrientationCode,
				'OrderItemFilePage.process_rotate' => true
			],
			[
				'OrderItemFilePage.order_item_file_id' => $orderItemFileIds,
				'OrderItemFilePage.orientation <>' => $strTargetOrientationCode
			]
		);

		if ($result) {

			foreach ($orderItemFileIds as $orderItemFileId) {
				$this->OrderItem->OrderItemFile->setOrientationConsistency($orderItemFileId);
			}

			$result = $this->OrderItem->OrderItemProcess->record(
				$this->orderItemId,
				$processCode,
				[
					'rotate-angle' => $rotateAngle,
					'rotate-to' => $strTargetOrientationCode
				]
			);

			return [
				'success' => true,
				'code' => 'pages-rotated',
				'requote' => false,
				'message' => 'We\'ll rotate your pages.'
			];

		} else {

			return [
				'success' => false,
				'code' => 'update-failed',
				'requote' => false,
				'message' => 'Page rotation failed.'
			];
		}
	}

	/**
		This function takes individual files and changes the paper size to match the paper size of the product related to the order item.
	*/
	private function fixScaleToPaperSize() {

		if (empty($this->options['paper_size_id'])) {
			return [
				'success' => false,
				'code' => 'no-scale-option',
				'requote' => false,
				'message' => 'Please tell us how we should resize your pages.'
			];
		}

		$orderItem = $this->OrderItem->find(
			'first',
			[
				'conditions' => [
					'OrderItem.id' => $this->orderItemId
				],
				'fields' => [
					'OrderItem.id',
					'OrderItem.order_id',
					'OrderItem.product_id'
				],
				'contain' => [
					'Product' => [
						'fields' => [
							'Product.id',
							'Product.paper_size_id',
							'Product.binding_type_id'
						]
					]
				]
			]
		);

		$targetPaperSizeId = $this->options['paper_size_id'];

		$newProduct = $this->OrderItem->Product->find(
			'first',
			[
				'conditions' => [
					'Product.binding_type_id' => $orderItem['Product']['binding_type_id'],
					'Product.paper_size_id' => $targetPaperSizeId
				]
			]
		);

		if (!$newProduct) {
			return [
				'success' => false,
				'code' => 'new-product-not-available',
				'requote' => false,
				'message' => 'That product is not available.'
			];
		}

		$newProductId = $newProduct['Product']['id'];

		if (!empty($orderItem['Product']['paper_size_id'])) {
			$currentPaperSizeId = $orderItem['Product']['paper_size_id'];
		}

		// From this get the process code to apply
		// The cut-sheet codes scale both up and down
		$processCode = 'scale-to-cut-sheet-' . $targetPaperSizeId;

		$targetPaperSizeId = $this->OrderItem->sqlize($targetPaperSizeId, 'string');
		$targetPaperSizeMatch = $this->OrderItem->sqlize('exact', 'string');

		// Set the order item:
		// - show the paper size matches as exact
		// - the paper size match consistency is all true
		$this->OrderItem->set([
			'product_id' => $newProductId,
			'file_paper_size_match' => 'exact',
			'file_paper_size_match_consistency' => true,
			'file_paper_size_order_item_match' => '='
		]);
		$this->OrderItem->save();

		$output = $this->OrderItem->OrderItemProcess->record(
			$this->orderItemId,
			$processCode,
			[
				'paper-size' => $targetPaperSizeId
			]
		);

		$result['success'] = true;
		$result['requote'] = false;
		$result['message'] = 'We\'ll resize your pages.';

		if ($targetPaperSizeId !== $currentPaperSizeId) {

			$result['requote'] = true;

		}

		return $result;

	}

	private function fixSetBindingEdge() {

		if (empty($this->options['binding_edge'])) {
			return [
				'success' => false,
				'code' => 'no-binding-edge-option',
				'requote' => false,
				'message' => 'Please specify a binding edge.'
			];
		}

		$productId = $this->OrderItem->field(
			'product_id'
		);

		$productBindingEdgeTypeIds = $this->OrderItem->Product->ProductBindingEdgeType->find(
			'list',
			[
				'conditions' => ['ProductBindingEdgeType.product_id' => $productId],
				'fields' => [
					'ProductBindingEdgeType.binding_edge_type_id'
				]
			]
		);

		$bindingEdgeTypes = $this->OrderItem->Product->ProductBindingEdgeType->BindingEdgeType->find(
			'first',
			[
				'conditions' => [
					'BindingEdgeType.id' => $productBindingEdgeTypeIds,
					'BindingEdgeType.code' => $this->options['binding_edge']
				],
				'fields' => [
					'BindingEdgeType.id',
					'BindingEdgeType.name'
				]
			]
		);

		if (!empty($bindingEdgeTypes['BindingEdgeType']['id'])) {
			$data = [
				'id' => $this->orderItemId,
				'binding_edge_criteria' => true,
				'binding_edge_type_id' => $bindingEdgeTypes['BindingEdgeType']['id']
			];

			$result = $this->OrderItem->save($data, ['callbacks' => false]);

			if (!$result) {
				return [
					'success' => false,
					'requote' => false,
					'message' => 'Failed to set he binding edge.'
				];
			}
		}

		return [
			'success' => true,
			'requote' => false,
			'message' => 'The binding edge has been set.'
		];

	}

	private function fixSetBindingSide() {

		if (empty($this->options['binding_side'])) {
			return [
				'success' => false,
				'code' => 'no-binding-side-option',
				'requote' => false,
				'message' => 'Please specify a binding side.'
			];
		}

		$productId = $this->OrderItem->field(
			'product_id'
		);

		$productBindingSideTypeIds = $this->OrderItem->Product->ProductBindingSideType->find(
			'list',
			[
				'conditions' => ['ProductBindingSideType.product_id' => $productId],
				'fields' => [
					'ProductBindingSideType.binding_side_type_id'
				]
			]
		);

		$bindingSideTypes = $this->OrderItem->Product->ProductBindingSideType->BindingSideType->find(
			'first',
			[
				'conditions' => [
					'BindingSideType.id' => $productBindingSideTypeIds,
					'BindingSideType.code' => $this->options['binding_side']
				],
				'fields' => [
					'BindingSideType.id',
					'BindingSideType.name'
				]
			]
		);

		if (!empty($bindingSideTypes['BindingSideType']['id'])) {

			$data = [
				'id' => $this->orderItemId,
				'binding_side_criteria' => true,
				'binding_side_type_id' => $bindingSideTypes['BindingSideType']['id']
			];

			$result = $this->OrderItem->save($data, ['callbacks' => false]);

			if (!$result) {
				return [
					'success' => false,
					'requote' => false,
					'message' => 'Failed to set the binding side.'
				];
			}

		}

		return [
			'success' => true,
			'requote' => false,
			'message' => 'The binding side has been set.'
		];

	}

}
