<?php
App::uses('ModelBehavior', 'Model');

/**
 * Behavior to manage and extract configuration values
 */
class ConfigurableBehavior extends ModelBehavior {

	public function setup(Model $Model, $settings = array()) {

		$this->modelAlias = $Model->alias;

		if (!isset($this->settings[$this->modelAlias])) {
			$this->settings[$this->modelAlias] = array(
				'recursive' => true,
				'notices' => true,
				'autoFields' => true
			);
		}
		$this->settings[$this->modelAlias] = array_merge($this->settings[$this->modelAlias], $settings);
	}

	public function getConfigurationValue(Model $model, $codes = null, $delimiter = null) {

		if (!$codes) {
			return null;
		}

		// Load the Configuration model
		App::import('model', 'Configuration');

		// Start a new instance
		$this->Configuration = new Configuration();

		return $this->Configuration->getConfigurationValue(
			$codes,
			$delimiter
		);

	}

}
