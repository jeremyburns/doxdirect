<?php
App::uses('ModelBehavior', 'Model');

/**
 * Behavior to interract with Pace
 */
class PaceBehavior extends ModelBehavior {

	public function setup(Model $model, $settings = []) {

		$this->modelAlias = $model->alias;
		$this->model = $model;

		Configure::load('pace_config', 'default');

		$this->config = Configure::read('pace');
		$this->apiUrl = $this->config['api_url'];

	}

	public function getProductCatalogue() {

		return $this->execute('product_list');

	}

	/**
 * @param bool $refresh
 * @return mixed
 * Get a list of delivery options from either the Cache (if available and not refreshing) or Pace.
 */

	public function getDeliveryOptions(Model $model) {

		$response = $this->execute('list_delivery_options');

		return $response['response'];

	}

	/**
 * @param bool $refresh
 * @return mixed
 * A list of countries and their codes (relevant for both Pace customer/shipping data and UPS API stuff) can be retrieved using the listCountries API function (GET or POST). This function returns a JSON array, with each element comprising:

 * - id - int unique ID of the country
 * - name - string name of the country
 * - isoCode - string two-letter ISO-standard country code
 */

	public function getCountries(Model $model) {

		$response = $this->execute('list_countries');

		return $response['response'];

	}

	public function localPaceJobsPath(Model $model, $includeRoot = true) {

		$folderName = '';

		if ($includeRoot) {
			$folderName .= WWW_ROOT;
		}

		$paceFolderName = $this->config['pace_jobs_folder'];

		$folderName .= $paceFolderName . DS;

		return $folderName;

	}

	public function externalPathToJobs() {

//		return 'http://test.doxdirect.com/order/'. $this->config['pace_jobs_folder'];

		return Router::url('/', true) . $this->config['pace_jobs_folder'];

	}

	public function padKey(Model $model, $key = 0) {

		return str_pad($key + 1, 2, '0', STR_PAD_LEFT);

	}

	public function mergeQuote(Model $model, $data = [], $paceQuote = [], $orderItemId = null, $orderItemKey = 0) {

		if (isset($paceQuote['data'])) {

			// Merge the keys into the Order
			$data['Order'] = array_merge(
				$data['Order'],
				$paceQuote['data']['Order']
			);

		}

		// Store the Pace results against the Order
		$data['Order']['latest_quote_result'] = $paceQuote['success'];
		$data['Order']['latest_quote'] = json_encode($paceQuote);

		// If the quote was a success
		if ($paceQuote['success']) {
			// Store the Pace quote id for the order
			$data['Order']['pace_quote_id'] = $paceQuote['response']['output']['quote_id'];
			$data['Order']['amount_delivery'] = $paceQuote['response']['output']['delivery'];
			$data['Order']['amount_turnaround'] = $paceQuote['response']['output']['turnaround'];
		}

		$orderId = !empty($data['Order']['id'])
			? $data['Order']['id']
			: null
		;

		// Assume we want the first pace quote from the paceQuote data
		$paceQuoteKey = 0;

		if ($orderId) {
			// We have an order id so let's get the order
			$order = $model->getOrder($orderId);
		} else {
			// Assume that this is a new quote and therefore data will form the new order
			$order = $data;
		}

		$paceProducts = $paceQuote['response']['output']['products'];

		$orderItemPaceQuoteProductIds = Hash::extract(
			$order['OrderItem'],
			'{n}.pace_quote_product_id'
		);

		$paceQuoteProductIds = Hash::extract(
			$paceProducts,
			'{n}.quote_product_id'
		);

		$orderQuoteKey = 0;

		if (!$orderItemPaceQuoteProductIds && count($paceQuoteProductIds) === 1) {
			// $newOrderItem = $this->extractQuote($paceQuote, $paceQuoteKey);
			$orderQuoteKey = 0;
		} else {

			foreach ($paceQuoteProductIds as $paceQuoteProductIdKey => $paceQuoteProductId) {

				foreach ($orderItemPaceQuoteProductIds as $orderItemPaceQuoteProductIdKey => $orderItemPaceQuoteProductId) {

					if (intval($orderItemPaceQuoteProductId) == $paceQuoteProductId) {
						unset($paceQuoteProductIds[$paceQuoteProductIdKey]);
						unset($orderItemPaceQuoteProductIds[$orderItemPaceQuoteProductIdKey]);
					}

				}

			}

			// There ought to be just one pace product left
			$paceQuoteProductIdKeys = array_keys($paceQuoteProductIds);
			$paceQuoteKey = $paceQuoteProductIdKeys[0];

			foreach ($orderItemPaceQuoteProductIds as $orderItemPaceQuoteProductKey => $value) {
				$orderQuoteKey = $orderItemPaceQuoteProductKey;
				continue;
			}

		}

		// This is an amendment to an existing order item
		$newOrderItem = $this->extractQuote($paceQuote, $paceQuoteKey);

		$data['OrderItem'][$orderQuoteKey] = array_merge(
			$data['OrderItem'][$orderQuoteKey],
			$newOrderItem
		);

		return $data;
	}

	private function extractQuote($paceQuote = [], $paceQuoteKey = 0) {

		$newOrderItem = $paceQuote['data']['OrderItem'][0];

		$newOrderItem['pace_quote_product_id'] = $paceQuote['response']['output']['products'][$paceQuoteKey]['quote_product_id'];

		// And the figures
		$newOrderItem['amount_net'] = $paceQuote['response']['output']['products'][$paceQuoteKey]['price'];
		//   $newOrderItem['amount_turnaround'] = $paceQuote['response']['output']['products'][$paceQuoteKey]['turnaround'];

		return $newOrderItem;
	}

	private function prepareQuoteData($data = [], $orderItemKey = 0) {

		// Start an empty array
		$paceQuoteItems = [];

		// Get the fields we have to populate
		// These come straight out of the posted data rather than being calculated
		$paceQuoteItemFields = [
			'media_id',
			'binding_id',
			'drilling_type_id',
			'side_id',
			'click_id',
			'cover_type_inner_front_id',
			'cover_type_inner_back_id',
			'cover_type_outer_front_id',
			'cover_type_outer_back_id',
			'cover_type_wrap_around_id'
		];

		$orderItem = $data['OrderItem'][$orderItemKey];

		// Loop through them and get the value from the orderItem
		foreach ($paceQuoteItemFields as $paceQuoteItemField) {

			if (!empty($orderItem[$paceQuoteItemField])
//				isset($data['OrderItem'][$orderItemKey][$paceQuoteItemField]) && $data['OrderItem'][$orderItemKey][$paceQuoteItemField] !== '' && $data['OrderItem'][$orderItemKey][$paceQuoteItemField] !== null && $data['OrderItem'][$orderItemKey][$paceQuoteItemField] !== false
			) {

				$paceQuoteItems[$paceQuoteItemField] = $orderItem[$paceQuoteItemField];

			}

		}

		// Now break the Pace data array into a comma separated list
		$paceQuoteItemsList = implode(',', $paceQuoteItems);

		// Set up an array to pass into PACE
		$paceData = [
			'source' => $this->config['source'],
			'ipAddress' => $data['Order']['ip_address'],
			'productType' => $data['OrderItem'][$orderItemKey]['product_id'],
			'copies' => $data['OrderItem'][$orderItemKey]['copies'],
			'pagesPerCopy' => $data['OrderItem'][$orderItemKey]['pages'],
			'turnaround' => $data['Order']['turnaround'],
			'delivery' => $data['Order']['delivery_option_id'],
			'items' => $paceQuoteItemsList,
			'paceQuoteItems' => $paceQuoteItems
		];

		// If the user has selected tabs, add the tab count to the main Pace array
		if (isset($orderItem['tab_count']) && $orderItem['tab_count']) {
			$paceData['tabCount'] = $orderItem['tab_count'];
		}

		// We need to know what type of quote we are doing. Start off by assuming it's a new quote
		$functionName = 'new_quote';

		// Append the existing pace quote ids where necessary

		if (!empty($data['Order']['pace_quote_id'])) {

			// Assume we are adding a new quote item to an existing quote
			$functionName = 'add_to_quote';
			$paceData['quote'] = $data['Order']['pace_quote_id'];
		}

		if (!empty($orderItem['pace_quote_product_id'])) {

			// Assume we are changing an existing quote item
			$functionName = 'replace_in_quote';
			$paceData['quoteProduct'] = $orderItem['pace_quote_product_id'];
		}

		if (isset($paceData['paceQuoteItems'])) {

			// Get them from the result
			$paceQuoteItems = $paceData['paceQuoteItems'];

			// Remove them from the result to avoid breaking Pace
			unset($paceData['paceQuoteItems']);
		}

		// Return the result
		return [
			'data' => $data,
			'paceData' => $paceData,
			'functionName' => $functionName
		];
	}

	/**
	 * Required keys:
	 * - quote int - quote ID for the quote to be removed from (matches with order->pace_quote_id)
	 * - quoteProduct int - the individual item within a quote (matches with order_item->pace_quote_product_id)
	 */
	private function prepareDataRemoveFromQuote($paceQuoteProductId = null, $paceQuoteId = null, $order = null) {

		if (!$paceQuoteProductId) {
			return [
				'success' => false,
				'code' => 'no-quote-product-id',
				'message' => 'No quote product id was passed.',
				'from' => 'Order.prepareDataRemoveFromQuote'
			];
		}

		if (!$paceQuoteId) {
			return [
				'success' => false,
				'code' => 'no-quote-id',
				'message' => 'No quote id was passed.',
				'from' => 'Order.prepareDataRemoveFromQuote'
			];
		}

		$turnaroundCode = $this->model->TurnaroundOption->convertToPaceCode($order['turnaround_option_id']);

		// Set up an array to pass into PACE
		$paceData = [
			'source' => $this->config['source'],
			'ipAddress' => $this->model->getIpAddress(),
			'quoteProduct' => $paceQuoteProductId,
			'quote' => $paceQuoteId,
			'turnaround' => $turnaroundCode,
			'delivery' => $order['delivery_option_id'],
		];

		return $paceData;
	}

	/**
	 * The getQuote function calls a new quote from Pace, using the Pace API:
	 * Calls getQuote - sets up an initial quote (one product only)
	 * Required keys:
	 * - source string - always “DOX”
	 * - productType string - product type ID (e.g. “002” for unbound)
	 * - copies int - number of copies of the product
	 * - pagesPerCopy int - number of pages in each copy
	 * - turnaround int - turnaround time in days, accepted values are 0, 1 and 2, where:
	 *   - 0 = same-day turnaround (50%  * markup)
	 *   - 1 = 1-day turnaround (25% markup)
	 *   - 2 = 2-day turnaround (no markup)
	 * - items [int) - array (comma-separated string also accepted) of quoteItemType IDs
	 *   for each of the items to be included within the product.
	 *   - Sides and Click categories are required (error thrown if not included).
	 *
	 */
	public function getQuote(Model $model, $data = [], $orderItemKey = 0) {

		if (!$data) {
			return [
				'success' => false,
				'code' => 'no-quote-data',
				'message' => 'No quote data was passed.',
				'from' => 'Order.getQuote'
			];
		}

		// Examine the passed in data, convert values to pace codes, update the data array and get ready to post to pace then extract the $data and $paceData variables
		$paceQuoteData = $this->prepareQuoteData($data, $orderItemKey);
		extract($paceQuoteData);

		$paceQuoteResult = $this->execute($functionName, $paceData);
		extract($paceQuoteResult);

		if (!$success) {

			return [
				'success' => false,
				'data' => $data,
				'response' => $response,
				'paceData' => $paceData,
				'code' => 'pace-response-null',
				'message' => 'There was an error retrieving your quote.',
				'functionName' => $functionName,
				'from' => 'Order.getQuote'
			];
		}

		if ($response['status'] === 0) {

			return [
				'success' => false,
				'data' => $data,
				'paceData' => $paceData,
				'response' => $response,
				'code' => 'pace-status-0',
				'message' => $response['message'],
				'functionName' => $functionName,
				'from' => 'Order.getQuote'
			];
		}

		if (empty($response['output']['products'])) {

			return [
				'success' => false,
				'data' => $data,
				'paceData' => $paceData,
				'response' => $response,
				'code' => 'pace-products-empty',
				'message' => 'There was an error retrieving a quote.',
				'functionName' => $functionName,
				'from' => 'Order.getQuote'
			];
		}

		return [
			'success' => true,
			'data' => $data,
			'paceData' => $paceData,
			'response' => $response,
			'functionName' => $functionName
		];

	}

	/**
	 * The removeFromQuote function removes a product from an existing quote (will remove the entire quote if it is the only product in the quote - Pace cannot handle a quote with no products, so we would have to start again with getQuote)
	 * Calls removeFromQuote - removes a product from an existing quote
	 * Required keys:
	 * - quote int - quote ID for the quote to be removed from (matches with order->pace_quote_id)
	 * - quoteProduct int - the individual item within a quote (matches with order_item->pace_quote_product_id)
	 */
	public function removeFromQuote(Model $model, $paceQuoteProductId = null, $paceQuoteId = null, $order = []) {

		if (!$paceQuoteProductId) {
			return [
				'success' => false,
				'code' => 'no-quote-product-id',
				'message' => 'No quote product id was passed.',
				'from' => 'Order.removeFromQuote'
			];
		}

		if (!$paceQuoteId) {
			return [
				'success' => false,
				'code' => 'no-quote-id',
				'message' => 'No quote id was passed.',
				'from' => 'Order.removeFromQuote'
			];
		}

		$functionName = $this->config['functions']['remove_from_quote'];

		$paceUrl = $this->apiUrl . $functionName;

		if (!$paceUrl) {
			return [
				'success' => false,
				'code' => 'no-pace-quote-url-add',
				'message' => 'The PACE quote URL is not configured.',
				'from' => 'Order.removeFromQuote'
			];
		}

		try {

			// Examine the passed in data, convert values to pace codes, update the data array and get ready to post to pace then extract the $data and $paceData variables
			$paceData = $this->prepareDataRemoveFromQuote($paceQuoteProductId, $paceQuoteId, $order);

			extract($this->execute('remove_from_quote', $paceData));

			if (!$response || !$result) {

				return [
					'success' => false,
					'response' => $response,
					'paceData' => $paceData,
					'code' => 'pace-response-null',
					'message' => 'There was an error removing your document.',
					'from' => 'Order.removeFromQuote'
				];
			}

			if ($response['status'] === 0) {

				return [
					'success' => false,
					'data' => $data,
					'paceData' => $paceData,
					'response' => $response,
					'code' => 'pace-status-0',
					'message' => $response['message'],
					'from' => 'Order.removeFromQuote'
				];
			}

			return [
				'success' => true
			];

		} catch (Exception $e) {

			return [
				'success' => false,
				'code' => 'pace-remove-quote-product-failed',
				'message' => $e->getMessage(),
				'from' => 'Order.removeFromQuote'
			];
		}
	}

	public function executePace(Model $model, $functionName = '', $paceData = []) {
		return $this->execute($functionName, $paceData);
	}

	private function execute($functionName = '', $paceData = []) {

		if (!$functionName) {
			return [
				'success' => false,
				'code' => 'no-function-name',
				'from' => 'Order.execute'
			];
		}

		$function = $this->config['functions'][$functionName];

		$paceUrl = $this->apiUrl . $function;

		if (!$paceUrl) {
			return [
				'success' => false,
				'code' => 'no-pace-url',
				'message' => 'The PACE URL is not configured.',
				'from' => 'Order.execute'
			];
		}

		$curl = curl_init();

		$config = [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_POST => 1,
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_URL => $paceUrl
		];

		if ($paceData) {
			$config[CURLOPT_POSTFIELDS] = $paceData;
		}

		curl_setopt_array(
			$curl,
			$config
		);

		$response = false;

		try {

			$response = curl_exec($curl);

		} catch (Exception $e) {

			curl_close($curl);

			return [
				'success' => false,
				'response' => [],
				'message' => $e->getMessage()
			];

		}

		curl_close($curl);
//		if ($functionName == 'convert_job_to_quote') {
//			die(debug($response));
//		}
		if (!$response) {

			return [
				'success' => false,
				'response' => []
			];
		}

		return [
			'success' => true,
			'response' => json_decode($response, true)
		];
	}
	/**
	source - string (always “DOX”) *
	email - string *
	billingFirstName - string (max 45) *
	billingLastName - string (max 45) *
	billingCompanyName - string (max 45)
	billingAddress1 - string (max 35) *
	billingAddress2 - string (max 35) *
	billingAddress3 - string (max 35) *
	billingCity - string (max 35) *
	billingCounty - string
	billingPostCode - string (max 10) *
	billingCountryID - int *
	useBillingForShipping - int (0 or 1) *
	shippingFirstName - string (max 45)
	shippingLastName - string (max 45)
	shippingCompanyName - string (max 45)
	shippingAddress1 - string (max 35)
	shippingAddress2 - string (max 35)
	shippingAddress3 - string (max 35)
	shippingCity - string (max 35)
	shippingCounty - string
	shippingCountryID - int

	Required fields are marked with *. Essentially, the billing address is all required apart from company name and county, which are both optional. If the shipping and billing addresses are the same, useBillingForShipping should be 1, and you then don’t need to pass the shippingXYZ values.

This returns:

status - int 1 on success, 0 on failure
message - string only present on failure
customer - array
- customer_id - int customer ID to be passed to the quote for job conversion
- billing_contact_id - int contact ID for billing
- shipping_contact_id - int contact ID for shipping
execution_time - float time the function took to execute
*/

	public function createCustomer(Model $model, $user = []) {

		$data = [
			'source' => $this->config['source'],
			'email' => $user['User']['username'],
			'billingFirstName' => $user['User']['first_name'],
			'billingLastName' => $user['User']['last_name'],
			'billingCompanyName' => $user['BillingAddress']['company_name'],
			'billingAddress1' => $user['BillingAddress']['address_1'],
			'billingAddress2' => $user['BillingAddress']['address_2'],
			'billingAddress3' => $user['BillingAddress']['address_3'],
			'billingCity' => $user['BillingAddress']['city'],
			'billingCounty' => $user['BillingAddress']['county'],
			'billingPostCode' => $user['BillingAddress']['post_code'],
			'billingCountryID' => $user['BillingAddress']['country_id'],
			'useBillingForShipping' => $user['BillingAddress']['billing_same_as_shipping']
		];

		if (!empty($user['ShippingAddress'])) {

			$data = array_merge(
				$data,
				[
					'shippingFirstName' => $user['User']['first_name'],
					'shippingLastName' => $user['User']['last_name'],
					'shippingCompanyName' => $user['ShippingAddress']['company_name'],
					'shippingAddress1' => $user['ShippingAddress']['address_1'],
					'shippingAddress2' => $user['ShippingAddress']['address_2'],
					'shippingAddress3' => $user['ShippingAddress']['address_3'],
					'shippingCity' => $user['ShippingAddress']['city'],
					'shippingCounty' => $user['ShippingAddress']['county'],
					'shippingCountryID' => $user['ShippingAddress']['country_id']
				]
			);
		}

		return $this->execute('create_customer', $data);

	}


	/**
	convertQuoteToJob

quote - int quote ID
billingContact - int contact ID for billing
shippingContact - int contact ID for shipping
promiseDate - string YYYY-MM-DD format
shippingDate - string YYYY-MM-DD format
contentFiles - string JSON-encoded array, split by Quote Product ID - example: [{“QUOTEPRODUCTID":[{"filename”:"FILENAME","pages”:NUMPAGES,"fileUrl”:"FILEURL"}]},{“QUOTEPRODUCTID":[{"filename”:"FILENAME","pages”:NUMPAGES,"fileUrl”:"FILEURL"}]},{“QUOTEPRODUCTID":[{"filename”:"FILENAME","pages”:NUMPAGES,"fileUrl”:"FILEURL"}]}] - where the capitalised strings are self-explanatory replacements

All fields are required.

The function returns a JSON array:

status - int (0 or 1), 0 on error, 1 on success
message - string only if status = 0
job - int Pace Job number, only if status = 1
execution_time - float Time to run conversion process in seconds
*/
	public function convertQuoteToJob(Model $model, $data = []) {

		$response = $this->execute('convert_job_to_quote', $data);

		return $response;

	}

	public function setPaceJobStatus (Model $model, $paceJobId = null, $statusCode = '') {

		if (empty($this->config['status_codes'][$statusCode])) {
			return [
				'success' => false,
				'code' => 'invalid_status_code',
				'status_code' => $statusCode
			];
		}

		$paceStatusCode = $this->config['status_codes'][$statusCode];

		$data = [
			'job' => $paceJobId,
			'statusCode' => $paceStatusCode
		];

		$result = $this->execute('change_job_status', $data);

		return $result;

	}

}
