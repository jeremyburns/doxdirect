<?php
App::uses('ModelBehavior', 'Model');
App::uses('Folder', 'Utility');

/**
 *	/etc/callas/pdfToolbox --dist --endpoint=172.31.12.205 --topdf /var/www/doxdirect/app/webroot/files/test/word.docx
 *
 *
 *	To start a dispatcher:
 *
 *	cd /etc/callas
 *	sudo /etc/callas/pdfToolbox dispatcher --dispatcher
 *
 *	To start a satellite
 *	cd c:\Program Files\callas pdfToolbox Server 7
 *	pdfToolbox.exe --satellite --endpoint=172.31.12.205
*/

/**
 * Behavior to interract with Callas on either the server based hot folders (dev) or the api based command line (prod)
 */
class CallasBehavior extends ModelBehavior {

	public function setup(Model $Model, $settings = array()) {

		Configure::load('callas_config', 'default');

		$this->callas_method = Configure::read('callas.method');

		$this->config = Configure::read('callas.' . $this->callas_method);

	}

	public function callasMethod() {

		return $this->callas_method;

	}

	/**
 * This function sends a file to Callas. It chooses the right method; api when it's available, or server when it's not.
 * The api version processes everything on request.
 * The server version submits the file to a hot folder, so the completion time is indeterminate.
 * In both cases there is a try and wait loop in order to let the process complete.
 */
	public function sendToCallas(Model $model, $orderItemFile = [], $orderId = null) {

		$orderItemFileId = $orderItemFile['OrderItemFile']['id'];

		if (!empty($orderItemFile['OrderItemFile']['callas_filename']) && !empty($orderItemFile['OrderItemFile']['file_name_without_extension'])) {

			$callasFileName = $orderItemFile['OrderItemFile']['callas_filename'];
			$callasFileNameWithoutExtension = $orderItemFile['OrderItemFile']['file_name_without_extension'];

		} else {

			$result = $model->setCallasFilename($orderItemFileId);

			if (!$result['success']) {

				return [
					'success' => false,
					'code' => 'failed-to-set-callas-file-name'
				];

			}

			$orderItemFile = $model->getOrderItemFile($orderItemFileId);

			$callasFileName = $result['callas_filename'];
			$callasFileNameWithoutExtension = $result['file_name_without_extension'];

		}

		if ($this->callasMethod() === 'server') {

			$response = $this->serverSendToCallas($orderItemFile);

		} else {

			$response = $this->apiSendToCallas($orderItemFile);

//			$orderItemFolder = $orderItemFile['OrderItemFile']['path_root'];
//
//			$response = $this->getCallasXml($model, $orderItemFile);
//
//			$callasXml = $response['xml'];
//
//			if ($response['source'] == 'callas') {
//				$model->saveCallasInfoFile($orderItemFileId, $callasXml);
//			}
//
//			// Now process the pages
//			$result = $model->extractPages($orderItemFileId, $callasXml);
//
//			if (!$result['success']) {
//				return $result;
//			}
//
//			$makePreviews = $this->apiMakePreviews($model, $orderItemFile, $callasXml);
//
//			$response['preview_page_ids'] = $makePreviews['preview_page_ids'];
//
////			return array(
////				'success' => true,
////				'response' => $makePreviews,
////				'preview_page_ids' => $makePreviews['preview_page_ids']
////			);

		}

		if (!$result['success']) {

			$data['message'] = 'There was a problem processing your file.';

		} else {

			$data = [
				'success' => $response['success'],
				'sent_to_callas' => date('Y-m-d H:i:s'),
				'callas_filename' => $callasFileName,
//				'callas_info' => json_encode($callasXml),
				'callas_method' => $this->callas_method,
				'message' => 'The file was successfully uploaded.'
			];

		}

		$response['data'] = $data;

		return $response;

	}

	private function apiCallasPath() {

		return $this->config['path'];

	}

	private function apiCallasDespatcherIP() {

		if($this->config['despatch'] === true) {
			return $this->config['despatcher_ip'];
		}
		else {
			return '';
		}

	}

	private function apiCallasProfilesFolder() {

		$profilesFolder = $this->config['profiles_folder'];

		if ($profilesFolder) {
			return $profilesFolder . DS;
		}

		return '';

	}

	private function apiCallasCmd($despatch = true) {

		$callasPath = $this->apiCallasPath();

		$apiCallasCmd = $callasPath . 'pdfToolbox';

		if ($despatch) {
			$callasDespatcherIp = $this->apiCallasDespatcherIP();

			if ($callasDespatcherIp) {
				$apiCallasCmd .= ' --dist --endpoint=' . $callasDespatcherIp;
			}
		}

		return $apiCallasCmd;

	}

	public function callasExecute(Model $model, $command) {

		return $this->apiCallasExecute($command);

	}

	private function apiCallasExecute($command) {

		$callasExecutable = $this->apiCallasCmd();

		$callasCmd = $callasExecutable . ' ' . $command;

		$this->log("CMD: $callasCmd", 'debug');

		$result = exec($callasCmd);

		return $result;

	}

	private function apiSendToCallas($orderItemFile = []) {

		if (!$orderItemFile) {
			return [
				'success' => false,
				'code' => 'no-order-item-file',
				'from' => 'CallaseBehavior.apiSendToCallas'
			];
		}

		$result = $this->apiCallasPdfDetails($orderItemFile);

		$result['success'] = true;

		return $result;

	}

	public function getFileNameWithoutExtension(Model $model, $fileName = '') {

		$fileNameWithOutExtension = $model->stripExtension($fileName);
		$fileNameWithOutExtension = str_replace(' ', '_', $fileNameWithOutExtension);

		return $fileNameWithOutExtension;

	}

	private function apiCallasGetOrderItemFolder($orderItemFile = [], $fileNameWithOutExtension = '') {

		if (empty($orderItemFile['OrderItem']['Order']['pace_quote_id'])) {
			return [
				'success' => false,
				'code' => 'no-pace-quote-id',
				'from' => 'CallasBehaviour.apiCallasPdfDetails'
			];
		}

		$paceQuoteId = $orderItemFile['OrderItem']['Order']['pace_quote_id'];

		if (empty($orderItemFile['OrderItem']['pace_quote_product_id'])) {
			return [
				'success' => false,
				'code' => 'no-pace-quote-product-id',
				'from' => 'CallasBehaviour.apiCallasPdfDetails'
			];
		}

		$paceQuoteProductId = $orderItemFile['OrderItem']['pace_quote_product_id'];

		$orderItemFolder = $this->localFilesPath() . $paceQuoteId . DS . $paceQuoteProductId . DS . $fileNameWithOutExtension;

		return [
			'success' => true,
			'orderItemFolder' => $orderItemFolder
		];

	}

	private function apiCallasPdfDetails($orderItemFile = array()) {

		// /etc/callas/pdfToolbox
		// /etc/callas/pdf_details.kfpx
		// -report=XML,ALWAYS,ALL,PATH=/var/www/doxdirect/app/webroot/files/test/profile.xml
		// /var/www/doxdirect/app/webroot/files/test/test_2_page_doc.pdf

		$callasFileName = $orderItemFile['OrderItemFile']['callas_filename'];
		$fileNameWithOutExtension = $orderItemFile['OrderItemFile']['file_name_without_extension'];
		$orderItemFolder = $orderItemFile['OrderItemFile']['path_root'];

		if (!$orderItemFolder) {
			return [
				'success' => false,
				'code' => 'no-path-root',
				'from' => 'CallasBehaviour.apiCallasPdfDetails'
			];
		}

		$xmlFileName = $fileNameWithOutExtension . '.xml';

		$originalFilePath = $orderItemFolder . DS . 'original' . DS . $orderItemFile['OrderItemFile']['callas_filename'];

		$profilesFolder = $this->apiCallasProfilesFolder();

		$callasPath = $this->apiCallasPath();

		// Original profile was pdf_details.kfpx
		$command = $callasPath . $profilesFolder . 'checkcloseobjects.kfpx --overwrite --report=XML,ALWAYS,ALL,PATH=' . $orderItemFolder . DS . $xmlFileName . ' ' . $originalFilePath;

		$result = $this->apiCallasExecute($command);

		return [
			'result' => $result,
			'xmlFileName' => $xmlFileName
		];

	}

	public function makePreviews(Model $model, $orderItemFile = [], $callasXml = []) {

		if ($this->callasMethod() !== 'api') {
			return [
				'success' => true,
				'code' => 'not-api'
			];
		}

		$result = $this->apiMakePreviews($model, $orderItemFile, $callasXml);

		return $result;

	}

//	private function apiMakePreviews($orderItemFile, $previewResolution = null, $pageRange = null, $orderItemFolderName = '') {

	private function apiMakePreviews(Model $model, $orderItemFile = [], $callasXml = [], $pageRange = null) {

		if (!$callasXml) {
			$callasXml = json_decode($orderItemFile['OrderItemFile']['callas_info'], true);
		}

		if (empty($callasXml['report']['document']['pages']['page'])) {
			return [
				'success' => false,
				'code' => 'no-page-key-in-xml-file',
				'from' => 'CallasBehaviour.apiSendToCallas'
			];
		}

		$pages = $callasXml['report']['document']['pages']['page'];

		if (isset($pages[0])) {
			$pageCount = count($pages);
		} else {
			$pageCount = 1;
		}

		$pageLimit = $this->config['preview_limit'];
		$previewPageIds = array();

		if ($pageCount > $pageLimit) {

			$pageRange = '1-' . $pageLimit;

			// Fill up a small array with the page numbers that have had their previews generated
			for ($n = 1; $n <= $pageLimit; $n++) {
				$previewPageIds[] = $n;
			}

		} else {

			$pageRange = null;

			// Fill up a small array with the page numbers that have had their previews generated
			for ($n = 1; $n <= $pageCount; $n++) {
				$previewPageIds[] = $n;
			}
		}

		$orderItemFolderName = $orderItemFile['OrderItemFile']['path_root'];

		$callasPath = $this->apiCallasPath();

		$previewResolution = $this->config['preview_resolution'];

		$previewFolderName = $orderItemFolderName . DS . 'preview';

		$previewFolderExists = $model->folderExists($previewFolderName, true, 0777);

		if (!$previewFolderExists['success']) {
			return $previewFolderExists;
		}

		$originalFileName = $orderItemFolderName . DS . 'original' . DS . $orderItemFile['OrderItemFile']['name'];

		$pageRangeCommand = '';

		// If we are specifying certain pages fill in the pageRangeCommand
		if ($pageRange) {
			$pageRangeCommand = ' --pagerange=' . $pageRange . ' ';
		}

		$command = '--saveasimg --overwrite --imgformat=PNG --resolution=' . $previewResolution . $pageRangeCommand . ' --outputfolder=' . $previewFolderName . ' ' . $originalFileName;

		$result = $this->apiCallasExecute($command);

		return [
			'success' => true,
			'response' => $result,
			'preview_page_ids' => $previewPageIds
		];

	}

	/**
 * The completePreviews function fills in any missing preview files not created when the file was originally passed to Callas
 * No point doing it for server callas as they are generally done at outset
*/
	public function completePreviews(Model $model, $orderItemFile = array()) {

		if (!$orderItemFile) {
			return false;
		}

		$previewPageIds = array();

		if ($this->callas_method == 'api') {

			foreach ($orderItemFile['OrderItemFile']['OrderItemFilePage'] as $pageKey => $page) {

				if (!$page['has_preview']) {

					if (!$this->fileExists($page['preview_image_path'])) {
						$previewPageIds[$page['id']] = $page['callas_nr'];
					}

				}

			}

			if ($previewPageIds) {

				$pageRange = implode(',', $previewPageIds);

				$result = $this->apiMakePreviews($model, $orderItemFile, null, $pageRange);

			}

		}

		return array(
			'success' => true,
			'preview_page_ids' => $previewPageIds
		);

	}

	private function apiGetCallasXmlFile($orderItemFile = []) {

		$fileNameWithOutExtension = $orderItemFile['OrderItemFile']['file_name_without_extension'];

		$orderItemFolder = $orderItemFile['OrderItemFile']['path_root'];

		$xmlFileName = $orderItemFolder . DS . $fileNameWithOutExtension . '.xml';

		$fileExists = false;

		try {
			$file = new File($xmlFileName, false);
		} catch (Exception $e) {

			return array(
				'success' => false,
				'code' => 'no-callas-info-file',
				'path' => $xmlFileName,
				'message' => $e->getMessage()
			);

		}

		$try = 1;

		while ($try <= $this->config['retryCount']) {

			$fileExists = $file->exists($xmlFileName);

			if (!$fileExists) {
				sleep($this->config['sleepTime']);
			} else {
				break;
			}

			$try++;

		}

		if (!$fileExists) {

			return array(
				'success' => false,
				'code' => 'no-callas-info-file',
				'path' => $xmlFileName
			);

		}

		return array(
			'success' => true,
			'file_path' => $xmlFileName
		);

	}

	private function serverCallasExecute($callasUrl = null, $data = []) {

		$curl = curl_init();

		$options = [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_POST => 1,
			CURLOPT_URL => $callasUrl
		];

		if ($data) {
			$options[CURLOPT_POSTFIELDS] = $data;
		}

		curl_setopt_array(
			$curl,
			$options
		);

		$response = curl_exec($curl);

		curl_close($curl);

		return $response;

	}

	/**
 * This function simply submits the file associated with this record to Callas.
 * There is only a response of 'Done' from Callas as it places the file into one or more hot folders defined in the $actions input array.
 * Callas will process them in due course.
 * The results of that processing are retrievable from getCallasInfo
 */
	private function serverSendToCallas($orderItemFile = array()) {

		$callasActions = $this->config['actions'];

		$callasPostUrl = $this->config['url'] . $this->config['function_post'];

		$callasFileName = $orderItemFile['OrderItemFile']['callas_filename'];

		$originalFile = $orderItemFile['OrderItemFile']['path_root'] . DS . 'original' . DS . $orderItemFile['OrderItemFile']['name'];

		$data = array(
			'file_name' => $callasFileName,
			'file' => base64_encode(file_get_contents($originalFile)),
			'actions' => $callasActions
		);

		$response = $this->serverCallasExecute($callasPostUrl, $data);

		$response = json_decode($response, true);

		if ($response == 'Done') {

			$response = array(
				'success' => true,
				'response' => $response
			);

		} else {

			$response = array(
				'success' => false,
				'response' => $response
			);

		}

		return $response;

	}

	private function fileExists($callasFilePath) {

		App::uses('HttpSocket', 'Network/Http');

		$http = new HttpSocket();

		$response = $http->get($callasFilePath);

		$fileExists = $response->code == '200';

		return $fileExists;

	}

	private function serverGetCallasXmlFile(Model $model, $orderItemFile = array()) {

		$callasInfoUrl = $this->config['url'] . $this->config['folder_pdf_info'];

		$fileName = $model->stripExtension($orderItemFile['OrderItemFile']['callas_filename']);

		$fileName .= '.xml';

		$callasFilePath = $callasInfoUrl . $fileName;

		$fileExists = false;

		$try = 1;

		while ($try <= $this->config['retryCount']) {

			$fileExists = $this->fileExists($callasFilePath);

			if (!$fileExists) {

				sleep($this->config['sleepTime']);

			} else {

				break;

			}

			$try++;

		}

		if (!$fileExists) {

			return array(
				'success' => false,
				'code' => 'no-callas-info-file',
				'path' => $callasFilePath
			);

		}

		return array(
			'success' => true,
			'file_path' => $callasFilePath
		);

	}

	public function getCallasXml(Model $model, $orderItemFile = []) {

		if (!empty($orderItemFile['OrderItemFile']['callas_info'])) {

			$xml = json_decode($orderItemFile['OrderItemFile']['callas_info'], true);

			return [
				'success' => true,
				'xml' => $xml,
				'source' => 'database'
			];
		}

		if ($this->callasMethod() == 'server') {

			$response = $this->serverGetCallasXmlFile($model, $orderItemFile);

		} else {

			$response = $this->apiGetCallasXmlFile($orderItemFile);

		}

		$response['source'] = 'callas';

		if (!$response['success']) {
			return $response;
		}

		$callasFilePath = $response['file_path'];

		try {

			$xml = simplexml_load_file($callasFilePath);

			$xml = Xml::toArray($xml);

			return array(
				'success' => true,
				'xml' => $xml,
				'source' => 'callas'
			);

		} catch (Exception $e) {

			return array(
				'success' => false,
				'code' => 'caught-excpetion',
				'message' => $e->getMessage(),
				'source' => 'callas'
			);

		}

	}

	public function getPreviewPath(Model $model, $fileNameWithOutExtension = null, $pageNumber = null, $orderId = null, $previewPath = null) {

		if (!$fileNameWithOutExtension || !$pageNumber) {
			return '';
		}

		if ($this->callasMethod() == 'server') {

			$callasPreviewPath = $this->config['url'] . $this->config['folder_png'];

			return $callasPreviewPath . DS . $fileNameWithOutExtension . '_' . $pageNumber . '.png';

		} else {

			return $previewPath . DS . $fileNameWithOutExtension . '_' . $pageNumber . '.png';

			// return $this->localFilesPath(false) . $orderId . DS . 'previews' . DS . $fileNameWithOutExtension . '_' . $pageNumber . '.png';

		}

	}

	public function localFilesPath(Model $model, $includeRoot = true) {

		if ($includeRoot) {
			return WWW_ROOT . 'files' . DS;
		} else {
			return 'files' . DS;
		}

	}

	public function makeCallasFileName(Model $model, $orderId = null, $fileName = null) {

		if (!$orderId || !$fileName) {
			return false;
		}

		$fileName = str_replace(' ', '_', $fileName);

		if ($this->callasMethod() === 'server') {

			$timestamp = $model->getTimestamp(['format' => true, 'formatString' => 'Y-m-d-H-i-s']);
			$callasFileName = $timestamp . '-' . $orderId . '-' . $fileName;

		} else {

			$callasFileName = $fileName;

		}

		return $callasFileName;

	}

	public function extractCoverPages(Model $model, $orderItem, $frontCoverOptions = null, $backCoverOptions = null, $frontOrderItemFile = null, $backOrderItemFile = null) {

		$frontCover = null;
		$backCover = null;

		if($frontCoverOptions == "use_and_discard") {

			if($this->callas_method == "server") {
				$frontCover = $this->serverExtractCover($model, $frontOrderItemFile, "first", 1);
			}
			else {
				$frontCover = $this->apiExtractCover($model, $frontOrderItemFile, "first", 1);
			}


		}

		if($backCoverOptions == "use_and_discard") {

			if($this->callas_method == "server") {
				$backCover = $this->serverExtractCover($model, $backOrderItemFile, "last", -1);
			}
			else {
				$backCover = $this->apiExtractCover($model, $backOrderItemFile, "last", -1);
			}

		}

		return array('front' => $frontCover, 'back' => $backCover);

	}

	private function serverExtractCover(Model $model, $orderItemFile = array(), $side = null, $page = null) {

		if(!$side) {
			return false;
		}

		App::uses('File', 'Utility');

		$callasPostUrl = $this->config['url'] . $this->config['function_post'];

		$callasCoverUrl = $this->config['url'] . $this->config['folder_extract_' . $side];

		$filePath = $orderItemFile['OrderItemFile']['path_root'] . DS . 'original' . DS . $orderItemFile['OrderItemFile']['name'];
		$fileName = $orderItemFile['OrderItemFile']['callas_filename'];
		$fileDotPosition = strrpos($fileName, '.');

		if($side == "first") {
			$endFileName = substr($fileName, 0, $fileDotPosition) . '_0001.pdf';
		}
		else {
			$lastPageNumber = $orderItemFile['OrderItemFile']['count_order_item_file_page'];
			$endFileName = substr($fileName, 0, $fileDotPosition) . '_' . str_pad($lastPageNumber, 4, '0', STR_PAD_LEFT) . '.pdf';
		}

		$curl_data = array(
			'file_name' => $fileName,
			'file' => base64_encode(file_get_contents($filePath)),
			'actions' => 'extract_' . $side . '_page'
		);

		$curl = curl_init();
		curl_setopt_array(
			$curl,
			[
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_POST => 1,
				CURLOPT_URL => $callasPostUrl,
				CURLOPT_POSTFIELDS => $curl_data
			]
		);
		$response = curl_exec($curl);
		curl_close($curl);

		$response = json_decode($response, true);

		if($response == "Done") {

			// If it's been successfully posted, now download the file
			$endFilePath = $callasCoverUrl . '/' . $endFileName;
			$fileExists = false;
			$try = 1;

			while($try <= $this->config['retryCount']) {

				$fileExists = $this->fileExists($endFilePath);

				if(!$fileExists) {
					sleep($this->config['sleepTime']);
				}
				else {
					break;
				}

				$try++;

			}

			if(!$fileExists) {
				// Can't find the file
			}
			else {

				$localFilePath = $orderItemFile['OrderItemFile']['path_root'] . DS . "cover_designer" . DS . $endFileName;
				//$logFile = new File("/www/dox_cake/app/webroot/files/logfile");
				//$logFile->open('a');
				//$result = $logFile->write("FILE DATA: $localFilePath\n", "a");
				//$logFile->close();
				$data = file_get_contents($endFilePath);
				$file = new File($localFilePath, true);
				$file->open('w');
				$result = $file->write($data);
				$file->close();
				return $localFilePath;
			}

		}
		else {
			return false;
		}

	}

	private function apiExtractCover(Model $model, $orderItemFile = array(), $side = null, $page = null) {

		if(!$side) {
			return false;
		}

		$filePath = $orderItemFile['OrderItemFile']['path_root'] . DS . 'original' . DS . $orderItemFile['OrderItemFile']['name'];
		$fileName = $orderItemFile['OrderItemFile']['callas_filename'];
		$fileDotPosition = strrpos($fileName, '.');

		if($side == "first") {
			$endFileName = substr($fileName, 0, $fileDotPosition) . '_0001.pdf';
			$pageRange = "1";
		}
		else {
			$lastPageNumber = $orderItemFile['OrderItemFile']['count_order_item_file_page'];
			$endFileName = substr($fileName, 0, $fileDotPosition) . '_' . str_pad($lastPageNumber, 4, '0', STR_PAD_LEFT) . '.pdf';
			$pageRange = $lastPageNumber;
		}

		$outputFilePath = $orderItemFile['OrderItemFile']['path_root'] . DS . 'cover_designer' . DS . $endFileName;

		// Need to get the order ID here to go in pdfPath call
		$orderId = $model->field('order_id', array('OrderItem.id' => $orderItemFile['OrderItemFile']['order_item_id']));
		$command = '--splitpdf --splitscheme=' . $pageRange . ' --outputfile=' . $outputFilePath . ' ' . $filePath;

		$result = $this->apiCallasExecute($command);
		//$this->log($outputFilePath, 'debug');
		if(strpos($result, 'Duration') !== false) return $outputFilePath;

	}

	public function runProfile(Model $model, $profileName = null, $finalFilePath = null, $parameters = null) {

		if ($this->callasMethod() === 'server') {

			$callasPostUrl = $this->config['url'] . $this->config['function_post'];

			$data = array(
				'file_name' => $finalFilePath,
				'file' => base64_encode(file_get_contents($finalFilePath)),
				'actions' => $profileName
			);

			$response = $this->serverCallasExecute($callasPostUrl, $data);

		} else {

			$profilesFolder = $this->apiCallasProfilesFolder();

			$callasPath = $this->apiCallasPath();

			if (!empty($parameters)) {
				$profileName .= ' ' . $parameters;
			}

			$command = $callasPath . $profilesFolder . $profileName . ' ' . $finalFilePath . ' ' . $finalFilePath;

			$result = $this->apiCallasExecute($command);

			return $result;

		}

		/*
		sudo
		/etc/callas/pdfToolbox
		/etc/callas/scale_pages_to_a4cutsheet_205x202.kfpx
		--outputfile=/var/www/doxdirect/app/webroot/files/55379cde-c8bc-4d1a-8cf6-1778ac1f0ccd/a5new.pdf 				/var/www/doxdirect/app/webroot/files/55379cde-c8bc-4d1a-8cf6-1778ac1f0ccd/a5.pdf
		*/

	}

}
