<!--
when empty

	-> set up empty keys

when adding quote (saving)

	-> set order key

		if order item

			-> set the order item key


when changing a quote

	-> nothing


when adding an order item

	-> set the order item key


when adding an order item file

	-> set the order item file key


when removing an order item

	-> set the order item key to 0


when removing an order item file

	-> set the order item file key to 0
-->
<?php
App::uses('ModelBehavior', 'Model');

/**
 * Behavior to manage the order keys for the current user
 * In other words, which order, order item, order item file and order tab is he/she working on
 */
class OrderKeyBehavior extends ModelBehavior {

	public function setup(Model $Model, $settings = array()) {

		$this->modelAlias = $Model->alias;

	}

	private $defaults = array(
		'Order' => array(
			'id' => null
		),
		'OrderItem' => array(
			'key' => null,
			'id' => null
		),
		'OrderItemFile' => array(
			'key' => null,
			'id' => null
		),
		'OrderItemTab' => array(
			'key' => null,
			'id' => null
		)
	);

	public function orderKeysStart(Model $model, $orderId = null) {

		$orderKeys = $this->defaults;

		if (!$orderId) {
			return $orderKeys;
		}

		$orderKeys['Order']['id'] = $orderId;

		$order = $model->find(
			'first',
			array(
				'conditions' => array(
					'Order.id' => $orderId
				),
				'fields' => array(
					'Order.id',
					'Order.count_order_item'
				),
				'contain' => array(
					'OrderItem' => array(
						'fields' => array(
							'OrderItem.id',
							'OrderItem.count_order_item_file',
							'OrderItem.count_order_item_tab'
						),
						'OrderItemFile' => array(
							'fields' => array(
								'OrderItemFile.id',
								'OrderItemFile.count_order_item_file_page'
							),
							'OrderItemFilePage.id'
						),
						'OrderItemTab.id'
					)
				)
			)
		);

		if (empty($order['OrderItem'])) {
			return $orderKeys;
		}

		// This loop will leave the values set at the last key
		foreach($order['OrderItem'] as $orderItemKey => $orderItem) {
			$orderItemId = $orderItem['id'];
			$orderItemFile = $orderItem['OrderItemFile'];
			$orderItemTab = $orderItem['OrderItemTab'];
		}

		$orderKeys['OrderItem'] = array(
			'key' => $orderItemKey,
			'id' => $orderItemId
		);

		if (!empty($orderItemFile)) {
			// This loop will leave the values set at the last key
			foreach($orderItemFile as $orderItemFileKey => $orderItemFile) {
				$orderItemFileId = $orderItemFile['id'];
			}

			$orderKeys['OrderItemFile'] = array(
				'key' => $orderItemFileKey,
				'id' => $orderItemFileId
			);

		}

		if (!empty($orderItemTab)) {
			// This loop will leave the values set at the last key
			foreach($orderItemTab as $orderItemTabKey => $orderItemTab) {
				$orderItemTabId = $orderItemTab['id'];
			}

			$orderKeys['OrderItemTab'] = array(
				'key' => $orderItemTabKey,
				'id' => $orderItemTabId
			);

		}

		return $orderKeys;

	}

	public function orderKeys(Model $model, $orderId = null, $currentValues = array(), $newValues = array(), $code = null) {



		if (!$orderId) {
			return $defaults;
		}

		$currentValues = array_merge(
			$defaults,
			$currentValues
		);


		if (!empty($code)) {

			switch ($code) {

				case 'get-last-order-item':

					$orderItemKeys = Hash::extract($orderKeys, 'OrderItem.{n}.id');

					if ($orderItemKeys) {

						// This loop will leave the values set at the last key
						foreach($orderItemKeys as $orderItemKey => $orderItemId) {
						}

						$currentValues['OrderItem'] = array(
							'key' => $orderItemKey,
							'id' => $orderItemId
						);

					}

					break;

			}

		}

		return $orderKeys;

		if (!$orderKeys) {
			return array();
		}

		if (!isset($orderItemKey)) {

			$orderItemCount = count($order['OrderItem']);

			if ($orderKeys['Order']['count_order_item'] === 1) {
				$orderItemKey = 0;
			}

		}

		$orderKeys['OrderItemKey'] = $orderItemKey;

		if (!empty($order['OrderItem'][$orderItemKey]['id'])) {
			$orderItemId = $order['OrderItem'][$orderItemKey]['id'];
			$orderKeys['OrderItemId'] = $orderItemId;
		}

		if (!isset($orderItemFileKey) && !empty($order['OrderItem'][$orderItemKey]['OrderItemFile'])) {

			$orderItemFileCount = count($order['OrderItem'][$orderItemKey]['OrderItemFile']);

			if ($orderItemFileCount === 1) {
				$orderItemFileKey = 0;
			}

		}

		if (isset($orderItemFileKey)) {
			$orderKeys['OrderItemFileKey'] = $orderItemFileKey;
			$orderItemFileId = $order['OrderItem'][$orderItemKey]['OrderItemFile'][$orderItemFileKey]['id'];
			$orderKeys['OrderItemFileId'] = $orderItemFileId;
		}

		return $orderKeys;

	}



}
