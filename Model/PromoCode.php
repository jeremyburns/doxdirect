<?php
App::uses('AppModel', 'Model');
App::uses('CakeTime', 'Utility');
App::uses('CakeNumber', 'Utility');

/**
 * PromoCode Model
 *
 * @property Order $Order
 */
class PromoCode extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'Order' => [
			'className' => 'Order',
			'foreignKey' => 'promo_code_id',
			'dependent' => false,
			'counterCache' => 'count_order'
		]
	];

	public function beforeSave($options = []) {

		if ($this->data['PromoCode']['user_id'] == '') {
			$this->data['PromoCode']['user_id'] = null;
		}

		return parent::beforeSave();

	}

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = [
		'code' => [
			'required' => [
				'rule' => 'notBlank',
				'message' => 'Please specify a promo code.',
				'last' => true
			],
			'unique' => [
				'rule' => 'isUnique',
				'message' => 'That promo code already exists.',
				'last' => true
			],
			'maxLength' => [
				'rule' => ['maxLength', 20],
				'message' => 'Promo code cannot contain more than 20 characters.'
			]
		],
		'description' => [
			'rule' => ['maxLength', 255],
			'message' => 'Description cannot be longer than 255 characters.'
		],
		'apply_discount_to' => [
			'required' => [
				'rule' => 'notBlank',
				'message' => 'Please specify the amount the discount should apply to.',
				'last' => true
			]
		],
		'discount_type' => [
			'required' => [
				'rule' => 'notBlank',
				'message' => 'Please specify the amount the discount type.',
				'last' => true
			]
		]
	];

	public function discountTypes() {

		return [
			'amount' => 'Amount',
			'percent' => 'Percent'
		];

	}

	public function applyDiscountToFieldNames() {

		return [
			'amount_net' => 'Printing cost',
			'amount_delivery' => 'Delivery cost',
			'amount_turnaround' => 'Turnaround cost',
			'amount_vat' => 'VAT',
			'amount_total' => 'Total cost'
		];

	}

	public function add($data = []) {

// 		$this->unsetEmptyDiscounts($data);

		$this->create();

		return $this->save($data);

	}

	public function getDiscount($order = []) {

		if (!$order) {
			return [
				'success' => false,
				'code' => 'invalid-order',
				'message' => 'That is an invalid order.'
			];
		}

		if (empty($order['Order']['promo_code_id'])) {

			return [
				'success' => false,
				'code' => 'no-promo-code-id'
			];
		}

		$promoCode = $this->findById($order['Order']['promo_code_id']);

		if (!$promoCode) {

			return [
				'success' => false,
				'code' => 'invalid-promo-code'
			];

		}

		$validCheck = $this->validCheck($promoCode, $order);

		if (!$validCheck['success']) {
			return $validCheck;
		}

		$discount = $this->calculateDiscount($promoCode, $order);

		$discount = array_merge(
			$discount,
			[
				'success' => true,
				'promo_code_id' => $promoCode['PromoCode']['id'],
				'code' => $promoCode['PromoCode']['code']
			]
		);

		return $discount;

	}

/**
	* The validCheck function checks that the promo code against a number of rules.
	* @param array $promoCode the array containing the promo code
	* @param array $order the array containing the order
	*
	* @return TRUE if is enabled, else false.
 */
	private function validCheck($promoCode = [], $order = []) {

		if (!$this->checkIsEnabled($promoCode)) {

			return [
				'success' => false,
				'code' => 'invalid-promo-code',
				'message' => 'That is an inactive promo code.'
			];

		}

		$promoCodeId = $promoCode['PromoCode']['id'];

		$validTo = $promoCode['PromoCode']['valid_to'];

		if ($this->checkHasExpired($validTo)) {

			return [
				'success' => false,
				'code' => 'expired-promo-code',
				'message' => 'That promo code expired on ' . CakeTime::format($validTo, '%A %d %B') . '.'
			];

		}

		$validFrom = $promoCode['PromoCode']['valid_from'];

		if ($this->checkNotYetValid($validFrom)) {

			return [
				'success' => false,
				'code' => 'promo-code-not-valid',
				'message' => 'That promo code is not valid until ' . CakeTime::format($validFrom, '%A %d %B') . '.'
			];

		}

		if (!empty($order['Order']['user_id'])) {
			$userLimitCheck = $this->checkLimitPerUser(
				$promoCode['PromoCode']['limit_per_user'],
				$promoCodeId,
				$order['Order']['user_id']
			);

			if (!$userLimitCheck) {

				return [
					'success' => false,
					'code' => 'limit-reached',
					'message' => 'You have already used that promo code as many times as you can.'
				];

			}

		}

		if (!$this->checkLimitTotal($promoCode['PromoCode']['limit_total'], $promoCodeId)) {

			return [
				'success' => false,
				'code' => 'limit-reached',
				'message' => 'That promo code has already used up by other users.'
			];

		}

		$minimumSpend = $promoCode['PromoCode']['minimum_spend'];

		// If the $order is coming through the checkout we won't have an amount net, so get it
		if (!isset($order['Order']['amount_net'])) {
			$order['Order']['amount_net'] = $this->Order->field(
				'amount_net',
				['Order.id' => $order['Order']['id']]
			);
		}

		if (!$this->checkMinimumSpend($minimumSpend, $order['Order']['amount_net'])) {

			return [
				'success' => false,
				'code' => 'below-minimum-spend',
				'message' => 'You cannot use that promo code as your order is below the minimum spend (' . CakeNumber::format($minimumSpend, [
					'before' => '£',
					'places' => 2
				]) . ').'
			];

		}

		return ['success' => true];

	}

/**
	* The isLive function determines whether or not a promo code is currently live based on dates and enabled.
	* @param array $promoCode the array containing the promo code
	*
	* @return TRUE if is live, else false.
 */
	public function isLive($promoCode = []) {

		if (!$this->checkIsEnabled($promoCode)) {
			return false;
		}

		if (!$this->checkHasExpired($promoCode['PromoCode']['valid_to'])) {
			return true;
		}

		if (!$this->checkNotYetValid($promoCode['PromoCode']['valid_from'])) {
			return true;
		}

		return true;

	}

/**
	* The checkEnabled function checks that the promo code has been enabled.
	* @param array $promoCode the array containing the promo code
	*
	* @return TRUE if is enabled, else false.
 */
	private function checkIsEnabled ($promoCode = []) {

		return !empty($promoCode['PromoCode']['is_active']);

	}

/**
	* The checkHasExpired function checks the valid to date is not in the past, if set.
	*
	* @return TRUE if there is a valid to date and it is in the past, else FALSE
	*
	* In other words, FALSE fails the rule, TRUE passes it
 */
	private function checkHasExpired ($validTo = null) {

		if (empty($validTo)) {
			return false;
		}

		if (CakeTime::isPast($validTo)) {
			return true;
		}

		return false;

	}

/**
	* The checkNotYetValid function checks the valid from date is not in the future, if set.
	*
	* @return TRUE if there is a valid from date and it is in the future, else FALSE
 */
	private function checkNotYetValid ($validFrom = null) {

		if (!$validFrom) {
			return false;
		}

		if (CakeTime::isFuture($validFrom)) {
			return true;
		}

		return false;

	}

/**
	* The checkLimitPerUser function checks that the user has not already used this promo code too many times, if set.
	* @param string $userId the user placing this order
	*
	* @return FALSE if there is a user limit and the user has already met or exceeded it, else TRUE
 */
	private function checkLimitPerUser ($limitPerUser, $promoCodeId, $userId) {

		// Does the promo code have a limit per user?
		if (!$limitPerUser) {

			// No, so return false
			return true;
		}

		$orderCount = $this->Order->find(
			'count',
			[
				'conditions' => [
					'Order.user_id' => $userId,
					'Order.promo_code_id' => $promoCodeId,
					'Order.order_status_id' => 'complete'
				]
			]
		);

		if ($orderCount >= $limitPerUser) {
			return false;
		}

		return true;

	}

/**
	* The checkLimitTotal function checks that the promo code has not already used too many times by any users, if set.
	* @param integer $limitTotal the number of times this promo code can be used
	*
	* @return FALSE if there is a user limit and the user has already met or exceeded it, else TRUE
 */
	private function checkLimitTotal ($limitTotal, $promoCodeId) {

		// Does the promo code have a limit per user?
		if (!$limitTotal) {

			// No, so return false
			return true;
		}

		$orderCount = $this->Order->find(
			'count',
			[
				'conditions' => [
					'Order.promo_code_id' => $promoCodeId,
					'Order.order_status_id' => 'complete'
				]
			]
		);

		if ($orderCount >= $limitTotal) {
			return false;
		}

		return true;

	}

/**
	* The checkMinimumSpend function checks that the order exceeds the minimum spend, if set.
	* @param decimal $amount a the amount of the order to be checked
	*
	* @return TRUE if there is no minimum spend or the amount is above it. The minimum spend if there is one and the amount is below it
 */
	private function checkMinimumSpend ($minimumSpend = 0, $orderAmount = 0) {

		if ($minimumSpend && ($orderAmount < $minimumSpend)) {
			return false;
		}

		return true;

	}

	/**
	* The calculateDiscount function calculates the discount and appends it to the order
	* @param array $promoCode the array containing the promo code
	* @param array $order the array containing the order
	*
	* @return array the order with the discount applied
 */
	public function calculateDiscount ($promoCode = [], $order = []) {

		if (empty($order['Order']['promo_code_id'])) {

			return 0;

		}

		// Which field do we apply the discount to
		$discountFieldName = $promoCode['PromoCode']['apply_discount_to'];

		$discountAmount = $promoCode['PromoCode']['discount_amount'];

		// If the $order is coming through the checkout we won't have an amount field, so get it
		if (!isset($order['Order'][$discountFieldName])) {
			$order['Order'][$discountFieldName] = $this->Order->field(
				$discountFieldName,
				['Order.id' => $order['Order']['id']]
			);
		}

		$grossAmount = $order['Order'][$discountFieldName];

		if ($promoCode['PromoCode']['discount_type'] == 'percent') {
			$discountAmount = ($grossAmount / 100) * $discountAmount;
		}

		// As we are dealing with pennies, multiply by 100, then round, then divide by 100
		$discountAmount = (ceil($discountAmount * 100)) / 100;

		// Take the amount of discount off of the gross amount
		$netAmount = $grossAmount - $discountAmount;

		// Make sure the discount doesn't give money away
		if ($netAmount <= 0) {
			// Set the discount amount to the gross amount
			$discountAmount = $grossAmount;
			// and set the net amount to 0
			$netAmount = 0;
		}

		$discount['amount_discount'] = $discountAmount; // stored as a negative amount for accounting;

		return $discount;

	}

}
