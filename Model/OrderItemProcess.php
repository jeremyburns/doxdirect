<?php
App::uses('AppModel', 'Model');
/**
* OrderItemProcess Model
*
*/
class OrderItemProcess extends AppModel {


	public $order = 'sequence';

	/**
* Display field
*
* @var string
*/

	/**
* hasMany associations
*
* @var array
*/
	public $belongsTo = array(
		'OrderItem' => array(
			'className' => 'ProcesOrderItem',
			'foreignKey' => 'order_item_id',
			'dependent' => false
		),
		'Process' => array(
			'className' => 'Process',
			'foreignKey' => 'process_id',
			'dependent' => false
		)
	);

	public function record($orderItemId = null, $processCode = null, $config = null) {

		if (!$processCode || !$orderItemId) {

			return array(
				'success' => false,
				'code' => 'missing-parameter'
			);

		}

		$processId = $this->Process->getIdByCode($processCode);

		if (!$processId) {

			return array(
				'success' => false,
				'code' => 'invalid-process-code'
			);

		}

		$data = array(
			'process_id' => $processId,
			'order_item_id' => $orderItemId
		);

		if ($config) {
			$data['config'] = json_encode($config);
		}

		$this->set($data);

		if ($this->save()) {
			return array('success' => true);
		}

		return array(
			'success' => false,
			'code' => 'save-failed'
		);

	}

	public function recordMany($orderItemIds = [], $processCode = null, $config = null) {

		if (!$processCode || !$orderItemIds) {

			return array(
				'success' => false,
				'code' => 'missing-parameter'
			);

		}

		foreach ($orderItemIds as $orderItemId) {

			$this->record(
				$orderItemId,
				$processCode,
				$config
			);
		}

		return array('success' => true);

	}

	public function applyProcesses($orderItemId = null) {

		if (!$orderItemid) {

			return [
				'success' => false,
				'code' => 'no-order-item-id',
				'from' => 'OrderItemProcess.applyProcesses'
			];

		}

		$orderItemProcesses = $this->find(
			'all',
			[
				'conditions' => [
					'OrderItemProcess.order_item_id' => $orderItemId
				]
			]
		);

		if (!$orderItemProcesses) {

			return [
				'success' => true,
				'code' => 'no-processes'
			];

		}

		$result = [];

		foreach ($orderItemProcesses as $orderItemProcess) {

			$result[] = $this->applyProcess($orderItemProcess);

		}

		return [
			'success' => true,
			'code' => 'processes-applied',
			'result' => $result
		];

	}

	public function applyProcess($orderItemProcess = []) {

		die(debug($orderItemProcess));

	}

}
