<?php
App::uses('AppModel', 'Model');
/**
 * OrderDate Model
 *
 * @property Order $Order
 * @property OrderStatus $OrderStatus
 */
class OrderDate extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
/*
	public $validate = array(
		'order_id' => array(
			'uuid' => array(
				'rule' => array('uuid'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
*/

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'order_id',
			'counterCache' => 'count_order_date'
		),
		'OrderStatus' => array(
			'className' => 'OrderStatus',
			'foreignKey' => 'order_status_id',
			'counterCache' => 'count_order_date'
		)
	);

	public function add($orderId = null, $orderStatusId = null, $source = null) {

		if (!$orderId || !$orderStatusId) {
			return false;
		}

		$order = $this->Order->field(
			'id',
			[
				'Order.id' => $orderId
			]
		);

		if ($order) {

			$data['OrderDate'] = array(
				'order_id' => $orderId,
				'order_status_id' => $orderStatusId,
				'source' => $source
			);

			$this->create();

			return $this->save($data);

		}

		return false;

	}

}
