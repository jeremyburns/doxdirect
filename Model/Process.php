<?php
App::uses('AppModel', 'Model');
/**
 * Process Model
 *
 */
class Process extends AppModel {

/**
 * Display field
 *
 * @var string
 */

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'OrderItemProcess' => array(
			'className' => 'OrderItemProcess',
			'foreignKey' => 'process_id',
			'dependent' => false
		),
		'OrderItemFileProcess' => array(
			'className' => 'OrderItemFileProcess',
			'foreignKey' => 'process_id',
			'dependent' => false
		),
		'OrderItemFilePageProcess' => array(
			'className' => 'OrderItemFilePageProcess',
			'foreignKey' => 'process_id',
			'dependent' => false
		)
	);

	public $types = array(
		'cli' => 'Callas CLI command',
		'profile' => 'Callas profile'
	);

	public function getIdByCode($processCode = null, $refresh = false) {

		if (!$processCode) {

			return null;

		}

		if ($refresh === true) {
			Cache::delete('process_codes', 'callas');
		}

		$model = $this;

		return Cache::remember(
			'process_codes',
			function() use ($model, $processCode) {

				return $model->field(
					'id',
					array(
						'Process.code' => $processCode
					)
				);

			},
			'callas'
		);

	}


}
