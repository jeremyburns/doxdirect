<?php
App::uses('AppModel', 'Model');
/**
 * ProductMedia Model
 *
 * @property Product $Product
 * @property Media $Media
 */
class ProductMedia extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'Media' => array(
			'className' => 'Media',
			'foreignKey' => 'media_id',
			'dependent' => false
		)
	);

	public function setLimits($productMediaId = null) {

		if (!$productMediaId) {
			return;
		}

		$productMedia = $this->find(
			'first',
			array(
				'conditions' => array(
					'ProductMedia.id' => $productMediaId
				),
				'fields' => array(
					'ProductMedia.id',
					'ProductMedia.product_id',
					'ProductMedia.media_id',
					'ProductMedia.leaves_min',
					'ProductMedia.leaves_max',
					'ProductMedia.pages_min',
					'ProductMedia.pages_max',
					'ProductMedia.thickness_min',
					'ProductMedia.thickness_max',
					'ProductMedia.format_width_min',
					'ProductMedia.format_width_max',
					'ProductMedia.format_height_min',
					'ProductMedia.format_height_max'
				),
				'contain' => array(
					'Product' => array(
						'fields' => array(
							'Product.id',
							'Product.binding_type_id',
							'Product.name'
						),
						'BindingType' => array(
							'fields' => array(
								'BindingType.id',
								'BindingType.leaves_min',
								'BindingType.leaves_max',
								'BindingType.pages_min',
								'BindingType.pages_max',
								'BindingType.thickness_min',
								'BindingType.thickness_max',
								'BindingType.format_width_min',
								'BindingType.format_width_max',
								'BindingType.format_height_min',
								'BindingType.format_height_max'
							)
						)
					),
					'Media' => array(
						'fields' => array(
							'Media.id',
							'Media.thickness',
							'Media.weight'
						)
					)
				)
			)
		);

		if (!$productMedia) {
			return;
		}

		$this->id = $productMediaId;

		// Set it to at least 1
		$minimumLeafCount = 1;

		$bindingMinimumPages = $productMedia['Product']['BindingType']['pages_min'];
		$mediaThickness = $productMedia['Media']['thickness'];

		if (!is_null($bindingMinimumPages)) {

			// The minimum pages set by the product catalogue overrules any calculations
			$minimumLeafCount = $bindingMinimumPages;

		} else {

			$bindingMinimumThickness = $productMedia['Product']['BindingType']['thickness_min'];

			// Work out the minimum leaf count

			// Does the binding type have a minimum thickness?
			if ($bindingMinimumThickness) {

				// Yes, so divide the thickness by the thickness of the media
				$minimumLeafCount = ceil($bindingMinimumThickness / $mediaThickness);

				// Is there a minimum leaf count for the binding
				$mediaMinimumLeafCount = $productMedia['Product']['BindingType']['leaves_min'];

				// If the calculated minimum leaf count lower than the minimum leaf count for the binding, set it to that
				if ($minimumLeafCount < $mediaMinimumLeafCount) {
					$minimumLeafCount = $mediaMinimumLeafCount;
				}

				if ($minimumLeafCount === 0) {
					$minimumLeafCount = 1;
				}

			}
		}

		$this->saveField('leaves_min', $minimumLeafCount);


		// Work out the maximum leaf count

		$bindingMaximumPages = $productMedia['Product']['BindingType']['pages_max'];

		if (!is_null($bindingMaximumPages)) {

			// The maximum pages set by the product catalogue overrules any calculations
			$maximumLeafCount = $bindingMaximumPages;

		} else {

			$bindingMaximumThickness = $productMedia['Product']['BindingType']['thickness_max'];

			// Does the binding type have a maximum thickness?
			if ($bindingMaximumThickness) {

				// Yes, so divide the thickness by the thickness of the media
				$maximumLeafCount = ceil($bindingMaximumThickness / $mediaThickness);

				// Is there a maximum leaf count for the binding
				$mediaMaximumLeafCount = $productMedia['Product']['BindingType']['leaves_max'];

				// If the calculated maximum leaf count higher than the maximum leaf count for the binding, set it to that
				if ($mediaMaximumLeafCount && $maximumLeafCount > $mediaMaximumLeafCount) {

					$maximumLeafCount = $mediaMaximumLeafCount;

				}

			}

		}

		if (isset($maximumLeafCount)) {
			$this->saveField('leaves_max', $maximumLeafCount);
		}

	}

/*
	* The productMediaLimits function produces an array that helps define the limits for a product on a media when print on n sides.
	* The keys returned are:
	* productId
	* 	-> mediaId
	* 		-> sides (1/single, 2/double)
	* 			-> leavesMin (optional)
	* 			-> leavesMax (optional)
	* When passed through to the quote calcualtor the js can read the right key to determine id there are min or max page counts.
	* In other words, for a given product printed on certain media on single/double sides...
*/
	public function productMediaLimits() {

		// First get all the productMedia rows
		$productMedias = $this->find(
			'all',
			[
				'fields' => [
					'ProductMedia.product_id',
					'ProductMedia.media_id',
					'ProductMedia.leaves_min',
					'ProductMedia.leaves_max'
				],
				'contain' => [
					'Product' => [
						'fields' => [
							'Product.id'
						],
						'ProductSide' => [
							'fields' => [
								'ProductSide.product_id',
								'ProductSide.side_id'
							],
							'Side.sides'
						]
					]
				]
			]
		);

		// If none, return an empty array
		if (!$productMedias) {
			return [];
		}

		$productMediaLimits = [];

		// Now loop through them
		foreach ($productMedias as $productMedia) {

			// For code clarity, extract some variables
			// Resulting variables will be in the form of $product_id
			extract($productMedia['ProductMedia']);

			foreach ($productMedia['Product']['ProductSide'] as $side) {

				$sideCount = $side['Side']['sides'];  // should be 1 and/or 2

				//Simple way to calculate the min/max number of pages:
				// For a single sided doc the number of pages = the number of leaves * 1
				// For a double sided doc the number of pages = the number of leaves * 2
				// Where 1 or 2 = $sideCount
				$productMediaLimits[$product_id][$media_id][$sideCount]['pages_min'] = $leaves_min * $sideCount;

				// If there is no max, $leaves_max will be 0, so only set this key if there is a genuine max
				if ($leaves_max) {
					$productMediaLimits[$product_id][$media_id][$sideCount]['pages_max'] = $leaves_max * $sideCount;
				}

//				// Min leaves will generally be 1, but set it to make the js code easier
//				$productMediaLimits[$product_id][$media_id][$sides[1]]['pages_min'] = $leaves_min;
//				// When printed double sided you can fit twice as many pages into the same number of leaves
//				$productMediaLimits[$product_id][$media_id][$sides[2]]['pages_min'] = $leaves_min * 2;

			}

		}

		// Return
		return $productMediaLimits;

	}

}
