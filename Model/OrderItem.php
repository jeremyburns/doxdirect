<?php

App::uses('AppModel', 'Model');

/**
 * OrderItem Model
 *
 * @property Binding $Binding
 * @property BindingEdgeType $BindingEdgeType
 * @property BindingSideType $BindingSideType
 * @property BindingType $BindingType
 * @property Click $Click
 * @property CoverType $CoverType
 * @property DrillingType $DrillingType
 * @property PaperSize $PaperSize
 * @property Media $Media
 * @property Order $Order
 * @property Product $Product
 * @property Side $Side
 */
class OrderItem extends AppModel
{

	public $actsAs = [
		'Callas',
		'OrderItemFixable'
	];
	public $order = 'OrderItem.created';

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = [
		'Binding' => [
			'className' => 'Binding',
			'foreignKey' => 'binding_id',
			'counterCache' => 'count_order_item'
		],
		'BindingType' => [
			'className' => 'BindingType',
			'foreignKey' => 'binding_type_id',
			'counterCache' => 'count_order_item'
		],
		'BindingEdgeType' => [
			'className' => 'BindingEdgeType',
			'foreignKey' => 'binding_edge_type_id',
			'counterCache' => 'count_order_item'
		],
		'BindingSideType' => [
			'className' => 'BindingSideType',
			'foreignKey' => 'binding_side_type_id',
			'counterCache' => 'count_order_item'
		],
		'Click' => [
			'className' => 'Click',
			'foreignKey' => 'click_id',
			'counterCache' => 'count_order_item'
		],
		'CoverTypeInnerFront' => [
			'className' => 'CoverType',
			'foreignKey' => 'cover_type_inner_front_id',
			'conditions' => [
				'CoverTypeInnerFront.front_back' => 'f',
				'CoverTypeInnerFront.inner_outer' => 'i'
			],
			'counterCache' => 'count_order_item_inner_front'
		],
		'CoverTypeInnerBack' => [
			'className' => 'CoverType',
			'foreignKey' => 'cover_type_inner_back_id',
			'conditions' => [
				'CoverTypeInnerBack.front_back' => 'b',
				'CoverTypeInnerBack.inner_outer' => 'i'
			],
			'counterCache' => 'count_order_item_inner_back'
		],
		'CoverTypeOuterFront' => [
			'className' => 'CoverType',
			'foreignKey' => 'cover_type_outer_front_id',
			'conditions' => [
				'CoverTypeOuterFront.inner_outer' => 'o',
				'CoverTypeOuterFront.front_back' => 'f'
			],
			'counterCache' => 'count_order_item_outer_front'
		],
		'CoverTypeOuterBack' => [
			'className' => 'CoverType',
			'foreignKey' => 'cover_type_outer_back_id',
			'conditions' => [
				'CoverTypeOuterBack.inner_outer' => 'o',
				'CoverTypeOuterBack.front_back' => 'b'
			],
			'counterCache' => 'count_order_item_outer_back'
		],
		'CoverTypeWrapAround' => [
			'className' => 'CoverType',
			'foreignKey' => 'cover_type_wrap_around_id',
			'conditions' => [
				'CoverTypeWrapAround.inner_outer' => 'w',
				'CoverTypeWrapAround.front_back' => 'w'
			],
			'counterCache' => 'count_order_item_wrap_around'
		],
		'DrillingType' => [
			'className' => 'DrillingType',
			'foreignKey' => 'drilling_type_id',
			'counterCache' => 'count_order_item'
		],
		'FilePaperSize' => [
			'className' => 'PaperSize',
			'foreignKey' => 'file_paper_size_id',
			'counterCache' => 'count_order_item'
		],
		'Media' => [
			'className' => 'Media',
			'foreignKey' => 'media_id',
			'counterCache' => 'count_order_item'
		],
		'Order' => [
			'className' => 'Order',
			'foreignKey' => 'order_id',
			'counterCache' => 'count_order_item'
		],
		'Product' => [
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'counterCache' => 'count_order_item'
		],
		'Side' => [
			'className' => 'Side',
			'foreignKey' => 'side_id',
			'counterCache' => 'count_order_item'
		]
	];

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = [
		'OrderItemFile' => [
			'className' => 'OrderItemFile',
			'foreignKey' => 'order_item_id'
		],
		'OrderItemTab' => [
			'className' => 'OrderItemTab',
			'foreignKey' => 'order_item_id'
		],
		'OrderItemProcess' => [
			'className' => 'OrderItemProcess',
			'foreignKey' => 'order_item_id',
			'dependent' => true
		]
	];

	/**
	 * Validation rules
	 *
	 * @var []
	 */
	// public $validate = [
	// 	'order_id' => [
	// 		'required' => [
	// 			'rule' => 'notBlank',
	// 			'message' => 'Order items must belong to an order.',
	// 			'last' => true
	// 		],
	// 		'noEmptyString' => [
	// 			'rule' => ['validationNoEmptyString', 'order_id'),
	// 			'message' => 'No empty string',
	// 			'last' => true
	// 		]
		// 	),
	// 	'name' => [
	// 		'maxLength' => [
	// 			'rule' => ['maxLength', 255],
	// 			'message' => 'Document name cannot be longer than 255 characters.',
	// 			'last' => true
	// 		)
	// 	),
	// 	'product_id' => [
	// 		'required' => [
	// 			'rule' => 'notBlank',
	// 			'message' => 'Please specify a product.'
	// 		)
	// 	),
	// 	'media_id' => [
	// 		'required' => [
	// 			'rule' => 'notBlank',
	// 			'message' => 'Please specify the media.'
	// 		)
	// 	),
	// 	// 'drilling_type_id' => [
	// 	// 	'required' => [
	// 	// 		'rule' => 'notBlank',
	// 	// 		'message' => 'Please specify the drilling type.'
	// 	// 	)
	// 	// ),
	// 	'side_id' => [
	// 		'required' => [
	// 			'rule' => 'notBlank',
	// 			'message' => 'Please specify the sides.'
	// 		)
	// 	),
	// 	'click_id' => [
	// 		'required' => [
	// 			'rule' => 'notBlank',
	// 			'message' => 'Please specify the clicks.'
	// 		)
	// 	),
	// 	// 'holes' => [
	// 	// 	'required' => [
	// 	// 		'rule' => ['comparison', '>=', 0),
	// 	// 		'message' => 'Holes have to be 0 or higher.'
	// 	// 	)
	// 	// ),
	// 	'tab_count' => [
	// 		'required' => [
	// 			'rule' => ['comparison', '>=', 0),
	// 			'message' => 'Tab count has to be 0 or higher.'
	// 		)
	// 	)
	// 	// 'amount_net' => [
	// 	// 	'required' => [
	// 	// 		'rule' => ['decimal', 2),
	// 	// 		'message' => 'Amount net must be a decimal number.'
	// 	// 	)
	// 	// ),
	// 	// 'amount_turnaround' => [
	// 	// 	'required' => [
	// 	// 		'rule' => ['decimal', 2),
	// 	// 		'message' => 'Amount turnaround must be a decimal number.'
	// 	// 	)
	// 	// )
	// );

	public function requestData($orderItemId) {

		$orderItem = $this->find(
			'first', [
				'conditions' => [
					'OrderItem.id' => $orderItemId
				],
				'contain' => [
					'Order' => [
						'fields' => [
							'Order.id',
							'Order.turnaround_option_id',
							'Order.delivery_option_id'
						]
					]
				]
			]
		);

		return $orderItem;
	}

	public function defaultQuote($refresh = false) {

		$defaultFields = [
			'order_id' => [
				'value' => null
			],
			'pace_quote_product_id' => [
				'value' => null
			],
			'name' => [
				'value' => null
			],
			'pages' => [
				'value' => 1
			],
			'copies' => [
				'value' => 1
			],
			'click_id' => [
				'modelName' => 'Click'
			],
			'side_id' => [
				'modelName' => 'Side'
			],
			'tab_count' => [
				'value' => 0
			],
			'product_id' => [
				'modelName' => 'Product'
			],
			'media_id' => [
				'modelName' => 'Media'
			],
			'cover_type_inner_front_id' => [
				'modelName' => 'CoverTypeInnerFront',
				'criteria' => [
					'CoverTypeInnerFront.front_back' => 'f',
					'CoverTypeInnerFront.inner_outer' => 'i'
				]
			],
			'cover_type_inner_back_id' => [
				'modelName' => 'CoverTypeInnerBack',
				'criteria' => [
					'CoverTypeInnerBack.front_back' => 'b',
					'CoverTypeInnerBack.inner_outer' => 'i'
				]
			],
			'cover_type_outer_front_id' => [
				'modelName' => 'CoverTypeOuterFront',
				'criteria' => [
					'CoverTypeOuterFront.inner_outer' => 'o',
					'CoverTypeOuterFront.front_back' => 'f'
				]
			],
			'cover_type_wrap_around_id' => [
				'modelName' => 'CoverTypeWrapAround',
				'criteria' => [
					'CoverTypeWrapAround.inner_outer' => 'w',
					'CoverTypeWrapAround.front_back' => 'w'
				]
			]
		];

		foreach ($defaultFields as $defaultFieldName => $defaultField) {

			if (array_key_exists('value', $defaultField)) {

				// Hard coding a value in
				$orderItem[$defaultFieldName] = $defaultField['value'];
			} else {

				$options = [
					'fieldName' => isset($defaultField['fieldName'])
						? $defaultField['fieldName']
						: 'id'
					,
					'refresh' => $refresh
				];

				if (isset($defaultField['criteria'])) {
					$options['criteria'] = $defaultField['criteria'];
				}

				$orderItem[$defaultFieldName] = $this->{$defaultField['modelName']}->getDefault(
					$options
				);

			}
		}

		return $orderItem;
	}

	public function removeFromOrder($order = [], $orderItemKey = null, $rebase = false) {

		if ($orderItemKey === null || !isset($order['OrderItem'][$orderItemKey])) {
			return false;
		}

		unset($order['OrderItem'][$orderItemKey]);

		if ($rebase) {
			$order = $this->Order->rebaseOrderItems($order);
		}

		$order = $this->Order->sumNetAmount($order);

		return $order;
	}

	public function stripUnquotedOrderItems($orderId = null) {

		if (!$orderId) {
			return false;
		}

		$this->deleteAll(
			[
				'OrderItem.order_id' => $orderId,
				'OrderItem.pace_quote_product_id' => null
			]
		);

		// Return the order
		return true;
	}

	public function addNewOrderItemToOrder($orderId = null) {

		// First get rid of any order items that haven't been quoted
		$this->stripUnquotedOrderItems($orderId);

		// Get a default order item
		$newOrderItem = $this->defaultQuote();

		// Connect it with this order
		$newOrderItem['order_id'] = $orderId;

		// Create it
		$this->create();

		// Save it
		if ($this->save($newOrderItem)) {
			return $this->id;
		} else {
			return false;
		}
	}

	public function listOrderItems($order = []) {

		if (empty($order['OrderItem'])) {
			return [];
		}

		$orderItems = [];

		foreach ($order['OrderItem'] as $orderItemKey => $orderItem) {

			$orderItems[$orderItemKey] = $orderItem['name'];
		}

		return $orderItems;
	}

	public function orderItemTotal($orderId = null) {

		$orderTotal = $this->find(
			'first',
			[
				'conditions' => [
					'OrderItem.order_id' => $orderId
				],
				'fields' => [
					'SUM(amount_net) as amount_net'
				]
			]
		);

		if (empty($orderTotal[0]['amount_net'])) {
			return 0;
		}

		return $orderTotal[0]['amount_net'];
	}

	public function getPaceIds($orderItemId = null) {

		if (!$orderItemId) {
			return [];
		}

		$orderItem = $this->find(
			'first',
			[
				'conditions' => [
					'OrderItem.id' => $orderItemId
				],
				'fields' => [
					'OrderItem.id',
					'OrderItem.pace_quote_product_id'
				],
				'contain' => [
					'Order' => [
						'fields' => [
							'Order.id',
							'Order.pace_quote_id'
						]
					]
				]
			]
		);

		if (!$orderItem) {
			return [];
		}

		return [
			'paceQuoteId' => $orderItem['Order']['pace_quote_id'],
			'paceQuoteProductId' => $orderItem['OrderItem']['pace_quote_product_id']
		];
	}

	public function processCallas($orderItemId = null) {

		if (!$orderItemId) {

			return [
				'success' => false,
				'code' => 'no-order-item-id',
				'from' => 'OrderItem.processCallas'
			];
		}

		$orderItem = $this->find(
			'first',
			[
				'conditions' => ['OrderItem.id' => $orderItemId],
				'fields' => [
					'OrderItem.id'
				],
				'contain' => [
					'OrderItemFile' => [
						'fields' => [
							'OrderItemFile.id',
							'OrderItemFile.order_item_id'
						]
					]
				]
			]
		);

		if (empty($orderItem['OrderItemFile'])) {

			return [
				'success' => false,
				'code' => 'no-order-item-files',
				'from' => 'OrderItem.processCallas'
			];
		}

		foreach ($orderItem['OrderItemFile'] as $orderItemFile) {

			$response = $this->OrderItemFile->processCallas($orderItemFile['id']);
		}

		return $response;
	}

	public function validatePageCounts($orderItemId = null) {

		if (!$orderItemId) {
			return false;
		}

		$pageCount = $this->OrderItemFile->find(
			'first',
			[
				'fields' => 'SUM(OrderItemFile.count_order_item_file_page) as page_count',
				'conditions' => ['OrderItemFile.order_item_id' => $orderItemId]
			]
		);

		if (!isset($pageCount[0]['page_count'])) {
			$pageCount = 0;
		} else {
			$pageCount = $pageCount[0]['page_count'];
		}

		$productMap = $this->Product->productMap();

		$orderItem = $this->find(
			'first',
			[
				'conditions' => ['OrderItem.id' => $orderItemId],
				'fields' => [
					'OrderItem.id',
					'OrderItem.product_id',
					'OrderItem.media_id',
					'OrderItem.product_id',
					'OrderItem.click_id'
				],
				'contain' => [
					'Side.sides'
				]
			]
		);

		$productId = $orderItem['OrderItem']['product_id'];
		$mediaId = $orderItem['OrderItem']['media_id'];
		$sides = $orderItem['Side']['sides'];
		$pageLimits = $productMap['selectOptions'][$productId]['pageLimits'][$mediaId][$sides];

		$minPages = isset($pageLimits['pages_min'])
			? $pageLimits['pages_min']
			: 1
		;

		$maxPages = isset($pageLimits['pages_max'])
			? $pageLimits['pages_max']
			: 0
		;

		if ($pageCount < $minPages) {

			$validation = 'below_min';
			$success = false;

		}  elseif ($maxPages && ($pageCount > $maxPages)) {

			$validation = 'above_max';
			$success = false;

		} else {

			$validation = 'ok';
			$success = true;

		}

		$data = [
			'file_page_count_valid' => $success,
			'file_page_count_validation' => $validation,
			'file_page_count' => $pageCount,
			'file_page_limit_min' => $minPages,
			'file_page_limit_max' => $maxPages
		];

		$this->id = $orderItemId;

		$this->save(
			$data,
			[
				'callbacks' => false,
				'validation' => false
			]
		);

		return [
			'success' => $success,
			'data' => $data
		];

	}

	public function updateTabCount($orderItemId = null) {

		if (!$orderItemId) {
			return false;
		}

		$orderItemTabCount = $this->OrderItemTab->find(
			'count',
			[
				'conditions' => [
					'OrderItemTab.order_item_id' => $orderItemId
				]
			]
		);

		$this->id = $orderItemId;
		$this->set('tab_count', $orderItemTabCount);
		$this->save();

		return;
	}

	/**
	 *
	 * @param type $orderItemId
	 * @return boolean
	 *
	 * This function loops through all pages in all files in an order item looking for inconsistencies. It also flags issues up to the order item level.
	 */
	public function collatePages($orderItemId = null) {

		if (!$orderItemId) {
			// We need an id
			return [
				'success' => false,
				'code' => 'no-order-item-id'
			];
		}

		$orderItem = $this->find(
			'first',
			[
				'conditions' => ['OrderItem.id' => $orderItemId],
				'fields' => [
					'OrderItem.id',
					'OrderItem.order_id',
					'OrderItem.product_id',
					'OrderItem.pages',
					'OrderItem.file_paper_size_id',
					'OrderItem.file_orientation'
				],
				'contain' => [
					'Product.paper_size_id',
					'Product.binding_type_id'
				]
			]
		);

		$orderId = $orderItem['OrderItem']['order_id'];

		// Check the page count as specified by the user in the quote (or set previously from actual page counts)
		$currentPageCount = $orderItem['OrderItem']['pages'];

		// Also check the page size
		$currentPaperSizeId = $orderItem['Product']['paper_size_id'];

		// Get the core fields from all order item files for this order item
		$orderItemFiles = $this->OrderItemFile->find(
			'all', [
				'conditions' => ['OrderItemFile.order_item_id' => $orderItemId],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.colour',
					'OrderItemFile.count_order_item_file_page',
					'OrderItemFile.paper_size_id',
					'OrderItemFile.paper_size_consistency',
					'OrderItemFile.paper_size_match_consistency',
					'OrderItemFile.paper_size_match',
					'OrderItemFile.paper_size_scale_consistency',
					'OrderItemFile.orientation',
					'OrderItemFile.orientation_consistency',
					'OrderItemFile.orientation_count_p',
					'OrderItemFile.orientation_count_l',
					'OrderItemFile.has_trim_marks'
				],
				'contain' => [
					'OrderItemFilePage' => [
						'fields' => [
							'OrderItemFilePage.order_item_file_id',
							'OrderItemFilePage.paper_size_id'
						]
					]
				]
			]
		);

		// Start some variables
		$filesOk = true;
		$filePaperSizeId = $orderItem['OrderItem']['file_paper_size_id'];
		$fileOrientation = $orderItem['OrderItem']['file_orientation'];
		$orderItemPageCount = 0;
		// Start with an unknown orientation
		$fileOrientation = 'u';
		$fileOrientationCounts = [];
		$filePaperSizeConsistency = true;
		$filePaperSizeMatch = 'exact';
		$filePaperSizeMatchConsistency = true;
		$paperSizeMatch = 'exact';
		$filePaperSizeOrderItemMatch = '=';
		$filePaperSizeScaleConsistency = true;
		$fileOrientationConsistency = true;
		$fileHasTrimMarks = false;

		if ($orderItemFiles) {

			// Loop through the order item files
			foreach ($orderItemFiles as $orderItemFile) {

				// Set the colour of this file
				$this->OrderItemFile->setColour(
					$orderItemFile['OrderItemFile']['id'],
					$orderItemFile['OrderItemFile']['colour']
				);

				// This determines whether any files have trim marks
				if ($orderItemFile['OrderItemFile']['has_trim_marks']) {
					$fileHasTrimMarks = true;
				}

				// Increment the page count
				$orderItemPageCount += $orderItemFile['OrderItemFile']['count_order_item_file_page'];

				// Increment the page orientations

				// Landscape first
				if ($orderItemFile['OrderItemFile']['orientation_count_l']) {

					if (empty($fileOrientationCounts['l'])) {
						$fileOrientationCounts['l'] = $orderItemFile['OrderItemFile']['orientation_count_l'];
					} else {
						$fileOrientationCounts['l'] += $orderItemFile['OrderItemFile']['orientation_count_l'];
					}
				}

				// Then portrait
				if ($orderItemFile['OrderItemFile']['orientation_count_p']) {

					if (empty($fileOrientationCounts['p'])) {
						$fileOrientationCounts['p'] = $orderItemFile['OrderItemFile']['orientation_count_p'];
					} else {
						$fileOrientationCounts['p'] += $orderItemFile['OrderItemFile']['orientation_count_p'];
					}

				}

				// Have we already got a paper size?
				if (empty($filePaperSizeId)) {
					// No - so start it
					$filePaperSizeId = $orderItemFile['OrderItemFile']['paper_size_id'];

				} elseif ($filePaperSizeId != $orderItemFile['OrderItemFile']['paper_size_id'] && $filePaperSizeConsistency) {

					// Yes we have and it's currently consistent but this one is different
					$filePaperSizeConsistency = false;
					// Set the paper size to null as we can't determine the paper size
					$filePaperSizeId = null;
					$filesOk = false;
				}

				// This determines whether the paper sizes are consistent within a file
				if (!$orderItemFile['OrderItemFile']['paper_size_consistency']) {
					// There are inconsistencies within the file so the whole order item is inconsistent too
					$filePaperSizeConsistency = false;
					$filesOk = false;
				}

				// This determines whether all pages have the same paper size match
				if (!$orderItemFile['OrderItemFile']['paper_size_match_consistency']) {
					// There are inconsistencies within the file so the whole order item is inconsistent too
					$filePaperSizeMatchConsistency = false;
					$filesOk = false;
				}

				// This determines whether the pages exactly match a paper size
				if ($orderItemFile['OrderItemFile']['paper_size_match'] !== 'exact') {
					// At least one of the pages do not exactly match an A paper size
					$filePaperSizeMatch = $orderItemFile['OrderItemFile']['paper_size_match'];
					$filesOk = false;
				}

				// This determines whether the pages scale to the paper size of the product
				// In other words, does the file match the chosen paper size
				if (!$orderItemFile['OrderItemFile']['paper_size_scale_consistency']) {
					// There are inconsistencies within the file so the whole order item is inconsistent too
					$filePaperSizeScaleConsistency = false;
					$filesOk = false;
				}

				if ($orderItemFile['OrderItemFile']['paper_size_id'] !== $orderItem['Product']['paper_size_id']) {
					$scale = $this->OrderItemFile->PaperSize->paperSizeScale(
						$orderItem['Product']['paper_size_id'],
						$orderItemFile['OrderItemFile']['paper_size_id']
					);

					if ($scale < 0) {
						$filePaperSizeOrderItemMatch = 'u';
					} else {
						$filePaperSizeOrderItemMatch = 'o';
					}
					$filesOk = false;

				}

			}

		}

		if (count($fileOrientationCounts) > 1) {

			// We've got mixed orientation
			$fileOrientation = 'm';

			// Invalidate the order item
			$fileOrientationConsistency = false;
			$filesOk = false;

		} else {

			// All good - just one orientation
			$fileOrientationConsistency = true;

			// Get the array keys
			$fileOrientationKeys = array_keys($fileOrientationCounts);

			// Key 0 contains the orientation
			$fileOrientation = $fileOrientationKeys[0];

			// Now fill the blanks with a 0
			$fileOrientationCounts = array_merge(
				$fileOrientationCounts,
				[
					'l' => 0,
					'p' => 0
				]
			);

		}

		$data = [
			'id' => $orderItemId,
			'pages' => !$orderItemPageCount ? 1 : $orderItemPageCount, // always have at least one page
			'files_ok' => $filesOk,
			'file_paper_size_id' => $filePaperSizeId,
			'file_paper_size_consistency' => $filePaperSizeConsistency,
			'file_paper_size_match_consistency' => $filePaperSizeMatchConsistency,
			'file_paper_size_match' => $filePaperSizeMatch,
			'file_paper_size_scale_consistency' => $filePaperSizeScaleConsistency,
			'file_paper_size_order_item_match' => $filePaperSizeOrderItemMatch,
			'file_orientation' => $fileOrientation,
			'file_orientation_consistency' => $fileOrientationConsistency,
			'file_orientation_count_p' => $fileOrientationCounts['p'],
			'file_orientation_count_l' => $fileOrientationCounts['l'],
			'file_has_trim_marks' => $fileHasTrimMarks
		];

		// Assume we don't need to quote again
		$requote = false;

		$validatePageCounts = $this->validatePageCounts($orderItemId);

		if (!$validatePageCounts['success']) {

			// The uploaded pages are above the max, which is not allowed.
			// Business rules state the order must be quoted at the max, so change the order item page counts and set a requote
			if ($validatePageCounts['data']['file_page_count_validation'] === 'above_max') {

				$requote = true;
				$data['pages'] = $validatePageCounts['data']['file_page_limit_max'];

			}

		}

		// Do the page totals match what is currently set?
		if (($currentPageCount !== $orderItemPageCount)) { //|| $currentPaperSizeId !== $filePaperSizeId) {
			// No, so we need to quote again
			$requote = true;

		}

		$result = $this->save(
			$data,
			[
				'callbacks' => false,
				'validate' => false
			]
		);

		if ($requote) {
			$result = $this->Order->requote(
				$orderId,
				$orderItemId
			);
		}

		return [
			'success' => true,
			'requoted' => $requote
		];

	}

	public function setPaperSizeConsistency($orderItemId = null) {

		if (!$orderItemId) {
			return [
				'success' => false,
				'code' => 'no-order-item-id'
			];
		}

		$orderItemFiles = $this->OrderItemFile->find(
			'all', [
				'conditions' => [
					'OrderItemFile.order_item_id' => $orderItemId
				],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.count_order_item_file_page',
					'OrderItemFile.paper_size_id',
					'OrderItemFile.paper_size_consistency',
					'OrderItemFile.paper_size_match_consistency',
					'OrderItemFile.paper_size_scale_consistency',
					'OrderItemFile.orientation',
					'OrderItemFile.orientation_consistency',
					'OrderItemFile.has_trim_marks'
				]
			]
		);

		if (!$orderItemFiles) {
			return [
				'success' => false,
				'code' => 'no-order-item-file'
			];
		}

		foreach ($orderItemFiles as $orderItemFileId => $orderItemFile) {

			$paperSizeConsistency[$orderItemFileId] = $this->OrderItemFile->paperSizeConsistency($orderItemFileId);
		}
	}

	/**
	 * This function examines each page in a file looking at its size.
	 * It then determines whether each page has the same size and sets the consistency
	 */
	public function paperSizeConsistency($orderItemId = null) {

		// Make sure we've got something to look at
		if (!$orderItemId) {

			// Else fail
			return [
				'success' => false,
				'code' => 'no-order-item-id',
				'action' => 'upload'
			];
		}

		$orderItemFileIds = $this->orderItemFileIds($orderItemId);

		// Do a group by query to count how many pages of each paper size we have
		$pages = $this->OrderItemFile->OrderItemFilePage->find(
			'all',
			[
				'conditions' => [
					'OrderItemFilePage.order_item_file_id' => $orderItemFileIds
				],
				'fields' => [
					'OrderItemFilePage.paper_size_id',
					'count(OrderItemFilePage.paper_size_id) as page_count'
				],
				'group' => [
					'OrderItemFilePage.paper_size_id'
				]
			]
		);

		if (!$pages) {

			// No pages, so fail
			return [
				'success' => false,
				'code' => 'no-order-file-pages',
				'action' => 'process-files'
			];
		}

		if (count($pages) === 1) {

			// All pages are of the same paper size
			// The results array has a key with the page totals
			$paperSizes[$pages[0]['OrderItemFilePage']['paper_size_id']] = $pages[0][0]['page_count'];

			// Get the single size
			$paperSizeId = $pages[0]['OrderItemFilePage']['paper_size_id'];

			return [
				'success' => true,
				'data' => [
					'consistency' => true,
					'paper_size_id' => $paperSizeId,
					'paperSizes' => $paperSizes
				]
			];
		}

		// Set a benchmark page count
		$pageCount = 0;

		// The pages have mixed sizes
		// The consistency key confirms that they are mixed

		$consistency = false;

		// Loop through the results
		foreach ($pages as $page) {

			// Populate the result key with the page count for this size
			$paperSizes[$page['OrderItemFilePage']['paper_size_id']] = $page[0]['page_count'];

			// If there are more of these pages than the previously set benchmark, change the size to this one as it is dominant
			if ($page[0]['page_count'] > $pageCount) {

				// Reset the benchmark
				$pageCount = $page[0]['page_count'];

				// Set the size
				$paperSizeId = $page['OrderItemFilePage']['paper_size_id'];
			}
		}

		return [
			'success' => false,
			'data' => [
				'consistency' => false,
				'paper_size_id' => $paperSizeId,
				'paperSizes' => $paperSizes
			]
		];
	}

	/**
	 * This function examines how well pages are matched to paper sizes
	 */
	public function paperSizeMatchConsistency($orderItemId = null) {

		// Make sure we've got something to look at
		if (!$orderItemId) {

			// Else fail
			return [
				'success' => false,
				'code' => 'no-order-item-id'
			];
		}

		$orderItemFileIds = $this->orderItemFileIds($orderItemId);

		// Do a group by query to count how many pages of each paper size we have
		$pages = $this->OrderItemFile->OrderItemFilePage->find(
			'all',
			[
				'conditions' => [
					'OrderItemFilePage.order_item_file_id' => $orderItemFileIds
				],
				'fields' => [
					'OrderItemFilePage.paper_size_match',
					'count(OrderItemFilePage.paper_size_match) as page_count'
				],
				'group' => [
					'OrderItemFilePage.paper_size_match'
				]
			]
		);

		if (!$pages) {

			// No pages, so fail
			return [
				'success' => false,
				'code' => 'no-order-file-pages'
			];
		}

		if (count($pages) === 1) {

			// All pages are of the same paper size

			$consistency = true;

			// The results array has a key with the page totals
			$matches[$pages[0]['OrderItemFilePage']['paper_size_match']] = $pages[0][0]['page_count'];
		} else {

			// At least some of the pages do not match standard paper sizes
			$consistency = false;

			// Loop through the results
			foreach ($pages as $page) {

				// Populate the result key with the page count for this match
				$matches[$page['OrderItemFilePage']['paper_size_match']] = $page[0]['page_count'];
			}
		}

		// Construct and return the result
		return [
			'consistency' => $consistency,
			'matches' => $matches
		];
	}

	public function setOrientationConsistency($orderItemId = null) {

		if (!$orderItemId) {
			return [
				'success' => false,
				'code' => 'no-order-item-id'
			];
		}

		$orderItemFiles = $this->OrderItemFile->find(
			'list',
			[
				'conditions' => [
					'OrderItemFile.order_item_id' => $orderItemId,
					'OrderItemFile.orientation_consistency' => false
				]
			]
		);

		if ($orderItemFiles) {
			$orderItemFilesConsistency = false;
		} else {
			$orderItemFilesConsistency = true;
		}

		$orderItemFiles = $this->OrderItemFile->find(
			'list',
			[
				'conditions' => [
					'OrderItemFile.order_item_id' => $orderItemId,
					'OrderItemFile.orientation_consistency' => false
				]
			]
		);

		$this->id = $orderItemId;
		$this->set('file_orientation_consistency', $orderItemFilesConsistency);
		return $this->save();
	}

	public function pageConsistency($orderItemId = null) {

		if (!$orderItemId) {
			return [
				'success' => false,
				'code' => 'no-order-item-id',
				'from' => 'OrderItem.pageConsistency'
			];
		}

		$orderItemFiles = $this->OrderItemFile->find(
			'list',
			[
				'conditions' => [
					'OrderItemFile.order_item_id' => $orderItemId
				]
			]
		);

		if (!$orderItemFiles) {
			return [
				'success' => false,
				'code' => 'no-order-item-file',
				'from' => 'OrderItem.pageConsistency'
			];
		}

		foreach ($orderItemFiles as $orderItemFileId => $orderItemFile) {

			$pageConsistency = $this->OrderItemFile->pageConsistency($orderItemFileId);
		}

		return [
			'success' => false,
			'code' => 'unknown',
			'from' => 'OrderItem.pageConsistency'
		];
	}

	public function orderItemFileIds($orderItemId = null) {

		if (!$orderItemId) {
			return [];
		}

		return $this->OrderItemFile->find(
			'list', [
				'conditions' => ['OrderItemFile.order_item_id' => $orderItemId],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.id'
				]
			]
		);
	}

	public function setBindingEdgeTypeId($orderItemId = null, $bindingEdgeTypeCode = 'long') {

		if ($orderItemId) {

			$bindingEdgeTypeId = $this->BindingEdgeType->field(
				'id', [
					'BindingEdgeType.pace_name LIKE' => '%' . $bindingEdgeTypeCode . '%'
				]
			);

			if ($bindingEdgeTypeId) {

				$this->id = $orderItemId;

				if ($this->exists()) {

					$this->set([
						'binding_edge_type_id' => $bindingEdgeTypeId,
						'binding_edge_criteria' => 'y'
					]);
					$this->save();

					return true;
				}
			}
		}

		return false;
	}

	public function setBindingEdgeCriteria($orderItemId = null, $criteria = '?') {

		if ($orderItemId) {

			$this->id = $orderItemId;

			if ($this->exists()) {

				$this->set('binding_edge_criteria', $criteria);
				$this->save();

				return true;
			}
		}

		return false;
	}

	public function setBindingSideTypeId($orderItemId = null, $bindingSideTypeCode = null) {

		if ($orderItemId || !$bindingSideTypeCode) {

			$bindingSideName = $this->getConfigurationValue('binding_side_' . $bindingSideTypeCode);

			if (!$bindingSideName) {
				return false;
			}

			$bindingSideTypeId = $this->BindingSideType->field(
				'id', ['LOWER(BindingSideType.pace_name)' => strtolower($bindingSideName)]
			);

			if ($bindingSideTypeId) {

				$this->id = $orderItemId;

				if ($this->exists()) {

					$this->set([
						'binding_side_type_id' => $bindingSideTypeId,
						'binding_side_criteria' => 'y'
					]);
					$this->save();

					return $bindingSideName;
				}
			}
		}

		return false;
	}

	public function setBindingSideCriteria($orderItemId = null, $criteria = '?') {

		if ($orderItemId) {

			$this->id = $orderItemId;

			if ($this->exists()) {

				$this->set('binding_side_criteria', $criteria);
				$this->save();

				return true;
			}
		}

		return false;
	}

	public function setPaperSize($orderItemid = null, $targetPaperSizeId = null) {

		$productId = $this->field(
			'product_id',
			['OrderItem.id' => $orderItemId]
		);

		$bindingTypeId = $this->Product->field(
			'binding_type_id',
			['id' => $productId]
		);

		$newProductId = $this->Product->field(
			'id',
			[
				'Product.binding_type_id' => $bindingTypeId,
				'Product.paper_size_id' => $targetPaperSizeId
			]
		);

		$this->id = $orderItemId;
		$this->set('product_id', $newProductId);
		$this->save();

		$this->Order->requote($orderId, $orderItemId);

	}

	public function updatePrintedCoverOptions($orderItemId = null, $frontCoverOptions = null, $backCoverOptions = null) {

		if (!$orderItemId) {
			return [
				'success' => false,
				'code' => 'no-order-item-id'
			];
		}

		$this->id = $orderItemId;

		$this->set([
			'printed_cover_front_options' => $frontCoverOptions,
			'printed_cover_back_options' => $backCoverOptions,
			'printed_cover_front_id' => null,
			'printed_cover_back_id' => null
		]);
		return $this->save();
	}

	public function updatePrintedCoverJobId($orderItemId = null, $coverJobId = null) {

		if (!$orderItemId) {
			return [
				'success' => false,
				'code' => 'no-order-item-id'
			];
		}

		$this->id = $orderItemId;
		$this->set('printed_cover_job_id', $coverJobId);
		return $this->save();
	}

	public function getCallasCovers($orderItemId = null, $frontCoverOptions = null, $backCoverOptions = null) {

		if (!$orderItemId) {
			return [
				'success' => false,
				'code' => 'no-order-item-id'
			];
		}

		$orderItem = $this->find(
			'first',
			['conditions' => ['OrderItem.id' => $orderItemId]]
		);

		// Determine how many documents are in this order item
		$orderItemFiles = $this->OrderItemFile->find(
			'all',
			['conditions' => ['OrderItemFile.order_item_id' => $orderItemId]]
		);

		if ($frontCoverOptions == "use_and_discard") {

			// Extract page 1 from the first OrderItemFile
			$frontOrderItemFile = $orderItemFiles[0];
		} else {

			$frontOrderItemFile = null;
		}

		if ($backCoverOptions == "use_and_discard") {

			// Extract last page from last OrderItemFile
			$backOrderItemFile = $orderItemFiles[count($orderItemFiles) - 1];
		} else {

			$backOrderItemFile = null;
		}

		// Check to see whether we've already extracted the cover pages and sent them to PrintUI...
		$frontCoverFileRef = $this->field('printed_cover_front_id', ['OrderItem.id' => $orderItemId]);
		$backCoverFileRef = $this->field('printed_cover_back_id', ['OrderItem.id' => $orderItemId]);

		if(($frontCoverOptions == "use_and_discard" && $frontCoverFileRef) || ($backCoverFileRef == "use_and_discard" && $backCoverFileRef)) {
			return true;
		}

		$extractedCoverFiles = $this->extractCoverPages($orderItemId, $frontCoverOptions, $backCoverOptions, $frontOrderItemFile, $backOrderItemFile);

		$printUIJobID = $this->field(
			'printed_cover_job_id',
			['OrderItem.id' => $orderItemId]
		);
		$frontCoverFileRef = null;
		$backCoverFileRef = null;
		$frontFrameID = null;
		$backFrameID = null;

		if ($extractedCoverFiles['front']) {
			$frontCoverFile = curl_file_create($extractedCoverFiles['front'], "application/pdf", basename($extractedCoverFiles['front']));
			$frontCoverData = $this->printuiConnect(
				'putimage',
				[
					'id' => $printUIJobID,
					'f' => 'frontcover.pdf',
					'file' => $frontCoverFile
				]
			);

			if (isset($frontCoverData['file'])) {
				$frontCoverFileRef = $frontCoverData['file'];

				$data = [
					'id' => $orderItemId,
					'printed_cover_front_id' => $frontCoverFileRef
				];
				$this->save($data, ['callbacks' => false, 'counterCache' => false]);

			} else {
				//$this->log($frontCoverData);
			}
		}

		if ($extractedCoverFiles['back']) {
			$backCoverFile = curl_file_create($extractedCoverFiles['back'], "application/pdf", basename($extractedCoverFiles['back']));
			$backCoverData = $this->printuiConnect(
				'putimage',
				[
					'id' => $printUIJobID,
					'f' => 'backcover.pdf',
					'file' => $backCoverFile
				]
			);

			if (isset($backCoverData['file'])) {
				$backCoverFileRef = $backCoverData['file'];

				$data = [
					'id' => $orderItemId,
					'printed_cover_back_id' => $backCoverFileRef
				];
				$this->save($data, ['callbacks' => false, 'counterCache' => false]);

			} else {
				//$this->log($backCoverData);
			}
		}

		// Get the template frame IDs
		$frames = $this->printuiConnect(
			'getframes',
			[
				'id' => $printUIJobID,
				'show' => 'all'
			]
		);

		foreach ($frames->spreads->spread->frame as $frame) {

			if ((string)$frame['type'] == "pdf") {
				$frameValue = (string)$frame;

				if (stripos($frameValue, "Front_Cover.pdf") !== false) {
					$frontFrameID = (string)$frame['id'];
				} else if (stripos($frameValue, "Back_Cover.pdf") !== false) {
					$backFrameID = (string)$frame['id'];
				}
			}
		}

		// Generate the replace XML string if required
		if ($frontCoverFileRef || $backCoverFileRef) {
			$xmlString = '<replace>';

			if ($frontCoverFileRef) {
				$xmlString .= '<image id="' . $frontFrameID . '"><with>' . $frontCoverFileRef . '</with></image>';
			}

			if ($backCoverFileRef) {
				$xmlString .= '<image id="' . $backFrameID . '"><with>' . $backCoverFileRef . '</with></image>';
			}

			$xmlString .= '</replace>';
			$frameReplacement = $this->printuiConnect(
				'replaceimage',
				[
					'id' => $printUIJobID,
					'data' => $xmlString
				]
			);
		}

		return true;
	}

	/**
	 * Various functions for PrintUI integration
	 */
	public function printuiConnect($functionName = null, $params = [], $returnXML = true) {

		$baseUrl = 'https://ids.w2p-tools.com/d003/';
		$url = $baseUrl . $functionName . '.php';
		$params['auth'] = '45719d57';

		$curlObj = curl_init();
		curl_setopt_array(
			$curlObj,
			[
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_CONNECTTIMEOUT => 3600,
				CURLOPT_TIMEOUT => 7200,
				CURLOPT_HTTPHEADER => array('Expect:'),
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => $params,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_SSL_VERIFYHOST => 2,
				CURLOPT_SAFE_UPLOAD => true
			]
		);
		$curlResult = curl_exec($curlObj);
		$curlInfo = curl_getinfo($curlObj);
		$curlInfo['result'] = $curlResult;

		if ($curlInfo['http_code'] == 200 && $returnXML) {

			return simplexml_load_string($curlResult);

		} else if ($curlInfo['http_code'] == 200 && !$returnXML) {

			return $curlResult;

		} else {

			$curlInfo['params'] = $params;
			return $curlInfo;
		}
	}

	public function createNewCoverDocument($orderItemId = null, $isBlank = false) {

		$orderItem = $this->find(
			'first',
			[
				'conditions' => ['OrderItem.id' => $orderItemId],
				'contain' => ['Binding', 'Side', 'Media']
			]
		);

		/* NEED TO GET MEDIA FROM ORDER ITEM */

		// No cover job ID yet, so we need to generate that
		// We need:
		// 1. Paper Size
		// 2. Document Type
		// 3. Orientation
		// 4. Single/Double-sided
		// 5. Number of pages
		// 6. Sheet thickness
		$paperSize = $orderItem['Media']['paper_size_id'];
		$documentType = substr($orderItem['OrderItem']['name'], 3);
		$orientation = $orderItem['Binding']['name'];
		$sides = $orderItem['Side']['sides'];
		$pages = $orderItem['OrderItem']['pages'];

		if ($orderItem['OrderItem']['printed_cover_front_options'] == "use_and_discard") {

			$pages--;
		}
		if ($orderItem['OrderItem']['printed_cover_back_options'] == "use_and_discard") {

			$pages--;
		}

		$sheetThickness = $orderItem['Media']['thickness'];
		$templateName = strtoupper($paperSize . "_" . $documentType . "_" . $orientation);
		$documentThickness = ($sides == 1 ? $sheetThickness * $pages : $sheetThickness * ceil($pages / 2));
		$roundedThickness = $this->_roundToHalf($documentThickness);

		// Work out which template size to use
		$templateSize = $this->returnTemplateSize($roundedThickness, $templateName);

		if (!$templateSize) {

			return [
				'success' => false,
				'code' => 'no-matching-template',
				'message' => 'The document(s) you have uploaded do not meet the minimum thickness for this cover. Please upload more pages or a different document.'
			];
		}

		//$this->log("Paper Size: $paperSize, Doc Type: $documentType, Orientation: $orientation, Pages: $pages, Sheet Thickness: $sheetThickness, Template Name: $templateName, Doc Thickness: $documentThickness, Rounded Thickness: $roundedThickness, Template Size: $templateSize");

		if ($isBlank)
			$templateName .= "_BLANK";

		// Create document via PrintUI
		$jobData = $this->printuiConnect(
			"newjob",
			[
				"t" => $templateName,
				"n" => $templateSize,
				"order" => $orderItemId
			]
		);

		// Return PrintUI Job ID
		if (isset($jobData['id'])) {

			$this->updatePrintedCoverJobId($orderItemId, $jobData['id']);
			return [
				'success' => true,
				'id' => $jobData['id']
			];
		} else {

			return [
				'success' => false,
				'data' => $jobData
			];

		}
	}

	public function returnTemplateSize($roundedThickness = null, $templateName = null) {

		if (!$roundedThickness || !$templateName) {
			return false;
		} else {

			// Assuming both $roundedThickness and $templateName have been set, let's proceed
			$templates = [
				'A4_HARDBACK_PORTRAIT' => [
					'height' => 327,
					'base_width' => 452,
					'cover_thickness' => 4
				],
				'A4_HARDBACK_LANDSCAPE' => [
					'height' => 228.16,
					'base_width' => 614,
					'cover_thickness' => 6
				],
				'A5_HARDBACK_PORTRAIT' => [
					'height' => 239.21,
					'base_width' => 331.83,
					'cover_thickness' => 4
				],
				'A5_HARDBACK_LANDSCAPE' => [
					'height' => 168.45,
					'base_width' => 448.23,
					'cover_thickness' => 6
				],
				'A4_PAPERBACK_PORTRAIT' => [
					'height' => 320.9,
					'base_width' => 445.28,
					'cover_thickness' => 0
				],
				'A4_PAPERBACK_LANDSCAPE' => [
					'height' => 234.1,
					'base_width' => 619.32,
					'cover_thickness' => 0
				],
				'A5_PAPERBACK_PORTRAIT' => [
					'height' => 234.05,
					'base_width' => 321.8,
					'cover_thickness' => 0
				],
				'A5_PAPERBACK_LANDSCAPE' => [
					'height' => 172.55,
					'base_width' => 444.75,
					'cover_thickness' => 0
				]
			];

			$templateSize = (string)($templates[$templateName]['base_width'] + $templates[$templateName]['cover_thickness'] + $roundedThickness) . "x" . (string)$templates[$templateName]['height'];

			return $templateSize;
		}
	}

	protected function _roundToHalf($number_to_round) {

		if ($number_to_round >= ($half = ($ceil = ceil($number_to_round)) - 0.5) + 0.25)
			return $ceil;
		else if ($number_to_round < $half - 0.25)
			return floor($number_to_round);
		else
			return $half;
	}

	public function insertBlankPages($orderItemId = null, $blankPageCount = 0) {

		if (!$orderItemId || !$blankPageCount) {
			return [
				'success' => false,
				'code' => 'missing-parameter',
				'orderItemId' => $orderItemId,
				'blankPageCount' => $blankPageCount,
				'from' => 'OrderItem.insertBlankPages'
			];
		}

		// Get the id of the last order item file
		$orderItemFileId = $this->OrderItemFile->field(
			'id',
			['OrderItemFile.order_item_id' => $orderItemId],
			'OrderItemFile.sequence DESC'
		);

		$this->OrderItemFile->OrderItemFilePage->insertBlankPages($orderItemFileId, $blankPageCount);

		return ['success' => true];

	}

	public function processOriginalFiles($orderId = null) {

		if (!$orderId) {

			return [
				'success' => false,
				'code' => 'no-order-id',
				'from' => 'OrderItem.processOriginalFiles'
			];

		}

		$orderItems = $this->find(
			'list',
			[
				'conditions' => [
					'OrderItem.order_id' => $orderId
				],
				'fields' => 'OrderItem.id'
			]
		);

		if (!$orderItems) {

			return [
				'success' => true,
				'code' => 'no-order-items',
				'from' => 'OrderItem.processOriginalFiles'
			];

		}

		$result = [];

		foreach ($orderItems as $orderItemId) {

			$result[] = $this->OrderItemFile->processOriginalFiles($orderItemId);

		}

		return [
			'success' => true,
			'result' => $result
		];

	}

}
