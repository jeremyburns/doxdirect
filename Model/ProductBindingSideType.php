<?php
App::uses('AppModel', 'Model');
/**
 * ProductBindingSideType Model
 *
 * @property Product $Product
 * @property BindingSideType $BindingSideType
 */
class ProductBindingSideType extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'BindingSideType' => array(
			'className' => 'BindingSideType',
			'foreignKey' => 'binding_side_type_id',
			'dependent' => false
		)
	);

}