<?php
App::uses('AppModel', 'Model');
/**
 * ProductMapCategory Model
 *
 */
class ProductMapCategory extends AppModel {

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'ProductMapCategoryExtraField' => array(
            'className' => 'ProductMapCategoryExtraField',
            'foreignKey' => 'product_map_category_id',
            'dependent' => false
        )
    );

    // Maps Dox fields to Pace fields
    public function fieldMap() {

        return Cache::remember(
            'field_map',
            function() {

                return array(
                    'name' => 'description',
                    'pace_name' => 'name',
                    'is_active' => 'active'
                );

            },
            'pace'
        );

    }

    // Reads the map categories in from the table
    // The map categories are the groups of options that are applicable to each product
    // This produces a list of categories to products for matching up select options
    // In other words, it defines what options are available for given products,
    // e.g. drilling options for products
    public function getProductMapCategories() {

        $model = $this;

        return Cache::remember(
            'product_category_map',
            function() use ($model) {

                // Do the basic find
                $map = $this->find(
                    'all',
                    array(
                        'fields' => array(
                            'ProductMapCategory.id',
                            'ProductMapCategory.name',
                            'ProductMapCategory.ignore',
                            'ProductMapCategory.model_path',
                            'ProductMapCategory.join_model',
                            'ProductMapCategory.join_field'
                        ),
                        'contain' => array(
                            'ProductMapCategoryExtraField'
                        )
                    )
                );

                // If no records are found simply return
                if (!$map) {
                    return array();
                }

                // Now build the result
                $result = array();

                // Loop through the maps
                foreach ($map as $category) {

                    // Get the id and map it to the category name
                    $result[$category['ProductMapCategory']['id']] = $category['ProductMapCategory'];

                    // Are there extra fields defined?
                    if ($category['ProductMapCategoryExtraField']) {

                        // Yup - so loop through them
                        foreach($category['ProductMapCategoryExtraField'] as $extraField) {

                            // Add them to the result
                            $result[$category['ProductMapCategory']['id']]['extra_fields'][$extraField['field_name']] = $extraField['value'];

                        }

                    }

                }

                // Return the result
                return $result;

            },
            'pace'
        );

    }

}
