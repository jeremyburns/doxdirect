<?php
App::uses('AppModel', 'Model');
/**
 * Holiday Model
 *
 */
class Holiday extends AppModel {

	public function isHoliday($date = null) {

		if (!$date) {
			return false;
		}

		$day = $date->format('d');
		$month = $date->format('m');
		$year = $date->format('Y');

		$holiday = $this->find(
			'first',
			array(
				'conditions' => array(
					'OR' => array(
						array(
							'Holiday.day' => $day,
							'Holiday.month' => $month,
							'Holiday.year' => $year
						),
						array(
							'Holiday.day' => $day,
							'Holiday.month' => $month
						)
					)
				),
				'fields' => array(
					'Holiday.name',
					'Holiday.will_despatch',
					'Holiday.will_collect',
					'Holiday.will_deliver',
				)
			)
		);

		if (!$holiday) {
			return false;
		}

		return $holiday;

	}

	public function will($willWhat = '', $date = null) {

		if (!$willWhat || !$date) {
			return false;
		}

		$holiday = $this->isHoliday($date);

		if (!$holiday) {
			return true;
		}

		$fieldName = 'will_' . $willWhat;

		return $holiday['Holiday'][$fieldName];

	}

	public function isWeekend($date = null) {

		if (!$date) {
			return false;
		}

		$weekday = $date->format('w');

		$sunday = 0;
		$saturday = 6;

		return (in_array($weekday, array($saturday, $sunday)));

	}

}
