<?php
App::uses('AppModel', 'Model');

/**
 * Order Model
 *
 * @property User $User
 * @property PromoCode $PromoCode
 * @property OrderStatus $OrderStatus
 * @property DeliveryOption $DeliveryOption
 * @property OrderDate $OrderDate
 * @property OrderItem $OrderItem
 */
class Order extends AppModel {

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = [
		'User' => [
			'className' => 'User',
			'foreignKey' => 'user_id',
			'counterCache' => 'count_order'
		],
		'PromoCode' => [
			'className' => 'PromoCode',
			'foreignKey' => 'promo_code_id',
			'counterCache' => 'count_order'
		],
		'OrderStatus' => [
			'className' => 'OrderStatus',
			'foreignKey' => 'order_status_id',
			'counterCache' => 'count_order'
		],
		'JobStatus' => [
			'className' => 'JobStatus',
			'foreignKey' => 'job_status_id',
			'counterCache' => 'count_order'
		],
		'DeliveryOption' => [
			'className' => 'DeliveryOption',
			'foreignKey' => 'delivery_option_id',
			'counterCache' => 'count_order'
		],
		'TurnaroundOption' => [
			'className' => 'TurnaroundOption',
			'foreignKey' => 'turnaround_option_id',
			'counterCache' => 'count_order'
		],
		'Source' => [
			'className' => 'Source',
			'foreignKey' => 'source_id',
			'counterCache' => 'count_order'
		],
		'PaymentMethod' => [
			'className' => 'PaymentMethod',
			'foreignKey' => 'payment_method_id',
			'counterCache' => 'count_order'
		],
		'ShippingAddress' => [
			'className' => 'UserAddress',
			'foreignKey' => 'shipping_address_id',
			'counterCache' => 'count_order'
		],
		'BillingAddress' => [
			'className' => 'UserAddress',
			'foreignKey' => 'billing_address_id',
			'counterCache' => 'count_order'
		]
	];

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = [
		'OrderDate' => [
			'className' => 'OrderDate',
			'foreignKey' => 'order_id',
			'dependent' => false,
			'counterCache' => 'count_order_item'
		],
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'order_id',
			'dependent' => false,
			'counterCache' => 'count_order_item'
		],
		'PaypalPayment' => [
			'className' => 'PaypalPayment',
			'foreignKey' => 'order_id',
		]
	];

/**
	* Attach the Callas and Pace behaviours, which bring together all of the code for these services
*/
	public $actsAs = [
		'Callas',
		'Pace'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = [
		/*
		  'delivery_option_id' => array(
		  'required' => array(
		  'rule' => 'notBlank',
		  'message' => 'Please specify the delivery option.',
		  'last' => true
		  )
		  ),
		  'turnaround_option_id' => array(
		  'required' => array(
		  'rule' => 'notBlank',
		  'message' => 'Please specify the turnaround option.',
		  'last' => true
		  )
		  )
		 */
	];

	/**
	 * Constructor
	 *
	 * @param bool|string $id ID
	 * @param string $table Table
	 * @param string $ds Datasource
	 */
	public function __construct($id = false, $table = null, $ds = null) {

		$this->_setupValidation();
		parent::__construct($id, $table, $ds);
	}

	/**
	 * Setup validation rules
	 *
	 * @return void
	 */
	protected function _setupValidation() {

		$this->validateCheckout = [
			'accept_terms' => [
				'rule' => ['equalTo', 1],
				'message' => 'Please indicate that you accept the terms and conditions.'
			]
		];
	}

	public function beforeValidate($options = []) {

		if (isset($this->data['Order']['accept_terms'])) {

			$this->data['Order']['accept_terms'] = intval($this->data['Order']['accept_terms']);
		}

		return true;
	}

	public function beforeSave($options = []) {

		if (!empty($this->data['Order']['id'])) {

			$this->orderBeforeSave = $this->find(
				'first',
				[
					'conditions' => [
						'Order.id' => $this->data['Order']['id']
					],
					'fields' => $this->requoteFields
				]
			);

		} else {

			$order = $this->data['Order'];

			$this->orderBeforeSave = $order;

		}

		return parent::beforeSave();

	}

	var $requoteFields = [
		'turnaround_option_id',
		'delivery_option_id'
	];

	public function defaultQuote($options = ['refresh' => false]) {

		// Set a cacheKey variable as we use it in a few places - makes code management easier
		$cacheKey = 'default_quote';

		// Store the default in the cache with the id of 'default'
		$cacheId = 'default';

		if ($options['refresh']) {

			Cache::delete($cacheId, $cacheKey);

		}

		$model = $this;

		// Find the full on order and store it in the cache under the orders key
		return Cache::remember(
			$cacheId,
			function() use ($model, $options) {

				$defaultFields = [
					'pace_quote_id' => [
						'value' => null
					],
					'order_status_id' => [
						'modelName' => 'OrderStatus',
						'fieldName' => 'id'
					],
					'job_status_id' => [
						'modelName' => 'JobStatus',
						'fieldName' => 'id'
					],
					'turnaround_option_id' => [
						'modelName' => 'TurnaroundOption'
					],
					'delivery_option_id' => [
						'modelName' => 'DeliveryOption'
					],
					'count_order_item' => [
						'value' => 0
					],
					'amount_total' => [
						'value' => 0
					]
				];

				foreach ($defaultFields as $defaultFieldName => $defaultField) {

					if (array_key_exists('value', $defaultField)) {
						$defaultQuote['Order'][$defaultFieldName] = $defaultField['value'];
					} else {

						$options = [
							'fieldName' => isset($defaultField['fieldName'])
								? $defaultField['fieldName']
								: 'id'
							,
							'refresh' => $options['refresh']
						];

						if (isset($defaultField['criteria'])) {
							$options['criteria'] = $defaultField['criteria'];
						}

						$defaultQuote['Order'][$defaultFieldName] = $this->{$defaultField['modelName']}->getDefault(
							$options
						);
					}
				}

				$defaultQuote['success'] = true;

				$defaultQuote['OrderItem'][0] = $this->OrderItem->defaultQuote(
					['refresh' => $options['refresh']]
				);

				return $defaultQuote;

			},
			$cacheKey

		);

	}

	public function setStatus($orderStatusId = null, $orderId = null) {

		//We must have an order id
		if (!$orderId) {
			return false;
		}

		// If no status is passed...
		if (!$orderStatusId) {

			// If it doesn't exist, return
			return true;

		}

		// Check the passed status exists
		$this->OrderStatus->id = $orderStatusId;
		$statusExists = $this->OrderStatus->exists();

		if (!$statusExists) {
			// If it doesn't exist, return
			return true;

		}

		// What's the current status
		$currentOrderStatusId = $this->field(
			'order_status_id',
			['Order.id' => $orderId]
		);

		// Is it already where we want it to be?
		if (!$currentOrderStatusId || $currentOrderStatusId !== $orderStatusId) {

			$this->id = $orderId;

			$data = [
				'order_status_id' => $orderStatusId,
				'source' => 'set_status'
			];

			$this->save(
				$data,
				[
					'validate' => false,
					'callbacks' => false
				]
			);

			$this->OrderDate->add(
				$orderId,
				$orderStatusId
			);

		}

		return true;
	}

	public function setDefaultPaymentMethod($order) {

		if (isset($order['Order']['payment_method_id']) && !empty($order['Order']['payment_method_id'])) {
			return $order;
		}

		$order['Order']['payment_method_id'] = $this->PaymentMethod->getDefault();

		return $order;
	}

	/*
	 * The quoteParameters function gets the data for the selects and inputs on the quote calculator form
	 */

	public function quoteParameters($refresh = false) {

		// This array specifies the variables we need to build
		// - each key is the name of a variable that will be extracted elsewhere
		// - modelName specifies which model we have to query for the data
		// - when present, we just get the value of variableName from the model, otherwise
		// - we get the fields specified by 'fields'
		// - when there is a conditions key we use those to further filter the search
		// - by default, the filter also includes 'is_active' => true
		$quoteParameters = [
			'deliveryOptions' => [
				'modelName' => 'DeliveryOption'
			],
			'turnaroundOptions' => [
				'modelName' => 'TurnaroundOption'
			],
			'products' => [
				'modelName' => 'OrderItem.Product',
				'fields' => [
					'id',
					'name',
					'binding_name'
				]
			],
			'sides' => [
				'modelName' => 'OrderItem.Side'
			],
			'clicks' => [
				'modelName' => 'OrderItem.Click'
			],
			'drillingTypes' => [
				'modelName' => 'OrderItem.DrillingType'
			]
		];

		// Loop through the keys
		foreach ($quoteParameters as $tableName => $parameters) {

			// Get the related model
			$relatedModel = $this->getRelatedModel($parameters['modelName']);

			// Get the quote Options from the model
			$result[$tableName] = $relatedModel->quoteOptions($tableName, $refresh, $parameters);
		}

		return $result;
	}

	public function rebaseOrderItems($order) {

		if (isset($order['OrderItem'])) {
			$order['OrderItem'] = array_merge($order['OrderItem']);
		}

		return $order;
	}

	public function quote($data = [], $orderItemKey = 0) {

		// First check we have some data
		if (!$data) {

			return [
				'success' => false,
				'code' => 'no-quote-data',
				'message' => 'No quote data passed.',
				'data' => $data
			];
		}

		// Get the turnaround code
		$data['Order']['turnaround'] = $this->TurnaroundOption->convertToPaceCode($data['Order']['turnaround_option_id']);

		$orderItem = $data['OrderItem'][$orderItemKey];

		$orderItemId = !empty($orderItem['id'])
			? $orderItem['id']
			: null
		;

		// Is there a product?
		if (!$orderItem['product_id']) {
			return [
				'success' => false,
				'code' => 'no-product-id',
				'message' => 'No product key.',
				'data' => $data
			];
		}

		// Store the IP address and and session id to help with tracking
		if (empty($data['Order']['ip_address'])) {
			$data['Order']['ip_address'] = $this->getIpAddress();
		}

		if (empty($data['Order']['session_id'])) {
			$data['Order']['session_id'] = session_id();
		}

		// If the user hasn't given this job a friendly name, default it to the product name
		if (empty($orderItem['name'])) {
			$orderItem['name'] = $this->OrderItem->Product->field(
				'name',
				['Product.id' => $orderItem['product_id']]
			);
		}

		// Now set the order item back
		$data['OrderItem'][$orderItemKey] = $orderItem;

		// Now get the quote
		$paceQuote = $this->getQuote($data, $orderItemKey);

		if (!$paceQuote || !$paceQuote['success']) {
			return $paceQuote;
		}

		// Store the quote result
		$data = $this->mergeQuote($data, $paceQuote, $orderItemId, $orderItemKey);

		// Get the despatch data
		$data = $this->appendDespatchDetails($data);

		// Set the status of the order to quote
		$data['Order']['order_status_id'] = 'quote';

		// Set the status of the job to quote
		$data['Order']['job_status_id'] = 'incomplete';

		if (empty($data['Order']['id'])) {
			$this->create();
		}

		$result = $this->saveAssociated($data, ['deep' => true]);

		$orderId = $this->id;

		$orderKeys = $this->orderKeysStart($orderId);

		// The pricing will come from PACE
		$this->calculateOrderTotals($orderId);

		$order = $this->getOrder($orderId);

		$order = $this->addHolidayAlert($order);

		$order['success'] = true;

		return $order;
	}

	private function addHolidayAlert($order = []) {

		// See if we have any holiday alerts to add
		$order['HolidayAlert'] = [];

		// Load the Holiday model
		App::import('model', 'HolidayAlert');

		// Start a new instance
		$this->HolidayAlert = new HolidayAlert();

		$date = $this->getTimestamp();

		$holidayAlert = $this->HolidayAlert->message($date);

		if ($holidayAlert) {
			$order['HolidayAlert'] = $holidayAlert['HolidayAlert'];
		}

		return $order;

	}

	public function requote($orderId = null, $orderItemId = null) {

		$order = $this->find(
			'first',
			[
				'conditions' => [
					'Order.id' => $orderId
				],
				'contain' => [
					'OrderItem' => [
						'conditions' => [
							'OrderItem.id' => $orderItemId
						]
					]
				]
			]
		);

		if (!empty($order['OrderItem'][0])) {

			$quote = $this->quote($order, 0);

			return $quote;
		}

		return false;
	}

	public function appendDespatchDetails($data) {

		$despatchDetails = $this->latestDespatchDateForOrder(
			$data['OrderItem'], $data['Order']['turnaround_option_id']
		);

		// Based on the despatch date, when could it be delivered?
		$deliveryDetails = $this->DeliveryOption->deliveryDetails(
			$data['Order']['delivery_option_id'], $despatchDetails['despatch_date_obj']
		);

		// Drop the PHP date arrays as we don't need them
		unset($despatchDetails['despatch_date_obj']);
		unset($deliveryDetails['delivery_date_obj']);

		// Merge the details in
		$data['Order'] = array_merge(
			$data['Order'],
			$despatchDetails,
			$deliveryDetails
		);

		// Return the data
		return $data;
	}

	/**
	 * This function examines an order and its order items -> order item files -> order item file pages
	 * It does some basic checks to see if things are ready for the user to configure it
	 * Some things need the system to fix it, in which case 'user_configure' will be set to false.
	 * If it's something the user needs to make a decision on, 'user_configure' will be set to true.
	 */
	public function getOrderFixFailures($orderId = null) {

		// No order id passed in
		if (!$orderId) {

			return [
				'success' => false,
				'code' => 'no-order-id',
				'action' => 'quote'
			];
		}

		// Get the order
		$order = $this->getOrder($orderId);

		// No order items
		if (empty($order['OrderItem'])) {

			return [
				'success' => false,
				'code' => 'invalid-order-id',
				'action' => 'quote',
				'from' => 'Order.getOrderFixFailures'
			];
		}

		$fixFailures = [];

		// Loop through the order items
		foreach ($order['OrderItem'] as $orderItemKey => $orderItem) {

			// No files
			if (empty($orderItem['OrderItemFile'])) {

				return [
					'success' => false,
					'invalid_order_item_key' => $orderItemKey,
					'code' => 'no-order-item-file',
					'action' => 'upload'
				];
			}

			foreach ($orderItem['OrderItemFile'] as $orderItemFileKey => $orderItemFile) {

				// Check some basics, like sent to Callas, has pages and so on
				// Not sent to Callas yet
				if (!$orderItemFile['sent_to_callas'] || !$orderItemFile['callas_info']) {

					return [
						'success' => false,
						'code' => 'not-sent-to-callas',
						'invalid_order_item_key' => $orderItemKey,
						'invalid_order_item' => $orderItem,
						'action' => 'process-files',
						'from' => 'Order.getOrderFixFailures'
					];
				}

				if (empty($orderItemFile['OrderItemFilePage'])) {

					// No pages

					return [
						'success' => false,
						'code' => 'no-order-item-file-pages',
						'invalid_order_item_key' => $orderItemKey,
						'invalid_order_item' => $orderItem,
						'action' => 'process-files',
						'from' => 'Order.getOrderFixFailures'
					];
				}
			}

			$fixFailures = $this->OrderItem->getOrderItemFixFailures($orderItem, $orderItemKey);

			if (empty($fixFailures['success'])) {
				return $fixFailures;
			}

		}

		return ['success' => true];
	}

	public function completePreviews($order = []) {

		if (!$order) {
			return false;
		}


		if (empty($order['OrderItem'])) {
			return false;
		}

		foreach ($order['OrderItem'] as $orderItem) {

			if (empty($orderItem['OrderItemFile'])) {
				return false;
			}

			foreach ($orderItem['OrderItemFile'] as $file) {

				$orderItemFile['OrderItemFile'] = $file;

				// Call the completePreviews method of the Callas behavior
				$response = $this->completePreviews($orderItemFile);

				// $pageRanges will be empty if no previews have been triggered
				// If not empty, the keys are the ods of the pages that have had their preview triggered
				if (!empty($response['preview_page_ids'])) {

					$pageIds = array_flip($response['preview_page_ids']);

					// Set the has_preview field to true
					$this->OrderItem->OrderItemFile->OrderItemFilePage->updateHasPreview($pageIds);
				}
			}
		}

		return true;
	}

	public function getOrder($orderId = null, $options = []) {

		$defaults = [
			'refresh' => true,
			'createDefault' => true,
			'includeOrderContains' => true,
			'includeOrderItemContains' => true,
			'containUser' => false
		];

		$options = array_merge(
			$defaults,
			$options
		);

		// Turn the $options array into variables
		extract($options);

		if (!$orderId) {

			if (!$createDefault) {

				return [
					'success' => false,
					'code' => 'invalid-order-id',
					'order-id' => $orderId,
					'from' => 'Order.getOrder'
				];

			} else {

				// Get the default quote (which is cache managed)
				return $this->defaultQuote();

			}

		}

		// Are we setting the status of this order? The status is derived from $referer
		if (!empty($referer)) {
			// Yes, so set it...
			$this->setStatus($referer, $orderId);
			//... and then set a flag that'll force the cache to be refreshed
			$options['refresh'] = true;
		}

		// Set a cacheKey variable as we use it in a few places - makes code management easier
		$cacheKey = 'orders';

		if ($refresh) {

			Cache::delete($orderId, $cacheKey);

		}

		$model = $this;

		// Find the full on order and store it in the cache under the orders key
		$order = Cache::remember(
			$orderId,
			function() use ($model, $orderId) {

				if ($orderId) {

					$contain = [
						'OrderItem' => [
							'Product' => [
								'BindingType',
								'ProductTabType'
							],
							'Binding',
							'Media' => [
								'PaperSize'
							],
							'DrillingType',
							'CoverTypeInnerFront',
							'CoverTypeInnerBack',
							'CoverTypeOuterFront',
							'CoverTypeOuterBack',
							'CoverTypeWrapAround',
							'Side',
							'Click',
							'BindingEdgeType',
							'BindingSideType',
							'OrderItemFile' => [
								'order' => [
									'OrderItemFile.sequence',
									'OrderItemFile.created'
								],
								'OrderItemFilePage' => [
									'order' => [
										'OrderItemFilePage.callas_nr'
									]
								]
							],
							'OrderItemTab'
						],
						'DeliveryOption',
						'TurnaroundOption',
						'PromoCode',
						'PaypalPayment',
						'User',
						'ShippingAddress' => [
							'Country'
						],
						'BillingAddress' => [
							'Country'
						]
					];

					$order = $this->find(
						'first',
						[
							'conditions' => [
								'Order.id' => $orderId
							],
							'contain' => $contain
						]
					);

					if (isset($order['User']['password'])) {
						unset($order['User']['password']);
					}

					// We always need at least a defult order item
					if (empty($order['OrderItem'])) {

						$order['OrderItem'][] = $this->OrderItem->defaultQuote();

						if ($orderId) {
							$order['OrderItem'][0]['order_id'] = $orderId;
						}

					}

					return $order;
				}

			},
			$cacheKey
		);

		// Now we've got and have stored the order in cache
		// This request might not need all of it though, so clean it before returning it
		// The full order is still cached

		if (!$includeOrderContains) {

			$keys = [
				'PromoCode',
				'DeliveryOption',
				'TurnaroundOption',
				'PaypalPayment'
			];

			foreach ($keys as $key) {

				if (isset($order[$key])) {
					unset($order[$key]);
				}

			}

		}

		if (!$includeOrderItemContains) {

			$keys = [
				'Product',
				'Binding',
				'Media',
				'DrillingType',
				'CoverTypeInnerFront',
				'CoverTypeInnerBack',
				'CoverTypeOuterFront',
				'CoverTypeOuterBack',
				'CoverTypeWrapAround',
				'Side',
				'Click',
				'BindingEdgeType',
				'BindingSideType',
				'OrderItemFile',
				'OrderItemTab'
			];

			foreach ($keys as $key) {

				foreach ($order['OrderItem'] as $orderItemKey => $orderItem) {

					if (isset($order['OrderItem'][$orderItemKey][$key])) {
						unset($order['OrderItem'][$orderItemKey][$key]);
					}
				}

			}

		}

		if (!$containUser) {

			$keys = [
				'User',
				'ShippingAddress',
				'BillingAddress'
			];

			foreach ($keys as $key) {
				if (isset($order[$key])) {
					unset($order[$key]);
				}
			}

		}

		return $order;

	}

	public function adminGetOrder($orderId = null) {

		if ($orderId) {

			$contain = [
				'OrderStatus',
				'JobStatus',
				'ShippingAddress',
				'BillingAddress',
				'PromoCode',
				'PaypalPayment',
				'OrderItem' => [
					'Product' => [
						'BindingType'
					],
					'Binding',
					'Media' => [
						'PaperSize'
					],
					'DrillingType',
					'CoverTypeInnerFront',
					'CoverTypeInnerBack',
					'CoverTypeOuterFront',
					'CoverTypeOuterBack',
					'Side',
					'Click',
					'OrderItemFile' => [
						'OrderItemFileProcess' => [
							'Process'
						],
						'OrderItemFilePage' => [
							'order' => [
								'OrderItemFilePage.callas_nr'
							],
							'OrderItemFilePageProcess' => [
								'Process'
							]
						]
					],
					'OrderItemTab',
					'OrderItemProcess' => [
						'Process'
					]
				],
				'DeliveryOption',
				'TurnaroundOption',
				'OrderDate' => [
					'OrderStatus'
				]
			];

			$order = $this->find(
				'first',
				[
					'conditions' => ['Order.id' => $orderId],
					'contain' => $contain
				]
			);

			return $order;
		}

		return [];
	}

	public function cartDetails($orderId = null) {

		$cart = [
			'display' => false,
			'count_order_item' => 0,
			'amount_total' => 0
		];

		if (!$orderId) {
			$cart['display'] = false;
			return $cart;
		}

		$order = $this->find(
			'first',
			[
				'conditions' => [
					'Order.id' => $orderId
				],
				'fields' => [
					'Order.id',
					'Order.pace_job_number',
					'Order.order_status_id',
					'Order.count_order_item',
					'Order.amount_net',
					'Order.amount_delivery',
					'Order.amount_turnaround',
					'Order.amount_discount',
					'Order.amount_vat',
					'Order.amount_total',
					'Order.amount_due'
				],
				'contain' => [
					'OrderStatus.show_cart'
				]
			]
		);

		$order['Order']['display'] = isset($order['OrderStatus']['show_cart'])
			? $order['OrderStatus']['show_cart']
			: false
		;

		if (!$order || !$order['Order']['display']) {
			return $cart;
		}

		return $order['Order'];

	}

	/*
	 * The getOrderItemPaceCode function takes a model name and an id. It tries to find the row with that id in that model and handles checks consistently.
	 *
	 */

/*
	private function getOrderItemPaceCode($modelName = null, $id = null, $codeName = null) {

		if (!$modelName || !$id || !$codeName) {
			return array(
				'success' => false,
				'code' => 'missing-parameter',
				'from' => 'Order.getOrderItemPaceCode'
			);
		}

		$data = $this->OrderItem->{$modelName}->find(
			'first', array(
				'conditions' => array(
					$modelName . '.id' => $id
				),
				'fields' => array(
					$modelName . '.id',
					$modelName . '.is_active'
				)
			)
		);

		// Did we find the row and is it active?
		if (!$data) {
			return array(
				'success' => false,
				'code' => 'invalid-' . $codeName,
				'from' => 'Order.getOrderItemPaceCode'
			);
		} elseif (!$data[$modelName]['is_active']) {
			return array(
				'success' => false,
				'code' => 'inactive-' . $codeName,
				'from' => 'Order.getOrderItemPaceCode'
			);
		}

		return array(
			'success' => true,
			'id' => $data[$modelName]['id']
		);
	}
*/

	public function calculateOrderTotals($orderId = null) {

		$amountNet = $this->OrderItem->orderItemTotal($orderId);

		$order = $this->find(
			'first',
			[
				'conditions' => [
					'Order.id' => $orderId
				],
				'fields' => [
					'Order.id',
					'Order.amount_discount',
					'Order.amount_delivery',
					'Order.amount_turnaround',
					'Order.amount_discount',
					'Order.delivery_option_id',
					'Order.amount_paid',
					'Order.amount_due',
				]
			]
		);

		// hard coded for now - until we decide if/how we will apply VAT in Pace
		$order['Order']['amount_vat'] = 0;

		$amountTurnaround = $order['Order']['amount_turnaround'];
		$amountDelivery = $order['Order']['amount_delivery'];
		// Discounts are stored as negative numbers
		$amountDiscount = $order['Order']['amount_discount'];

		$order['Order']['amount_net'] = $amountNet;

		// Assuming all items are zero rated for now. Might change as PACE is updated.
		$order['Order']['amount_vat'] = 0;

		$amountVat = $order['Order']['amount_vat'];

		// Discounts are stored as negative numbers
		$order['Order']['amount_total'] = ($amountNet + $amountTurnaround + $amountDelivery + $amountVat) - $amountDiscount;

		// The dispatch amount is totally here for later convenience and is not included in the total price
		$order['Order']['amount_despatch'] = $amountDelivery + $amountTurnaround;

		$order['Order']['amount_due'] = $order['Order']['amount_total'] - $order['Order']['amount_paid'];

		$this->save($order);

	}



	public function removeOrderItem($orderItemId = null) {

		if (!$orderItemId) {
			return [
				'success' => false,
				'code' => 'no-order-item-id',
				'message' => 'No order item id provided.',
				'from' => 'Order.removeOrderItem'
			];
		}

		$orderItem = $this->OrderItem->find(
			'first',
			[
				'conditions' => [
					'OrderItem.id' => $orderItemId
				],
				'contain' => [
					'Order' => [
						'fields' => [
							'Order.id',
							'Order.pace_quote_id',
							'Order.delivery_option_id',
							'Order.turnaround_option_id'
						]
					]
				]
			]
		);

		if (!$orderItem) {
			return [
				'success' => false,
				'code' => 'no-order-item',
				'message' => 'That is an invalid order item id.',
				'from' => 'Order.removeOrderItem'
			];
		}

		$paceQuoteProductId = $orderItem['OrderItem']['pace_quote_product_id'];
		$paceQuoteId = $orderItem['Order']['pace_quote_id'];
		$orderId = $orderItem['OrderItem']['order_id'];
		$order = $orderItem['Order'];

		if (!$paceQuoteProductId || !$paceQuoteId) {

			return [
				'success' => false,
				'code' => 'no-pace-quote-ids',
				'message' => 'Invalid or missing Pace quote ids.',
				'from' => 'Order.removeOrderItem'
			];
		}

		$paceResponse = $this->removeFromQuote($paceQuoteProductId, $paceQuoteId, $order);

		if (!$paceResponse['success']) {
			return $paceResponse;
		}

		$this->OrderItem->OrderItemTab->removeFromOrderItem($orderItemId);
		$this->OrderItem->OrderItemFile->removeFromOrderItem($orderItemId);
		$this->OrderItem->delete($orderItemId);

		$this->calculateOrderTotals($orderId);

		$order = $this->getOrder($orderId);

		if (empty($order['OrderItem'])) {
			$orderItem = $this->OrderItem->defaultQuote();
			$orderItem['order_id'] = $orderId;
			$order['Order']['new'] = true;
			$order['OrderItem'][] = $orderItem;
		}

		return [
			'success' => true,
			'order' => $order
		];
	}

	public function applyPromoCode($order = []) {

		$orderId = $order['Order']['id'];

		if (!empty($order['PromoCode']['code'])) {

			$promoCodeId = $this->PromoCode->field(
				'id',
				['LOWER(PromoCode.code)' => strtolower($order['PromoCode']['code'])]
			);

			if ($promoCodeId) {
				// If the promo code doesn't exist this will effectivley clear it
				$order['Order']['promo_code_id'] = $promoCodeId;
			} else {

				return [
					'success' => false,
					'code' => 'invalid-promo-code',
					'message' => 'That is an invalid promo code',
					'flash_key' => 'promo-code'
				];

			}

		}

		$discount = $this->PromoCode->getDiscount($order);

		if (empty($discount['success'])) {
			$this->removePromoCode($orderId);
			return $discount;
		}

		if (empty($promoCodeId)) {
			$promoCodeId = $discount['promo_code_id'];
		}

		$this->id = $orderId;

		$data = [
			'promo_code_id' => $promoCodeId,
			'amount_discount' => $discount['amount_discount']
		];

		$save = $this->save(
			$data,
			[
				'validate' => false,
				'callbacks' => false
			]
		);

		if (!$save) {
			return [
				'success' => false,
				'code' => 'save-failed',
				'discount' => $discount
			];
		}

		$this->calculateOrderTotals($orderId);

		return [
			'success' => true,
			'discount' => $discount
		];

	}

	public function removePromoCode($orderId = null) {

		if (!$orderId) {
			return [
				'success' => false,
				'message' => 'That is an invalid order id.'
			];
		}

		$this->id = $orderId;

		$data = [
			'promo_code_id' => null,
			'amount_discount' => 0
		];

		$this->save(
			$data,
			[
				'validate' => false,
				'callbacks' => false
			]
		);

		$this->calculateOrderTotals($orderId);

		return [
			'success' => true,
			'message' => [
				'heading' => 'The promo code has been removed from this order',
				'message' => 'You can add a different promo code if you have one.'
			]
		];

	}

	public function latestDespatchDateForOrder($orderItems = [], $turnaroundOptionId = null, $orderTime = null) {

		// What's the time, Mr Wolf?
		if (!$orderTime) {
			$orderTime = $this->getTimestamp();
		}

		// Let's assume we can despatch immediately
		$despatchDate = $orderTime->format('Y-m-d');

		// What products are in the order?
		$productIds = Hash::extract($orderItems, '{n}.product_id');

		// What are the binding types of the products?
		$bindingTypeIds = $this->OrderItem->Product->find(
			'list',
			[
				'conditions' => [
					'Product.id' => $productIds
				],
				'fields' => [
					'Product.binding_type_id',
					'Product.binding_type_id'
				]
			]
		);

		foreach ($bindingTypeIds as $bindingTypeId) {

			// When can that be despatched?
			$despatchDetails = $this->TurnaroundOption->despatchDate(
				$turnaroundOptionId,
				$orderTime,
				$bindingTypeId
			);

			// If this despatch date is later than the current latest (or we don't have one), update it
			if (empty($latestDespatchDetails) || $despatchDetails['despatch_date'] > $despatchDate) {
				$despatchDate = $despatchDetails['despatch_date'];
				$latestDespatchDetails = $despatchDetails;
			}
		}

		return $latestDespatchDetails;
	}

	/*
	  public function despatchDetails($turnaroundOptionId = null, $deliveryOptionId = null, $bindingTypeId = null) {

	  if (!$turnaroundOptionId || !$deliveryOptionId || !$bindingTypeId) {
	  return false;
	  }

	  $time = new DateTime('NOW');

	  $despatchDetails = $this->TurnaroundOption->despatchDate(
	  $turnaroundOptionId,
	  $time,
	  $bindingTypeId
	  );

	  $deliveryDetails = $this->DeliveryOption->deliveryDetails(
	  $deliveryOptionId,
	  $despatchDetails['despatch_date_obj']
	  );

	  $result = array_merge(
	  $despatchDetails,
	  $deliveryDetails
	  );

	  return $result;

	  }
	 */

	public function processCallas($orderId = null) {

		if (!$orderId) {
			return [
				'success' => false,
				'code' => 'no-order-id',
				'from' => 'Order.processCallas'
			];
		}

		$order = $this->find(
			'first',
			[
				'conditions' => ['Order.id' => $orderId],
				'fields' => [
					'Order.id'
				],
				'contain' => [
					'OrderItem' => [
						'fields' => [
							'OrderItem.id',
							'OrderItem.order_id'
						]
					]
				]
			]
		);

		if (empty($order['OrderItem'])) {
			return [
				'success' => false,
				'code' => 'no-order-items',
				'from' => 'Order.processCallas'
			];
		}

		foreach ($order['OrderItem'] as $orderItem) {

			$response = $this->OrderItem->processCallas($orderItem['id']);

		}

		return $response;

	}

	/**
	 * This function looks for the Callas output files.
	 */
	public function getCallasInfo($order = []) {

		if (!$order) {
			return [
				'success' => false,
				'code' => 'no-order',
				'order' => $order
			];
		}

		$orderId = $order['Order']['id'];

		if (!empty($order['OrderItem'])) {

			$data = [];

			foreach ($order['OrderItem'] as $orderItem) {

				if (!empty($orderItem['OrderItemFile'])) {

					foreach ($orderItem['OrderItemFile'] as $file) {

						// We have collected this as $file as we need to send through a properly formed $orderItemFile
						$orderItemFile['OrderItemFile'] = $file;

// 						$orderItemFile['OrderItemFile']['order_id'] = $orderId;

						$orderItemFileId = $orderItemFile['OrderItemFile']['id'];

						if (!$orderItemFile['OrderItemFile']['sent_to_callas']) {

							// This file hasn't been sent to Callas
// 							$response = $this->sendToCallas($orderItemFile);
							$this->sendToCallas(
								$orderItemFile,
								$orderId
							);
						}

						$response = $this->OrderItem->OrderItemFile->getCallasXml($orderItemFile);

						if (!$response['success']) {

							if ($response['code'] == 'no-callas-info-file') {

								// Sometimes the Callas process just doesn't work, so send it again
								$response = $this->sendToCallas($orderItemFile);
							} else {

								return $response;
							}
						}

						if (!$response['success']) {

							return $response;
						}

						if (empty($response['xml'])) {

							return [
								'success' => false,
								'code' => 'no-callas-xml',
								'order' => $order,
								'response' => $response
							];
						} else {

							$callasInfoFile = $response['xml'];

							if ($response['source'] !== 'database') {

								$response = $this->OrderItem->OrderItemFile->saveCallasInfoFile(
									$orderItemFileId,
									$callasInfoFile
								);

								if (!$response['success']) {
									return $response;
								}

								$response = $this->OrderItem->OrderItemFile->extractPages(
									$orderItemFileId,
									$callasInfoFile
								);

							}

						}

						return $response;

					}
				}
			}
		}

		return ['success' => true];
	}

	public function checkout($order = [], $loggedInUserId = null) {

		if (empty($order['Order'])) {
			return [
				'success' => false,
				'code' => 'no-order-data',
				'message' => 'That is an invalid order.'
			];
		}

		if (empty($order['Order']['shipping_address_id']) && empty($order['ShippingAddress'])) {

			return [
				'success' => false,
				'code' => 'no-order-delivery-address',
				'message' => 'Please provide your delivery and billing details.'
			];
		}

		$dataSource = $this->getDataSource();
		$dataSource->useNestedTransactions = true;

		$dataSource->begin();

		$orderId = $order['Order']['id'];

		// Does the data contain a user id?
		$orderUserId = !empty($order['Order']['user_id'])
			? $order['Order']['user_id']
			: null
		;

		$userOK = true;
		$overridePostCodeCheck = true;

		if (!$orderUserId) {

			if ($loggedInUserId) {

				// If there's no user id in the order but there's a logged in user,
				// make the logged in user own the order

				$order['Order']['user_id'] = $loggedInUserId;

			} else {

				// There's no user id in the order and no user logged in
				// Try and create the user from the data

				$user['User'] = $order['User'];

				$newUser = $this->User->register($user);

				if (!$newUser['success']) {

					$dataSource->rollback();

					return $newUser;

				} else {

					$loggedInUserId = $newUser['User']['id'];
					$order['Order']['user_id'] = $loggedInUserId;
					$order['User']['id'] = $loggedInUserId;

					// If this is a returning user who hasn't logged in or didn't apply a password
					// we can't challenge for duplicate addresses by post code
					if (!empty($newUser['code'])) {
						$overridePostCodeCheck = $newUser['code'] == 'existing-user';
					}

				}

			}

		}

		$this->validate = $this->validateCheckout;

		$this->set($order['Order']);

		if (!$this->validates()) {

			$dataSource->rollback();

			return [
				'success' => false,
				'code' => 'failed-validation',
				'message' => 'Please address the following issues.',
				'errors' => $this->validationErrors
			];
		}

		$userId = $order['Order']['user_id'];

		if (!empty($order['PromoCode']['code'])) {

			$promoCodeId = $this->PromoCode->field(
				'id',
				['LOWER(PromoCode.code)' => strtolower($order['PromoCode']['code'])]
			);

			if ($promoCodeId) {
				// If the promo code doesn't exist this will effectivley clear it
				$order['Order']['promo_code_id'] = $promoCodeId;
			} else {

				$dataSource->rollback();

				return [
					'success' => false,
					'code' => 'invalid-promo-code',
					'message' => 'That is an invalid promo code',
					'flash_key' => 'promo-code'
				];

			}

		}

		if (!empty($order['Order']['billing_same_as_shipping'])) {
			unset($order['BillingAddress']);
		}

		// Save any and all addresses and update the order
		$addressKeys = [
			'ShippingAddress' => 'shipping_address_id',
			'BillingAddress' => 'billing_address_id'
		];

		// The UserAddress model has aliases of ShippingAddress and BillingAddress, so pass the right one in
		foreach ($addressKeys as $addressKey => $addressFieldName) {

			if (!empty($order[$addressKey])) {

				$userAddress[$addressKey] = $order[$addressKey];

				$userAddress[$addressKey]['user_id'] = $userId;

				$response = $this->User->{$addressKey}->add($userAddress, $overridePostCodeCheck);

				if (!$response['success']) {
					$dataSource->rollback();
					return $response;
				}

				$userAddress = $response['userAddress'];

				$order[$addressKey] = $userAddress[$addressKey];

				$order['Order'][$addressFieldName] = $userAddress[$addressKey]['id'];

			}

		}

		if (!empty($order['Order']['promo_code_id'])) {

			$appliedPromoCode = $this->applyPromoCode($order);

			if (empty($appliedPromoCode['success'])) {

				$dataSource->rollback();
				return $appliedPromoCode;
			}

			$promoCodeResult = [
				'message' => sprintf('Promo code %s has been applied to your order.', $appliedPromoCode['discount']['code']),
				'flash_key' => 'promo-code'
			];

			$order['Order']['amount_discount'] = $appliedPromoCode['discount']['amount_discount'];

		}

		// Save the order before calculating any discount
		$result = $this->save($order['Order']);

		if (!$result) {

			$dataSource->rollback();
			return [
				'success' => false,
				'code' => 'order-checkout-save-failed'
			];
		}

		$order = $this->getOrder(
			$orderId,
			[
				'includeOrderItemContains' => true,
				'containUser' => true
			]
		);

		// See if any fields necessitate a requote
		// Do this before promo codes in case relevant amounts change

		$requote = false;

		foreach ($this->requoteFields as $requoteField) {

			if ($this->orderBeforeSave['Order'][$requoteField] !== $order['Order'][$requoteField]) {

				$requote = true;

				// We've requoted, so bomb out
				continue;
			}

			if ($requote) {
				continue;
			}

		}

		if ($requote) {
			$result = $this->requote(
				$orderId,
				$orderItemId = $order['OrderItem'][0]['id']
			);
		}

		$order = $this->getOrder(
			$orderId,
			[
				'includeOrderItemContains' => true,
				'containUser' => true
			]
		);

		$result = [
			'success' => true,
			'order' => $order
		];

		$readyToPay = $this->readyToPay($order);

		if ($readyToPay !== true) {

			$result = array_merge(
				$result,
				[
					'success' => false,
					'missing_data' => $readyToPay
				]
			);
		}

		$dataSource->commit();

		if (!empty($promoCodeResult)) {
			$result['PromoCode'] = $promoCodeResult;
		}

		return $result;
	}

	public function convertPaceQuoteToJob($orderId = null) {

		if (!$orderId) {
			return [
				'success' => false,
				'code' => 'no-order-id',
				'from' => 'Order.convertPaceQuoteToJob',
				'message' => 'There was an error processing your order. Please try again.'
			];
		}

		$order = $this->getOrderForPace($orderId);

		$data = [
			'quote' => $order['Order']['pace_quote_id'],
			'customer' => $order['User']['pace_customer_id'],
			'billingContact' => !empty($order['BillingAddress']['pace_contact_id'])
				? $order['BillingAddress']['pace_contact_id']
				: $order['ShippingAddress']['pace_contact_id']
			,
			'shippingContact' => $order['ShippingAddress']['pace_contact_id'],
			'promiseDate' => $order['Order']['delivery_date'],
			'shippingDate' => $order['Order']['despatch_date']
		];

		if (!empty($order['PromoCode']['code'])) {
			$data = array_merge(
				$data,
				[
					'promoCode' => $order['PromoCode']['code'],
					'promoCodeDescription' => $order['PromoCode']['description'],
					'promoCodeAmount' => $order['Order']['amount_discount']
				]
			);
		}

		$quoteProducts = [];

		foreach ($order['OrderItem'] as $orderItem) {

			$quoteProducts[$orderItem['pace_quote_product_id']] = [];

			foreach ($orderItem['OrderItemFile'] as $orderItemFile) {

				$fileName = $orderItemFile['name'];
				$fileNameWithoutExtension = $orderItemFile['file_name_without_extension'];

				$quoteProducts[$orderItem['pace_quote_product_id']][] = [
					'filename' => $fileName,
					'pages' => $orderItemFile['count_order_item_file_page'],
					'fileUrl' => '[##PLACEHOLDER##]' . DS . $fileNameWithoutExtension . DS . 'final' . DS . $fileName
				];

			}

			// If there is a printed_cover_job_id for this orderItem, add that as a separate content file
			if ($orderItem['printed_cover_job_id']) {

				$quoteProducts[$orderItem['pace_quote_product_id']][] = [
					'filename' => 'cover.pdf',
					'pages' => 1,
					'fileUrl' => '[##PLACEHOLDER##]' . DS . $orderItem['OrderItemFile'][0]['file_name_without_extension'] . DS . 'final' . DS . 'cover.pdf'
				];

				// Now make the request to PrintUI to get the PDF and IDML files - the actual download process takes place elsewhere
				$pdfXML = $this->OrderItem->printuiConnect("requestpdf", ["id" => $orderItem['printed_cover_job_id'], "preset" => "PDF/X-4:2008"]);
				$pdfLocation = $pdfXML['loc'];

				$idmlXML = $this->OrderItem->printuiConnect("requestdocument", ["id" => $orderItem['printed_cover_job_id']]);
				$idmlLocation = $idmlXML['loc'];

				$coverData = [
					'id' => $orderItem['id'],
					'printed_cover_pdf_location' => $pdfLocation,
					'printed_cover_idml_location' => $idmlLocation
				];

				$this->OrderItem->save($coverData);

			}

		}

		$data['contentFiles'] = json_encode($quoteProducts);

		$response = $this->convertQuoteToJob($data);

		if (!$response['success'] || empty($response['response']['job'])) {
			return [
				'success' => false,
				'code' => 'pace-convert-quote-to-job-failed',
				'result' => $response,
				'message' => 'There was an error processing your order. Please try again.'
			];
		}

		$data = [
			'id' => $orderId,
			'pace_job_number' => $response['response']['job']
		];

		$result = $this->save(
			$data,
			[
				'callbacks' => false,
				'validation' => false
			]
		);

		return [
			'success' => true,
			'result' => $result
		];

	}

	function getOrderForPace($orderId) {

		$this->User->createInPace($orderId);

		$order = $this->find(
			'first',
			[
				'conditions' => ['Order.id' => $orderId],
				'fields' => [
					'Order.id',
					'Order.pace_quote_id',
					'Order.shipping_address_id',
					'Order.billing_address_id',
					'Order.despatch_date',
					'Order.delivery_date',
					'Order.promo_code_id',
					'Order.amount_discount'
				],
				'contain' => [
					'PromoCode' => [
						'fields' => [
							'PromoCode.id',
							'PromoCode.code',
							'PromoCode.description'
						]
					],
					'User' => [
						'fields' => [
							'User.id',
							'User.pace_customer_id',
						]
					],
					'ShippingAddress' => [
						'fields' => [
							'ShippingAddress.id',
							'ShippingAddress.pace_contact_id',
							'ShippingAddress.company_name',
							'ShippingAddress.address_1',
							'ShippingAddress.address_2',
							'ShippingAddress.address_3',
							'ShippingAddress.city',
							'ShippingAddress.post_code',
							'ShippingAddress.country_id'
						]
					],
					'BillingAddress' => [
						'fields' => [
							'BillingAddress.id',
							'BillingAddress.pace_contact_id',
							'BillingAddress.company_name',
							'BillingAddress.address_1',
							'BillingAddress.address_2',
							'BillingAddress.address_3',
							'BillingAddress.city',
							'BillingAddress.post_code',
							'BillingAddress.country_id'
						]
					],
					'OrderItem' => [
						'fields' => [
							'OrderItem.id',
							'OrderItem.pace_quote_product_id',
							'OrderItem.printed_cover_job_id'
						],
						'OrderItemFile' => [
							'fields' => [
								'OrderItemFile.id',
								'OrderItemFile.order_item_id',
								'OrderItemFile.name',
								'OrderItemFile.file_name_without_extension',
								'OrderItemFile.count_order_item_file_page'
							],
							'order' => 'OrderItemFile.sequence'
						]
					]
				]
			]
		);

		return $order;

	}

	public function readyToPay($order = []) {

		$missingData = [];

		if (!$order) {
			$missingData[] = 'no-order';
		}

		if (empty($order['Order']['user_id'])) {
			$missingData[] = 'no-user';
		}

		if (empty($order['Order']['shipping_address_id'])) {
			$missingData[] = 'no-delivery-address';
		}

		if (!$order['Order']['billing_same_as_shipping'] && empty($order['Order']['billing_address_id'])) {
			$missingData[] = 'no-billing-address';
		}

		if ($missingData) {

			return $missingData;
		}

		return true;
	}

	public function receivePayment($orderId = null, $paymentAmount = 0) {

		if (!$orderId) {

			return [
				'success' => false,
				'code' => 'no-order-id',
				'from' => 'Order.receivePayment'
			];
		}

		if (!$paymentAmount) {

			return [
				'success' => false,
				'code' => 'no-amount-paid',
				'from' => 'Order.receivePayment'
			];
		}

		$order = $this->findById($orderId);

		if (!$order) {

			return [
				'success' => false,
				'code' => 'no-order',
				'from' => 'Order.receivePayment'
			];
		}

		$amountDue = $order['Order']['amount_total'];
		$amountPaid = $order['Order']['amount_paid'];

		$amountPaid += $paymentAmount;
		$amountDue -= $paymentAmount;

		$this->id = $orderId;

		$this->set([
			'amount_paid' => $amountPaid,
			'amount_due' => $amountDue
		]);

		if (!$amountDue) {
			$this->set([
				'order_status_id' => 'paid',
				'job_status_id' => 'ready'
			]);
		}

		$result = $this->save();

		if ($result && !$amountDue) {
			// Tell Pace that the job has been paid for - which means it'll be awaiting files
			$this->setPaceJobStatus(
				$order['Order']['pace_job_number'],
				'awaiting_files'
			);
		}

		return [
			'success' => true,
			'amount_due' => $amountDue
		];
	}

	private $orderKeyDefaults = [
		'Order' => [
			'id' => null
		],
		'OrderItem' => [
			'key' => null,
			'id' => null
		],
		'OrderItemFile' => [
			'key' => null,
			'id' => null
		],
		'OrderItemTab' => [
			'key' => null,
			'id' => null
		]
	];

	/** This array defines a model's parent in the order keys array */
	private $orderKeysParentModels = [
		'OrderItemFile' => 'OrderItem'
	];

	/** This function returns the parent model name */
	public function orderKeysParentModelName($modelName = null) {

		if (!$modelName) {
			return null;
		}

		if (!empty($this->orderKeysParentModels[$modelName])) {
			return $this->orderKeysParentModels[$modelName];
		}

		return null;
	}

	public function orderKeysOrder($orderId = null) {

		if (!$orderId) {
			return [];
		}

		return $this->find(
			'first',
			[
				'conditions' => [
					'Order.id' => $orderId
				],
				'fields' => [
					'Order.id',
					'Order.count_order_item'
				],
				'contain' => [
					'OrderItem' => [
						'fields' => [
							'OrderItem.id',
							'OrderItem.count_order_item_file',
							'OrderItem.count_order_item_tab'
						],
						'OrderItemFile' => [
							'fields' => [
								'OrderItemFile.id',
								'OrderItemFile.count_order_item_file_page'
							],
							'OrderItemFilePage.id'
						],
						'OrderItemTab.id'
					]
				]
			]
		);
	}

	public function orderKeysStart($orderId = null) {

		$orderKeys = $this->orderKeyDefaults;

		if (!$orderId) {
			return $orderKeys;
		}

		$orderKeys['Order']['id'] = $orderId;

		$order = $this->orderKeysOrder($orderId);

		if (empty($order['OrderItem'])) {
			return $orderKeys;
		}

		// This loop will leave the values set at the last key
		foreach ($order['OrderItem'] as $orderItemKey => $orderItem) {
			$orderItemId = $orderItem['id'];
			$orderItemFile = $orderItem['OrderItemFile'];
			$orderItemTab = $orderItem['OrderItemTab'];
		}

		$orderKeys['OrderItem'] = [
			'key' => $orderItemKey,
			'id' => $orderItemId
		];

		if (!empty($orderItemFile)) {
			// This loop will leave the values set at the last key
			foreach ($orderItemFile as $orderItemFileKey => $orderItemFile) {
				$orderItemFileId = $orderItemFile['id'];
			}

			$orderKeys['OrderItemFile'] = [
				'key' => $orderItemFileKey,
				'id' => $orderItemFileId
			];
		}

		if (!empty($orderItemTab)) {
			// This loop will leave the values set at the last key
			foreach ($orderItemTab as $orderItemTabKey => $orderItemTab) {
				$orderItemTabId = $orderItemTab['id'];
			}

			$orderKeys['OrderItemTab'] = [
				'key' => $orderItemTabKey,
				'id' => $orderItemTabId
			];
		}

		return $orderKeys;
	}

	/**
	 * This function takes in the current order keys, a model name and an array of values (id/key).
	 * If both $key and $id are set, it sets those values inside the current values and returns them.
	 * If $id is set but not $key, it goes off and gets the key and sets them.
	 */
	public function orderKeysSetModel($currentValues = [], $modelName = null, $newValues = []) {

		$id = null;
		$key = null;

		// Get the id and key values, if they've been passed
		extract($newValues);

		if (!$modelName) {
			return $currentValues;
		}

		if ($id && $key === null) {

			$key = $this->orderKeysGetKey(
				$currentValues, $modelName, $id
			);

			if ($key === false) {

				return $currentValues;
			}
		}

		$currentValues[$modelName] = [
			'key' => $key,
			'id' => $id
		];

		return $currentValues;
	}

	public function orderKeysMoveToLast($orderKeysCurrent = [], $modelName = null) {

		if (!$orderKeysCurrent || empty($orderKeysCurrent['Order']['id']) || !$modelName) {
			return false;
		}

		$orderId = $orderKeysCurrent['Order']['id'];

		$parentModelName = $this->orderKeysParentModelName($modelName);

		$order = $this->orderKeysOrder($orderId);

		if (!empty($parentModelName)) {

			$parentModelKey = $orderKeysCurrent[$parentModelName]['key'];
			$model = $order[$parentModelName][$parentModelKey][$modelName];
		} else {

			$model = $order[$modelName];
		}

		$id = null;
		$key = null;

		if ($model) {
			foreach ($model as $key => $modelRow) {
				$id = $modelRow['id'];
			}
		}

		// We're now on the last row
		$currentValues = $this->orderKeysSetModel(
			$orderKeysCurrent,
			'OrderItemFile',
			[
				'key' => $key,
				'id' => $id
			]
		);

		return $currentValues;
	}

	/** Given a model key and an id, this returns the matching key */
	public function orderKeysGetKey($currentValues = [], $modelName = null, $id = null) {

		if (!empty($currentValues[$modelName]) && $currentValues[$modelName]['id'] == $id) {
			return $currentValues[$modelName]['key'];
		}

		$order = $this->orderKeysOrder($currentValues['Order']['id']);

		if (!$order) {
			return false;
		}

		$parentModelName = $this->orderKeysParentModelName($modelName);

		if (!empty($parentModelName)) {
			$parentModelKey = $currentValues[$parentModelName]['key'];
			$model = $order[$parentModelName][$parentModelKey][$modelName];
		} else {
			$model = $order[$modelName];
		}

		if (!$model) {
			return false;
		}

		foreach ($model as $modelKey => $modelRow) {

			if ($modelRow['id'] == $id) {

				return $modelKey;
			}
		}

		return false;
	}

	public function orderKeysGetModel($currentValues = [], $modelName = null) {

		if (!$modelName || !$currentValues) {
			return false;
		}

		if (!isset($currentValues[$modelName])) {
			return false;
		}

		return $currentValues[$modelName];
	}

	public function admin_totals_by_day() {

		$orders = $this->find(
			'all',
			[
				'fields' => [
					'DATE_FORMAT(Order.created, "%Y") AS year',
					'DATE_FORMAT(Order.created, "%c") AS month',
					'DATE_FORMAT(Order.created, "%e") AS day',
					'Order.order_status_id',
					'COUNT(Order.id) as order_count',
					'SUM(Order.amount_total) AS amount_net',
					'SUM(Order.amount_total) AS amount_delivery',
					'SUM(Order.amount_total) AS amount_turnaround',
					'SUM(Order.amount_total) AS amount_discount',
					'SUM(Order.amount_total) AS amount_vat',
					'SUM(Order.amount_total) AS amount_total',
					'SUM(Order.amount_total) AS amount_paid',
					'SUM(Order.amount_total) AS amount_due'
				],
				'group' => 'DATE_FORMAT(Order.created, "%Y"), DATE_FORMAT(Order.created, "%c"), DATE_FORMAT(Order.created, "%e"), Order.order_status_id',
				'contain' => 'OrderStatus.sequence'
			]
		);

		debug($orders);

		if (!$orders) {
			return [];
		}

		foreach ($orders as $order) {
			$year = $order[0]['year'];
			$month = $order[0]['month'];
			$day = $order[0]['day'];
			$orderStatusId = $order['OrderStatus']['sequence'];
			$result[$year][$month][$day][$orderStatusId] = $order;
		}

		ksort($result);
		die(debug($result));
		return ($result);

	}

	public function createJobArchives() {

		$orders = $this->find(
			'list',
			[
				'conditions' => [
					'Order.job_status_id' => 'processed'
//					'Order.pace_archive_created' => NULL
				]
			]
		);

		if (!$orders) {

			return [
				'success' => false,
				'code' => 'no-orders-to-archive',
				'message' => 'There are no orders to archive.'
			];

		}

		$result = [];

		foreach ($orders as $orderId) {

			$result[] = $this->createJobArchive($orderId);

		}

		return $result;

	}

	public function createJobArchive($orderId = null) {

		$order = $this->find(
			'first',
			[
				'conditions' => ['Order.id' => $orderId],
				'fields' => [
					'Order.id',
					'Order.pace_quote_id',
					'Order.pace_job_number'
				],
				'contain' => [
					'OrderItem' => [
						'fields' => [
							'OrderItem.id',
							'OrderItem.pace_quote_product_id',
							'OrderItem.printed_cover_job_id',
							'OrderItem.printed_cover_pdf_location',
							'OrderItem.printed_cover_idml_location'
						],
						'OrderItemFile' => [
							'fields' => [
								'OrderItemFile.id',
								'OrderItemFile.order_item_id',
								'OrderItemFile.name',
								'OrderItemFile.path_root',
								'OrderItemFile.file_name_without_extension',
								'OrderItemFile.count_order_item_file_page'
							],
							'order' => 'OrderItemFile.sequence'
						]
					]
				]
			]
		);

		$this->log("Create Job Archive for " . $order['Order']['pace_job_number'], 'debug');

		if (!$order) {
			return [
				'success' => false,
				'code' => 'invalid-order',
				'order-id' => $orderId
			];
		}

		// We need the Folder and File utilities
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');

		// Get the Pace ids
		$paceQuoteId = $order['Order']['pace_quote_id'];

		if (!$paceQuoteId) {
			return [
				'success' => false,
				'code' => 'no-pace-quote-id'
			];
		}

		$paceJobNumber = $order['Order']['pace_job_number'];

		if (!$paceJobNumber) {
			return [
				'success' => false,
				'code' => 'no-pace-job-number'
			];
		}

		// Tell Pace we're producing the zip file
		$this->setPaceJobStatus($paceJobNumber, 'awaiting_files');

		// Get the folder that contains the order's files
		$orderFolderName = $this->localFilesPath() . $paceQuoteId;

		// Iterate through OrderItem records to see if any of them have cover files to download
		foreach($order['OrderItem'] as $orderItem) {

			$orderItemFolderName = $orderItem['OrderItemFile'][0]['path_root'];

			if($orderItem['printed_cover_pdf_location']) {
				$pdfFilePath = $orderItemFolderName . DS . 'final' . DS . 'cover.pdf';
				//$this->log($pdfFilePath, 'debug');
				$pdfFile = new File($pdfFilePath, true);
				$pdfData = $this->OrderItem->printuiConnect("getpdf", ["id" => $orderItem['printed_cover_job_id'], "loc" => $orderItem['printed_cover_pdf_location']], false);
				$result = $pdfFile->append($pdfData);
				$pdfFile->close();
			}

			if($orderItem['printed_cover_idml_location']) {
				$idmlFilePath = $orderItemFolderName . DS . 'cover_designer' . DS . 'cover.idml';
				//$this->log($idmlFilePath, 'debug');
				$idmlFile = new File($idmlFilePath, true);
				$idmlData = $this->OrderItem->printuiConnect("getdocument", ["id" => $orderItem['printed_cover_job_id'], "loc" => $orderItem['printed_cover_idml_location']], false);
				$idmlFile->append($idmlData);
				$idmlFile->close();
			}

		}

		$orderFolder = new Folder($orderFolderName);

		// Get the destination folder for the zip file
		$paceJobsFolderName = $this->localPaceJobsPath();

		// Create folder to copy and manipulate the Pace quote id folders
		// we need to rename them to 0, 1, 2 etc before creating the zip
		$workingFolderName = $orderFolderName . '_working';

		// Now create an empty working folder
		$workingFolder = new Folder();
		$workingFolder->create($workingFolderName);

		// Get the tree structure
		$paceQuoteProductFolders = $orderFolder->read(true, [], true);

		$paceQuoteProductFolderNames = $paceQuoteProductFolders[0];

		foreach ($paceQuoteProductFolderNames as $key => $paceQuoteProductFolderName) {

			// Get a reference to the folder for this Pace quote product
			$paceQuoteProductFolder = new Folder($paceQuoteProductFolderName);

			// Pad the key with leading 0s
			$paddedKey = $this->padKey($key);

			// Now copy the folder
			$paceQuoteProductFolder->copy($workingFolderName . DS . $paddedKey);

		}

		// Initialize archive object
		$zip = new ZipArchive();

		// Open the zip file
		$zip->open($paceJobsFolderName . $paceJobNumber . '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator($workingFolderName),
			RecursiveIteratorIterator::LEAVES_ONLY
		);

		foreach ($files as $name => $file) {
			// Skip directories (they would be added automatically)
			if (!$file->isDir()) {
				// Get real and relative path for current file
				$filePath = $file->getRealPath();

				$relativePath = substr($filePath, strlen($workingFolderName) + 1);

				// Add current file to archive
				$zip->addFile($filePath, $relativePath);
			}
		}

		// Zip archive will be created only after closing object
		$zip->close();

		// Delete the working folder
//		$workingFolder->delete();

		// function getJobZipFile

			// Method: POST
			// Parameters (both required):

			// job - string/int The Pace job number for the job
			// zipSource - string The URL for the directory where the zip file is stored, e.g. http://test.doxdirect.com/order/pace_jobs

			// This will write a file to the API server which will give the instructions for a daemon process to then download the zip and extract it into the relevant job folder.

		// Now tell Pace the files are ready
		$result = $this->executePace(
			'get_job_zip_file',
			[
				'job' => $paceJobNumber,
				'zipSource' => $this->externalPathToJobs()
			]
		);

		$timestamp = $this->getTimestamp(['format' => true]);

		$this->id = $orderId;

		$data = [
			'pace_archive_created' => $timestamp,
			'job_status_id' => 'archived'
		];

		$this->save(
			$data,
			[
				'callbacks' => false,
				'validate' => false
			]
		);

		return $result;

	}

	/**
	 * This function takes an order, get its order items and runs their files through Callas, applying any necessary profiles. The aim is to take original files and create a set of finals.
	 */
	public function processOriginalFiles($orderId = null) {

		$conditions = [
			'Order.job_status_id' => 'ready'
		];

		if ($orderId) {
			$conditions['Order.id'] = $orderId;
		}

		$orders = $this->find(
			'list',
			[
				'conditions' => $conditions,
				'fields' => 'Order.id'
			]
		);

		if (!$orders) {

			return [
				'success' => true,
				'code' => 'no-orders-to-process',
				'from' => 'Order.processOriginalFiles'
			];

		}

		foreach ($orders as $orderId) {

			$result = $this->OrderItem->processOriginalFiles($orderId);

			$timestamp = $this->getTimestamp(['format' => true]);

			if ($result['success']) {

				$data = [
					'job_status_id' => 'processed',
					'job_status_message' => 'Files processed in Callas',
					'original_files_processed' => $timestamp
				];

			} else {

				$data = [
					'job_status_message' => 'Files FAILED to process in Callas:<br>' . json_encode($result),
					'original_files_processed' => $timestamp
				];

			}

			$this->id = $orderId;

			$result = $this->save(
				$data,
				[
					'validate' => false,
					'callbacks' => false
				]
			);

			return $result;

		}

	}

	public function checkBlankCover($order = []) {

		if (!$order) {
			return [
				'success' => false,
				'code' => 'invalid-order',
				'from' => 'Order.checkBlankCover'
			];
		}

		foreach ($order['OrderItem'] as $orderItem) {

			if (!$orderItem['printed_cover_job_id'] && $orderItem['Product']['BindingType']['use_cover_designer']) {

				$orderItemId = $orderItem['id'];

				// If no printed cover job ID has been created and it's a Hardback/Paperback product, create a blank cover
				$blankCoverJob = $this->OrderItem->createNewCoverDocument($orderItemId, true); // set boolean true flag for a blank cover

				if ($blankCoverJob['success']) {
					$this->OrderItem->updatePrintedCoverJobId(
						$orderItemId,
						$blankCoverJob['id']
					);
				} else {
					return $blankCoverJob;
				}

			}

		}

		return [
			'success' => true
		];

	}

	public function qualifiesForSameDayTurnaround($order = []) {

		if (!$order) {
			return false;
		}

		// When checking whether an order can have same day turnaround we need to examine the products in the order items. This field aggregates those into a single variable for ease.
		$qualifiesForSameDayTurnaround = true;

		// Find out if any order items are products that need extra processing days
		$extraProcessingDays = Hash::extract(
			$order,
			'OrderItem.{n}.Product.BindingType.extra_processing_day'
		);

		if ($extraProcessingDays) {

			// Loop through them...
			foreach ($extraProcessingDays as $extraProcessingDay) {

				if ($extraProcessingDay) {

					// This product needs extra processing days, so set the qualifies flag to false...
					$qualifiesForSameDayTurnaround = false;

					// And leave the loop
					continue;

				}

			}

		}

		return $qualifiesForSameDayTurnaround;

	}

}
