<?php
App::uses('AppModel', 'Model');
/**
 * ProductDrillingType Model
 *
 * @property Product $Product
 * @property DrillingType $DrillingType
 */
class ProductDrillingType extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'DrillingType' => array(
			'className' => 'DrillingType',
			'foreignKey' => 'drilling_type_id',
			'dependent' => false
		)
	);

}