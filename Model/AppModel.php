<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	var $recursive = -1;

	var $actsAs = array(
		'Containable',
		'Configurable'
	);

	var $sunday = 0;
	var $monday = 1;
	var $tuesday = 2;
	var $wednesday = 3;
	var $thursday = 4;
	var $friday = 5;
	var $saturday = 6;

	public function validationNoEmptyString($check, $fieldName) {

		return $check[$fieldName] !== '';

	}

	public function emptyCache() {

		Cache::clear(false, 'pace');
		Cache::clear(false, 'configuration');
		Cache::clear(false, 'pace_codes');
		Cache::clear(false, 'quote_defaults');
		Cache::clear(false, 'quote_parameters');

	}

	public function getIpAddress() {

		$ipaddress = '';

		if (getenv('REMOTE_ADDR')) {
			$ipaddress = getenv('REMOTE_ADDR');
		} elseif (getenv('HTTP_CLIENT_IP')) {
			$ipaddress = getenv('HTTP_CLIENT_IP');
		} elseif(getenv('HTTP_X_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		} elseif(getenv('HTTP_X_FORWARDED')) {
			$ipaddress = getenv('HTTP_X_FORWARDED');
		} elseif(getenv('HTTP_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		} elseif(getenv('HTTP_FORWARDED')) {
			$ipaddress = getenv('HTTP_FORWARDED');
		} else {
			$ipaddress = 'UNKNOWN';
		}

		return $ipaddress;

	}

	public function getRelatedModel($modelPath = null) {

		if (!$modelPath) {
			return false;
		}

		// Break the path down into an array
		$modelPath = explode('.', $modelPath);

		// Find the model by starting from here
		$relatedModel = $this;

		// Navigate along the path to get to the right model
		foreach ($modelPath as $modelName) {
			$relatedModel = $relatedModel->{$modelName};
		}

		return $relatedModel;

	}

	public function minimumValue($minimumValue = 0, $passedValue = null) {

		if (is_null($passedValue) || $passedValue < $minimumValue) {
			return false;
		}

		return true;

	}

/**
 * Validation method to check the combined content of multiple fields is unique
 *
 * @param mixed $data Array - the contents of the row being saved
 * @param string $fields Array - the field names that form the unique index
 * @return boolean True on success
 */
	public function checkUnique($data = array(), $fields = array()) {

		// check if the param contains multiple columns or a single one
		if (!is_array($fields)) {
			$fields = array($fields);
		}

		// go trough all columns and get their values from the parameters
		foreach($fields as $key) {
			$unique[$key] = $this->data[$this->name][$key];
		}

		// primary key value must be different from the posted value
		if (isset($this->data[$this->name][$this->primaryKey])) {
			$unique[$this->primaryKey] = "<>" . $this->data[$this->name][$this->primaryKey];
		}

		// use the model's isUnique function to check the unique rule
		return $this->isUnique($unique, false);

	}

	public function quoteOptions($tableName = null, $refresh = false, $options = array()) {

		$cacheKey = 'quote_parameters';

		if ($refresh === true) {
			Cache::delete($tableName, $cacheKey);
		}

		$model = $this;

		return Cache::remember(
			$tableName,
			function() use ($model, $options) {

				// Has the model got a quoteOptions variable?
				if ($model->quoteOptions) {

					// Yes, so just return it
					return $model->quoteOptions;

				}

				// Use the model's quote field options if present, else the standard ones

				$displayField = !empty($model->displayField)
					? $model->displayField
					: 'name'
				;

				$fields = $model->quoteOptionFields
					? $model->quoteOptionFields
					: array(
						'id',
						$displayField
					)
				;

				// Loop through the fields and set the fields for the criteria
				foreach ($fields as $field) {
					$criteria['fields'][] = $this->alias . '.' . $field;
				}

				// By default we want only active rows
				$criteria['conditions'][$this->alias . '.is_active'] = true;

				// Are there additional conditions?
				if (isset($options['conditions'])) {

					// Yes, so merge them
					$criteria['conditions'] = array_merge(
						$criteria['conditions'],
						$options['conditions']
					);

				}

				// Now do a simple list find
				return $model->find(
					'list',
					$criteria
				);
			},
			$cacheKey
		);

	}

	public function changeActiveStatus($id = null, $activeStatus = null) {

		if (!$id || !$activeStatus) {
			return;
		}

		// Move to this record
		$this->id = $id;

		// Does it exist?
		if ($this->exists()) {
			// Yes, so set the is_active flag to false
			$this->set('is_active', $activeStatus);
			//...and save it
			$this->save();
		}

	}

	/**
	 * This function strips the extension from a filename. It assumes the extension is .*
	 * @param  string $fileName = null The name of the file
	 * @return string The filename with the extension
	 */
	public function stripExtension($fileName = null) {

		if (!$fileName) {
			return $fileName;
		}

		// convert it into an array by splitting at '.'
		$fileNameParts = explode('.', $fileName);

		if (count($fileNameParts) > 1) {
			// There are more than one keys, suggesting there was a full stop in the title
			// Remove the last key, which will hold the extension
			unset($fileNameParts[(count($fileNameParts) - 1)]);
		}

		// then implode the array into a string
		return implode($fileNameParts);

	}

	public function niceFormattedDate($date) {

		App::uses('CakeTime', 'Utility');

		if (CakeTime::isToday($date)) {
			return 'Today';
		} elseif (CakeTime::isTomorrow($date)) {
			return 'Tomorrow';
		} else {
			return CakeTime::format($date, '%A %e %B');
		}

	}

	public function sqlize($value, $type) {
		$db = $this->getDataSource();
		return $db->value($value, $type);
	}

	public function folderExists($folderName = null, $createIfMissing = true, $mode = false) {

		if (!$folderName) {
			return [
				'success' => false,
				'code' => 'no-folder-name',
				'from' => 'AppModel.folderExists'
			];
		}

		App::uses('Folder', 'Utility');

		$folder = new Folder($folderName);

		$folderExists = $folder->path;

		if ($folderExists) {
			return [
				'success' => true
			];
		}

		if (!$createIfMissing) {
			return [
				'success' => false,
				'code' => 'folder-does-not-exist',
				'from' => 'AppModel.folderExists'
			];
		}

		$folderExists = $folder->create($folderName, $mode);

		if (!$folderExists) {

			return [
				'success' => false,
				'code' => 'folder-create-failed',
				'from' => 'AppModel.folderExists'
			];

		}

		return ['success' => true];

	}

	public function getTimestamp($options = []) {

		$defaults = [
			'format' => false,
			'formatString' => 'Y-m-d H:i:s',
			'dateTime' => 'NOW'
		];

		$options = array_merge(
			$defaults,
			$options
		);

		extract($options);

		$time = new DateTime($dateTime);

		if (!$format) {
			return $time;
		}

		return $time->format($formatString);

	}

}
