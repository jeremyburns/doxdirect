<?php

App::uses('AppModel', 'Model');

/**
 * BindingSideType Model
 *
 * @property Order $Order
 */
class BindingSideType extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';
	public $actsAs = ['Defaultable'];

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = [
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'binding_side_type_id',
			'dependent' => false
		],
		'ProductBindingSideType' => [
			'className' => 'ProductBindingSideType',
			'foreignKey' => 'binding_side_type_id'
		]
	];

}
