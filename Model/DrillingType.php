<?php
App::uses('AppModel', 'Model');
/**
 * DrillingType Model
 *
 * @property OrderItem $OrderItem
 */
class DrillingType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Order field
 *
 * @var string
 */
	public $order = 'holes';

	public $actsAs = ['Defaultable'];

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'Quote' => [
			'className' => 'Quote',
			'foreignKey' => 'drilling_type_id',
			'dependent' => false,
			'counterCache' => 'count_quote'
		],
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'drilling_type_id',
			'dependent' => false,
			'counterCache' => 'count_order'
		],
		'ProductDrillingType' => [
			'className' => 'ProductDrillingType',
			'foreignKey' => 'drilling_type_id',
		]
	];

	public function convertToPaceCode($holes = 0, $pages = 1, $sides = 1) {

		// Is there anything to punch?
		if ($holes === 0 || $pages === 0 || $sides === 0) {
			// No, so return null
			return null;
		}

		// How many leaves are there?
		$leaves = ceil($pages / $sides);

		// Now get the first punching type that matches
		$drillingType = $this->find(
			'first',
			[
				'conditions' => [
					'DrillingType.holes' => $holes,
					'DrillingType.leaves <=' => $leaves
				],
				'order' => [
					'DrillingType.leaves' => 'desc'
				],
				'fields' => [
					'DrillingType.id'
				]
			]
		);

		// Nothing? Then return null
		if (!$drillingType) {
			return null;
		}

		// Return the id
		return $drillingType['DrillingType']['id'];

	}

}
