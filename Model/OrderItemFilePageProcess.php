<?php
App::uses('AppModel', 'Model');
/**
 * OrderItemFilePageProcess Model
 *
 */
class OrderItemFilePageProcess extends AppModel {


	public $order = 'sequence';

/**
 * Display field
 *
 * @var string
 */

/**
 * hasMany associations
 *
 * @var array
 */
	public $belongsTo = array(
		'OrderItemFilePage' => array(
			'className' => 'ProcesOrderItemFilePage',
			'foreignKey' => 'order_item_file_page_id',
			'dependent' => false
		),
		'Process' => array(
			'className' => 'Process',
			'foreignKey' => 'process_id',
			'dependent' => false
		)
	);

	public function record($orderItemFilePageId = null, $processCode = null, $config = null) {

		if (!$processCode || !$orderItemFilePageId) {

			return array(
				'success' => false,
				'code' => 'missing-parameter'
			);

		}

		$processId = $this->Process->getIdByCode($processCode);

		if (!$processId) {

			return array(
				'success' => false,
				'code' => 'invalid-process-code'
			);

		}

		$data = array(
			'process_id' => $processId,
			'order_item_file_page_id' => $orderItemFilePageId
		);

		if ($config) {
			$data['config'] = json_encode($config);
		}

		$this->set($data);

		if ($this->save()) {

			return array('success' => true);

		}

		return array(
			'success' => false,
			'code' => 'save-failed'
		);

	}

	public function recordMany($orderItemFilePageIds = [], $processCode = null, $config = null) {

		if (!$processCode || !$orderItemFilePageIds) {

			return array(
				'success' => false,
				'code' => 'missing-parameter'
			);

		}

		foreach ($orderItemFilePageIds as $orderItemFilePageId) {

			$this->record(
				$orderItemFilePageId,
				$processCode,
				$config
			);
		}

		return array('success' => true);

	}

}
