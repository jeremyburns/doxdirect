<?php
App::uses('AppModel', 'Model');
/**
 * ProductBindingEdgeType Model
 *
 * @property Product $Product
 * @property BindingEdgeType $BindingEdgeType
 */
class ProductBindingEdgeType extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'BindingEdgeType' => array(
			'className' => 'BindingEdgeType',
			'foreignKey' => 'binding_edge_type_id',
			'dependent' => false
		)
	);

}