<?php
App::uses('AppModel', 'Model');
/**
 * PaperSize Model
 *
 * @property OrderItem $OrderItem
 */
class PaperSize extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $order = array('width' => 'desc');
	public $actsAs = array('Defaultable');

	private $pointsToMMFactor = 0.352777778;

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'OrderItem' => array(
			'className' => 'OrderItem',
			'foreignKey' => 'file_paper_size_id'
		),
		'OrderItemFilePage' => array(
			'className' => 'OrderItemFilePage',
			'foreignKey' => 'paper_size_id'
		),
		'Media' => array(
			'className' => 'Media',
			'foreignKey' => 'paper_size_id'
		)
	);

	public function getSizeFromPage($page = array()) {

		if (!$page) {
			return array();
		}

		$sizeKey = '@mediabox';
		$hasTrimMarks = false;

		if (!empty($page['@cropbox'])) {

			$sizeKey = '@cropbox';

			// Return the width and height of the size box
			$cropboxDimensions = $this->getDimensions($page, '@cropbox');

			$cropboxArea = $cropboxDimensions['area'];

			$width = $cropboxDimensions['width'];
			$height = $cropboxDimensions['height'];

			if (!empty($page['@trimbox'])) {

				// There's also a trimbox

				$trimboxDimensions = $this->getDimensions($page, '@trimbox');
				$trimboxArea = $trimboxDimensions['area'];

				// A simple way to see which is larger - compare the areas
				if ($cropboxArea > $trimboxArea) {
					// The cropbox is larger than the trimbox so we assume there are trim marks
					$hasTrimMarks = true;
				}

			}

		} elseif (!empty($page[$sizeKey])) { // will be @mediabox

			$mediaboxDimensions = $this->getDimensions($page, $sizeKey); // will be @mediabox
			$width = $mediaboxDimensions['width'];
			$height = $mediaboxDimensions['height'];

		} else {

			return [];

		}

		$paperSize = $this->matchBySize($width, $height);

		$result = array_merge(
			[
				'width' => $width,
				'height' => $height,
				'size_box' => $sizeKey,
				'has_trim_marks' => $hasTrimMarks
			],
			$paperSize
		);

		return $result;

	}

	private function getDimensions($page = [], $sizeKey = null) {

		if (!$page || !$sizeKey || !isset($page[$sizeKey])) {
			return [
				'width' => 0,
				'height' => 0,
				'area' => 0
			];
		}

		$sizeBox = explode('/', $page[$sizeKey]);

		$coordinates = [
			'tl_x' => $sizeBox[0],
			'tl_y' => $sizeBox[1],
			'br_x' => $sizeBox[2],
			'br_y' => $sizeBox[3]
		];

		$width = $coordinates['br_x'] - $coordinates['tl_x'];
		$height = $coordinates['br_y'] - $coordinates['tl_y'];

		return [
			'width' => $width,
			'height' => $height,
			'area' => $width * $height
		];

	}

	public function convertPointsToMm($points = null) {

		if (!$points) {
			return 0;
		}

		return round($points * $this->pointsToMMFactor, 0);

	}

/**
* The match function takes a width and a height in mm or points and tries to match it to a defined paper size.
*/
	public function matchBySize($width = null, $height = null, $measurement = 'points') {

		$width = round($width);
		$height = round($height);

		if ($measurement == 'mm') {

			$widthMm = $width;
			$heightMm = $height;


		} else {

			$widthMm = $this->convertPointsToMm($width);
			$heightMm = $this->convertPointsToMm($height);

		}

		// Do we have both dimensions?
		if (!$width || !$height) {

			// Return an error
			return array(
				'success' => false,
				'code' => 'missing-parameter',
				'paper_size_match' => 'unmatched',
				'paper_size_id' => 'NS',
				'orientation' => 'u',
				'width' => $widthMm,
				'height' => $heightMm
			);

		}

		// Is this a landscape page?

		$orientation = $width > $height
			? 'l'
			: 'p'
		;

		// The paper sizes are stored in portrait mode, so the function first switches the provided dimensions into portrait.
		if ($orientation == 'l') {

			// Supplied dimensions are in landscape mode, so switch them
			$searchHeight = $width;
			$searchWidth = $height;

		} else {

			// Supplied dimensions are in portrait mode, so don't switch them
			$searchHeight = $height;
			$searchWidth = $width;

		}

		// Are we matching by mms or points?
		if ($measurement == 'points') {

			// Make sure we match on the points fields
			$widthFieldName = 'width_points';
			$heightFieldName = 'height_points';

		} else {

			// Match by the mms fields
			$widthFieldName = 'width';
			$heightFieldName = 'height';

		}

		// First try and find a direct match
		$paperSize = $this->find(
			'first',
			array(
				'fields' => array(
					'PaperSize.id',
					'PaperSize.width',
					'PaperSize.height'
				),
				'conditions' => array(
					'PaperSize.' . $widthFieldName => $searchWidth,
					'PaperSize.' . $heightFieldName => $searchHeight,
					'PaperSize.is_active' => true
				)
			)
		);

		if ($paperSize) {

			// We have a direct match, so return it
			// We return the id and the supplied dimensions in MM, which will mirror the supplied orientation

			$result = array(
				'success' => true,
				'paper_size_match' => 'exact',
				'paper_size_id' => $paperSize['PaperSize']['id'],
				'orientation' => $orientation,
				'width' => $widthMm,
				'height' => $heightMm
			);

			return $result;

		}

		// If no direct match is found, find the first one that is larger than both dimensions
		$paperSize = $this->find(
			'first',
			array(
				'fields' => array(
					'PaperSize.id',
					'PaperSize.width',
					'PaperSize.height'
				),
				'conditions' => array(
					'PaperSize.' . $widthFieldName . ' >= ' => $searchWidth,
					'PaperSize.' . $heightFieldName . ' >=' => $searchHeight,
					'PaperSize.is_active' => true
				),
				'order' => array(
					'PaperSize.' . $widthFieldName
				)
			)
		);

		if ($paperSize) {

			// We have one, so return
			// but mark it as 'upscaled' which means the paper is larger than the image

			$result = array(
				'success' => true,
				'paper_size_match' => 'upscaled',
				'paper_size_id' => $paperSize['PaperSize']['id'],
				'orientation' => $orientation,
				'width' => $widthMm,
				'height' => $heightMm
			);

			return $result;

		}

		// If we are here we can't find a match, so return non standard
		return array(
			'success' => false,
			'code' => 'non-standard',
			'paper_size_' => 'unmatched',
			'paper_size_id' => 'NS',
			'orientation' => $orientation,
			'width' => $widthMm,
			'height' => $heightMm
		);

	}

/**
 * nextSmaller function.
 * This function returns the id of the paper size immediately smaller than the passed paper size
 *
 * @access public
 * @param mixed $paperSizeId
 * @return void
 */
	public function nextSmaller($paperSizeId = null) {

		if (!$paperSizeId) {
			return false;
		}

		$width = $this->field(
			'PaperSize.width',
			array(
				'PaperSize.id' => $paperSizeId
			)
		);

		if (!$width) {
			return false;
		}

		$smallerPaperSizeId = $this->field(
			'PaperSize.id',
			array(
				'PaperSize.width <' => $width,
				'PaperSize.id <>' => 'NS'
			),
			'PaperSize.width desc'
		);

		if (!$smallerPaperSizeId) {
			return false;
		} else {
			return $smallerPaperSizeId;
		}

	}

/**
 * nextLarger function.
 * This function returns the id of the paper size immediately larger than the passed paper size
 *
 * @access public
 * @param mixed $paperSizeId
 * @return void
 */
	public function nextLarger($paperSizeId = null) {

		if (!$paperSizeId) {
			return false;
		}

		$width = $this->field(
			'PaperSize.width',
			array(
				'PaperSize.id' => $paperSizeId
			)
		);

		if (!$width) {
			return false;
		}

		$largerPaperSizeId = $this->field(
			'PaperSize.id',
			array(
				'PaperSize.width >' => $width,
				'PaperSize.id <>' => 'NS'
			),
			'PaperSize.width asc'
		);

		if (!$largerPaperSizeId) {
			return false;
		} else {
			return $largerPaperSizeId;
		}

	}

/**
	* This function returns the relative rank of the size of this paper (where 6 is big and 1 is small)
*/

	public function pageSizeRank($paperSizeId = null, $refresh = false) {

		if (!$paperSizeId) {
			return null;
		}

		$cacheKey = 'configuration.paper_size_ranks';

		if ($refresh === true) {

			Cache::delete($paperSizeId, $cacheKey);

		}

		$model = $this;

		return Cache::remember(
			$paperSizeId,
			function() use ($model, $paperSizeId) {

				// Return the value
				return $model->field(
					'size_rank',
					array(
						'id' => $paperSizeId
					)
				);

			},
			$cacheKey
		);

	}

	public function paperSizeScale($pagePaperSizeId = null, $orderItemPaperSizeId = null) {

		$pageSizeRank = $this->pageSizeRank($pagePaperSizeId);

		$orderItemPaperSizeRank = $this->pageSizeRank($orderItemPaperSizeId);

		return ($orderItemPaperSizeRank - $pageSizeRank) * 100;

	}


}