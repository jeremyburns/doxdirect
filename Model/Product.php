<?php
App::uses('AppModel', 'Model');
/**
 * Product Model
 *
 */
class Product extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = [
		'Defaultable',
		'Pace'
	];

	public $order = ['Product.binding_name', 'Product.paper_size_id'];

	public $quoteOptionFields = [
		'id',
		'name',
		'binding_name'
	];

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = [
		'BindingType' => [
			'className' => 'BindingType',
			'foreignKey' => 'binding_type_id',
			'dependent' => false
		],
		'PaperSize' => [
			'className' => 'PaperSize',
			'foreignKey' => 'paper_size_id',
			'dependent' => false
		]
	];

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductClick' => [
			'className' => 'ProductClick',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductMedia' => [
			'className' => 'ProductMedia',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductDrillingType' => [
			'className' => 'ProductDrillingType',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductSide' => [
			'className' => 'ProductSide',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductCoverType' => [
			'className' => 'ProductCoverType',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductBinding' => [
			'className' => 'ProductBinding',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductBindingEdgeType' => [
			'className' => 'ProductBindingEdgeType',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductBindingSideType' => [
			'className' => 'ProductBindingSideType',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductTabType' => [
			'className' => 'ProductTabType',
			'foreignKey' => 'product_id',
			'dependent' => false
		],
		'ProductCoverTypeInnerFront' => [
			'className' => 'ProductCoverType',
			'foreignKey' => 'product_id',
			'conditions' => [
				'ProductCoverTypeInnerFront.inner_outer' => 'i',
				'ProductCoverTypeInnerFront.front_back' => 'f'
			],
			'dependent' => false
		],
		'ProductCoverTypeInnerBack' => [
			'className' => 'ProductCoverType',
			'foreignKey' => 'product_id',
			'conditions' => [
				'ProductCoverTypeInnerBack.inner_outer' => 'i',
				'ProductCoverTypeInnerBack.front_back' => 'b'
			],
			'dependent' => false
		],
		'ProductCoverTypeOuterFront' => [
			'className' => 'ProductCoverType',
			'foreignKey' => 'product_id',
			'conditions' => [
				'ProductCoverTypeOuterFront.inner_outer' => 'o',
				'ProductCoverTypeOuterFront.front_back' => 'f'
			],
			'dependent' => false
		],
		'ProductCoverTypeOuterBack' => [
			'className' => 'ProductCoverType',
			'foreignKey' => 'product_id',
			'conditions' => [
				'ProductCoverTypeOuterBack.inner_outer' => 'o',
				'ProductCoverTypeOuterBack.front_back' => 'b'
			],
			'dependent' => false
		],
		'ProductCoverTypeWrapAround' => [
			'className' => 'ProductCoverType',
			'foreignKey' => 'product_id',
			'conditions' => [
				'ProductCoverTypeWrapAround.inner_outer' => 'w',
				'ProductCoverTypeWrapAround.front_back' => 'w'
			],
			'dependent' => false
		]
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = [
		'name' => [
			'required' => [
				'rule' => 'notBlank',
				'message' => 'You must specify a name for this product.',
				'allowEmpty' => false,
				'last' => true
			],
			'noEmptyString' => [
				'rule' => ['validationNoEmptyString', 'name'],
				'message' => 'No empty string',
				'last' => true
			],
			'maxLength' => [
				'rule' => ['maxLength', 50],
				'message' => 'Product name cannot be longer than 50 characters.',
				'last' => true
			],
			'minLength' => [
				'rule' => ['minLength', 1],
				'message' => 'Product name must have at least 1 character.',
				'last' => true
			],
			'unique' => [
				'rule' => 'isUnique',
				'message' => 'This product name already exists.'
			]
		]
	];

	public function getProductCatalogueFile($refresh = true) {

		if ($refresh === true) {
			Cache::delete('product_catalogue', 'pace');
		}

		$model = $this;

		return Cache::remember(
			'product_catalogue',
			function() use ($model) {

				$productCatalogue = $model->getProductCatalogue();

				return $productCatalogue['response'];

			},
			'pace'
		);

	}

	public function refreshProductCatalogueFromPace($refresh = true) {

		if ($refresh) {
			$this->emptyCache();
		}

		$dataSource = $this->getDataSource();

		$dataSource->begin();

		// This gets the product catalogue from Pace (or the cached version if refresh == false)
		$productCatalogue = $this->getProductCatalogueFile($refresh);

		// product map categories define how bindings and products go together
		$this->productMapCategories = $this->getProductMapCategories();

		// 'all items'  are the various end tables, such as medi, clicks and so on
		$allItems = $productCatalogue['all_items'];

		// Tis function syncs the pace 'all items' with the Dox tables
		$result = $this->updateAllItemsFromPace($allItems);

		if (!$result['success']) {
			// Rollback and return
			$dataSource->rollback();
			return $result;
		}

		// This function takes the bindings, makes products out of them and relates them to 'all items'.
		$result = $this->updateBindingsFromPace($productCatalogue);

		if (!$result['success']) {
			// Rollback and return
			$dataSource->rollback();
			return $result;
		}

		// A small function to keep the media type column up to date
		$this->ProductMedia->Media->updateTypes();

		// If we are here, nothing has failed, so commit and return success
		$dataSource->commit();

		return [
			'success' => true,
			'message' => [
				'heading' => 'Pace product catalogue refresh complete',
				'message' => 'The product catalogue has been refreshed from Pace'
			]
		];

	}

/**
 * The updateBindingsFromPace function loops through the all_items key of the PACE product catalogue. It calls either updateFromPace or addFromPace for each key
 */
	public function updateBindingsFromPace($productCatalogue = []) {

		if (!$productCatalogue) {
			return [
				'success' => false,
				'code' => 'no-product-catalogue',
				'message' => [
					'heading' => 'Product catalogue missing',
					'message' => 'No product catalogue sent to update bindings'
				]
			];
		}

		// Start of by making all products inactive (they get to set to active as they are synced)
		$this->updateAll(
			['is_active' => false]
		);

//		debug($doxProducts);
//
//		die(debug($paceProducts));

		// Loop through the main keys of the productCatalogue - these are Dox binding types
		foreach ($productCatalogue as $paceBindingKey => $paceBinding) {

			// We don't want the all_items or execution_time keys
			if (is_numeric($paceBindingKey)) {

				$result = $this->syncPaceBindings($paceBinding);

				if (!$result['success']) {
					return $result;
				}

				$result = $this->syncPaceForBindingToProducts($paceBinding);

				if (!$result['success']) {
					return $result;
				}

			}

		}

		return ['success' => true];

	}

	public function syncPaceBindings($paceBinding = []) {

		// Maps dox fields to Pac fields
		// 'dox_field' => 'pace_field'
		$fieldMap = [
			'id' => 'id',
			'name' => 'description',
			'is_active' => 'active',
			'pages_min' => 'U_minPages',
			'pages_max' => 'U_maxPages',
			'thickness_min' => 'U_minThickness',
			'thickness_max' => 'U_maxThickness',
			'format_width_min' => 'U_minFormatWidth',
			'format_width_max' => 'U_maxFormatWidth',
			'format_height_min' => 'U_minFormatHeight',
			'format_height_max' => 'U_maxFormatHeight',
			'page_divisor' => 'U_pageDivisor'
		];

		// Can we find this binding type?
		$doxBindingType = $this->BindingType->findById($paceBinding['id']);

		if (!$doxBindingType) {

			// The binding type does not exist in the Dox database so create it

			// Loop through the field map to create the array
			foreach ($fieldMap as $doxFieldName => $paceFieldName) {
				$data['BindingType'][$doxFieldName] = $paceBinding[$paceFieldName];
			}

			$this->BindingType->create();
			$this->BindingType->save($data);

		} else {

			// Set the update flag so we know we have to save it
			$update = false;

			// Get a data array ready to update
			$data['BindingType']['id'] = $paceBinding['id'];

			// Check each field for a mismatch
			foreach ($fieldMap as $doxFieldName => $paceFieldName) {

				// The field values don't match
				if ($doxBindingType['BindingType'][$doxFieldName] !== $paceBinding[$paceFieldName]) {

					// Set the update flag so we know we have to save it
					$update = true;

					// Build the array to update
					$data['BindingType'][$doxFieldName] = $paceBinding[$paceFieldName];
				}

			}

			// Do we need to do an update?
			if ($update) {

				// Save the data
				if (!$this->BindingType->save($data)) {
					// Unhandled error just now...

					return [
						'success' => false,
						'code' => 'binding-type-save-failed',
						'message' => [
							'heading' => 'Binding type save failed',
							'message' => 'Binding type save failed'
						],
						'data' => $data,
						'validationErrors' => $this->BindingType->validationErrors
					];

				}

			}

		}

		return ['success' => true];

	}

	// How are options mapped to products?
	public function getProductMapCategories() {

		// Load up the ProductMapCategory model
		App::import('model', 'ProductMapCategory');

		// Start a new instance
		$productMapCategory = new ProductMapCategory();

		// Return the value
		return $productMapCategory->getProductMapCategories();

	}

	// Maps Dox fields to Pace fields
	public function fieldMap() {

		// Load up the ProductMapCategory model
		App::import('model', 'ProductMapCategory');

		// Start a new instance
		$productMapCategory = new ProductMapCategory();

		// Return the value
		return $productMapCategory->fieldMap();

	}

/**
* Takes a binding and maps it to Dox products
*/
	public function syncPaceForBindingToProducts($paceBinding = []) {

		foreach ($paceBinding['productTypes'] as $paceProduct) {

			// Create some variables
			$productId = $paceProduct['id'];

			$productName = $paceProduct['productName'] . ' ' . $paceBinding['description'];

			// Try and find the Dox product
			$doxProduct = $this->findById($productId);

			// Does it already exist?
			if (!$doxProduct) {

				// Add the product
				$data['Product'] = [
					'id' => $productId,
					'name' => $productName,
					'paper_size_id' => $paceProduct['productName'],
					'binding_type_id' => $paceBinding['id'],
					'binding_name' => $paceBinding['description'],
					'is_active' => true
				];

				// Create and save it
				$this->create();
				$this->set($data);
				$result = $this->save();

			} else {

				// Create an array of data to update
				// We at least need to make it active
				$data['Product'] = ['is_active' => true];

				// Does anything need to be updated?
				if (
					$doxProduct['Product']['name'] !== $productName
					|| $doxProduct['Product']['paper_size_id'] !== $paceProduct['productName']
					|| $doxProduct['Product']['binding_type_id'] !== $paceBinding['id']
					|| $doxProduct['Product']['binding_name'] !== $paceBinding['description']
				) {

					// Create an array of data to update
					$data['Product'] = array_merge(
						$data['Product'],
						[
							'name' => $productName,
							'binding_name' => $paceBinding['description']
						]
					);

				}

				// Update it
				$this->id = $productId;
				$this->set($data);
				$this->save();

			}

			foreach ($paceProduct['productItems']['categories'] as $categoryId => $category) {

				if (!isset($this->productMapCategories[$categoryId])) {

					return [
						'success' => false,
						'code' => 'new-category',
						'message' => [
							'heading' => 'A new category has been added',
							'message' => 'This must be incorporated into the data model and code before it can be imported.'
						],
						'category' => $category
					];

				}

				// This is the category as stored in the Dox db
				$doxCategory = $this->productMapCategories[$categoryId];

				// Don't do this if we have to ignore the category
				if (!empty($doxCategory['ignore'])) {

					// This makes it go to the next record in the foreach loop
					continue;

				}

				// Get the joining model and field name
				$doxJoinModelName = $doxCategory['join_model'];
				$doxJoinFieldName = $doxCategory['join_field'];

				// Get the path to the Dox model to update
				$joinModel = $this->getRelatedModel($doxJoinModelName);

				// We need to build conditions to retrieve the current rows
				$conditions = [
					$doxJoinModelName . '.product_id' => $productId
				];

				// Amend the conditions to include extra fields if defined
				if (isset($doxCategory['extra_fields'])) {

					foreach ($doxCategory['extra_fields'] as $extraFieldName => $extraFieldValue) {
						$conditions[$doxJoinModelName . '.' . $extraFieldName] = $extraFieldValue;
					}

				}

				// Now get the current rows
				$currentJoinRecords = $joinModel->find(
					'list',
					[
						'conditions' => $conditions,
						'fields' => [
							$doxJoinModelName . '.id',
							$doxJoinModelName . '.id'
						]
					]
				);

				// Loop through the items in the Pace category
				foreach ($category['items'] as $item) {

					// The $conditions used to pull back all rows is useful here, so reuse it
					$joinConditions = $conditions;

					// We need to make the conditions more specific to restirct it to this quoteItemType
					$joinConditions[$doxJoinModelName . '.' . $doxJoinFieldName] = $item['quoteItemType'];

					//Now get this specific row
					$joinRecord = $joinModel->find(
						'first',
						['conditions' => $joinConditions]
					);

					// Did we find this row?
					if ($joinRecord) {

						// Yes, so remove it from the $currentJoinRecords as we don't need to do anything else with it
						$joinModelId = $joinRecord[$doxJoinModelName]['id'];
						unset($currentJoinRecords[$joinModelId]);

					} else {

						// We didin't, so create it
						$data[$doxJoinModelName] = [
							'product_id' => $productId,
							$doxJoinFieldName => $item['quoteItemType']
						];

						if (isset($doxCategory['extra_fields'])) {
							$data[$doxJoinModelName] = array_merge(
								$data[$doxJoinModelName],
								$doxCategory['extra_fields']
							);
						}

						$joinModel->create();
						$result = $joinModel->save($data);

						if (!$result) {

							return [
								'success' => false,
								'code' => 'join-model-save-failed',
								'message' => [
									'heading' => 'Sync Pace bindings to products failed',
									'message' => 'Sync Pace bindings to products failed'
								],
								'from' => 'syncPaceForBindingToProducts',
								'data' => $data
							];

						}

						$joinModelId = $joinModel->id;

					}

					// $joinModelId holds the id for this record, so go off and set any limits for it
					if (method_exists($joinModel, 'setLimits')) {
						$joinModel->setLimits($joinModelId);
					}

				}

				// Any records left in $currentJoinRecords are redundant, so they can go
				if ($currentJoinRecords) {

					$joinModel->deleteAll(
						[$doxJoinModelName . '.id' => $currentJoinRecords]
					);

				}

			}

		}

		return ['success' => true];

	}

/**
 * The updateAllItemsFromPace function loops through the all_items key of the PACE product catalogue. It calls either updateFromPace or addFromPace for each key
 */
	public function updateAllItemsFromPace($allItems = []) {

		if (!$allItems) {
			return ['success' => false];
		}

		// Loop through the items
		foreach ($allItems as $productItemKey => $productItems) {

			if (!isset($this->productMapCategories[$productItemKey])) {

				return [
					'success' => false,
					'code' => 'missing-category',
					'message' => [
						'heading' => 'Pace has added a new category which is not in the Dox database.',
						'message' => 'Missing category ' . $productItemKey
					]
				];

			}

			// Find the category as defined in this model
			$category = $this->productMapCategories[$productItemKey];

			// Get the path to the Dox model to update
			$model = $this->getRelatedModel($category['model_path']);

			$this->removeMissingPaceRecords(
				$productItems['items'],
				$model,
				$category['join_model'],
				$category['join_field']
			);

			// Loop through the PACE records
			foreach ($productItems['items'] as $productItem) {

				// Try and find the matching Dox record
				$currentRecord = $model->findById($productItem['id']);

				if (!$currentRecord) {

					$this->addFromPace($model, $category, $productItem);

				} else {

					$this->updateFromPace($currentRecord, $model, $category, $productItem);

				}

			}

		}

		return ['success' => true];

	}

/*
* The removeMissingPaceRecords function marks records that are in a Dox table but not in the Pace catalogue as inactive, and removes any joing rows (e.g. ProductTab)
*/

	private function removeMissingPaceRecords($productItems, $model, $doxJoinModelName, $doxJoinFieldName) {

		// Check for Dox rows that are missing from the PACE catalogue

		$paceIds = array_flip(Hash::extract($productItems, '{n}.id'));

		$modelName = $model->alias;

		$doxIds = $model->find(
			'list',
			[
				'fields' => [
					$modelName . '.id',
					$modelName . '.id'
				]
			]
		);

		// Loop through the Dox ids
		foreach ($doxIds as $doxId) {

			// Is it in the Pace ids?
			if (!isset($paceIds[$doxId])) {

				// No, so mark the record as inactive in Dox
				$model->changeActiveStatus($doxId, false);

				// Now remove all joing rows
				// Get the path to the Dox model to update
				$joinModel = $this->getRelatedModel($doxJoinModelName);

				$currentJoinRecords = $joinModel->find(
					'all',
					[
						'conditions' => [
							// $doxJoinModelName . '.product_id' => $doxId,
							$doxJoinModelName . '.' . $doxJoinFieldName => $doxId
						]
					]
				);

				if ($currentJoinRecords) {

					$joinIds = Hash::extract($currentJoinRecords, '{n}.' . $doxJoinModelName . '.id');

					$joinModel->deleteAll([$doxJoinModelName . '.id' => $joinIds]);

				}

			}
		}

	}

/**
 * The updateFromPace function loops through a PACE product item record and compares it with the matching Dox fields. If a mismatch is found, it updates the Dox record.
 */
	public function updateFromPace($currentRecord, $model, $category, $productItem) {

		// Set a flag to determine if we need to do an update
		$doUpdate = false;

		$modelName = $model->alias;

		// Get to the actual row rather than the model - it's just cleaner code
		$currentRecord = $currentRecord[$modelName];

		// Start an array for updates, if needed
		$data[$modelName] = [
			'id' => $currentRecord['id']
		];

		// We need to map pace fields to Dox fields
		$fieldMap = array_flip($this->fieldMap());

		foreach ($productItem as $paceFieldName => $value) {

			// What is the corresponding Dox fieldname?
			$doxFieldName = isset($fieldMap[$paceFieldName])
				? $fieldMap[$paceFieldName]
				: $paceFieldName
			;

			if (isset($currentRecord[$doxFieldName]) && $currentRecord[$doxFieldName] != $productItem[$paceFieldName]) {

				$doUpdate = true;

				$data[$modelName][$doxFieldName] = $productItem[$paceFieldName];

			}

		}

		if ($doUpdate) {

			$model->set($data);
			$result = $model->save();

		}

	}

/**
 * The addFromPace function loops through a PACE product item record and creates a matching Dox record.
 */
	public function addFromPace($model, $category, $productItem) {

		// We need to map pace fields to Dox fields
		$fieldMap = array_flip($this->fieldMap());

		$modelName = $model->alias;

		foreach ($productItem as $paceFieldName => $value) {

			// What is the corresponding Dox fieldname?
			$doxFieldName = isset($fieldMap[$paceFieldName])
				? $fieldMap[$paceFieldName]
				: $paceFieldName
			;

			// Add it to the data array
			$data[$modelName][$doxFieldName] = $productItem[$paceFieldName];

		}

		// Create a record
		$model->create();

		// Set the data
		$model->set($data);

		// Save it
		$model->save();

	}

/**
* The productMap function produces a
*/
	public function productMap($refresh = false) {

		if ($refresh === true) {
			Cache::delete('product_map', 'pace');
		}

		$model = $this;

		return Cache::remember(
			'product_map',
			function() use ($model) {

				return $model->refreshProductMap();

			},
			'pace'
		);

	}

	public function refreshProductMap() {

		Cache::delete('product_map', 'pace');

		$products = $this->find(
			'all',
			[
				'fields' => [
					'Product.id',
					'Product.name',
					'Product.binding_name'
				],
				'order' => [
					'Product.binding_name',
					'Product.name'
				],
				'contain' => [
					'BindingType' => [
						'fields' => [
							'BindingType.id',
							'BindingType.page_divisor',
							'BindingType.extra_processing_day'
						]
					],
					'ProductBinding' => [
						'fields' => [
							'ProductBinding.product_id',
							'ProductBinding.binding_id'
						],
						'Binding' => [
							'fields' => [
								'Binding.id',
								'Binding.name'
							]
						]
					],
					'ProductBindingEdgeType' => [
						'fields' => [
							'ProductBindingEdgeType.product_id',
							'ProductBindingEdgeType.binding_edge_type_id'
						],
						'BindingEdgeType' => [
							'fields' => [
								'BindingEdgeType.id',
								'BindingEdgeType.name'
							]
						]
					],
					'ProductClick' => [
						'fields' => [
							'ProductClick.product_id',
							'ProductClick.click_id'
						],
						'Click' => [
							'fields' => [
								'Click.id',
								'Click.name'
							]
						]
					],
					'ProductMedia' => [
						'fields' => [
							'ProductMedia.product_id',
							'ProductMedia.media_id',
							'ProductMedia.leaves_min',
							'ProductMedia.leaves_max'
						],
						'Media' => [
							'fields' => [
								'Media.id',
								'Media.name',
								'Media.weight'
							]
						]
					],
					'ProductTabType' => [
						'fields' => [
							'ProductTabType.product_id',
							'ProductTabType.tab_type_id'
						],
						'TabType' => [
							'fields' => [
								'TabType.id',
								'TabType.name'
							]
						]
					],
					'ProductDrillingType' => [
						'fields' => [
							'ProductDrillingType.product_id',
							'ProductDrillingType.drilling_type_id'
						],
						'DrillingType' => [
							'fields' => [
								'DrillingType.id',
								'DrillingType.name'
							]
						]
					],
					'ProductSide' => [
						'fields' => [
							'ProductSide.product_id',
							'ProductSide.side_id'
						],
						'Side' => [
							'fields' => [
								'Side.id',
								'Side.name'
							]
						]
					],
					'ProductCoverTypeInnerFront' => [
						'fields' => [
							'ProductCoverTypeInnerFront.product_id',
							'ProductCoverTypeInnerFront.cover_type_id'
						],
						'CoverType' => [
							'fields' => [
								'CoverType.id',
								'CoverType.name'
							]
						]
					],
					'ProductCoverTypeInnerBack' => [
						'fields' => [
							'ProductCoverTypeInnerBack.product_id',
							'ProductCoverTypeInnerBack.cover_type_id'
						],
						'CoverType' => [
							'fields' => [
								'CoverType.id',
								'CoverType.name'
							]
						]
					],
					'ProductCoverTypeOuterFront' => [
						'fields' => [
							'ProductCoverTypeOuterFront.product_id',
							'ProductCoverTypeOuterFront.cover_type_id'
						],
						'CoverType' => [
							'fields' => [
								'CoverType.id',
								'CoverType.name'
							]
						]
					],
					'ProductCoverTypeOuterBack' => [
						'fields' => [
							'ProductCoverTypeOuterBack.product_id',
							'ProductCoverTypeOuterBack.cover_type_id'
						],
						'CoverType' => [
							'fields' => [
								'CoverType.id',
								'CoverType.name'
							]
						]
					],
					'ProductCoverTypeWrapAround' => [
						'fields' => [
							'ProductCoverTypeWrapAround.product_id',
							'ProductCoverTypeWrapAround.cover_type_id'
						],
						'CoverType' => [
							'fields' => [
								'CoverType.id',
								'CoverType.name'
							]
						]
					]
				]
			]
		);

		if (!$products) {
			return [
				'success' => false,
				'message' => [
					'heading' => 'No products found',
					'message' => 'No products found when refreshing product map'
				]
			];
		}

		// This array gives us some structure as we parse over the products array
		$relatedModels = [
			'ProductClick' => [
				'model_name' => 'Click',
				'unavailable_text' => 'No options available',
				'update_model' => 'click',
				'option_group' => 'click',
				'hide_when_single' => false
			],
			'ProductBinding' => [
				'model_name' => 'Binding',
				'unavailable_text' => 'No options available',
				'update_model' => 'binding',
				'option_group' => 'binding',
				'hide_when_single' => true
			],
			'ProductBindingEdgeType' => [
				'model_name' => 'BindingEdgeType',
				'unavailable_text' => 'No options available',
				'update_model' => 'binding-edge-type',
				'option_group' => 'binding-edge-type',
				'hide_when_single' => true
			],
			'ProductBindingSideType' => [
				'model_name' => 'BindingSideType',
				'unavailable_text' => 'No options available',
				'update_model' => 'binding-side-type',
				'option_group' => 'binding-side-type',
				'hide_when_single' => true
			],
			'ProductMedia' => [
				'model_name' => 'Media',
				'unavailable_text' => 'No options available',
				'update_model' => 'media',
				'option_group' => null,
				'hide_when_single' => false
			],
			'ProductDrillingType' => [
				'model_name' => 'DrillingType',
				'unavailable_text' => 'Hole drilling not available',
				'update_model' => 'drilling-type',
				'option_group' => 'drilling-type',
				'hide_when_single' => false
			],
			'ProductSide' => [
				'model_name' => 'Side',
				'unavailable_text' => 'No options available',
				'update_model' => 'side',
				'option_group' => 'side',
				'hide_when_single' => true
			],
			'ProductCoverTypeInnerFront' => [
				'model_name' => 'CoverType',
				'model_name_alias' => 'CoverTypeInnerFront',
				'unavailable_text' => 'No options available',
				'update_model' => 'cover-type-inner-front',
				'option_group' => 'cover-type-inner-front',
				'hide_when_single' => false,
				'set_default' => true,
				'connect_through_model' => 'OrderItem'
			],
			'ProductCoverTypeInnerBack' => [
				'model_name' => 'CoverType',
				'model_name_alias' => 'CoverTypeInnerBack',
				'unavailable_text' => 'No options available',
				'update_model' => 'cover-type-inner-back',
				'option_group' => 'cover-type-inner-back',
				'hide_when_single' => false,
				'set_default' => true,
				'connect_through_model' => 'OrderItem'
			],
			'ProductCoverTypeOuterFront' => [
				'model_name' => 'CoverType',
				'model_name_alias' => 'CoverTypeOuterFront',
				'unavailable_text' => 'No options available',
				'update_model' => 'cover-type-outer-front',
				'option_group' => 'cover-type-outer-front',
				'hide_when_single' => false,
				'set_default' => true,
				'connect_through_model' => 'OrderItem'
			],
			'ProductCoverTypeOuterBack' => [
				'model_name' => 'CoverType',
				'model_name_alias' => 'CoverTypeOuterBack',
				'unavailable_text' => 'No options available',
				'update_model' => 'cover-type-outer-back',
				'option_group' => 'cover-type-outer-back',
				'hide_when_single' => false,
				'set_default' => true,
				'connect_through_model' => 'OrderItem'
			],
			'ProductCoverTypeWrapAround' => [
				'model_name' => 'CoverType',
				'model_name_alias' => 'CoverTypeWrapAround',
				'unavailable_text' => 'No options available',
				'update_model' => 'cover-type-wrap-around',
				'option_group' => 'cover-type-wrap-around',
				'hide_when_single' => false,
				'set_default' => true,
				'connect_through_model' => 'OrderItem'
			],
			'ProductTabType' => [
				'model_name' => 'TabType',
				'unavailable_text' => 'No options available',
				'update_model' => 'tab-type',
				'option_group' => 'tab-type',
				'hide_when_single' => true
			]
		];

		// Extract the input class from it
		// The input class is used to target selects that have to be updated by js
		foreach ($relatedModels as $relatedModel) {

			// Get the name of the related model
			$modelNameField = isset($relatedModel['model_name_alias'])
				? 'model_name_alias'
				: 'model_name'
			;

			$relatedModelNameField = $relatedModel[$modelNameField];

			$relatedModelName = $relatedModel[$modelNameField];

			$result['settings'][$relatedModelNameField] = [
				'hideWhenSingle' => $relatedModel['hide_when_single'],
				'updateModel' => $relatedModel['update_model']
			];

			if (!empty($relatedModel['option_group'])) {
				$result['settings'][$relatedModelNameField]['optionGroup'] = 'option-group-' . $relatedModel['option_group'];
			}

			if (isset($relatedModel['placeholder_text'])) {
				$result['settings'][$relatedModelNameField]['placeHolderText'] = $relatedModel['placeholder_text'];
			}

			if (!empty($relatedModel['set_default'])) {

				if (isset($relatedModel['connect_through_model'])) {

					$connectingModelName = $relatedModel['connect_through_model'];

					App::uses($connectingModelName, 'Model');
					$this->{$connectingModelName} = ClassRegistry::init($connectingModelName);

					$criteria = $this->{$connectingModelName}->belongsTo[$relatedModelName]['conditions'];

					$default = $this->{$connectingModelName}->{$relatedModelName}->getDefault(['criteria' => $criteria]);
					if ($default !== null) {
						$result['settings'][$relatedModelNameField]['default'] = $default;
					}

				}

			}

		}

		// Get the product/media/sides limit matrix
		$productMediaLimits = $this->ProductMedia->productMediaLimits();

		// Loop through the list of products
		foreach($products as $product) {

			// Get the product id
			// This is used in js to choose the right options when a user selects a product
			$productId = $product['Product']['id'];

			$result['products'][$productId] = $product['Product'];
			$result['products'][$productId]['page_divisor'] = $product['BindingType']['page_divisor'];
			$result['products'][$productId]['extra_processing_day'] = $product['BindingType']['extra_processing_day'];

			// Loop through the relatedModels array
			foreach ($relatedModels as $relatedModel => $model) {

				// Get the actual model name
				$modelName = $model['model_name'];

				// In cases such as covers we need to alias the model name, else all flavours are combined into a single key
				$modelNameAlias = isset($model['model_name_alias'])
					? $model['model_name_alias']
					: $modelName
				;

				// If there is no data for this key it indicates these options are not applicable for this product
				if (!empty($product[$relatedModel])) {

					$selectOptions[$productId]['name'] = $product['Product']['name'];

					// Loop through each data row - these are the select options
					foreach ($product[$relatedModel] as $productKey) {

						// Get the id and value
						$options = array_values($productKey[$modelName]);
						$id = $options[0];
						$value = $options[1];

						// Add them to the $selectOptions array
						$selectOptions[$productId]['options'][$modelNameAlias][$id] = $value;

					}

					if (!empty($selectOptions[$productId]['options'][$modelNameAlias])) {
						natsort($selectOptions[$productId]['options'][$modelNameAlias]);
					}

					$selectOptions[$productId]['hideWhenSingle'][$modelNameAlias] = (count($product[$relatedModel]) === 1 && $model['hide_when_single'] == true);

				}

				// If we have select options, sort them
				if (isset($selectOptions[$productId][$modelNameAlias])) {
					asort($selectOptions[$productId][$modelNameAlias]);
				}

				if (isset($productMediaLimits[$productId])) {
					$selectOptions[$productId]['pageLimits'] = $productMediaLimits[$productId];
				} else {
					$selectOptions[$productId]['pageLimits'] = [];
				}

			}

		}

		// Add the options to the result
		$result['selectOptions'] = $selectOptions;

		$result['sides'] = $this->ProductSide->Side->find(
			'list',
			[
				'fields' => [
					'Side.id',
					'Side.sides'
				]
			]
		);

		$result['success'] = true;

		return $result;

	}

	public function pageCountValidation($orderItem = []) {

		$productMap = $this->productMap();

		$productId = $orderItem['product_id'];

		$product = $productMap['selectOptions'][$productId];

		$pageDivisor = $productMap['products'][$productId]['page_divisor'];

		$mediaId = $orderItem['media_id'];

		$sideId = $orderItem['Side']['id'];

		$pageLimits = !empty($product['pageLimits'][$mediaId][$sideId])
			? $product['pageLimits'][$mediaId][$sideId]
			: []
		;

		$totalPages = 0;
		$fileCount = 0;

		$pageCounts = Hash::extract($orderItem, 'OrderItemFile.{n}.count_order_item_file_page');

		foreach ($pageCounts as $pageCount) {
			$fileCount++;
			$totalPages += $pageCount;
		}

		if ($fileCount > 1) {
			$strFile = 'files';
			$strContain = 'contain';
			$strDoes = 'do';
			$strHas = 'have';
		} else {
			$strFile = 'file';
			$strContain = 'contains';
			$strDoes = 'does';
			$strHas = 'has';
		}

		if ($totalPages == 1) {
			$strPages = 'page';
		} else {
			$strPages = 'pages';
		}

		if ($pageLimits) {

			if (!empty($pageLimits['pages_min']) && $totalPages < $pageLimits['pages_min']) {

				return [
					'success' => false,
					'code' => 'page-count-below-minimum',
					'message' => [
						'heading' => 'The ' . $strFile . ' you have uploaded ' . $strDoes . ' not have enough pages for the binding you have chosen.',
						'message' => 'You must have at least ' . $pageLimits['pages_min'] . ' pages; your ' . $strFile . ' ' . $strHas . ' ' . $totalPages . ' ' . $strPages . '. Please either upload more/different files or choose a different product.'
					],
					'data' => [
						'limit' => $pageLimits['pages_min']
					],
					'action' => 'upload'
				];

			}

			if (!empty($pageLimits['pages_max']) && $totalPages > $pageLimits['pages_max']) {

				return [
					'success' => false,
					'code' => 'page-count-above-maximum',
					'message' => [
						'heading' => 'The ' . $strFile . ' you have uploaded ' . $strHas . ' too many pages for the binding you have chosen.',
						'message' => 'The maximum you can have is ' . $pageLimits['pages_max'] . '; your ' . $strFile . ' ' . $strHas . ' '  . $totalPages . ' ' . $strPages . '. Please either remove one or more files or choose a different product.'
					],
					'data' => [
						'limit' => $pageLimits['pages_max']
					],
					'action' => 'upload'
				];

			}

		}

		if ($pageDivisor) {

			$modulo = $totalPages % $pageDivisor;
			$bindingName = strtolower($productMap['products'][$productId]['binding_name']);

			if ($modulo) {

				$strInsertPages = $modulo == 1
					? 'page'
					: 'pages'
				;

				return [
					'success' => false,
					'code' => 'page-count-divisor',
					'message' => [
						'heading' => 'The number of pages in a ' . $bindingName . ' document must be divisible by ' . $pageDivisor,
						'message' => 'As your ' . $strFile . ' ' . $strHas . ' ' . $totalPages . ' ' . $strPages . ' we need to insert ' . $modulo . ' blank ' . $strInsertPages . ' to bring the total to ' . ($totalPages + $modulo) . '.'
					],
					'data' => [
						'insert_page_count' => $modulo
					]
				];

			}

		}

		return ['success' => true];

	}

	public function bindingOptions($productId = null, $orientation = null) {

		if (!$productId) {

			return [
				'success' => false,
				'code' => 'no-product-id',
				'from' => 'Product.bindingOptions'
			];

		}

		if (!$orientation) {

			return [
				'success' => false,
				'code' => 'no-orientation',
				'from' => 'Product.bindingOptions'
			];

		}

		$product = $this->find(
			'first',
			[
				'conditions' => [
					'Product.id' => $productId
				],
				'contain' => [
					'ProductBindingEdgeType' => [
						'BindingEdgeType'
					],
					'ProductBindingSideType' => [
						'BindingSideType'
					]
				]
			]
		);

		return $product;

	}

	public function hasRelatedRows($modelName = null, $productId = null) {

		if (!$productId || !$modelName) {
			return false;
		}

		return $this->{$modelName}->find(
			'count',
			[
				'conditions' => [
					$modelName . '.product_id' => $productId
				]
			]
		);

	}

}
