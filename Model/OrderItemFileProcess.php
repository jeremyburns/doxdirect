<?php
App::uses('AppModel', 'Model');
/**
* OrderItemFileProcess Model
*
*/
class OrderItemFileProcess extends AppModel {


	public $order = 'sequence';

	/**
* Display field
*
* @var string
*/

	/**
* hasMany associations
*
* @var array
*/
	public $belongsTo = array(
		'OrderItemFile' => array(
			'className' => 'ProcesOrderItemFile',
			'foreignKey' => 'order_item_file_id',
			'dependent' => false
		),
		'Process' => array(
			'className' => 'Process',
			'foreignKey' => 'process_id',
			'dependent' => false
		)
	);

	public function record($orderItemFileId = null, $processCode = null, $config = null) {

		if (!$processCode || !$orderItemFileId) {

			return array(
				'success' => false,
				'code' => 'missing-parameter'
			);

		}

		$processId = $this->Process->getIdByCode($processCode);

		if (!$processId) {

			return array(
				'success' => false,
				'code' => 'invalid-process-code'
			);

		}

		$data = array(
			'process_id' => $processId,
			'order_item_file_id' => $orderItemFileId
		);

		if ($config) {
			$data['config'] = json_encode($config);
		}

		$this->set($data);

		if ($this->save()) {

			return array('success' => true);

		}

		return array(
			'success' => false,
			'code' => 'save-failed'
		);

	}

	public function recordMany($orderItemFileIds = [], $processCode = null, $config = null) {

		if (!$processCode || !$orderItemFileIds) {

			return array(
				'success' => false,
				'code' => 'missing-parameter'
			);

		}

		foreach ($orderItemFileIds as $orderItemFileId) {

			$this->record(
				$orderItemFileId,
				$processCode,
				$config
			);
		}

		return array('success' => true);

	}

}
