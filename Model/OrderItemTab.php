<?php
App::uses('AppModel', 'Model');
/**
 * OrderItemTab Model
 *
 * @property OrderItem $OrderItem
 */
class OrderItemTab extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'filename';

    /**
     * Order field
     *
     * @var string
     */
    public $order = 'before_page';

    /**
     * hasMany associations
     *
     * @var array
     */
    public $belongsTo = array(
        'OrderItem' => array(
            'className' => 'OrderItem',
            'foreignKey' => 'order_item_id',
            'dependent' => true,
            'counterCache' => 'count_order_item_tab'
        )
    );


    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'order_item_id' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Tabs must belong to an order item.'
            )
        ),
        'before_page' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Please specify which page this tab should go before.',
                'last' => true
            ),
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Page before must be a number.',
                'last' => true
            ),
            'unique' => array(
                'rule' => array(
                    'checkUnique',
                    array(
                        'order_item_id',
                        'before_page'
                    )
                ),
                'message' => 'You already have a tab before this page.'
            )
        ),
        'text' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Please specify the text to appear on this tab.',
                'last' => true
            ),
            'minLength' => array(
                'rule' => array('minLength', 1),
                'message' => 'Tab text must have at least 1 character.',
                'last' => true
            ),
            'maxLength' => array(
                'rule' => array('maxLength', 50),
                'message' => 'Tab text cannot contain more than 50 characters.'
            )
        )
    );

    public function beforeDelete($cascade = true) {

        $orderItemTab = $this->findById($this->id);

        if ($orderItemTab) {
            $this->orderItemId = $orderItemTab['OrderItemTab']['order_item_id'];
        }

    }

    public function afterDelete() {

        if ($this->orderItemId) {
            $this->OrderItem->updateTabCount($this->orderItemId);
        }

    }


    public function add($data = array()) {

        if (!$data) {

            return array(
                'success' => false,
                'code' => 'no-data'
            );

        }

        $this->create();
        $result = $this->save($data);

        if ($result) {

            return array('success' => true);

        } else {

            return array(
                'success' => false,
                'code' => 'save-failed',
                'errors' => $this->validationErrors
            );

        }

        return array(
            'success' => false,
            'code' => 'unknown'
        );

    }

    public function removeFromOrderItem($orderItemId = null) {

        if (!$orderItemId) {
            return;
        }

        $this->deleteAll(
            array(
                'OrderItemTab.order_item_id' => $orderItemId
            )
        );

        return;

    }

}
