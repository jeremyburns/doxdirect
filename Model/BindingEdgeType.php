<?php

App::uses('AppModel', 'Model');

/**
 * BindingEdgeType Model
 *
 * @property OrderItem $OrderItem
 */
class BindingEdgeType extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';
	public $actsAs = ['Defaultable'];

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = [
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'binding_edge_type_id',
			'dependent' => false
		],
		'ProductBindingEdgeType' => [
			'className' => 'ProductBindingEdgeType',
			'foreignKey' => 'binding_edge_type_id'
		]
	];

	public function shortLong($bindingEdgeTypeId = null) {

		if (!$bindingEdgeTypeId) {
			return null;
		}

		$name = $this->field(
			'name',
			['BindingEdgeType.id' => $bindingEdgeTypeId]
		);

		if (!$name) {
			return null;
		}

		$name = strtolower($name);

		if (stripos($name, 'long') !== false) {
			return 'long';
		} elseif (stripos($name, 'short') !== false) {
			return 'short';
		}

		return null;
	}

}
