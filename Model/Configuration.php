<?php
App::uses('AppModel', 'Model');
/**
 * Configuration Model
 *
  */
class Configuration extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public function afterSave($created, $options = []) {

		if (isset($this->data['Configuration']['code'])) {
			Cache::delete($this->data['Configuration']['code'], 'configuration');
		}
	}

	public function getConfigurationValue($codes = null, $implodeString = null, $refresh = false) {

		if (!$codes) {
			return null;
		}

		if (!is_array($codes)) {

			return $this->getValue($codes, $refresh);

		}

		$values = array();

		foreach ($codes as $code) {

			$value = $this->getValue($code, $refresh);

			if ($value !== null) {
				$values[] = $value;

				if ($refresh === true) {
					Cache::delete($code, $cacheKey);
				}

			}

		}

		if ($values && $implodeString !== null) {

			return implode($implodeString, $values);

		}

		return $values;

	}

	private function getValue($code = null, $refresh = false) {

		if (!$code) {
			return null;
		}

		$cacheKey = 'configuration';

		if ($refresh === true) {

			Cache::delete($code, $cacheKey);

		}

		$model = $this;

		return Cache::remember(
			$code,
			function() use ($model, $code) {

				// Return the value
				return $model->field(
					'name',
					array(
						'code' => $code,
						'is_active' => true
					)
				);

			},
			$cacheKey
		);

	}


}
