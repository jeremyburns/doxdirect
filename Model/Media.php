<?php
App::uses('AppModel', 'Model');
/**
 * Media Model
 *
 * @property Order $Order
 */
class Media extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $actsAs = ['Defaultable'];
	public $order = 'weight';

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'media_id',
			'dependent' => false
		],
		'ProductMedia' => [
			'className' => 'ProductMedia',
			'foreignKey' => 'media_id',
		]
	];

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = [
		'PaperSize' => [
			'className' => 'PaperSize',
			'foreignKey' => 'paper_size_id'
		]
	];

	private $mediaTypes = [
		'matt',
		'silk',
		'satin',
		'plain'
	];

	public function beforeSave($options = []) {

		if (!empty($this->data['Media']['pace_name'])) {

			$this->data['Media']['paper_size_id'] = substr($this->data['Media']['pace_name'], 0, 2);

			foreach ($this->mediaTypes as $mediaType) {

				if (strpos($this->data['Media']['pace_name'], $mediaType)) {
					$this->data['Media']['type'] = $mediaType;
				}

			}

		}

		return parent::beforeSave();

	}

	public function updateTypes() {

		$medias = $this->find (
			'all',
			[
				'fields' => [
					'id',
					'pace_name',
					'type'
				]
			]
		);

		foreach ($medias as $mediaId => $media) {

			foreach ($this->mediaTypes as $mediaType) {

				if (strpos($media['Media']['pace_name'], $mediaType)) {

					if ($media['Media']['type'] !== $mediaType) {

						$this->id = $media['Media']['id'];

						$result = $this->saveField(
							'type',
							$mediaType,
							[
								'callbacks' => false
							]
						);

						continue;

					}

				}

			}

		}

	}

}
