<?php
App::uses('AppModel', 'Model');
/**
 * Source Model
 *
 * @property Order $Order
 */
class Source extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $order = 'sequence';

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'source_id',
			'dependent' => false
		)
	);

	public function findActive() {

        return $this->find(
        	'list',
        	array(
        		'conditions' => array(
        			'Source.is_active' => true
        		)
        	)
        );
    }

}