<?php
App::uses('AppModel', 'Model');
/**
 * ProductTabType Model
 *
 * @property Product $Product
 * @property Tab $Tab
 */
class ProductTabType extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'TabType' => array(
			'className' => 'TabType',
			'foreignKey' => 'tab_type_id',
			'dependent' => false
		)
	);

}