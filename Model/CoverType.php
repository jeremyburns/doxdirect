<?php
App::uses('AppModel', 'Model');
/**
 * CoverType Model
 *
 */
class CoverType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $actsAs = array('Defaultable');
	public $order = 'name';

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'OrderItemFrontCover' => array(
			'className' => 'OrderItem',
			'foreignKey' => 'cover_type_front_id',
			'dependent' => false
		),
		'OrderItemBackCover' => array(
			'className' => 'OrderItem',
			'foreignKey' => 'cover_type_back_id',
			'dependent' => false
		),
		'OrderItemOuterCover' => array(
			'className' => 'OrderItem',
			'foreignKey' => 'cover_type_outer_id',
			'dependent' => false
		),
		'OrderItemWrapAround' => array(
			'className' => 'OrderItem',
			'foreignKey' => 'cover_type_wrap_around_id',
			'dependent' => false
		)
	);

}
