<?php
App::uses('AppModel', 'Model');
/**
 * ProductClick Model
 *
 * @property Order $Order
 * @property OrderStatus $OrderStatus
 */
class ProductClick extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'Click' => array(
			'className' => 'Click',
			'foreignKey' => 'click_id',
			'dependent' => false
		)
	);

}