<?php
App::uses('AppModel', 'Model');
/**
 * TabType Model
 *
 * @property Product $Product
 * @property TabType $Tab
 */
class TabType extends AppModel {

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'ProductTabType' => [
			'className' => 'ProductTabType',
			'foreignKey' => 'tab_type_id'
		]
	];

}
