<?php
App::uses('AppModel', 'Model');
App::uses('CakeTime', 'Utility');
/**
 * DeliveryOption Model
 *
 * @property Order $Order
 */
class DeliveryOption extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';
	public $order = 'price';
	public $actsAs = [
		'Defaultable',
		'Pace'
	];

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'delivery_option_id',
			'dependent' => false,
			'counterCache' => 'count_order'
		)
	);

	public function refresh($refresh = true) {

		$cacheKey = 'delivery_options';

		if ($refresh === true) {
			Cache::delete($cacheKey, 'pace');
		}

		$model = $this;

		$deliveryOptions = Cache::remember(
			$cacheKey,
			function() use ($model, $refresh) {

				$deliveryOptions = $this->getDeliveryOptions($refresh);

				return $deliveryOptions;

			},
			'pace'
		);

		$this->updateDeliveryOptions($deliveryOptions);

		return [
			'success' => true,
			'result' => $deliveryOptions
		];

	}

	/**
	 * @param array $paceDeliveryOptions
	 * @return array
	 * @throws Exception
	 */
	public function updateDeliveryOptions($paceDeliveryOptions = array()) {

		// Wrap this in a transaction
		$dataSource = $this->getDataSource();
		$dataSource->begin();

		// First we need to loop through the current delivery options and mark as inactive any that are not in the new set

		// Get the new ids by flipping the values
		$paceDeliveryOptionIds = array_flip(array_keys($paceDeliveryOptions));

		// Get the current ids
		$currentDeliveryOptionIds = $this->find('list');

		// Make sure we have some current ones
		if ($currentDeliveryOptionIds) {

			// Loop through them
			foreach ($currentDeliveryOptionIds as $currentDeliveryOptionId => $currentDeliveryOption) {

				// Is the current one in the new set?
				if (!isset($paceDeliveryOptionIds[$currentDeliveryOptionId])) {

					// No, so mark the current one as inactive
					$this->id = $currentDeliveryOptionId;
					$this->set('is_active', false);
					$this->save();

				}
			}

		}

		// Now we need to add or update the current ones from the new list

		// First get the current delivery options
		$currentDeliveryOptions = $this->find('all');
		$tmpCurrentDeliveryOptions = [];

		// It'll be easier later if the array key matches the actual id, so create a new array
		foreach ($currentDeliveryOptions as $currentDeliveryOption) {
			$tmpCurrentDeliveryOptions[$currentDeliveryOption['DeliveryOption']['id']] = $currentDeliveryOption['DeliveryOption'];
		}

		// Now replace our current delivery options array with the new one
		$currentDeliveryOptions = $tmpCurrentDeliveryOptions;
		unset($tmpCurrentDeliveryOptions);

		// Set a flag to record the presence of an error
		$errors = array();

		// Now loop through the new delivery options
		foreach ($paceDeliveryOptions as $paceDeliveryOption) {

			// Is this one in the current set?
			if (isset($currentDeliveryOptions[$paceDeliveryOption['id']])) {

				// It exists, so compare and update if needed

				// Set a flag to denote whether the record has changed
				$changed = false;

				// Get the current delivery option
				$currentDeliveryOption = $currentDeliveryOptions[$paceDeliveryOption['id']];

				// Step through the fields of the new one
				foreach ($paceDeliveryOption as $paceDeliveryOptionFieldName => $paceDeliveryOptionFieldValue) {

					// Do the values match
					if ($currentDeliveryOption[$paceDeliveryOptionFieldName] <> $paceDeliveryOptionFieldValue) {

						$changed = true;
						$currentDeliveryOption[$paceDeliveryOptionFieldName] = $paceDeliveryOptionFieldValue;

					}
				}

				if ($changed) {

					unset($currentDeliveryOption['modified']);
					unset($currentDeliveryOption['created']);

					$this->set($currentDeliveryOption);
					$result = $this->save();

					if (!$result) {
						$errors[] = $result;
					}

				}

			} else {

				// This is a new delivery option
				$this->create();
				$this->set($paceDeliveryOption);
				$result = $this->save();

				if (!$result) {
					$errors[] = $result;
				}

			}

		}

		if ($errors) {

			$dataSource->rollback();

			return array(
				'success' => false,
				'errors' => $errors
			);

		}

		$dataSource->commit();

		// We probably havn't got a default...
		if (!$this->getDefault()) {

			// Find the first with the maximum amount of days where it's not guaranteed - it's likely to be Royal Mail
			$defaultId = $this->find(
				'first',
				array(
					'conditions' => array(
						'DeliveryOption.is_active' => true,
					),
					'order' => array(
						'DeliveryOption.days' => 'desc',
						'DeliveryOption.is_guaranteed' => 'asc'
					)
				)
			);

			// We've got it, so set it
			if ($defaultId) {
				$this->setDefault($defaultId['DeliveryOption']['id']);
			}

		}

		// Clear the cache
		Cache::delete('delivery_options', 'pace');

		return array(
			'success' => true
		);

	}

	public function minimumPrice($deliveryOptionId = null, $amountNet = 0) {

		if (!$deliveryOptionId || !$amountNet) {
			return false;
		}

		$deliveryOption = $this->find(
			'first',
			array(
				'conditions' => array(
					'DeliveryOption.id' => $deliveryOptionId
				),
				'fields' => array(
					'DeliveryOption.minimum_order_value',
					'DeliveryOption.price'
				)
			)
		);

		if (!$deliveryOption) {
			return false;
		}

		$minimumOrderValue = $deliveryOption['DeliveryOption']['minimum_order_value'];

		if ($minimumOrderValue > 0 && $amountNet >= $minimumOrderValue) {

			// This is free
			return 0;

		} else {

			return $deliveryOption['DeliveryOption']['price'];

		}

	}

	// public function deliveryDetails($despatchDate = null, $deliveryDays = null, $weekdaysOnly = true) {
	public function deliveryDetails($deliveryOptionId = null, $despatchDate = null) {

		$deliveryOption = $this->find(
			'first',
			array(
				'conditions' => array(
					'DeliveryOption.id' => $deliveryOptionId
				),
				'fields' => array(
					'DeliveryOption.name',
					'DeliveryOption.days',
					'DeliveryOption.is_guaranteed',
					'DeliveryOption.latest_delivery_time',
					'DeliveryOption.weekdays_only'
				)
			)
		);

		$result = array(
			'delivery_option_name' => $deliveryOption['DeliveryOption']['name'],
			'is_guaranteed' => $deliveryOption['DeliveryOption']['is_guaranteed'],
			'latest_delivery_time' => $deliveryOption['DeliveryOption']['latest_delivery_time']
		);

		$deliveryDays = $deliveryOption['DeliveryOption']['days'];
		$weekdaysOnly = $deliveryOption['DeliveryOption']['weekdays_only'];

		$deliveryDate = $despatchDate->modify('+' . $deliveryDays . ' days');

		$weekday = $deliveryDate->format('w');

		if ($weekday == $this->saturday && $weekdaysOnly) {
			$deliveryDate->modify('+2 days');
		} elseif ($weekday == $this->sunday) {
			$deliveryDate->modify('+1 days');
		}

		// Load the Holiday model
		App::import('model', 'Holiday');

		// Start a new instance
		$this->Holiday = new Holiday();

		// If the delivery date falls on a day when there are no deliveries...
		while (!$this->Holiday->will('deliver', $deliveryDate)) {
			// ...move it forward by a day
			$deliveryDate->modify('+1 days');
		}

		$result['delivery_date_obj'] = $deliveryDate;
		$result['delivery_date'] = $deliveryDate->format('Y-m-d');

		// Calls a function in AppModel
		$result['delivery_date_formatted'] = $this->niceFormattedDate($deliveryDate);

		$result['latest_delivery_time'] = !empty($result['latest_delivery_time'])
			? $result['latest_delivery_time']
			: NULL
		;

		return $result;

	}

}
