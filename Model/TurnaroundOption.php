<?php
App::uses('AppModel', 'Model');
App::uses('CakeTime', 'Utility');
/**
 * TurnaroundOption Model
 *
 * @property Order $Order
 */
class TurnaroundOption extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $actsAs = ['Defaultable'];
	public $order = ['days' => 'asc'];

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'Order' => [
			'className' => 'Order',
			'foreignKey' => 'turnaround_option_id',
			'dependent' => false
		]
	];

	/**
	 * This function returns the hour beyond which same day turnaround is unavailable
	 * @return integer The hour (in 24 hour time) when same day orders have to be placed
	 */
	public function sameDayTurnaroundCutOffHour() {

		// The same_day_turnaround_cut_off_hour value is stored in the configures table
		$sameDayTurnaroundCutOffHour = $this->getConfigurationValue('same_day_cut_off_hour');

		if (empty($sameDayTurnaroundCutOffHour)) {
			$sameDayTurnaroundCutOffHour = 11;
		}

		return $sameDayTurnaroundCutOffHour;

	}

	/**
	 * This function returns the minute beyond which same day turnaround orders must be placed. It's a soft setting that is not shown to the user and is used to allow a bit of lee way
	 * @return integer The minute past the hour when same day orders have to be placed
	 */
	public function sameDayTurnaroundCutOffMinute() {

		// The same_day_turnaround_cut_off_hour value is stored in the configures table
		$sameDayTurnaroundCutOffMinute = $this->getConfigurationValue('same_day_cut_off_minute');

		if (empty($sameDayTurnaroundCutOffMinute)) {
			$sameDayTurnaroundCutOffMinute = 15;
		}

		return $sameDayTurnaroundCutOffMinute;

	}

	/**
	 * This function determines whether same day processing is available 'now' for a given product.
	 * The time aspect (is it past 11:15) is ignored when $isOrder is fale; in other words, you can quote it but you can't order it.
	 * @param  boolean [$isOrder           = false]  Whether this check is in an order (i.e. at checkout) or in a quote
	 * @return boolean An indicator of whether same day is allowed
	 */
	public function sameDayIsAvailable($data = []) {

		// Set up some defaults
		$defaults = [
			'is_order' => false,
			'dateTime' => 'NOW'
		];

		// Merge the defaults and data
		$options = array_merge(
			$defaults,
			$data
		);

		// Start off assuming the product does not need any extra processing day
		// If it does, you can't do same day processing
		$extraProcessingDays = 0;

		// Start off assuming same day processing is available
		$available = true;

		// What are the cut off hour and minute...?
		$sameDayTurnaroundCutOffHour = $this->sameDayTurnaroundCutOffHour();
		$sameDayTurnaroundCutOffMinute = $this->sameDayTurnaroundCutOffMinute();

		// Assume the published cut off time is o'clock (this is what we publish to the user)
		$sameDayTurnaroundCutOffHourText = $sameDayTurnaroundCutOffHour . ':00';

		// For consistency, set up some messages for user feedback
		$messages = [
			'available' => sprintf('Please note that orders for same day processing must be placed by %s.', $sameDayTurnaroundCutOffHourText),
			'not-available' => sprintf('Please note that same day processing can only be applied to orders placed before %s.', $sameDayTurnaroundCutOffHourText),
			'not-available-product' => 'Same day order processing is not available for this product.',
			'not-available-holiday' => 'Because today is a holiday at Doxdirect, same day processing is not available.',
			'changed' => ' We\'ve changed your processing option to the next available day. You can still select a faster delivery option if you\'d like your documents sooner.'
		];

		$productId = !empty($options['product_id'])
			? $options['product_id']
			: null
		;

		if (isset($options['qualifies_for_same_day_turnaround'])) {

			// Only change the value if it's set and then look for a positive value
			$qualifiesForSameDayTurnaround = !empty($options['qualifies_for_same_day_turnaround']);

			if (!$qualifiesForSameDayTurnaround) {
				// Trip the extra processing days up to 1 so same day is not allowed
				$extraProcessingDays = 1;
			}

		}

//		if ($productId === null && !isset($qualifiesForSameDayTurnaround)) {
//
//			// We can't work this out, so return an error
//			return [
//				'success' => false,
//				'allowed' => false,
//				'code' => 'no-product-or-qualifier',
//				'message' => 'That is an invalid product or order.'
//			];
//
//		}

		// Is this an order (true) or a quote (false)
		$isOrder = $options['is_order'];

		// Which option has been selected?
		$selectedTurnaroundOptionId = isset($options['turnaround_option_id'])
			? intval($options['turnaround_option_id'])
			: null
		;

		if ($productId) {

			// Get the product map
			$productMap = $this->Order->OrderItem->Product->productMap();

			// Does this product need extra processing days?
			if (!empty($productMap['products'][$productId]['extra_processing_day'])) {
				// Yes, so collect it
				$extraProcessingDays = $productMap['products'][$productId]['extra_processing_day'];
			}

		}

		if ($extraProcessingDays) {

			// Same day is never available for this product (typically books) or a product in the order (when many)
			$result = [
				'success' => true,
				'available' => false
			];

			// Has the user selected same day?
			// No point warning him if he hasn't
			if ($selectedTurnaroundOptionId === 1) {

				// Set the right message key
				$result['message'] = $messages['not-available-product'];

				$result = $this->sameDayProcessingChanged($result);

			}

			return $result;

		}

		// If we're here, we've got a product that allows same day processing

		if (!$isOrder) {
			// This is only a quote, so allow it
			$result = [
				'success' => true,
				'available' => true
			];

			// Only show the message if the user has selected the same day option
			if ($selectedTurnaroundOptionId === 1) {
				$result['message'] = $messages['available'];
			}

			return $result;

		}

		// If we are here we are at the order stage, so need to check against date (for holidays and the like) /time

		$date = $this->getTimestamp(['dateTime' => $options['dateTime']]);

		$hour = $date->format('H');
		$minute = $date->format('i');

		if ($hour >= $sameDayTurnaroundCutOffHour) {
			// We're later than the cut off hour, so decline it
			$available = false;
		}

		if ($available && ($hour == $sameDayTurnaroundCutOffHour)) {
			// We're in the cut off hour - is it past the cut off minute?
			if ($minute > $sameDayTurnaroundCutOffMinute) {
				$available = false;
			}
		}

		if (!$available) {
			$result = [
				'success' => true,
				'available' => false
			];

			if ($selectedTurnaroundOptionId === 1) {

				$result['message'] = $messages['not-available'];

				$result = $this->sameDayProcessingChanged($result);

			}

			return $result;

		}

		App::uses('Holiday', 'Model');
		$this->Holiday = ClassRegistry::init('Holiday');

		$willDespatch = $this->Holiday->will('despatch', $date);

		if (!$willDespatch) {
			// Same day is not allowed as we are not despatching due to holiday

			$result = [
				'success' => true,
				'available' => false
			];

			if ($selectedTurnaroundOptionId === 1) {

				$result['message'] = $messages['not-available-holiday'];

				$result = $this->sameDayProcessingChanged($result);

			}

			return $result;
		}

		// If we're here, the product allows same day and we're before the cut off time, so allow it
		$result = [
			'success' => true,
			'available' => true
		];

		if ($selectedTurnaroundOptionId === 1) {
			$result['message'] = $messages['available'];
		}

		return $result;

	}

	private function sameDayProcessingChanged($result = []) {

		$result['message'] .= $messages['changed'];

		// Find the one day option

		$newOption = $this->field(
			'id',
			[
				'TurnaroundOption.days' => 1
			]
		);

		$result['new_option'] = $newOption;

		return $result;

	}

	public function convertToPaceCode($turnaroundOptionId = null, $refresh = false) {

		if ($refresh === true) {
			Cache::delete($turnaroundOptionId, 'pace_codes');
		}

		$model = $this;

		return Cache::remember(
			'TurnaroundOption.' . $turnaroundOptionId,
			function() use ($model, $turnaroundOptionId){

				return $this->field(
					'days',
					[
						'TurnaroundOption.id' => $turnaroundOptionId
					]
				);

			},
			'pace_codes'
		);

	}

	public function despatchDate($turnaroundOptionId = null, $orderTime = null, $bindingTypeId = null) {

		$sameDayTurnaroundCutOffHour = $this->sameDayTurnaroundCutOffHour();

		$turnaroundOption = $this->find(
			'first',
			[
				'conditions' => [
					'TurnaroundOption.id' => $turnaroundOptionId
				],
				'fields' => [
					'TurnaroundOption.name',
					'TurnaroundOption.days'
				]
			]
		);

		$result['turnaround_option_name'] = $turnaroundOption['TurnaroundOption']['name'];

		$turnaroundDays = intval($turnaroundOption['TurnaroundOption']['days']);
		$hour = intval($orderTime->format('H'));
		$minute = intval($orderTime->format('i'));
		$tooLateForToday = false;

		if ($turnaroundDays === 0) {

			if ($hour >= $sameDayTurnaroundCutOffHour) {
				$tooLateForToday = true;
				$turnaroundDays++;
			}
		}

		$extraProcessingDay = 0;

		if ($bindingTypeId) {
			$extraProcessingDay = $this->Order->OrderItem->BindingType->field(
				'extra_processing_day',
				[
					'BindingType.id' => $bindingTypeId
				]
			);
		}

		$turnaroundDays += intval($extraProcessingDay);

		$despatchDate = $orderTime;

		// Load the Holiday model
		App::import('model', 'Holiday');

		// Start a new instance
		$this->Holiday = new Holiday();

		// The $final variable indicates we have reached the final despatch date
		$final = false;
		$turnaroundDaysCounter = $turnaroundDays;

		while (!$final) {

			// If the despatch date is either a day when we don't despatch or is a weekend...
			while (!$this->Holiday->will('despatch', $despatchDate) || $this->Holiday->isWeekend($despatchDate)) {

				// ...move it forward by a day
				$despatchDate->modify('+1 day');

			}

			// Do we have to move the date forward?
			if ($turnaroundDaysCounter > 0) {
				// Yes, so move it
				$despatchDate->modify('+1 day');
				// ... and subtract 1 from the number of tunraround days
				$turnaroundDaysCounter--;
			}

			// Hvae we used up all the turnaround days?
			if ($turnaroundDaysCounter <= 0) {
				// Yes, so we are there
				$final = true;
			}

			// Just check we haven't moved it to a non despatch day
			while (!$this->Holiday->will('despatch', $despatchDate) || $this->Holiday->isWeekend($despatchDate)) {

				// If so, move it
				$despatchDate->modify('+1 day');

			}

		}

		$result['despatch_date_obj'] = $despatchDate;
		$result['despatch_date'] = $despatchDate->format('Y-m-d');

		// Calls a function in AppModel
		$result['despatch_date_formatted'] = $this->niceFormattedDate($despatchDate);

		if ($turnaroundDays === 0) {
			$result['despatch_date_notice'] = 'Available for orders placed before ' . $sameDayTurnaroundCutOffHour . ':00am';
		} elseif ($tooLateForToday) {
			$result['despatch_date_notice'] = 'Same day despatch is only available for orders placed before ' . $sameDayTurnaroundCutOffHour . ':00am';
		}

		return $result;

	}

}
