<?php
App::uses('AppModel', 'Model');
/**
 * Country Model
 *
 */
class Country extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = array(
		'Defaultable',
		'Pace'
	);

	public $order = [
		'Country.tier',
		'Country.name'
	];

/**
 * belongsTo associations
 *
 * @var array
 */
/*
	public $belongsTo = array(
		'Currency' => array(
			'className' => 'Currency',
			'foreignKey' => 'currency_id',
			'dependent' => false
		)
	);
*/

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'UserAddress' => array(
			'className' => 'UserAddress',
			'foreignKey' => 'country_id',
			'dependent' => false
		)
	);

	public function findActive() {

		return $this->find(
			'list',
			array(
				'conditions' => array(
					'Country.is_active' => true
				)
			)
		);

	}

	public function refresh($refresh = true) {

		$cacheKey = 'countries';

		if ($refresh === true) {
			Cache::delete($cacheKey, 'pace');
		}

		$model = $this;

		$countries =  Cache::remember(
			$cacheKey,
			function() use ($model, $refresh) {

				$countries = $this->getCountries($refresh);

				return $countries;

			},
			'pace'
		);

		$this->updateCountries($countries);

		return [
			'success' => true,
			'result' => $countries
		];

	}

    /**
     * @param array $countries
     * @return array
     * @throws Exception
     */
    public function updateCountries($paceCountries = array()) {

		// Wrap this in a transaction
		$dataSource = $this->getDataSource();
		$dataSource->begin();

		// First we need to loop through the current countries and mark as inactive any that are not in the new set

		// Get the new ids by flipping the values
		$countryIds = array_flip(array_keys($paceCountries));

		// Get the current ids
		$currentCountries = $this->find('list');

		// Make sure we have some current ones
		if ($currentCountries) {

			// Loop through them
			foreach ($currentCountries as $currentCountryId => $currentCountry) {

				// Is the current one in the new set?
				if (!isset($countryIds[$currentCountryId])) {

					// No, so mark the current one as inactive
					$this->id = $currentCountryId;
					$this->set('is_active', false);
					$this->save();

				}
			}

		}

		// Now we need to add or update the current ones from the new list

		// First get the current countries
		$currentCountrys = $this->find('all');
        $tmpCurrentCountries = [];

		// It'll be easier later if the array key matches the actual id, so create a new array
		foreach ($currentCountrys as $currentCountry) {
			$tmpCurrentCountries[$currentCountry['Country']['id']] = $currentCountry['Country'];
		}

		// Now replace our current countries array with the new one
		$currentCountrys = $tmpCurrentCountries;
		unset($tmpCurrentCountries);

		// Set a flag to record the presence of an error
		$errors = array();

		// Now loop through the new countries
		foreach ($paceCountries as $paceCountry) {

			// Is this one in the current set?
			if (isset($currentCountrys[$paceCountry['id']])) {

				// It exists, so compare and update if needed

				// Set a flag to denote whether the record has changed
				$changed = false;

				// Get the current delivery option
				$currentCountry = $currentCountrys[$paceCountry['id']];

				// Step through the fields of the new one
				foreach ($paceCountry as $paceCountryFieldName => $paceCountryFieldValue) {

					$paceCountryFieldName = Inflector::underscore($paceCountryFieldName);

					// Do the values match
					if ($currentCountry[$paceCountryFieldName] <> $paceCountryFieldValue) {

						$changed = true;
						$currentCountry[$paceCountryFieldName] = $paceCountryFieldValue;

					}
				}

				if ($changed) {

					unset($currentCountry['modified']);
					unset($currentCountry['created']);

					$this->set($currentCountry);
					$result = $this->save();

					if (!$result) {
						$errors[] = $result;
					}

				}

			} else {

				// This is a new delivery option
				$this->create();
				$this->set($paceCountry);
				$result = $this->save();

				if (!$result) {
					$errors[] = $result;
				}

			}

		}

		if ($errors) {

			$dataSource->rollback();

			return array(
				'success' => false,
				'errors' => $errors
			);

		}

		$dataSource->commit();

		// Clear the cache
		Cache::delete('countries', 'pace');

		return array(
			'success' => true
		);

	}

}