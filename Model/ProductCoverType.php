<?php
App::uses('AppModel', 'Model');
/**
 * ProductCoverType Model
 *
 * @property Produc $Product
 * @property CoverType $CoverType
 */
class ProductCoverType extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'CoverType' => array(
			'className' => 'CoverType',
			'foreignKey' => 'cover_type_id',
			'dependent' => false
		)
	);

}
