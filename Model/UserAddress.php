<?php
App::uses('AppModel', 'Model');
/**
 * UserAddress Model
 *
*/
class UserAddress extends AppModel {

/**
 * hasMany associations
 *
 * @var array
 */
	public $belongsTo = [
		'User' => [
			'className' => 'User',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'counterCache' => 'count_user_address'
		],
		'Country' => [
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'dependent' => false,
			'counterCache' => 'count_user_address'
		]
	];

	public $hasMany = [
		'OrderBillingAddress' => [
			'className' => 'Order',
			'foreignKey' => 'billing_address_id',
			'dependent' => false
		],
		'OrderShippingAddress' => [
			'className' => 'Order',
			'foreignKey' => 'shipping_address_id',
			'dependent' => false
		]
	];

	public $validate = [
		'user_id' => [
			'notBlank' => [
				'rule' => 'notBlank',
				'message' => 'Addresses must belong to a user.'
			]
		],
		'company_name' => [
			'maxLength' => [
				'rule' => ['maxLength', 45],
				'message' => 'Company name cannot be longer than 45 characters.'
			]
		],
		'address_1' => [
			'notBlank' => [
				'rule' => 'notBlank',
				'message' => 'Please enter the first line of your address.'
			],
			'maxLength' => [
				'rule' => ['maxLength', 35],
				'message' => 'Address 1 cannot be longer than 35 characters.'
			]
		],
		'address_2' => [
			'maxLength' => [
				'rule' => ['maxLength', 35],
				'message' => 'Address 2 cannot be longer than 35 characters.'
			]
		],
		'address_3' => [
			'maxLength' => [
				'rule' => ['maxLength', 35],
				'message' => 'Address 3 cannot be longer than 35 characters.'
			]
		],
		'city' => [
			'notBlank' => [
				'rule' => 'notBlank',
				'message' => 'Please enter your city.'
			]
		],
		'country_id' => [
			'notBlank' => [
				'rule' => 'notBlank',
				'message' => 'Please enter your country.'
			]
		],
		'post_code' => [
			'notBlank' => [
				'rule' => 'notBlank',
				'message' => 'Please enter your post/zip code.'
			]
		]
	];

	public $displayField = 'full_address';

	var $order = 'full_address';

	public function beforeSave($options = array()) {

		$this->data[$this->alias]['full_address'] = $this->setFullAddress($this->data);

		return parent::beforeSave();

	}

	public function softDelete($userAddressId = null, $userId = null) {

		if (!$userAddressId || !$userId) {

			return [
				'success' => false,
				'message' => 'That is an invalid address or you do not have permission to manage it.'
			];
		}

		$this->id = $userAddressId;

		if ($this->field('user_id') !== $userId) {

			return [
				'success' => false,
				'message' => 'You do not have permission to manage this address.'
			];

		}

		$result = $this->saveField(
			'is_active',
			0,
			[
				'callbacks' => false,
				'validate' => false
			]
		);

		if (!$result) {

			return [
				'success' => false,
				'message' => 'The address could not be deleted.'
			];

		}

		return [
			'success' => true,
			'message' => 'The address has been deleted.'
		];

	}

	public function getActiveAddresses($userId = null, $userAddressId = null) {

		$conditions = [
			'UserAddress.user_id' => $userId,
			'UserAddress.is_active' => true
		];

		if ($userAddressId) {
			$conditions['UserAddress.id'] = $userAddressId;

			$results = $this->find(
				'first',
				['conditions' => $conditions]
			);

		} else {

			$results = $this->find(
				'all',
				['conditions' => $conditions]
			);

		}

		return $results;

	}

/**
	This function is called as part of the order checkout function.
*/
	public function add($userAddress = [], $overridePostCodeCheck = false) {

		$alias = $this->alias;

		switch($alias) {
			case 'ShippingAddress':
				$addressType = 'shipping address';
				break;
			case 'BillingAddress':
				$addressType = 'billing address';
				break;
			default:
				$addressType = 'address';
		}

		if (empty($userAddress[$alias])) {

			return [
				'success' => false,
				'code' => 'no-user-address',
				'message' => sprintf('Please provide a valid %s.', $addressType),
				'from' => 'UserAddress.add'
			];

		}

		if (empty($userAddress[$alias]['id'])) {
			$this->create();
		}

		if (!$overridePostCodeCheck) {

			$existingUserAddresses = $this->find(
				'list',
				[
					'conditions' => [
						$alias . '.user_id' => $userAddress[$alias]['user_id'],
						'LOWER(REPLACE(' . $alias . '.post_code, " " , ""))' => strtolower(str_replace(' ', '', $userAddress[$alias]['post_code'])),
						$alias . '.is_active' => true
					]
				]
			);

			if ($existingUserAddresses) {

				return [
					'success' => false,
					'code' => 'potential-duplicate',
					'message' => [
						'heading' => 'You already have an address with that post code',
						'message' => 'Do you still want to add this new one?'
					],
					'existingUserAddresses' => $existingUserAddresses
				];

			}

		}

		// Now we'll see if there is a complete match
		// If there is, use the saved one

		$addressFields = [
			'company_name',
			'house_number',
			'address_1',
			'address_2',
			'address_3',
			'city',
			'county',
			'post_code'
		];

		$conditions = [
			$alias . '.user_id' => $userAddress[$alias]['user_id'],
			$alias . '.is_active' => true
		];

		foreach ($addressFields as $addressField) {

			if (!empty($userAddress[$alias][$addressField])) {

				$conditions['LOWER(REPLACE(' . $alias . '.' . $addressField . ', " " , ""))'] = strtolower(str_replace(' ', '', $userAddress[$alias][$addressField]));

			}

		}

		$existingUserAddresses = $this->find(
			'first',
			[
				'conditions' => $conditions
			]
		);

		if ($existingUserAddresses) {

			// There's an exact match, so use it

			return [
				'success' => true,
				'message' => sprintf('Your %s has been added.', $addressType),
				'userAddress' => $existingUserAddresses
			];

		}

		// Save the new address
		$result = $this->save($userAddress);

		if (!$result) {

			return [
				'success' => false,
				'code' => 'user-address-failed-validation',
				'userAddress' => $result,
				'message' => sprintf('Please resolve these issues with your %s.', $addressType),
				'errors' => $this->validationErrors
			];

		}

		return [
			'success' => true,
			'message' => sprintf('Your %s has been added.', $addressType),
			'userAddress' => $result
		];

	}

	public function setPaceContactId($userAddressId = null, $contactId = null) {

		$existingContactId = $this->field(
			'pace_contact_id',
			['UserAddress.id' => $userAddressId]
		);

		if (!$existingContactId) {

			$this->id = $userAddressId;

			$this->saveField('pace_contact_id', $contactId, ['callbacks' => false, 'validate' => false]);

		}

		return true;

	}

	public function addressesAsList($userId = null) {

		if (!$userId) {
			return [];
		}

		return $this->find(
			'list',
			[
				'conditions' => [
					'UserAddress.user_id' => $userId,
					'UserAddress.is_active' => true
				],
				'order' => 'UserAddress.full_address'
			]
		);

	}

	/**
	 * This is a convenience function that takes the address components and creates a single string with the whole address in
	 * It's used for displaying lists of addresses - on the checkout form, for example
	 * @return string A string formed of the address components
	 */
	public function setFullAddress($userAddress = []) {

		$fields = [
			'company_name',
			'house_number',
			'address_1',
			'address_2',
			'address_3',
			'city',
			'county',
			'post_code'
		];

		$thisAddress = [];

		foreach ($fields as $field) {
			if (!empty($userAddress[$this->alias][$field])) {
				$thisAddress[] = $userAddress[$this->alias][$field];
			}
		}

		if (!empty($userAddress[$this->alias]['country_id'])) {
			$thisAddress[] = $this->Country->field(
				'name',
				['Country.id' => $userAddress[$this->alias]['country_id']]
			);
		}

		return implode(', ', $thisAddress);

	}

/*
	public function createCustomer(Model $model, $user = []) {

		$data = [
			'source' => $this->config['source'],
			'email' => $user['username'],
			'billingFirstName' => $user['first_name'],
			'billingLastName' => $user['last_name'],
			'billingCompanyName' => $user['BillingAddress']['company_name'],
			'billingAddress1' => $user['BillingAddress']['address_1'],
			'billingAddress2' => $user['BillingAddress']['address_2'],
			'billingAddress3' => $user['BillingAddress']['address_3'],
			'billingCity' => $user['BillingAddress']['city'],
			'billingCounty' => $user['BillingAddress']['county'],
			'billingPostCode' => $user['BillingAddress']['post_code'],
			'billingCountryID' => $user['BillingAddress']['country_id'],
			'useBillingForShipping' => $user['BillingAddress']['use_for_shipping']
		];

		if ($user['BillingAddress']['use_for_shipping'] && !empty($user['ShippingAddress'])) {
			$data = array_merge(
				$data,
				[
					'shippingFirstName' => $user['first_name'],
					'shippingLastName' => $user['first_name'],
					'shippingCompanyName' => $user['ShippingAddress']['company_name'],
					'shippingAddress1' => $user['ShippingAddress']['address_1'],
					'shippingAddress2' => $user['ShippingAddress']['address_2'],
					'shippingAddress3' => $user['ShippingAddress']['address_3'],
					'shippingCity' => $user['ShippingAddress']['city'],
					'shippingCounty' => $user['ShippingAddress']['county'],
					'shippingCountryID' => $user['ShippingAddress']['country_id']
				]
			);
		}

		$result = $this->executePace('create_customer', $data);
die(debug($result));
		return $result;
*/

}
