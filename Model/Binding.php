<?php
App::uses('AppModel', 'Model');
/**
 * Binding Model
 *
 */
class Binding extends AppModel {

	/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'ProductBinding' => [
			'className' => 'ProductBinding',
			'foreignKey' => 'binding_id',
			'dependent' => false
		]
	];

	public function removeRedundantBindings($productCatalogue) {

		$doxBindingIds = $this->find('list');
		debug($doxBindingIds);
		$paceBindingIds = Hash::extract($productCatalogue, '{n}.id');

		debug($paceBindingIds);

		die(debug($productCatalogue));

	}
}
