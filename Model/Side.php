<?php
App::uses('AppModel', 'Model');
/**
 * Side Model
 *
 * @property Order $Order
 */
class Side extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $order = 'sides';
	public $actsAs = ['Defaultable'];

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'side_id',
			'dependent' => false
		],
		'ProductSide' => [
			'className' => 'ProductSide',
			'foreignKey' => 'side_id'
		]
	];

	public function convertFromPaceCode($sides = 1) {

		$sideId = $this->field(
			'id',
			['Side.sides' => $sides]
		);

		if (!$sideId) {
			$sideId = $this->field('id');
		}

		return $sideId;

	}

}
