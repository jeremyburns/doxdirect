<?php
App::uses('AppModel', 'Model');
/**
 * OrderStatus Model
 *
 * @property OrderDate $OrderDate
 * @property Order $Order
 */
class OrderStatus extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $order = 'sequence';
	public $actsAs = array('Defaultable');

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'OrderDate' => array(
			'className' => 'OrderDate',
			'foreignKey' => 'order_status_id',
			'dependent' => false
		),
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'order_status_id',
			'dependent' => false
		)
	);


	public function steps($active = null) {

		$orderStatuses = $this->find(
			'all',
			array(
				'conditions' => array(
					'show_tab' => true
				),
				'fields' => array(
					'OrderStatus.id',
					'OrderStatus.icon',
					'OrderStatus.name',
					'OrderStatus.description',
					'OrderStatus.link'
				)
			)
		);

		if (!$orderStatuses) {
			return array();
		}

		if (!$active) {
			$active = $orderStatuses[0]['OrderStatus']['id'];
		}

		foreach ($orderStatuses as $orderStatus) {
			$steps[$orderStatus['OrderStatus']['id']] = $orderStatus['OrderStatus'];
		}

		$allComplete = false;

		foreach ($steps as $stepName => $step) {
			if ($stepName == $active) {
				$steps[$stepName]['class'] = 'step active';
				$allComplete = true;
			} elseif (!$allComplete) {
				$steps[$stepName]['class'] = 'step complete';
			} else {
				$steps[$stepName]['class'] = 'step';
			}
		}

		return $steps;

	}

	public function showInCart($orderStatusId = '') {

		if (!$orderStatusId) {
			return false;
		}

		return $this->field(
			'show_cart',
			array(
				'OrderStatus.id' => $orderStatusId
			)
		);

	}

	public function adminOrderSummary() {

		$totalOrderCount = $this->find(
			'first',
			[
				'fields' => [
					'SUM(OrderStatus.count_order) as sum'
				]
			]
		);

		if ($totalOrderCount) {
			$totalOrderCount = intval($totalOrderCount[0]['sum']);
		} else {
			$totalOrderCount = 0;
		}

		$result = [];

		if ($totalOrderCount) {

			$orderStatuses = $this->find(
				'list',
				[
					'fields' => [
						'OrderStatus.name',
						'OrderStatus.count_order'
					],
					'order' => 'OrderStatus.sequence'
				]
			);

			if ($orderStatuses) {

				foreach ($orderStatuses as $orderStatusName => $orderCount) {
					$percent = (intval(round(($orderCount / $totalOrderCount) * 100)));
					$result[$orderStatusName] = [
						'percent' => $percent,
						'count' => $orderCount
					];
				}

			}

		}

		return $result;

	}

}
