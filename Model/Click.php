<?php
App::uses('AppModel', 'Model');
/**
 * Click Model
 *
 * @property Order $Order
 */
class Click extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $actsAs = ['Defaultable'];

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = [
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'click_id',
			'dependent' => false
		],
		'ProductClick' => [
			'className' => 'ProductClick',
			'foreignKey' => 'click_id',
		]
	];

/*
	public $paceNames = array(
		'col' => 'Colour',
		'bw' => 'BW'
	);

	public $quoteOptions = array(
		'col' => 'Colour',
		'bw' => 'Black & White'
	);
*/

/*
	public function convertToPaceCode($colour = 'col', $mediaId, $refresh = false) {

		$code = $colour . '.' . $mediaId;

		if ($refresh === true) {
			Cache::delete($code, 'pace_codes');
		}

		$model = $this;

		return Cache::remember(
			'Click.' . $code,
			function() use ($model, $colour, $mediaId){

				$paperSizeId = $this->OrderItem->Media->field('paper_size_id', array('Media.id' => $mediaId));

				$paceName = $paperSizeId . '_' . $this->paceNames[$colour];

				return $this->field(
					'id',
					array('pace_name' => $paceName)
				);
			},
			'pace_codes'
		);

	}
*/

}
