<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
App::uses('CakeTime', 'Utility');
App::uses('CakeEmail', 'Network/Email');

/**
 * User Model
 *
 * @property Order $Order
 * @property PromoCode $PromoCode
 * @property UserAddress $UserAddress
 */
class User extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'user_id',
			'dependent' => false
		),
		'PromoCode' => array(
			'className' => 'PromoCode',
			'foreignKey' => 'user_id',
			'dependent' => false
		),
		'UserAddress' => array(
			'className' => 'UserAddress',
			'foreignKey' => 'user_id',
			'dependent' => false
		),
		'ShippingAddress' => array(
			'className' => 'UserAddress',
			'foreignKey' => 'user_id',
			'dependent' => false
		),
		'BillingAddress' => array(
			'className' => 'UserAddress',
			'foreignKey' => 'user_id',
			'dependent' => false
		)
	);

	public $actsAs = ['Pace'];

/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'username' => array(
			'notBlank' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter your email address as your username.',
				'required' => 'create',
				'last' => true
			),
			'minLength' => array(
				'rule' => array('minLength', 6),
				'message' => 'Username must be between 6 and 50 characters.',
				'last' => true
			),
			'maxLength' => array(
				'rule' => array('maxLength', 50),
				'message' => 'Username must be between 6 and 50 characters.',
				'last' => true
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'That username has already been taken. Please choose another one.',
				'last' => true
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'That does not appear to be a valid email address.',
				'last' => true
			)
		),
		'password' => array(
			'notBlank' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter a password.',
				'last' => true
			),
			'minLength' => array(
				'rule' => array('minLength', 6),
				'message' => 'Your password must be between 6 and 20 characters.',
				'last' => true
			),
			'maxLength' => array(
				'rule' => array('maxLength', 20),
				'message' => 'Your password must be between 6 and 20 characters.'
			)
		),
		'password_confirm' => array(
			'rule' => array('compareFields', 'password', 'password_confirm'),
			'message' => 'Your passwords don\'t match.'
		),
		'old_password' =>  array(
			'rule' => 'validateOldPassword',
			'message' => 'You have not entered your current password correctly.'
		),
		'first_name' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter your first name.',
				'required' => 'create',
				'last' => true
			),
			'maxLength' => array(
				'rule' => array('maxLength', 50),
				'message' => 'First name cannot be longer than 50 characters.'
			)
		),
		'last_name' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter your last name.',
				'required' => 'create',
				'last' => true
			),
			'maxLength' => array(
				'rule' => array('maxLength', 50),
				'message' => 'Last name cannot be longer than 50 characters.'
			)
		)
	);

	public $displayField = 'username';

	public function beforeSave($options = array()) {

		if (!empty($this->data[$this->alias]['password'])) {

// 			$passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
			$passwordHasher = new SimplePasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash(
				$this->data[$this->alias]['password']
			);

		}

		if (isset($this->data['User']['first_name']) || isset($this->data['User']['last_name'])) {

			if (isset($this->data['User']['first_name']) && isset($this->data['User']['last_name'])) {

				$this->data['User']['full_name'] = $this->data['User']['first_name'] . ' ' . $this->data['User']['last_name'];

			} else {

				if (isset($this->data['User']['id'])) {

					$user = $this->find(
						'first',
						array(
							'fields' => array(
								'User.first_name',
								'User.last_name'
							)
						)
					);

					$user = array_merge(
						$user,
						$this->data
					);

				}

			}

		}

		return true;

	}

/**
 * Generates a password
 *
 * @param int $length Password length
 * @return string
 */
	public function generateToken($tokenLength = 10, $includeSymbols = true) {

		$charPool = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

		if ($includeSymbols) {
			$charPool .= '!@£$%^&*()_+=-/.,><;:][{}|';
		}

		$pass = array();

		$length = strlen($charPool) - 1;

		for ($i = 0; $i < $tokenLength; $i++) {
			$n = rand(0, $length);
			$pass[] = $charPool[$n];
		}

		return implode($pass);

	}

/**
 * Clears down the invalid login attempt count after successful login
 *
 * @param string $userId of the user to clear
 * @return array - (boolean) login, (string) message)
 */
	public function afterLogin($userId = null) {

		if ($userId) {

			$this->id = $userId;

			$this->set(array(
				'failed_login_attempts' => 0,
				'last_login' => date('Y-m-d H:i:s')
			));

			$this->save();
		}

		return true;

	}

/**
	The password_reset_request function is called when a user asks to reset their password.
	Its purpsoe is to generate a token that is sent via email as part of a link. When the user
	clicks the link we know it has gone to the right place.
*/
	public function passwordResetRequest($username = null) {

		if (!$username) {
			return false;
		}

		$user = $this->findByUsername($username);

		if (!$user) {
			return false;
		}

		$passwordResetCode = $this->generateToken(20, false);

		$now = $this->getTimestamp();
		$passwordResetCodeExpires = $now->modify('+2 days')->format('Y-m-d H:m:s');

		$this->id = $user['User']['id'];
		$this->set(array(
			'password_reset_code' => $passwordResetCode,
			'password_reset_code_expires' => $passwordResetCodeExpires
		));

		$this->save();

		$Email = new CakeEmail('default');
		$Email->from(['jeremyb@doxdirect.com' => 'Doxdirect'])
			->sender('jeremyb@doxdirect.com', 'Doxdirect')
			->to($username)
			->subject('Password reset request')
			->template('password_reset_request', 'default')
			->emailFormat('both')
			->viewVars([
				'passwordResetCode' => $passwordResetCode
			])
			->send();
		;

		// TODO send link via email
		return $passwordResetCode;

	}

/**
	The passwordResetRequestCheck function is called when a user clicks a password reset request link. It checks that the right user's record is going to be updated..
*/
	public function passwordResetRequestCheck($passwordResetCode = null) {

		if (!$passwordResetCode) {
			return false;
		}

		$passwordResetCodeExpires = $this->getTimestamp(['format' => true]);

		$user = $this->find(
			'first',
			array(
				'conditions' => array(
					'User.password_reset_code' => $passwordResetCode,
					'User.password_reset_code_expires >=' => time()
				)
			)
		);

		if ($user) {
			return true;
		}

		return false;

	}

	public function resetPassword($data = array()) {

		if (!$data) {
			return [
				'success' => false,
				'code' => 'no-data'
			];
		}

		if (empty($data['User']['username']) || empty($data['User']['password_new']) || empty($data['User']['password_new_confirm'])) {

			return [
				'success' => false,
				'code' => 'missing-parameter'
			];
		}

		$user = $this->find(
			'first',
			array(
				'fields' => [
					'User.id'
				],
				'conditions' => array(
					'User.password_reset_code' => $data['User']['password_reset_code'],
					'User.password_reset_code_expires >=' => time(),
					'User.username' => $data['User']['username']
				)
			)
		);

		if (!$user) {
			return [
				'success' => false,
				'code' => 'user-not-found'
			];
		}

		$userId = $user['User']['id'];

		$data['User'] = array(
			'id' => $userId,
			'password_new' => $data['User']['password_new'],
			'password_new_confirm' => $data['User']['password_new_confirm']
		);

		$response = $this->passwordChange($data);

		if ($response['success']) {

			// Clear the reset code and expiry date
			$this->id = $userId;
			$this->set(array(
				'password_reset_code' => null,
				'password_reset_code_expires' => null
			));
			$this->save();

			$user = $this->findById($userId);

			return [
				'success' => true,
				'message' => 'Your password has been changed. Please use your new password next time you log in.',
				'user' => $user
			];

		}

		return [
			'success' => false,
			'code' => $response['code'],
			'message' => 'Your password was not changed.',
			'errors' => $response['errors']
		];

	}

	public function passwordChange($passwordData = array()) {


		if (empty($passwordData['User']['id'])) {

			return [
				'success' => false,
				'code' => 'missing-user-id'
			];

		}

		if (empty($passwordData['User']['password_new'])) {

			return [
				'success' => false,
				'code' => 'missing-password-new'
			];

		}

		if (empty($passwordData['User']['password_new_confirm'])) {

			return [
				'success' => false,
				'code' => 'missing-password-new-confirm'
			];

		}

		$userId = $passwordData['User']['id'];
		$newPassword = $passwordData['User']['password_new'];
		$newPasswordConfirm = $passwordData['User']['password_new_confirm'];

		$this->id = $userId;

		if (!$this->exists()) {

			return [
				'success' => false,
				'code' => 'bad-user-id'
			];

		}

		if ($newPassword !== $newPasswordConfirm) {

			return [
				'success' => false,
				'code' => 'passwords-unmatched',
				'message' => 'Your passwords do not match.'
			];

		}

		$this->set([
			'password' => $newPassword,
			'user_supplied_password' => true
		]);

		if ($this->save()) {

			$user = $this->findById($userId);

			return [
				'success' => true,
				'user' => $user,
				'message' => 'Your password has been changed. Please use your new password next time you log in.'
			];

		}

		return [
			'success' => false,
			'code' => 'save-failed',
			'errors' => $this->validationErrors
		];

	}

/**
	This function is called as part of the order checkout function.
	It checks to see if the user exists.
	If not, it validates and creates it.
	If so, it fails and the orders controller should ask the user to log in.
*/
	public function register($user = array()) {

		if (empty($user['User'])) {

			return [
				'success' => false,
				'code' => 'no-user-data'
			];

		}

		$existingUser = $this->findByUsername($user['User']['username']);

		if ($existingUser) {

			$result = array_merge(
				[
					'success' => true,
					'code' => 'existing-user'
				],
				$existingUser
			);

			return $result;

		}

		if (empty($user['User']['password'])) {
			$user['User']['password'] = $this->generateToken();
			$user['User']['user_supplied_password'] = false;
		} else {
			$user['User']['user_supplied_password'] = true;
		}

		if (empty($user['User']['id'])) {
			unset($user['User']['id']);
		}

		$this->create();
		$this->set($user);

		$registeredUser = $this->save();

		if (!$registeredUser) {

			return [
				'success' => false,
				'code' => 'user-create-failed',
				'errors' => $this->validationErrors
			];

		}

		$result = array_merge(
			['success' => true],
			$registeredUser
		);

		return $result;

	}

/**
	This function accepts an orderId and extracts the user and addresses associated with it. It then submits the user and addresses to pace, saving the Pace ids.
*/

	public function createInPace($orderId) {

		$order = $this->Order->find(
			'first',
			[
				'conditions' => ['Order.id' => $orderId],
				'fields' => [
					'Order.id',
					'Order.shipping_address_id',
					'Order.billing_address_id',
					'Order.billing_same_as_shipping'
				],
				'contain' => [
					'User' => [
						'fields' => [
							'User.id',
							'User.pace_customer_id',
							'User.username',
							'User.first_name',
							'User.last_name'
						]
					],
					'ShippingAddress' => [
						'fields' => [
							'ShippingAddress.id',
							'ShippingAddress.pace_contact_id',
							'ShippingAddress.company_name',
							'ShippingAddress.address_1',
							'ShippingAddress.address_2',
							'ShippingAddress.address_3',
							'ShippingAddress.city',
							'ShippingAddress.county',
							'ShippingAddress.post_code',
							'ShippingAddress.country_id'
						]
					],
					'BillingAddress' => [
						'fields' => [
							'BillingAddress.id',
							'BillingAddress.pace_contact_id',
							'BillingAddress.company_name',
							'BillingAddress.address_1',
							'BillingAddress.address_2',
							'BillingAddress.address_3',
							'BillingAddress.city',
							'BillingAddress.county',
							'BillingAddress.post_code',
							'BillingAddress.country_id'
						]
					]
				]
			]
		);

		if (
			empty($order['User']['pace_customer_id'])
			|| empty($order['ShippingAddress']['pace_contact_id'])
			|| empty($order['BillingAddress']['pace_contact_id'])
		) {

			// Pace assumes the prescence of a billing address first and assumes that's also the shipping addres. That's the other way around from this system, which assumes users will ship to an address that is most likely their billing address as well. So we need to flick stuff around a bit here.

			if ($order['Order']['billing_same_as_shipping']) {

				unset($order['BillingAddress']);

				$billingAddress = $order['ShippingAddress'];

			} else {

				if (empty($order['Order']['billing_address_id'])) {
					// Something's wrong - the shipping and billing addresses are different but we don't have a billing address
					return [
						'success' => false,
						'code' => 'no-billing-address'
					];
				}

				$shippingAddress = $order['ShippingAddress'];
				$billingAddress = $order['BillingAddress'];

			}

			$billingAddress['billing_same_as_shipping'] = $order['Order']['billing_same_as_shipping'];

			$user['User'] = $order['User'];
			$user['BillingAddress'] = $billingAddress;
			if (!empty($shippingAddress)) {
				$user['ShippingAddress'] = $shippingAddress;
			}

			$result = $this->createCustomer($user);

			if ($result['success'] && !empty($result['response']['customer'])) {

				$customer = $result['response']['customer'];

				$customerId = !empty($customer['customer_id'])
					? $customer['customer_id']
					: null
				;
				$billingContactId = !empty($customer['billing_contact_id'])
					? $customer['billing_contact_id']
					: null
				;
				$shippingContactId = !empty($customer['shipping_contact_id'])
					? $customer['shipping_contact_id']
					: null
				;

				if ($customerId) {
					$this->setPaceCustomerId($user['User']['id'], $customerId);
				}

				if ($billingContactId && !empty($user['BillingAddress'])) {
					$this->UserAddress->setPaceContactId($user['BillingAddress']['id'], $billingContactId);
				}

				if ($shippingContactId && !empty($user['ShippingAddress'])) {
					$this->UserAddress->setPaceContactId($user['ShippingAddress']['id'], $shippingContactId);
				}
			}

		}

		return ['success' => true];

	}

	private function setPaceCustomerId($userId = null, $customerId = null) {

		$existingCustomerId = $this->field(
			'pace_customer_id',
			['User.id' => $userId]
		);

		if (!$existingCustomerId) {

			$data = [
				'id' => $userId,
				'pace_customer_id' => $customerId
			];

			$this->save(
				$data,
				['callbacks' => false]
			);

		}

		return true;

	}

}
