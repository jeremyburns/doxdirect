<?php
App::uses('AppModel', 'Model');
/**
 * ProductSide Model
 *
 * @property Product $Product
 * @property Side $Side
 */
class ProductSide extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'Side' => array(
			'className' => 'Side',
			'foreignKey' => 'side_id',
			'dependent' => false
		)
	);

}