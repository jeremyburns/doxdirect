<?php
App::uses('AppModel', 'Model');
/**
 * BindingType Model
 *
 */
class BindingType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'name';
    public $actsAs = array('Defaultable');
    public $order = 'name';

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Quote' => array(
            'className' => 'Quote',
            'foreignKey' => 'binding_type_id',
            'dependent' => false
        ),
        'OrderItem' => array(
            'className' => 'OrderItem',
            'foreignKey' => 'binding_type_id',
            'dependent' => false
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'You must specify a name for this binding type.',
                'allowEmpty' => false,
                'last' => true
            ),
            'noEmptyString' => array(
                'rule' => array('validationNoEmptyString', 'name'),
                'message' => 'No empty string',
                'last' => true
            ),
            'maxLength' => array(
                'rule' => array('maxLength', 50),
                'message' => 'Binding type name cannot be longer than 50 characters.',
                'last' => true
            ),
            'minLength' => array(
                'rule' => array('minLength', 1),
                'message' => 'Binding type name must have at least 1 character.',
                'last' => true
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'This binding type name already exists.'
            )
        )
    );

    public function beforeSave($options = array()) {

        // We can't allow NULL values through from the product catalogue, so unset them before saving
        foreach ($this->data['BindingType'] as $fieldName => $value) {

            if ($value === NULL) {

                unset($this->data['BindingType'][$fieldName]);

            }

        }

        return parent::beforeSave();

    }

    public function imageName($bindingTypeId = null, $width = 50, $extension = 'png') {

        if (!$bindingTypeId) {
            return '';
        }

        $imageName = $this->field(
            'image_name',
            array(
                'BindingType.id' => $bindingTypeId
            )
        );

        if ($imageName) {
            $imageName .= '-' . $width . '.' . $extension;
        }

        return $imageName;

    }

}
