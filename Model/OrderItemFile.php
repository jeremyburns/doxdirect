<?php

App::uses('AppModel', 'Model');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('SessionComponent', 'Controller/Component');

/**
 * OrderItemFile Model
 *
 * @property OrderItem $OrderItem
 */
class OrderItemFile extends AppModel {

	public $actsAs = [
		'Callas',
		'Configurable'
	];
	public $order = 'OrderItemFile.sequence, OrderItemFile.created';

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = [
		'OrderItem' => [
			'className' => 'OrderItem',
			'foreignKey' => 'order_item_id',
			'dependent' => true,
			'counterCache' => 'count_order_item_file'
		],
		'PaperSize' => [
			'className' => 'PaperSize',
			'foreignKey' => 'paper_size_id',
			'dependent' => true,
			'counterCache' => 'count_order_item_file'
		]
	];

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = [
		'OrderItemFilePage' => [
			'className' => 'OrderItemFilePage',
			'foreignKey' => 'order_item_file_id',
			'dependent' => true
		],
		'OrderItemFileProcess' => [
			'className' => 'OrderItemFileProcess',
			'foreignKey' => 'order_item_file_id',
			'dependent' => true
		]
	];

	public function beforeSave($options = []) {

		// The sequence field holds the position of this file within a document (OrderItem) - only gets set on insert
		if (empty($this->data['OrderItemFile']['id']) && !empty($this->data['OrderItemFile']['order_item_id'])) {

			$maxSequence = $this->find(
				'first',
				[
					'conditions' => [
						'OrderItemFile.order_item_id' => $this->data['OrderItemFile']['order_item_id']
					],
					'fields' => ['OrderItemFile.sequence'],
					'order' => ['OrderItemFile.sequence' => 'desc']
				]
			);

			if (!isset($maxSequence['OrderItemFile']['sequence'])) {
				$sequence = 1;
			} else {
				$sequence = $maxSequence['OrderItemFile']['sequence'] + 1;
			}

			$this->data['OrderItemFile']['sequence'] = $sequence;
		}

		return parent::beforeSave();
	}

	public function beforeDelete($cascade = true) {

		$this->orderItemId = $this->field('order_item_id');

		return true;
	}

//	public function afterDelete() {
//
//		$this->resync();
//
//	}

	public function resync($orderItemId = null) {

		if (!$orderItemId) {

			if ($this->orderItemId) {

				$orderItemId = $this->orderItemId;

			}

		}

		if ($orderItemId) {
			$this->resequence();
			$result = $this->OrderItem->collatePages($orderItemId);

			return [
				'success' => true,
				'code' => 'resynced',
				'order-item-id' => $orderItemId,
				'result' => $result
			];
		}

		return [
			'success' => false,
			'code' => 'no-resync'
		];

	}

	public function orderId($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return null;
		}

		$orderItemFile = $this->find(
			'first',
			[
				'conditions' => ['OrderItemFile.id' => $orderItemFileId],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.order_item_id'
				],
				'contain' => [
					'OrderItem' => [
						'fields' => [
							'OrderItem.id',
							'OrderItem.order_id'
						]
					]
				]
			]
		);

		if (empty($orderItemFile['OrderItem']['order_id'])) {
			return null;
		}

		return $orderItemFile['OrderItem']['order_id'];
	}

	public function getOrderItemFile($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return false;
		}

		$orderItemFile = $this->find(
			'first',
			[
				'conditions' => ['OrderItemFile.id' => $orderItemFileId],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.name',
					'OrderItemFile.path_root',
					'OrderItemFile.order_item_id',
					'OrderItemFile.sent_to_callas',
					'OrderItemFile.callas_info',
					'OrderItemFile.callas_filename',
					'OrderItemFile.file_name_without_extension',
					'OrderItemFile.has_trim_marks'
				],
				'contain' => [
					'OrderItemFilePage' => [
						'fields' => [
							'OrderItemFilePage.id',
							'OrderItemFilePage.order_item_file_id'
						]
					],
					'OrderItem' => [
						'fields' => [
							'OrderItem.id',
							'OrderItem.order_id',
							'OrderItem.product_id',
							'OrderItem.pace_quote_product_id'
						],
						'Product' => [
							'fields' => [
								'Product.id',
								'Product.paper_size_id'
							]
						],
						'Order' => [
							'fields' => [
								'Order.id',
								'Order.pace_quote_id'
							]
						]
					]
				]
			]
		);

		return $orderItemFile;
	}

	public function orderItemFilePageIds($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return array();
		}

		return $this->OrderItemFilePage->find(
			'list', [
				'conditions' => ['OrderItemFilePage.order_item_file_id' => $orderItemFileId],
				'fields' => [
					'OrderItemFilePage.id',
					'OrderItemFilePage.id'
				]
			]
		);
	}

	public function getFileTypeIcon($fileType = null) {

		switch ($fileType) {
			case 'application/pdf':
				$extension = 'pdf';
				$icon = 'fa fa-file-pdf-o';
				$title = 'Adobe PDF';
				break;
			case 'application/vnd.ms-powerpoint';
			case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
				$extension = 'pptx';
				$icon = 'fa fa-file-powerpoint-o';
				$title = 'MS PowerPoint';
				break;
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
			case 'application/msword':
				$extension = 'docx';
				$icon = 'fa fa-file-word-o';
				$title = 'MS Word';
				break;
			default:
				$extension = '-';
				$icon = 'fa fa-file-text-o';
				$title = 'Unknown';
		}

		return [
			'extension' => $extension,
			'file_type_icon' => $icon,
			'file_type_title' => $title
		];
	}

	/**
	 * The uploadFiles function uploads files and adds then to the order item files stack
	 * @param array $files the array containing the files to be uploaded
	 * @param string $orderId the current session id
	 * @param array $orderItemFiles the possibly empty array of files already uploaded
	 *
	 * @return array an array with order item files and success/fail status and message
	 */
	public function uploadFiles($order = [], $orderItemId = null, $files = []) {

		if (empty($files['files']['name'])) {

			return [
				'success' => false,
				'message' => 'There were no new files to upload.',
				'order' => $order
			];
		}

		if (empty($order['Order']['id'])) {
			return [
				'success' => false,
				'code' => 'no-order-id',
				'message' => 'Your session has expired. Please re-enter your quote details.'
			];
		}

		$this->maximumUploadFileSize = $this->getConfigurationValue('maximum_upload_file_size');

		// Get the orderId
		$orderId = $order['Order']['id'];

		// Get the Pace quote id - this is the order level
		$paceQuoteId = $order['Order']['pace_quote_id'];

		// Get the Pace quote product id - this is the order item level
		$paceQuoteProductId = $order['OrderItem'][0]['pace_quote_product_id'];

		// The files array has named keys, and then numeric keys under each for each file
		// It's easier to pull them out into a numeric key for each file with named keys under, so we'll swap them
		// Extract the various file keys (name, type, size etc)
		$fileKeys = array_keys($files['files']);

		// Start an empty array to hold the successfully uploaded order item file ids
		$orderItemFileIds = [];

		// How many files are being uploaded?
		$fileCount = count($files['files']['name']);

		// loop and process files
		for ($fileId = 0; $fileId < $fileCount; $fileId++) {

			$result = $this->uploadFile(
				$fileKeys,
				$files,
				$fileId,
				$orderId,
				$orderItemId,
				$paceQuoteId,
				$paceQuoteProductId
			);

			if (!$result['success']) {
				return $result;
			}

			$orderItemFileId = $result['order_item_file_id'];

			$orderItemFileIds[] = $orderItemFileId;

			$result = $this->uploadToCallas($orderItemFileId);

			$result = $this->processCallas($orderItemFileId);

		}

		$response = [
			'success' => true,
			'message' => 'Your files have been uploaded.',
			'orderItemFileIds' => $orderItemFileIds
		];

		return $response;

	}


	private function uploadFile($fileKeys, $files, $fileId, $orderId, $orderItemId, $paceQuoteId, $paceQuoteProductId) {

		// Populate the response with the file details
		foreach ($fileKeys as $fileKey) {

			$orderItemFile['OrderItemFile'][$fileKey] = str_replace(' ', '_', $files['files'][$fileKey][$fileId]);

		}

		if ($orderItemFile['OrderItemFile']['error'] == 2 || $orderItemFile['OrderItemFile']['size'] > $this->maximumUploadFileSize) {

			$maximumFileSize = CakeNumber::toReadableSize($this->maximumUploadFileSize);
			return [
				'success' => false,
				'code' => 'file-exceeds-maximum-upload-size',
				'message' => 'This file exceeds the maximunm file upload size of ' . $maximumFileSize  . '.',
				'file_name' => $orderItemFile['OrderItemFile']['name']
			];
		}

		$fileIcon = $this->getFileTypeIcon($orderItemFile['OrderItemFile']['type']);

		if ($fileIcon['extension'] == '-') {
			// This is not an accepted file type
			return [
				'success' => false,
				'code' => 'file-type-not-accepted',
				'message' => $orderItemFile['OrderItemFile']['name'] . ' is not an accepted file format.',
				'file_name' => $orderItemFile['OrderItemFile']['name']
			];
		}

		// Add details of the file type and icon
		$orderItemFile['OrderItemFile'] = array_merge(
			$orderItemFile['OrderItemFile'],
			$fileIcon
		);

		// Set the relational keys
		// Store a readable format of the file size
		$readableSize = CakeNumber::toReadableSize($orderItemFile['OrderItemFile']['size']);

		// Convert the spaces in the file name to underscores
		$fileName = str_replace(' ', '_', $orderItemFile['OrderItemFile']['name']);

		// Get the filename without its extension
		$fileNameWithOutExtension = $this->stripExtension($fileName);

		// Get the folders ready for uploading and storing
		$folderPaths = $this->createFolders($paceQuoteId, $paceQuoteProductId, $fileNameWithOutExtension);

		// Set some field values based on file name and folder paths
		$orderItemFile['OrderItemFile'] = array_merge(
			$orderItemFile['OrderItemFile'],
			[
				'order_id' => $orderId,
				'order_item_id' => $orderItemId,
				'file_name_without_extension' => $fileNameWithOutExtension,
				'path_root' => $folderPaths['OrderItemFile'],
				'readable_size' => $readableSize
			]
		);

		try {

			// Move the temp file into it's target
			move_uploaded_file(
				$files['files']['tmp_name'][$fileId],
				$folderPaths['OrderItemFileOriginal'] . DS . $fileName
			);

		} catch (Excpetion $e) {

			$orderItemFile['OrderItemFile']['result'] = 'fail';
			$orderItemFile['OrderItemFile']['message'] = 'There was a problem uploading this file: ' . $e->getMessage();
			$error = true;

			return [
				'success' => false,
				'code' => 'file-move-failed',
				'message' => 'There was a problem uploading this file.',
				'file_name' => $orderItemFile['OrderItemFile']['name']
			];

		}

		// Create a new record if necessary
		if (empty($orderItemFile['OrderItemFile']['id'])) {
			$this->create();
		}

		$this->set($orderItemFile);

		$result = $this->save();

		if ($result) {
			return [
				'success' => true,
				'order_item_file_id' => $this->id
			];
		}

		return [
			'success' => false,
			'code' => 'unknown-error',
			'message' => 'An unknown error occurred during the upload.',
			'file_name' => $orderItemFile['OrderItemFile']['name']
		];

	}

	private function createFolders($paceQuoteId, $paceQuoteProductId, $fileNameWithOutExtension) {

		// Create an array to store the various paths
		$folderPaths = [];

		// Each order has a folder that contains its files
		$folderPaths['Order'] = $this->localFilesPath() . $paceQuoteId;

		// Each item in the order has a folder too
		$folderPaths['OrderItem'] = $folderPaths['Order'] . DS . $paceQuoteProductId;

		// Each file in an order item has its own folder
		$orderItemFilePath = $folderPaths['OrderItem'] . DS . $fileNameWithOutExtension;

		// Get the containing folder for this file
		$folderPaths['OrderItemFile'] = $orderItemFilePath;

		// Create the oredr item file subfolders
		$folderPaths['OrderItemFileOriginal'] = $orderItemFilePath . DS . 'original';
		$folderPaths['OrderItemFilePreview'] = $orderItemFilePath . DS . 'preview';
		$folderPaths['OrderItemFileFinal'] = $orderItemFilePath . DS . 'final';
		$folderPaths['OrderItemFileCoverDesigner'] = $orderItemFilePath . DS . 'cover_designer';

		foreach ($folderPaths as $folderPath) {
			$folderExists = $this->folderExists($folderPath, true, 0777);
		}

		// When using the api we need a path relative to webroot for previews
		// rather than the full path for creating them

		if ($this->callasMethod() === 'api') {

			$folderPaths['OrderItemFilePreview'] = $this->localFilesPath(false) . $paceQuoteId . DS . $paceQuoteProductId . DS . $fileNameWithOutExtension . DS . 'preview';

		}

		return $folderPaths;

	}

	public function uploadFilesToCallas($orderItemFileIds = []) {

		if (!$orderItemFileIds) {
			return [
				'success' => false,
				'code' => 'no-order-item-file-ids',
				'from' => 'OrderItemFile.uploadToCallas'
			];
		}

		foreach ($orderItemFileIds as $orderItemFileId) {

			$this->uploadToCallas($orderItemFileId);

			$this->processCallas($orderItemFileId);

		}

		return [
			'success' => true
		];

	}

	public function uploadToCallas($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return [
				'success' => false,
				'code' => 'no-order-item-file-id',
				'from' => 'OrderItemFile.uploadToCallas'
			];
		}

		$orderItemFile = $this->getOrderItemFile($orderItemFileId);

		if (!$orderItemFile) {

			return [
				'success' => false,
				'code' => 'invalid-order-item-file',
				'message' => 'That is not a valid file.'
			];

		}

		$response = $this->sendToCallas(
			$orderItemFile,
			$orderItemFile['OrderItem']['order_id']
		);

		if (!empty($response['preview_page_ids'])) {
			$this->OrderItemFilePage->updateHasPreview($response['preview_page_ids']);
		}

		$orderItemFile['OrderItemFile'] = array_merge(
			$orderItemFile['OrderItemFile'],
			$response['data']
		);

		$result = $this->save($orderItemFile);

		return $result;

	}

	public function setCallasFilename($orderItemFileId = null) {

		$orderItemFile = $this->find(
			'first',
			[
				'conditions' => [
					'OrderItemFile.id' => $orderItemFileId
				],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.name'
				],
				'contain' => [
					'OrderItem' => [
						'fields' => [
							'OrderItem.id',
							'OrderItem.order_id'
						]
					]
				]
			]
		);

		if (!$orderItemFile) {

			return [
				'success' => false,
				'code' => 'invalid-order-item-file'
			];

		}

		$callasFileName = $this->makeCallasFileName(
			$orderItemFile['OrderItem']['order_id'],
			$orderItemFile['OrderItemFile']['name']
		);

		$callasFileNameWithoutExtension = $this->getFileNameWithoutExtension($callasFileName);

		$data = [
			'callas_filename' => $callasFileName,
			'file_name_without_extension' => $callasFileNameWithoutExtension
		];

		$this->id = $orderItemFileId;

		$result = $this->save(
			$data,
			[
				'validate' => false,
				'callbacks' => false
			]
		);

		return [
			'success' => true,
			'callas_filename' => $callasFileName,
			'file_name_without_extension' => $callasFileNameWithoutExtension
		];

	}

	public function processCallas($orderItemFileId = null) {

		if (!$orderItemFileId) {

			// We need the order item file id
			return [
				'success' => false,
				'code' => 'no-order-item-file-id',
				'from' => 'OrderItemFile.processCallas'
			];
		}

		// Get the order item file
		$orderItemFile = $this->getOrderItemFile($orderItemFileId);

		if (!$orderItemFile) {

			// It doesn't exist
			return [
				'success' => false,
				'code' => 'no-order-item-file',
				'from' => 'OrderItemFile.processCallas'
			];
		}

		$orderItemId = $orderItemFile['OrderItemFile']['order_item_id'];

		$orderItemFolder = $orderItemFile['OrderItemFile']['path_root'];

		$response = $this->getCallasXml($orderItemFile);

		if (!$response['success']) {
			// Generally someting wrong with Callas
			return [
				'success' => false,
				'code' => 'no-callas-response',
				'from' => 'OrderItemFile.processCallas'
			];
		}

		$callasXml = $response['xml'];

		if ($response['source'] == 'callas') {

			$this->saveCallasInfoFile($orderItemFileId, $callasXml);

			$orderItemFile = $this->getOrderItemFile($orderItemFileId);

			// Now process the pages
			$result = $this->extractPages($orderItemFileId, $callasXml);

			if (!$result['success']) {
				return $result;
			}

			$result = $this->OrderItem->collatePages($orderItemId);

			$makePreviews = $this->makePreviews($orderItemFile, $callasXml);

			if (!empty($makePreviews['preview_page_ids'])) {
				$response['preview_page_ids'] = $makePreviews['preview_page_ids'];
			}

		}

		return $response;

	}

	/**
	 *
	 * @param type $orderItemFileId
	 * @return array
	 *
	 * This function loops through all pages in a file. It's purpose is to create the db rows for each page.
	 * It clears all existing rows before starting.
	 */
	public function extractPages($orderItemFileId = null, $callasInfoFile = null) {

		if (!$orderItemFileId) {

			// No id submitted
			return [
				'success' => false,
				'code' => 'no-order-item-file-id',
				'from' => 'OrderItemFile->extractPages'
			];
		}

		// Get the order item file
		$orderItemFile = $this->getOrderItemFile($orderItemFileId);

		if (!$callasInfoFile) {

			// Get the Callas info from the order item file
			$callasInfoFile = $orderItemFile['OrderItemFile']['callas_info'];

			// Decode the info
			$callasInfoFile = json_decode($callasInfoFile, true);

		}

		if (!$callasInfoFile) {

			// We need the Callas info to continue
			return [
				'success' => false,
				'code' => 'no-callas-info-file',
				'from' => 'OrderItemFile->extractPages'
			];
		}

		// Get the page key
		$pages = $callasInfoFile['report']['document']['pages']['page'];

		if (!$pages) {

			// No pages
			return [
				'success' => false,
				'code' => 'no-pages',
				'from' => 'OrderItemFile->extractPages'
			];
		}

		// The pages key is either an array (for multiple page docs) or a single key (for single page docs)
		if (!isset($pages[0])) {
			// This is a single page document
			// Get the single page
			$page = $pages;
			// Get rid of the original $pages array
			unset($pages);
			// Recreate it with the single page as the 0 key
			$pages[0] = $page;
			// The get rid of the temporary variable
			unset($page);
		}

		// Get the order id
		$orderId = $orderItemFile['OrderItem']['order_id'];

		// And the order item id
		$orderItemId = $orderItemFile['OrderItem']['id'];

		// Get the paper size of the chosen product so we can see how well the pages do or don't fit
		$orderItemPaperSizeId = $orderItemFile['OrderItem']['Product']['paper_size_id'];

		// Are we doing hot folders (server) or cli?
		$fieldName = $this->callasMethod() == 'server'
			? 'callas_filename'
			: 'name';
		;

		// Get the name of the actual file
		$fileName = $this->field(
			$fieldName,
			['OrderItemFile.id' => $orderItemFileId]
		);

		// Take off the extension
		$fileNameWithOutExtension = $this->stripExtension($fileName);

		// Remove any existing page records as we are going to do them clean
		$this->OrderItemFilePage->deleteAll(
			['OrderItemFilePage.order_item_file_id' => $orderItemFileId]
		);

		$pathRoot = $this->field(
			'path_root',
			['OrderItemFile.id' => $orderItemFileId]
		);

		if ($this->callasMethod() == 'api') {

			$paceQuoteId = $orderItemFile['OrderItem']['Order']['pace_quote_id'];
			$paceQuoteProductId = $orderItemFile['OrderItem']['pace_quote_product_id'];
			$previewPath = $this->localFilesPath(false) . $paceQuoteId . DS . $paceQuoteProductId . DS . $fileNameWithOutExtension . DS . 'preview';

		} else {

			$previewPath = $pathRoot . DS . 'preview';

		}

		$firstPageDimensions = [];

		// Loop through the pages
		foreach ($pages as $pageId => $page) {

			// Process the page
			$response = $this->OrderItemFilePage->extractPage(
				$page,
				$fileNameWithOutExtension,
				$orderItemFileId,
				$orderId,
				$orderItemPaperSizeId,
				$previewPath
			);

			// We need to capture the dimensions of the first page
			// This will only get set once
			if (empty($firstPageDimensions)) {

				$firstPageDimensions = [
					'first_page_width' => $response['OrderItemFilePage']['width'],
					'first_page_height' => $response['OrderItemFilePage']['height']
				];

				$this->id = $orderItemFileId;

				$save = $this->save(
					$firstPageDimensions,
					[
						'callbacks' => false,
						'validation' => false
					]
				);

			}

		}

		$result = $this->analysePages($orderItemFileId);

		// Return success
		return ['success' => true];

	}

	public function analysePages($orderItemFileId = null) {

		$orderItemFile = $this->find(
			'first',
			[
				'conditions' => ['OrderItemFile.id' => $orderItemFileId],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.order_item_id',
					'OrderItemFile.has_trim_marks',
				]
			]
		);

		if (!$orderItemFile) {
			return [
				'success' => false,
				'code' => 'invalid-order-item-file'
			];
		}

		// Set the page consistency across the pages
		$response = $this->setOrderItemFilePageConsistency($orderItemFileId);

		// Work out if any pages in the file have trim marks
		$this->setHasTrimMarks($orderItemFileId, $orderItemFile['OrderItemFile']['has_trim_marks']);

		// Round the page info up to the order item (includes all pages from all files)
		// The result of collatePages will be true or false
		// If true, something has changed that has triggered a requote
		$requoted = $this->OrderItem->collatePages($orderItemFile['OrderItemFile']['order_item_id']);

		return [
			'success' => true,
			'requoted' => $requoted
		];

	}

	/**
	 * This function examines all pages within a file and sets some fields
	 * in the OrderItemFile record to identify any fixes needed to the pages.
	 */
	public function setOrderItemFilePageConsistency($orderItemFileId = null) {

		if (!$orderItemFileId) {
			// We need an id
			return [
				'success' => false,
				'code' => 'invalid-order-item-file-id',
				'from' => 'OrderItemFile.setOrderItemFilePageConsistency'
			];
		}

		// Get the id of the order item it belongs to
		$orderItemId = $this->field(
			'order_item_id',
			['OrderItemFile.id' => $orderItemFileId]
		);

		if (!$orderItemId) {
			// It doesn't exist
			return [
				'success' => false,
				'code' => 'invalid-order-item-id',
				'from' => 'OrderItemFile.setOrderItemFilePageConsistency'
			];
		}

		// Set the orientation consistency
		$result = $this->setOrientationConsistency($orderItemFileId);

		// Set the page size consistency
		$paperSizeConsistency = $this->setPaperSizeConsistency($orderItemFileId);

		// Set whether the page sizes match standard sizes
		$this->setPaperSizeMatchConsistency($orderItemFileId);

		// Check whether the actual page sizes match the chosen paper size (or do they have to scale)
		$result = $this->setPaperSizeScaleConsistency($orderItemFileId);

		return $paperSizeConsistency;

	}

	public function saveCallasInfoFile($orderItemFileId = null, $callasInfoFile = []) {

		if (!$callasInfoFile) {

			return [
				'success' => false,
				'code' => 'missing-callas-info-file',
				'from' => 'OrderItemFile.saveCallasInfoFile'
			];
		}

		if (!$orderItemFileId) {

			return [
				'success' => false,
				'code' => 'missing-order-item-file-id',
				'from' => 'OrderItemFile.saveCallasInfoFile'
			];
		}

		if (!isset($callasInfoFile['report']['document']['doc_info']['filesize_byte'])) {

			return [
				'success' => false,
				'code' => 'missing-byte-size',
				'from' => 'OrderItemFile.saveCallasInfoFile'
			];
		}

		$filesizeByte = $callasInfoFile['report']['document']['doc_info']['filesize_byte'];

		$this->id = $orderItemFileId;

		if (!$this->exists()) {

			return [
				'success' => false,
				'code' => 'invalid-order-item-file-id',
				'from' => 'OrderItemFile.saveCallasInfoFile'
			];
		}

		$data = [
			'callas_info' => json_encode($callasInfoFile),
			'filesize_byte' => $filesizeByte
		];

		$result = $this->save(
			$data,
			[
				'callbacks' => false,
				'validate' => false
			]
		);

		if (!$result) {

			return [
				'success' => false,
				'code' => 'save-failed',
				'errors' => $this->validationErrors,
				'from' => 'OrderItemFile.saveCallasInfoFile'
			];
		}

		return ['success' => true];
	}

	/**
	 * This function gets the orientation consistency of the pages in the file
	 * and saves them to the OrderItemFile record
	 */
	public function setOrientationConsistency($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return false;
		}

		$orientation = $this->orientation($orderItemFileId);

		$data = [
			'id' => $orderItemFileId,
			 'orientation' => $orientation['orientation'],
			 'orientation_consistency' => $orientation['consistency'],
			 'orientation_count_p' => !empty($orientation['pageOrientations']['p'])
				? $orientation['pageOrientations']['p']
				: 0
			 ,
			 'orientation_count_l' => !empty($orientation['pageOrientations']['l'])
				? $orientation['pageOrientations']['l']
				: 0
		];

		$this->set($data);
		$result = $this->save();

		$orderItemId = $this->field(
			'order_item_id',
			['OrderItemFile.id' => $orderItemFileId]
		);

		if ($orderItemId) {
			$this->OrderItem->setOrientationConsistency($orderItemId);
		}

		return $result;

	}

	public function firstPageDimensions($orderItemFileId = null) {

		if (!$orderItemFileId) {

			return [
				'success' => false,
				'code' => 'invalid-order-item-file-id'
			];

		}

		$dimensions = $this->OrderItemFilePage->find(
			'first',
			[
				'conditions' => [
					'OrderItemFilePage.order_item_file_id' => $orderItemFileId
				],
				'fields' => [
					'OrderItemFilePage.width',
					'OrderItemFilePage.height'
				]
			]
		);

		if (!$dimensions) {

			return [
				'success' => false,
				'code' => 'no-dimensions'
			];

		}

		return [
			'success' => true,
			'dimensions' => $dimensions
		];

	}

	/**
	 * This function examines the orientation of the pages in a file.
	 * It determines how pure they are. If all pages have the
	 * same orientation, the consistency is 'single'. Otherwise, it's 'mixed'.
	 */
	public function orientation($orderItemFileId = null) {

		// Start with some defaults
		$defaultOrientation = 'p';

		// Make sure we've got something to look at
		if (!$orderItemFileId) {

			// Else fail and return the default orientation
			return [
				'success' => false,
				'code' => 'no-order-file-id',
				'orientation' => $defaultOrientation
			];
		}

		// Do a group by query to count how many pages of each orientation we have
		$pages = $this->OrderItemFilePage->find(
			'all', ['conditions' => ['OrderItemFilePage.order_item_file_id' => $orderItemFileId
									],
					'fields' => ['OrderItemFilePage.orientation',
								 'count(OrderItemFilePage.orientation) as page_count'
								],
					'group' => ['OrderItemFilePage.orientation'
							   ]
				   ]
		);

		if (!$pages) {

			// No pages, so fail and return the default orientation
			return [
				'success' => false,
				'code' => 'no-order-file-pages',
				'orientation' => $defaultOrientation
			];
		}

		if (count($pages) === 1) {

			// All pages are of the same orientation
			// The consistency key confirms that they are all the same orientation
			$consistency = true;

			// The results array has a key with the page totals
			$pageOrientations[$pages[0]['OrderItemFilePage']['orientation']] = $pages[0][0]['page_count'];

			// Get the single orientation
			$orientation = $pages[0]['OrderItemFilePage']['orientation'];
		} else {

			// Set a benchmark page count
			$pageCount = 0;

			// The pages have mixed orientations
			// The consistency key confirms that they are mixed

			$consistency = false;
			$orientation = 'm';

			// Loop through the results
			foreach ($pages as $page) {

				// Populate the result key with the page count for this orientation
				$pageOrientations[$page['OrderItemFilePage']['orientation']] = $page[0]['page_count'];

				// If there are more of these pages than the previously set benchmark, change the orientation to this one as it is dominant
				if ($page[0]['page_count'] > $pageCount) {

					// Reset the benchmark
					$pageCount = $page[0]['page_count'];
				}
			}
		}

		// Construct and return the result
		return [
			'success' => true,
			'consistency' => $consistency,
			'orientation' => $orientation,
			'pageOrientations' => $pageOrientations
		];
	}

	/**
	 This function looks to see if any pages in a file are colour. If so, the file is marked as colour; otherwise black & white.
	 */
	public function setColour($orderItemFileId = null, $existingColour = null) {

		if (!$orderItemFileId) {
			return;
		}

		$colours = $this->OrderItemFilePage->find(
			'count',
			[
				'conditions' => [
					'OrderItemFilePage.order_item_file_id' => $orderItemFileId,
					'OrderItemFilePage.colour' => 'col'
				]
			]
		);

		// If there are any colour pages, set the file as colour
		if ($colours) {
			$colour = 'col';
		} else {
			$colour = 'bw';
		}

		// Do we need to save the new value?
		if (!$existingColour) {
			$existingColour = $this->field(
				'colour',
				['OrderItemFile.id' => $orderItemFileId]
			);

		}

		// Yup - so save it
		if ($existingColour !== $colour) {
			$data = [
				'id' => $orderItemFileId,
				'colour' => $colour
			];
			$this->save($data, ['callbacks' => false]);
		}

	}

	/**
	 * This function looks to see if any pages in a file have trim marks. If so, the file is marked as having trim marks (true); otherwise false.
	 */
	public function setHasTrimMarks($orderItemFileId = null, $existingValue = null) {

		if (!$orderItemFileId) {
			return;
		}

		$hasTrimMarks = $this->OrderItemFilePage->find(
			'count',
			[
				'conditions' => [
					'OrderItemFilePage.order_item_file_id' => $orderItemFileId,
					'OrderItemFilePage.has_trim_marks' => true
				]
			]
		);

		$hasTrimMarks = $hasTrimMarks > 0;

		if (!$existingValue) {
			$existingValue = $this->field(
				'has_trim_marks',
				['OrderItemFile.id' => $orderItemFileId]
			);

		}

		if ($existingValue !== $hasTrimMarks) {
			$this->id = $orderItemFileId;
			$this->set('has_trim_marks', $hasTrimMarks);
			$result = $this->save();
		}

	}

	/**
	 * This function gets the sizes of the pages in the file and saves the
	 * result to the OrderItemFile record
	 */
	public function setPaperSizeConsistency($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return false;
		}

		$paperSizeConsistency = $this->paperSizeConsistency($orderItemFileId);

		$data = [
			'id' => $orderItemFileId,
			'paper_size_id' => $paperSizeConsistency['data']['paper_size_id'],
			'paper_size_consistency' => $paperSizeConsistency['data']['consistency']
		];

		$this->set($data);
		$this->save();

		return $paperSizeConsistency;
	}

	/**
	 * This function examines each page in a file looking at its size.
	 * It then determines whether each page has the same size and sets the consistency
	 */
	public function paperSizeConsistency($orderItemFileId = null) {

		// Make sure we've got something to look at
		if (!$orderItemFileId) {

			// Else fail
			return [
				'success' => false,
				'code' => 'no-order-file-id',
				'action' => 'upload'
			];
		}

		// Do a group by query to count how many pages of each paper size we have
		$pages = $this->OrderItemFilePage->find(
			'all',
			[
				'conditions' => ['OrderItemFilePage.order_item_file_id' => $orderItemFileId],
				'fields' => [
					'OrderItemFilePage.paper_size_id',
					'count(OrderItemFilePage.paper_size_id) as page_count'
				],
				'group' => ['OrderItemFilePage.paper_size_id']
			]
		);

		if (!$pages) {

			// No pages, so fail
			return [
				'success' => false,
				'code' => 'no-order-file-pages',
				'action' => 'process-files'
			];
		}

		if (count($pages) === 1) {

			// All pages are of the same paper size
			// The results array has a key with the page totals
			$paperSizes[$pages[0]['OrderItemFilePage']['paper_size_id']] = $pages[0][0]['page_count'];

			// Get the single size
			$paperSizeId = $pages[0]['OrderItemFilePage']['paper_size_id'];

			return [
				'success' => true,
				'data' => [
					'consistency' => true,
					'paper_size_id' => $paperSizeId,
					'paperSizes' => $paperSizes
				]
			];
		}

		// Set a benchmark page count
		$pageCount = 0;

		// The pages have mixed sizes
		// The consistency key confirms that they are mixed

		$consistency = false;

		// Loop through the results
		foreach ($pages as $page) {

			// Populate the result key with the page count for this size
			$paperSizes[$page['OrderItemFilePage']['paper_size_id']] = $page[0]['page_count'];

			// If there are more of these pages than the previously set benchmark, change the size to this one as it is dominant
			if ($page[0]['page_count'] > $pageCount) {

				// Reset the benchmark
				$pageCount = $page[0]['page_count'];

				// Set the size
				$paperSizeId = $page['OrderItemFilePage']['paper_size_id'];
			}
		}

		return [
			'success' => false,
			'data' => [
				'consistency' => false,
				'paper_size_id' => $paperSizeId,
				'paperSizes' => $paperSizes
			]
		];
	}

	public function setPaperSizeMatchConsistency($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return false;
		}

		$paperSizeMatchConsistency = $this->paperSizeMatchConsistency($orderItemFileId);

		$data = [
			'id' => $orderItemFileId,
			'paper_size_match_consistency' => $paperSizeMatchConsistency['consistency']
		];

		$data['paper_size_match'] = $this->paperSizeMatch($paperSizeMatchConsistency);

		$this->set($data);
		$this->save();
	}

	/**
	 This function examines how well pages are matched to paper sizes
	 */
	public function paperSizeMatchConsistency($orderItemFileId = null) {

		// Make sure we've got something to look at
		if (!$orderItemFileId) {

			// Else fail
			return [
				'success' => false,
				'code' => 'no-order-file-id'
			];
		}

		// Do a group by query to count how many pages of each paper size we have
		$pages = $this->OrderItemFilePage->find(
			'all',
			[
				'conditions' => ['OrderItemFilePage.order_item_file_id' => $orderItemFileId],
				'fields' => [
					'OrderItemFilePage.paper_size_match',
					'count(OrderItemFilePage.paper_size_match) as page_count'
				],
				'group' => ['OrderItemFilePage.paper_size_match']
			]
		);

		if (!$pages) {

			// No pages, so fail
			return [
				'success' => false,
				'code' => 'no-order-file-pages'
			];
		}

		if (count($pages) === 1) {

			// All pages are of the same paper size

			$consistency = true;

			// The results array has a key with the page totals
			$matches[$pages[0]['OrderItemFilePage']['paper_size_match']] = $pages[0][0]['page_count'];

		} else {

			// At least some of the pages do not match standard paper sizes
			$consistency = false;

			// Loop through the results
			foreach ($pages as $page) {

				// Populate the result key with the page count for this match
				$matches[$page['OrderItemFilePage']['paper_size_match']] = $page[0]['page_count'];
			}

		}

		// Construct and return the result
		return [
			'consistency' => $consistency,
			'matches' => $matches
		];
	}

	private function paperSizeMatch($paperSizeMatchConsistency = []) {

		// Start off assuming all is fine
		$paperSizeMatch = 'exact';

		// If nothing is passed, assume the pages are fine
		if (empty($paperSizeMatchConsistency['matches'])) {
			return $paperSizeMatch;
		}

		// get the matches key for convenience
		$paperSizeMatches = $paperSizeMatchConsistency['matches'];

		// Get rid of the exact matches as they don't count (they are fine)
		if (!empty($paperSizeMatches['exact'])) {
			unset($paperSizeMatches['exact']);
		}

		// Is there anything left?
		if (!$paperSizeMatches) {
			// No, so return the default - all is fine
			return $paperSizeMatch;
		}

		// Swap the keys and values around
		$paperSizeMatches = array_flip($paperSizeMatches);

		// Sort them by the count in reverse order
		// If two or more had the same count only one will be left - but that's fine
		krsort($paperSizeMatches);

		// Loop through the remaining values but return the first one
		foreach ($paperSizeMatches as $paperSizeMatch) {
			return $paperSizeMatch;
		}

		// Return the default in case we get here by accident
		return $paperSizeMatch;

	}

	/**
	 * This function takes the paper size of the product selected in the order item
	 * and compares it with the pages in the files.
	 * It's purposes is to detect under and over sizing.
	 */
	public function setPaperSizeScaleConsistency($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return false;
		}

		$paperSizeScale = $this->paperSizeScaleConsistency($orderItemFileId);

		$data = ['id' => $orderItemFileId,
				 'paper_size_scale_consistency' => $paperSizeScale['consistency']
				];

		$this->set($data);
		$result = $this->save();

		return $result;
	}

	/**
	 * This function examines each the scale factor of each page in a file
	 * to determine if the scaling is consistent.
	 */
	public function paperSizeScaleConsistency($orderItemFileId = null) {

		// Make sure we've got something to look at
		if (!$orderItemFileId) {

			// Else fail
			return [
				'success' => false,
				'code' => 'no-order-file-id',
				'from' => 'OrderItemFile.paperSizeScale'
			];
		}

		// Do a group by query to count how many pages of each paper size we have
		$pages = $this->OrderItemFilePage->find(
			'all',
			[
				'conditions' => ['OrderItemFilePage.order_item_file_id' => $orderItemFileId],
				'fields' => [
					'OrderItemFilePage.paper_size_scale',
					'count(OrderItemFilePage.paper_size_scale) as page_count'
				],
				'group' => ['OrderItemFilePage.paper_size_scale']
			]
		);

		if (!$pages) {

			// No pages, so fail
			return [
				'success' => false,
				'code' => 'no-order-file-pages',
				'from' => 'OrderItemFile.paperSizeScale'
			];
		}

		if (count($pages) === 1) {

			// All pages are of the same paper size

			$consistency = true;

			// The results array has a key with the page totals
			$matches[$pages[0]['OrderItemFilePage']['paper_size_scale']] = $pages[0][0]['page_count'];

		} else {

			// At least some of the pages do not match standard paper sizes
			$consistency = false;

			// Loop through the results
			foreach ($pages as $page) {

				// Populate the result key with the page count for this match
				$matches[$page['OrderItemFilePage']['paper_size_scale']] = $page[0]['page_count'];
			}
		}

		// Construct and return the result
		return [
			'consistency' => $consistency,
			'matches' => $matches
		];
	}

	public function removeFromOrderItem($orderItemId = null) {

		if (!$orderItemId) {
			return;
		}

		$orderItemFiles = $this->find(
			'list',
			[
				'conditions' => [
					'OrderItemFile.order_item_id' => $orderItemId
				],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.id'
				]
			]
		);

		if (!$orderItemFiles) {
			return;
		}

		$requote = false;

		foreach ($orderItemFiles as $orderItemFileId) {

			$result = $this->deleteOrderItemFile($orderItemFileId);

		}

		$this->resync($orderItemId);

		return;
	}

	/**
	 * The delete uploaded function removes an uploaded file from $_FILES and the file system
	 * @param array $documents the array containing the files to be removed
	 * @param int $orderItemFileKey the key that relates to the document to be deleted
	 *
	 * @return boolean true/false for sucees/fail
	 */
	public function deleteOrderItemFile($orderItemFileId = null) {

		if (!$orderItemFileId) {
			return [
				'success' => false,
				'code' => 'missing-id',
				'message' => 'There was an error deleting the file.'
			];
		}

		$this->id = $orderItemFileId;

		$orderItemId = $this->field(
			'order_item_id',
			['OrderItemFile.id' => $orderItemFileId]
		);

		if (!$this->exists()) {
			return [
				'success' => false,
				'code' => 'missing-document',
				'message' => 'That file does not exist.'
			];
		}

		$orderItemFile = $this->findById($orderItemFileId);

		$pathRoot = $orderItemFile['OrderItemFile']['path_root'];

		$folder = new Folder($pathRoot);

		if (!$folder->path) {

			$this->delete($orderItemFileId);

		} else {

			try {

				$folderDeleted = $folder->delete();

				if ($folderDeleted) {

					$this->delete($orderItemFileId);

				} else {

					return [
						'success' => false,
						'code' => 'error',
						'message' => 'There was a problem deleting your file.'
					];

				}

			} catch (Exception $e) {

				return [
					'success' => false,
					'code' => 'error',
					'message' => $e->getMessage()
				];
			}

		}

		if ($orderItemId) {
			$result = $this->resync($orderItemId);

		};

		return [
			'success' => true,
			'code' => 'file-deleted',
			'message' => 'The file has been deleted.'
		];

	}

	public function rotateImage($fileName = null, $rotateAngle = 0) {

		if ($this->callasMethod() == 'server') {

			// Images can't be rotated on the current Windows based server
			// (which is only used for local testing) so simply return true
			return true;
		}

		App::import('Vendor', 'ImageTool/ImageTool');

		if (substr($fileName, 0, 1) == '/') {
			$fileName = substr($fileName, 1);
		}

		$path = WWW_ROOT . $fileName;

		if (!file_exists($fileName)) {
			return false;
		}

		$result = ImageTool::rotate([
			'input' => $fileName,
			'output' => $fileName,
			'degrees' => $rotateAngle
		]);

		return $result;
	}

	public function resequence($orderItemFileIds = []) {

		if (!$orderItemFileIds && $this->orderItemId) {

			$orderItemFileIds = $this->find(
				'list',
				[
					'conditions' => [
						'OrderItemFile.order_item_id' => $this->orderItemId
					],
					'fields' => [
						'OrderItemFile.id',
						'OrderItemFile.id'
					],
					'order' => 'OrderItemFile.sequence'
				]
			);

			if ($orderItemFileIds) {

				$orderItemFileIds = array_values($orderItemFileIds);
			}
		}

		if (!$orderItemFileIds) {

			return;
		}

		$result = [];

		foreach ($orderItemFileIds as $key => $orderItemFileId) {

			$this->id = $orderItemFileId;

			if ($this->exists()) {

				$sequence = ($key + 1);

				$this->set('sequence', $sequence);

				if ($this->save()) {

					$result[$orderItemFileId] = $sequence;
				}
			}
		}

		return $result;

	}

	/**
	 * This function processes the 'original' files by running them through Callas, applying the required profiles
	 * @param  string [$orderItemId         = null] The id of the order item we are working on
	 * @return array  An array that contains the response
	 */
	public function processOriginalFiles($orderItemId = null) {

		if (!$orderItemId) {

			return [
				'success' => false,
				'code' => 'no-order-item-id',
				'from' => 'OrderItemFile.processOriginalFiles'
			];

		}

		$orderItemFiles = $this->find(
			'list',
			[
				'conditions' => [
					'OrderItemFile.order_item_id' => $orderItemId
				],
				'fields' => 'OrderItemFile.id'
			]
		);

		if (!$orderItemFiles) {

			return [
				'success' => true,
				'code' => 'no-order-item-filess',
				'from' => 'OrderItemFile.processOriginalFiles'
			];

		}

		$result = [];

		foreach ($orderItemFiles as $orderItemFileId) {

			$result[] = $this->processOriginalFile($orderItemFileId);

		}

		return [
			'success' => true,
			'result' => $result
		];

	}

	/**
	 * This function takes an 'original' file and runs it through Callas, applying the requirted profiles
	 * @param  string [$orderItemFileId         = null] The id of the order item file we are working on
	 * @return array  An array of results
	 */
	public function processOriginalFile($orderItemFileId = null) {

		if (!$orderItemFileId) {

			return [
				'success' => false,
				'code' => 'no-order-item-file_id',
				'from' => 'OrderItemFile.processOriginalFile'
			];

		}

		$orderItemFile = $this->find(
			'first',
			[
				'conditions' => [
					'OrderItemFile.id' => $orderItemFileId
				],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.order_item_id',
					'OrderItemFile.sequence',
					'OrderItemFile.name',
					'OrderItemFile.type',
					'OrderItemFile.file_name_without_extension',
					'OrderItemFile.path_root',
					'OrderItemFile.callas_filename',
					'OrderItemFile.pages'
				],
				'contain' => [
					'OrderItem' => [
						'fields' => [
							'OrderItem.id',
							'OrderItem.pace_quote_product_id',
							'OrderItem.count_order_item_file',
							'OrderItem.printed_cover_job_id',
							'OrderItem.printed_cover_front_options',
							'OrderItem.printed_cover_back_options'
						],
						'OrderItemProcess' => [
							'Process'
						]
					]
				]
			]
		);

		if (!$orderItemFile) {

			return [
				'success' => false,
				'code' => 'no-order-item-file',
				'from' => 'OrderItemFile.processOriginalFile'
			];

		}

		$result = $this->moveToFinalFolder($orderItemFileId);

		if (!$result['success']) {
			return $result;
		}

		// SG: Find out if the file needs pages removed for a cover
		$this->log("Processing File for OrderItem id " . $orderItemFile['OrderItem']['pace_quote_product_id'], 'debug');

		$finalFileName = $orderItemFile['OrderItemFile']['path_root'] . DS . 'final' . DS . $orderItemFile['OrderItemFile']['callas_filename'];

		try {

			// Test the file exists
			$finalFile = New File($finalFileName);

		} catch (Exception $e) {

			// Just catch and debug the error for now

			return [
				'success' => false,
				'code' => 'new-file-failed',
				'from' => 'OrderItemFile.processOriginalFile',
				'finale-file-name' => $finalFileName,
				'message' => $e->getMessage()
			];

		}

		if($orderItemFile['OrderItem']['printed_cover_job_id']) {

			$this->log("Has Cover, need to process final file...", 'debug');

			$this->log("FRONT: " . $orderItemFile['OrderItem']['printed_cover_front_options'] . ", SEQUENCE: " . $orderItemFile['OrderItemFile']['sequence'], 'debug');
			$this->log("BACK: " . $orderItemFile['OrderItem']['printed_cover_back_options'] . ", SEQUENCE: " . $orderItemFile['OrderItemFile']['sequence'] . ", FILES: " . $orderItemFile['OrderItem']['count_order_item_file'], 'debug');

			// We have a cover ID, so let's see if use_and_discard is picked for either front or back options
			if($orderItemFile['OrderItem']['printed_cover_front_options'] == "use_and_discard" && $orderItemFile['OrderItemFile']['sequence'] == 1) {
				$command = '--splitpdf --splitscheme=2--1 --outputfile=' . $finalFileName . '_nofront ' . $finalFileName;
				$result = $this->callasExecute($command);
				$finalFile = new File($finalFileName);
				$finalFile->delete();
				$finalFileNoFront = new File($finalFileName . "_nofront");
				$finalFileNoFront->copy($finalFileName, true);
				$finalFileNoFront->delete();
			}

			if($orderItemFile['OrderItem']['printed_cover_back_options'] == "use_and_discard" && $orderItemFile['OrderItemFile']['sequence'] == $orderItemFile['OrderItem']['count_order_item_file']) {
				$command = '--splitpdf --splitscheme=1--2 --outputfile=' . $finalFileName . '_noback ' . $finalFileName;
				$result = $this->callasExecute($command);
				$finalFile = new File($finalFileName);
				$finalFile->delete();
				$finalFileNoBack = new File($finalFileName . "_noback");
				$finalFileNoBack->copy($finalFileName, true);
				$finalFileNoBack->delete();
			}

		}

		if (empty($orderItemFile['OrderItem']['OrderItemProcess'])) {

			return [
				'success' => false,
				'code' => 'no-order-item-file_id',
				'from' => 'OrderItemFile.processOriginalFile'
			];

		}

		// Loop throught the profiles
		foreach ($orderItemFile['OrderItem']['OrderItemProcess'] as $process) {

			// Get the profile name
			$profileName = $process['Process']['profile_name'];
			$profileParams = $process['Process']['parameters'];

			//$this->log("PROFILE: $profileName $profileParams", 'debug');

			// Now run the profile against it
			$result = $this->runProfile($profileName, $finalFileName, $profileParams);

			//$this->log("PROFILE RESULT: " . print_r($result, true), 'debug');

		}


		return ['success' => true];

	}

	public function moveToFinalFolder($orderItemFileId = null) {

		if (!$orderItemFileId) {

			return [
				'success' => false,
				'code' => 'no-order-item-file',
				'from' => 'OrderItemFile.moveToFinalFolder'
			];

		}

		$orderItemFile = $this->find(
			'first',
			[
				'conditions' => [
					'OrderItemFile.id' => $orderItemFileId
				],
				'fields' => [
					'OrderItemFile.id',
					'OrderItemFile.name',
					'OrderItemFile.path_root'
				]
			]
		);

		$path = $orderItemFile['OrderItemFile']['path_root'] . DS . '%s' . DS . $orderItemFile['OrderItemFile']['name'];

		$originalFilePath = sprintf($path, 'original');
		$finalFilePath = sprintf($path, 'final');

		$file = new File($originalFilePath);

		if (!$file->path) {

			return [
				'success' => false,
				'code' => 'file-not-found',
				'original_file_path' => $originalFilePath,
				'from' => 'OrderItemFile.moveToFinalFolder'
			];
		}

		try {

			$file->copy($finalFilePath, true);

			return [
				'success' => true,
				'original_file_path' => $originalFilePath,
				'final_file_path' => $finalFilePath
			];

		} catch (Exception $e) {

			return [
				'success' => false,
				'code' => 'file-copy-failed',
				'message' => $e->getMessage(),
				'from' => 'OrderItemFile.moveToFinalFolder'
			];
		}

		return [
			'success' => false,
			'code' => 'unknown-error',
			'from' => 'OrderItemFile.moveToFinalFolder'
		];

	}

}
