<?php
App::uses('HttpSocket','Network/Http');
App::uses('DataSource','Model/Datasource');
class PaypalSource extends DataSource {

/**
 * Http is the HttpSocket Object.
 * @access public
 * @var object
 */
	var $Http = null;

/**
 * constructer.  Load the HttpSocket into the Http var.
 */
	function __construct($config = array()) {
		$this->log(print_r($config, true), 'paypal');

		$this->Http = new HttpSocket();

// Sending options back to PayPal was failing
//		$this->Http = new HttpSocket([
//			'ssl_verify_peer' => true,
//			'ssl_allow_self_signed' => true,
//			'ssl_cafile' => '/var/www/doxdirect_wp/order/app/Config/certificate.pem'
//		]);
	}

/**
 * Strip slashes
 * @param string value
 * @return string
 */
	static function clearSlash($value){
		return get_magic_quotes_runtime() ? stripslashes($value) : $value;
	}

/**
 * verifies POST data given by the paypal instant payment notification
 * @param array $data Most likely directly $_POST given by the controller.
 * @return boolean true | false depending on if data received is actually valid from paypal and not from some script monkey
 */
	function isValid($data = []) {

		$this->log(print_r($data, true), 'paypal');

		// Hacked in to resolve the SSL certificate verification issue. Should be removed in live.
//		return true;

		if (!$data) {
			$this->log('No data', 'paypal');
		}

		$data['cmd'] = '_notify-validate';

		$data = array_map(array('PaypalSource', 'clearSlash'), $data);

		$this->log($data, 'paypal');

		if (isset($data['test_ipn'])) {
			$server = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		} else {
			$server = 'https://www.paypal.com/cgi-bin/webscr';
		}

		$response = $this->Http->post($server, $data);

		if ($response->body == "VERIFIED") {
			return true;
		}

		if (!$response) {
			$this->log('HTTP Error in PaypalSource::isValid while posting back to PayPal', 'paypal');
		}

		return false;
	}

}
