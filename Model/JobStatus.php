<?php
App::uses('AppModel', 'Model');
/**
* JobStatus Model
*
* @property OrderDate $OrderDate
* @property Order $Order
*/
class JobStatus extends AppModel {

	/**
* Display field
*
* @var string
*/
	public $displayField = 'name';
	public $order = 'sequence';
	public $actsAs = array('Defaultable');

	/**
* hasMany associations
*
* @var array
*/
	public $hasMany = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'job_status_id',
			'dependent' => false
		)
	);

}
