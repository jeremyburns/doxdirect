<?php
App::uses('AppModel', 'Model');
App::uses('CakeTime', 'Utility');
/**
 * HolidayAlert Model
 *
 */
class HolidayAlert extends AppModel {

	public function message($date = null) {

		if (!$date) {
			return null;
		}

		$day = $date->format('d');
		$month = $date->format('m');
		$year = $date->format('Y');
		$strDate = $date->format('Y-m-d');

		$holidayAlert = $this->find(
			'first',
			array(
				'conditions' => array(
					'HolidayAlert.is_active' => true,
					'HolidayAlert.display_from_date <=' => $strDate,
					'HolidayAlert.display_to_date >=' => $strDate,
				),
				'fields' => array(
					'HolidayAlert.heading',
					'HolidayAlert.text'
				)
			)
		);

/**
	This piece needs work. Want to be able to identify to and from periods but that's hard
	as the month might go from 12 to 1 etc.
	Need to understand the logic.
	E.g:

	Date from = 29-4-2015
	Date to = 5-5-2015

	Actual date (1) 30-4-215
	Actual date (2) 1-5-2015

	Day has reduced while the month has increased.
*/
/*
		if (!$holidayAlert) {

			$holidayAlert = $this->find(
				'first',
				array(
					'conditions' => array(
						'OR' => array(
							array(
								'HolidayAlert.display_from_day' => $day,
								'HolidayAlert.display_from_month' => $month,
								'HolidayAlert.display_from_year' => $year
							)
						)
					),
					'fields' => array(
						'HolidayAlert.title',
						'HolidayAlert.text'
					)
				)
			);

		}
*/

		if (!$holidayAlert) {
			return false;
		}

		return $holidayAlert;

	}
}
