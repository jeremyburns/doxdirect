<?php
App::uses('AppModel', 'Model');
/**
 * OrderItemFilePage Model
 *
 * @property OrderItem $OrderItem
 */
class OrderItemFilePage extends AppModel {

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $belongsTo = [
		'OrderItemFile' => [
			'className' => 'OrderItemFile',
			'foreignKey' => 'order_item_file_id',
			'counterCache' => 'count_order_item_file_page'
		],
		'PaperSize' => [
			'className' => 'PaperSize',
			'foreignKey' => 'paper_size_id'
		]
	];

	public $hasMany = [
		'OrderItemFilePageProcess' => [
			'className' => 'OrderItemFilePageProcess',
			'foreignKey' => 'order_item_file_page_id',
			'dependent' => true
		]
	];

	var $order = 'callas_nr';

	public $actsAs = ['Callas'];

	public $orientations = [
		null => 'Unknown',
		'm' => 'Mixed',
		'p' => 'Portrait',
		'l' => 'Landscape',
		'u' => 'Unknown'
	];

	public $images = [
		[
			'images' => [
				'add_crop_marks_and_trim.png'
			],
			'caption' => 'Add crop marks and fit',
			'description' => 'Used when the document is odd sized and we offer to add crop marks and trim to size.'
		],
		[
			'images' => [
				'print_to_trim_marks.png'
			],
			'caption' => 'Print on SRA/larger paper',
			'description' => 'Used when we have identified trim marks. We will print the document on SRA paper (or the next larger A size) and trim to size.'
		],
		[
			'images' => [
				'rotate_l_to_p.png'
			],
			'caption' => 'Rotate landscape to portrait',
			'description' => 'Used when there is mixed orientation. We will rotate all landscape pages to portrait (there is a choice of clockwise or anticlockwise).'
		],
		[
			'images' => [
				'rotate_p_to_l.png'
			],
			'caption' => 'Rotate portrait to landscape',
			'description' => 'Used when there is mixed orientation. We will rotate all portrait pages to landscape (there is a choice of clockwise or anticlockwise).'
		],
		[
			'images' => [
				'landscape_binding_edge_long.png',
				'portrait_binding_edge_long.png'
			],
			'caption' => 'Binding edge long',
			'description' => 'Used when there is the option of placing the binding on the long edge.'
		],
		[
			'images' => [
				'landscape_binding_edge_short.png',
				'portrait_binding_edge_short.png'
			],
			'caption' => 'Binding edge short',
			'description' => 'Used when there is the option of placing the binding on the short edge.'
		],
		[
			'images' => [
				'landscape_binding_edge_left.png',
				'portrait_binding_edge_left.png'
			],
			'caption' => 'Binding edge left',
			'description' => 'Used to determine whether the bindgin should go on the left hand edge.'
		],
		[
			'images' => [
				'landscape_binding_edge_right.png',
				'portrait_binding_edge_right.png'
			],
			'caption' => 'Binding edge right',
			'description' => 'Used to determine whether the bindgin should go on the right hand edge.'
		],
		[
			'images' => [
				'landscape_binding_side_head_to_foot.png',
				'portrait_binding_side_head_to_foot.png'
			],
			'caption' => 'Head to foot binding',
			'description' => 'Used when there is the option of binding head to foot.'
		],
		[
			'images' => [
				'landscape_binding_side_head_to_head.png',
				'portrait_binding_side_head_to_head.png'
			],
			'caption' => 'Head to head binding',
			'description' => 'Used when there is the option of binding head to head.'
		],
		[
			'images' => [
				'landscape_insert_blank_pages.png',
				'portrait_insert_blank_pages.png'
			],
			'caption' => 'Insert blank pages',
			'description' => 'Used when the page counts are not correctly divisible and we need to insert blank pages.'
		],
		[
			'images' => [
				'landscape_print_as_is.png',
				'portrait_print_as_is.png'
			],
			'caption' => 'Print as is',
			'description' => 'Used when the uploaded file is an odd size. We will just print the document as is with white borders around.'
		],
		[
			'images' => [
				'landscape_scale_up_to_paper_size.png',
				'portrait_scale_up_to_paper_size.png'
			],
			'caption' => 'Enlarge image',
			'description' => 'Used when the uploaded file is smaller than the chosen paper size. We will scale up the image to fill the paper (and warn that the result might be blurred).'
		],
		[
			'images' => [
				'landscape_scale_down_to_paper_size.png',
				'portrait_scale_down_to_paper_size.png'
			],
			'caption' => 'Reduce image',
			'description' => 'Used when the uploaed file is larger than the chosen paper. We will shrink the image to fit within the paper.'
		],
		[
			'images' => [
				'landscape_increase_paper.png',
				'portrait_increase_paper.png'
			],
			'caption' => 'Increase paper',
			'description' => 'Used when the uploaded file is larger than the chosen paper. We will increase the paper size so the document fits.'
		],
		[
			'images' => [
				'landscape_reduce_paper.png',
				'portrait_reduce_paper.png'
			],
			'caption' => 'Reduce paper',
			'description' => 'Used when the uploaded image is smaller than the chosen paper. We will reduce the paper size to fit the image.'
		]
	];

/**
	The extract function extracts the meta data about a page from the supplied XML, performs some simple calculations and creates a database row for it.
*/
	public function extractPage($page = [], $fileNameWithOutExtension, $orderItemFileId, $orderId, $orderItemPaperSizeId, $previewPath) {

		if (!$page) {
			return ['success' => false];
		}

		$colour = $this->extractColourFromPlates($page['platenames']);

		$orderItemFilePage = [
			'order_item_file_id' => $orderItemFileId,
			'callas_id' => $page['@id'],
			'callas_nr' => $page['@nr'],
			'callas_rotate' => isset($page['rotate']) ? $page['rotate'] : 0,
			'plates' => $page['plates'],
			'colour' => $colour
		];

		$paperSize = $this->PaperSize->getSizeFromPage($page);

		$orderItemFilePage = array_merge(
			$orderItemFilePage,
			$paperSize
		);

		$orderItemFilePage['preview_image_path'] = $this->getPreviewPath(
			$fileNameWithOutExtension,
			$page['@nr'],
			$orderId,
			$previewPath
		);

		$this->create();
		$result = $this->save($orderItemFilePage);

		$orderItemFilePageId = $this->id;

		$this->analysePage($orderItemFilePageId, ['orderItemPaperSizeId' => $orderItemPaperSizeId]);

		return $result;

	}

	public function analysePage($orderItemFilePageId = null, $metrics = []) {

		$this->setPaperSizeScale($orderItemFilePageId = null, $metrics = []);

	}

	public function setPaperSizeScale($orderItemFilePageId = null, $metrics = []) {

		if (!empty($metrics['orderItemPaperSizeId'])) {

			$paperSizeId = $this->field(
				'paper_size_id',
				['OrderItemFilePage.id' => $orderItemFilePageId]
			);

			$orderItemPaperSizeId = $metrics['orderItemPaperSizeId'];

			// Is this page scaled compared with the size chosen in the order?

			$paperSizeScale = $this->PaperSize->paperSizeScale(
				$paperSizeId,
				$orderItemPaperSizeId
			);

			$this->id = $orderItemFilePageId;
			$this->set('paper_size_scale', $paperSizeScale);
			$this->save();

		}

	}

	private function extractColourFromPlates($plateNames = []) {

		if (!$plateNames) {
			return 'bw';
		}

		if (count($plateNames['platename']) === 1 && $plateNames['platename'][0] == 'Black') {
			return 'bw';
		}

		return 'col';

	}

	public function updateHasPreview($pageIds = []) {

		$this->updateAll(
			['OrderItemFilePage.has_preview' => true],
			['OrderItemFilePage.id' => $pageIds]
		);

	}

	public function insertBlankPages($orderItemFileId = null, $blankPageCount = 0) {

		// Get the id of the last page in the order item file to use as a template
		$orderItemFilePage = $this->find(
			'first',
			[
				'conditions' => [
					'OrderItemFilePage.order_item_file_id' => $orderItemFileId
				],
				'order' => [
					'OrderItemFilePage.callas_nr DESC'
				],
				'fields' => [
					'OrderItemFilePage.order_item_file_id',
					'OrderItemFilePage.callas_nr',
					'OrderItemFilePage.callas_rotate',
					'OrderItemFilePage.size_box',
					'OrderItemFilePage.has_trim_marks',
					'OrderItemFilePage.tl_x',
					'OrderItemFilePage.tl_y',
					'OrderItemFilePage.br_x',
					'OrderItemFilePage.br_y',
					'OrderItemFilePage.width',
					'OrderItemFilePage.height',
					'OrderItemFilePage.paper_size_id',
					'OrderItemFilePage.paper_size_match',
					'OrderItemFilePage.paper_size_scale',
					'OrderItemFilePage.orientation',
					'OrderItemFilePage.plates',
					'OrderItemFilePage.colour'
				]
			]
		);

		$currentPageNumber = $orderItemFilePage['OrderItemFilePage']['callas_nr'];
		$orientation = $orderItemFilePage['OrderItemFilePage']['orientation'];
		$previewImagePath = $orientation == 'l'
			? 'pages' . DS . 'landscape_'
			: 'pages' . DS . 'portrait_'
		;

		$previewImagePath .= 'inserted_blank_page';

		$blankPages = [];

		for ($n = 1; $n <= $blankPageCount; $n++) {

			$blankPage['OrderItemFilePage'] = array_merge(
				$orderItemFilePage['OrderItemFilePage'],
				[
					'callas_id' => 'blank',
					'callas_nr' => intval($currentPageNumber + $n),
					'preview_image_path' => $previewImagePath,
					'has_preview' => true
				]
			);

			$this->create();
			$result = $this->save($blankPage);

		}

		$this->OrderItemFile->OrderItemFileProcess->record(
			$orderItemFileId,
			'insert-blank-pages',
			['page_count' => $blankPageCount]
		);

		return ['success' => true];

	}

}
