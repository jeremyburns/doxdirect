<?php
App::uses('AppModel', 'Model');
/**
 * ProductMapCategoryExtraField Model
 *
 */
class ProductMapCategoryExtraField extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ProductMapCategory' => array(
			'className' => 'ProductMapCategory',
			'foreignKey' => 'product_map_category_id',
			'dependent' => false
		)
	);

}
