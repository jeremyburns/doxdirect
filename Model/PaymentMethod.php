<?php
App::uses('AppModel', 'Model');
/**
 * PaymentMethod Model
 *
 */
class PaymentMethod extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $actsAs = array('Defaultable');

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'payment_method_id',
			'dependent' => false,
			'counterCache' => 'count_order'
		)
	);

}
