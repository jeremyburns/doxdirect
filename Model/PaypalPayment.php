<?php
App::uses('PaypalSource', 'Model/Datasource');
App::uses('AppModel', 'Model');
/**
 * Product Model
 *
 */
class PaypalPayment extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'order_id'
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	var $hasMany = array(
		'PaypalPaymentItem' => array(
			'className' => 'PaypalPaymentItem',
		)
	);

/**
 * the PaypalSource
 */
	var $paypal = null;

/**
 * verifies POST data given by the paypal instant payment notification
 * @param array $data Most likely directly $_POST given by the controller.
 * @return boolean true | false depending on if data received is actually valid from paypal and not from some script monkey
 */
	function isValid($data) {

		$this->log(print_r($data, true), 'paypal');

		if (!empty($data)) {

			if (!class_exists('PaypalSource')) {
				trigger_error(__d('paypal', 'PaypalSource: The datasource could not be found.'), E_USER_ERROR);
			}

			$this->Paypal = new PaypalSource();

			return $this->Paypal->isValid($data);

		}

		return false;

	}

	/**
	* If all we have is item_number1 but not just item_number, assign item_number to item_number1
	*
	* @param array data
	* @param array options
	* @return mixed result of save
	*/
	public function saveAll($data = array(), $options = array()) {

		$fields = array(
			'mc_gross',
			'item_number',
			'item_name',
		);

		foreach ($fields as $field) {

			if (!isset($data[$this->alias][$field]) && isset($data[$this->alias][$field . '1'])) {
				$data[$this->alias][$field] = $data[$this->alias][$field . '1'];
			}

		}
		$this->log('Saveall data:', 'paypal');
		$this->log($data, 'paypal');

		return parent::saveAll($data, $options);

	}

/**
 * builds the associative array for PaypalPaymentItems only if it was a cart upload
 *
 * @param raw post data sent back from paypal
 * @return array of cakePHP friendly association array.
 */
  function buildAssociationsFromIPN($post) {

		$retval = array();

		$retval[$this->alias] = $post;

		if (isset($post['num_cart_items']) && $post['num_cart_items'] > 0) {

			$retval['PaypalPaymentItem'] = array();

			for ($i=1;$i<=$post['num_cart_items'];$i++) {
				$key = $i - 1;
				$retval['PaypalPaymentItem'][$key]['item_name'] = $post["item_name{$i}"];
				$retval['PaypalPaymentItem'][$key]['item_number'] = $post["item_number{$i}"];
				$retval['PaypalPaymentItem'][$key]['quantity'] = $post["quantity{$i}"];
				$retval['PaypalPaymentItem'][$key]['mc_shipping'] = $post["mc_shipping{$i}"];
				$retval['PaypalPaymentItem'][$key]['mc_handling'] = $post["mc_handling{$i}"];
				$retval['PaypalPaymentItem'][$key]['mc_gross'] = $post["mc_gross_{$i}"];
				$retval['PaypalPaymentItem'][$key]['tax'] = $post["tax{$i}"];
			}

		}

		$this->log('Retval:', 'paypal');
		$this->log($retval, 'paypal');
		return $retval;

	}

/**
 * searches existing IPNs transactions using txn_id or parent_txn_id
 *
 * @param array $data
 * @return mixed false if not matched, existingIpnId otherwise
 * @todo Figure out what to do if more than one record is found
 */
	function searchPaypalPaymentTxn($data = array()) {

		if (empty($data['PaypalPayment']['txn_id'])) {
			return false;
		}

		$paypalPaymentId = $this->findAllByTxnId($data['PaypalPayment']['txn_id']);

		if (!empty($paypalPaymentId) && (count($paypalPaymentId) == 1)) {
			return $paypalPaymentId[0]['PaypalPayment']['id'];
		}

		$paypalPaymentId = $this->findAllByParentTxnId($data['PaypalPayment']['txn_id']);

		if (!empty($paypalPaymentId) && (count($paypalPaymentId) == 1)) {
			return $paypalPaymentId[0]['PaypalPayment']['id'];
		}

		return false;

	}

	public function afterPaypalNotification($paypalPaymentId = null) {

		if (!$paypalPaymentId) {
			$this->log('No paypalPaymentId supplied', 'paypal');
		}

		$this->log('After PayPal payment ' . $paypalPaymentId, 'paypal');

		$paypalPayment = $this->findById($paypalPaymentId);

		if (!$paypalPayment) {

			$result = [
				'success' => false,
				'code' => 'paypal-payment-not-found',
				'paypal-payment-id' => $paypalPaymentId
			];

			$this->log($result, 'paypal');

			return $result;

		}
$this->log('PayPal Payment', 'paypal');
$this->log(print_r($paypalPayment, true), 'paypal');

		$orderId = $this->Order->field(
			'id',
			[
				'Order.pace_job_number' => $paypalPayment['PaypalPayment']['item_number']
			]
		);

		if (!$orderId) {
			$result = [
				'success' => false,
				'code' => 'no-order-id',
				'paypal-payment-id' => $paypalPaymentId
			];

			$this->log($result, 'paypal');

			return $result;
		}

		$this->id = $paypalPaymentId;
		$result = $this->saveField(
			'order_id',
			$orderId
		);

		if (!$result) {
			$result = [
				'success' => false,
				'code' => 'paypal-payment-order-id-update-failed',
				'paypal-payment-id' => $paypalPaymentId,
				'order-id' => $paypalPaymentId,
			];

			$this->log($result, 'paypal');

			return $result;
		}

		$paymentStatus = $paypalPayment['PaypalPayment']['payment_status'];

		if ($paymentStatus == 'Completed') {
			$amountPaid = $paypalPayment['PaypalPayment']['mc_gross'];
			$result = $this->Order->receivePayment($orderId, $amountPaid);

			return $result;
		}

		return [
			'success' => true
		];

	}

}
