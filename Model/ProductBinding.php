<?php
App::uses('AppModel', 'Model');
/**
 * ProductBinding Model
 *
 * @property Product $Product
 * @property Binding $Binding
 */
class ProductBinding extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'dependent' => false
		),
		'Binding' => array(
			'className' => 'Binding',
			'foreignKey' => 'binding_id',
			'dependent' => false
		)
	);

}