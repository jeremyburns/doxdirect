<?php

class OrderShell extends AppShell {

	public $uses = array('Order');

	// The main function runs by default
	public function main() {

		$this->out('STARTING TO PREPARE ORDERS FOR PACE');

		$result = $this->prepareOrdersForPace();

		$this->out('COMPLETED PREPARING ORDERS FOR PACE');

	}

	public function prepareOrdersForPace() {

		// Call the Order->processOriginalFiles function
		$this->out('Processing original files...');
		$result = $this->Order->processOriginalFiles();
		debug($result);
		$this->out('Completed processing original files');

		// Call the Order->createJobArchives function
		$this->out('Creating file archives...');
		$result = $this->Order->createJobArchives();
		debug($result);
		$this->out('File archives created');

	}

	public function prepareOrderForPace() {

		if (empty($this->args[0])) {

			$this->out('Please provide a valid order id.');

			die();

		}

		$orderId = $this->args[0];

		$this->out(sprintf('STARTING TO PREPARE ORDER ID %s FOR PACE', $orderId));

		// Call the Order->processOriginalFiles function
		$this->out(sprintf('Processing original files for order id %s...', $orderId));
		$result = $this->Order->processOriginalFiles($orderId);
		debug($result);
		$this->out(sprintf('Original files processed for order id %s', $orderId));

		// Call the Order->createJobArchives function
		$this->out(sprintf('Creating file archives for order id %s...', $orderId));
		$result = $this->Order->createJobArchive($orderId);
		debug($result);
		$this->out(sprintf('File archives created for order id %s', $orderId));

		$this->out(sprintf('COMPLETED PREPARING ORDER ID %s FOR PACE', $orderId));

	}

}
