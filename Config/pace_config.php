<?php

	$config = [
		'pace' => [
			'api_url' => 'http://api.pagevisions.co.uk/',
			'source' => 'DOX',
			'functions' => [
				'new_quote' => 'getQuote',
				'add_to_quote' => 'addToQuote',
				'remove_from_quote' => 'removeFromQuote',
				'replace_in_quote' => 'replaceInQuote',
				'list_delivery_options' => 'listDeliveryOptions',
				'list_products' => 'listProducts',
				'list_countries' => 'listCountries',
				'create_customer' => 'createCustomer',
				'convert_job_to_quote' => 'convertQuoteToJob',
				'product_list' => 'listProducts',
				'change_job_status' => 'changeJobStatus',
				'get_job_zip_file' => 'getJobZipFile'
			],
			'status_codes' => [
				'awaiting_files' => '4', // used after payment - see Order.receivePayment
				'send_to_digital' => 'J' // when files are ready - see Order.createJobArchive
			],
			'pace_jobs_folder' => 'pace_jobs'
		]
	];
