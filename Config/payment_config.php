<?php

	// This config file contains all values for both test and production of all payment methods.
	// To switch between environments change the values method key (e.g. paypal) from (e.g.) the keys inside paypal_test to the keys inside paypal_production
	// This means the various bits of code that read the file will read values from payment_methods.paypal

	$config = [
		'payment_methods' => [
			'paypal' => [
				'test' => false,
				'server' => 'https://www.paypal.com',
				'locale' => 'GB',
				'item_name' => 'Printing of Documents',
				'business' => 'accounts@doxdirect.com',
				'currency_code' => 'GBP',
				'url' => [
					'return' => [
						'admin' => false,
						'controller' => 'orders',
						'action' => 'paid'
					],
					'cancel_return' => [
						'admin' => false,
						'controller' => 'orders',
						'action' => 'checkout'
					],
					'notify' => [
						'admin' => false,
						'controller' => 'paypal_payments',
						'action' => 'process'
					]
				]
			],
			'paypal_production' => [
				'test' => false,
				'server' => 'https://www.paypal.com',
				'locale' => 'GB',
				'item_name' => 'Printing of Documents',
				'business' => 'accounts@doxdirect.com',
				'currency_code' => 'GBP',
				'url' => [
					'return' => [
						'admin' => false,
						'controller' => 'orders',
						'action' => 'paid'
					],
					'cancel_return' => [
						'admin' => false,
						'controller' => 'orders',
						'action' => 'checkout'
					],
					'notify' => [
						'admin' => false,
						'controller' => 'paypal_payments',
						'action' => 'process'
					]
				]
			],
			'paypal_test' => [
				'test' => true,
				'server' => 'https://www.sandbox.paypal.com',
				'locale' => 'GB',
				'item_name' => 'Printing of Documents',
				'business' => 'jeremyb@doxdirect.com',
				'currency_code' => 'GBP',
				'url' => [
					'return' => [
						'admin' => false,
						'controller' => 'orders',
						'action' => 'paid'
					],
					'cancel_return' => [
						'admin' => false,
						'controller' => 'orders',
						'action' => 'checkout'
					],
					'notify' => [
						'admin' => false,
						'controller' => 'paypal_payments',
						'action' => 'process'
					]
				]
			]
		]
	];
