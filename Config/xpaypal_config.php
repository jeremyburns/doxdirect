<?php
/************
 * Use these settings to set defaults for the Paypal Helper class.
 * The PaypalHelper class will help you create paynow, subscribe, donate, or addtocart buttons for you.
 *
 * All these options can be set on the fly as well within the helper
 */
class PaypalConfig {

/************
 * Each settings key coresponds to the Paypal API.  Review www.paypal.com for more.
 */
	var $default = array(
		'business'      => 'jeremyb@doxdirect.com',
		'server'        => 'https://www.sandbox.paypal.com',
		'notify_url'    => 'https://test.doxdirect.com/order/paypal_payments/process',
		'currency_code' => 'GBP',
		'lc'            => 'UK',
		'item_name'     => 'Documents for printing',
		'amount'        => 0,
		'encrypt'       => false,
		'test' => true
	);

	var $production = array(
		'business'      => 'accounts@doxdirect.com',
		'server'        => 'https://www.paypal.com',
		'notify_url'    => 'https://www.doxdirect.com/order/paypal_payments/process',
		'currency_code' => 'GBP',
		'lc'            => 'UK',
		'item_name'     => 'Documents for printing',
		'amount'        => 0,
		'encrypt'       => false,
		'test'			=> false
	);

	/***********
 * Test settings to test with using a sandbox paypal account.
 */
	var $test = array(
		'business'      => 'jeremyb@doxdirect.com',
		'server'        => 'https://www.sandbox.paypal.com',
		'notify_url'    => 'https://test.doxdirect.com/order/paypal_payments/process',
		'currency_code' => 'GBP',
		'lc'            => 'UK',
		'item_name'     => 'Documents for printing',
		'amount'        => 0,
		'encrypt'       => false,
		'test' => true
	);

	var $encryption_default = array(
		'cert_id'       => '',                              // Certificate ID (gotten after certificate uploaded to paypal)
		'key_file'      => '',                              // Absolute path to Private Key File
		'cert_file'     => '',                              // Absolute path to Public Certificate file
		'paypal_cert_file' => '',                           // Absolute path to Paypal certificate file
		'openssl'       => '/usr/bin/openssl',              // OpenSSL location
		'bn'            => 'cakephp_paypal-ipn-plugin',     // Build Notation
	);

	var $encryption_test = array(
		'cert_id'       => '',                              // Certificate ID (gotten after certificate uploaded to paypal)
		'key_file'      => '',                              // Absolute path to Private Key File
		'cert_file'     => '',                              // Absolute path to Public Certificate file
		'paypal_cert_file' => '',                           // Absolute path to Paypal certificate file
		'openssl'       => '/usr/bin/openssl',              // OpenSSL location
		'bn'            => 'cakephp_paypal-ipn-plugin',     // Build Notation
	);

}
