<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/login', ['admin' => false, 'controller' => 'users', 'action' => 'login']);
	Router::connect('/cancel_login', ['admin' => false, 'controller' => 'users', 'action' => 'cancel_login']);
	Router::connect('/request-password-reset', ['admin' => false, 'controller' => 'users', 'action' => 'request_password_reset']);
	Router::connect('/reset-password/*', ['admin' => false, 'controller' => 'users', 'action' => 'reset_password']);
	Router::connect('/profile', ['admin' => false, 'controller' => 'users', 'action' => 'view']);
	Router::connect('/logout', ['admin' => false, 'controller' => 'users', 'action' => 'logout']);


	// The home page is actually the quote page
	Router::connect('/', ['controller' => 'orders', 'action' => 'quote']);

	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	Router::connect('/dashboard', array('admin' => true, 'controller' => 'users', 'action' => 'dashboard'));

	// A page that can be called from ajax that displays a page with custom alert info
	Router::connect('/ajax_alert', array('controller' => 'pages', 'action' => 'display', 'alert'));

	// Some of these routes are duplicated in short and long form
	Router::connect('/privacy-policy', array('controller' => 'pages', 'action' => 'display', 'privacy'));
	Router::connect('/privacy', array('controller' => 'pages', 'action' => 'display', 'privacy'));
	Router::connect('/cookie-policy', array('controller' => 'pages', 'action' => 'display', 'cookies'));
	Router::connect('/cookies', array('controller' => 'pages', 'action' => 'display', 'cookies'));
	Router::connect('/terms-conditions', array('controller' => 'pages', 'action' => 'display', 'terms'));
	Router::connect('/terms', array('controller' => 'pages', 'action' => 'display', 'terms'));
	Router::connect('/sitemap', array('controller' => 'pages', 'action' => 'display', 'sitemap'));
	Router::connect('/about', array('controller' => 'pages', 'action' => 'display', 'about'));
	Router::connect('/contact', array('controller' => 'pages', 'action' => 'display', 'contact'));
	Router::connect('/help', array('controller' => 'pages', 'action' => 'display', 'help'));
	Router::connect('/faqs', array('controller' => 'pages', 'action' => 'display', 'faqs'));
	Router::connect('/pdf', array('controller' => 'pages', 'action' => 'display', 'pdf'));

	Router::connect('/doc-wizard', ['controller' => 'orders', 'action' => 'wizard']);

	Router::connect('/quote', ['controller' => 'orders', 'action' => 'quote']);
	Router::connect('/wp_quote', ['controller' => 'orders', 'action' => 'wp_quote']);

	Router::connect('/add-another-document', ['controller' => 'orders', 'action' => 'add_order_item']);
	Router::connect('/remove-document/*', ['controller' => 'orders', 'action' => 'remove_order_item']);

	Router::connect('/upload', ['controller' => 'orders', 'action' => 'upload']);

	Router::connect('/process-files', ['controller' => 'orders', 'action' => 'process_files']);

	Router::connect('/resize-pages/*', ['controller' => 'order_item_file_pages', 'action' => 'resize']);
	Router::connect('/crop-pages/*', ['controller' => 'order_item_file_pages', 'action' => 'crop']);

	Router::connect('/configure/*', ['controller' => 'orders', 'action' => 'configure']);

	Router::connect('/checkout', ['controller' => 'orders', 'action' => 'checkout']);

	Router::connect('/pay-now', ['controller' => 'orders', 'action' => 'pay_now']);

	Router::connect('/paid', ['controller' => 'orders', 'action' => 'paid']);

	Router::connect('/cover-designer', ['controller' => 'orders', 'action' => 'cover_designer']);
	Router::connect('/add-cover-files/*', ['controller' => 'orders', 'action' => 'add_cover_files']);
	Router::connect('/update-printed-cover/*', ['controller' => 'orders', 'action' => 'update_printed_cover']);
	Router::connect('/delete-printed-cover', ['controller' => 'orders', 'action' => 'delete_printed_cover']);

	Router::connect(
		'/paypal_ipn/process',
		array(
			'controller' => 'paypal_payments',
			'action' => 'process'
		)
	);


/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
