<?php

	$config = [
		'callas' => [
			'method' => 'server',
			'api' => [
				'path' => DS . 'etc' . DS . 'callas' . DS,
				'path_local' => DS . 'Volumes' . DS . 'Macintosh HD' . DS . 'Applications' . DS . 'callas pdfToolbox Server 8' . DS . 'cli' . DS,
				'despatch' => false,
				'despatcher_ip' => '172.31.29.211:1200',
				'profiles_folder' => 'profiles',
				'sleepTime' => 2,
				'retryCount' => 10,
				'preview_limit' => 1000,
				'preview_resolution' => 36
			],
			'server' => [
				'url' => 'http://callas.pagevisions.co.uk/',
				'function_post' => 'dox_process.php',
				'folder_pdf_info' => 'pdf_details/Info/',
				'folder_png' => 'create_pngs/Success',
				'actions' => 'pdf_details,create_pngs',
				'sleepTime' => 6,
				'retryCount' => 10,
				'folder_extract_first' => 'extract_first_page/Success',
				'folder_extract_last' => 'extract_last_page/Success'
			]
		]
	];
