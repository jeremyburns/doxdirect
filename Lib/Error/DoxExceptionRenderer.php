<?php
App::uses('ExceptionRenderer', 'Error');

class DoxExceptionRenderer extends ExceptionRenderer {

	protected function _outputMessage($template) {
		$this->controller->layout = 'error';
		parent::_outputMessage($template);
  	}

}