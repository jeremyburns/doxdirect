<?php
App::uses('AppController', 'Controller');
/**
 * PaperSizes Controller
 *
 * @property PaperSize $PaperSize
 * @property PaginatorComponent $Paginator
 */
class PaperSizesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('paperSizes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PaperSize->exists($id)) {
			throw new NotFoundException(__('Invalid paper size'));
		}
		$options = array('conditions' => array('PaperSize.' . $this->PaperSize->primaryKey => $id));
		$this->set('paperSize', $this->PaperSize->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PaperSize->create();
			if ($this->PaperSize->save($this->request->data)) {
				$this->Flash->success(
					__('The paper size has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The paper size could not be saved. Please, try again.')
				);
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PaperSize->exists($id)) {
			throw new NotFoundException(__('Invalid paper size'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PaperSize->save($this->request->data)) {
				$this->Flash->success(
					__('The paper size has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The paper size could not be saved. Please, try again.')
				);
			}
		} else {
			$options = array('conditions' => array('PaperSize.' . $this->PaperSize->primaryKey => $id));
			$this->request->data = $this->PaperSize->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PaperSize->id = $id;
		if (!$this->PaperSize->exists()) {
			throw new NotFoundException(__('Invalid paper size'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PaperSize->delete()) {
			$this->Flash->success(
				__('The paper size has been deleted.')
			);
		} else {
			$this->Flash->error(
				__('The paper size could not be deleted. Please, try again.')
			);
		}
		return $this->redirect(array('action' => 'index'));
	}
}
