<?php
App::uses('AppController', 'Controller');
/**
 * Orders Controller
 *
 * @property Order $Order
 * @property PaginatorComponent $Paginator
 */
class OrdersController extends AppController {

	public function beforeFilter() {

		$this->Auth->allow(
			'add_order_item',
			'apply_promo_code',
			'cartDetails',
			'checkout',
			'complete_previews',
			'configure',
			'cover_designer',
			'add_cover_files',
			'update_printed_cover',
			'delete_printed_cover',
			'currentOrderId',
			'order_item_files',
			'order_summary',
			'order_progress',
			'pay_now',
			'paid',
			'process_callas',
			'process_files',
			'quote',
			'wp_quote',
			'edit',
			'remove_order_item',
			'remove_promo_code',
			'reset_quote',
			'resume',
			'review',
			'set_order_item_id',
			'update_cart',
			'upload'
		);

		$showProductsInNavigation = false;
		$showResetQuoteButton = Configure::read('debug') > 0;

		$this->set(compact(
			'showProductsInNavigation',
			'showResetQuoteButton'
		));

		parent::beforeFilter();

	}

	private function trackProgress($orderStatusId = null) {

		if (!$orderStatusId) {
			return;
		}

		if ($this->Session->check('Order.id')) {

			$this->Session->write('Order.order_status_id', $orderStatusId);

			$this->Order->setStatus(
				$orderStatusId,
				$this->Session->read('Order.id')
			);

		}

		return;

	}

	public function reset_quote($clearPace = false) {

		if ($this->Session->check('Order.id')) {
			Cache::delete($this->Session->read('Order.id'), 'orders');
		}

		// Kill the session
		$this->Session->destroy();

		if ($clearPace) {

			Cache::clear(false, 'pace');

		} else {

			// Find all Cache entries in the order group
			$configs = Cache::groupConfigs('order');

			// Loop through them and clear them
			foreach ($configs['order'] as $config) {
				Cache::clearGroup('order', $config);
			}

		}

		return $this->redirect('/');

	}

	/**
	 * This function takes the user back to the current spot in their order, if there is one.
	 * Else it goes back to the home page
	 * @param  boolean [$return          = false] When true, returns the redirect path rather than actually redirecting
	 * @return array   The url to redirect to, if return is true
	 */
	public function resume($return = false) {

		return $this->goBackToOrder($return);

	}

/**
 * currentOrder method
 *
 * @return array - the order from session. Creates a default order if there isn't one in the session
 */
	public function currentOrder($options = []) {

		// invalidStates are the states where an order is required
		// If we go to one of these pages without an order we go back to the quote page
		$invalidStates = [
			'process_files',
			'configure',
			'checkout',
			'pay_now'
		];

		$defaults = [
			'referer' => null,
			'refresh' => false,
			'returnFullOrder' => false
		];

		$options = array_merge(
			$defaults,
			$options
		);

		extract($options);

		// Is there an order id in session?
		$orderId = $this->Session->check('Order.id')
			? $this->Session->read('Order.id')
			: null
		;


//		if (!$this->Session->check('Order.id')) {
//
//			// Create a default order
//			$order = $this->Order->defaultQuote();
//
//		} else {
//
//			$order = $this->Session->read('Order');
//
//		}

		if (!$orderId) {

			// Looks like the session has expired

			if ($referer && in_array($referer, $invalidStates)) {

				$this->Flash->info(
					'Your session has expired.'
				);

				return $this->reset_quote();

			}

		}

		$order = $this->Order->getOrder($orderId, $options);


		// Save it to the session
//		$this->storeOrderInSession($order);

		// return it
		return $order;

	}


/**
 * order_item_files method
 *
 * @return array - the uploaded files from session if they exists. Redirects to the upload page if not
 */
	public function order_item_files($orderItemId = null, $redirect = false) {

		$order = $this->currentOrder(['refresh' => true]);

		if (!$order && $redirect) {
			// No order and we can redirect - redirect to the upload page
			return $this->redirect(['admin' => false, 'controller' => 'orders', 'action' => 'upload']);
		}

		$orientations = $this->Order->OrderItem->OrderItemFile->OrderItemFilePage->orientations;

		$this->set(compact('order', 'orientations'));

		$this->render('/Elements/orders/order_items/order_item_files/list');

	}

/**
 * order_summary method
 *
 * @return array - renders a panel of the current order summary
 */
	public function order_summary() {

		$order = $this->currentOrder();

		$this->set('order', $order);

		$this->render('/Elements/orders/summary/print_summary');

	}

/**
 * order_progress method
 *
 * @return array - renders a panel of the current order progress
 */
	public function order_progress() {

		$order = $this->currentOrder();

		$this->set('order', $order);

		$this->render('/Elements/orders/summary/progress');

	}

	public function cartDetails() {

		return $this->Order->cartDetails($this->currentOrderId());

	}

	public function update_cart() {

		$referer = $this->referer();

		$this->set('cart', $this->cartDetails()); //, true);

		if ($this->request->is('ajax')) {
			$this->render('/Elements/orders/checkout/header_cart');
		} else {
			return $this->redirect($referer);
		}

	}

	public function set_order_item_id($orderItemId = null) {

		$referer = $this->referer();

		$this->orderKeysSetModel('OrderItem', ['id' => $orderItemId]);

		return $this->redirect($referer);

	}

	public function orderKeysStart($orderId = null) {

		if (!$orderId) {
			$orderId = $this->currentOrderId();
		}

		$orderKeys = $this->Order->orderKeysStart($this->currentOrderId());

		$this->Session->write('OrderKeys', $orderKeys);

		return $orderKeys;

	}

	public function orderKeysSetModel($modelName = null, $newValue = []) {

		if (!$modelName || !$newValue) {
			return false;
		}

		$orderKeysCurrent = $this->orderKeysCurrent();

		if (!$orderKeysCurrent) {
			$orderKeysCurrent = $this->orderKeysStart();
		}

		$orderKeys = $this->Order->orderKeysSetModel(
			$this->orderKeysCurrent(),
			$modelName,
			$newValue
		);

		$this->Session->write('OrderKeys', $orderKeys);

		return $orderKeys;

/*
		if (!$orderItemId && isset($this->request->data['Order']['order_item_id'])) {

			$orderItemId = $this->request->data['Order']['order_item_id'];

		}

		$this->setOrderKeys(array('OrderItemId' => $orderItemId));
*/

	}

/**
	This function keeps track of which order item and order item file the user is currently viewing/managing.
*/

	public function orderKeysSet() {

		$orderId = $this->currentOrderId();

		if (!$orderId) {
			return [];
		}

// 		orderKeysSetModel($currentValues = array(), $modelName = null, $newValues = array())
		$orderKeys = $this->Order->orderKeysSet(
			$orderId,
			$this->orderKeysCurrent(),
			$newOrderKeys
		);

		$this->Session->write('OrderKeys', $orderKeys);

		return $orderKeys;

	}

	public function orderKeysCurrentId($modelName = null) {

		if (!$modelName) {
			return null;
		}

		if ($this->Session->check('OrderKeys.' . $modelName . '.id')) {

			return $this->Session->read('OrderKeys.' . $modelName . '.id');

		}

		if (!empty($this->request->data[$modelName]['id'])) {

			$id = $this->request->data[$modelName]['id'];

			$orderKeys = $this->Order->orderKeysSet(
				$this->orderKeysCurrent(),
				$modelName,
				['id' => $id]
			);

			$this->Session->write('OrderKeys', $orderKeys);

			return $id;

		}

		$orderKeys = $this->Order->orderKeysStart($this->currentOrderId());

		if (!$orderKeys) {

			return null;

		}

		$this->Session->write('OrderKeys', $orderKeys);

		$id = $this->Order->orderKeysGetId($orderKeys, $modelName);

		return $id;

	}

	public function currentOrderItemId($order = []) {

		if ($this->Session->check('OrderKeys.OrderItem.id')) {

			return $this->Session->read('OrderKeys.OrderItem.id');

		}

		$order = $this->currentOrder();

		$orderItemId = null;

		if (!empty($order['OrderItem'])) {

			reset($order['OrderItem']);

			if (!empty($order['OrderItem'][0]['id'])) {

				$orderItemId = $order['OrderItem'][0]['id'];

			}

		}

		$this->orderKeysSetModel('OrderItem', ['id' => $orderItemId]);

		return $orderItemId;

	}

	public function addOrderItem() {

		$orderId = $this->currentOrderId();

		if (!$orderId) {

			return false;

		}

		$orderItemId = $this->Order->OrderItem->addNewOrderItemToOrder($orderId);

		if (!$orderItemId) {
			return $this->redirect('/');
		}

		$order = $this->Order->getOrder($orderId);

		$this->storeOrderInSession($order);

		$this->orderKeysSetModel('OrderItem', ['id' => $orderItemId]);

		return true;

	}

	public function add_order_item() {

		$this->addOrderItem();

		return $this->redirect('/');

	}

	public function remove_order_item($orderItemId = null) {

		// Record where we came from
		$referer = $this->referer();

		$result = $this->Order->removeOrderItem($orderItemId);

		if ($result['success']) {

			$this->storeOrderInSession($result['order']);

		}

		$order = $this->Order->getOrder($this->currentOrderId());

		$hasOrderItems = !empty($order['OrderItem']);

		// If it's called from ajax just response with the response
		if ($this->request->is('ajax')) {
			$result['has_order_items'] = $hasOrderItems;
			return json_encode($result);
		} else {

			if (!$hasOrderItems) {
				return $this->redirect('/');
			}
			// Just go back to where we came from
			return $this->redirect($referer);
		}

	}

	public function setCurrentUser() {

		// If there's a logged in user and the user id of the quote is unset, make it the current user
		if ($this->request->data && empty($this->request->data['User']['id']) && $this->Auth->user('id')) {
			$this->request->data['Order']['user_id'] = $this->Auth->user('id');
		}

	}


/**
 * Quote method
 *
 * @return void
 */
	public function wp_quote($orderId = null) {

		$orderItemId = null;

		if ($orderId) {

			$order = $this->Order->getOrder($orderId);

			$this->storeOrderInSession($order);

		}

		$renderForWordpress = true;

		if (($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data)) {

			$this->Session->delete('FixFailures');

			$this->setCurrentUser();

			$order = $this->Order->quote($this->request->data);

			if (isset($order['success']) && !$order['success']) {
				return $this->redirect([
					'admin' => false,
					'controller' => 'orders',
					'action' => 'wp_quote'
				]);
			}

			$orderId = $order['Order']['id'];
			$orderItemId = $this->Order->OrderItem->id;

		} else {

			$order = $this->currentOrder(['referer' => 'quote']);

		}

		$this->storeOrderInSession($order);

		$sameDayTurnaroundCutOffHour = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffHour();
		$sameDayTurnaroundCutOffMinute = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffMinute();

		if ($this->request->is('ajax')) {

			$html = $this->ajaxRenderElement(
				'/Elements/orders/quote/quote_result',
				[
					'order' => $order,
					'orderItemId' => $orderItemId,
					'renderForWordpress' => $renderForWordpress,
					'sameDayTurnaroundCutOffHour' => $sameDayTurnaroundCutOffHour,
					'sameDayTurnaroundCutOffMinute' => $sameDayTurnaroundCutOffMinute
				]
			);

//			$view = new View($this, false);
//			$view->set(compact(
//				'order',
//				'orderItemId',
//				'renderForWordpress',
//				'sameDayTurnaroundCutOffHour',
//				'sameDayTurnaroundCutOffMinute'
//			));
//			$html = $view->render('/Elements/orders/quote/quote_result');

			$result = [
				'order' => $order,
				'html' => $html
			];

			return json_encode($result);

		}

		$this->request->data = $order;

		$cart = $this->cartDetails();

		extract ($this->Order->quoteParameters());

		$productMap = json_encode($this->Order->OrderItem->Product->productMap());

		$title_for_layout = 'Online document printing services UK | Digital Printing | Doxdirect';

		$orderItemKey = 0;

		$js = [
			'orders/quote.min'
		];

		$jsVariables = [
			'productMap' => $productMap
		];

		$this->set(compact(
			'title_for_layout',
			'js',
			'jsVariables',
			'currentStep',
			'cart',
			'orderKeys',
			'orderItemId',
			'products',
			'sides',
			'clicks',
			'drillingTypes',
			'deliveryOptions',
			'turnaroundOptions',
			'order',
			'orderItemKey',
			'orderItemId',
			'renderForWordpress',
			'sameDayTurnaroundCutOffHour',
			'sameDayTurnaroundCutOffMinute'
		));

		Configure::write('debug', 0);

		$this->layout = 'content_only';

	}

/**
 * Quote method
 *
 * @return void
 */
	public function quote($orderId = null) {

		$this->trackProgress($this->action);

		$orderItemId = null;

		// If an orderId is passed in, get and store that order
		if ($orderId) {

			$order = $this->Order->getOrder($orderId);

			$this->storeOrderInSession($order);

		}

		$renderForWordpress = false;

		if ($this->request->is(['post', 'put']) && !empty($this->request->data)) {

			$this->Session->delete('FixFailures');

			$this->setCurrentUser();

			$quote = $this->Order->quote($this->request->data);

			if (empty($quote['success'])) {

				$order = $this->request->data;
				$order['success'] = false;

				if (!empty($quote['message'])) {
					$order['message'] = $quote['message'];
				}

			} else {

				$order = $quote;

			}

			$orderId = $order['Order']['id'];
			$orderItemId = $order['OrderItem'][0]['id'];; // $this->Order->OrderItem->id;

		} else {

			$order = $this->currentOrder(['referer' => $this->action]);
			$order['success'] = true;

		}
//		die(debug($order));
		$this->storeOrderInSession($order);

		$orderKeys = $this->orderKeysCurrent();

		if (empty($orderKeys['Order']['id'])) {
			$orderKeys = $this->orderKeysStart($orderId);
		}

		if ($orderItemId) {
			$orderKeys = $this->orderKeysSetModel('OrderItem', ['id' => $orderItemId]);
		}

		$sameDayTurnaroundCutOffHour = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffHour();
		$sameDayTurnaroundCutOffMinute = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffMinute();

		if ($this->request->is('ajax')) {

			$html = $this->ajaxRenderElement(
				'/Elements/orders/quote/quote_result',
				[
					'order' => $order,
					'orderItemId' => $orderItemId,
					'renderForWordpress' => $renderForWordpress,
					'sameDayTurnaroundCutOffHour' => $sameDayTurnaroundCutOffHour,
					'sameDayTurnaroundCutOffMinute' => $sameDayTurnaroundCutOffMinute
				]
			);

//			$view = new View($this, false);
//			$view->set(compact(
//				'order',
//				'orderItemId',
//				'renderForWordpress',
//				'sameDayTurnaroundCutOffHour',
//				'sameDayTurnaroundCutOffMinute'
//			));
//			$html = $view->render('/Elements/orders/quote/quote_result');

			$result = [
				'order' => $order,
				'html' => $html
			];

			return json_encode($result);

		}

		$this->request->data = $order;

		$cart = $this->cartDetails();

		extract ($this->Order->quoteParameters());

		$title_for_layout = 'Online document printing services UK | Digital Printing | Doxdirect';

		$orderItemKey = 0;

		$js = [
			'orders/quote.min',
			'global/reviews_widget.min'
		];

		$productMap = json_encode($this->Order->OrderItem->Product->productMap());

		$jsVariables = [
			'productMap' => $productMap
		];

		$jsScripts = [
			'//dash.reviews.co.uk/packages/widget/doxdirect/2015-05-20.08.05.05-320.js'
		];

		$this->set(compact(
			'title_for_layout',
			'js',
			'jsVariables',
			'jsScripts',
			'currentStep',
			'cart',
			'orderKeys',
			'orderItemId',
			'products',
			'media',
			'sides',
			'clicks',
			'drillingTypes',
			'deliveryOptions',
			'turnaroundOptions',
			'sameDayTurnaroundCutOffHour',
			'sameDayTurnaroundCutOffMinute',
			'order',
			'orderItemKey',
			'orderItemId',
			'renderForWordpress'
		));

	}

	public function edit($orderItemId = null) {

//		if (!$orderItemId) {
//			return $this->redirect('/');
//		}
//
//		$this->Order->OrderItem->id = $orderItemId;
//
//		if (!$this->Order->OrderItem->exists()) {
//			return $this->redirect('/');
//		}
//
//		if ($this->request->is('ajax')) {
//			$this->render('/Elements/orders/quote/calculator_form');
//		}

		return $this->redirect('/');

	}
/**
 * upload method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function upload() {

		$this->trackProgress($this->action);

		$order = $this->currentOrder(['referer' => $this->action]);

		$this->storeOrderInSession($order);

		$this->request->data = $order;

		// Which order item are we working on?
		$orderItemId = $this->currentOrderItemId($order);

		if (!empty($_FILES['files']['name'][0])) {

			$this->Session->delete('FixFailures');

			$response = $this->Order->OrderItem->OrderItemFile->uploadFiles(
				$order,
				$orderItemId,
				$_FILES
			);

			if (!$response['success']) {

				if ($this->request->is('ajax')) {

					return json_encode($response);

				} else {

					$this->Flash->error(
						$response['message']
					);

					if ($response['code'] == 'no-order-id') {

						return $this->redirect('/');

					} else {

						return $this->redirect([
							'admin' => false,
							'controller' => 'orders',
							'action' => 'upload'
						]);

					}

				}

			}

			// Get the number of files uploaded
			$orderItemFileCount = count($response['orderItemFileIds']) - 1;

			// Get the last file uploaded
			$orderItemFileId = $response['orderItemFileIds'][$orderItemFileCount];

			// Make the last file the one we are working with
			$this->orderKeysSetModel('OrderItemFile', ['id' => $orderItemFileId]);

			$this->Flash->info(
				'Your quote has been updated',
				['key' => 'quote-updated']
			);

			// Refresh the order and store it in session
			$order = $this->refreshOrder();

			if ($this->request->is('ajax')) {

				// If we are doing this from ajax return the response

				$response['html'] = [
					'orderItemSummary' => $this->ajaxRenderElement(
						'/Elements/orders/summary/print_summary',
						['order' => $order]
					),
					'orderProgress' => $this->ajaxRenderElement(
						'/Elements/orders/summary/progress',
						['order' => $order]
					)
				];

				return json_encode($response);

			} else {

				// Redirect back here to clear the $_FILES array so they don't keep uploading on page refresh
				return $this->redirect(
					[
						'admin' => false,
						'controller' => 'orders',
						'action' => 'upload'
					]
				);

			}

		}

		if ($this->request->is('ajax')) {

			// If we are here, we have submitted the upload form via ajax but there are no files
			$response = [
				'success' => false,
				'code' => 'no-files',
				'message' => 'Please select one or more files to upload.'
			];

			return json_encode($response);

		}

		$cart = $this->cartDetails();

		$orderKeys = $this->orderKeysCurrent();

		extract ($this->Order->quoteParameters());

		$currentStep = 1;

		$orderItems = $this->Order->OrderItem->listOrderItems($order);

		$productMap = json_encode($this->Order->OrderItem->Product->productMap());

		$orientations = $this->Order->OrderItem->OrderItemFile->OrderItemFilePage->orientations;

		$sameDayTurnaroundCutOffHour = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffHour();
		$sameDayTurnaroundCutOffMinute = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffMinute();

		$maximumFileUploadSize = $this->Order->getConfigurationValue('maximum_upload_file_size');

		$jsVariables = [
			'productMap' => $productMap
		];

		$js = [
			'orders/upload.min'
		];

		$this->set(compact(
			'title_for_layout',
			'js',
			'jsVariables',
			'currentStep',
			'cart',
			'orderKeys',
			'products',
			'media',
			'sides',
			'clicks',
			'drillingTypes',
			'deliveryOptions',
			'turnaroundOptions',
			'sameDayTurnaroundCutOffHour',
			'sameDayTurnaroundCutOffMinute',
			'orientations',
			'uploadResult',
			'order',
			'orderItems',
			'orderItemKey',
			'orderItemId',
			'maximumFileUploadSize'
		));

	}

/**
 * complete_previews method
 *
 * This function triggers a call to Callas to complete the preview creation process
 * if they weren't all requested when the file was originally pushed to Callas
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function complete_previews() {

		$this->autoRender = false;

		$order = $this->currentOrder(['referer' => $this->action]);

		$result = $this->Order->completePreviews($order);

		$order = $this->Order->getOrder($order['Order']['id']);

		$this->storeOrderInSession($order);

	}

	public function getOrderFixFailures($orderId = null, $refresh = false) {

		if (!$orderId) {
			$orderId = $this->currentOrderId();
		}

		if (!$this->Session->check('FixFailures') || $refresh) {

			$fixFailures = $this->Order->getOrderFixFailures($orderId);

			$this->Session->write(
				'FixFailures',
				$fixFailures
			);

		}

		return $this->Session->read('FixFailures');

	}

	public function process_callas() {

		$orderId = $this->currentOrderrId();
		// This functiona analyses the order, order items, order item files and order item file pages
		$response = $this->Order->processCallas($orderId);

	}

/**
 * process_file method
 *
 * A page that retrieves file information from Callas and presents any decisions to the user
 * before continuing to configure
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function process_files() {

		$this->trackProgress($this->action);

		$order = $this->currentOrder(['referer' => $this->action]);

		$response = $this->Order->getCallasInfo($order);

		if (!$response['success']) {

			$this->Flash->error(
				'There was a problem processing your uploaded file. Please try again.'
			);

			return $this->redirect([
				'admin' => false,
				'controller' => 'orders',
				'action' => 'upload'
			]);

		}

		// This function analyses the order, order items, order item files and order item file pages
		$response = $this->Order->processCallas($order['Order']['id']);

		$order = $this->Order->getOrder($order['Order']['id']);

		$this->storeOrderInSession($order);

		$fixFailures = $this->getOrderFixFailures($order['Order']['id']);

		if ($fixFailures['success']) {

			if ($this->request->is('ajax')) {

				return true;
			} else {

				return $this->redirect([
					'admin' => false,
					'controller' => 'orders',
					'action' => 'configure'
				]);

			}

		}

		return $this->redirect([
			'admin' => false,
			'controller' => 'orders',
			'action' => 'configure'
		]);

	}


/**
 * configure method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function configure($orderItemFileId = null, $pageNumber = null) {

		$this->trackProgress($this->action);

		$order = $this->currentOrder(['referer' => $this->action, 'refresh' => true]);

		if (empty($order['Order']['id'])) {
			// Looks like the session has expired
			$this->Flash->info(
				'Your session has expired.'
			);
			return $this->redirect('/');
		}

		$orderId = $order['Order']['id'];

		// Which order item are we working on?
		$orderItemId = $this->currentOrderItemId();

		// Let's assume we are ready to preview the document
		$readyForPreview = true;

		// $fixFailures will have a 'success' key set to true if all tests pass, else will have a 'failures' key with a key for each failed test
		$fixFailures = $this->getOrderFixFailures($orderId, false);

		$readyForPreview = $fixFailures['success'];

		if (!$readyForPreview) {

			// We're not ready to preview the document

			if (!empty($fixFailures['failure']['message'])) {
				$this->Flash->error(
					$fixFailures['failure']['message']
				);
			}

			if (!empty($fixFailures['failure']['action'])) {

				$action = $fixFailures['failure']['action'];

				if ($action !== $this->action) {

					$this->Order->setStatus($action, $orderId);

					return $this->redirect([
						'admin' => false,
						'controller' => 'orders',
						'action' => $action
					]);

				}

			}

		} else {

			$orderItemPages = Hash::extract($order['OrderItem'], '{n}.OrderItemFile.{n}.OrderItemFilePage.{n}');

			$pageCount = count($orderItemPages);

		}

		$this->request->data = $order;

		$currentStep = 2;

		$cart = $this->cartDetails();

		$orderKeys = $this->orderKeysCurrent();

		$orderItemKey = !empty($orderKeys['OrderItem']['key'])
			? $orderKeys['OrderItem']['key']
			: 0
		;

		$orderItem = $order['OrderItem'][$orderItemKey];

		// Specific hardback and paperback book cover options - this needs to be done before we retrieve the OrderItem object
		if ($orderItem['Product']['BindingType']['use_cover_designer']) {

			// Check to see if we already have options set - if not, set some defaults
			if(empty($orderItem['printed_cover_job_id']) && empty($orderItem['printed_cover_front_options']) && empty($orderItem['printed_cover_back_options'])) {

				$this->Order->OrderItem->updatePrintedCoverOptions($orderItemId, 'blank_cover', 'blank_cover');
				$order = $this->currentOrder(['referer' => $this->action, 'refresh' => true]);

			}

		}

		$orderItem = $order['OrderItem'][$orderItemKey];

		$tabs = $order['OrderItem'][$orderItemKey]['OrderItemTab'];

		$orientations = $this->Order->OrderItem->OrderItemFile->OrderItemFilePage->orientations;

		$sameDayTurnaroundCutOffHour = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffHour();
		$sameDayTurnaroundCutOffMinute = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffMinute();

		if ($this->Session->check('Error')) {

			$errors = $this->Session->read('Error');

			if (!$this->validationErrors) {

				$this->validationErrors = $errors;

			} else {

				$this->validationErrors = array_merge(
					$this->validationErrors,
					$errors
				);

			}

			$this->Session->delete('Error');

		} else {

			$errors = [];

		}

		extract ($this->Order->quoteParameters());

		$js = [
			'orders/configure.min',
			'previewer/previewer.min'
		];

		if (!empty($orderItemPages)) {
			$jsVariables = [
				'orderItemPages' => json_encode($orderItemPages, JSON_UNESCAPED_UNICODE)
			];
		}

		$this->set(compact(
			'js',
			'jsVariables',
			'currentStep',
			'cart',
			'orderKeys',
			'tabs',
			'products',
			'media',
			'sides',
			'colours',
			'clicks',
			'orientations',
			'deliveryOptions',
			'turnaroundOptions',
			'sameDayTurnaroundCutOffHour',
			'sameDayTurnaroundCutOffMinute',
			'order',
			'orderItem',
			'pageCount',
			'orderItemPages',
			'orderItemFileId',
			'pageNumber',
			'fixFailures',
			'errors',
			'orderItemKey',
			'tmpOrderItem'
		));

		// If the user has to do something, render the configure action view
		if (!empty($fixFailures) && !$fixFailures['success']) {

			$failure = $fixFailures['failure'];

			$this->set('failure', $failure);

			$this->render('order_item_fixes');

		}

	}


/**
 * cover_designer method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cover_designer() {

		$currentStep = 2;
		$cart = $this->cartDetails();
		$order = $this->currentOrder();
		$orderItemId = $this->currentOrderItemId();
		$orderKeys = $this->orderKeysCurrent();

		$orderItemKey = !empty($orderKeys['OrderItem']['key'])
			? $orderKeys['OrderItem']['key']
			: 0
		;

		$orderItem = $order['OrderItem'][$orderItemKey];

		if (!$orderItem['printed_cover_job_id']) {

			$printedCoverJob = $this->Order->OrderItem->createNewCoverDocument($orderItemId);

			if ($printedCoverJob['success']) {
				$printedCoverJobId = $printedCoverJob['id'];
			} else {

				$this->Flash->error(
					'There was a problem setting up your cover. Please try again. If this continues please contact support.'
				);

				$this->redirect([
					'admin' => false,
					'controller' => 'orders',
					'action' => 'configure'
				]);

			}

			$order = $this->currentOrder([
				'referer' => $this->action,
				'refresh' => true
			]);

		} else {

			$printedCoverJobId = $orderItem['printed_cover_job_id'];

		}

		$js = [
			'orders/cover_designer.min'
		];

		$this->set(compact(
			'currentStep',
			'cart',
			'orderItem',
			'printedCoverJobId',
			'js'
		));

	}

	public function add_cover_files() {

		if(!$this->request->is('ajax')) {

			return false;

		}
		else {

			$order = $this->currentOrder();
			$orderItemId = $this->currentOrderItemId();
			$orderKeys = $this->orderKeysCurrent();

			$orderItemKey = !empty($orderKeys['OrderItem']['key'])
				? $orderKeys['OrderItem']['key']
				: 0
			;

			$orderItem = $order['OrderItem'][$orderItemKey];

			$covers = $this->Order->OrderItem->getCallasCovers($orderItemId, $orderItem['printed_cover_front_options'], $orderItem['printed_cover_back_options']);

			if($covers) {

				return json_encode(["status" => "done"]);

			}

		}

	}

	public function update_printed_cover($frontCoverOptions = null, $backCoverOptions = null) {

		if(!$this->request->is('ajax')) {

			// If it's not AJAX, we're not interested
			return false;

		}
		else {

			$order = $this->currentOrder();
			$orderItemId = $this->currentOrderItemId();
			$this->Order->OrderItem->updatePrintedCoverOptions($orderItemId, $frontCoverOptions, $backCoverOptions);
			$order = $this->currentOrder();

		}

	}

	public function delete_printed_cover() {

		if(!$this->request->is('ajax')) {

			// If it's not AJAX, we're not interested
			return false;

		}
		else {

			$order = $this->currentOrder();
			$orderItemId = $this->currentOrderItemId();
			$this->Order->OrderItem->updatePrintedCoverJobId($orderItemId, null);
			$order = $this->currentOrder(['refresh' => true]);

		}

	}

/**
 * checkout method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function checkout() {

		$this->trackProgress($this->action);

		$loggedInUserId = $this->Auth->user('id');

		if ($this->request->data) {

			$result = $this->Order->checkout($this->request->data, $loggedInUserId);

			if (!$result['success']) {

				$this->Flash->error(
					$result['message']
				);

				if (!empty($result['flash_key'])) {
					$this->Flash->warning(
						$result['message'],
						['key' => $result['flash_key']]
					);
				}

			} else {

				/*
				if (!$this->Auth->user() && !empty($user['id']) && $user['loginUser']) {
					$result = $this->Auth->login($user);
				}
*/
				$this->Flash->success(
					'Your details have been saved. Please enter your payment details.'
				);

				if (!empty($result['flash_key'])) {
					$this->Flash->success(
						$result['message'],
						['key' => $result['flash_key']]
					);
				}

				if (!empty($result['PromoCode']['message'])) {
					$this->Flash->info(
						$result['PromoCode']['message'],
						['key' => $result['PromoCode']['flash_key']]
					);
				}

				return $this->redirect([
					'admin' => false,
					'controller' => 'orders',
					'action' => 'pay_now'
				]);

			}

			$order = $this->currentOrder([
				'referer' => $this->action,
				'refresh' => true,
				'returnFullOrder' => true
			]);

		} else {

			$order = $this->currentOrder([
				'referer' => $this->action,
				'refresh' => true,
				'returnFullOrder' => true,
				'containUser' => true
			]);

			$this->request->data = $order;

		}

		// When checking whether an order can have same day turnaround we need to examine the products in the order items. This field aggregates those into a single variable for ease.
		$qualifiesForSameDayTurnaround = $this->Order->qualifiesForSameDayTurnaround($order);;

		$result = $this->Order->checkBlankCover($order);

		if (!$result['success']) {

			$this->Flash->error(
				$result['message']
			);

			$this->redirect([
				'admin' => false,
				'controller' => 'orders',
				'action' => 'upload'
			]);

		}

		if ($loggedInUserId) {

			$userAddresses = $this->Order->User->UserAddress->addressesAsList($loggedInUserId);

			$loggedInUser = $this->Order->User->find(
				'first',
				['conditions' => ['User.id' => $loggedInUserId]]
			);
		}

		$currentStep = 3;

		$cart = $this->cartDetails();

		$sources = $this->Order->Source->findActive();

		$countries = $this->Order->ShippingAddress->Country->findActive();

		$turnaroundOptions = $this->Order->TurnaroundOption->find('list');
		$deliveryOptions = $this->Order->DeliveryOption->find('list');

		$defaultCountryId = $this->Order->ShippingAddress->Country->getDefault();

		$paymentMethods = $this->Order->PaymentMethod->find(
			'all',
			[
				'fields' => [
					'PaymentMethod.id',
					'PaymentMethod.code',
					'PaymentMethod.name'
				],
				'conditions' => [
					'PaymentMethod.is_active' => true
				]
			]
		);

		$sameDayTurnaroundCutOffHour = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffHour();
		$sameDayTurnaroundCutOffMinute = $this->Order->TurnaroundOption->sameDayTurnaroundCutOffMinute();

		$orderSourceLabel = $this->Order->getConfigurationValue('order_source_label');

		$js = [
			'orders/checkout.min',
			'user_addresses/manage.min'
		];

		$this->set(compact(
			'js',
			'currentStep',
			'cart',
			'sameDayTurnaroundCutOffHour',
			'sameDayTurnaroundCutOffMinute',
			'qualifiesForSameDayTurnaround',
			'orderSourceLabel',
			'sources',
			'countries',
			'defaultCountryId',
			'paymentMethods',
			'documents',
			'order',
			'loggedInUser',
			'userAddresses',
			'turnaroundOptions',
			'deliveryOptions'
		));

	}

	public function apply_promo_code() {

		if ($this->request->is('post') || $this->request->is('put') && $this->request->data) {

			if (empty($this->request->data['promo_code'])) {

				$result = [
					'success' => false,
					'code' => 'no-promo-code',
					'message' => 'Please enter a promo code.'
				];

				if ($this->request->is('ajax')) {

					return json_encode($result);

				} else {

					$this->Flash->error(
						$result['message']
					);

					return $this->redirect([
						'admin' => false,
						'controller' => 'orders',
						'action' => 'checkout'
					]);
				}

			}

			$this->request->data['PromoCode']['code'] = $this->request->data['promo_code'];
			unset($this->request->data['promo_code']);

			$orderId = $this->currentOrderId();

			$this->request->data['Order']['id'] = $orderId;

			$result = $this->Order->applyPromoCode($this->request->data);

			if (!$result['success']) {

				if ($this->request->is('ajax')) {

					return json_encode($result);

				} else {

					if (!empty($result['flash_key'])) {

						$this->Flash->warning(
							$result['message'],
							['key' => $result['flash_key']]

						);
					}

				}

			} else {

				$this->Flash->success(
					'The promo code has been applied.',
					['key' => 'promo-code']
				);

				if ($this->request->is('ajax')) {

					$orderId = $this->currentOrderId();

//					$order = $this->Order->getOrder($orderId);
					$order = $this->Order->getOrder(
						$orderId,
						[
							'includeOrderItemContains' => true,
							'containUser' => true
						]
					);

					$options = [
						'orderOptions' => [
							'allow_edit' => false,
							'show_add_link' => false
						]
					];

//					$viewPromoCode = new View($this, false);
//					$viewOrderSummary = new View($this, false);

//					$viewPromoCode->set(compact(
//						'order'
//					));
//
//					$viewOrderSummary->set(compact(
//						'order',
//						'options'
//					));
//
//					$result['html']['promo_code'] = $viewPromoCode->render('/Elements/orders/checkout/panels/promo_code');
//
//					$result['html']['order_summary'] = $viewOrderSummary->render('/Elements/orders/quote/amounts');


					$result['html']['promo_code'] = $this->ajaxRenderElement(
						'/Elements/orders/checkout/panels/promo_code',
						[
							'order' => $order
						]
					);

					$result['html']['order_summary'] = $this->ajaxRenderElement(
						'/Elements/orders/quote/amounts',
						[
							'order' => $order,
							'options' => $options
						]
					);




					return json_encode($result);

				}

				return $this->redirect([
					'admin' => false,
					'controller' => 'orders',
					'action' => 'checkout'
				]);

			}

		} else {

			$order = $this->currentOrder([
				'referer' => $this->action,
				'refresh' => true,
				'returnFullOrder' => true
			]);

			$this->request->data = $order;

		}

		$currentStep = 3;

		$cart = $this->cartDetails();

//		$js = [
//			'orders/checkout.min'
//		];

		$this->set(compact(
			'js',
			'currentStep',
			'cart',
			'documents',
			'order'
//			'loggedInUser',
		));

	}

	public function remove_promo_code() {

		$orderId = $this->currentOrderId();

		$result = $this->Order->removePromoCode($orderId);

		if ($result['success']) {
			$this->Flash->success(
				$result['message']
			);
		} else {
			$this->Flash->error(
				$result['message']
			);
		}

		if ($this->request->is('ajax')) {

			$orderId = $this->currentOrderId();

			$order = $this->Order->getOrder(
				$orderId,
				[
					'containUser' => true
				]
			);

			$options = [
				'orderOptions' => [
					'allow_edit' => false,
					'show_add_link' => false
				]
			];

//			$viewPromoCode = new View($this, false);
//			$viewOrderSummary = new View($this, false);
//			$viewPromoCode->set(compact(
//				'order'
//			));
//
//			$viewOrderSummary->set(compact(
//				'order',
//				'options'
//			));
//
//			$result['html']['promo_code'] = $viewPromoCode->render('/Elements/orders/checkout/panels/promo_code');
//
//			$result['html']['order_summary'] = $viewOrderSummary->render('/Elements/orders/quote/amounts');

			$result['html']['promo_code'] = $this->ajaxRenderElement(
				'/Elements/orders/checkout/panels/promo_code',
				[
					'order' => $order
				]
			);

			$result['html']['order_summary'] = $this->ajaxRenderElement(
				'/Elements/orders/quote/amounts',
				[
					'order' => $order,
					'options' => $options
				]
			);

			return json_encode($result);

		}

		$this->resume();

	}

/**
 * pay_now method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function pay_now() {

		$this->trackProgress($this->action);

		if (!$this->Session->check('Order.id')) {

			$this->redirect([
				'admin' => false,
				'controller' => 'orders',
				'action' => 'upload'
			]);
		}

		$cart = $this->cartDetails();

		$orderId = $this->currentOrderId();

		$order = $this->Order->findById($orderId);

		if (!$order['Order']['amount_due']) {

			$this->Flash->info(
				[
					'heading' => 'Nothing to pay!',
					'message' => 'Your order has been paid in full.'
				]
			);

			$this->redirect([
				'admin' => false,
				'controller' => 'orders',
				'action' => 'paid'
			]);

		}

		Configure::load('payment_config', 'default');

		$paymentConfig = Configure::read('payment_methods');

		$currentStep = 3;

		$paymentMethods = $this->Order->PaymentMethod->find(
			'all',
			[
				'fields' => [
					'PaymentMethod.id',
					'PaymentMethod.code',
					'PaymentMethod.name'
				],
				'conditions' => ['PaymentMethod.is_active' => true]
			]
		);

		$result = $this->Order->convertPaceQuoteToJob($orderId);

		if (!$result['success']) {

			$this->Flash->error(
				$result['message']
			);
			$this->redirect([
				'admin' => false,
				'controller' => 'orders',
				'action' => 'checkout'
			]);
		}

		$order = $this->currentOrder(
			[
				'referer' => $this->action,
				'refresh' => true,
				'returnFullOrder' => true
			]
		);

		$cart = $this->cartDetails();

		$this->set(compact(
			'currentStep',
			'paymentMethods',
			'order',
			'cart',
			'paymentConfig'
		));

	}

/**
 * paid method
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function paid() {

		$this->trackProgress($this->action);

		$loggedInUserId = $this->Auth->user('id');

		$currentStep = 3;

		if ($this->Session->check('Paid.order_id')) {
			$orderId = $this->Session->read('Paid.order_id');
		} elseif ($this->Session->check('Order.id')) {
			$orderId = $this->Session->read('Order.id');
			$this->Session->write('Paid.order_id', $orderId);
		}

		if (!$orderId) {
			$this->redirect([
				'admin' => false,
				'controller' => 'orders',
				'action' => 'quote'
			]);
		}

		$order = $this->Order->getOrder(
			$orderId,
			[
				'containUser' => true,
				'createDefault' => false,
				'includeOrderItemContains' => true
			]
		);

		$cart = $this->cartDetails();

		$this->set(compact(
			'currentStep',
			'cart',
			'order'
		));

		// Clear the order down to prevent any mishaps by going back through the site
		$this->Session->delete('Order');
		$this->Session->delete('OrderKeys');
		$this->Session->delete('FixFailures');

	}

	public function admin_search() {

		if ($this->request->is('post') && $this->request->data) {

			$paceQuoteId = $this->request->data['Order']['pace_quote_id'];
			$orderId = $this->Order->field(
				'id',
				['Order.pace_quote_id' => $paceQuoteId]
			);

			if (!$orderId) {

				$this->Flash->error(
					'Order not found'
				);
				return $this->redirect([
					'admin' => true,
					'controller' => 'orders',
					'action' => 'index'
				]);

			}

			return $this->redirect([
				'admin' => true,
				'controller' => 'orders',
				'action' => 'view',
				$orderId
			]);

		}

	}

	public function admin_index() {

		$this->Paginator->settings = [
			'contain' => [
				'OrderStatus',
				'JobStatus',
				'User'
			],
			'order' => ['Order.modified' => 'desc']
		];

		$orders = $this->Paginator->paginate('Order');

		$adminOrderSummary = $this->Order->OrderStatus->adminOrderSummary();

		$this->set(compact(
			'orders',
			'adminOrderSummary'
		));

	}

	public function admin_view($orderId = null) {

		if (!$orderId) {

			$this->Flash->error(
				'Please specify an order id.'
			);

			return $this->redirect([
				'admin' => true,
				'controller' => 'orders',
				'action' => 'index'
			]);
		}

		$order = $this->Order->adminGetOrder($orderId);

		$latestQuote = '';

		if (!empty($order['Order']['latest_quote'])) {
			$latestQuote = $order['Order']['latest_quote'];
			$latestQuote = json_decode($latestQuote, 1);
		}

		if (!$order) {

			$this->Flash->error(
				$orderId . ' is an invalid order id.'
			);

			return $this->redirect([
				'admin' => true,
				'controller' => 'orders',
				'action' => 'index'
			]);

		}

		$orientations = $this->Order->OrderItem->OrderItemFile->OrderItemFilePage->orientations;

		$this->set(compact(
			'order',
			'latestQuote',
			'orientations'
		));

	}

	public function admin_process_original_files($orderId = null) {

		if (!$orderId) {
			$this->Flash->error('That is not a valid order id.');
		}

		$this->Order->processOriginalFiles($orderId);

		$this->Flash->success('The files have been processed.');

		$this->redirect(
			[
				'admin' => true,
				'controller' => 'orders',
				'action' => 'view',
				$orderId
			]
		);

	}

	public function admin_create_job_archive($orderId = null) {

		if (!$orderId) {
			$this->Flash->error('That is not a valid order id.');
		}

		$this->Order->createJobArchive($orderId);

		$this->Flash->success('The archive has been created.');

		$this->redirect(
			[
				'admin' => true,
				'controller' => 'orders',
				'action' => 'view',
				$orderId
			]
		);

	}

}
