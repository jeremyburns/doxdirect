<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $bindingType
 * @property PaginatorComponent $Paginator
 */
class ProductsController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('products', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		$options = [
			'conditions' => ['Product.' . $this->Product->primaryKey => $id],
			'contain' => [
				'ProductBindingEdgeType.BindingEdgeType',
				'ProductBindingSideType.BindingSideType',
				'ProductBinding.Binding',
				'ProductClick.Click',
				'ProductCoverType.CoverType',
				'ProductDrillingType.DrillingType',
				'ProductMedia.Media',
				'ProductSide.Side',
				'ProductTabType.TabType'
			]
		];
		$this->set('product', $this->Product->find('first', $options));
	}

	public function admin_empty_cache() {

		$this->Product->emptyCache();

		return $this->redirect(['admin' => true, 'controller' => 'users', 'action' => 'dashboard']);

	}

	public function admin_refresh_product_map($refresh = false, $toScreen = true) {

		$this->Product->emptyCache();

		$this->Product->refreshProductMap(true, $toScreen);

		return $this->redirect(array(
			'admin' => true,
			'controller' => 'products',
			'action' => 'product_map'
		));

	}

	public function admin_product_catalogue() {

		if ($this->request->data) {

			$clearCache = $this->request->data['Product']['clear_cache'];
			$rebuildProductMap = $this->request->data['Product']['rebuild_product_map'];

			$result = $this->Product->refreshProductCatalogueFromPace($clearCache);

			$flashElement = $result['success']
				? 'success'
				: 'error'
			;

			if (!$result['success']) {

				$this->Flash->error(
					$result['message']
				);

			} else {

				if ($rebuildProductMap) {
					$result = $this->Product->refreshProductMap($clearCache, true);
				}

				if (!$result['success']) {

					$this->Flash->error(
						$result['message']
					);

				} else {

					$this->Flash->success(
						[
							'heading' => 'Product catalogue refresh complete',
							'message' => 'The product catalogue has been refreshed from Pace.'
						]
					);

				}

			}

		}

		$productCatalogue = $this->Product->getProductCatalogueFile(false);

		if ($this->request->is('ajax')) {

			$html = $this->ajaxRenderElement(
				'/Elements/product/admin/product_catalogue',
				[
					'productCatalogue' => $productCatalogue
				]
			);

			$result['productCatalogue'] = $html;

//			$view = new View($this, false);
//			$view->set(compact('productCatalogue'));
//			$result['productCatalogue'] = $view->render('/Elements/product/admin/product_catalogue', 'ajax');

			$view = new View($this, false);
			$result['flash'] = $view->render(
				'admin_flash',
				'ajax'
			);

			return json_encode($result);

		}

		$js = [
			'products/admin.min'
		];

		$this->set(compact(
			'productCatalogue',
			'js'
		));
	}

	public function admin_product_map($refresh = false) {

		$productMap = $this->Product->productMap(false);
		$this->set(compact('productMap'));

	}

}
