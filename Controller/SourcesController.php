<?php
App::uses('AppController', 'Controller');
/**
 * Sources Controller
 *
 * @property Source $Source
 * @property PaginatorComponent $Paginator
 */
class SourcesController extends AppController {

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('sources', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Source->exists($id)) {
			throw new NotFoundException(__('Invalid source'));
		}
		$options = array('conditions' => array('Source.' . $this->Source->primaryKey => $id));
		$this->set('source', $this->Source->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Source->create();
			if ($this->Source->save($this->request->data)) {
				$this->Flash->success(
					__('The source has been saved.'),
					['element' => 'success']
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The source could not be saved. Please, try again.'),
					['element' => 'error']
				);
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Source->exists($id)) {
			throw new NotFoundException(__('Invalid source'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Source->save($this->request->data)) {
				$this->Flash->success(
					__('The source has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The source could not be saved. Please, try again.')
				);
			}
		} else {
			$options = array('conditions' => array('Source.' . $this->Source->primaryKey => $id));
			$this->request->data = $this->Source->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Source->id = $id;
		if (!$this->Source->exists()) {
			throw new NotFoundException(__('Invalid source'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Source->delete()) {
			$this->Flash->success(
				__('The source has been deleted.')
			);
		} else {
			$this->Flash->error(
				__('The source could not be deleted. Please, try again.')
			);
		}
		return $this->redirect(array('action' => 'index'));
	}
}
