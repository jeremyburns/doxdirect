<?php

App::uses('AppController', 'Controller');

/**
 * OrderItems Controller
 *
 * @property OrderItem $OrderItem
 * @property PaginatorComponent $Paginator
 */
class OrderItemsController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow(
			'apply_fix'
		);
		parent::beforeFilter();
	}

	public function apply_fix() {

		// Make sure we're posting and have the name of the fix to apply
		if (!$this->request->is('post') || empty($this->request->data['OrderItem']['fix_code'])) {

			// If not, flash a message and do nothing more
			$this->Flash->error(
				'Something went wrong. Please select an option and try again.'
			);

		} else {

			if (!$this->Session->check('FixFailures.failure')) {

				// There's no fix in the session, so flash a message and do nothing more
				$this->Flash->error(
					'Something went wrong. Please select an option and try again.'
				);

			} else {

				// Get the fix failure from the session
				$fixFailure = $this->Session->read('FixFailures.failure');

				// Get the id of the invalid order item
				$orderItemId = $fixFailure['invalid_order_item']['id'];

				// Get the fix name
				$fixName = $this->request->data['OrderItem']['fix_code'];

				// Get any data keys - these are the options (if any) chosen by the user
				$fixData = !empty($this->request->data['FixData'])
					? $this->request->data['FixData']
					: []
				;

				// Now send it to the OrderItem model (well, the OrderItemFixable behavior to be precise)
				$result = $this->OrderItem->applyFix(
					$fixName,
					$orderItemId,
					$fixFailure,
					$fixData
				);

				// Clear the fix failures from session so they will be refreshed later
				$this->Session->delete('FixFailures');

				if ($this->request->is('ajax')) {

					return json_encode($result);

				} else {

					// Is there a message to flash?
					if (!empty($result['message'])) {

						if (!empty($result['message']['element'])) {
							$this->Flash->set(
								$result['message'],
								['element' => $result['message']['element']]
							);
						} else {

							if ($result['success']) {
								$this->Flash->success(
									$result['message']
								);
							} else {
								$this->Flash->error(
									$result['message']
								);
							}

						}

					}

				}

			}

		}

		// Whatever happens, redirect back to the configure action
		// This means the order can be reappraised after the fix has been applied - there might be more fixes needed
		return $this->redirect([
			'admin' => false,
			'controller' => 'orders',
			'action' => 'configure'
		]);

	}


	/**
	  This function rotates all portrait pages to landscape
	 */
	public function rotate($targetOrientation = 'portrait', $direction = 'clockwise', $orderItemId = null) {

		$result = $this->OrderItem->fixRotatePages($orderItemId, $targetOrientation, $direction);

		$this->Flash->success(
			'The pages have been rotated.'
		);


	}

	/**
	  This function rotates all portrait pages to landscape
	 */
	public function resize($orderItemId = null) {

		$result = $this->OrderItem->fixResize($orderItemId);

		$this->Flash->success(
			'The pages will be resized to fit on your chosen paper.'
		);

	}

	/**
	  This function rotates all landscape pages to portrait
	 */
	public function crop($orderItemId = null) {

		$result = $this->OrderItem->fixCrop($orderItemId);

		$this->Flash->success(
			'The pages will be printed and trimmed.'
		);
	}

	/**
	  This function scales artwork up to fit inside the larger paper for the order item. It's used for scaling up an entire file to match the order item.
	 */
	public function scale_up_to_paper_size($orderItemId = null) {

		$result = $this->OrderItem->fixScaleToCutSheet($orderItemId);

		$this->Flash->success(
			'The artwork will be enlarged to fit your chosen paper.'
		);

	}

	/**
	  This function scales artwork down to fit inside the smaller paper for the order item. It's used for scaling down an entire file to match the order item.
	 */
	public function scale_down_to_paper_size($orderItemId = null) {

		$result = $this->OrderItem->fixScaleToCutSheet($orderItemId);

		$this->Flash->success(
			'The artwork will be reduced to fit your chosen paper.'
		);

	}

	/**
	  This function rotates all landscape pages to portrait
	 */
	public function change_paper_down($orderItemId = null) {


		$result = $this->OrderItem->fixChangePaperDown($orderItemId);

		$this->Flash->success(
			'We will print your document on smaller paper.'
		);

	}

	/**
	  This function rotates all landscape pages to portrait
	 */
	public function change_paper_up($orderItemId = null) {

		$result = $this->OrderItem->fixChangePaperUp($orderItemId);

		$this->Flash->success(
			'We will print your document on larger paper.'
		);

	}

	/**
	  This function rotates all landscape pages to portrait
	 */
	public function trim_to_size($orderItemId = null) {

		$result = $this->OrderItem->fixTrimToSize($orderItemId, 'y');

		$this->Flash->success(
			'We\'ll print your document on best fit paper.'
		);

	}

	/**
	  This function marks an order item so it pronts on SRA paper.
	 */
	public function print_on_sra($orderItemId = null) {

		$result = $this->OrderItem->fixPrintOnSra($orderItemId, 'y');

		$this->Flash->success(
			'We\'ll print your document on SRA paper.'
		);

	}

	/**
	  This function sets the binding edge for an order item
	 */
	public function set_binding_edge($bindingEdge = null, $orderItemId = null) {

		$result = $this->OrderItem->setBindingEdgeTypeId($orderItemId, $bindingEdge);

		$this->Flash->success(
			'We\'ll bind your document on the ' . $bindingEdge . ' edge.'
		);


	}

	/**
	  This function sets the binding side for an order item
	 */
	public function set_binding_side($bindingSide = null, $orderItemId = null) {

		$result = $this->OrderItem->setBindingSideTypeId($orderItemId, $bindingSide);

		$message = $result
			? 'We\'ll bind your document ' . $result . '.'
			: 'We\'ll bind your document as requested.'
		;

	}

}
