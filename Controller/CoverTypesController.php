<?php
App::uses('AppController', 'Controller');
/**
 * CoverTypes Controller
 *
 * @property CoverType $coverType
 * @property PaginatorComponent $Paginator
 */
class CoverTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('coverTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CoverType->exists($id)) {
			throw new NotFoundException(__('Invalid cover type'));
		}
		$options = array('conditions' => array('CoverType.' . $this->CoverType->primaryKey => $id));
		$this->set('coverType', $this->CoverType->find('first', $options));
	}

}