<?php
App::uses('AppController', 'Controller');
/**
 * TurnaroundOptions Controller
 *
 */
class TurnaroundOptionsController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		$this->Auth->allow('same_day_is_available');
		parent::beforeFilter();
	}

	/**
	 * check_same_day function.
	 *
	 * @access public
	 * @return void
	 */
	public function same_day_is_available() {

		if ($this->request->is('post')) {
			$sameDayIsAvailable = $this->TurnaroundOption->sameDayIsAvailable(
				$this->request->data
			);
		} else {
			$sameDayIsAvailable = [
				'success' => false,
				'message' => 'Invalid data'
			];
		}
		$this->log($sameDayIsAvailable, 'debug');
		if ($this->request->is('ajax')) {
			return json_encode($sameDayIsAvailable);
		}

		if (!empty($sameDayIsAvailable['message'])) {
			$this->Flash->info($sameDayIsAvailable['message']);
		}

		return $sameDayIsAvailable;

	}


/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('turnaroundOptions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TurnaroundOption->exists($id)) {
			throw new NotFoundException(__('Invalid turnaround option'));
		}
		$options = array('conditions' => array('TurnaroundOption.' . $this->TurnaroundOption->primaryKey => $id));
		$this->set('turnaroundOption', $this->TurnaroundOption->find('first', $options));
	}

}
