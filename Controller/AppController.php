<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array(
		'Session',
		'Flash',
		'Auth' => array(
			'loginAction' => '/login',
			'logoutRedirect' => '/',
			'loginRedirect' => [
				'admin' => false,
				'controller' => 'users',
				'action' => 'view'
			],
			'authError' => 'Please log in to access that content.',
/*
			'authenticate' => array(
				'Form' => array(
					'passwordHasher' => array(
						'className' => 'Simple',
						'hashType' => 'sha256'
					)
				)
			),
*/
			'authorize' => 'Controller'
		),
		'DebugKit.Toolbar',
		'Paginator'
	);

	public $helpers = array(
		'Session',
		'Flash',
		'Presentation',
		'BootstrapForm',
		'PaypalPayment',
		'Dox'
	);

	public function setUpForAjax() {
		// All the standard ajaxy stuff
		Configure::write('debug', 0);
		$this->autoRender = false;
		$this->layout = 'ajax';
		$this->disableCache();
	}

	public function beforeFilter() {

		if ($this->request->is('ajax')) {
			$this->setUpForAjax();
		}

		if (!empty($this->request['prefix']) && $this->request['prefix'] == 'admin') {
			$this->layout = 'admin';
		}

		$this->set(array(
			'title_for_layout' => 'Online document printing services UK | Digital Printing | Doxdirect',
			'showProductsInNavigation' => true
		));

		$this->setLoggedInUser();

		$this->Paginator->settings = array(
			'limit' => 20
		);

		parent::beforeFilter();

	}

	public function beforeRender() {

		$orderInProgress = false;

		if (
			empty($this->params['admin'])
			&& $this->params['controller'] !== 'orders'
			&& $this->Session->check('Order')
		) {
			$orderInProgress = true;
		}

		$this->set('orderInProgress', $orderInProgress);
		parent::beforeRender();
	}

	public function setLoggedInUser() {

		if ($this->Auth->user()) {
			$user['User'] = $this->Auth->user();
			$this->loggedInUser = $user;
		} else {
			$this->loggedInUser = [];
		}

		$this->set(
			'loggedInUser',
			$this->loggedInUser
		);

	}

	public function isAuthorized($user = null) {

		// Any registered user can access public functions
		if (empty($this->request->params['admin'])) {
			return true;
		}

		// Only admins can access admin functions
		if (isset($this->request->params['admin'])) {
			return (bool)($user['role'] === 'admin');
		}

		// Default deny
		return false;

	}

/**
 * make_default method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_make_default($id = null) {

		$controller = $this->request->params['controller'];
		$model = Inflector::classify($controller);

		if ($this->{$model}->setDefault($id)) {
			$this->Flash->success(
				__('The default has been set.')
			);
		} else {
			$this->Flash->error(
				__('The default could not be set.')
			);
		}

		return $this->redirect($this->referer());

	}

	public function getIpAddress() {

		$ipaddress = '';

		if (getenv('REMOTE_ADDR')) {
			$ipaddress = getenv('REMOTE_ADDR');
		} elseif (getenv('HTTP_CLIENT_IP')) {
			$ipaddress = getenv('HTTP_CLIENT_IP');
		} elseif(getenv('HTTP_X_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		} elseif(getenv('HTTP_X_FORWARDED')) {
			$ipaddress = getenv('HTTP_X_FORWARDED');
		} elseif(getenv('HTTP_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		} elseif(getenv('HTTP_FORWARDED')) {
			$ipaddress = getenv('HTTP_FORWARDED');
		} else {
			$ipaddress = 'UNKNOWN';
		}

		return $ipaddress;

	}

/**
 * currentOrderId method
 *
 * @return array - the order from session. Creates a default order if there isn't one in the session
 */
	public function currentOrderId() {

		// Is there a order in session?
		if ($this->Session->check('Order.Order.id')) {
			return $this->Session->read('Order.Order.id');
		}

		if ($this->Session->check('OrderKeys.Order.id')) {
			return $this->Session->read('OrderKeys.Order.id');
		}

		return array();

	}

	public function storeOrderInSession($order = array()) {

		if (isset($order['Order']['id'])) {

			$orderId = $order['Order']['id'];

			$this->Session->write('Order.id', $orderId);
			$this->Session->write(
				'OrderKeys.Order.id',
				$orderId
			);

			Cache::write($orderId, $order, 'orders');

		}

	}

	public function orderKeysCurrent() {

		if (!$this->Session->check('OrderKeys')) {
			return array();
		}

		return $this->Session->read('OrderKeys');

	}

	public function refreshOrder($orderId = null, $store = true) {

		if (!$orderId) {
			$orderId = $this->currentOrderId();
		}
//		debug($orderId);
//
//		die(debug($store));


		$this->loadModel('Order');

		$order = $this->Order->getOrder($orderId, ['refresh' => true]);

//		if ($store) {
//			// Save it to the session
//			$this->storeOrderInSession($order);
//		}

		return $order;

	}

	/**
	 * A utilty function to send the user back to where they were in an order, if they have one on the go
	 * @param  boolean [$return = false] Whether to redirect to or return the $url
	 * @return mixed   The url to go to  or the actual redirect
	 */
	public function goBackToOrder($return = false) {

		// Start off with a false url
		$url = false;

		// See if there is an order in progress
		if ($this->Session->check('Order.order_status_id')) {
			// If so, get the url of the latest position in the order

			$url = [
				'admin' => false,
				'controller' => 'orders',
				'action' => $this->Session->read('Order.order_status_id')
			];

		}

		// If we are returning the url, return it
		if ($return) {

			return $url;

		} else {

			// Otherwise we are redirecting
			// As we can't redirect to false, set the url to root
			if (!$url) {
				$url = '/';
			}

			// Then redirect
			return $this->redirect($url);

		}

	}

	public function ajaxRenderElement($elementName = null, $variables = null) {

		// If we are doing this from ajax return the response
		$view = new View($this, false);

		if ($variables) {
			foreach ($variables as $variableName => $variable) {
				$view->set($variableName, $variable);
			}
		}

		$html = $view->render($elementName);

		debug($html);

		return $html;

	}

}
