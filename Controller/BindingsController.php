<?php
App::uses('AppController', 'Controller');
/**
 * Bindings Controller
 *
 * @property Binding $binding
 * @property PaginatorComponent $Paginator
 */
class BindingsController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {

		$this->set('bindings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Binding->exists($id)) {
			throw new NotFoundException(__('Invalid binding'));
		}
		$options = array('conditions' => array('Binding.' . $this->Binding->primaryKey => $id));
		$this->set('binding', $this->Binding->find('first', $options));
	}

}