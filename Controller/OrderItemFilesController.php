<?php
App::uses('AppController', 'Controller');
/**
 * OrderItemFiles Controller
 *
 */
class OrderItemFilesController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow();
		parent::beforeFilter();
	}

	public function delete($orderItemFileId = null) {

		if (!$orderItemFileId) {

			$response = false;

		} else {

			$this->Session->delete('FixFailures');

			$response = $this->OrderItemFile->deleteOrderItemFile($orderItemFileId);

//			if ($response['success']) {

				$orderKeysCurrent = $this->orderKeysCurrent();

				if ($orderKeysCurrent) {

					$orderKeysCurrent = $this->OrderItemFile->OrderItem->Order->orderKeysMoveToLast($orderKeysCurrent, 'OrderItemFile');

					if ($orderKeysCurrent) {

						$this->Session->write('OrderKeys', $orderKeysCurrent);

					}
				}

//			}

		}

		$this->Flash->info(
			'Your quote has been updated',
			['key' => 'quote-updated']
		);

		// Refresh the order and store it in session
		$order = $this->refreshOrder(null, true);

		if ($this->request->is('ajax')) {

			// If we are doing this from ajax return the response
//			$view = new View($this, false);
//
//			$view->set(compact('order'));
//
//			$orderItemSummaryHtml = $view->render('/Elements/orders/summary/print_summary');
//
//			$response['orderItemSummaryHtml'] = $orderItemSummaryHtml;

			$response['html'] = [
				'orderItemSummary' => $this->ajaxRenderElement(
					'/Elements/orders/summary/print_summary',
					['order' => $order]
				),
				'orderProgress' => $this->ajaxRenderElement(
					'/Elements/orders/summary/progress',
					['order' => $order]
				)
			];

			return json_encode($response);

		}

		return $this->redirect([
			'admin' => false,
			'controller' => 'orders',
			'action' => 'upload'
		]);

	}

/**
 * The processCallas function is usually called via ajax after a file upload, so does not render a view
 * It's a wrapper for the send_to_callas and get_callas_info functions
 */
	public function process_callas($orderItemFileId = null) {

		if ($this->request->is('post')) {

			$orderItemFileId = $this->request->data('orderItemFileId');

		}

		if ($orderItemFileId) {

			$response = $this->OrderItemFile->processCallas($orderItemFileId);

			if ($this->request->is('ajax')) {

//				$view = new View($this, false);
//
//				$orderItemFile = $this->OrderItemFile->findById($orderItemFileId);
//				$orderItemFile = $orderItemFile['OrderItemFile'];
//				$view->set(compact('orderItemFile'));
//				$html = $view->render('/Elements/orders/order_items/order_item_files/list_tr');
//
//				$response['html'][$orderItemFileId] = $html;

				return json_encode(true);

			}

			return $response;

		}

//		die(debug($response));

// 		$orderItemFileId = '5523bc3b-5478-484b-bf20-0b4c70e0ca7d';
// 		$response = $this->OrderItemFile->processCallas($orderItemFileId);

// die(debug($response));
	}

/**
 * This function calls the equivalent function in the OrderItemFile model.
 * That model function simply submits the file associated with this record to Callas.
 * There is only a response of 'Done' from Callas as it places the file into one or more hot folders.
 * Callas will process them in due course.
 * The results of that processing are retrievable from get_callas_info
 */
	public function send_to_callas($orderItemFileId = null) {

		$this->autoRender = false;

		if (!$orderItemFileId) {
			return false;
		}

		$orderItemFile = $this->OrderItemFile->getOrderItemFile($orderItemFileId);

		if ($orderItemFile) {

			$result = $this->OrderItemFile->sendToCallas($orderItemFile);

		} else {

			$result = array(
				'success' => false,
				'code' => 'no-order-item-file',
				'message' => 'Order item file not found.'
			);

		}

		return $result;

	}

/**
 * This function calls the equivalent function in the OrderItemFile model.
 * That model function looks for the output folders in Callas
 */
	public function get_callas_info($orderItemFileId = null) {

		$orderId = $this->OrderItemFile->orderId($orderItemFileId);

		if (!$orderId) {
			return array(
				'success' => false,
				'code' => 'invalid-order-item-file-id',
				'message' => 'That is an invalid order item file id.'
			);
		}

		$order = $this->OrderItemFile->OrderItem->Order->getOrder($orderId);

		if (!$order) {
			return array(
				'success' => false,
				'code' => 'invalid-order',
				'message' => 'That is an invalid order.'
			);
		}

		$result = $this->OrderItemFile->getCallasInfo($order);

		return $result;

	}

	public function resequence() {

		if ($this->request->is('post') || $this->request->is('put')) {

			$orderItemFileIds = $this->request->data('orderItemFileIds');

			$response = $this->OrderItemFile->resequence($orderItemFileIds);

			$this->refreshOrder();

			if ($this->request->is('ajax')) {

				return json_encode($response);

			}

		}

		return $this->redirect(['admin' => false, 'controller' => 'orders', 'action' => 'upload']);

	}

}
