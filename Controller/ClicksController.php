<?php
App::uses('AppController', 'Controller');
/**
 * Clicks Controller
 *
 * @property Click $click
 * @property PaginatorComponent $Paginator
 */
class ClicksController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('clicks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Click->exists($id)) {
			throw new NotFoundException(__('Invalid click'));
		}
		$options = [
			'conditions' => ['Click.' . $this->Click->primaryKey => $id],
			'contain' => ['ProductClick.Product.BindingType']
		];
		$this->set('click', $this->Click->find('first', $options));
	}

}
