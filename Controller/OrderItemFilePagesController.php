<?php
App::uses('AppController', 'Controller');
/**
 * OrderItemFilePages Controller
 *
 */
class OrderItemFilePagesController extends AppController {

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

	public function admin_page_images() {

		$this->set(
			'pageImages',
			$this->OrderItemFilePage->images
		);

	}

}
