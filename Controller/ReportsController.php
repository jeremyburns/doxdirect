<?php
App::uses('AppController', 'Controller');
/**
 * Reports Controller
 *
 * @property Report $Report
 * @property PaginatorComponent $Paginator
 */
class ReportsController extends AppController {

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

	public $uses = array();

	public function admin_index() {

	}

}