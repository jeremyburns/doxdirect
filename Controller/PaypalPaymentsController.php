<?php
App::uses('AppController', 'Controller');
/**
 * PaypalPayments Controller
 *
 */

class PaypalPaymentsController extends AppController {

	/**
 * beforeFilter makes sure the process is allowed by auth
 *  since paypal will need direct access to it.
 */
	public function beforeFilter() {

		$this->log('beforeFilter', 'paypal');
		parent::beforeFilter();
		if (isset($this->Auth)) {
			$this->Auth->allow('process');
		}
		if (isset($this->Security) && $this->action == 'process') {
			$this->Security->validatePost = false;
		}
	}

	/**
 * Paypal IPN processing action..
 * Intake for a paypal_ipn callback performed by paypal itself.
 * This action will take the paypal callback, verify it (so trickery) and
 * save the transaction into your database for later review
 *
 * @access public
 * @author Nick Baker
 */
	public function process() {

		$this->autoRender = false;

		if ($this->request->is('post')) {
			$this->log('POST ' . print_r($this->request->data, true), 'paypal');
		}

		if ($this->PaypalPayment->isValid($this->request->data)) {

			$this->log('POST Valid', 'paypal');

			$paypalPayment = $this->PaypalPayment->buildAssociationsFromIPN($this->request->data);

			$existingPaypalPaymentId = $this->PaypalPayment->searchPaypalPaymentTxn($paypalPayment);

			if ($existingPaypalPaymentId !== false) {
				$paypalPayment['PaypalPayment']['id'] = $existingPaypalPaymentId;
			}

			$result = $this->PaypalPayment->saveAll($paypalPayment);

			$result = $this->__processTransaction($this->PaypalPayment->id);

		} else {

			$this->log('POST Not Validated', 'paypal');
		}

		return $this->redirect('/');

	}

	/**
 * __processTransaction is a private callback function used to log a verified transaction
 * @access private
 * @param String $paypalPaymentId is the string paypal ID and the id used in your database.
 */
	private function __processTransaction($paypalPaymentId){

		$this->log("Processing Transaction: {$paypalPaymentId}", 'paypal');

		$this->PaypalPayment->afterPaypalNotification($paypalPaymentId);

		return $this->redirect([
			'admin' => false,
			'controller' => 'orders',
			'action' => 'paid'
		]);

	}

}
