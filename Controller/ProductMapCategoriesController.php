<?php
App::uses('AppController', 'Controller');
/**
 * ProductMapCategories Controller
 *
 */
class ProductMapCategoriesController extends AppController {

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

}