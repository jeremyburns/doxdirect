<?php
App::uses('AppController', 'Controller');


/**
* UserAddresses Controller
*
* @property User $User
* @property UserAddress $UserAddress
* @property PaginatorComponent $Paginator
*/
class UserAddressesController extends AppController {

	public function beforeFilter() {
		$this->Auth->deny();

		$this->userId = $this->Auth->user('id');

		parent::beforeFilter();
	}

	/**
 * index method
 *
 * @return void
 */
	public function index() {

		$userAddresses = $this->UserAddress->getActiveAddresses($this->userId);

		if ($this->request->is('ajax')) {

			$html = $this->ajaxRenderElement(
				'/Elements/user_addresses/list',
				[
					'userAddresses' => $userAddresses
				]
			);

			$result = [
				'html' => $html
			];

			return json_encode($result);

		}

		$this->set('userAddresses', $userAddresses);

	}

	/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($userAddressId = null) {

		if (!$this->UserAddress->exists($id)) {
			throw new NotFoundException(__('Invalid address'));
		}

		$userAddress = $this->UserAddress->getActiveAddresses($this->userId, $userAddressId);

		if (!$userAddress) {

			$this->Flash->error(
				'You do not have permission to view that address.'
			);

		}

		$this->set('userAddress', $userAddress);

	}

	/**
 * add method
 *
 * @return void
 */
	public function add() {

		$result = [];

		if ($this->request->is(['post', 'put'])) {

			$overrideChecks = isset($this->request->data['add-duplicate']);

			$this->request->data['UserAddress']['user_id'] = $this->userId;

			$result = $this->UserAddress->add($this->request->data, $overrideChecks);

			if ($result['success']) {

				$this->Flash->success(
					$result['message']
				);

			} else {

				$this->Flash->warning(
					$result['message']
				);

			}

			$this->redirect([
				'admin' => false,
				'controller' => 'user_addresses',
				'action' => 'index'
			]);

		}

		$countries = $this->UserAddress->Country->find('list');

		if ($this->request->is('ajax')) {

			$html = $this->ajaxRenderElement(
				'/Elements/user_addresses/add',
				[
					'countries' => $countries
				]
			);

			$result = [
				'html' => $html
			];

			return json_encode($result);

		}

		$this->set(compact('countries', 'result'));

	}

	/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (!$this->UserAddress->exists($id)) {
			throw new NotFoundException(__('Invalid address'));
		}

		if ($this->UserAddress->field('user_id') !== $this->userId) {
			throw new NotFoundException(__('You do not have permission to edit that address.'));
		}

		if ($this->request->is(['post', 'put'])) {

			if ($this->UserAddress->save($this->request->data)) {

				$message = __('The address has been updated.');

//				if ($this->request->is('ajax')) {
//
//					$result = [
//						'success' => true,
//						'message' => $message
//					];
//
//					return json_encode($result);
//				}

				$this->Flash->success(
					$message
				);

				$this->redirect([
					'admin' => false,
					'controller' => 'user_addresses',
					'action' => 'index'
				]);

			} else {

				$message = __('The address could not be updated. Please, try again.');

				if ($this->request->is('ajax')) {

					$result = [
						'success' => false,
						'message' => $message
					];

					return json_encode($result);
				}

				$this->Flash->error(
					$message
				);

			}

		} else {

			$options = [
				'conditions' => [
					'UserAddress.id' => $id,
					'UserAddress.user_id' => $this->userId
				]
			];

			$userAddress = $this->UserAddress->find('first', $options);

			if (!$userAddress) {

				$this->Flash->error(
					'You do not have permission to edit that address.'
				);

			}

			$this->request->data = $userAddress;

		}

		$countries = $this->UserAddress->Country->find('list');

		if ($this->request->is('ajax')) {

			$html = $this->ajaxRenderElement(
				'/Elements/user_addresses/edit',
				[
					'countries' => $countries
				]
			);

			$result = [
				'html' => $html
			];

			return json_encode($result);

		}

		$this->set(compact('countries'));

	}

	/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($userAddressId = null) {

		$this->request->allowMethod('post', 'delete');

		$result = $this->UserAddress->softDelete($userAddressId, $this->userId);

		if ($result['success']) {

			$this->Flash->success(
				$result['message']
			);

		} else {

			$this->Flash->error(
				$result['message']
			);

		}

		$this->redirect([
			'admin' => false,
			'controller' => 'user_addresses',
			'action' => 'index'
		]);

	}

}
