<?php
App::uses('AppController', 'Controller');
/**
 * BindingTypes Controller
 *
 * @property BindingType $bindingType
 * @property PaginatorComponent $Paginator
 */
class BindingTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('bindingTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->BindingType->exists($id)) {
			throw new NotFoundException(__('Invalid binding type'));
		}
		$options = array('conditions' => array('BindingType.' . $this->BindingType->primaryKey => $id));
		$this->set('bindingType', $this->BindingType->find('first', $options));
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->BindingType->exists($id)) {
			throw new NotFoundException(__('Invalid binding type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BindingType->save($this->request->data)) {
				$this->Flash->success(
					__('The binding type has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The binding type could not be saved. Please, try again.')
				);
			}
		} else {
			$options = array('conditions' => array('BindingType.' . $this->BindingType->primaryKey => $id));
			$this->request->data = $this->BindingType->find('first', $options);
		}
	}

	public function admin_refresh_product_category() {

		$this->BindingType->refreshProductCategory();

	}

}
