<?php
App::uses('AppController', 'Controller');
/**
 * JobStatuses Controller
 *
 * @property JobStatus $JobStatus
 * @property PaginatorComponent $Paginator
 */
class JobStatusesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('jobStatuses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->JobStatus->exists($id)) {
			throw new NotFoundException(__('Invalid job status'));
		}
		$options = array('conditions' => array('JobStatus.' . $this->JobStatus->primaryKey => $id));
		$this->set('jobStatus', $this->JobStatus->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->JobStatus->create();
			if ($this->JobStatus->save($this->request->data)) {
				$this->Flash->success(
					__('The job status has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The job status could not be saved. Please, try again.')
				);
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->JobStatus->exists($id)) {
			throw new NotFoundException(__('Invalid job status'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->JobStatus->save($this->request->data)) {
				$this->Flash->success(
					__('The job status has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The job status could not be saved. Please, try again.')
				);
			}
		} else {
			$options = array('conditions' => array('JobStatus.' . $this->JobStatus->primaryKey => $id));
			$this->request->data = $this->JobStatus->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->JobStatus->id = $id;
		if (!$this->JobStatus->exists()) {
			throw new NotFoundException(__('Invalid job status'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->JobStatus->delete()) {
			$this->Flash->success(
				__('The job status has been deleted.')
			);
		} else {
			$this->Flash->error(
				__('The job status could not be deleted. Please, try again.')
			);
		}
		return $this->redirect(array('action' => 'index'));
	}
}
