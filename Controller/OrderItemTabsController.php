<?php
App::uses('AppController', 'Controller');
/**
 * OrderItemTab Controller
 *
 */
class OrderItemTabsController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow();
		parent::beforeFilter();
	}

	public function add() {

		if ($this->request->is('post') || $this->request->is('put') && !empty($this->request->data['OrderItemTab']['order_item_id'])) {

			$orderItemId = $this->request->data['OrderItemTab']['order_item_id'];

			$result = $this->OrderItemTab->add($this->request->data);

			if ($result['success']) {

				if ($this->request->is('ajax')) {

					$this->set(
						'orderItemTabs',
						$this->OrderItem->find(
							'all',
							array(
								'conditions' => array(
									'OrderItemTab.order_item_id' => $orderItemId
								)
							)
						)
					);

					$this->render('/Elements/orders/configure/tabs');

				}

			} else {

				if ($this->request->is('ajax')) {

					$this->set(
						'orderItemTabs',
						$this->OrderItem->find(
							'all',
							array(
								'conditions' => array(
									'OrderItemTab.order_item_id' => $orderItemId
								)
							)
						)
					);

					$this->render('/Elements/orders/configure/tabs');

				} else {

					$this->Flash->error(
						'Sorry - we couldn\'t add that tab. Please see the errors below.'
					);

					$this->Session->write(
						'Error.OrderItemTab',
						$result['errors']
					);

				}

			}

		}

		$this->redirect(['admin' => false, 'controller' => 'orders', 'action' => 'configure']);

	}

	public function delete($orderItemTabId = null) {

		if (!$orderItemTabId) {
			return $this->redirect(['admin' => false, 'controller' => 'orders', 'action' => 'configure']);
		}

		$result = $this->OrderItemTab->delete($orderItemTabId);

		if ($this->request->is('ajax')) {

			return json_encode($result);

		}

		if ($result) {

			$this->Flash->success(
				'The tab has been deleted'
			);

		} else {

			$this->Flash->error(
				'The tab could not be deleted'
			);

		}

		return $this->redirect(['admin' => false, 'controller' => 'orders', 'action' => 'configure']);

	}

}
