<?php
App::uses('AppController', 'Controller');

/**
* The UtilitiesController contains non model related functions, typically called via ajax
*/

class UtilitiesController extends AppController {

	var $uses = [];

	public function beforeFilter() {
		$this->Auth->allow();
		parent::beforeFilter();
	}

	public function ajax_flash() {

		$this->request->allowMethod('post');

		extract($this->request->data);

		if (!$message) {
			return null;
		}

//		$view = new View($this, false);
//		$view->set(compact('message'));
//		$flash = $view->render('/Elements/Flash/' . $element);

		$flash = $this->ajaxRenderElement(
			'/Elements/Flash/' . $element,
			[
				'message' => $message
			]
		);

		return json_encode($flash);

	}

	public function ajax_element() {

		$this->request->allowMethod('post');

		$elementName = $this->request->data['elementName'];

		if (empty($elementName)) {
			return null;
		}

		$view = new View($this, false);

		if (!empty($this->request->data['variables'])) {
			$variables = $this->request->data['variables'];

			foreach ($variables as $variableName => $variable) {
				$view->set($variableName, $variable);
			}
		}
//		$view->set(compact($variables));

		$html = $view->render('/Elements/' . $elementName);

		return json_encode($html);

	}

}
