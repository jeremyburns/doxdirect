<?php
App::uses('AppController', 'Controller');
/**
 * Medias Controller
 *
 * @property Media $media
 * @property PaginatorComponent $Paginator
 */
class MediaController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Media->updateTypes();
		$this->set('media', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Media->exists($id)) {
			throw new NotFoundException(__('Invalid media'));
		}
		$options = [
			'conditions' => ['Media.' . $this->Media->primaryKey => $id],
			'contain' => ['ProductMedia.Product.BindingType']
		];
		$this->set('media', $this->Media->find('first', $options));
	}

}
