<?php
App::uses('AppController', 'Controller');
/**
 * DrillingTypes Controller
 *
 * @property DrillingType $DrillingType
 * @property PaginatorComponent $Paginator
 */
class DrillingTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('drillingTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->DrillingType->exists($id)) {
			throw new NotFoundException(__('Invalid drilling type'));
		}

		$options = [
			'conditions' => ['DrillingType.' . $this->DrillingType->primaryKey => $id],
			'contain' => ['ProductDrillingType.Product.BindingType']
		];
		$this->set('drillingType', $this->DrillingType->find('first', $options));
	}

}
