<?php
App::uses('AppController', 'Controller');
/**
 * HolidayAlerts Controller
 *
 * @property HolidayAlert $HolidayAlert
 * @property PaginatorComponent $Paginator
 */
class HolidayAlertsController extends AppController {

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('holidayAlerts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->HolidayAlert->exists($id)) {
			throw new NotFoundException(__('Invalid holiday alert'));
		}
		$options = array('conditions' => array('HolidayAlert.' . $this->HolidayAlert->primaryKey => $id));
		$this->set('holidayAlert', $this->HolidayAlert->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->HolidayAlert->create();
			if ($this->HolidayAlert->save($this->request->data)) {
				$this->Flash->success(
					__('The holiday alert has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The holiday alert could not be saved. Please, try again.')
				);
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->HolidayAlert->exists($id)) {
			throw new NotFoundException(__('Invalid holiday alert'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->HolidayAlert->save($this->request->data)) {
				$this->Flash->success(
					__('The holiday alert has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The holiday alert could not be saved. Please, try again.')
				);
			}
		} else {
			$options = array('conditions' => array('HolidayAlert.' . $this->HolidayAlert->primaryKey => $id));
			$this->request->data = $this->HolidayAlert->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->HolidayAlert->id = $id;
		if (!$this->HolidayAlert->exists()) {
			throw new NotFoundException(__('Invalid holiday alert'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->HolidayAlert->delete()) {
			$this->Flash->success(
				__('The holiday alert has been deleted.')
			);
		} else {
			$this->Flash->error(
				__('The holiday alert could not be deleted. Please, try again.')
			);
		}
		return $this->redirect(array('action' => 'index'));
	}
}
