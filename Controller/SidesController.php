<?php
App::uses('AppController', 'Controller');
/**
 * Sides Controller
 *
 * @property Side $side
 * @property PaginatorComponent $Paginator
 */
class SidesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('sides', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Side->exists($id)) {
			throw new NotFoundException(__('Invalid side'));
		}
		$options = [
			'conditions' => ['Side.' . $this->Side->primaryKey => $id],
			'contain' => ['ProductSide.Product.BindingType']
		];
		$this->set('side', $this->Side->find('first', $options));
	}

}
