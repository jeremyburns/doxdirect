<?php
App::uses('AppController', 'Controller');
/**
 * BindingSideTypes Controller
 *
 * @property BindingSideType $bindingSideType
 * @property PaginatorComponent $Paginator
 */
class BindingSideTypesController extends AppController {

	/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

	/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('bindingSideTypes', $this->Paginator->paginate());
	}

	/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->BindingSideType->exists($id)) {
			throw new NotFoundException(__('Invalid binding side'));
		}
		$options = [
			'conditions' => ['BindingSideType.' . $this->BindingSideType->primaryKey => $id],
			'contain' => 'ProductBindingSideType.Product.BindingType'
		];
		$this->set('bindingSideType', $this->BindingSideType->find('first', $options));
	}

}
