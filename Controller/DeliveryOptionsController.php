<?php
App::uses('AppController', 'Controller');
/**
 * DeliveryOptions Controller
 *
 * @property DeliveryOption $DeliveryOption
 * @property PaginatorComponent $Paginator
 */
class DeliveryOptionsController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

	public function admin_refresh($refresh = false, $toScreen = true) {

		$result = $this->DeliveryOption->refresh($refresh, $toScreen);

		if ($result['success'] == true) {
			$this->Flash->success(
				'The delivery options have been refreshed.'
			);
		} else {

			$this->Flash->error(
				'There was an error refreshing the delivery options.'
			);
		}

		return $this->redirect(
			array(
				'admin' => true,
				'controller' => 'delivery_options',
				'action' => 'index'
			)
		);

	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('deliveryOptions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->DeliveryOption->exists($id)) {
			throw new NotFoundException(__('Invalid delivery option'));
		}
		$options = array('conditions' => array('DeliveryOption.' . $this->DeliveryOption->primaryKey => $id));
		$this->set('deliveryOption', $this->DeliveryOption->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->DeliveryOption->create();
			if ($this->DeliveryOption->save($this->request->data)) {
				$this->Flash->success(
					__('The delivery option has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The delivery option could not be saved. Please, try again.')
				);
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->DeliveryOption->exists($id)) {
			throw new NotFoundException(__('Invalid delivery option'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->DeliveryOption->save($this->request->data)) {
				$this->Flash->success(
					__('The delivery option has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The delivery option could not be saved. Please, try again.')
				);
			}
		} else {
			$options = array('conditions' => array('DeliveryOption.' . $this->DeliveryOption->primaryKey => $id));
			$this->request->data = $this->DeliveryOption->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->DeliveryOption->id = $id;
		if (!$this->DeliveryOption->exists()) {
			throw new NotFoundException(__('Invalid delivery option'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->DeliveryOption->delete()) {
			$this->Flash->success(
				__('The delivery option has been deleted.')
			);
		} else {
			$this->Flash->error(
				__('The delivery option could not be deleted. Please, try again.')
			);
		}
		return $this->redirect(array('action' => 'index'));
	}
}
