<?php
App::uses('AppController', 'Controller');
/**
 * TabTypes Controller
 *
 * @property TabType $tabType
 * @property PaginatorComponent $Paginator
 */
class TabTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('tabTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TabType->exists($id)) {
			throw new NotFoundException(__('Invalid tab type'));
		}
		$options = [
			'conditions' => ['TabType.' . $this->TabType->primaryKey => $id],
			'contain' => ['ProductTabType.Product.BindingType']
		];
		$this->set('tabType', $this->TabType->find('first', $options));
	}

}
