<?php
App::uses('AppController', 'Controller');
/**
 * BindingEdgeTypes Controller
 *
 * @property BindingEdgeType $bindingEdgeType
 * @property PaginatorComponent $Paginator
 */
class BindingEdgeTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('bindingEdgeTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->BindingEdgeType->exists($id)) {
			throw new NotFoundException(__('Invalid binding edge type'));
		}
		$options = [
			'conditions' => ['BindingEdgeType.' . $this->BindingEdgeType->primaryKey => $id],
			'contain' => 'ProductBindingEdgeType.Product.BindingType'
		];
		$this->set('bindingEdgeType', $this->BindingEdgeType->find('first', $options));
	}

}
