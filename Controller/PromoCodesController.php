<?php
App::uses('AppController', 'Controller');
/**
 * PromoCodes Controller
 *
 * @property PromoCode $PromoCode
 * @property PaginatorComponent $Paginator
 */
class PromoCodesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {

		$promoCodes = $this->Paginator->paginate();

		foreach ($promoCodes as $promoCodeKey => $promoCode) {
			$promoCodes[$promoCodeKey]['PromoCode']['is_live'] = $this->PromoCode->isLive($promoCode);
		}

		$this->set('promoCodes', $promoCodes);

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PromoCode->exists($id)) {
			throw new NotFoundException(__('Invalid promo code'));
		}
		$options = array('conditions' => array('PromoCode.' . $this->PromoCode->primaryKey => $id));
		$this->set('promoCode', $this->PromoCode->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if ($this->PromoCode->add($this->request->data)) {
				$this->Flash->success(
					__('The promo code has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The promo code could not be saved. Please, try again.')
				);
			}
		}

		$discountTypes = $this->PromoCode->discountTypes();
		$applyDiscountToFieldNames = $this->PromoCode->applyDiscountToFieldNames();

		$js = 'promo_codes/add.min';

		$this->set(compact(
			'discountTypes',
			'applyDiscountToFieldNames',
			'js'
		));

	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PromoCode->exists($id)) {
			throw new NotFoundException(__('Invalid promo code'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PromoCode->save($this->request->data)) {
				$this->Flash->success(
					__('The promo code has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The promo code could not be saved. Please, try again.')
				);
			}
		} else {
			$options = array('conditions' => array('PromoCode.' . $this->PromoCode->primaryKey => $id));
			$this->request->data = $this->PromoCode->find('first', $options);
		}

		$discountTypes = $this->PromoCode->discountTypes();
		$applyDiscountToFieldNames = $this->PromoCode->applyDiscountToFieldNames();

		$js = 'promo_codes/add.min';

		$this->set(compact(
			'discountTypes',
			'applyDiscountToFieldNames',
			'js'
		));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PromoCode->id = $id;
		if (!$this->PromoCode->exists()) {
			throw new NotFoundException(__('Invalid promo code'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PromoCode->delete()) {
			$this->Flash->success(
				__('The promo code has been deleted.')
			);
		} else {
			$this->Flash->error(
				__('The promo code could not be deleted. Please, try again.')
			);
		}
		return $this->redirect(array('action' => 'index'));
	}
}
