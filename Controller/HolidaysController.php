<?php
App::uses('AppController', 'Controller');
/**
 * Holidays Controller
 *
 * @property Holiday $Holiday
 * @property PaginatorComponent $Paginator
 */
class HolidaysController extends AppController {

	public function beforeFilter() {
		$this->Auth->deny();
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->set('holidays', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Holiday->exists($id)) {
			throw new NotFoundException(__('Invalid holiday'));
		}
		$options = array('conditions' => array('Holiday.' . $this->Holiday->primaryKey => $id));
		$this->set('holiday', $this->Holiday->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Holiday->create();
			if ($this->Holiday->save($this->request->data)) {
				$this->Flash->success(
					__('The holiday has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The holiday could not be saved. Please, try again.')
				);
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Holiday->exists($id)) {
			throw new NotFoundException(__('Invalid holiday'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Holiday->save($this->request->data)) {
				$this->Flash->success(
					__('The holiday has been saved.')
				);
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(
					__('The holiday could not be saved. Please, try again.')
				);
			}
		} else {
			$options = array('conditions' => array('Holiday.' . $this->Holiday->primaryKey => $id));
			$this->request->data = $this->Holiday->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Holiday->id = $id;
		if (!$this->Holiday->exists()) {
			throw new NotFoundException(__('Invalid holiday'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Holiday->delete()) {
			$this->Flash->success(
				__('The holiday has been deleted.')
			);
		} else {
			$this->Flash->error(
				__('The holiday could not be deleted. Please, try again.')
			);
		}
		return $this->redirect(array('action' => 'index'));
	}
}
