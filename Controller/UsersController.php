<?php
App::uses('AppController', 'Controller');


/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

	public function beforeFilter() {
		$this->Auth->allow(
			'add',
			'cancel_login',
			'login',
			'logout',
			'reset_password',
			'request_password_reset'
		);
		parent::beforeFilter();
	}

	public function login() {

		if ($this->request->is('post')) {

			if ($this->Auth->login()) {

				$this->setLoggedInUser();

				// housekeeping around the user's record
				//set the last login date and clear down failed login attempts
				$this->User->afterLogin($this->Auth->user('id'));

				$message = 'Welcome back, ' . $this->Session->read('Auth.User.first_name') . '.';

				if ($this->request->is('ajax')) {

					$result = [
						'success' => true,
						'message' => $message
					];

					return json_encode($result);

				} else {

					$this->Flash->success(
						$message
					);

					if ($this->Auth->user('role') == 'admin') {

						return $this->redirect([
							'admin' => true,
							'controller' => 'users',
							'action' => 'dashboard'
						]);

					}

					$url = $this->goBackToOrder(true);

					if ($url) {
						return $this->redirect($url);
					}

					return $this->redirect($this->Auth->redirectUrl());

				}

			} else {

				$message = 'Username or password is incorrect';

				if ($this->request->is('ajax')) {

					$result = [
						'success' => false,
						'message' => $message
					];

					return json_encode($result);

				} else {

					$this->Flash->error(
						__($message),
						['key' => 'auth']
					);

				}

			}

		}

		$this->set('cart', $this->cartDetails());

	}

	public function cancel_login() {

		return $this->redirect($this->referer());

	}

	public function logout() {

		$this->Session->destroy();

		$this->Flash->info(
			'You have signed out of your Dox account'
		);

		return $this->redirect($this->Auth->logout());

	}

/**
	This function creates a password rest code for a user based on the supplied username.
	It sends an email to the username with a link back to the site.
	The link points at password_reset/[the_code]
*/
	public function request_password_reset() {

		if (!empty($this->request->data['User']['username'])) {

			$passwordResetCode = $this->User->passwordResetRequest($this->request->data['User']['username']);

			$this->Flash->success(
				[
					'heading' => 'We\'ve sent you an email',
					'message' => 'We\'ve sent you a password reset link by email. Please check your email, including your junk folders (just in case).'
				]
			);

			$this->redirect([
				'admin' => false,
				'controller' => 'users',
				'action' => 'login'
			]);

		}

		$this->set('cart', $this->cartDetails());

	}

	public function reset_password($passwordResetCode = null) {

		if ($this->request->data) {

			$response = $this->User->resetPassword($this->request->data);

			if ($response['success']) {

				$this->Auth->login($response['user']['User']);

				$this->Flash->success(
					$response['message']
				);

				return $this->redirect([
					'admin' => false,
					'controller' => 'users',
					'action' => 'view'
				]);

			}

		}

		if (!$passwordResetCode) {

			$this->goBackToOrder();

		}

		if (!$this->User->passwordResetRequestCheck($passwordResetCode)) {

			$this->Flash->error(
				'That is an invalid or expired password reset code. Please request a new one.'
			);

			return $this->redirect([
				'admin' => false,
				'controller' => 'users',
				'action' => 'request_password_reset'
			]);

		}

		$this->set('passwordResetCode', $passwordResetCode);

	}

	public function change_password() {

		if ($this->request->data) {

			$data = $this->request->data;

			$data['User']['id'] = $this->Auth->user('id');

			$response = $this->User->passwordChange($data);

			if ($response['result'] === 'success') {

				$this->Auth->login($response['user']['User']);

				$this->Flash->success(
					$response['message']
				);

				return $this->redirect([
					'admin' => false,
					'controller' => 'users',
					'action' => 'view'
				]);

			}

		}

	}

	public function view() {

		if (!$this->Auth->user()) {
			$this->goBackToOrder();
		}

		$user = $this->User->find(
			'first',
			[
				'conditions' => [
					'User.id' => $this->Auth->user('id')
				],
				'contain' => [
					'Order' => [
						'OrderStatus',
						'OrderItem'
					],
					'UserAddress' => [
						'conditions' => [
							'UserAddress.is_active' => true
						],
						'Country'
					]
				]
			]
		);

		if (!$user) {
			$this->goBackToOrder();
		}

		$js = [
			'user_addresses/manage.min'
		];

		$cart = $this->cartDetails();

		$this->set(compact(
			'cart',
			'user',
			'js'
		));

	}

	public function edit() {

		if ($this->request->is(['post', 'put'])) {

			// Setting the user id here to prevent form tampering
			// which would allow a user to change another user's details
			$this->request->data['User']['id'] = $this->Auth->user('id');

			$saved = $this->User->save($this->request->data);

			if ($saved) {

				$this->Session->write(
					'Auth',
					$this->User->read(
						null,
						$this->Auth->User('id')
					)
				);

				$result = [
					'success' => true,
					'message' => 'Your details have been updated.'
				];

//				if ($this->request->is('ajax')) {
//
//					$result = [
//						'success' => true,
//						'message' => 'Your details have been updated.'
//					];
//
//					return json_encode($result);
//
//				} else {
//
//					$this->Flash->success(
//						__('Your details have been changed.')
//					);
//
//					$this->goBackToOrder();
//
//				}

			} else {

				if ($this->request->is('ajax')) {

					$result = [
						'success' => false,
						'message' => '<i class="fa fa-times-circle"></i> Your details could not be saved. Please, try again.'
					];

					return json_encode($result);

				} else {

					$this->Flash->error(
						__('Your details could not be saved. Please, try again.')
					);

				}

			}

		}

		$user = $this->User->find(
			'first',
			[
				'conditions' => [
					'User.id' => $this->Auth->user('id')
				]
			]
		);

		if (!$user) {
			$this->goBackToOrder();
		}

		$this->set(compact('user'));

		if ($this->request->is('ajax')) {

//			$view = new View($this, false);
//			$view->set(compact('user'));
//			$html = $view->render('/Elements/users/edit');

			$html = $this->ajaxRenderElement(
				'/Elements/users/edit',
				[
					'user' => $user
				]
			);

			$result = [
				'html' => $html
			];

			return json_encode($result);

		}

		$this->set('cart', $this->cartDetails());

	}

	public function cartDetails() {

		return $this->User->Order->cartDetails($this->currentOrderId());

	}

	public function returned_user($encode = true) {

		$userId = $this->Auth->user('id');

		$loggedInUser = $this->loggedInUser;

		if ($userId) {

			$userAddresses = $this->User->UserAddress->addressesAsList($userId);

//			$userAddressesView = new View($this, false);
//			$userAddressesView->set(compact('loggedInUser', 'userAddresses'));
//
//			$result['userDetails'] = $userAddressesView->render('/Elements/orders/checkout/panels/returned_user');

			$result['userDetails'] = $this->ajaxRenderElement(
				'/Elements/orders/checkout/panels/returned_user',
				[
					'loggedInUser' => $loggedInUser,
					'userAddresses' => $userAddresses
				]
			);

//			$userView = new View($this, false);
//			$userView->set(compact('loggedInUser'));
//
//			$result['loginLink'] = $userView->render('/Elements/users/login_link');

			$result['loginLink'] = $this->ajaxRenderElement(
				'/Elements/users/login_link',
				[
					'loggedInUser' => $loggedInUser
				]
			);

		}

		if ($encode) {
			$result = json_encode($result);
		}

		return $result;

	}

	public function admin_index() {

		$this->set('users', $this->Paginator->paginate('User'));

	}

	public function admin_dashboard() {}

}
