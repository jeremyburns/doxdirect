<div class="container">

	<div class="row">

		<div class="col-xs-24">

			<h1>Privacy policy</h1>

			<h2>Our privacy policy</h2>

			<p>Your privacy is as important to us as it is to you. This document details our policy towards protecting your privacy, whether you are an individual or a business user.</p>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-6">

			<h3><a name="contents"></a>Contents</h3>

			<ul class="list-unstyled list-divided">
				<li><a href="#scope">Scope</a></li>
				<li><a href="#collection-and-storage">Collection and storage</a></li>
				<li><a href="#marketing-by-third-parties">Marketing by third parties</a></li>
				<li><a href="#email-marketing">Email marketing</a></li>
				<li><a href="#use-of-your-information">Use of your information</a></li>
				<li><a href="#information-disclosure">Our disclosure of your information</a></li>
				<li><a href="#data-controller">Data controller</a></li>
				<li><a href="#security">Security</a></li>
				<li><a href="#amendments">Amendments to this privacy policy</a></li>
			</ul>

		</div>

		<div class="col-sm-18">

			<h3><a name="scope"></a>Scope</h3>

			<p>This privacy policy describes how Doxdirect Limited (&#8220;we&#8221;, &#8220;us&#8221; or &#8220;our&#8221;) collects, stores, uses and discloses your personal or business information in connection with the performance of the services that are made available to you through www.doxdirect.com (&#8220;this Site&#8221;).</p>


			<p>By accepting the Terms &amp; Conditions during the process of ordering a printed document, or by agreeing to the Prime customer vendor agreement (if you are a business), you expressly consent to the collection, storage, use and disclosure of your personal information by us as described in this Privacy Policy. This Privacy Policy is valid from 1 January 2010 and is effective upon acceptance for new users of our service.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

			<h3><a name="collection-and-storage"></a>Collection and storage</h3>

			<p>You can browse this site without telling us who you are or revealing any personal information about yourself. However, once you give us your personal information, you are no longer anonymous to us. If you choose to provide us with personal information, you expressly consent to us transferring that information to, and storing it on, servers located in the United Kingdom. We may collect and store the following personal information about you: email address, physical address, telephone number and other contact information; transactional information based on your activities on the site (such as information on your ordering of printed documents); postage, billing and other information you provide to purchase or post a document. Under no circumstances do we collect any financial information about you as all payment processing is conducted by a secure third party such as PayPal.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

			<h3><a name="marketing-by-third-parties"></a>Marketing by third parties</h3>

			<p>We don&#8217;t sell or rent your personal information to third parties for their marketing purposes without your explicit consent (see email marketing). However, we may aggregate your personal information with information on other users and information we collect from other companies (such as demographic data), and disclose such aggregated, non personal information to advertisers and other third parties.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

			<h3><a name="email-marketing"></a>Email marketing</h3>

			<p>When you place an order with Doxdirect you are asked to confirm (by ticking a checkbox) that you would like to receive marketing material from Doxdirect, or any of Doxdirect&#8217;s carefully selected partners. Any marketing material sent out will contain information relevant to Doxdirect&#8217;s activities.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

			<h3><a name="use-of-your-information"></a>Use of your information</h3>

			<p>Our primary purpose in collecting personal information is to provide you with a safe, smooth, efficient and customised experience.</p>

			<p>We may use your personal information to:</p>

			<ol>
				<li>Provide the services and customer support you request.</li>
				<li>Resolve disputes, remit commission payments (if you are a prime customer) and troubleshoot problems.</li>
				<li>Prevent potentially prohibited or illegal activities.</li>
				<li>Customise, measure and improve our services and the site&#8217;s content and layout.</li>
				<li>Tell you about our existing products and services, changes to our existing products and services, new products and services and promotional offers based on your notification preferences.</li>
			</ol>

			<p>If you do not wish to receive marketing communications from us, you have an opportunity to decline when you place an order with us for a document. If you are a prime customer your vendor agreement will provide you with an opportunity to decline.</p>

			<p>Google Analytics reporting on Demographics and Interests is enabled on our website. This information is used to develop more relevant products, offers and marketing for our customers. You can opt out of Google Analytics for Display Advertising in your <a  href="https://www.google.com/settings/ads" target="_blank">Ad Settings</a> and find out more by reading Google Analytics&#8217; <a  href="https://tools.google.com/dlpage/gaoptout/" target="_blank">currently available opt-outs</a> for the web.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

			<h3><a name="information-disclosure"></a>Our disclosure of your information</h3>

			<p>We may disclose your personal information to law enforcement agencies, other governmental agencies or third parties in response to a request for information (such as a Section 29 (3) Data Protection Act 1998 form) relating to a criminal investigation, alleged illegal activity or any other activity that may expose us or you to legal liability. The personal information we disclose may include your User ID and User ID history (if you are a prime customer), name, city, county, telephone number, email address, or anything else that we deem relevant.</p>

			<p>We may also disclose your personal information to other business entities, should we plan to merge with, or be acquired by that business entity.</p>

			<p>Should such a combination occur, we will require that the new combined entity follow this Privacy policy with respect to your personal information. If your personal information will be used contrary to this Privacy policy, you will receive prior notice.</p>

			<p>Without limiting the above, in an effort to respect your privacy, we will not otherwise disclose your personal information to third parties without a court order (or in circumstances where we are under a substantially similar legal obligation), except when we believe in good faith that the disclosure of such information is necessary to prevent imminent physical harm, property damage or financial loss or to report or put a stop to suspected illegal activity.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

			<h3><a name="data-controller"></a>Data controller</h3>

			<p>Doxdirect Limited is the sole data controller with regard to any processing of users&#8217; personal information.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

			<h3><a name="security"></a>Security</h3>

			<p>We use standard industry practices, including encryption, passwords and physical security, to protect your personal information against unauthorised access and disclosure. However, as you probably know, third parties may unlawfully intercept or access transmissions or private communications. Therefore, although we work very hard to protect your privacy, we do not promise, and you should not expect, that your personal information or private communications will always remain private.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

			<h3><a name="amendments"></a>Amendments to this Privacy policy</h3>

			<p>We may amend this Privacy policy at any time by posting the amended terms on the Site. All amended terms shall take effect automatically 30 days after they are initially posted on the site.</p>

			<a href="#contents"><i class="fa fa-arrow-up"></i> Contents...</a>

		</div>

	</div>

</div>