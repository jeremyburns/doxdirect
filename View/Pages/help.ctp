<div class="container">

	<div class="row">

		<div class="col-xs-24">

			<h1>Help</h1>

			<h2>Printing your documents with Doxdirect</h2>

			<p>Here&#8221;s the knowledge that will help you print your documents online.</p>

			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-info">
					<div class="panel-heading" role="tab" id="heading-help-printing-terminology">

						<a data-toggle="collapse" data-parent="#accordion-help" href="#help-printing-terminology" aria-expanded="false" aria-controls="help-printing-terminology">
							+ Printing terminology
						</a>

					</div>
					<div id="help-printing-terminology" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-help-printing-terminology">
						<div class="panel-body">

							<h4>Printing terminology used at Doxdirect</h4>

							<p>Below is a brief guide to some of the printing terminology we use on our website at Doxdirect.</p>

							<dl>
								<dt>Number of pages or printed sides</dt>
								<dd>It may be a little confusing as to what we actually mean when we refer to pages, sheets, sides or leaves, as finished electronic documents always refer to a side of text as a &#8216;page&#8217;. In order to help clarify matters, at Doxdirect we use the following terminology to refer to the number of sheets of paper or printed sides in a document or book.</dd>

								<dt>Sheet/s of paper = leaf or leaves</dt>
								<dd>Example: if you choose single sided printing, then the number of leaves in a document will be equal to the number of printed sides, but if you select double sided printing, then the number of leaves will be half the number of printed sides.</dd>

								<dt>Side/s = the number of printed sides</dt>
								<dd>Example: if you choose single sided printing, then the number of sides will be equal to the number of leaves in a document, but if you choose double sided printing, the number of leaves will be half the number of printed sides.</dd>

								<dt>Paper weight/paper quality = weight of paper</dt>
								<dd>The quality of paper is partly determined by its weight &#8211; the heavier the weight, the better the quality. The weight of the paper is measured in &#8216;gsm&#8217; which means grams per square metre. For example, 90gsm means 90 grams per square metre and 280gsm means 280 grams per square metre.</dd>

								<dt>Paper sizes</dt>
								<dd>At Doxdirect we work with the standard series A paper sizes, so, if possible, please ensure that any document you upload is a standard series A size. If, however, your document is not a standard series A size, it will be detected by our system on upload and you will be prompted to select which series A size you would like printed or to crop your artwork using your crop marks.</dd>

								<dt>FSC paper</dt>
								<dd>As part of our commitment to environmental sustainability, Doxdirect uses FSC certified paper. FSC stands for Forest Stewardship Council, and they are an international, non-governmental organisation dedicated to promoting responsible management of the world’s forests.</dd>

							</dl>

						</div>
					</div>
				</div>
				<div class="panel panel-info">
					<div class="panel-heading" role="tab" id="heading-how-printing-works">

						<a class="collapsed" data-toggle="collapse" data-parent="#accordion-help" href="#how-printing-works" aria-expanded="false" aria-controls="how-printing-works">
							Printing your documents online is simple
						</a>

					</div>
					<div id="how-printing-works" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-how-printing-works">
						<div class="panel-body">

							<p>Doxdirect provides a fast and affordable way of professionally printing and binding your documents in a range of different styles and delivering them directly to your door.</p>

							<h4>Step 1 &#8211; Upload your document and select your binding style</h4>

							<p>Click on the &#8216;Order Now&#8217; button found in the top banner of all our website pages. You will then be prompted to upload your document, either by selecting from your computer’s directory or by dragging and dropping the document into the provided box. Doxdirect accepts documents in PDF, Word and PowerPoint formats.</p>

							<p>You can then select your binding type from the list of options (e.g. spiral bound, saddle stitched brochure, paperback or hardback book etc). For full details of different products and binding types available please visit our products page.</p>

							<h4>Step 2 &#8211; Customise your document and set the number of copies</h4>

							<p>You can now customise your order with options like paper weight, covers and size to be printed. To see a preview of what your document will look like simply click on the link in the page.</p>

							<p>Available options for each binding type will automatically be displayed at this stage. You do not need to download or install any additional software in order to customise your order. At this stage you can also select how many copies of your order you wish to be printed.</p>

							<p>If you are ordering a hardback or paperback book, you will be prompted to use the Cover Design Tool to specify or create the type of cover you would like and also to add any titles to the cover or spine of your book.</p>

							<h4>Step 3 &#8211; Review your order</h4>

							<p>Make sure you&#8217;ve happy with your printing and binding specifications. If not, you can go back and change them by clicking the &#8216;Amend&#8217; button.</p>

							<h4>Step 4 &#8211; Delivery options and checkout</h4>

							<p>Select your delivery option and proceed to checkout, where you will be prompted to fill in your delivery details and billing information. Then sit back and wait for your order to arrive!</p>

						</div>
					</div>
				</div>
				<div class="panel panel-info">
					<div class="panel-heading" role="tab" id="heading-faqs">

						<a class="collapsed" data-toggle="collapse" data-parent="#accordion-help" href="#faqs" aria-expanded="false" aria-controls="faqs">
							Frequently asked questions
						</a>

					</div>
					<div id="faqs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-faqs">
						<div class="panel-body">
							<?php echo $this->element('global_content/faqs'); ?>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>
