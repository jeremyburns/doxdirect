<div class="container">

	<div class="row">

		<div class="col-xs-24">

			<h1>Admin dashboard</h1>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-8">

			<?php echo $this->element('dashboard/orders/quote/summary'); ?>

		</div>

		<div class="col-sm-8">

			<ul class="list-group">

				<?php echo $this->element('navigation/admin_sub/sales'); ?>

			</ul>

		</div>

		<div class="col-sm-8">

			<ul class="list-group">

				<?php echo $this->element('navigation/admin_sub/meta_data'); ?>

			</ul>

		</div>

	</div>

</div>