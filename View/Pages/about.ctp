<div class="container">

	<div class="row">

		<div class="col-xs-24">

			<h1>About Doxdirect</h1>

			<h2>Document Online Printing Services</h2>

			<p>Doxdirect is a quick, affordable and easy to use online document printing service for businesses and individuals. No need to register, and no minimum order. Simply upload your document, select your printing and binding options, tell us where you would like it to be sent, and you’re done!</p>

			<hr>

			<h2>Document printing</h2>

			<p>Our online document printing services are designed to be as simple as possible. We have an easy 3 step process that will enable you to upload your PDF, Word or PowerPoint file, then select from our range of printing options such as black and white or colour, single or double sided, plus your binding requirements. We use the latest digital printing techniques to ensure you&#8217;ll be getting quality as well as efficiency.</p>

			<ul>
				<li>Loose leaf</li>
				<li>Corner stapled</li>
				<li>Brochures</li>
				<li>Leaflets</li>
				<li>Flyers</li>
				<li>Posters</li>
			</ul>

			<hr>

			<h2>Document binding</h2>

			<p>Whether you’re binding a university dissertation or your latest corporate brochure, our extensive document binding service offers you a highly professional document with the swiftest possible service. We offer a range of document binding services including spiral binding, ring binding, saddle stitched books, paperback and hardback books. This gives you plenty of choice when it comes to the look and feel of your document. You can also choose the paper quality that your document is printed on, giving you maximum flexibility.</p>

			<ul>
				<li>Spiral binding</li>
				<li>Ring binding</li>
				<li>Saddle stitched books</li>
				<li>Paperback books</li>
				<li>Hardback books</li>
			</ul>

			<p>To get an instant quote for any of our binding options, please use the Price Calculator on the right of this page.</p>

		</div>

	</div>

</div>
