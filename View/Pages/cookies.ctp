<div class="container">

	<div class="row">

		<div class="col-xs-24">

			<h1>Cookie policy</h1>

			<h2>Our cookie policy</h2>

			<p>We use ‘cookies’ (small files placed on your computer’s hard drive) on certain pages of the site to help provide you with our service. Aside from on-demand printing customers, Doxdirect does not require customers to create an account to use our printing services. We therefore require the use of cookies to:</p>

			<ol>
				<li>Recognise you when you return to our website</li>
				<li>Monitor your progress through the printing processes to help ensure a smoother experience for you as a customer</li>
			</ol>

		</div>

	</div>

</div>