<?php
	if ($this->request->is('post') && $this->request->data) {
		extract($this->request->data);
	}
	$class = !empty($class)
		? $class
		: 'info'
	;
	$icon = !empty($icon)
		? $icon
		: 'fa fa-circle-o-notch fa-spin'
	;
	$message = !empty($message)
		? $message
		: 'Working...'
	;
?>
<div class="alert alert-<?php echo $class; ?>" role="alert">
	<i class="<?php echo $icon; ?>"></i> <?php echo $message; ?>
</div>