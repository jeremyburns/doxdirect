<div class="container">

	<div class="row">

		<div class="col-xs-24">

			<h1>Contact Doxdirect</h1>

			<p>If you are experiencing any technical problems, have any questions, would like to give us any feedback or leave us a comment we would love to hear from you!</p>

			<p>To find out more about who we are, what we’ve been up to and for ideas and inspiration for your printed products check out our <?php echo $this->Html->link('blog', '/blog'); ?>!</p>

			<h3>Telephone</h3>
			<p>Call us on <strong>0333 200 7272</strong>.</p>

			<p>Calls to 03 numbers cost no more than a national rate call to an 01 or 02 number and must count towards any inclusive minutes in the same way as 01 and 02 calls. These rules apply to calls from any type of line including mobile, BT, other fixed line or payphone.</p>

			<h3>Email</h3>
			<p><a href="mailto:service@doxdirect.com" target="_top">service@doxdirect.com</a></p>

			<div class="row">

				<div class="col-sm-12">

					<h3>Doxdirect Headquarters <small>(The printing team)</small></h3>
					<p>
						<address>
							Doxdirect Limited<br>
							Perth House<br>
							Perth Trading Estate<br>
							Slough<br>
							SL1 4XX
						</address>
					</p>

				</div>

				<div class="col-sm-12">

					<h3>Doxdirect Webquarters <small>(The website team)</small></h3>
					<p>
						<address>
							Doxdirect Limited<br>
							Unit 101<br>
							14 Greville Street<br>
							Farringdon<br>
							London<br>
							EC1N 8SB
						</address>
					</p>

				</div>

			</div>

			<h3>Social media</h3>

			<p>To talk with other people using Doxdirect services and to find our latest promo-codes and offers be sure to check our social media pages.</p>

			<span class="header-contact-icons">

				<a href="https://twitter.com/Doxdirect" target="_blank" class="mrs">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
					</span> Twitter
				</a>

				<a href="https://www.facebook.com/doxdirect" target="_blank" class="mrs">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
					</span> Facebook
				</a>

				<a href="https://plus.google.com/103282843558341836830/posts" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
					</span> Google+
				</a>
			</span>

			<hr>

			<h3>VAT number</h3>
			<p>GB 154 7372 00</p>

			<h3>Company number</h3>
			<p>8363111</p>

		</div>

	</div>

</div>