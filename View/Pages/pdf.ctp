<div class="container">

	<div class="row">

		<div class="col-xs-24 col-sm-20 col-sm-offset-2 col-md-16 col-md-offset-4 col-lg-12 col-lg-offset-6">

			<h1>We recommend you upload PDF documents</h1>

			<p class="lead">We can print your documents from Microsoft Word, Microsoft PowerPoint and Adobe PDF formats. It's important to us to make sure the printed item we send you matches as closely as possible the document you send us. For that reason we advise you to upload PDF documents where possible. Here's why.</p>

			<p>Our digital printing process ultimately prints from PDFs because they faithfully retain fonts, formats and layouts. This means the finished item we send you matches exactly what you send us.</p>

			<p>When you upload a document in another format (Microsoft Word or PowerPoint) we first need to convert it to PDF. Although we have a suite of computers with various versions of Microsoft Office software installed we cannot guarantee that we have an environment that matches yours, possibly because you have used a font we don't have.</p>

			<p>Because we cannot guarantee to convert your file exactly the way you intended your finished document to be, we encourgae you to output your document in PDF format and upload that in preference to the Word or PowerPoint version.</p>

			<h2>How to convert your document to PDF before uploading it</h2>

			<p>In Microsoft Office you have the option of saving your Word document or Excel spreadsheet as a PDF file. Just click on ‘Save As’ and PDF will appear as one of the options. (With some versions you may need to click 'Export' instead.)</p>

			<p>Alternatively, because Adobe Acrobat PDF editing software is not free of charge, there are several free PDF converters available online. If you search for 'free PDF convertor' you should get plenty of results.</p>

			<p>Please note: Doxdirect has no connection with or responsibility for external websites. We are simply providing information on where and how you may be able to obtain software to enable you to convert any of your documents to a PDF format.</p>

		</div>

	</div>

</div>
