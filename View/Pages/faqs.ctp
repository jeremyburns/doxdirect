<div class="container">

	<div class="row">

		<div class="col-xs-24">

			<h1>Frequently Asked Questions</h1>

			<p>Here are some answers to the questions we are often asked.</p>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-6">

			<h3><a name="contents">Contents</a></h3>

			<ul class="list-unstyled list-divided">
				<li><a href="#document-types">Document types</a></li>
				<li><a href="#non-standard-document-sizes">Document sizes</a></li>
				<li><a href="#convert-xl-to-pdf">Converting Excel docs to PDF</a></li>
				<li><a href="#pdf-not-uploading">My PDF isn't uploading</a></li>
				<li><a href="#unable-to-process-this-document">Unable to process this document error</a></li>
				<li><a href="#file-size-limits">File size limits</a></li>
				<li><a href="#cover-design-tool">Cover deisgn tool issues</a></li>
				<li><a href="#hole-punching-options">Hole punching options for ring binding</a></li>
				<li><a href="#processing-delivery-times">Processing and delivery times</a></li>
				<li><a href="#standard-processing-time">Standard processing time</a></li>
				<li><a href="#public-holidays-and-weekends">Public holidays and weekends</a></li>
				<li><a href="#next-day-delivery">Next day delivery</a></li>
			</ul>

		</div>

		<div class="col-sm-18">

			<?php echo $this->element('global_content/faqs'); ?>

		</div>

	</div>

</div>