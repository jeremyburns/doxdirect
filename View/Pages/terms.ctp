<div class="container">

	<div class="row">

		<div class="col-xs-24">

			<h1>Terms and conditions</h1>

			<h2>For customers of Doxdirect</h2>

			<p>The following Terms and Conditions constitute the entire agreement between the parties and supersede any previous agreements, warranties, representations, undertakings or understandings between the parties and may not be varied except in writing.</p>

			<h3>1. Definitions</h3>

			<ol>
				<li>&#8220;Doxdirect&#8221; means the party providing the goods or services under these Terms and Conditions.</li>
				<li>&#8220;You&#8221;, &#8220;Your&#8221; refers to the party contracting with Doxdirect to acquire a printed Document supplied under these Terms and Conditions.</li>
				<li>&#8220;Document&#8221; means the document selected by You (and any number of additional copies) to be printed by Doxdirect.</li>
				<li>&#8220;Intermediates&#8221; means all products produced during the manufacturing process including non-exhaustively discs, film, plate, intellectual property.</li>
				<li>&#8220;Electronic File&#8221; means the electronic form of the Document You have selected for printing.</li>
				<li>&#8220;Insolvency&#8221; means Doxdirect is in a position where it is unable to pay its debts or has a winding up petition issued against it or has a receiver, administrator or administrative receiver appointed to it or has a bankruptcy petition issued against it.</li>
			</ol>

			<h3>2. Payment</h3>

			<p>You have to pay online and in advance for the printed copy of the Document.</p>

			<h3>3. Delivery</h3>

			<ol>
				<li>You have to choose the delivery method for your Document when you place an order.</li>
				<li>Delivery times quoted to You by Doxdirect are a guide only and, whilst Doxdirect will make every effort to adhere to proposed timescales, time is not of the essence in any contract with You.</li>
				<li>The Document is deemed as delivered when a) it is dispatched by ordinary post or b) when it is collected by courier. Should you not receive the Document and need to trace it (and have chosen courier as the delivery method), contact the email address <a href="mailto:service@doxdirect.com" target="_top">service@doxdirect.com</a>, quoting your order number.</li>
			</ol>

			<h3>4. Electronic Files</h3>

			<ol>
				<li>Doxdirect shall not be responsible for checking the accuracy of supplied input from an Electronic File. That is Your responsibility.</li>
				<li>Without prejudice to clause 4, if an Electronic File is not suitable for outputting on equipment normally adequate for such purposes without adjustment or other corrective action, Doxdirect may make a charge for any resulting additional cost incurred or may reject the file without prejudice to Doxdirect&#8217;s rights to payment for Document done/material purchased.</li>
				<li>Without prejudice to clause 4, where materials are so supplied or specifed, and Doxdirect so advises You, and You instruct Doxdirect in writing or by email to proceed anyway, Doxdirect will use reasonable endeavours to secure the best results, but shall incur no liability for the quality of the end product(s).</li>
			</ol>

			<h3>5. Finished goods</h3>

			<ol>
				<li>The risk in the Document shall pass to You on dispatch and You should insure accordingly.</li>
				<li>On completion of the printing of the Document, Doxdirect destroys any Electronic Files associated with the Document without further notice.</li>
			</ol>

			<h3>6. Retention of title</h3>

			<p>The Document remains the property of its copyright holder at all times. It is Your responsibility to ensure that You hold copyright, or have permission from the copyright holder, for reproduction of all text, images, designs and any other intellectual property contained in all Electronic Files uploaded to www.doxdirect.com for printing and/or binding.</p>

			<h3>7. Claims</h3>

			<ol>
				<li>Advice of damage, delay or loss of goods in transit or of non-delivery must be given in writing to Doxdirect and the carrier within 3 clear days of delivery (or, in the case of non-delivery, within 3 days of notification of despatch of the goods) and any claim in respect thereof must be made in writing to Doxdirect and the carrier within 7 clear days of delivery (or, in the case of non-delivery, within 7 days of notification of despatch). All other claims must be made in writing to Doxdirect within 14 days of delivery. Doxdirect shall not be liable in respect of any claim unless the aforementioned requirements have been complied with except in any particular case where You prove that (i) it was not possible to comply with the requirements and (ii) the claim was made as soon as reasonably possible.</li>
				<li>If the Document is defective so that You may in law reject it, said rejection must take place within 7 days of delivery of the goods, failing which You will be deemed to have accepted the Document.</li>
				<li>In the event of all or any claims or rejections, Doxdirect reserves the right to inspect the Document within 7 days of the claim or rejection being notified.</li>
			</ol>

			<h3>8. Liability</h3>

			<ol>
				<li>Insofar as is permitted by law where the Document is defective for any reason, including negligence, Doxdirect&#8217;s liability (if any) shall be limited to rectifying such defect, or crediting its value against any invoice raised in respect of the Document.</li>
				<li>Where Doxdirect performs its obligations to rectify a defective Document under this condition Doxdirect shall not be liable for indirect loss, consequential loss or third party claims occasioned by a defective Document and You shall not be entitled to any further claim in respect of the Document nor shall You be entitled to repudiate the contract.</li>
				<li>A defective Document must be returned to Doxdirect at Your own expense before a replacement or credits can be issued. If the subject Document is not available to Doxdirect, Doxdirect will hold that You have accepted the Document and no credits or replacement Document will be provided.</li>
				<li>Doxdirect shall not be liable for indirect loss, consequential loss or third party claims occasioned by delay in completing the Document or for any loss to You arising from delay in transit, whether as a result of Doxdirect&#8217;s negligence or otherwise.</li>
				<li>Where Doxdirect Limited offers to replace a defective Document You must accept such an offer unless You can show clear cause for refusing so to do. If You opt to have the Document re-done by any third party without reference to Doxdirect Limited You automatically revoke Your right to any remedy from Doxdirect Limited, including but not exclusively the right to a credit in respect of the Document produced by Doxdirect Limited.</li>
				<li>Doxdirect reserves the right to reject any Electronic File forwarded to them even after payment has been made. In the event that an Electronic File is rejected Doxdirect will issue a refund for the order.</li>
			</ol>

			<h3>9. Illegal matter</h3>

			<ol>
				<li>Doxdirect shall not be required to print any matter which in their opinion is or may be of an illegal, libellous or offensive nature, or an infringement of the proprietary or other rights of any third party.</li>
				<li>Doxdirect shall be indemnified by You in respect of any claims, costs and expenses arising out of the printing by Doxdirect for You of any illegal or unlawful matter including matter which is libellous, offensive or infringes copyright, patent, design or any other proprietary or personal rights. The indemnity shall include (without limitation) any amounts paid on a lawyer&#8217;s advice in settlement of any claim that any matter is libellous or such an infringement.</li>
			</ol>

			<h3>10. Cancellations</h3>

			<p>As our service is one which is to be provided within a period of less than 7 working days the right to a “cooling-off” period under the Distance Selling Regulations does not apply. Therefore, if you wish to cancel your order you must do so before we begin to process your order. As we process most orders shortly after receipt of instructions you must send an email to <a href="mailto:cancellations@doxdirect.com" target="_top">cancellations@doxdirect.com</a>, quoting your order number. Cancellation can only be accepted if we have not already begun to process your order. Emails sent to other email addresses are not likely to be seen in time.</p>

			<h3>11. Force majeure</h3>

			<p>Doxdirect shall be under no liability if they are unable to carry out any provision of the contract for any reason beyond their reasonable control including (without limiting the foregoing): Act of God; legislation; war; fire;flood; drought; inadequacy or unsuitability of any instructions, Electronic File or other data or materials supplied by You; failure of power supply; lock-out, strike or other action taken by employees in contemplation or furtherance of a dispute; or owing to any inability to procure materials required for the performance of the contract. During the continuance of such a contingency You may by written notice to Doxdirect elect to terminate the contract, but subject thereto shall otherwise accept delivery when available.</p>

			<h3>12. Data protection</h3>

			<p>Refer to the Doxdirect privacy policy.</p>

			<h3>13. Law</h3>

			<p>These conditions and all other express and implied Terms of the contract shall be governed and construed in accordance with the laws of England and the parties agree to submit to the jurisdiction of the courts of England and Wales.</p>

			<h3>14. Notices</h3>

			<p>All specifications and notices relied on by either party and all variations to this agreement must be in writing and include a duly authorised signature.</p>

			<h3>15. Consumers</h3>

			<p>Nothing in these Terms shall affect the rights of Consumers.</p>

			<h3>16. Severability</h3>

			<p>All clauses and sub-clauses of this Agreement are severable and if any clause or identifiable part thereof is held to be unenforceable by any court of competent jurisdiction then such enforceability shall not affect the enforceability of the remaining provisions or identifiable parts thereof in these Terms and Conditions.</p>

		</div>

	</div>

</div>