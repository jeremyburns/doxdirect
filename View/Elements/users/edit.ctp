<h4>Update your details</h4>

<?php echo $this->Form->create(
	'User',
	[
		'role' => 'form',
		'class' => 'form-horizontal'
	]
);

echo $this->BootstrapForm->horizontalInputs([
	'options' => [
		'divClass' => 'col-sm-'
	],
	'inputs' => [
		'username' => [
			'label' => 'Username (your email address)',
			'required' => true,
			'value' => $user['User']['username'],
			'width-class-label' => THIRD,
			'width-class-input' => TWOTHIRDS
		],
		'first_name' => [
			'label' => 'First name',
			'required' => true,
			'value' => $user['User']['first_name'],
			'width-class-label' => THIRD,
			'width-class-input' => TWOTHIRDS
		],
		'last_name' => [
			'label' => 'Last name',
			'value' => $user['User']['last_name'],
			'required' => true,
			'width-class-label' => THIRD,
			'width-class-input' => TWOTHIRDS
		],
		'phone' => [
			'label' => 'Phone',
			'value' => $user['User']['phone'],
			'required' => true,
			'width-class-label' => THIRD,
			'width-class-input' => TWOTHIRDS
		]
	]
]);

	echo $this->Form->button(
		'<i class="fa fa-plus-circle"></i> Save',
		array(
			'type' => 'submit',
			'class' => 'btn btn-default btn-success',
			'escape' => false
		)
	);

	echo $this->Html->link(
		'<i class="fa fa-undo"></i> Cancel',
		[
			'admin' => false,
			'controller' => 'orders',
			'action' => 'resume'
		],
		array(
			'class' => 'btn btn-link',
			'escape' => false,
			'data-action' => 'resume'
		)
	);

echo $this->Form->end();
