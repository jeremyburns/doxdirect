<span data-content="login-link">

	<?php

		if (empty($loggedInUser) || empty($loggedInUser['User']['user_supplied_password'])) {

			echo $this->Html->link(
				'<i class="fa fa-sign-in"></i> Customer Login',
				['admin' => false, 'controller' => 'users', 'action' => 'login'],
				array('escape' => false)
			);

		} else {

			echo $this->Html->link(
				$loggedInUser['User']['username'],
				['admin' => false, 'controller' => 'users', 'action' => 'view']
			);

			echo $this->Html->link(
				'<i class="fa fa-sign-out"></i> Logout',
				['admin' => false, 'controller' => 'users', 'action' => 'logout'],
				array('escape' => false)
			);

		}

	?>

</span>
