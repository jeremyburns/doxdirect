<?php echo $this->Flash->render(); ?>

<span id="login-flash-alert" style="display: none;">
	<p id="login-flash-message" class="alert"></p>
</span>

<div class="row">

	<?php echo $this->Form->create(
		'User',
		array(
			'action' => 'login',
			'id' => 'LoginForm',
			'class' => 'form-horizontal'
		)
	);

		echo $this->BootstrapForm->horizontalInputs(array(
			'options' => array(
				'divClass' => 'col-sm-'
			),
			'inputs' => array(
				'username' => array(
					'label' => __d('users', 'Username'),
					'type' => 'text',
					'placeHolder' => 'Username (your email)',
					'default' => null,
					'required' => true,
					'width-class-label' => THIRD,
					'width-class-input' => HALF
				),
				'password' => array(
					'label' => __d('users', 'Password'),
					'type' => 'password',
					'placeHolder' => null,
					'default' => null,
					'required' => true,
					'width-class-label' => THIRD,
					'width-class-input' => HALF
				)
			)
		)); ?>

		<div class="col-sm-20 col-sm-offset-<?php echo THIRD; ?>">

			<?php

				echo $this->Form->button(
					'<i class="fa fa-sign-in"></i> Login',
					array(
						'type' => 'submit',
						'class' => 'btn btn-success',
						'id' => 'login-btn',
						'escape' => false
					)
				);

				echo $this->Html->link(
					'<i class="fa fa-question-circle"></i> I forgot my password',
					['admin' => false, 'controller' => 'users', 'action' => 'request_password_reset'],
					array(
						'class' => 'btn btn-link',
						'escape' => false
					)
				);

				if (!empty($showCancelLink)) {

					echo $this->Html->link(
						'<i class="fa fa-undo"></i> Cancel',
						['admin' => false, 'controller' => 'users', 'action' => 'cancel_login'],
						array(
							'id' => 'cancel-login-link',
							'class' => 'btn btn-link',
							'escape' => false
						)
					);
				}

			?>

		</div>

	<?php echo $this->Form->end(); ?>

</div>
