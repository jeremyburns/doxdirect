<h3><?php echo __('Order history'); ?></h3>

<?php if (empty($orders)) { ?>

	<p class="alert alert-info-pale">
		You have no orders
	</p>

<?php } else { ?>

	<table class="table">

		<thead>

			<tr>
				<th>Date</th>
				<th class="text-right"><?php echo __('Net'); ?></th>
				<th class="text-right"><?php echo __('Delivery'); ?></th>
				<th class="text-right"><?php echo __('Processing'); ?></th>
				<th class="text-right"><?php echo __('Discount'); ?></th>
				<th class="text-right"><?php echo __('VAT'); ?></th>
				<th class="text-right"><?php echo __('Total'); ?></th>
				<th class="text-right"><?php echo __('Paid'); ?></th>
				<th class="text-right"><?php echo __('Due'); ?></th>
				<th><?php echo __('Status'); ?></th>
			</tr>

		</thead>

		<tbody>

			<?php foreach ($orders as $order): ?>
				<tr>
					<td><?php echo $this->Presentation->niceDate($order['created']); ?></td>
					<td class="text-right"><?php echo $this->Number->format($order['amount_net'], ['before' => '£', 'places' => 2]); ?></td>
					<td class="text-right"><?php echo $this->Number->format($order['amount_delivery'], ['zero' => 'FREE', 'before' => '£', 'places' => 2]); ?></td>
					<td class="text-right"><?php echo $this->Number->format($order['amount_turnaround'], ['before' => '£', 'places' => 2]); ?></td>
					<td class="text-right"><?php echo $this->Number->format($order['amount_discount'], ['before' => '£', 'places' => 2]); ?></td>
					<td class="text-right"><?php echo $this->Number->format($order['amount_vat'], ['before' => '£', 'places' => 2]); ?></td>
					<td class="text-right"><?php echo $this->Number->format($order['amount_total'], ['before' => '£', 'places' => 2]); ?></td>
					<td class="text-right"><?php echo $this->Number->format($order['amount_paid'], ['before' => '£', 'places' => 2]); ?></td>
					<td class="text-right"><?php echo $this->Number->format($order['amount_due'], ['before' => '£', 'places' => 2]); ?></td>
					<td><?php echo $order['OrderStatus']['name']; ?></td>
				</tr>
			<?php endforeach; ?>

		</tbody>

	</table>

<?php } ?>
