<?php

	function tabClass($thisStep, $currentStep) {

		if ($thisStep == $currentStep) {
			return 'active';
		} elseif ($thisStep > $currentStep) {
			return 'disabled';
		} else {
			return 'completed';
		}

	}

?>

<div class="container mts mbs">
	<div class="row">
		<div class="col-sm-24">
			<div class="steps">

				<div class="<?php echo tabClass(1, $currentStep); ?> step">
					<div class="content">
						<div class="title"><i class="fa fa-cloud-upload"></i> Upload</div>
						<div class="description">Upload your files</div>
					</div>
				</div>

				<div class="<?php echo tabClass(2, $currentStep); ?> step">
					<div class="content">
						<div class="title"><i class="fa fa-wrench"></i> Configure</div>
						<div class="description">Choose your options</div>
					</div>
				</div>

				<div class="<?php echo tabClass(3, $currentStep); ?> step">
					<div class="content">
						<div class="title"><i class="fa fa-shopping-cart"></i> Checkout</div>
						<div class="description">Review and pay</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
