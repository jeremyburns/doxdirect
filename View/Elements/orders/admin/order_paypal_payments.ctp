<table class="table table-bordered table-condensed">

	<caption>PayPal payments</caption>

	<thead>

		<tr>
			<th>Date</th>
			<th>PayPal TXN id</th>
			<th>Payer email</th>
			<th>Item no</th>
			<th>Status</th>
			<th class="text-right">Amount received</th>
		</tr>

	</thead>

	<tbody>

		<?php if (empty($order['PaypalPayment'])) { ?>

			<tr>
				<td colspan="3">There are no PayPal payments.</td>
			</tr>

		<?php } else {

			$paypalPayments = $order['PaypalPayment'];

			foreach ($paypalPayments as $paypalPayment) { ?>

				<tr>
					<td><?php echo $this->Presentation->niceDate($paypalPayment['created']); ?></td>
					<td><?php echo $paypalPayment['txn_id']; ?></td>
					<td><?php echo $paypalPayment['payer_email']; ?></td>
					<td><?php echo $paypalPayment['item_number']; ?></td>
					<td><?php echo $paypalPayment['payment_status']; ?></td>
					<td class="text-right"><?php echo $this->Number->currency($paypalPayment['mc_gross'], $paypalPayment['mc_currency']); ?></td>
				</tr>

			<?php }

		} ?>

	</tbody>

</table>
