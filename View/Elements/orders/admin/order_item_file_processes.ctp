<table class="table table-condensed">

	<caption><small>Processes</small></caption>

	<thead>
		<tr>
			<th>Seq</th>
			<th>Name</th>
			<th>Profile name</th>
			<th>Parameters</th>
			<th class="text-center">Complete</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach ($orderItemFileProcesses as $orderItemFileProcess) { ?>

		<tr>
			<td><?php echo $orderItemFileProcess['sequence']; ?></td>
			<td><?php echo $orderItemFileProcess['Process']['name']; ?></td>
			<td><?php echo $orderItemFileProcess['Process']['profile_name']; ?></td>
			<td><small><code><?php echo $orderItemFileProcess['config']; ?></code></small></td>
			<td class="text-center"><?php echo $this->Presentation->yesNo($orderItemFileProcess['is_complete']); ?></td>
		</tr>

		<?php } ?>

	</tbody>

</table>
