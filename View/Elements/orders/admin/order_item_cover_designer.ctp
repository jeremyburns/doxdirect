<table class="table table-bordered table-condensed">

	<caption>Cover designer</caption>

	<thead>

		<tr>
			<th>Job ID</th>
			<th>Options, front</th>
			<th>Options, back</th>
		</tr>

	</thead>

	<tbody>

		<tr>
			<td><?php echo $orderItem['printed_cover_job_id']; ?></td>
			<td><?php echo $orderItem['printed_cover_front_options']; ?></td>
			<td><?php echo $orderItem['printed_cover_back_options']; ?></td>
		</tr>

	</tbody>

</table>

