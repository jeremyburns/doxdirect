<table class="table table-bordered table-condensed">

	<caption>Promo code</caption>

	<tbody>

		<?php if (empty($order['Order']['promo_code_id'])) { ?>

			<tr>
				<td>None applied</td>
			</tr>

		<?php } else {

			$promoCode = $order['PromoCode']; ?>

			<tr>
				<th>Code</th>
				<td>
					<?php
						echo $this->Html->link(
							$promoCode['code'],
							[
								'admin' => true,
								'controller' => 'promo_codes',
								'action' => 'view',
								$promoCode['id']
							]
						);
					?>
				</td>
			</tr>

			<tr>
				<th>Name</th>
				<td><?php echo $promoCode['name']; ?></td>
			</tr>

			<tr>
				<th>Description</th>
				<td><?php echo $promoCode['description']; ?></td>
			</tr>

			<tr>
				<th>Applies to</th>
				<td><?php echo $promoCode['apply_discount_to']; ?></td>
			</tr>

			<tr>
				<th>Discount type</th>
				<td><?php echo $promoCode['discount_type']; ?></td>
			</tr>

			<tr>
				<th>Discount amount</th>
				<td><?php echo $promoCode['discount_amount']; ?></td>
			</tr>

		<?php } ?>

	</tbody>

</table>
