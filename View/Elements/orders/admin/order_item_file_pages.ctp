<table class="table table-condensed">

	<caption><small>Pages</small></caption>

	<thead>
		<tr>
			<th>Page</th>
			<th>Size box</th>
			<th>Width</th>
			<th>Height</th>
			<th>Paper</th>
			<th>Paper match</th>
			<th>Orientation</th>
			<th>Plates</th>
			<th>Colour</th>
			<th>Trims</th>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($orderItemFilePages as $orderItemFilePage) { ?>
			<tr>
				<td><?php echo $orderItemFilePage['callas_id']; ?></td>
				<td><?php echo $orderItemFilePage['size_box']; ?></td>
				<td><?php echo $orderItemFilePage['width']; ?></td>
				<td><?php echo $orderItemFilePage['height']; ?></td>
				<td><?php echo $orderItemFilePage['paper_size_id']; ?></td>
				<td><?php echo $orderItemFilePage['paper_size_match']; ?></td>
				<td><?php echo $orientations[$orderItemFilePage['orientation']]; ?></td>
				<td><?php echo $orderItemFilePage['plates']; ?></td>
				<td><?php echo $orderItemFilePage['colour']; ?></td>
				<td><?php echo $this->Presentation->yesNo($orderItemFilePage['has_trim_marks']); ?></td>
			</tr>

			<?php if (!empty($orderItemFilePage['OrderItemFilePageProcess'])) { ?>

				<tr>
					<td></td>

					<td colspan="8">

						<?php echo $this->element(
							'orders/admin/order_item_file_page_processes',
							array(
								'orderItemFilePageProcesses' => $orderItemFilePage['OrderItemFilePageProcess']
							)
						); ?>

					</td>

				</tr>

			<?php }

		} ?>

	</tbody>

</table>