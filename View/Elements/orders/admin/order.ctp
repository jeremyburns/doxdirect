<table class="table table-bordered table-condensed">

	<caption>Order</caption>

	<tfoot>

		<tr>

			<td colspan="3">

				<div class="btn-group">

					<?php
						echo $this->Form->postLink(
							'<i class="fa fa-cogs"></i> Process original files',
							[
								'admin' => true,
								'controller' => 'orders',
								'action' => 'process_original_files',
								$order['Order']['id']
							],
							[
								'class' => 'btn btn-sm btn-default',
								'escape' => false,
								'confirm' => 'Are you sure you want to process the original files for this job?'
							]
						);

						echo $this->Form->postLink(
							'<i class="fa fa-file-archive-o"></i> Create job archive',
							[
								'admin' => true,
								'controller' => 'orders',
								'action' => 'create_job_archive',
								$order['Order']['id']
							],
							[
								'class' => 'btn btn-sm btn-default',
								'escape' => false,
								'confirm' => 'Are you sure you want to create the archive for this job?'
							]
						);
					?>

				</div>

			</td>

		</tr>

	</tfoot>

	<tbody>

		<tr>
			<th>Id</th>
			<td colspan="2"><?php echo $order['Order']['id']; ?></td>
		</tr>

		<tr>
			<th rowspan="5">Pace</th>
		</tr>

		<tr>
			<th>Quote id:</th>
			<td><?php echo $order['Order']['pace_quote_id']; ?></td>
		</tr>

		<tr>
			<th>Job number:</th>
			<td><?php echo $order['Order']['pace_job_number']; ?></td>
		</tr>

		<tr>
			<th>Original files processed:</th>
			<td>
				<?php
					if (!empty($order['Order']['original_files_processed'])) {
						echo $this->Time->niceShort($order['Order']['original_files_processed']);
					}
				?>
			</td>
		</tr>

		<tr>
			<th>Job archive created:</th>
			<td>
				<?php
					if (!empty($order['Order']['pace_archive_created'])) {
						echo $this->Time->niceShort($order['Order']['pace_archive_created']);
					}
				?>
			</td>
		</tr>

		<tr>
			<th rowspan="4">Status</th>
		</tr>

		<tr>
			<th>Order:</th>
			<td>
				<?php
					echo $this->Html->link(
						'<i class="fa ' . $order['OrderStatus']['icon'] . '"></i> ' . $order['OrderStatus']['name'],
						[
							'admin' => true,
							'controller' => 'order_statuses',
							'action' => 'view',
							$order['Order']['order_status_id']
						],
						[
							'escape' => false
						]
					);
				?>
			</td>

		</tr>

		<tr>
			<th>Job:</th>
			<td>
				<?php
					echo $this->Html->link(
						'<i class="fa ' . $order['JobStatus']['icon'] . '"></i> ' . $order['JobStatus']['name'],
						[
							'admin' => true,
							'controller' => 'job_statuses',
							'action' => 'view',
							$order['Order']['job_status_id']
						],
						[
							'escape' => false
						]
					);
				?>
			</td>

		</tr>

		<tr>
			<th>Latest message:</th>
			<td>
				<?php
					if (!empty($order['Order']['job_status_message'])) {
						echo $order['Order']['job_status_message'];
					}
				?>
			</td>
		</tr>

		<tr>
			<th>Turnaround</th>
			<td>
				<?php
					echo $this->Html->link(
						$order['TurnaroundOption']['name'],
						array(
							'admin' => true,
							'controller' => 'turnaround_options',
							'action' => 'view',
							$order['Order']['turnaround_option_id']
						)
					);
				?>
			</td>
			<td><?php echo $this->Presentation->niceDate($order['Order']['despatch_date'], false); ?></td>
		</tr>

		<tr>
			<th>Delivery:</th>
			<td>
				<?php
					echo $this->Html->link(
						$order['DeliveryOption']['name'],
						array(
							'admin' => true,
							'controller' => 'delivery_options',
							'action' => 'view',
							$order['Order']['delivery_option_id']
						)
					);
				?>
			</td>
			<td><?php echo $this->Presentation->niceDate($order['Order']['delivery_date'], false); ?></td>
		</tr>

		<tr>
			<th>Shipping address:</th>
			<td colspan="2">
				<?php
					if (!empty($order['ShippingAddress']['id'])) {
						echo $this->Dox->address($order['ShippingAddress']);
					}
				?>
			</td>
		</tr>

		<tr>
			<th>Billing address:</th>
			<td colspan="2">
				<?php
					if ($order['Order']['billing_same_as_shipping']) { ?>
						Same as shipping
					<?php }

					if (!empty($order['BillingAddress']['id'])) {
						echo $this->Dox->address($order['BillingAddress']);
					}
				?>
			</td>
		</tr>

		<tr>
			<th><?php echo __('Created'); ?></th>
			<td colspan="2"><?php echo h($this->Time->niceShort($order['Order']['created'])); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td colspan="2"><?php echo h($this->Time->niceShort($order['Order']['modified'])); ?></td>
		</tr>

	</tbody>

</table>
