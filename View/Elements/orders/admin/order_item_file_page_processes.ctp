<table class="table table-condensed">

	<caption><small>Processes</small></caption>

	<thead>
		<tr>
			<th>Seq</th>
			<th>Name</th>
			<th>Profile name</th>
			<th>Parameters</th>
			<th class="text-center">Complete</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach ($orderItemFilePageProcesses as $orderItemFilePageProcess) { ?>

			<tr>
				<td><?php echo $orderItemFilePageProcess['sequence']; ?></td>
				<td><?php echo $orderItemFilePageProcess['Process']['name']; ?></td>
				<td><?php echo $orderItemFilePageProcess['Process']['profile_name']; ?></td>
				<td><small><code><?php echo $orderItemFilePageProcess['config']; ?></code></small></td>
				<td class="text-center"><?php echo $this->Presentation->yesNo($orderItemFilePageProcess['is_complete']); ?></td>
			</tr>

		<?php } ?>

	</tbody>

</table>
