<table class="table table-bordered table-condensed">

	<caption>Order history</caption>

	<thead>

		<tr>
			<th>Date</th>
			<th>Status</th>
		</tr>

	</thead>

	<tbody>

		<?php foreach ($orderDates as $orderDate) { ?>

			<tr>

				<td><?php echo h($this->Time->niceShort($orderDate['created'])); ?></td>
				<td>
					<?php
						echo $this->Html->link(
							$orderDate['OrderStatus']['name'],
							array(
								'admin' => true,
								'controller' => 'order_statuses',
								'action' => 'view',
								$orderDate['order_status_id']
							)
						);
					?>
				</td>

			</tr>

		<?php } ?>

	</tbody>

</table>