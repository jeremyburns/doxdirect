<table class="table table-bordered table-condensed">

	<caption>Tabs</caption>

	<thead>

		<tr>
			<th>Before page</th>
			<th>Text</th>
		</tr>

	</thead>

	<tbody>

		<?php if (empty($orderItemTabs)) { ?>

			<tr>
				<td colspan="2">There are no tabs.</td>
			</tr>

		<?php } else {

			foreach ($orderItemTabs as $orderItemTab) { ?>

				<tr>

					<td><?php echo h($orderItemTab['before_page']); ?></td>
					<td><?php echo h($orderItemTab['text']); ?></td>

				</tr>

			<?php }

		} ?>

	</tbody>

</table>
