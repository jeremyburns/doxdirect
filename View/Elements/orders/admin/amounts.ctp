<table class="table table-bordered table-condensed">

	<caption>Amounts</caption>

	<tbody>

		<tr>

			<th>Net</th>
			<td class="text-right">
				<?php
					echo $this->Number->format(
						$order['Order']['amount_net'],
						[
							'before' => '£',
							'places' => 2
						]
					);
				?>
			</td>

		</tr>

		<tr>

			<th>Delivery</th>
			<td class="text-right">
				<?php
					echo $this->Number->format(
						$order['Order']['amount_delivery'],
						[
							'before' => '£',
							'places' => 2
						]
					);
				?>
			</td>

		</tr>

		<tr>

			<th>Turnaround</th>
			<td class="text-right">
				<?php
					echo $this->Number->format(
						$order['Order']['amount_turnaround'],
						[
							'before' => '£',
							'places' => 2
						]
					);
				?>
			</td>

		</tr>

		<tr>

			<th>

			<?php if (!empty($order['PromoCode']['code'])) {

				$promoCodeLink = $this->Html->link(
					$order['PromoCode']['code'],
					[
						'admin' => true,
						'controller' => 'promo_codes',
						'action' => 'view',
						$order['PromoCode']['id']
					]
				);

				echo sprintf('Discount (%s)', $promoCodeLink);

			} else { ?>
				Discount
			<?php } ?>

			<td class="text-right">
				<?php
					echo $this->Number->format(
						$order['Order']['amount_discount'],
						[
							'before' => '£',
							'places' => 2
						]
					);
				?>
			</td>

		</tr>

		<tr>

			<th>VAT</th>
			<td class="text-right">
				<?php
					echo $this->Number->format(
						$order['Order']['amount_vat'],
						[
							'before' => '£',
							'places' => 2
						]
					);
				?>
			</td>

		</tr>

		<tr>

			<th>Total</th>
			<td class="text-right amount-total">
				<?php
					echo $this->Number->format(
						$order['Order']['amount_total'],
						[
							'before' => '£',
							'places' => 2
						]
					);
				?>
			</td>

		</tr>

		<tr>

			<th>Paid</th>
			<td class="text-right">
				<?php
					echo $this->Number->format(
						$order['Order']['amount_paid'],
						[
							'before' => '£',
							'places' => 2
						]
					);
				?>
			</td>

		</tr>

		<tr>

			<th>Due</th>
			<?php
				$class =  $order['Order']['amount_due']
					? ' amount-outstanding'
					: ''
				;
			?>
			<td class="text-right<?php echo $class; ?>">
				<?php
					echo $this->Number->format(
						$order['Order']['amount_due'],
						[
							'before' => '£',
							'places' => 2
						]
					);
				?>
			</td>

		</tr>

	</tbody>

</table>
