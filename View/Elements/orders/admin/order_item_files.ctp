<table class="table table-condensed">

	<caption>Uploaded files</caption>

	<tbody>

		<?php if (empty($orderItemFiles)) { ?>

			<tr>
				<td>There are no uploaded files.</td>
			</tr>

		<?php } else {

			foreach ($orderItemFiles as $orderItemFile) { ?>

				<tr>
					<th>Seq</th>
					<th>Name</th>
					<th>Type</th>
					<th>Size</th>
					<th>Paper</th>
					<th>Paper match</th>
					<th>Pages</th>
					<th>Col</th>
					<th>Orientation</th>
					<th>P</th>
					<th>L</th>
					<th>Trims</th>
					<th>To Callas</th>
					<th>Callas resp</th>
				</tr>

				<tr>
					<td><?php echo $orderItemFile['sequence']; ?></td>
					<td><?php echo $orderItemFile['name']; ?></td>
					<td><?php echo $orderItemFile['type']; ?></td>
					<td><?php echo $orderItemFile['readable_size']; ?></td>
					<td><?php echo $orderItemFile['paper_size_id']; ?></td>
					<td><?php echo $orderItemFile['paper_size_match']; ?></td>
					<td><?php echo $orderItemFile['count_order_item_file_page']; ?></td>
					<td><?php echo $orderItemFile['colour']; ?></td>
					<td><?php echo $orientations[$orderItemFile['orientation']]; ?></td>
					<td><?php echo $orderItemFile['orientation_count_p']; ?></td>
					<td><?php echo $orderItemFile['orientation_count_l']; ?></td>
					<td class="text-center"><?php echo $this->Presentation->yesNo($orderItemFile['has_trim_marks']); ?></td>
					<td><?php echo h($this->Time->niceShort($orderItemFile['sent_to_callas'])); ?></td>
					<td class="text-center"><?php echo $this->Presentation->yesNo(!empty($orderItemFile['callas_info'])); ?></td>
				</tr>

				<?php if (!empty($orderItemFile['OrderItemFileProcess'])) { ?>

					<tr>
						<td></td>

						<td colspan="14">

							<?php echo $this->element(
							'orders/admin/order_item_file_processes',
							array(
								'orderItemFileProcesses' => $orderItemFile['OrderItemFileProcess']
							)
						); ?>

						</td>

					</tr>

				<?php }

					if (!empty($orderItemFile['callas_info'])) {
						$callasInfo = $orderItemFile['callas_info'];
						$callasInfo = json_decode($callasInfo, 1);
					} else {
						$callasInfo = '';
				} ?>

				<tr>
					<td colspan="14">
						<?php if ($callasInfo) { ?>
							<a class="btn btn-sm btn-link" role="button" data-toggle="collapse" href="#callas-info-<?php echo $orderItemFile['id']; ?>" aria-expanded="false" aria-controls="collapseExample">Show/hide Callas info</a>
							<div class="collapse" id="callas-info-<?php echo $orderItemFile['id']; ?>">
								<div class="well">
									<small><pre><?php print_r($callasInfo); ?></pre></small>
								</div>
							</div>
						<?php } else { ?>
							No Callas info
						<?php } ?>
					</td>
				</tr>

				<?php if (!empty($orderItemFile['OrderItemFilePage'])) { ?>

					<tr>
						<td></td>
						<td colspan="13">

							<?php echo $this->element(
								'orders/admin/order_item_file_pages',
								['orderItemFilePages' => $orderItemFile['OrderItemFilePage']]
							); ?>

						</td>

					</tr>

				<?php }

				}

			}

		?>

	</tbody>

</table>
