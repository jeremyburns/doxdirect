<?php foreach ($orderItems as $orderItemKey => $orderItem) { ?>

	<div class="page-header">
		<h3>Order item <?php echo $orderItemKey + 1; ?>: <?php echo $orderItem['name']; ?></h3>
	</div>

	<div class="row">

		<div class="col-sm-10">

			<table class="table table-bordered table-condensed">

				<caption>Details</caption>

				<tbody>

					<tr>
						<th>Id</th>
						<td><?php echo $orderItem['id']; ?></td>
					</tr>

					<tr>
						<th>Quote product id</th>
						<td><?php echo $orderItem['pace_quote_product_id']; ?></td>
					</tr>

					<tr>
						<th>Name</th>
						<td><?php echo $orderItem['name']; ?></td>
					</tr>

					<tr class="header">
						<th colspan="2">File issues</th>
					</tr>

					<tr>
						<td>Paper size consistency</td>
						<td><?php echo $this->Presentation->yesNo($orderItem['file_paper_size_consistency']); ?></td>
					</tr>

					<tr>
						<td>Paper size match</td>
						<td><?php echo $orderItem['file_paper_size_match']; ?></td>
					</tr>

					<tr>
						<td>Paper size match consistency</td>
						<td><?php echo $this->Presentation->yesNo($orderItem['file_paper_size_match_consistency']); ?></td>
					</tr>

					<tr>
						<td>Paper size scale consistency</td>
						<td><?php echo $this->Presentation->yesNo($orderItem['file_paper_size_scale_consistency']); ?></td>
					</tr>

					<tr>
						<td>File orientation</td>
						<td><?php echo $orientations[$orderItem['file_orientation']]; ?></td>
					</tr>

					<tr>
						<td>Orientation consistency</td>
						<td><?php echo $this->Presentation->yesNo($orderItem['file_orientation_consistency']); ?></td>
					</tr>

					<tr>
						<td colspan="2">Orientation page counts:</td>
					</tr>

					<tr>
						<td class="text-right">Portrait:</td>
						<td><?php echo $orderItem['file_orientation_count_p']; ?></td>
					</tr>

					<tr>
						<td class="text-right">Landscape:</td>
						<td><?php echo $orderItem['file_orientation_count_l']; ?></td>
					</tr>

					<tr>
						<td>Trim marks</td>
						<td><?php echo $this->Presentation->yesNo($orderItem['file_has_trim_marks']); ?></td>
					</tr>

					<tr class="header">
						<th colspan="2">User choices</th>
					</tr>

					<tr>
						<td>Binding edge criteria</td>
						<td><?php echo $this->Presentation->yesNo($orderItem['binding_edge_criteria']); ?></td>
					</tr>

					<tr>
						<td>Binding side criteria</td>
						<td><?php echo $this->Presentation->yesNo($orderItem['binding_side_criteria']); ?></td>
					</tr>

					<tr>
						<td>Print on SRA</td>
						<td><?php echo $this->Presentation->yesNo($orderItem['print_on_sra']); ?></td>
					</tr>

					<tr>
						<td>Print on cut sheet</td>
						<td><?php echo $orderItem['print_on_cut_sheet']; ?></td>
					</tr>

				</tbody>

			</table>

		</div>

		<div class="col-sm-8">

			<table class="table table-bordered table-condensed">

				<caption>Product</caption>

				<tbody>

					<tr>
						<th>Quantities</th>
						<td><?php echo $orderItem['pages']; ?> pages</td>
						<td><?php echo $orderItem['copies']; ?> copies</td>
					</tr>

					<tr>
						<th>Product</th>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Product']['id'],
									array(
										'admin' => true,
										'controller' => 'products',
										'action' => 'view',
										$orderItem['Product']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Product']['name'],
									array(
										'admin' => true,
										'controller' => 'products',
										'action' => 'view',
										$orderItem['Product']['id']
									)
								);
							?>
						</td>
					</tr>

					<tr>
						<th>Binding type</th>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Product']['BindingType']['id'],
									array(
										'admin' => true,
										'controller' => 'binding_types',
										'action' => 'view',
										$orderItem['Product']['BindingType']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Product']['BindingType']['name'],
									array(
										'admin' => true,
										'controller' => 'binding_types',
										'action' => 'view',
										$orderItem['Product']['BindingType']['id']
									)
								);
							?>
						</td>
					</tr>

					<tr>
						<th>Binding</th>
						<td>
							<?php if (!empty($orderItem['Binding'])) {
								echo $this->Html->link(
									$orderItem['Binding']['id'],
									array(
										'admin' => 'true',
										'controller' => 'bindings',
										'action' => 'view',
										$orderItem['Binding']['id']
									)
								);
							} ?>
						</td>
						<td>
							<?php if (!empty($orderItem['Binding'])) {
								echo $this->Html->link(
									$orderItem['Binding']['name'],
									array(
										'admin' => 'true',
										'controller' => 'bindings',
										'action' => 'view',
										$orderItem['Binding']['id']
									)
								);
							} ?>
						</td>
					</tr>

					<tr>
						<th>Binding edge</th>
						<td>
							<?php if (!empty($orderItem['BindingEdgeType'])) {
								echo $this->Html->link(
									$orderItem['BindingEdgeType']['id'],
									array(
										'admin' => 'true',
										'controller' => 'binding_edges',
										'action' => 'view',
										$orderItem['BindingEdgeType']['id']
									)
								);
							} ?>
						</td>
						<td>
							<?php if (!empty($orderItem['BindingEdgeType'])) {
								echo $this->Html->link(
									$orderItem['BindingEdgeType']['name'],
									array(
										'admin' => 'true',
										'controller' => 'binding_edges',
										'action' => 'view',
										$orderItem['BindingEdgeType']['id']
									)
								);
							} ?>
						</td>
					</tr>

					<tr>
						<th>Binding side</th>
						<td>
							<?php if (!empty($orderItem['BindingSideType'])) {
								echo $this->Html->link(
									$orderItem['BindingSideType']['id'],
									array(
										'admin' => 'true',
										'controller' => 'binding_side_types',
										'action' => 'view',
										$orderItem['BindingSideType']['id']
									)
								);
							} ?>
						</td>
						<td>
							<?php if (!empty($orderItem['BindingSideType'])) {
								echo $this->Html->link(
									$orderItem['BindingSideType']['name'],
									array(
										'admin' => 'true',
										'controller' => 'binding_side_types',
										'action' => 'view',
										$orderItem['BindingSideType']['id']
									)
								);
							} ?>
						</td>
					</tr>

					<tr>
						<th>Media</th>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Media']['id'],
									array(
										'admin' => true,
										'controller' => 'media',
										'action' => 'view',
										$orderItem['Media']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Media']['name'],
									array(
										'admin' => true,
										'controller' => 'media',
										'action' => 'view',
										$orderItem['Media']['id']
									)
								);
							?>
						</td>
					</tr>

					<tr>
						<th>Drilling</th>
						<td>
							<?php
								if (!empty($orderItem['DrillingType'])) {
									echo $this->Html->link(
										$orderItem['DrillingType']['id'],
										array(
											'admin' => true,
											'controller' => 'drilling_types',
											'action' => 'view',
											$orderItem['DrillingType']['id']
										)
									);
								}
							?>
						</td>
						<td>
							<?php
								if (!empty($orderItem['DrillingType'])) {
									echo $this->Html->link(
										$orderItem['DrillingType']['name'],
										array(
											'admin' => true,
											'controller' => 'drilling_types',
											'action' => 'view',
											$orderItem['DrillingType']['id']
										)
									);
								}
							?>
						</td>
					</tr>

					<tr>
						<th>Click</th>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Click']['id'],
									array(
										'admin' => true,
										'controller' => 'clicks',
										'action' => 'view',
										$orderItem['Click']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Click']['name'],
									array(
										'admin' => true,
										'controller' => 'clicks',
										'action' => 'view',
										$orderItem['Click']['id']
									)
								);
							?>
						</td>
					</tr>

					<tr>
						<th>Sides</th>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Side']['id'],
									array(
										'admin' => true,
										'controller' => 'sides',
										'action' => 'view',
										$orderItem['Side']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$orderItem['Side']['name'],
									array(
										'admin' => true,
										'controller' => 'sides',
										'action' => 'view',
										$orderItem['Side']['id']
									)
								);
							?>
						</td>

					</tr>

					<tr>
						<th>Outer front cover</th>
						<td>
							<?php
								if (!empty($orderItem['CoverTypeOuterFront'])) {
									echo $this->Html->link(
										$orderItem['CoverTypeOuterFront']['id'],
										array(
											'admin' => true,
											'controller' => 'cover_types',
											'action' => 'view',
											$orderItem['CoverTypeOuterFront']['id']
										)
									);
								}
							?>
						</td>
						<td>
							<?php
								if (!empty($orderItem['CoverTypeOuterFront'])) {
									echo $this->Html->link(
										$orderItem['CoverTypeOuterFront']['name'],
										array(
											'admin' => true,
											'controller' => 'cover_types',
											'action' => 'view',
											$orderItem['CoverTypeOuterFront']['id']
										)
									);
								}
							?>
						</td>

					</tr>

					<tr>
						<th>Front cover</th>
						<td>
							<?php
								if (!empty($orderItem['CoverTypeInnerFront'])) {
									echo $this->Html->link(
										$orderItem['CoverTypeInnerFront']['id'],
										array(
											'admin' => true,
											'controller' => 'cover_types',
											'action' => 'view',
											$orderItem['CoverTypeInnerFront']['id']
										)
									);
								}
							?>
						</td>
						<td>
							<?php
								if (!empty($orderItem['CoverTypeInnerFront'])) {
									echo $this->Html->link(
										$orderItem['CoverTypeInnerFront']['name'],
										array(
											'admin' => true,
											'controller' => 'cover_types',
											'action' => 'view',
											$orderItem['CoverTypeInnerFront']['id']
										)
									);
								}
							?>
						</td>

					</tr>

					<tr>
						<th>Back cover</th>
						<td>
							<?php
								if (!empty($orderItem['CoverTypeInnerBack'])) {
									echo $this->Html->link(
										$orderItem['CoverTypeInnerBack']['id'],
										array(
											'admin' => true,
											'controller' => 'cover_types',
											'action' => 'view',
											$orderItem['CoverTypeInnerBack']['id']
										)
									);
								}
							?>
						</td>
						<td>
							<?php
								if (!empty($orderItem['CoverTypeInnerBack'])) {
									echo $this->Html->link(
										$orderItem['CoverTypeInnerBack']['name'],
										array(
											'admin' => true,
											'controller' => 'cover_types',
											'action' => 'view',
											$orderItem['CoverTypeInnerBack']['id']
										)
									);
								}
							?>
						</td>

					</tr>

					<tr>
						<th>Outer back cover</th>
						<td>
							<?php
								if (!empty($orderItem['CoverTypeOuterBack'])) {
									echo $this->Html->link(
										$orderItem['CoverTypeOuterBack']['id'],
										array(
											'admin' => true,
											'controller' => 'cover_types',
											'action' => 'view',
											$orderItem['CoverTypeOuterBack']['id']
										)
									);
								}
							?>
						</td>
						<td>
							<?php
								if (!empty($orderItem['CoverTypeOuterBack'])) {
									echo $this->Html->link(
										$orderItem['CoverTypeOuterBack']['name'],
										array(
											'admin' => true,
											'controller' => 'cover_types',
											'action' => 'view',
											$orderItem['CoverTypeOuterBack']['id']
										)
									);
								}
							?>
						</td>

					</tr>

				</tbody>

			</table>

		</div>

		<div class="col-sm-6">

			<table class="table table-bordered table-condensed">

				<caption>Amounts</caption>

				<tbody>

					<tr>
						<th>Net</th>
						<td class="text-right"><?php echo $this->Number->currency($orderItem['amount_net'], 'GBP'); ?></td>

					</tr>

					<tr>

						<th>Turnaround</th>
						<td class="text-right"><?php echo $this->Number->currency($orderItem['amount_turnaround'], 'GBP'); ?></td>

					</tr>

				</tbody>

			</table>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-24">

			<?php echo $this->element(
					'orders/admin/order_item_tabs',
					array('orderItemTabs' => $orderItem['OrderItemTab'])
				);
			?>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-24">

			<?php echo $this->element(
					'orders/admin/order_item_cover_designer',
					array('orderItem' => $orderItem)
				);
			?>

		</div>

	</div>

	<div class="page-header">
		<h4>Uploaded files for order item <?php echo $orderItemKey + 1; ?></h4>
	</div>

	<div class="row">

		<div class="col-sm-24">

			<?php echo $this->element(
					'orders/admin/order_item_processes',
					array('orderItemProcesses' => $orderItem['OrderItemProcess'])
				); ?>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-24">

			<?php echo $this->element(
				'orders/admin/order_item_files',
				array('orderItemFiles' => $orderItem['OrderItemFile'])
			); ?>

		</div>

	</div>

<?php }
