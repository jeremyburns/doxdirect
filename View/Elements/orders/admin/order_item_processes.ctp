<table class="table table-condensed">

	<caption>Processes</caption>

	<thead>
		<tr>
			<th>Seq</th>
			<th>Name</th>
			<th>Profile name</th>
			<th>Parameters</th>
			<th class="text-center">Complete</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach ($orderItemProcesses as $orderItemProcess) { ?>

			<tr>
				<td><?php echo $orderItemProcess['sequence']; ?></td>
				<td><?php echo $orderItemProcess['Process']['name']; ?></td>
				<td><?php echo $orderItemProcess['Process']['profile_name']; ?></td>
				<td><small><code><?php echo $orderItemProcess['config']; ?></code></small></td>
				<td class="text-center"><?php echo $this->Presentation->yesNo($orderItemProcess['is_complete']); ?></td>
			</tr>

		<?php } ?>

	</tbody>

</table>
