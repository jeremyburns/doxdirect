<dl class="spaced">

	<dt>Processing priority:</dt>
	<dd><i class="fa fa-arrow-right"></i> <?php echo $despatchMethod; ?></dd>

	<dt>Your order will be despatched by:</dt>
	<dd><i class="fa fa-truck fa-flip-horizontal"></i> <?php echo $despatchDateFormatted; ?></dd>

	<?php if (!empty($despatchDateNotice)) { ?>
		<dd><span class="label label-warning">Attention <i class="fa fa-exclamation-circle"></i></span> <small><?php echo $despatchDateNotice; ?></small></dd>
	<?php } ?>

	<dt>Delivery method:</dt>
	<dd><i class="fa fa-envelope-o"></i> <?php echo $deliveryMethod; ?></dd>

	<?php if ($isGuaranteed) { ?>
		<dt>Guaranteed delivery date and time:</dt>
	<?php } else { ?>
		<dt>Estimated delivery date:</dt>
	<?php } ?>

	<dd><i class="fa fa-calendar"></i> <?php echo $deliveryDateFormatted; ?></dd>

	<?php if (!empty($latestDeliveryTime)) { ?>
		<dd><i class="fa fa-clock-o"></i> Before <?php echo $latestDeliveryTime; ?></dd>
	<?php } ?>

</dl>

<?php if (!empty($sameDayTurnaroundCutOffHour) && date('H') <= $sameDayTurnaroundCutOffHour) { ?>
	<p><strong><i class="fa fa-clock-o"></i> Want it sooner?</strong> Order by <?php echo $sameDayTurnaroundCutOffHour; ?>:00 today and choose 'Same day processing' with courier delivery at the checkout for guaranteed next working day delivery.</p>
<?php }
