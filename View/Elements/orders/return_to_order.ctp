<div class="header-band">

	<div class="container">

		<div class="row">

			<div class="col-xs-24">

				<p class="text-center">

					<?php

						echo $this->Html->link(
							'<i class="fa fa-undo"></i> Continue with your order',
							[
								'controller' => 'orders',
								'action' => 'resume'
							],
							[
								'escape' => false
							]
						);

					?>

				</p>

			</div>

		</div>

	</div>

</div>
