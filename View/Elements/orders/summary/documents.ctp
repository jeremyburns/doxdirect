<?php if (!empty($order['Order']) && $order['Order']['count_order_item'] > 1) { ?>

	<div class="panel panel-default">

		<div class="panel-heading">
			<i class="fa fa-file-text-o"></i> Your current documents
		</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-condensed table-borderless table-white">

					<tbody>

						<?php
							echo $this->element(
								'orders/order_items/document_list',
								array(
									'order' => $order
								)
							);
						?>

						<tr class="border-dashed">
							<td class="td-label" colspan="2">
								Printing total:
							</td>
							<td class="text-right">
								<?php
									echo $this->Number->format(
										$order['Order']['amount_net'],
										[
											'before' => '£',
											'places' => 2
										]
									);
								?>
							</td>
						</tr>

						<tr>
							<td colspan="2">Processing:</td>
							<td class="text-right">
								<?php
									echo $this->Number->format(
										$order['Order']['amount_turnaround'],
										[
											'before' => '£',
											'places' => 2
										]
									);
								?>
							</td>
						</tr>

						<tr>
							<td colspan="2">Post & packing:</td>
							<td class="text-right">
								<?php
									echo $this->Number->format(
										$order['Order']['amount_delivery'],
										[
											'before' => '£',
											'places' => 2
										]
									);
								?>
							</td>
						</tr>

						<tr>
							<td colspan="2">VAT:</td>
							<td class="text-right">
								<?php
									echo $this->Number->format(
										$order['Order']['amount_vat'],
										[
											'before' => '£',
											'places' => 2
										]
									);
								?>
							</td>
						</tr>

						<tr>
							<td class="td-label" colspan="2">Order total:</td>
							<td class="text-right font-weight-bold">
								<?php
									echo $this->Number->format(
										$order['Order']['amount_total'],
										[
											'before' => '£',
											'places' => 2
										]
									);
								?>
							</td>
						</tr>

					</tbody>

				</table>

			</div>

			<p class="text-right">
				<?php
					echo $this->Html->link(
						'Order Now <i class="fa fa-chevron-right"></i>',
						['admin' => false, 'controller' => 'orders', 'action' => 'upload'],
						array(
							'id' => 'link-order-now',
							'class' => 'btn btn-xs btn-action',
							'escape' => false
						)
					);
				?>
			</p>

		</div>

	</div>

<?php }
