<div class="row">

	<div class="col-sm-8">

		<div class="panel panel-info">

			<div class="panel-heading">
				<i class="fa fa-print"></i> Your document
			</div>

			<div class="panel-body">

				<?php echo $this->element(
					'orders/summary/print_details',
					array('order' => $order)
				); ?>

			</div>

		</div>

	</div>

	<div class="col-sm-8 col-md-push-8">

		<div class="panel panel-info">

			<div class="panel-heading">
				<i class="fa fa-calculator"></i> Your quote
			</div>

			<div class="panel-body">

				<?php echo $this->element(
					'orders/quote/amounts',
					array(
						'order' => $order,
						'orderOptions' => array(
							'allow_edit' => false,
							'show_add_link' => false
						)
					)
				); ?>

			</div>

		</div>

	</div>

	<div class="col-sm-8 col-md-pull-8">

		<div class="panel panel-info">

			<div class="panel-heading">
				<i class="fa fa-truck fa-flip-horizontal"></i> Despatch details
			</div>

			<div class="panel-body">

				<?php
					if (!empty($order['Order'])) {
						echo $this->element(
							'orders/despatch/summary',
							array(
								'despatchDateFormatted' => $order['Order']['despatch_date_formatted'],
								'despatchMethod' => $order['TurnaroundOption']['name'],
								'deliveryMethod' => $order['DeliveryOption']['name'],
								'deliveryDateFormatted' => $order['Order']['delivery_date_formatted'],
								'latestDeliveryTime' => $order['DeliveryOption']['latest_delivery_time'],
								'isGuaranteed' => $order['DeliveryOption']['is_guaranteed']
							)
						);
					}
				?>

			</div>

		</div>

	</div>

</div>