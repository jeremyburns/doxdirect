<?php if (empty($order['OrderItem'])) { ?>

	<p>Please upload a file or get a quote to see how we can print your document.</p>

	<?php echo $this->Html->link(
		'<i class="fa fa-calculator"></i> Get a quote',
		'/',
		array(
			'escape' => false,
			'class' => 'btn btn-sm btn-default'
		)
	); ?>

<?php } else { ?>

	<div class="panel-group" id="order-item-list" role="tablist" aria-multiselectable="true">

		<?php foreach ($order['OrderItem'] as $orderItemKey => $orderItem) {

			if (isset($orderItem['amount_net'])) { ?>

				<div class="panel panel-default">

					<div class="panel-heading" role="tab" id="order-item-list-<?php echo $orderItemKey; ?>-heading">

						<h4 class="panel-title">

							<a data-toggle="collapse"  href="#order-item-list-<?php echo $orderItemKey; ?>" aria-expanded="true" aria-controls="order-item-list-<?php echo $orderItemKey; ?>">

								<?php echo $orderItem['name']; ?>

							</a>

						</h4>

					</div>

					<div id="order-item-list-<?php echo $orderItemKey; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="order-item-list-<?php echo $orderItemKey; ?>-heading">

						<table class="table table-condensed">

							<tfoot>
								<tr>
									<td colspan="2">
										<?php
											echo $this->element(
												'orders/order_items/link_edit',
												array(
													'orderItemId' => $orderItem['id']
												)
											);
										?>
									</td>
								</tr>

							</tfoot>

							<tbody>

								<tr>
									<td class="td-label">Product:</td>
									<td><?php echo $orderItem['Product']['name']; ?></td>
								</tr>

								<?php if (Configure::read('debug')) { ?>

									<tr>
										<td class="td-label">Quote id:</td>
										<td><?php echo $orderItem['pace_quote_product_id']; ?></td>
									</tr>

								<?php } ?>

								<tr>
									<td rowspan="4" class="td-label">Paper:</td>
									<td>
										<?php
											if ($orderItem['print_on_sra'] == 'y')
											{
												echo 'SR' . $orderItem['Media']['PaperSize']['id'];
											}
											else
											{
												echo $orderItem['Media']['PaperSize']['id']; ?> (<?php echo $orderItem['Media']['PaperSize']['width']; ?>mm x <?php echo $orderItem['Media']['PaperSize']['height']; ?>mm)
											<?php }
										?>

									</td>
								</tr>

								<tr>
									<td><?php echo $orderItem['Media']['name']; ?></td>
								</tr>

								<tr>
									<td><?php echo $orderItem['Click']['name']; ?></td>
								</tr>

								<tr>
									<td><?php echo $orderItem['Side']['name']; ?></td>
								</tr>

								<tr>
									<td class="td-label">Pages</td>
									<td><?php echo $orderItem['pages']; ?></td>

								</tr>

								<tr>
									<th class="td-label">Copies</td>
									<td><?php echo $orderItem['copies']; ?></td>
								</tr>

								<?php if (!empty($orderItem['tab_count'])) { ?>

									<tr>
										<th class="td-label">Tabs</td>
										<td><?php echo $orderItem['tab_count']; ?></td>
									</tr>

								<?php } ?>

								<?php if (!empty($orderItem['DrillingType']['name'])) { ?>

									<tr>
										<td class="td-label">Hole punching:</td>
										<td><?php echo $orderItem['DrillingType']['name']; ?></td>
									</tr>

								<?php }



									$hasCover = false;

									$coverFields = [
										'cover_type_outer_front_id' => [
											'label' => 'Outer front:',
											'model' => 'CoverTypeOuterFront'
										],
										'cover_type_inner_front_id' => [
											'label' => 'Front:',
											'model' => 'CoverTypeInnerFront'
										],
										'cover_type_inner_back_id' => [
											'label' => 'Back:',
											'model' => 'CoverTypeInnerBack'
										],
										'cover_type_outer_back_id' => [
											'label' => 'Outer back:',
											'model' => 'CoverTypeOuterBack'
										],
										'cover_type_wrap_around_id' => [
											'label' => 'Wraparound:',
											'model' => 'CoverTypeWrapAround'
										]
									];

									foreach ($coverFields as $coverField => $coverName) {
										if (!empty($orderItem[$coverField])) {
											$hasCover = true;
										}
									}

									if ($hasCover) { ?>

										<tr>
											<td colspan="2" class="td-label">Covers:</td>
										</tr>

										<?php foreach ($coverFields as $coverField => $coverType) {

											if (!empty($orderItem[$coverField])) { ?>

												<tr>
													<td> - <?php echo $coverType['label']; ?></td>
													<td><?php echo $orderItem[$coverType['model']]['name']; ?></td>
												</tr>

											<?php }
										}

									}

								?>

								<tr>
									<td class="td-label">Printing cost:</td>
									<td><?php echo $this->Number->format($orderItem['amount_net'], ['before' => '£', 'places' => 2]); ?></td>
								</tr>

							</tbody>

						</table>

					</div>

				</div>

			<?php }

		} ?>

	</div>

	<?php // echo $this->element('orders/order_items/link_btn_add_another_order_item'); ?>

<?php }
