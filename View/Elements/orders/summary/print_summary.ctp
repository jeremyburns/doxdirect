<div class="panel panel-default">

	<div class="panel-heading">
		<i class="fa fa-print"></i> Here's how we will print your document(s)
	</div>

	<div class="panel-body">

		<?php

			echo $this->Flash->render('quote-updated');

			echo $this->element('orders/summary/print_details', array('order' => $order));

		?>

	</div>

</div>
