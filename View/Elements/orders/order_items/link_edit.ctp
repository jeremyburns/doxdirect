<?php
	echo $this->Html->link(
		'<i class="fa fa-pencil"></i> Edit',
		array(
			'controller' => 'orders',
			'action' => 'edit',
			$orderItemId
		),
		array(
			'escape' => false,
			'class' => 'link-remove-order-item font-size-small'
		)
	);
?>
