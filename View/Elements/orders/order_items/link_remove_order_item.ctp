<?php
	echo $this->Html->link(
		'<i class="fa fa-trash"></i> Remove',
		array(
			'controller' => 'orders',
			'action' => 'remove_order_item',
			$orderItemId
		),
		array(
			'escape' => false,
			'title' => 'Remove ' . $orderItemName . ' from your quote',
			'class' => 'link-remove-order-item font-size-small'
		)
	);
?>
