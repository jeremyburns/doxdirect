<?php
	$orderOptions = isset($orderOptions) ? $orderOptions : array();

	$orderItemKey = 1;

	foreach ($order['OrderItem'] as $orderItem) {

		if (isset($orderItem['amount_net'])) { ?>

			<tr class="font-weight-light border-dashed">
				<td class="ptn pbn">
					<?php echo $orderItemKey . ' : ' . $orderItem['name']; ?>
				</td>
				<td class="ptn pbn text-right">
					<?php
/*
						if (!isset($orderOptions['allow_edit']) || $orderOptions['allow_edit'] === true) {
							echo $this->element(
								'orders/order_items/link_remove_order_item',
								array(
									'orderItemId' => $orderItem['id'],
									'orderItemName' => $orderItem['name']
								)
							);
						}
*/
					?>
				</td>
				<td class="ptn pbn text-right">
					<?php
						echo $this->Number->format(
							$orderItem['amount_net'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<?php $orderItemKey++;

		}

	}
?>