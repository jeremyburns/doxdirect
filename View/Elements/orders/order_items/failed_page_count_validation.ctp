<?php if ($pageCount < $limits['min']) { ?>

	<tr class="info">

		<td colspan="<?php echo $colspan; ?>">

			<p>Your document is below the minimum page count for this binding type. Please upload a new file or additional files, or click 'Continue' if you're happy for us to add blank pages to reach the minimum page count of <?php echo $limits['min']; ?> pages.</p>

		</td>

	</tr>

<?php } elseif ($limits['max'] && ($pageCount > $limits['max'])) { ?>

	<tr class="error">

		<td colspan="<?php echo $colspan; ?>">

			<p>Your document is above the maximum page count for this binding type. Please remove one or more files or upload a new file to fit within the maximum page count of <?php echo $limits['max']; ?> pages.

			<?php

				// This field is here so that jquery can detect and act upon it by hiding the continue buttton
				echo $this->Form->input(
					'page_count_validation',
					[
						'value' => 'above_max',
						'label' => false,
						'type' => 'hidden'
					]
				);

			?>

		</td>

	</tr>

<?php }
