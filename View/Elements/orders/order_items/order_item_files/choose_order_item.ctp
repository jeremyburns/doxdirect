<?php
	if (count($orderItems) > 1) {

		echo $this->Form->create(
			null,
			array(
				'url' => array(
					'controller' => 'orders',
					'action' => 'set_order_item_id'
				),
				'role' => 'form',
				'inputDefaults' => array(
					'label' => false
				),
				'class' => 'form-inline'
			)
		);
	?>

		<div class="panel panel-info">

			<div class="panel-heading">
				Which doc is this file for?
			</div>

			<div class="panel-body">

				<div class="form-group text">

					<?php
						echo $this->Form->select(
							'order_item_id',
							$orderItems,
							array(
								'value' => $orderItemId,
								'empty' => false,
								'class' => 'form-control',
								'label' => false
							)
						);
					?>

				</div>

				<?php echo $this->Form->button(
					'Update',
					array(
						'type' => 'submit',
						'class' => 'btn btn-sm btn-primary'
					)
				); ?>

			</div>

		</div>

	<?php echo $this->Form->end();

} ?>