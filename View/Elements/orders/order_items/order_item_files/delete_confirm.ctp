<tr id="delete-order-item-file-confirm-<?php echo $orderItemFileId; ?>" class="delete-order-item-file-confirm no-top-border" style="display: none;">

	<td colspan="9">

		<div class="alert alert-info-pale fade in" role="alert">

			<?php echo $this->Form->create(
				null,
				array(
					'id' => 'delete-order-item-file-form-' . $orderItemFileId,
					'url' => $deleteUrl,
					'class' => 'delete-order-item-file-form',
					'data-order-item-file-id' => $orderItemFileId
				)
			); ?>

				<span data-action="confirm">

					<h4>Are you sure you want to delete '<?php echo $orderItemFileName; ?>'?</h4>

					<p>

						<button type="submit" class="btn btn-info">Yes - delete</button>
						<button type="reset" class="btn btn-default">Cancel</button>

					</p>

				</span>

			<?php echo $this->Form->end(); ?>

		</div>

	</td>

</tr>
