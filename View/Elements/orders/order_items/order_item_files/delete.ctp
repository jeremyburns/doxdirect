<span id="delete-order-item-file-link-<?php echo $orderItemFileId; ?>" class="delete-order-item-file-link">

	<?php
		echo $this->Html->link(
			'<i class="fa fa-trash"></i>',
			$deleteUrl,
			array(
				'class' => 'delete-order-item-file-link',
				'escape' => false,
				'data-order-item-file-id' => $orderItemFileId
			)
		);
	?>

</span>
