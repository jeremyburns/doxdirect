<?php
	$resequenceUrl = $this->Html->url(
		array(
			'controller' => 'order_item_files',
			'action' => 'resequence'
		)
	);
?>

<div class="table-responsive">

	<table id="order-item-files-list" class="table" data-resequence-url="<?php echo $resequenceUrl; ?>">

		<caption>Your files</caption>

		<tbody id="order-item-files-list-tbody" class="sortable">

			<?php

				if (!empty($order['OrderItem'])) {

					foreach ($order['OrderItem'] as $orderItemKey => $orderItem) {

						$printOnSRA = $orderItem['print_on_sra'] == 'y';

						if (!empty($orderItem['id'])) { ?>

							<tr id="order-item-files-list-header-<?php echo $orderItem['id']; ?>" class="">
								<td colspan="9">
									Files for order item "<?php echo $orderItem['name']; ?>":
								</td>
							</tr>

							<?php

								if (!$orderItem['file_page_count_valid'] && $orderItem['file_page_count_validation']) {

									echo $this->element(
										'orders/order_items/failed_page_count_validation',
										[
											'colspan' => 9,
											'pageCount' => $orderItem['file_page_count'],
											'limits' => [
												'min' => $orderItem['file_page_limit_min'],
												'max' => $orderItem['file_page_limit_min']
											]
										]
									);

								}

							if (empty($orderItem['OrderItemFile'])) { ?>

								<tr id="order-item-files-list-none-<?php echo $orderItem['id']; ?>">
									<td colspan="8">
										No uploaded files
									</td>
								</tr>

							<?php } else {

								if (count($orderItem['OrderItemFile']) > 1) { ?>

									<tr>
										<td colspan="9">
											<span class="text-info"><small><i class="fa fa-info-circle"></i> Your files will be merged into a single document in the listed order. <i class="fa fa-arrows"></i> Drag and drop to change the sequence.</small></span>
										</td>
									</tr>

									<?php
										if (!$orderItem['files_ok']) {
											echo $this->element(
												'orders/order_items/issues',
												array(
//													'orderItem' => $orderItem,
													'colspan' => 9
												)
											);
										}
									?>

								<?php } ?>

								<tr>
									<th>Seq</th>
									<th colspan="2">Name</th>
									<th>File size</th>
									<th>Pages</th>
									<th>Doc size</th>
									<th>Orientation</th>
									<th>Colour</th>
									<th></th>
								</tr>

			<?php foreach ($orderItem['OrderItemFile'] as $orderItemFileKey => $orderItemFile) {

									echo $this->element(
										'orders/order_items/order_item_files/list_tr',
										array(
											'orderItemFile' => $orderItemFile,
											'printOnSRA' => $printOnSRA
										)
									);

								}

							}

						}

					}

				}

			?>

		</tbody>

	</table>

</div>
