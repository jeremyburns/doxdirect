<?php

	echo $this->Form->create(
		null,
		array(
			'url' => array(
				'controller' => 'orders',
				'action' => 'upload'
			),
			'type' => 'file',
			'data-status-url' => $this->Html->url([
				'controller' => 'orders',
				'action' => 'upload_status'
			])
		)
	);

?>
	<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $maximumFileUploadSize; ?>" />

	<ul class="list-inline">

		<li>
			<input type="file" name="files[]" id="files" class="inputfile" data-multiple-caption="{count} files selected" multiple />
			<label id="upload-label" for="files" class="btn btn-primary btn-file"><i class="fa fa-folder-open-o"></i> <span>Choose a file...</span></label>
		</li>

		<li>
			<button id="btn-upload" type="submit" class="btn btn-success hide">
				<i class="fa fa-cloud-upload"></i>&nbsp;Upload
			</button>
		</li>

	</ul>

	<ul id="upload-status" class="list-unstyled list-inline"></ul>

	<div id="upload-progress-bar-container" class="progress mtx hide">
		<div id="upload-progress-bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-percent="0"></div>
	</div>

<?php echo $this->Form->end();
