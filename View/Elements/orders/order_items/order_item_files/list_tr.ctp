<?php
	$deleteUrl = array(
		'controller' => 'order_item_files',
		'action' => 'delete',
		$orderItemFile['id']
	);
?>

<tr id="order-item-file-<?php echo $orderItemFile['id']; ?>" class="order-item-file" data-order-item-file-id="<?php echo $orderItemFile['id']; ?>">

	<td class="sequence"><?php echo $orderItemFile['sequence']; ?></td>

	<td><i class="<?php echo $orderItemFile['file_type_icon']; ?>" title="<?php echo $orderItemFile['file_type_title']; ?>"></i></td>

	<td><?php echo $orderItemFile['name']; ?></td>

	<td class="text-nowrap"><?php echo $orderItemFile['readable_size']; ?></td>

	<?php if ($orderItemFile['count_order_item_file_page'] == 0) { ?>

		<td colspan="4" class="order-item-file-processing">
			<i id="order-item-file-icon-<?php echo $orderItemFile['id']; ?>" class="fa fa-circle-o-notch fa-spin color-green"></i> We're examining your file in the background
		</td>

	<?php } else { ?>

		<td><?php echo $orderItemFile['count_order_item_file_page']; ?></td>
		<td>
			<?php
				if ($orderItemFile['paper_size_match'] == "exact" && $orderItemFile['paper_size_consistency'] == 1) {

					if ($printOnSRA) {
						echo 'SR' . $orderItemFile['paper_size_id'];
					} else {
						echo $orderItemFile['paper_size_id'];
					}

				} elseif ($orderItemFile['paper_size_match'] != "exact") {
						echo intval($orderItemFile['first_page_width']) . "x" . intval($orderItemFile['first_page_height']) . "mm";

				} elseif ($orderItemFile['paper_size_consistency'] == 0) {

					echo "Mixed";

				}
			?>
		</td>

		<td>
			<?php echo $orientations[$orderItemFile['orientation']]; ?>
		</td>

		<td>
			<?php if ($orderItemFile['colour'] == 'col') { ?>
				Col
			<?php } else { ?>
				B&W
			<?php } ?>
		</td>

	<?php } ?>

	<td>
		<?php
			echo $this->element(
				'orders/order_items/order_item_files/delete',
				[
					'orderItemFileId' => $orderItemFile['id'],
					'orderItemFileName' => $orderItemFile['name'],
					'deleteUrl' => $deleteUrl
				]
			);

		?>
	</td>

</tr>

<?php
	echo $this->element(
		'orders/order_items/order_item_files/delete_confirm',
		[
			'orderItemFileId' => $orderItemFile['id'],
			'orderItemFileName' => $orderItemFile['name'],
			'deleteUrl' => $deleteUrl
		]
	);
