<label><?php echo $orderSourceLabel; ?></label>
<?php
	echo $this->BootstrapForm->horizontalInputs(array(
		'options' => array(
			'divClass' => 'col-sm-'
		),
		'inputs' => array(
			'Order.source_id' => array(
				'type' => 'select',
				'empty' => 'Select an option...',
				'label' => false,
// 				'div' => 'col-xs-24',
				'selectOptions' => $sources,
				'width-class-label' => 0,
				'width-class-input' => 24
			)
		)
	));
?>