<?php

	// Get $labelClass and other variables
	extract($this->Presentation->formDimensions());

	echo $this->Form->create(
		'Order',
		array(
			'url' => array(
				'controller' => 'orders',
				'action' => 'checkout'
			),
			'role' => 'form',
			'class' => 'form-horizontal',
			'inputDefaults' => array(
				'class' => 'form-control',
				'div' => $inputDivClassWide,
				'label' => false
			)
		)
	); ?>

	<div class="form-group radio">

		<?php foreach($paymentMethods as $paymentMethod) { ?>

			<div class="col-xs-24">

				<div class="radio inline">

					<label>

						<?php

							echo $this->element(
								'payment_methods/' . $paymentMethod['PaymentMethod']['code'],
								array(
									'paymentMethod' => $paymentMethod
								)
							);

						?>

					</label>

				</div>

			</div>

		<?php } ?>

	</div>

	<p class="text-right">

		<?php
			echo $this->Form->button(
			'Pay securely <i class="fa fa-chevron-right"></i>',
			array(
				'class' => 'btn btn-lg btn-action',
				'id' => 'quote-submit-button',
				'escape' => false
			)
		); ?>

	</p>

<?php echo $this->Form->end(); ?>