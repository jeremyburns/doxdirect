<div class="row">

	<div class="col-sm-24">

		<div class="panel panel-info">

			<div class="panel-body">

				<div class="col-sm-8">

					<h4>We accept:</h4>

					<ul class="list-inline dark-blue">
						<li><i class="fa fa-cc-visa fa-4x"></i></li>
						<li><i class="fa fa-cc-mastercard fa-4x"></i></li>
						<li><i class="fa fa-cc-paypal fa-4x"></i></li>
					</ul>

				</div>

				<div class="col-sm-8">

					<h4>Our no hassle guarantee</h4>
					<p>If you are unhappy with your order, return it and we will reprint or issue a refund.</p>

				</div>

				<div class="col-sm-8">

					<h4>We're secure</h4>

					<?php
						echo $this->Html->image(
							'logos/norton/norton_100w.png',
							array(
								'width' => 100
							)
						);
					?>

				</div>

			</div>

		</div>

	</div>

</div>