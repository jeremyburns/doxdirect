<fieldset id="checkout-small-print" data-user="neutral">

	<legend><i class="fa fa-check-square-o"></i> The small print</legend>

	<div class="alert alert-info-pale">

		<p class="lead">By placing my order I agree that:</p>

		<ul>

			<li>I have read and accept the <a href="https://www.doxdirect.com/terms-conditions/" target="_blank">terms and conditions</a>.</li>
			<li>I have entered the correct shipping address and billing details.</li>
			<li>I have checked my document(s) for typos and errors and understand that my printed document(s) will be exactly as my uploaded file(s) and options selected.</li>

		</ul>

		<?php

			echo $this->BootstrapForm->horizontalInputs(array(
				'options' => array(
					'divClass' => 'col-sm-'
				),
				'inputs' => array(
					'Order.accept_terms' => array(
						'type' => 'checkbox',
						'label' => 'I agree',
						'width-class-label' => 0,
						'width-class-input' => 24,
						'required' => true
// 						'error' => 'Please tick to indicate you agree to our terms and conditions'
					),
					'Order.keep_me_up_to_date' => array(
						'type' => 'checkbox',
						'label' => 'Please keep me up to date with offers and new products and services from Doxdirect.',
						'width-class-label' => 0,
						'width-class-input' => 24
					)
				)
			));

		?>

	</div>

	<?php

		echo $this->element(
			'orders/checkout/order_source',
			array(
				'orderSourceLabel' => $orderSourceLabel,
				'sources' => $sources
			)
		);

	?>

</fieldset>
