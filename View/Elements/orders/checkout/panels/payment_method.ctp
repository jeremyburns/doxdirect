<?php

	echo $this->PaypalPayment->button(
		'https://www.paypalobjects.com/en_US/i/btn/x-click-but6.gif',
		array(
			'test' => true,
			'business' => 'mail-facilitator@classoutfit.com',
			'tax' => $cart['amount_vat'],
			'amount' => $cart['amount_due'],
			'item_name' => 'Printing of Documents',
			'item_number' => $cart['pace_job_number'],
			'currency_code' => 'GBP',
			'return' => 'https://test.doxdirect.com/paid',
			'cancel_return' => $this->Html->url(
				[
					'admin' => false,
					'controller' => 'orders',
					'action' => 'checkout'
				],
				true
			),
			'lc' => 'GB'
		)
	);

	echo $this->element(
		'orders/checkout/payment_options',
		array(
			'paymentMethods' => $paymentMethods
		)
	);
?>
