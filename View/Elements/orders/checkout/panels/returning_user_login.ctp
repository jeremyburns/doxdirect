<?php if (empty($loggedInUser)) { ?>

	<div class="panel panel-default" data-user="new">

		<div class="panel-heading">
			Returning customer?
		</div>

		<div class="panel-body">

			<?php echo $this->Html->link(
				'<i class="fa fa-sign-in"></i> Log in here',
				['admin' => false, 'controller' => 'users', 'action' => 'login'],
				array(
					'escape' => false
				)
			); ?>

		</div>

	</div>

<?php }
