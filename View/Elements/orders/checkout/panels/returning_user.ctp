<span data-user="new">

	<p class="alert alert-info-pale" id="returning-user-notice"><i class="fa fa-sign-in"></i> Returning user? <?php echo $this->Html->link('Click here to log in', ['admin' => false, 'controller' => 'users', 'action' => 'login'], array('id' => 'checkout-returning-user-login-link')); ?>.</p>

</span>

<span id="checkout-returning-user" data-user="returning">

	<?php

		echo $this->element(
			'users/login',
			array('showCancelLink' => true)
		);

	?>

</span>

<!--<span id="user-details"></span>-->
