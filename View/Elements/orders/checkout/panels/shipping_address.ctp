<fieldset id="checkout-shipping-address" data-user="new">

	<legend><i class="fa fa-map-o"></i> Shipping address</legend>

	<?php

		echo $this->BootstrapForm->horizontalInputs(array(
			'options' => array(
				'divClass' => 'col-sm-'
			),
			'inputs' => array(
				'ShippingAddress.id' => array(
					'type' => 'hidden'
				),
//				'ShippingAddress.house_number' => array(
//					'label' => 'House/building number',
//					'required' => false,
//					'placeHolder' => 'No',
//					'width-class-label' => QUARTER,
//					'width-class-input' => SIXTH
//				),
				'ShippingAddress.address_1' => array(
					'label' => 'Address 1',
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => HALF
				),
				'ShippingAddress.address_2' => array(
					'label' => 'Address 2',
					'width-class-label' => QUARTER,
					'width-class-input' => HALF
				),
				'ShippingAddress.city' => array(
					'label' => 'City',
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				),
				'ShippingAddress.county' => array(
					'label' => 'County',
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				),
				'ShippingAddress.post_code' => array(
					'label' => 'Post code',
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => QUARTER
				),
				'ShippingAddress.country_id' => array(
					'label' => 'Country',
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD,
					'type' => 'select',
					'selectOptions' => $countries,
					'default' => $defaultCountryId
				),
				'Order.billing_same_as_shipping' => array(
					'type' => 'checkbox',
					'label' => 'This is also the billing address',
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				)
			)
		));

	?>

</fieldset>
