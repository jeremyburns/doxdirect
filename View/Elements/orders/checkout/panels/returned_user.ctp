<?php echo $this->Form->input(
	'Order.user_id',
	[
		'type' => 'hidden',
		'value' => $loggedInUser['User']['id']
	]
); ?>

<legend><i class="fa fa-user"></i> About you</legend>

<div class="row">

	<div class="col-xs-<?php echo QUARTER; ?>">
		User name (your email):
	</div>

	<div class="col-xs-<?php echo THREEQUARTERS; ?>">
		<strong><?php echo $loggedInUser['User']['username']; ?></strong>
	</div>

</div>

<div class="row">

	<div class="col-xs-<?php echo QUARTER; ?>">
		First name:
	</div>

	<div class="col-xs-<?php echo THREEQUARTERS; ?>">
		<?php echo $loggedInUser['User']['first_name']; ?>
	</div>

</div>

<div class="row">

	<div class="col-xs-<?php echo QUARTER; ?>">
		Last name:
	</div>

	<div class="col-xs-<?php echo THREEQUARTERS; ?>">
		<?php echo $loggedInUser['User']['last_name']; ?>
	</div>

</div>

<div class="row">

	<div class="col-xs-<?php echo QUARTER; ?>">
		Phone:
	</div>

	<div class="col-xs-<?php echo THREEQUARTERS; ?>">
		<?php echo $loggedInUser['User']['phone']; ?>
	</div>

</div>

<div class="row ptx">

	<div class="col-xs-<?php echo THREEQUARTERS; ?> col-sm-offset-<?php echo QUARTER; ?>">
		<p>
			<?php
				echo $this->Html->link(
					'<i class="fa fa-pencil-square"></i> Change your details',
					[
						'admin' => false,
						'controller' => 'users',
						'action' => 'edit'
					],
					[
						'escape' => false,
						'id' => 'change-my-details'
					]
				);
			?>
		</p>
	</div>

</div>


<legend><i class="fa fa-map-marker"></i> Your addresses</legend>

<span id="user-addresses">

	<?php if (empty($userAddresses)) {

		echo $this->element('user_addresses/no_active_records');

	} else {

		echo $this->BootstrapForm->horizontalInputs([
			'options' => [
				'divClass' => 'col-sm-'
			],
			'inputs' => [
				'Order.shipping_address_id' => [
					'type' => 'select',
					'empty' => false,
					'label' => 'Shipping address',
					'selectOptions' => $userAddresses,
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => THREEQUARTERS
				],
				'Order.billing_address_id' => [
					'type' => 'select',
					'empty' => false,
					'label' => 'Billing address',
					'selectOptions' => $userAddresses,
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => THREEQUARTERS
				]
			]
		]); ?>

		<div class="row">

			<div class="col-xs-<?php echo THREEQUARTERS; ?> col-sm-offset-<?php echo QUARTER; ?>">
				<?php

					echo $this->Html->link(
						'<i class="fa fa-map-marker"></i> Manage your addresses',
						[
							'admin' => false,
							'controller' => 'user_addresses',
							'action' => 'index'
						],
						[
							'escape' => false,
							'id' => 'user-addresses-edit-link'
						]
					);

				?>
			</div>

		</div>

	<?php } ?>

</span>
