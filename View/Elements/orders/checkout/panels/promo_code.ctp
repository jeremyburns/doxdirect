<fieldset id="checkout-promo-code" data-user="all">

	<?php echo $this->Flash->render('promo-code'); ?>

	<legend><i class="fa fa-qrcode"></i> Promo code</legend>

	<span id="promo-code-alert" style="display: none;"></span>

	<?php if (empty($order['PromoCode']['id'])) { ?>

		<p id="checkput-promo-code-link-text">If you have a promo code, please <?php echo $this->Html->link('enter it here <i class="fa fa-angle-right"></i>', ['admin' => false, 'controller' => 'orders', 'action' => 'apply_promo_code'], ['id' => 'checkput-promo-code-link', 'escape' => false]); ?></p>

		<span id="checkout-promo-code-input" style="display: none;">

			<p>Please enter your promo code here:</p>

			<?php echo $this->element('orders/checkout/panels/promo_code_input'); ?>

		</span>

	<?php } else { ?>

		<div class="row ptx">

			<div class="col-xs-<?php echo THREEQUARTERS; ?> col-sm-offset-<?php echo QUARTER; ?>">

				<p>You have used this promo code:</p>

				<dl>
					<dt><?php echo $order['PromoCode']['code']; ?></strong></dt>
					<dd><?php echo $order['PromoCode']['description']; ?></dd>

					<dt>Discount applied: <?php echo $this->Number->format($order['Order']['amount_discount'], ['before' => '£', 'places' => 2]); ?></dt>

					<dd>

						<?php
							echo $this->Html->link(
								'<i class="fa fa-scissors"></i> Remove this promo code',
								[
									'admin' => false,
									'controller' => 'orders',
									'action' => 'remove_promo_code'
								],
								[
									'id' => 'remove-promo-code',
									'escape' => false
								]
							);
						?>

					</dd>

				</dl>

			</div>

		</div>

	<?php } ?>

</fieldset>
