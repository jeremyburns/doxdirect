<fieldset id="checkout-about-you" data-user="new">

	<legend><i class="fa fa-user"></i> About you</legend>

	<?php

		echo $this->BootstrapForm->horizontalInputs(array(
			'options' => array(
				'divClass' => 'col-sm-'
			),
			'inputs' => array(
				'User.id' => array(
					'type' => 'hidden'
				),
				'User.first_name' => array(
					'label' => 'First name',
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				),
				'User.last_name' => array(
					'label' => 'Last name',
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				),
				'User.username' => array(
					'label' => 'Email',
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => HALF
				),
				'User.phone' => array(
					'label' => 'Contact phone number',
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => HALF
				),
				'User.password' => array(
					'label' => 'Password (optional)',
					'type' => 'password',
					'width-class-label' => QUARTER,
					'width-class-input' => HALF,
					'help-block' => '<i class="fa fa-lock"></i> Creating a password means you can check your order status and quickly re-order your docs.'
				)
			)
		));

	?>

</fieldset>
