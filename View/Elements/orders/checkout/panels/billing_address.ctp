<fieldset id="checkout-billing-address" data-user="new">

	<legend><i class="fa fa-shopping-cart"></i> Billing address</legend>

	<?php

		echo $this->BootstrapForm->horizontalInputs(array(
			'options' => array(
				'divClass' => 'col-sm-'
			),
			'inputs' => array(
				'BillingAddress.id' => array(
					'type' => 'hidden'
				),
				'BillingAddress.first_name' => array(
					'label' => 'First name',
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				),
				'BillingAddress.last_name' => array(
					'label' => 'Last name',
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				),
				'BillingAddress.email' => array(
					'label' => 'Email',
					'width-class-label' => QUARTER,
					'width-class-input' => HALF
				),
//				'BillingAddress.house_number' => array(
//					'label' => 'House/building number',
//					'placeHolder' => 'No',
//					'width-class-label' => QUARTER,
//					'width-class-input' => SIXTH
//				),
				'BillingAddress.address_1' => array(
					'label' => 'Address 1',
					'width-class-label' => QUARTER,
					'width-class-input' => HALF
				),
				'BillingAddress.address_2' => array(
					'label' => 'Address 2',
					'width-class-label' => QUARTER,
					'width-class-input' => HALF
				),
				'BillingAddress.city' => array(
					'label' => 'City',
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				),
				'BillingAddress.county' => array(
					'label' => 'County',
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				),
				'BillingAddress.post_code' => array(
					'label' => 'Post code',
					'width-class-label' => QUARTER,
					'width-class-input' => QUARTER
				),
				'BillingAddress.country_id' => array(
					'label' => 'Country',
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD,
					'type' => 'select',
					'selectOptions' => $countries,
					'default' => $defaultCountryId
				)
			)
		));

	?>

</fieldset>
