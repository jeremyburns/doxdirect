<?php

	$applyButton = $this->Html->link(
		'<i class="fa fa-plus-circle"></i> Apply',
		[
			'admin' => false,
			'controller' => 'orders',
			'action' => 'apply_promo_code'
		],
		[
			'class' => 'btn btn-success btn-sm',
			'escape' => false,
			'id' => 'btn-apply-promo-code'
		]
	);

	echo $this->BootstrapForm->horizontalInputs([
		'options' => [
			'divClass' => 'col-sm-'
		],
		'inputs' => [
			'Order.promo_code_id' => [
				'type' => 'hidden'
			],
			'PromoCode.code' => [
				'label' => 'Promo code',
				'required' => false,
				'placeHolder' => 'Enter your promo code here',
				'width-class-label' => QUARTER,
				'width-class-input' => THIRD,
				'wrap' => 'input-group',
				'after'=> '<span class="input-group-btn">' . $applyButton . '</span>'
			]
		]
	]);
