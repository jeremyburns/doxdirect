<fieldset id="checkout-options" data-user="neutral">

	<legend><i class="fa fa-truck fa-flip-horizontal"></i> Processing and delivery options</legend>

	<p class="help-block"><i class="fa fa-info-circle"></i> Please note that changing these options could affect your order price.</p>

	<?php

		$checkUrl = $this->Html->url([
			'admin' => false,
			'controller' => 'turnaround_options',
			'action' => 'same_day_is_available'
		], true);

		echo $this->BootstrapForm->horizontalInputs(array(
			'options' => array(
				'divClass' => 'col-sm-'
			),
			'inputs' => array(
				'Order.turnaround_option_id' => array(
					'label' => 'Priority processing',
					'type' => 'select',
					'selectOptions' => $turnaroundOptions,
					'empty' => false,
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD,
					'attributes' => [
						'data-check-turnaround' => 'yes',
						'data-check-url' => $checkUrl
					]
				),
				'Order.delivery_option_id' => array(
					'label' => 'Delivery method',
					'type' => 'select',
					'selectOptions' => $deliveryOptions,
					'empty' => false,
					'required' => true,
					'width-class-label' => QUARTER,
					'width-class-input' => THIRD
				)
			)
		));

	?>

</fieldset>
