<?php
	$dataRefreshLink = $this->Html->url(array('controller' => 'orders', 'action' => 'update_cart'));
	$returnToOrderLink = $this->Html->url(array('controller' => 'orders', 'action' => 'resume'));
?>

<span id="header-cart" data-refresh-link="<?php echo $dataRefreshLink; ?>">

	<?php if (!empty($cart) && $cart['display']) { ?>

		<a href="<?php echo $returnToOrderLink; ?>" class="label label-primary mlx mrx">
			<i class="fa fa-shopping-cart mrx"></i>
			<span class="mrx">
				<?php echo $cart['count_order_item']; ?>
			</span>
			<?php echo $this->Number->format($cart['amount_total'], ['before' => '£', 'places' => 2]); ?>
		</a>

	<?php } ?>

</span>
