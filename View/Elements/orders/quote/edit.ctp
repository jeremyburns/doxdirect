<table class="table table-condensed">

	<tfoot>
		<tr>
			<td colspan="2">
				<?php
/*
					echo $this->element(
						'orders/order_items/link_remove_order_item',
						array(
							'orderItemId' => $orderItem['id'],
							'orderItemName' => $orderItem['name']
						)
					);
*/
				?>
			</td>
		</tr>

	</tfoot>

	<tbody>

		<tr>
			<td class="td-label">Product:</td>
			<td><?php echo $orderItem['Product']['name']; ?></td>
		</tr>

		<tr>
			<td class="td-label">Size:</td>
			<td><?php echo $orderItem['Media']['PaperSize']['id']; ?> (<?php echo $orderItem['Media']['PaperSize']['width']; ?>mm x <?php echo $orderItem['Media']['PaperSize']['height']; ?>mm)</td>
		</tr>

		<tr>
			<td class="td-label">Paper:</td>
			<td><?php echo $orderItem['Media']['name']; ?></td>
		</tr>

		<tr>
			<td class="td-label">Quantities:</td>
			<td>
				<table class="table table-condensed table-sub mbn">
					<thead>
						<tr>
							<th>Pages</th>
							<th>Copies</th>
						</tr>
					</thead>
					<tr>
						<td><?php echo $orderItem['pages']; ?></td>
						<td><?php echo $orderItem['copies']; ?></td>
					</tr>
				</table>
			</td>

		</tr>

		<tr>
			<td class="td-label">Print in:</td>
			<td><?php echo $orderItem['Click']['name']; ?></td>
		</tr>

		<tr>
			<td class="td-label">Print on:</td>
			<td><?php echo $orderItem['Side']['name']; ?></td>
		</tr>

		<tr>
			<td class="td-label">Hole punching:</td>
			<td><?php echo !empty($orderItem['DrillingType']['name']) ? $orderItem['DrillingType']['name'] : ''; ?></td>
		</tr>

		<tr>
			<td class="td-label">Covers:</td>
			<td>No covers</td>
		</tr>

		<tr>
			<td class="td-label">Tabs:</td>
			<td>No tabs</td>
		</tr>

		<tr>
			<td class="td-label">Printing cost:</td>
			<td><?php echo $this->Number-format($orderItem['amount_net'], ['before' => '£', 'places' => 2]); ?></td>
		</tr>

	</tbody>

</table>