<?php if (!empty($order['Order'])) { ?>

	<?php echo $this->Flash->render('promo-code'); ?>

	<table class="table table-condensed table-borderless table-white">

		<tbody>

			<tr class="bbn">
				<td class="td-label" colspan="3">
					Cost of printing:
				</td>
			</tr>

			<?php

				$orderOptions = isset($orderOptions) ? $orderOptions : array();

				echo $this->element(
					'orders/order_items/document_list',
					array(
						'order' => $order,
						'orderOptions' => $orderOptions
					)
				);
			?>

			<tr class="border-dashed">
				<td class="td-label" colspan="2">
					<?php
						if (!isset($orderOptions['show_add_link']) || $orderOptions['show_add_link'] === true) {
							echo $this->element('orders/order_items/link_add_another_order_item');
						}
					?>
				</td>
				<td class="text-right">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_net'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<tr>
				<td class="td-label" colspan="2">
					Processing:
					<br>
					<span class="font-weight-light"><?php echo $order['TurnaroundOption']['name']; ?></span>
				</td>
				<td class="text-right">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_turnaround'],
							[
								'zero' => 'FREE',
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<tr>
				<td class="td-label" colspan="2">
					Post & packing:
					<br>
					<span class="font-weight-light"><?php echo $order['DeliveryOption']['name']; ?></span>
					<br>
					<span class="font-weight-light">
						<i class="fa fa-map-marker"></i> Assumes delivery to a mainland UK address.
						<br>
						We can ship overseas - see later.
					</span>
					<br>
					<span class="font-weight-light">Free UK Royal Mail 1st Class delivery applies to orders of £1.50 or more.</span>
				</td>
				<td class="text-right">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_delivery'],
							[
								'zero' => 'FREE',
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<tr>
				<td class="td-label" colspan="2">VAT @ 20%:</td>
				<td class="text-right">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_vat'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<?php if ($order['Order']['amount_discount'] > 0) {

				$discount = $this->Number->format(
					$order['Order']['amount_discount'],
					[
						'before' => '£',
						'places' => 2
					]
				); ?>

				<tr>
					<td class="td-label" colspan="2">Discount:</td>
					<td class="text-right">-<?php echo $discount; ?></td>
				</tr>

			<?php } ?>

			<tr class="info">
				<td class="td-label" colspan="2">Total:</td>
				<td class="text-right font-weight-bold">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_total'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

		</tbody>

	</table>

<?php }
