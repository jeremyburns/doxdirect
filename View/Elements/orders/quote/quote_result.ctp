<span id="quote-price">

	<?php if (empty($order['success'])) {

		echo $this->element(
			'orders/quote/fail',
			['order' => $order]
		);

	} else { ?>

		<?php $panelClass = empty($order['success'])
			? 'panel-error'
			: 'panel-primary'
		; ?>

			<div class="panel <?php echo $panelClass; ?>">

			<?php if (empty($order) || empty($order['Order']['count_order_item'])) { ?>

				<div id="quote-result-heading" class="panel-heading">
					<i class="fa fa-calculator"></i> Your quote will appear here
				</div>

				<?php if (empty($order['success'])) { ?>

					<div class="panel-body">
						<p><i class="fa fa-bolt"></i> We're sorry - there was an error retrieving the quote. Please try again in a few moments.</p>
					</div>

				<?php } ?>

				<?php if (!empty($order['Order']['pace_quote_id'])) { ?>
					<div class="panel-body">
						<p>Your quote is empty. Choose your options and click 'Calculate' to add a document.</p>
					</div>
				<?php } ?>

			<?php } else { ?>

				<div id="quote-result-heading" class="panel-heading">
					<a name="quote-result"></a><i class="fa fa-calculator"></i> Your quote
				</div>

				<div class="panel-body">

					<span id="quote-note"></span>

					<?php if (empty($order['Order']['amount_net'])) {

						echo $this->element(
							'orders/summary/documents',
							['data' => $order]
						);

					} else {

						echo $this->element(
							'orders/quote/amounts',
							[
								'data' => $order,
								'orderOptions' => [
									'allow_edit' => false,
									'show_add_link' => false
								]
							]
						);

						echo $this->element(
							'orders/despatch/summary',
							[
								'despatchDateFormatted' => $order['Order']['despatch_date_formatted'],
								'despatchDateNotice' => $order['Order']['despatch_date_notice'],
								'despatchMethod' => $order['TurnaroundOption']['name'],
								'deliveryMethod' => $order['DeliveryOption']['name'],
								'deliveryDateFormatted' => $order['Order']['delivery_date_formatted'],
								'latestDeliveryTime' => $order['DeliveryOption']['latest_delivery_time'],
								'isGuaranteed' => $order['DeliveryOption']['is_guaranteed'],
								'sameDayTurnaroundCutOffHour' => $sameDayTurnaroundCutOffHour,
								'sameDayTurnaroundCutOffMinute' => $sameDayTurnaroundCutOffMinute
							]
						); ?>

						<p class="text-right">

							<?php echo $this->Html->link(
								'Order Now <i class="fa fa-chevron-right"></i>',
								[
									'admin' => false,
									'controller' => 'orders',
									'action' => 'upload'
								],
								[
									'id' => 'link-order-now',
									'class' => 'btn btn-lg btn-action mlx',
									'escape' => false,
									'target' => '_parent'
								]
							); ?>

						</p>

						<?php if (!empty($order['HolidayAlert'])) {

							echo $this->element(
								'orders/quote/holiday_alert',
								['holidayAlert' => $order['HolidayAlert']]
							);
						} ?>

						<p id="order-now-note" class="alert alert-info-pale mbn">
							<strong>What next?</strong> Upload your document, confirm a few details and choose your despatch options. You can then check out or add more items to this order.
						</p>

					<?php } ?>

				</div>

			<?php } ?>

		</div>

	<?php }

	if (!empty($order['Order']['latest_quote_result'])) {
		echo $this->element(
			'orders/summary/documents',
			['data' => $order]
		);
	} ?>

</span>
