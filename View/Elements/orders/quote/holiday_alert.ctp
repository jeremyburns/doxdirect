<div class="panel panel-warning mts">

	<?php if (!empty($holidayAlert['heading'])) { ?>

		<div class="panel-heading">

			<h4><?php echo $holidayAlert['heading']; ?></h4>

		</div>

	<?php } ?>

	<?php if (!empty($holidayAlert['text'])) { ?>

		<div class="panel-body">

			<p><?php echo $holidayAlert['text']; ?></p>

		</div>

	<?php } ?>

</div>