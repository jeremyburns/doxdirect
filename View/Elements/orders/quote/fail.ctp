<div id="quote-price" class="panel panel-error">

	<div id="quote-result-heading" class="panel-heading">
		<i class="fa fa-exclamation-circle "></i> Quote error
	</div>

	<div class="panel-body">

		<p>Something went wrong while we were trying to prepare your quote.</p>

		<?php if (!empty($order['message'])) { ?>
			<p><?php echo $order['message']; ?></p>
		<?php } ?>

	</div>

</div>
