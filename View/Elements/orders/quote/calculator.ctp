<?php if (empty($renderForWordpress)) { ?>

	<div class="panel blue tight price-calculator flat-bottom">
		<h4>Price Calculator</h4>
		<p>Get an instant quote.</p>
	</div>

<?php } ?>

<div class="thumbnail flat-top">

	<div class="caption">

		<?php
			echo $this->element(
				'orders/quote/calculator_form',
				['renderForWordpress' => !empty($renderForWordpress)]
			);
		?>

	</div>

</div>
