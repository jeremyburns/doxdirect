<div class="panel panel-info">

	<div class="panel-heading">
		<i class="fa fa-calculator"></i> Your quote
	</div>

	<div class="panel-body">

		<?php echo $this->element(
			'orders/quote/amounts',
			array(
				'order' => $order,
				'orderOptions' => array(
					'allow_edit' => false,
					'show_add_link' => false
				)
			)
		); ?>

	</div>

</div>