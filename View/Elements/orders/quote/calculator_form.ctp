<?php

// Get $labelClass and other variables
extract($this->Presentation->formDimensions());

echo $this->Form->create(
	'Order',
	[
		'id' => 'OrderQuoteForm',
		'role' => 'form',
		'class' => 'form-horizontal',
		'inputDefaults' => [
			'class' => 'form-control',
			'div' => $inputDivClassWide,
			'label' => false
		],
		'data-control-path' => $this->Html->url([
			'controller' => 'orders',
			'action' => 'render_quote_control'
		])
	]
);

	echo $this->Form->hidden(
		'Order.id'
	);

	echo $this->Form->hidden(
		'Order.pace_quote_id'
	);

	// This input is used to tell the code that checks tunraound options if this is a quote (value = false) or an order (value = 1)
	echo $this->Form->hidden(
		'Order.is_order',
		[
			'id' => 'IsOrder',
			'value' => 0
		]
	);

	echo $this->Form->hidden(
		'OrderItem.' . $orderItemKey . '.id'
	);

	echo $this->Form->input(
		'OrderItem.' . $orderItemKey . '.pace_quote_product_id',
		[
			'type' => 'hidden'
		]
	); ?>

	<?php if (empty($renderForWordpress)) { ?>
		<legend>Your document</legend>
	<?php } ?>

	<div id="form-group-product" class="form-group select">

		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.product_id',
				$this->BootstrapForm->labelWithPopover (
					'Product',
					'Product',
					'What would you like to print?'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.product_id',
				[
					'empty' => false,
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-check-turnaround' => 'yes',
					'data-refresh' => 'product',
					'data-page-limit' => true
				]
			);

		?>

	</div>

	<div class="form-group select option-group-binding">
		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.binding_id',
				$this->BootstrapForm->labelWithPopover (
					'Binding',
					'Binding',
					'Binding options'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.binding_id',
				[
					'empty' => false,
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-refresh' => 'binding',
					'data-update-model' => 'binding'
				]
			);
		?>
	</div>

	<!-- 	<div class="form-group select option-group-binding-edge-type"> -->
		<?php
	/*
				echo $this->Form->label(
					'OrderItem.' . $orderItemKey . '.binding_edge_type_id',
					$this->BootstrapForm->labelWithPopover (
						'Binding edge',
						'Binding edge',
						'Binding edge'
					),
					[
						'class' => $labelClass
					]
				);

				echo $this->Form->input(
					'OrderItem.' . $orderItemKey . '.binding_edge_type_id',
					[
						'empty' => 'Binding edge...',
						'data-type' => 'select',
						'data-update-quote' => 'yes',
						'data-refresh' => 'binding-edge-type',
						'data-update-model' => 'binding-edge-type'
					]
				);
	*/
		?>
	<!-- 	</div> -->

	<!-- 	<div class="form-group select option-group-binding-side-type"> -->
		<?php
	/*
				echo $this->Form->label(
					'OrderItem.' . $orderItemKey . '.binding_side_type_id',
					$this->BootstrapForm->labelWithPopover (
						'Binding side',
						'Binding side',
						'Binding side'
					),
					[
						'class' => $labelClass
					]
				);

				echo $this->Form->input(
					'OrderItem.' . $orderItemKey . '.binding_side_type_id',
					[
						'empty' => 'Binding side...',
						'data-type' => 'select',
						'data-update-quote' => 'yes',
						'data-refresh' => 'binding-side-type',
						'data-update-model' => 'binding-side-type'
					]
				);
	*/
		?>

	<!-- 	</div> -->

	<div id="form-group-pages" class="form-group number">
		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.pages',
				$this->BootstrapForm->labelWithPopover (
					'Pages',
					'Pages',
					'How many pages does your document have?'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.pages',
				[
					'type' => 'number',
					'div' => $numberInputWidth,
					'min' => 1,
					'default' => 1,
					'data-type' => 'input',
					'data-page-limit' => true,
					'data-update-quote' => 'yes',
					'data-update-model' => 'pages'
				]
			);
		?>
	</div>

	<div class="form-group number">

		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.copies',
				$this->BootstrapForm->labelWithPopover (
					'Copies',
					'Copies',
					'How many copies shall we print?'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.copies',
				[
					'type' => 'number',
					'div' => $numberInputWidth,
					'min' => 1,
					'default' => 1,
					'data-type' => 'type',
					'data-update-quote' => 'yes'
				]
			);
		?>
	</div>

	<div class="form-group select option-group-side">

		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.side_id',
				$this->BootstrapForm->labelWithPopover (
					'Sides',
					'Sides',
					'Single sided or double sided printing?'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.side_id',
				[
					'empty' => false,
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'side',
					'data-page-limit' => true
				]
			);
		?>
	</div>

	<div class="form-group select option-group-click">
		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.click_id',
				$this->BootstrapForm->labelWithPopover (
					'Colour',
					'Colour/B&W',
					'Print in colour or black & white?'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.click_id',
				[
					'empty' => false,
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'click'
				]
			);
		?>
	</div>

	<div class="form-group select">
		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.media',
				$this->BootstrapForm->labelWithPopover (
					'Paper',
					'Paper',
					'Light or heavy paper?'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.media_id',
				[
					'empty' => false,
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'media',
					'data-page-limit' => true
				]
			);
		?>
	</div>

	<div class="form-group select option-group-drilling-type">
		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.drilling_type_id',
				$this->BootstrapForm->labelWithPopover (
					'Hole punches',
					'Hole punching',
					'Would you like your document pre-punched?'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.drilling_type_id',
				[
					'empty' => false,
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'drilling-type'
				]
			);
		?>
	</div>

	<div class="form-group select option-group-cover-type-inner-front">
		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.cover_type_inner_front_id',
				$this->BootstrapForm->labelWithPopover (
					'Front cover',
					'Add front cover',
					'Add a front cover to your finished document'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.cover_type_inner_front_id',
				[
					'empty' => 'Front cover...',
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'cover-type-inner-front'
				]
			);
		?>
	</div>

	<div class="form-group select option-group-cover-type-inner-back">
		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.cover_type_inner_back_id',
				$this->BootstrapForm->labelWithPopover (
					'Back cover',
					'Add back cover',
					'Add an back cover to your finished document'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.cover_type_inner_back_id',
				[
					'empty' => 'Back cover...',
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'cover-type-inner-back'
				]
			);
		?>
	</div>

	<div class="form-group select option-group-cover-type-outer-front">
		<?php

			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.cover_type_outer_front_id',
				$this->BootstrapForm->labelWithPopover (
					'Outer front cover',
					'Add an outer front cover',
					'Add an outer front cover to your finished document'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.cover_type_outer_front_id',
				[
					'empty' => 'Outer front cover...',
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'cover-type-outer-front'
				]
			);
		?>
	</div>

	<div class="form-group select option-group-cover-type-outer-back">
		<?php

			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.cover_type_outer_back_id',
				$this->BootstrapForm->labelWithPopover (
					'Outer back cover',
					'Add an outer back cover',
					'Add an outer back cover to your finished document'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.cover_type_outer_back_id',
				[
					'empty' => 'Outer back cover...',
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'cover-type-outer-back'
				]
			);
		?>
	</div>

	<div class="form-group select option-group-cover-type-wrap-around">
		<?php

			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.cover_type_wrap_around_id',
				$this->BootstrapForm->labelWithPopover (
					'Wraparound cover',
					'Add a wrap around cover',
					'Add a wrap around cover to your finished document'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.cover_type_wrap_around_id',
				[
					'empty' => 'Wrap around cover...',
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'cover-type-wrap-around'
				]
			);
		?>
	</div>

	<div class="form-group number option-group-tab-type">
		<?php
			echo $this->Form->label(
				'OrderItem.' . $orderItemKey . '.tab_count',
				$this->BootstrapForm->labelWithPopover (
					'No of tabs',
					'Tab dividers',
					'How many tab inserts? You can configure the placement and content of tabs later.'
				),
				[
					'class' => $labelClass
				]
			);

			echo $this->Form->input(
				'OrderItem.' . $orderItemKey . '.tab_count',
				[
					'type' => 'number',
					'div' => $numberInputWidth,
					'min' => 0,
					'data-type' => 'select',
					'data-update-quote' => 'yes',
					'data-update-model' => 'tab-type'
				]
			);
		?>
	</div>

	<legend>Processing and delivery</legend>
		<?php
			$checkUrl = $this->Html->url([
				'admin' => false,
				'controller' => 'turnaround_options',
				'action' => 'same_day_is_available'
			], true);
		?>

		<div id="form-group-turnaround-option-id" class="form-group select">

			<?php
				echo $this->Form->label(
					'Order.turnaround_option_id',
					$this->BootstrapForm->labelWithPopover (
						'Processing',
						'Priority processing',
						'Would you like us to give your document priority turnaround?'
					),
					[
						'class' => $labelClass
					]
				);

				echo $this->Form->input(
					'Order.turnaround_option_id',
					[
						'empty' => false,
						'data-update-quote' => 'yes',
						'data-check-turnaround' => 'yes',
						'data-type' => 'select',
						'data-check-url' => $checkUrl
					]
				);
			?>
		</div>

		<div class="form-group select">
			<?php
				echo $this->Form->label(
					'Order.delivery_option_id',
					$this->BootstrapForm->labelWithPopover (
						'Delivery',
						'Delivery method',
						'Choose how you would like us to get your document to you'
					),
					[
						'class' => $labelClass
					]
				);

				echo $this->Form->input(
					'Order.delivery_option_id',
					[
						'empty' => false,
						'data-update-quote' => 'yes',
						'data-type' => 'select'
					]
				);

			?>
		</div>

		<ul class="font-size-small">
			<li>Free UK Royal Mail 1st Class delivery applies to orders of £1.50 or more.</li>
			<li>Free delivery may be changed from Royal Mail to UPS Courier at our discretion.</li>
			<li>Orders are normally processed and despatched within two working days unless you opt for higher priority processing.</li>
			<li>Same day processing orders must be placed before 11am.</li>
		</ul>

		<span id="quote-update-notice"></span>

		<p class="text-right">

			<?php
				echo $this->Form->button(
					'<i class="fa fa-calculator"></i> Calculate',
					[
						'class' => 'btn btn-action',
						'id' => 'quote-submit-button',
						'escape' => false
					]
				);
			?>

		</p>

		<?php if (!empty($showResetQuoteButton)) {

			echo $this->Html->link(
				'Reset this quote',
				[
					'controller' => 'orders',
					'action' => 'reset_quote'
				],
				[
					'class' => 'btn btn-sm btn-default',
					'target' => '_parent'
				]
			);

		}

	echo $this->Form->end();
