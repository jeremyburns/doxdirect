<?php
	$amountDue = $order['Order']['amount_total'];
	$paymentNumber = 0;
?>

<div class="panel panel-info">

	<div class="panel-heading">
		Order total: <span class="badge pull-right"><?php echo $this->Number->format($amountDue, ['before' => '£', 'places' => 2]); ?></span>
	</div>

	<?php

		if (!empty($order['PaypalPayment'])) {

			foreach ($order['PaypalPayment'] as $payment) {

				$paymentNumber++;

				echo $this->element(
					'orders/paid/payments/paypal',
					array(
						'payment' => $payment,
						'paymentNumber' => $paymentNumber
					)
				);

			}
		}

	?>

	<div class="panel-footer">

		Order balance: <span class="badge pull-right"><?php echo $this->Number->format($order['Order']['amount_due'], ['before' => '£', 'places' => 2]); ?></span>

	</div>

</div>