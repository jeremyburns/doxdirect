<div class="table-responsive">

	<table class="table table-condensed table-borderless table-white">

		<thead>

			<tr>

				<th colspan="2">

					<strong><?php echo $paymentNumber; ?> : Payment via PayPal</strong>

				</th>

			</tr>

		</thead>

		<tbody>

			<tr>
				<td class="td-label">
					Date:
				</td>
				<td><?php echo $this->Presentation->niceDate($payment['created']); ?></td>
			</tr>

			<tr>
				<td class="td-label">
					PayPal TXN id:
				</td>
				<td><?php echo $payment['txn_id']; ?></td>
			</tr>


			<tr>

				<td class="td-label">
					Amount received:
				</td>

				<td>
					<span class="badge pull-right"><?php echo $this->Number->currency($payment['mc_gross'], $payment['mc_currency']); ?></span>
				</td>

			</tr>

		</tbody>

	</table>

</div>
