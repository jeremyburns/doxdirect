<dl class="spaced">

	<dt>Despatch priority</dt>
	<dd><i class="fa fa-envelope-o"></i> <?php echo $order['TurnaroundOption']['name']; ?></dd>

	<dt>Your order will be despatched by</dt>
	<dd><i class="fa fa-truck fa-flip-horizontal"></i> <?php echo $order['Order']['despatch_date_formatted']; ?></dd>

	<dt>Delivery method</dt>
	<dd><i class="fa fa-arrow-right"></i> <?php echo $order['DeliveryOption']['name']; ?></dd>

	<?php if ($order['Order']['is_guaranteed']) { ?>
		<dt>Guaranteed delivery date and time</dt>
	<?php } else { ?>
		<dt>Estimated delivery date</dt>
	<?php } ?>
	<dd><i class="fa fa-calendar"></i> <?php echo $order['Order']['delivery_date_formatted']; ?></dd>
	<?php if (!empty($order['DeliveryOption']['latest_delivery_time'])) { ?>
		<dd><i class="fa fa-clock-o"></i> Before <?php echo $order['DeliveryOption']['latest_delivery_time']; ?></dd>
	<?php } ?>

	<dt>Shipping address</dt>
	<dd>
		<?php echo $this->Dox->address($order['ShippingAddress']); ?>
	</dd>

	<dt>Billing address</dt>
	<?php if (empty($order['BillingAddress']['id'])) { ?>

		<dd>Same as the shipping address</dd>

	<?php } else { ?>

		<dd>
			<?php echo $this->Dox->address($order['BillingAddress']); ?>
		</dd>

	<?php } ?>

</dl>