<div class="table-responsive">

	<table class="table table-condensed table-borderless table-white">

		<tbody>

			<tr>
				<th colspan="2">Order number:</th>
				<td class="text-right"><?php echo $order['Order']['pace_job_number']; ?></td>
			</tr>

			<tr class="bbn">
				<th colspan="3">
					Cost of printing:
				</td>
			</tr>

			<?php

				echo $this->element(
					'orders/order_items/document_list',
					array(
						'order' => $order,
						'orderOptions' => array(
							'allow_edit' => false
						)
					)
				);
			?>

			<tr class="border-dashed">
				<th colspan="2">
				</td>
				<td class="text-right">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_net'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<tr>
				<th colspan="2">
					Processing:
					<br>
					<span class="font-weight-light"><?php echo $order['TurnaroundOption']['name']; ?></span>
				</td>
				<td class="text-right">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_turnaround'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<tr>
				<th colspan="2">
					Post & packing:
					<br>
					<span class="font-weight-light"><?php echo $order['DeliveryOption']['name']; ?></span>
				</td>
				<td class="text-right">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_delivery'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<tr>
				<th colspan="2">VAT @ 20%:</td>
				<td class="text-right">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_vat'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

			<?php if ($order['Order']['amount_discount'] > 0) { ?>

				<tr>
					<td class="td-label" colspan="2">Discount:</td>
					<td class="text-right">(
						<?php
							echo $this->Number->format(
								$order['Order']['amount_discount'],
								[
									'before' => '£',
									'places' => 2
								]
							);
						?>)
					</td>
				</tr>

			<?php } ?>

			<tr class="info">
				<th colspan="2">Total:</td>
				<td class="text-right font-weight-bold">
					<?php
						echo $this->Number->format(
							$order['Order']['amount_total'],
							[
								'before' => '£',
								'places' => 2
							]
						);
					?>
				</td>
			</tr>

		</tbody>

	</table>

</div>
