<?php
	$returnPath = urlencode($this->Html->url(['admin' => false, 'controller' => 'orders', 'action' => 'configure'], true));
	$encodedJobID = urlencode($coverJobId);
	$appURL = "https://webapp.printui.com/printui/d001";
?>

<script type="text/javascript" src="<?php echo $appURL; ?>/swfobject.js"></script>
<style type="text/css" media="screen">
	html, body  { height:100%; }
	/*body { margin:0; padding:0; overflow:auto; text-align:center; background-color: #ffffff; }*/
	object:focus { outline:none; }
	div.flashContent {max-width:80%; margin: 0 auto; position: relative; display:block; text-align:left; }
	/*div.flashContent:after { .clear-floats; }*/
</style>
<!-- Enable Browser History by replacing useBrowserHistory tokens with two hyphens -->
<!-- BEGIN Browser History required section -->
<link rel="stylesheet" type="text/css" href="<?php echo $appURL; ?>/history/history.css" />
<script type="text/javascript" src="<?php echo $appURL; ?>/history/history.js"></script>
<!-- END Browser History required section -->
<script type="text/javascript">
	// For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection.
	var swfVersionStr = "10.2.0";
	var xiSwfUrlStr = "<?php echo $appURL; ?>/playerProductInstall.swf";
	var flashvars = new Object;
	flashvars.jobId="<?php echo $encodedJobID; ?>";
	flashvars.path="<?php echo $returnPath; ?>"; // This is a URL-encoded string of the "go-to-when-finished" URL
	flashvars.client="pagevisions";
	flashvars.showDefaultColors="false";
	flashvars.allowStartOver="true";
	flashvars.allowFullScreenInteractive="false";
	flashvars.showUndoLabels="true";
	flashvars.allowColorSampling="true";
	flashvars.showAddRectangle="true";
	flashvars.finishedButtonLabel="FINISHED";
	var params = new Object;
	params.quality = "high";
	params.bgcolor = "#ffffff";
	params.allowscriptaccess = "always";
	params.allowfullscreen = "true";
	var attributes = new Object;
	attributes.id = "PrintUI";
	attributes.name = "PrintUI";
	attributes.align = "middle";
	var winW = 630, winH = 460;
	if (document.body && document.body.offsetWidth) {
		winW = document.body.offsetWidth;
		winH = document.body.offsetHeight;
	}
	if (document.compatMode=='CSS1Compat' &&
		document.documentElement &&
		document.documentElement.offsetWidth ) {
		winW = document.documentElement.offsetWidth;
		winH = document.documentElement.offsetHeight;
	}
	if (window.innerWidth && window.innerHeight) {
		winW = window.innerWidth;
		winH = window.innerHeight;
	}
	var HeaderHeight = 50;
	var MaxSwfHeight = winH - HeaderHeight;
	var MaxHeightRatio = (MaxSwfHeight * 100) / winH;
	//swfobject.embedSWF("<?php echo $appURL; ?>/PrintUI.swf", "flashContent","100%", "800px", swfVersionStr, xiSwfUrlStr,flashvars, params, attributes);
	
	function showDesignTool() {
		swfobject.embedSWF("<?php echo $appURL; ?>/PrintUI.swf", "flashContent","100%", "800px", swfVersionStr, xiSwfUrlStr,flashvars, params, attributes);
	}
</script>