<div class="list-group-item fix-option" data-class="fix-option-panel">

	<div class="media ">

		<div class="media-body">

			<h4>Option <?php echo $optionNumber; ?>: <?php echo $heading; ?></h4>

			<p><?php echo $message; ?></p>

			<ul class="list-unstyled list-inline">

				<?php foreach ($actions as $action) { ?>

					<li>

						<?php
							$class = !empty($action['class'])
								? $action['class']
								: 'btn btn-primary'
							;

							echo $this->Form->create(
								'OrderItem',
								[
									'url' => [
										'controller' => 'order_items',
										'action' => 'apply_fix'
									],
									'inputDefaults' => [
										'label' => false,
										'div' => false,
										'type' => 'hidden'
									],
									'data-class' => 'fix-option-form'
								]
							);

								echo $this->Form->input('OrderItem.fix_code', ['value' => $action['fix_code']]);

								if (!empty($action['data'])) {
									foreach ($action['data'] as $dataName => $dataValue) {
										echo $this->Form->input('FixData.' . $dataName, ['value' => $dataValue]);
									}
								}

								echo $this->Form->button(
									$action['label'],
									[
										'type' => 'submit',
										'class' => $class,
										'escape' => false,
										'data-class' => 'fix-option-button'
									]
								);

							echo $this->Form->end();

						?>

					</li>

				<?php } ?>

			</ul>

		</div>

		<div class="media-right">

			<?php
				if (!empty($image)) {
					echo $this->Html->image($image);
				}
			?>

		</div>

	</div>

</div>
