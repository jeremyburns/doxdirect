<div class="panel panel-default">

	<div class="panel-heading">
		<i class="fa fa-folder-o fa-rotate-90"></i> Would you like to add tab dividers?
	</div>

	<div class="table-responsive">

		<table class="table table-condensed table-bordered">

			<thead>
				<tr>
					<th>Before page</th>
					<th>Text</th>
					<th></th>
				</tr>
			</thead>

			<tbody>

				<?php if ($tabs) {

					foreach ($tabs as $tab) { ?>

						<tr>
							<td><?php echo $tab['before_page']; ?></td>
							<td><?php echo $tab['text']; ?></td>
							<td>
								<?php
									echo $this->Html->link(
										'<i class="fa fa-trash"></i> Delete',
										array(
											'controller' => 'order_item_tabs',
											'action' => 'delete',
											$tab['id']
										),
										array('escape' => false)
									);
								?>
							</td>
						</tr>

					<?php }

				}

				echo $this->Form->create(
					'OrderItemTab',
					array(
						'url' => array(
							'controller' => 'order_item_tabs',
							'action' => 'add'
						),
						'inputDefaults' => array(
							'label' => false,
							'class' => 'form-control'
						)
					)
				);

				echo $this->Form->hidden(
					'OrderItemTab.order_item_id',
					array(
						'value' => $order['OrderItem'][0]['id']
					)
				); ?>

					<tr>

						<td><?php echo $this->Form->input('before_page'); ?></td>
						<td><?php echo $this->Form->input('text'); ?></td>
						<td>
							<?php
								echo $this->Form->button(
									'<i class="fa fa-plus"></i> Add',
									array(
										'type' => 'submit',
										'class' => 'btn btn-sm btn-primary',
										'escape' => false
									)
								);
							?>
						</td>

					</tr>

				<?php echo $this->Form->end(); ?>

			</tbody>

		</table>

	</div>

</div>
