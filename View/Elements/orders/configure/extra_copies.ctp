<div class="panel panel-default">

	<div class="panel-heading">
		<i class="fa fa-sort-amount-desc"></i> Extra copies are cheaper
	</div>

	<div class="table-responsive">

		<table class="table table-condensed table-bordered">

			<thead>
				<tr>
					<th>Number of copies</th>
					<th class="text-right">Price per copy (&pound;)</th>
				</tr>
			</thead>

			<tbody>

				<tr>
					<td class="td-label">1</td>
					<td class="text-right">1.00</td>
				</tr>

				<tr>
					<td class="td-label">100+</td>
					<td class="text-right">0.75</td>
				</tr>

				<tr>
					<td class="td-label">500+</td>
					<td class="text-right">0.50</td>
				</tr>

				<tr>
					<td class="td-label">1000+</td>
					<td class="text-right">0.25</td>
				</tr>

			</tbody>

		</table>

	</div>

	<div class="panel-footer">
		<?php
			echo $this->Form->create(
				null,
				array(
					'url' => ['admin' => false, 'controller' => 'orders', 'action' => 'configure'],
					'class' => 'form-inline',
					'inputDefaults' => array(
						'div' => 'form-group',
						'label' => false,
						'wrapInput' => false,
						'class' => 'form-control'
					),
					'role' => 'form'
				)
			);

		?>

			<div class="form-group">
				<?php
					echo $this->Form->input(
						'copies',
						array(
							'div' => false,
							'label' => 'How many copies shall we print?',
							'placeholder' => 1,
							'type' => 'number'
						)
					);

					echo $this->Form->button(
						'Update',
						array(
							'type' => 'submit',
							'div' => 'form-group',
							'class' => 'mls btn btn-primary btn-sm'
						)
					);
				?>
			</div>

			<?php echo $this->Form->end(); ?>
	</div>

</div>