<div class="table-responsive">

	<table class="table table-condensed">

		<tbody>

			<tr>
				<td class="td-label">Name</td>
				<td><?php echo $orderItem['name']; ?></td>
			</tr>

			<tr>
				<td class="td-label">Printed size</td>
				<td><?php echo $orderItem['Media']['name']; ?> (<?php echo $orderItem['Media']['PaperSize']['width']; ?>mm x <?php echo $orderItem['Media']['PaperSize']['height']; ?>mm)</td>
			</tr>

			<tr>
				<td class="td-label">Number of sides</td>
				<td><?php echo $orderItem['Side']['sides']; ?></td>
			</tr>

		</tbody>

	</table>

</div>