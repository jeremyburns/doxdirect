<div class="panel panel-default">
	<div class="panel-heading">
		<i class="fa fa-columns"></i> Design a Cover for your Book
	</div>
	<div class="panel-body">
		<p>Use our Cover Designer to design an outer cover for your book. We can automatically place the first and last pages of your document onto the cover for you, or give you a blank template to start from. Alternatively, we can produce a plain white cover - the choice is yours.</p>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Front Cover</div>
					<div class="panel-body">
						<p>
							<select id="front_cover_options" name="front_cover_options" class="form-control"<?php if($orderItem['printed_cover_job_id'] && ($orderItem['printed_cover_front_options'] != "blank_cover" && $orderItem['printed_cover_back_options'] != "blank_cover")): ?> disabled<?php endif; ?>>
								<option value="use_and_discard"<?php if($orderItem['printed_cover_front_options'] == "use_and_discard"): ?> selected="selected"<?php endif; ?>>Use page 1 of your document (remove from inside the book)</option>
								<option value="start_from_blank"<?php if($orderItem['printed_cover_front_options'] == "start_from_blank"): ?> selected="selected"<?php endif; ?>>Design your own cover (page 1 will be printed inside the book)</option>
								<option value="blank_cover"<?php if($orderItem['printed_cover_front_options'] == "blank_cover"): ?> selected="selected"<?php endif; ?>>Have a blank cover (front and back, page 1 will be printed inside the book)</option>
							</select>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Back Cover</div>
					<div class="panel-body">
						<p>
							<select id="back_cover_options" name="back_cover_options" class="form-control"<?php if($orderItem['printed_cover_job_id'] && ($orderItem['printed_cover_front_options'] != "blank_cover" && $orderItem['printed_cover_back_options'] != "blank_cover")): ?> disabled<?php endif; ?>>
								<option value="use_and_discard"<?php if($orderItem['printed_cover_back_options'] == "use_and_discard"): ?> selected="selected"<?php endif; ?>>Use page <?php echo $orderItem['pages']; ?> of your document (remove from inside the book)</option>
								<option value="start_from_blank"<?php if($orderItem['printed_cover_back_options'] == "start_from_blank"): ?> selected="selected"<?php endif; ?>>Design your own cover (page <?php echo $orderItem['pages']; ?> will be printed inside the book)</option>
								<option value="blank_cover"<?php if($orderItem['printed_cover_back_options'] == "blank_cover"): ?> selected="selected"<?php endif; ?>>Have a blank cover (front and back, page <?php echo $orderItem['pages']; ?> will be printed inside the book)</option>
							</select>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div id="cover_designer_info" class="row <?php if($orderItem['printed_cover_front_options'] != "blank_cover" && $orderItem['printed_cover_back_options'] != "blank_cover"): ?>hidden<?php endif; ?>">
			<div class="col-xs-24">
				<div class="alert alert-info">If you're happy with blank covers, proceed to the Checkout. If you'd like to modify your cover, choose your options above.</div>
			</div>
		</div>
		<input type="hidden" id="printed_cover_job_id" value="<?php echo $orderItem['printed_cover_job_id']; ?>">
		<?php if($orderItem['printed_cover_job_id'] && ($orderItem['printed_cover_front_options'] != "blank_cover" && $orderItem['printed_cover_back_options'] != "blank_cover")): ?>
		<div id="delete_cover_block" class="row">
			<div class="col-sm-12">
				<button id="cover_button" type="button" class="btn btn-primary btn-lg">Edit Your Cover Design</button>
			</div>
			<div class="col-sm-12">
				<p>This is a representation of the cover you have designed.</p>
				<p><img src="https://ids.w2p-tools.com/d003/getresult.php?c=pagevisions&type=png&res=500&id=<?php echo urlencode($orderItem['printed_cover_job_id']); ?>" alt="Your Designed Cover" class="img-responsive" /></p>
				<p><button id="delete_cover_button" type="button" class="btn btn-sm btn-danger">Delete this Cover Design</button></p>
			</div>
		</div>
		<?php else: ?>
		<p>
			<button id="cover_button" type="button" class="btn btn-primary btn-lg<?php if($orderItem['printed_cover_front_options'] == "blank_cover" && $orderItem['printed_cover_back_options'] == "blank_cover"): ?> hidden<?php endif; ?>">Go to Cover Designer</button>
		</p>
		<?php endif; ?>
	</div>
</div>