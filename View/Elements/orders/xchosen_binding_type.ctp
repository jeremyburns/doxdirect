<div class="caption">

	<div class="media mbs">

		<div class="media-body">

			<p>You have chosen</p>

			<h3 id="chosen-binding-type-name" class="media-heading"></h3>

			<?php echo $this->Html->link(
				'<i class="fa fa-refresh"></i> Change...',
				array(
					'controller' => 'orders',
					'action' => 'upload'
				),
				array(
					'id' => 'choose-again',
					'class' => 'btn btn-default btn-xs',
					'escape' => false
				)
			); ?>

		</div>

		<div class="media-right">
			<?php echo $this->Html->image(
				CLOUDFRONTUPLOADS . DS . 'paperback003_transparent.png',
				array(
					'alt' => 'Paperback book',
					'width' => 100,
					'id' => 'chosen-binding-type-image-name',
					'data-path' => CLOUDFRONTUPLOADS . DS
				)
			); ?>
		</div>

	</div>

</div>
