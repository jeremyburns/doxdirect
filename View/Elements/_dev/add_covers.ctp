<div class="form-group checkbox">

	<?php

		echo $this->Form->label(
			'OrderItem.' . $orderItemKey . '.covers',
			$this->BootstrapForm->labelWithPopover (
				'Covers:',
				'Front/back covers',
				'Would you like covers on your document?'
			),
			array(
				'class' => $labelClass . ' ptt'
			)
		);

	?>

	<div class="<?php echo $inputDivClassWide; ?> checkbox">

		<label>

			<?php echo $this->Form->checkbox(
				'OrderItem.' . $orderItemKey . '.covers',
				array(
					'id' => 'show-covers'
				)
			); ?>

			Add front or back covers

		</label>

	</div>

</div>