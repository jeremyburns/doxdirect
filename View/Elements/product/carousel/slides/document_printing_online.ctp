<h2>Document Printing Online</h2>
<h5>Online document printing with Doxdirect saves you time and money.</h5>
<p>Simply upload your files and choose your binding options.</p>
<p>We will print, bind and deliver them to your home, work or university – or directly to your clients.</p>
<?php echo $this->element('global_content/get_started'); ?>