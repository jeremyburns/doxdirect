<h2>Book printing</h2>
<h5>Print a bookshop quality novel, comic, portfolio or report</h5>
<ul class="bullet">
	<li>Hardback and paperback binding</li>
	<li>A4 and A5 sizes available</li>
	<li>Free online cover design</li>
	<li>From £1.85 per copy</li>
</ul>
<?php echo $this->element('global_content/get_started'); ?>