<h2>Everyday Document Printing</h2>
<h5>Professional quality and same day turnaround for everyday printing</h5>
<ul class="bullet">
	<li>Bound or loose leaf</li>
	<li>A4 and A5 sizes available</li>
	<li>No minimum order number</li>
	<li>From £0.04 per sheet</li>
</ul>
<?php echo $this->element('global_content/get_started'); ?>