<!-- The slides are only 12 cols wide so the text 'floats' left. The image is on the background 'floated' right -->
<div class="product-carousel">

	<ul class="slides">

		<li class="slide">

			<div class="container">

				<div class="row documents">

					<div class="col-sm-12">

						<?php echo $this->element('product/carousel/slides/document_printing_online'); ?>

					</div>

				</div>

			</div>

		</li>

		<li class="slide">

			<div class="container">

				<div class="row everyday">

					<div class="col-sm-12">	

						<?php echo $this->element('product/carousel/slides/everyday_printing'); ?>

					</div>

				</div>

			</div>

		</li>

		<li class="slide">

			<div class="container">

				<div class="row book">

					<div class="col-sm-12">

						<?php echo $this->element('product/carousel/slides/book_printing'); ?>

					</div>

				</div>

			</div>

		</li>

		<li class="slide">

			<div class="container">

				<div class="row dissertation">

					<div class="col-sm-12">

						<?php echo $this->element('product/carousel/slides/dissertation_printing'); ?>

					</div>

				</div>

			</div>

		</li>

	</ul>

</div>