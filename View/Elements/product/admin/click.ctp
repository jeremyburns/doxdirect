<table class="table table-bordered table-condensed">

	<caption>Clicks</caption>

	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Pace name</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach($productClicks as $productClick) { ?>

		<tr>
			<td>
				<?php
					echo $this->Html->link(
						$productClick['Click']['id'],
						[
							'admin' => true,
							'controller' => 'clicks',
							'action' => 'view',
							$productClick['Click']['id']
						]
					);
				?>
			</td>
			<td>
				<?php
					echo $this->Html->link(
						$productClick['Click']['name'],
						[
							'admin' => true,
							'controller' => 'clicks',
							'action' => 'view',
							$productClick['Click']['id']
						]
					);
				?>
			</td>
			<td><?php echo $productClick['Click']['pace_name']; ?></td>
		</tr>

		<?php } ?>

	</tbody>

</table>
