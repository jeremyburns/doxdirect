<table class="table table-bordered table-condensed">

	<caption>Media</caption>

	<thead>
		<tr>
			<th colspan="6"></th>
			<th colspan="4" class="text-center">Min/Max</th>
		</tr>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Pace name</th>
			<th>Paper size</th>
			<th>Thickness</th>
			<th>Weight</th>
			<th>Thickness</th>
			<th>Leaves</th>
			<th>Width</th>
			<th>Height</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach($product['ProductMedia'] as $productMedia) { ?>

		<tr>
			<td>
				<?php
					echo $this->Html->link(
						$productMedia['Media']['id'],
						[
							'admin' => true,
							'controller' => 'media',
							'action' => 'view',
							$productMedia['Media']['id']
						]
					);
				?>
			</td>
			<td>
				<?php
					echo $this->Html->link(
						$productMedia['Media']['name'],
						[
							'admin' => true,
							'controller' => 'media',
							'action' => 'view',
							$productMedia['Media']['id']
						]
					);
				?>
			</td>
			<td><?php echo $productMedia['Media']['pace_name']; ?></td>
			<td>
				<?php
					echo $this->Html->link(
						$productMedia['Media']['paper_size_id'],
						[
							'admin' => true,
							'controller' => 'paper_sizes',
							'action' => 'view',
							$productMedia['Media']['paper_size_id']
						]
					);
				?>
			</td>
			<td><?php echo $productMedia['Media']['thickness']; ?></td>
			<td><?php echo $productMedia['Media']['weight']; ?></td>
			<td><?php echo $productMedia['thickness_min']; ?>/<?php echo $productMedia['thickness_max']; ?></td>
			<td><?php echo $productMedia['leaves_min']; ?>/<?php echo $productMedia['leaves_max']; ?></td>
			<td><?php echo $productMedia['format_width_min']; ?>/<?php echo $productMedia['format_width_max']; ?></td>
			<td><?php echo $productMedia['format_height_min']; ?>/<?php echo $productMedia['format_height_max']; ?></td>
		</tr>

		<?php } ?>

	</tbody>

</table>
