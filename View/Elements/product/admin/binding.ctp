<table class="table table-bordered table-condensed">

    <caption>Bindings</caption>

    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Pace name</th>
        </tr>
    </thead>

    <tbody>

        <?php foreach($productBindings as $productBinding) { ?>

        <tr>
            <td>
                <?php
                    echo $this->Html->link(
                        $productBinding['Binding']['id'],
                        [
                            'admin' => true,
                            'controller' => 'bindings',
                            'action' => 'view',
                            $productBinding['Binding']['id']
                        ]
                    );
                ?>
            </td>
            <td>
                <?php
                    echo $this->Html->link(
                        $productBinding['Binding']['name'],
                        [
                            'admin' => true,
                            'controller' => 'bindings',
                            'action' => 'view',
                            $productBinding['Binding']['id']
                        ]
                    );
                ?>
            </td>
            <td><?php echo $productBinding['Binding']['pace_name']; ?></td>
        </tr>

        <?php } ?>

    </tbody>

</table>
