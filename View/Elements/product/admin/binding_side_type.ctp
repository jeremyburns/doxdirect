<table class="table table-bordered table-condensed">

	<caption>Binding sides</caption>

	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Pace name</th>
		</tr>
	</thead>

	<tbody>

		<?php if (!$productBindingSideTypes) { ?>

			<tr>
				<td colspan="3">There are no binding sides linked with this product.</td>
			</tr>

		<?php } else {

			foreach($productBindingSideTypes as $productBindingSideType) { ?>

				<tr>
					<td>
						<?php
							echo $this->Html->link(
								$productBindingSideType['BindingSideType']['id'],
								[
									'admin' => true,
									'controller' => 'binding_side_types',
									'action' => 'view',
									$productBindingSideType['BindingSideType']['id']
								]
							);
						?>
					</td>
					<td>
						<?php
							echo $this->Html->link(
								$productBindingSideType['BindingSideType']['name'],
								[
									'admin' => true,
									'controller' => 'binding_side_types',
									'action' => 'view',
									$productBindingSideType['BindingSideType']['id']
								]
							);
						?>
					</td>
					<td><?php echo $productBindingSideType['BindingSideType']['pace_name']; ?></td>
				</tr>

			<?php }

		} ?>

	</tbody>

</table>
