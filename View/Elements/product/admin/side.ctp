<table class="table table-bordered table-condensed">

	<caption>Sides</caption>

	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Pace name</th>
			<th>Sides</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach($productSides as $productSide) { ?>

		<tr>
			<td>
				<?php
					echo $this->Html->link(
						$productSide['Side']['id'],
						[
							'admin' => true,
							'controller' => 'sides',
							'action' => 'view',
							$productSide['Side']['id']
						]
					);
				?>
			</td>
			<td>
				<?php
					echo $this->Html->link(
						$productSide['Side']['name'],
						[
							'admin' => true,
							'controller' => 'sides',
							'action' => 'view',
							$productSide['Side']['id']
						]
					);
				?>
			</td>
			<td><?php echo $productSide['Side']['pace_name']; ?></td>
			<td><?php echo $productSide['Side']['sides']; ?></td>
		</tr>

		<?php } ?>

	</tbody>

</table>
