<table class="table table-bordered table-condensed">

	<caption>Binding edges</caption>

	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Pace name</th>
		</tr>
	</thead>

	<tbody>

		<?php if (!$productBindingEdgeTypes) { ?>

			<tr>
				<td colspan="3">There are no binding edges linked with this product.</td>
			</tr>

		<?php } else {

				foreach($productBindingEdgeTypes as $productBindingEdgeType) { ?>

				<tr>
					<td>
						<?php
							echo $this->Html->link(
								$productBindingEdgeType['BindingEdgeType']['id'],
								[
									'admin' => true,
									'controller' => 'binding_edge_types',
									'action' => 'view',
									$productBindingEdgeType['BindingEdgeType']['id']
								]
							);
						?>
					</td>
					<td>
						<?php
							echo $this->Html->link(
								$productBindingEdgeType['BindingEdgeType']['name'],
								[
									'admin' => true,
									'controller' => 'binding_edge_types',
									'action' => 'view',
									$productBindingEdgeType['BindingEdgeType']['id']
								]
							);
						?>
					</td>
					<td><?php echo $productBindingEdgeType['BindingEdgeType']['pace_name']; ?></td>
				</tr>

			<?php }

		} ?>

	</tbody>

</table>
