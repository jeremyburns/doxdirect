<table class="table table-bordered table-condensed">

	<caption>Products</caption>

	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Binding</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach($products as $product) { ?>

		<tr>
			<td>
				<?php
					echo $this->Html->link(
						$product['Product']['id'],
						[
							'admin' => true,
							'controller' => 'products',
							'action' => 'view',
							$product['Product']['id']
						]
					);
				?>
			</td>
			<td>
				<?php
					echo $this->Html->link(
						$product['Product']['name'],
						[
							'admin' => true,
							'controller' => 'products',
							'action' => 'view',
							$product['Product']['id']
						]
					);
				?>
			</td>
			<td>
				<?php
					echo $this->Html->link(
						$product['Product']['BindingType']['name'],
						[
							'admin' => true,
							'controller' => 'binding_types',
							'action' => 'view',
							$product['Product']['BindingType']['id']
						]
					);
				?>
			</td>
		</tr>

		<?php } ?>

	</tbody>

</table>
