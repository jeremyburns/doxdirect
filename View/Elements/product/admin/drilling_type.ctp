<table class="table table-bordered table-condensed">

	<caption>Drilling types</caption>

	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Pace name</th>
			<th>Holes</th>
			<th>Leaves</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach($productDrillingTypes as $productDrillingType) { ?>

		<tr>
			<td>
				<?php
					echo $this->Html->link(
						$productDrillingType['DrillingType']['id'],
						[
							'admin' => true,
							'controller' => 'drilling_types',
							'action' => 'view',
							$productDrillingType['DrillingType']['id']
						]
					);
				?>
			</td>
			<td>
				<?php
					echo $this->Html->link(
						$productDrillingType['DrillingType']['name'],
						[
							'admin' => true,
							'controller' => 'drilling_types',
							'action' => 'view',
							$productDrillingType['DrillingType']['id']
						]
					);
				?>
			</td>
			<td><?php echo $productDrillingType['DrillingType']['pace_name']; ?></td>
			<td><?php echo $productDrillingType['DrillingType']['holes']; ?></td>
			<td><?php echo $productDrillingType['DrillingType']['leaves']; ?></td>
		</tr>

		<?php } ?>

	</tbody>

</table>
