<table class="table table-bordered table-condensed">

    <caption>Tab types</caption>

    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Pace name</th>
            <th>Thickness</th>
            <th>Weight</th>
        </tr>
    </thead>

    <tbody>

        <?php foreach($productTabTypes as $productTabType) { ?>

        <tr>
            <td>
                <?php
                    echo $this->Html->link(
                        $productTabType['TabType']['id'],
                        [
                            'admin' => true,
                            'controller' => 'tab_types',
                            'action' => 'view',
                            $productTabType['TabType']['id']
                        ]
                    );
                ?>
            </td>
            <td>
                <?php
                    echo $this->Html->link(
                        $productTabType['TabType']['name'],
                        [
                            'admin' => true,
                            'controller' => 'tab_types',
                            'action' => 'view',
                            $productTabType['TabType']['id']
                        ]
                    );
                ?>
            </td>
            <td><?php echo $productTabType['TabType']['pace_name']; ?></td>
            <td><?php echo $productTabType['TabType']['thickness']; ?></td>
            <td><?php echo $productTabType['TabType']['weight']; ?></td>
        </tr>

        <?php } ?>

    </tbody>

</table>
