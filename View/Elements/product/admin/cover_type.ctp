<?php
	$frontBack = [
		'f' => 'Front',
		'b' => 'Back',
		'w' => 'Wrap around'
	];

	$innerOuter = [
		'i' => 'Inner',
		'o' => 'Outer',
		'w' => 'Wrap around'
	];
?>

	<table class="table table-bordered table-condensed">

	<caption>Cover types</caption>

	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Pace name</th>
			<th>Size</th>
			<th>Front/back</th>
			<th>Inner/outer</th>
		</tr>
	</thead>

	<tbody>

		<?php foreach($productCoverTypes as $productCoverType) { ?>

		<tr>
			<td>
				<?php
					echo $this->Html->link(
						$productCoverType['CoverType']['id'],
						[
							'admin' => true,
							'controller' => 'cover_types',
							'action' => 'view',
							$productCoverType['CoverType']['id']
						]
					);
				?>
			</td>
			<td>
				<?php
					echo $this->Html->link(
						$productCoverType['CoverType']['name'],
						[
							'admin' => true,
							'controller' => 'cover_types',
							'action' => 'view',
							$productCoverType['CoverType']['id']
						]
					);
				?>
			</td>
			<td><?php echo $productCoverType['CoverType']['pace_name']; ?></td>
			<td><?php echo $productCoverType['CoverType']['size_name']; ?></td>
			<td><?php echo $frontBack[$productCoverType['CoverType']['front_back']]; ?></td>
			<td><?php echo $innerOuter[$productCoverType['CoverType']['inner_outer']]; ?></td>
		</tr>

		<?php } ?>

	</tbody>

</table>
