<div class="actions">

    <h3><?php echo __('Actions'); ?></h3>

    <div class="btn-group">

        <?php

            if (empty($show)) {
                $show = [];
            }

            if (in_array('save', $show)) {

                echo $this->Form->button(
                    '<i class="fa fa-save"></i> Save',
                    [
                        'class' => 'btn btn-default btn-success',
                        'escape' => false
                    ]
                );

            }

            if (in_array('view', $show)) {

                echo $this->Html->link(
                    '<i class="fa fa-chevron-circle-right"></i> View',
                    [
                        'admin' => true,
                        'controller' => $controllerName,
                        'action' => 'view',
                        $data['id']
                    ],
                    [
                        'class' => 'btn btn-default',
                        'escape' => false
                    ]
                );
            }

            if (in_array('edit', $show)) {

                echo $this->Html->link(
                    '<i class="fa fa-pencil-square-o"></i> Edit',
                    [
                        'admin' => true,
                        'controller' => $controllerName,
                        'action' => 'edit',
                        $data['id']
                    ],
                    [
                        'class' => 'btn btn-default',
                        'escape' => false
                    ]
                );
            }

            if (in_array('delete', $show)) {

                echo $this->Form->postLink(
                    '<i class="fa fa-trash-o"></i> Delete',
                    [
                        'admin' => true,
                        'controller' => $controllerName,
                        'action' => 'delete',
                        $data['id']
                    ],
                    [
                        'class' => 'btn btn-default',
                        'escape' => false
                    ],
                    __('Are you sure you want to delete "%s"?', $data[$itemNameField])
                );

            }

            if (in_array('index', $show)) {

                echo $this->Html->link(
                    '<i class="fa fa-list-alt"></i> List all',
                    [
                        'admin' => true,
                        'controller' => $controllerName,
                        'action' => 'index'
                    ],
                    [
                        'class' => 'btn btn-default',
                        'escape' => false
                    ]
                );

            }

            if (in_array('add', $show)) {

                echo $this->Html->link(
                    '<i class="fa fa-plus-square"></i> Add new',
                    [
                        'admin' => true,
                        'controller' => $controllerName,
                        'action' => 'add'
                    ],
                    [
                        'class' => 'btn btn-default',
                        'escape' => false
                    ]
                );

            }

        ?>

    </div>

</div>
