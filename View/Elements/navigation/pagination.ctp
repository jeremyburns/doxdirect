<nav>

	<ul class="pagination pagination-sm">

		<li class="disabled">

			<span>

			<?php

				echo $this->Paginator->counter(
					array(
					'format' => 'range'
					)
				);

			?>

			</span>

		</li>

		<?php

			echo $this->Paginator->first(
				'<i class="fa fa-fast-backward"></i>',
				array(
					'tag' => 'li',
					'escape' => false
				),
				null,
				array(
					'tag' => 'li',
					'class' => 'disabled',
					'disabledTag' => 'span',
					'escape' => false
				)
			);

			echo $this->Paginator->prev(
				'<i class="fa fa-backward"></i>',
				array(
					'tag' => 'li',
					'escape' => false
				),
				null,
				array(
					'tag' => 'li',
					'class' => 'disabled',
					'disabledTag' => 'span',
					'escape' => false
				)
			);

			echo $this->Paginator->numbers(
				array(
					'tag' => 'li',
					'currentTag' => 'span',
					'currentClass' => 'active',
					'separator' => false
				)
			);

			echo $this->Paginator->next(
				'<i class="fa fa-chevron-right"></i>',
				array(
					'tag' => 'li',
					'escape' => false
				),
				null,
				array(
					'tag' => 'li',
					'class' => 'disabled',
					'disabledTag' => 'span',
					'escape' => false
				)
			);

			echo $this->Paginator->last(
				'<i class="fa fa-fast-forward"></i>',
				array(
					'tag' => 'li',
					'escape' => false
				),
				null,
				array(
					'tag' => 'li',
					'class' => 'disabled',
					'disabledTag' => 'span',
					'escape' => false
				)
			);
		?>

	</ul>

</nav>