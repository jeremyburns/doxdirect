<nav class="navbar navbar-inverse navbar-admin" role="navigation">

    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-admin">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php

                echo $this->Html->link(
                    '<i class="fa fa-gear"></i> Admin dashboard',
                    [
                        'admin' => true,
                        'controller' => 'users',
                        'action' => 'dashboard'
                    ],
                    [
                        'class' => 'navbar-brand',
                        'escape' => false
                    ]
                );
            ?>
        </div>

        <div class="collapse navbar-collapse" id="navbar-admin">

            <ul class="nav navbar-nav">

                <?php

                    echo $this->element('navigation/admin_sub/orders');

                    echo $this->element('navigation/admin_sub/quote_options');

                    echo $this->element('navigation/admin_sub/configuration');

                    echo $this->element('navigation/admin_sub/pace');

                    echo $this->element(
                        'navigation/admin_sub/order_search',
                        [
                            'class' => 'navbar-form navbar-left'
                        ]
                    );

                ?>

            </ul>

            <ul class="nav navbar-nav navbar-right">

                <?php
                    echo $this->element(
                        'navigation/admin_sub/login'
                    );
                ?>

            </ul>

        </div>

    </div>

</nav>
