<?php echo $this->Form->create(
	'Order',
	array(
		'url' => array(
			'admin' => true,
			'controller' => 'orders',
			'action' => 'search'
		),
		'class' => !empty($class) ? $class : 'navbar-form navbar-left',
		'role' => 'search'
	)
); ?>

	<div class="form-group">

		<div class="input-group">

			<?php echo $this->Form->input(
				'Order.pace_quote_id',
				array(
					'div' => false,
					'label' => false,
					'type' => 'text',
					'class' => 'form-control',
					'placeholder' => 'Pace quote id...'
				)
			); ?>

			<span class="input-group-btn">

				<button type="submit" class="btn btn-default">Search</button>

			</span>

		</div>

	</div>

<?php echo $this->Form->end();
