<li class="dropdown">

	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Configuration <span class="caret"></span></a>

	<ul class="dropdown-menu" role="menu">

		<li>
			<?php echo $this->Html->link(
				'Configuration',
				array(
					'admin' => true,
					'controller' => 'configurations'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Countries',
				array(
					'admin' => true,
					'controller' => 'countries'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Holidays',
				array(
					'admin' => true,
					'controller' => 'holidays',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Holiday alerts',
				array(
					'admin' => true,
					'controller' => 'holiday_alerts',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Order sources',
				array(
					'admin' => true,
					'controller' => 'sources',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Callas processes',
				array(
					'admin' => true,
					'controller' => 'processes',
					'action' => 'index'
				)
			); ?>
		</li>

		<li class="divider"></li>

		<li>
			<?php echo $this->Html->link(
				'Page images',
				array(
					'admin' => true,
					'controller' => 'order_item_file_pages',
					'action' => 'page_images'
				)
			); ?>
		</li>

	</ul>

</li>
