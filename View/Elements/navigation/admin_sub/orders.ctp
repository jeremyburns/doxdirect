<li class="dropdown">

	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Orders <span class="caret"></span></a>

	<ul class="dropdown-menu" role="menu">

		<li>
			<?php echo $this->Html->link(
				'List',
				array(
					'admin' => true,
					'controller' => 'orders',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Order statuses',
				array(
					'admin' => true,
					'controller' => 'order_statuses',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Job statuses',
				array(
					'admin' => true,
					'controller' => 'job_statuses',
					'action' => 'index'
				)
			); ?>
		</li>

		<li class="divider"></li>

		<li>
			<?php echo $this->Html->link(
				'Promo codes',
				array(
					'admin' => true,
					'controller' => 'promo_codes',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Users',
				array(
					'admin' => true,
					'controller' => 'users',
					'action' => 'index'
				)
			); ?>
		</li>

	</ul>

</li>
