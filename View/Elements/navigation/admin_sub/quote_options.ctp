<li class="dropdown">

	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Quote options <span class="caret"></span></a>

	<ul class="dropdown-menu" role="menu">


		<li>
			<?php echo $this->Html->link(
				'Binding edges',
				array(
					'admin' => true,
					'controller' => 'binding_edge_types',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Binding options',
				array(
					'admin' => true,
					'controller' => 'bindings',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Binding sides',
				array(
					'admin' => true,
					'controller' => 'binding_side_types',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Binding types',
				array(
					'admin' => true,
					'controller' => 'binding_types',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Clicks',
				array(
					'admin' => true,
					'controller' => 'clicks',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Cover types',
				array(
					'admin' => true,
					'controller' => 'cover_types',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Delivery options',
				array(
					'admin' => true,
					'controller' => 'delivery_options',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Drilling types',
				array(
					'admin' => true,
					'controller' => 'drilling_types',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Media',
				array(
					'admin' => true,
					'controller' => 'media',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Paper sizes',
				array(
					'admin' => true,
					'controller' => 'paper_sizes',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Products',
				array(
					'admin' => true,
					'controller' => 'products',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Sides',
				array(
					'admin' => true,
					'controller' => 'sides',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Tab types',
				array(
					'admin' => true,
					'controller' => 'tab_types',
					'action' => 'index'
				)
			); ?>
		</li>

		<li>
			<?php echo $this->Html->link(
				'Turnaround options',
				array(
					'admin' => true,
					'controller' => 'turnaround_options',
					'action' => 'index'
				)
			); ?>
		</li>

	</ul>

</li>
