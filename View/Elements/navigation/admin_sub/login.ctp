<?php

if ($loggedInUser) { ?>

	<li>

		<?php echo $this->Html->link(
			'<i class="fa fa-user"></i> ' . $loggedInUser['User']['username'],
			[
				'admin' => false,
				'controller' => 'users',
				'action' => 'view'
			],
			['escape' => false]
		); ?>

	</li>

	<li>

		<?php echo $this->Html->link(
			'<i class="fa fa-sign-out"></i> Logout',
			[
				'admin' => false,
				'controller' => 'users',
				'action' => 'logout'
			],
			['escape' => false]
		); ?>

	</li>

<?php }
