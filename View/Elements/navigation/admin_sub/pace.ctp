<li class="dropdown">

	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pace <span class="caret"></span></a>

	<ul class="dropdown-menu" role="menu">

		<li role="presentation">
			<?php echo $this->Html->link(
				'Empty cache',
				array(
					'admin' => true,
					'controller' => 'products',
					'action' => 'empty_cache'
				)
			); ?>
		</li>

		<li class="divider"></li>

		<li role="presentation">
			<?php echo $this->Html->link(
				'Delivery options - list',
				array(
					'admin' => true,
					'controller' => 'delivery_options',
					'action' => 'index'
				)
			); ?>
		</li>

		<li role="presentation">
			<?php echo $this->Html->link(
				'Delivery options - refresh',
				array(
					'admin' => true,
					'controller' => 'delivery_options',
					'action' => 'refresh'
				),
				array('confirm' => 'Are you sure you want to refresh delivery options from Pace?')
			); ?>
		</li>

		<li class="divider"></li>

		<li role="presentation">
			<?php echo $this->Html->link(
				'Countries - list',
				array(
					'admin' => true,
					'controller' => 'countries',
					'action' => 'index'
				)
			); ?>
		</li>

		<li role="presentation">
			<?php echo $this->Html->link(
				'Countries - refresh',
				array(
					'admin' => true,
					'controller' => 'countries',
					'action' => 'refresh'
				),
				array('confirm' => 'Are you sure you want to refresh countries from Pace?')
			); ?>
		</li>

		<li class="divider"></li>

		<li role="presentation">
			<?php echo $this->Html->link(
				'Product catalogue',
				array(
					'admin' => true,
					'controller' => 'products',
					'action' => 'product_catalogue'
				)
			); ?>
		</li>

		<li class="divider"></li>

		<li role="presentation">
			<?php echo $this->Html->link(
				'Product map - view',
				array(
					'admin' => true,
					'controller' => 'products',
					'action' => 'product_map'
				)
			); ?>
		</li>

		<li role="presentation">
			<?php echo $this->Html->link(
				'Product map - refresh',
				array(
					'admin' => true,
					'controller' => 'products',
					'action' => 'refresh_product_map'
				),
				array('confirm' => 'Are you sure you want to refresh the product map?')
			); ?>
		</li>

	</ul>

</li>
