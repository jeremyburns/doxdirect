<nav class="navbar navbar-default navbar-public" role="navigation">

	<div class="container">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-products">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<?php
				echo $this->Html->link(
					'<span class="fa fa-home"></span>',
					'/',
					[
						'class' => 'navbar-brand',
						'escape' => false
					]
				);
			?>
		</div>

		<div class="collapse navbar-collapse" id="navbar-products">

			<ul class="nav navbar-nav">

				<li role="presentation">
					<?php echo $this->Html->link(
						'Reset this quote',
						[
							'admin' => false,
							'controller' => 'orders',
							'action' => 'reset_quote'
						]
					); ?>
				</li>

				<?php if ($this->Session->check('Auth.User.role') && $this->Session->read('Auth.User.role') == 'admin') { ?>

					<li role="presentation">
						<?php echo $this->Html->link(
							'Admin dashboard',
							[
								'admin' => true,
								'controller' => 'users',
								'action' => 'dashboard'
							]
						); ?>
					</li>

				<?php } ?>

			</ul>

		</div>

	</div>

</nav>
