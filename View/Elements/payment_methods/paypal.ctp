<p class="lead">1: Pay securely by PayPal using your PayPal account or with a credit card</p>

<ul class="list-inline">
	<li><i class="fa fa fa-cc-paypal fa-2x color-blue"></i></li>
	<li><i class="fa fa fa-cc-mastercard fa-2x color-blue"></i></li>
	<li><i class="fa fa fa-cc-visa fa-2x color-blue"></i></li>
</ul>

<?php

	echo $this->PaypalPayment->button(
		'https://www.paypalobjects.com/en_US/i/btn/x-click-but6.gif',
		array(
			'test' => $config['test'],
			'business' => $config['business'],
			'tax' => $cart['amount_vat'],
			'amount' => $cart['amount_due'],
			'item_name' => $config['item_name'],
			'item_number' => $cart['pace_job_number'],
			'currency_code' => $config['currency_code'],
			'server' => $config['server'],
			'notify_url' => $this->Html->url(
				$config['url']['notify'],
				true
			),
			'return' => $this->Html->url(
				$config['url']['return'],
				true
			),
			'cancel_return' => $this->Html->url(
				$config['url']['cancel_return'],
				true
			),
			'lc' => $config['locale']
		)
	);
