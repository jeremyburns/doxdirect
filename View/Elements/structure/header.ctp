<header>

	<div class="container">

		<div class="row">

			<div class="col-sm-24 col-md-12">

				<?php echo $this->Html->image(
					'doxdirect/logo/doxdirect-logo.png',
					[
						'url' => '/',
						'height' => 65
					]
				); ?>

			</div>

			<div class="col-sm-24 col-md-12">

				<?php echo $this->element('global_content/contact'); ?>

			</div>

		</div>

	</div>

</header>

<div class="strapline">

	<div class="container">

		<div class="row">

			<div class="col-xs-24">

				<ul class="list-inline">

					<?php if (!empty($showResetQuoteButton)) { ?>

						<li role="presentation">
							<?php echo $this->Html->link(
								'Reset this quote',
								[
									'controller' => 'orders',
									'action' => 'reset_quote'
								],
								['class' => 'btn btn-sm btn-default']
							); ?>
						</li>

					<?php }

					if ($this->Session->check('Auth.User.role') && $this->Session->read('Auth.User.role') == 'admin') { ?>

						<li role="presentation">
							<?php echo $this->Html->link(
								'Admin dashboard',
								[
									'admin' => true,
									'controller' => 'users',
									'action' => 'dashboard'
								]
							); ?>
						</li>

					<?php } ?>

				</ul>

				<p class="large">
					<em>Print and bind your documents from one copy upwards.</em>
				</p>
				<p class="small">
					<em>Print and bind your documents from one copy upwards.</em>
				</p>

			</div>

		</div>

	</div>

</div>

<?php
	if ($this->Session->check('Auth.User.is_admin') && $this->Session->read('Auth.User.is_admin') == true) {
		echo $this->element('navigation/admin');
	}
//	else {
//		echo $this->element('navigation/public', array('showProductsInNavigation' => false));
//	}

?>
