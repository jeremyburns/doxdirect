<?php
	$url = !empty($options['url'])
		? $options['url']
		: null
	;

	$formOptions = array_merge(
		array(
			'url' => $url
		),
		$options['formOptions']
	);

	$buttonClass = empty($options['buttonOptions']['class'])
		? 'btn btn-link'
		: $options['buttonOptions']['class']
	;

	$buttonOptions = !empty($options['buttonOptions'])
		? $options['buttonOptions']
		: array()
	;

	$buttonOptions = array_merge(
		array(
			'escape' => false,
			'type' => 'submit',
			'class' => $buttonClass,
			'role' => 'button'
		),
		$buttonOptions
	);

	echo $this->Form->create(
		null,
		$formOptions
	);

		echo $this->Form->button(
			$options['label'],
			$buttonOptions
		);

	echo $this->Form->end();