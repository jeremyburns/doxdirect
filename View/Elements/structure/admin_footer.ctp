<footer class="footer">

	<div class="container">

		<div class="row">

			<div class="col col-xs-24">
				<p class="footnote">Copyright &copy; Doxdirect Limited 2015. All rights reserved. | <?php echo $this->Html->link('Privacy', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'privacy']); ?> & <?php echo $this->Html->link('Cookies', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'cookies']); ?> | <?php echo $this->Html->link('T&Cs', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'terms']); ?> | <?php echo $this->Html->link('Sitemap', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'sitemap']); ?></p>
			</div>

		</div>

	</div>

</footer>