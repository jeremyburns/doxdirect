<footer class="footer">

	<div class="container">

		<div class="row">

			<div class="col col-xs-12 col-sm-6">

				<nav>

					<h4>Products</h4>

					<ul class="list-unstyled">
						<li>
							<?php echo $this->Html->link(
								'Everyday document printing',
								'/everyday-document-printing'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Book publishing',
								'/book-publishing'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Thesis/Dissertation printing',
								'/thesis-dissertation-printing'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Brochures',
								'/brochures'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Leaflets and flyers',
								'/leaflets-flyers'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Manuals',
								'/manuals'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Posters',
								'/posters'
							);?>
						</li>
					</ul>

				</nav>

			</div>

			<div class="col col-xs-12 col-sm-6">

				<nav>

					<h4>Binding Options</h4>

					<ul class="list-unstyled">
						<li>
							<?php echo $this->Html->link(
								'Wire binding',
								'/wire-binding'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Ring binding',
								'/ring-binding'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Saddle stitch',
								'/saddle-stitch'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Corner stapling',
								'/corner-stapling'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Paper back books',
								'/paper-back-books'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'Hard cover books',
								'/hard-cover-books'
							);?>
						</li>
						<li>
							<?php echo $this->Html->link(
								'No binding',
								'/no-binding'
							);?>
						</li>
					</ul>

				</nav>

			</div>

			<div class="col col-xs-16 col-sm-8">

				<h4>Contact us</h4>

				<div class="row">

					<div class="col col-xs-10">

						<address>
							Doxdirect Limited<br>
							Perth House<br>
							Perth Trading Estate<br>
							Slough<br>
							SL1 4XX
						</address>

					</div>

					<div class="col col-xs-14">
						<ul class="list-unstyled">
							<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-at fa-stack-1x fa-inverse"></i>
								</span> <a href="mailto:service@doxdirect.com" target="_top">service@doxdirect.com</a>
							</li>
							<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span> 0333 200 7272
							</li>
						</ul>

					</div>

				</div>

			</div>

			<div class="col col-xs-8 col-sm-4">
				<h4>We accept</h4>
				<p><i class="fa fa-cc-mastercard fa-4x"></i></p>
				<p><i class="fa fa-cc-paypal fa-4x"></i></p>
				<p><i class="fa fa-cc-visa fa-4x"></i></p>
				<p>
					<?php
						echo $this->Html->image(
							'logos/norton/norton_100w.png',
							array(
								'width' => 100
							)
						);
					?>
				</p>
			</div>

		</div>

		<div class="row">

			<div class="col col-xs-24">
				<p class="footnote">Copyright &copy; Doxdirect Limited 2015. All rights reserved. | <?php echo $this->Html->link('Privacy', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'privacy']); ?> & <?php echo $this->Html->link('Cookies', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'cookies']); ?> | <?php echo $this->Html->link('T&Cs', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'terms']); ?> | <?php echo $this->Html->link('Sitemap', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'sitemap']); ?></p>
			</div>

		</div>

	</div>

</footer>

<?php
	echo $this->Form->input(
		'flash-path',
		[
			'type' => 'hidden',
			'value' => $this->Html->url([
				'admin' => false,
				'controller' => 'utilities',
				'action' => 'ajax_flash'
			]),
		]
	);

	echo $this->Form->input(
		'element-path',
		[
			'type' => 'hidden',
			'value' => $this->Html->url([
				'admin' => false,
				'controller' => 'utilities',
				'action' => 'ajax_element'
			]),
		]
	);
