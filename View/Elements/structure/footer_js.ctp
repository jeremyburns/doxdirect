<?php

	if (Configure::read('debug') < 1) {
		echo $this->Html->script('global/google_analytics.min');
	}

	echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js');
	echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js');
	echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
	echo $this->Html->script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js');

	echo $this->Html->script('global/global.min');

	if (!empty($jsScripts)) {
		foreach ($jsScripts as $jsScript) { ?>
			<script src='<?php echo $jsScript; ?>'></script>
		<?php }
	}

	if (!empty($js)) {
		echo $this->Html->script($js);
	}

	if (!empty($jsVariables)) {
		foreach ($jsVariables as $variableName => $variableValue) { ?>
			<script>
				var <?php echo $variableName; ?> = <?php echo $variableValue; ?>;
			</script>
		<?php }
	}

	echo $this->fetch('script');

?>