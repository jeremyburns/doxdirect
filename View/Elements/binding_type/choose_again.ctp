<?php echo $this->element('structure/hr', array('colClass' => 'xs-24')); ?>

<div id="binding-type-choose-again">

	<div class="row">

		<div class="col-sm-24">

			<h3>Choose a different binding type</h3>

		</div>

	</div>

	<div class="row thumbnails">

		<?php foreach ($bindingTypes as $bindingType) {

			$bindingType = $bindingType['BindingType']; ?>

			<div class="col-sm-12 col-md-6">

				<div class="media">
					<div class="media-left">
						<?php echo $this->Html->image(
							CLOUDFRONTUPLOADS . DS . $bindingType['image_name'],
							array(
								'alt' => $bindingType['name'],
								'width' => 75
							)
						); ?>
					</div>
					<div class="media-body">
						<h4 class="media-heading"><?php echo $bindingType['name']; ?></h4>
						<p>
							<?php echo $this->element(
								'structure/postlink',
								array(
									'options' => array(
										'url' => array(
											'controller' => 'orders',
											'action' => 'upload'
										),
										'formOptions' => array(
											'id' => 'form-binding-type-' . $bindingType['slug'],
											'class' => 'form-select-binding-type',
											'data-binding-type-id' => $bindingType['id'],
											'data-binding-type-slug' => $bindingType['slug'],
											'data-binding-type-name' => $bindingType['name'],
											'data-binding-type-image-name' => $bindingType['image_name']
										),
										'label' => 'Select',
										'buttonOptions' => array(
											'class' => 'btn btn-default btn-xs'
										)
									)
								)
							); ?>
						</p>
					</div>
				</div>
			</div>

		<?php } ?>

	</div>

</div>
