<div id="binding-type-choose" class="row">

	<div class="thumbnails">

		<div class="col-sm-12 col-md-8 col-lg-6">

			<div class="thumbnail binding-type-choice">

				<img src="<?php echo CLOUDFRONTUPLOADS; ?>/paperback003_transparent.png" alt="Document printing">

				<div class="caption">
					<h3>Instructions</h3>
					<ol>
						<li>Select the binding type you would like to apply to your document.</li>
						<li>Click 'Add files' and choose one or more documents to print.</li>
						<li>Click 'Start upload' to begin uploading your files.</li>
					</ol>
				</div>

			</div>

		</div>

		<?php foreach ($bindingTypes as $bindingType) {

			$bindingType = $bindingType['BindingType']; ?>

			<div class="col-sm-12 col-md-8 col-lg-6">

				<div class="thumbnail binding-type-choice">

					<?php echo $this->Html->image(
						CLOUDFRONTUPLOADS . DS . $bindingType['image_name'],
						array(
							'alt' => $bindingType['name']
						)
					); ?>

					<div class="caption">

						<h3><?php echo $bindingType['name']; ?></h3>
						<h4>From <?php echo $this->Number->currency($bindingType['from'], 'GBP'); ?> each</h4>
						<p><?php echo $bindingType['strapline']; ?></p>
						<p>
							<?php echo $this->element(
								'structure/postlink',
								array(
									'options' => array(
										'url' => array(
											'controller' => 'orders',
											'action' => 'upload'
										),
										'formOptions' => array(
											'id' => 'form-binding-type-' . $bindingType['slug'],
											'class' => 'form-select-binding-type',
											'data-binding-type-id' => $bindingType['id'],
											'data-binding-type-slug' => $bindingType['slug'],
											'data-binding-type-name' => $bindingType['name'],
											'data-binding-type-image-name' => $bindingType['image_name']
										),
										'label' => 'Select',
										'buttonOptions' => array(
											'class' => 'btn btn-primary'
										)
									)
								)
							); ?>
						</p>

					</div>

				</div>

			</div>

		<?php } ?>

	</div>

</div>