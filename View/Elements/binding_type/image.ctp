<span id="product-image">
	<?php
		if (!empty($imageName)) {
			echo $this->Html->image(
				'doxdirect/product/' . $imageName,
				array(
					'width' => 50
				)
			);
		}
	?>
</span>