<?php

if (count($order['OrderItem']) > 1) {

	echo $this->element(
		'previewer/order_item_tabs',
		array(
			'orderItems' => $order['OrderItem']
		)
	);

}

foreach ($order['OrderItem'] as $orderItem) {

	$orientation = $orderItem['file_orientation'] === 'l'
		? 'landscape'
		: 'portrait'
	;

	// Is it single or double sided?
	$sides = !empty($orderItem['Side']['sides'])
		? intval($orderItem['Side']['sides'])
		: 1
	;

	$duplex = $sides == 2
		? 'double'
		: 'single'
	;

	// Do we need to show a binding image?
	$bindingImage = empty($orderItem['Product']['BindingType']['image'])
		? false
		: $orderItem['Product']['BindingType']['image']
	;

	$options = [
		'orderItemPages' => $orderItemPages,
		'sides' => $sides,
		'pageCount' => $pageCount,
		'orientation' => $orientation
	];

	if ($bindingImage) {

		if ($orientation === 'landscape') {
			$options['bindingImage'] = 'bindings' . DS . 'landscape' . DS . $bindingImage;
		} else {
			$options['bindingImage'] = 'bindings' . DS . 'portrait' . DS . $bindingImage;
		}

		$bound = 'bound';
	} else {
		$bound = 'unbound';
	}

	$elementPath = 'previewer' . DS . $duplex . DS . $orientation . DS . $bound;

?>

	<div class="panel panel-primary">

		<div class="panel-heading">
			<i class="fa fa-eye"></i> Preview of <?php echo $orderItem['name']; ?>
		</div>

		<div class="panel-body shaded">

			<div class="row">

				<div class="col-sm-24">

					<div class="previewer" id="previewer" data-orientation="<?php echo $orientation; ?>" data-duplex="<?php echo $duplex; ?>" data-bound="<?php echo $bound; ?>" data-page-count="<?php echo $pageCount; ?>">

						<?php

							if ($pageCount >= 1) {

								echo $this->element(
									'previewer' . DS . $duplex . DS . 'arrows'
								);

								echo $this->element(
									$elementPath,
									$options
								);

							}

						?>

					</div>

				</div>

			</div>

		</div>

	</div>

<?php }
