<div class="row">

	<div class="col-xs-24 col-sm-12 col-sm-offset-6">

		<?php echo $this->element(
			'previewer/pages/left'
		); ?>

	</div>

</div>

<div class="row">

	<div class="col-xs-24 col-sm-12 col-sm-offset-6">

		<?php
			if (!empty($bindingImage)) {

				echo $this->Html->image(
					$bindingImage,
					['class' => 'spine']
				);
			}
		?>

	</div>

</div>

<div class="row">

	<div class="col-xs-24 col-sm-12 col-sm-offset-6">

		<?php echo $this->element(
			'previewer/pages/right'
		); ?>

	</div>

</div>
