<div id="page-numbers" class="row">

	<div class="col-xs-12">

		<p class="page-number">

			<?php echo $this->element('previewer/link_previous'); ?>

			<span id="page-number-left" class="label label-default"></span>

		</p>

	</div>

	<div class="col-xs-12">

		<p class="page-number">

			<span id="page-number-right" class="label label-default"></span>

			<?php echo $this->element('previewer/link_next'); ?>

		</p>

	</div>

</div>
