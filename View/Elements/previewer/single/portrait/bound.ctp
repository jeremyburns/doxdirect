<div class="row">

	<div class="col-xs-11">

		<?php echo $this->element(
			'previewer/pages/blank'
		); ?>

	</div>

	<div class="col-xs-2">

		<?php
			if ($bindingImage) {

				echo $this->Html->image(
					$bindingImage,
					['class' => 'spine']
				);
			}
		?>

	</div>

	<div class="col-xs-11">

		<?php echo $this->element(
			'previewer/pages/right'
		); ?>

	</div>

</div>
