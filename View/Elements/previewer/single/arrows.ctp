<div id="page-numbers" class="row">

	<div class="col-xs-24">

		<p class="page-number">

			<?php echo $this->element('previewer/link_previous'); ?>

			<span id="page-number-single" class="label label-default"></span>

			<?php echo $this->element('previewer/link_next'); ?>

		</p>

	</div>

</div>
