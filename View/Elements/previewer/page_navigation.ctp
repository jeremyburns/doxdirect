<div id="pagination" class="row">

	<div class="col-xs-24">

		<nav>

			<ul class="pagination">

				<!-- We want to always show a button with the first page number -->

				<li>

					<?php echo $this->Html->link(
						'<span aria-hidden="true">1</span>',
						[
							'controller' => 'orders',
							'action' => 'configure',
							$orderItemId,
							1
						],
						[
							'escape' => false,
							'aria-label' => 'Page 1',
							'class' => 'page-number-link',
							'data-page-number' => 1
						]
					); ?>

				</li>

				<!-- Now loop through the rest -->

				<?php for ($pageNumberKey = 1; $pageNumberKey < $pageCount; $pageNumberKey += $sides) {

					$pageNumber = $pageNumberKey + 1;

					if ($sides === 1) {
						$label = $pageNumber;
					} else {
						$label = $pageNumber . '-' . ($pageNumber + 1);
					} ?>

					<li>

						<?php echo $this->Html->link(
							'<span aria-hidden="true">' . $label . '</span>',
							[
								'controller' => 'orders',
								'action' => 'configure',
								$orderItemId,
								$pageNumber
							],
							[
								'escape' => false,
								'aria-label' => 'Page ' . $pageNumber,
								'class' => 'page-number-link',
								'data-page-number' => $pageNumber
							]
						); ?>

					</li>

				<?php } ?>

			</ul>

		</nav>

	</div>

</div>
