<?php
	echo $this->Html->link(
		'<i class="fa fa-chevron-left"></i>',
		'#',
		[
			'id' => 'page-link-previous',
			'escape' => false,
//			'class' => 'btn btn-link btn-lg page-number-link mrs',
			'class' => 'btn btn-link btn-lg page-number-link page-left mrs',
			'data-page-number' => '',
			'data-turn' => 'previous'
		]
	);
