<div id="<?php echo $id; ?>" class="alert alert-info page-loader" role="alert">
	<p class="lead"><i class="fa fa-circle-o-notch fa-spin"></i> Please wait while we generate your preview...</p>
</div>
