<?php
// paid.ctp
// Generates JavaScript to finalise purchase for Google Analytics ecommerce
?>

<script>
ga('require', 'ec');
ga('set', '&cu', 'GBP');

<?php foreach($order['OrderItem'] as $orderItem) { ?>
ga('ec:addProduct', {
	'id': '<?php echo $orderItem['product_id']; ?>',
	'name': '<?php echo $orderItem['Product']['name']; ?>',
	'category': '<?php echo $orderItem['Product']['BindingType']['name']; ?>/<?php echo $orderItem['Product']['paper_size_id']; ?>/<?php echo $orderItem['Click']['name']; ?>',
	'variant': '<?php echo $orderItem['Click']['name']; ?>, <?php echo $orderItem['pages']; ?> pages, <?php echo $orderItem['Side']['name']; ?>',
	'price': '<?php echo ($orderItem['amount_net'] / $orderItem['copies']); ?>',
	'quantity': <?php echo $orderItem['copies']; ?>
});
<?php } ?>

ga('ec:setAction', 'checkout', { 'step': 2 });

ga('ec:setAction', 'purchase', {
	'id': '<?php echo $order['Order']['pace_job_number']; ?>',
	'revenue': '<?php echo $order['Order']['amount_total']; ?>',
	'shipping': '<?php echo $order['Order']['amount_delivery']; ?>'<?php if(floatval($order['Order']['amount_discount']) > 0): ?>,
	'coupon': '<?php echo $order['PromoCode']['code']; ?>'<?php endif; ?>
});
</script>