<?php
// pay_now.ctp
// Generates JavaScript to add OrderItem objects to Google Analytics ecommerce
?>

<script>
ga('require', 'ec');
ga('set', '&cu', 'GBP');
</script>

<?php
foreach($order['OrderItem'] as $orderItem) {
	?>
	<script>		
		ga('ec:addProduct', {
			'id': '<?php echo $orderItem['product_id']; ?>',
			'name': '<?php echo $orderItem['Product']['name']; ?>',
			'category': '<?php echo $orderItem['Product']['BindingType']['name']; ?>/<?php echo $orderItem['Product']['paper_size_id']; ?>/<?php echo $orderItem['Click']['name']; ?>',
			'variant': '<?php echo $orderItem['Click']['name']; ?>, <?php echo $orderItem['pages']; ?> pages, <?php echo $orderItem['Side']['name']; ?>',
			'price': '<?php echo $orderItem['amount_net'] / $orderItem['copies']; ?>',
			'quantity': <?php echo $orderItem['copies']; ?>
		});
	</script>
	<?php
}
?>
<script>
	ga('ec:setAction', 'checkout', {
		'step': 1
	});
</script>