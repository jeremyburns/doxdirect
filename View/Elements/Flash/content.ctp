<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

<?php if (!empty($message['heading'])) { ?>

	<p class="lead"><?php echo $icon . ' ' . $message['heading']; ?></p>

	<?php if (!empty($message['message'])) { ?>
		<p><?php echo $message['message']; ?></p>
	<?php } ?>

<?php } elseif (!empty($message['message'])) { ?>

	<p><?php echo $icon . ' ' . $message['message']; ?></p>

<?php } else { ?>

	<p><?php echo $icon . ' ' . $message; ?></p>

<?php } ?>
