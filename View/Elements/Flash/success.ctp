<?php
	$icon = !empty($message['icon']) ? $message['icon'] : 'fa-check-circle';
	$icon = '<i class="fa ' . $icon . '"></i>';
?>

<div class="alert alert-success alert-dismissible fade in" role="alert">

	<?php echo $this->element(
		'Flash' . DS . 'content',
		[
			'icon' => $icon,
			'message' => $message
		]
	); ?>

</div>
