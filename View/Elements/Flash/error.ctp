<?php
	$icon = !empty($message['icon']) ? $message['icon'] : 'fa-times-circle';
	$icon = '<i class="fa ' . $icon . '"></i>';
?>

<div class="alert alert-error alert-dismissible fade in" role="alert">

	<?php echo $this->element(
		'Flash' . DS . 'content',
		[
			'icon' => $icon,
			'message' => $message]
	); ?>

</div>
