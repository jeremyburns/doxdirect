<div class="panel panel-default">
	<div class="panel-heading">
		Today's orders
	</div>
	<table class="table">
		<tbody>
			<tr>
				<td>Number of orders</td>
				<td class="text-right">9</td>
			</tr>
			<tr>
				<td>Revenue</td>
				<td class="text-right">£856.39</td>
			</tr>
		</tbody>
	</table>
</div>