<div class="panel panel-default">
	<div class="panel-heading">
		This month's orders
	</div>
	<table class="table">
		<tbody>
			<tr>
				<td>Number of orders</td>
				<td class="text-right">385</td>
			</tr>
			<tr>
				<td>Revenue</td>
				<td class="text-right">£24,723.73</td>
			</tr>
		</tbody>
	</table>
</div>