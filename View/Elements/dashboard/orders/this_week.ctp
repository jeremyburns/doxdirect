<div class="panel panel-default">
	<div class="panel-heading">
		This week's orders
	</div>
	<table class="table">
		<tbody>
			<tr>
				<td>Number of orders</td>
				<td class="text-right">86</td>
			</tr>
			<tr>
				<td>Revenue</td>
				<td class="text-right">£3,551.05</td>
			</tr>
		</tbody>
	</table>
</div>