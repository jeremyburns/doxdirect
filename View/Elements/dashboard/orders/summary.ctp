<div class="panel panel-default">
	<div class="panel-heading">
		Order Summary
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Period</th>
				<th class="text-right">Count</th>
				<th class="text-right">Revenue</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Today</td>
				<td class="text-right">9</td>
				<td class="text-right">£856.39</td>
			</tr>
			<tr>
				<td>Week to date</td>
				<td class="text-right">86</td>
				<td class="text-right">£3,551.05</td>
			</tr>
			<tr>
				<td>Month to date</td>
				<td class="text-right">385</td>
				<td class="text-right">£24,723.73</td>
			</tr>
			<tr>
				<td>Year to date</td>
				<td class="text-right">1,845</td>
				<td class="text-right">£74,239.84</td>
			</tr>
		</tbody>
	</table>
</div>