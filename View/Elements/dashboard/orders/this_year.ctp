<div class="panel panel-default">
	<div class="panel-heading">
		This year's orders
	</div>
	<table class="table">
		<tbody>
			<tr>
				<td>Number of orders</td>
				<td class="text-right">1,845</td>
			</tr>
			<tr>
				<td>Revenue</td>
				<td class="text-right">£74,239.84</td>
			</tr>
		</tbody>
	</table>
</div>