<div class="contact-details">

	<div class="contact-details-inner large">&nbsp;

		<div class="header-links">
			<?php

				echo $this->element('orders/checkout/header_cart');

				echo $this->element(
					'users/login_link'
				);

			?>
		</div>

		<div class="header-contact">

			<span class="header-phone">0333 200 7272</span>

			<span class="header-contact-icons">

				<a href="mailto:service@doxdirect.com">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x solid"></i>
						<i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
					</span>
				</a>

				<a href="https://twitter.com/Doxdirect" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x solid"></i>
						<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
					</span>
				</a>

				<a href="https://www.facebook.com/doxdirect" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x solid"></i>
						<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
					</span>
				</a>

				<a href="https://plus.google.com/103282843558341836830/posts" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x solid"></i>
						<i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
					</span>
				</a>

			</span>

		</div>

	</div>

	<div class="contact-details-inner small">&nbsp;

		<div class="header-links">
			<?php

				echo $this->element('orders/checkout/header_cart');

				echo $this->element(
					'users/login_link'
				);

			?>
		</div>

		<div class="header-contact">

			<span class="header-phone">0333 200 7272</span>

			<span class="header-contact-icons">

				<a href="mailto:service@doxdirect.com">
					<i class="fa fa-envelope"></i>
				</a>

				<a href="https://twitter.com/Doxdirect" target="_blank">
					<i class="fa fa-twitter"></i>
				</a>

				<a href="https://www.facebook.com/doxdirect" target="_blank">
					<i class="fa fa-facebook"></i>
				</a>

				<a href="https://plus.google.com/103282843558341836830/posts" target="_blank">
					<i class="fa fa-google-plus"></i>
				</a>

			</span>

		</div>

	</div>

</div>
