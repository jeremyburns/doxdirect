<?php
	echo $this->Html->image(
		'logos/pdf_50w.png',
		array(
			'width' => 50,
			'class' => 'mrs'
		)
	);

	echo $this->Html->image(
		'logos/word_50w.png',
		array(
			'width' => 50,
			'class' => 'mrs'
		)
	);

	echo $this->Html->image(
		'logos/powerpoint_50w.png',
		array(
			'width' => 50,
			'class' => 'mrs'
		)
	);