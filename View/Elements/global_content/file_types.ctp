<div class="panel panel-info">
	<div class="panel-heading">
		You can upload these <strong>document types</strong>
	</div>
	<div class="panel-body">
		<?php echo $this->element('global_content/file_type_images'); ?>
		<p>We recommend PDFs. <?php echo $this->Html->link('Why?', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'pdf'], array('class' => 'popup')); ?></p>
	</div>
</div>