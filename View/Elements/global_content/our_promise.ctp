<div class="inner-tube bordered">

	<h4 class="color-blue"><i class="fa fa-heart-o"></i> Our promise to you...</h4>
	<h3>Love us or your money back</h3>
	<p>If you aren't happy with your order, return it and we'll reprint it or issue a refund.</p>

	<?php echo $this->element('global_content/norton_seal/small'); ?>

</div>