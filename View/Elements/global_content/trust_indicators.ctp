<div class="row">

	<div class="col-sm-24 col-md-8">

		<div class="panel panel-success trust">
			<div class="panel-heading">
				<i class="fa fa-check"></i> Simple
			</div>
			<div class="panel-body">
				<ul>
					<li>No need to register</li>
					<li>No minimum order - we'll print a single copy</li>
					<li>Just upload your document and go</li>
				</ul>
			</div>
		</div>

	</div>

	<div class="col-sm-24 col-md-8">

		<div class="panel panel-success trust">
			<div class="panel-heading">
				<i class="fa fa-trophy"></i> Service
			</div>
			<div class="panel-body">
				<ul>
					<li>Free Royal Mail delivery on most orders</li>
					<li>Next day courier options</li>
					<li>Fast turnaround option for urgent jobs</li>
				</ul>
			</div>
		</div>

	</div>

	<div class="col-sm-24 col-md-8">

		<div class="panel panel-success trust">
			<div class="panel-heading">
				<i class="fa fa-smile-o"></i> Satisfaction
			</div>
			<div class="panel-body">
				<ul>
					<li>Hand finishing on all but the simplist of jobs</li>
					<li>Love us or your money back</li>
					<li>Premium quality FSC paper</li>
				</ul>
			</div>
		</div>

	</div>

</div>







