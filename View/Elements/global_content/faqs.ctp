<h3><a name="document-types"></a>What type of documents can I upload to Doxdirect?</h3>

<p>You may upload any digital documents (files) that are generated in Microsoft Word, PowerPoint and Adobe PDF files. They can be either portrait or landscape in orientation.</p>

<h3><a name="non-standard-document-sizes"></a>Can I upload files that are not standard A series sizes?</h3>

<p>Yes, but the dimensions may not be interpreted correctly by our system. As our service is automated, we are currently only able to automatically process documents that are produced in standard A series sizes, from A5 up to A0. For non-A series documents, please contact us as we can process these for you manually. We will be introducing additional document sizes into our product range at regular intervals, so please check back for up-to-date information. You may also upload the American version of the A4 size known as US letter. The dimensions of the US letter format are slightly different at 216mm wide by 279mm high.The industry standard viewing program is made by Adobe and is available free of charge as a download. Go to <a href="http://www.adobe.com/products/reader.html" target="_blank" rel="nofollow">http://www.adobe.com/products/reader/</a> if you do not have a copy installed on your computer. The main benefit of a PDF file is that once it has been created all the information regarding fonts, images and page format are preserved.</p>

<h3><a name="convert-xl-to-pdf"></a>I have a Microsoft Excel document that I should like to convert to a PDF. How do I do this?</h3>

<p>If you have the 2007 or 2010 editions of Microsoft Office you will have the option of saving your completed Excel spreadsheet as a PDF file. Just click on &#8216;Save As&#8217; and PDF will appear as one of the options. If you have an older version of Microsoft Office this option is not available. Unfortunately the Adobe Acrobat program is not free of charge, but there are several free PDF converters available on the internet. You may wish to try <a href="http://www.e-pdfconverter.com" target="_blank" rel="nofollow">www.e-pdfconverter.com</a>.</p>

<p><strong>Please note</strong>: Doxdirect has no connection with or responsibility for external sites. We are simply providing information on where and how you may be able to obtain software to enable you to convert any of your documents to a PDF format.</p>

<h3><a name="pdf-not-uploading"></a>I have converted my document into a PDF but it&#8217;s not uploading correctly. What do I do?</h3>

<p>Sometimes if you save your Word or PowerPoint document as a PDF it may become corrupted. If you experience this problem, please try uploading the original Word or PowerPoint document to our website. If an error still occurs, then please contact us for further help.</p>

<h3><a name="unable-to-process-this-document"></a>I am able to upload my document but am directed to a page saying &#8216;Unable to process this document. What do I do?</h3>

<p>If you can upload your document but are not being directed to a page where you can select the binding and printing options, then it is possible that the browser you&#8217;re using is experiencing a temporary blip. As a first resort, please try clearing the cache or browser history, or reboot your computer. If you still experience any problems, then please contact us.</p>

<h3><a name="file-size-limits"></a>I am having problems uploading my file &#8211; is there a limit on file size?</h3>

<p>Yes, we do have a limit of 200MB. However, on occasion you may experience problems uploading files that are smaller than this if your internet connection is either slow or slightly intermittent. In this case please contact us and we shall provide you with an alternative method of transferring your files to Doxdirect.</p>

<h3><a name="cover-design-tool"></a>I am having problems using the Cover Design Tool as the content is not displaying correctly. What do I do?</h3>

<p>To get the best out of the cover designer we recommend you update to the latest version of your internet browser. The cover designer functions well on Internet Explorer versions 8 and above, as well as on Mozilla Firefox and Google Chrome. If you are still encountering problems, then please either delete your browser history or try rebooting your computer.</p>

<h3><a name="hole-punching-options"></a>What are the hole-punching options for Ring Binding?</h3>

<p>For A5 and A4 ring binders, you can upload a portrait or landscape document and choose to bind it on the long or short edge. This gives four options; portrait document bound at the left (long edge &#8211; pages turn from the right) or bound at the top (short edge &#8211; pages turn from the bottom); landscape document bound at the left (short edge &#8211; pages turn from the right) or bound at the top (long edge &#8211; pages turn from the bottom). You can choose 2-hole punching for the most economical solution, or 4-hole punching for the most sturdy solution.</p>
<p>A5 ring binders can be hole-punched with either 2 or 4 holes. If you choose the 2-hole option, they will be spaced 80mm apart and centred on the page. If you choose the 4-hole option the middle two holes will be spaced 65mm apart, centred on the page, with 45mm between these and the top/bottom holes. Holes are punched 10mm from the binding edge for both options.</p>
<p>A4 ring binders can also be hole-punched with either 2 or 4 holes. For both options, holes are spaced 80mm apart and 10mm from the binding edge.</p>

<h3><a name="processing-delivery-times"></a>What are your minimum processing and delivery times?</h3>

<p>We offer a &#8216;same day&#8217; turnaround time option, which means that we will process your order on the same day if we receive it before 11am. Exceptions to this are orders for hardback and paperback books, which need an additional 2 days processing time before despatch. If you require a fast same day service, we recommend that you also select Weekday Courier as the delivery option.</p>

<h3><a name="standard-processing-time"></a>What is your standard order processing time?</h3>

<p>Orders are generally despatched 2 working days (Monday to Friday) from the day of receipt. For example, if you order on a Tuesday, then your order will be shipped on Thursday. If you order on a Friday, then your order will be despatched on the following Tuesday. Exceptions to this are orders for hardback and paperback books which need an additional 2 days processing time for despatch.</p>

<h3><a name="public-holidays-and-weekends"></a>Do you process orders during public holidays and weekends?</h3>

<p>We only process orders on weekdays, not at weekends or over public holidays. So if you place your order on a Saturday it will be processed as though it has been placed on the following Monday.</p>

<h3><a name="next-day-delivery"></a>What is the best delivery option for a next day delivery?</h3>

<p>We recommend that you select Weekday Courier for guaranteed next day delivery. There are also timed options available if you need it by a certain time that day.</p>

<p>If you haven&#8217;t found the answer to your question in the list above, please <?php echo $this->Html->link('Contact Us', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'contact']); ?> and we&#8217;ll help you out.</p>
