<?php $icon = '<i class="fa fa-question-circle"></i>'; ?>

<div class="row">

	<div class="col-sm-18">

		<h4>Frequently Asked Questions</h4>

		<div class="panel-group panel-faq" id="faq-choices" role="tablist" aria-multiselectable="true">

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-choices-paper-type">

					<a data-toggle="collapse" data-parent="#faq-choices" href="#faq-answer-choices-paper-type" aria-expanded="false" aria-controls="faq-answer-choices-paper-type">
						<?php echo $icon; ?> What is the best paper type for my document?
					</a>

				</div>

				<div id="faq-answer-choices-paper-type" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-choices-paper-type">
					<div class="panel-body">
						<p>Paper is measured by weight in 'gsm' (grams per square meter). Quality, paper weight and price increase as the gsm gets higher. We recommend:</p>
						<table class="table table-condensed">
							<tbody>
								<tr>
									<td>For simple <strong>black and white</strong> documents that are mostly text:</td>
									<td>90gsm</td>
									<td>For a quality yet cost effective finish.</td>
								</tr>
								<tr>
									<td>For simple <strong>colour</strong> documents that are mostly text:</td>
									<td>100gsm</td>
									<td>For a quality yet cost effective finish with bright colours.</td>
								</tr>
								<tr>
									<td>For a document with quite a lot of <strong>images</strong>:</td>
									<td>120gsm or 170gsm</td>
									<td>Best for making your images bright and clear.</td>
								</tr>
								<tr>
									<td>For anything that needs to be printed on a <strong>heavier paper</strong> (for example a greetings card):</td>
									<td>280gsm</td>
									<td>A nice card weight for substantial feeling documents.</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>

			</div>

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-choices-binding-type">

					<a data-toggle="collapse" data-parent="#faq-choices" href="#faq-answer-choices-binding-type" aria-expanded="false" aria-controls="faq-answer-choices-binding-type">
						<?php echo $icon; ?> I've chosen the wrong binding type! What do I do?
					</a>

				</div>

				<div id="faq-answer-choices-binding-type" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-choices-binding-type">
					<div class="panel-body">
						<p>Relax, it's going to be okay. Simply click on the link after the Document Specification above, and you can go back and choose another binding option for your document. You will find suggestions for which type of documents our binding types are best for on that page.</p>
					</div>
				</div>

			</div>

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-choices-preview">

					<a data-toggle="collapse" data-parent="#faq-choices" href="#faq-answer-choices-preview" aria-expanded="false" aria-controls="faq-answer-choices-preview">
						<?php echo $icon; ?> My document preview hasn't loaded yet. What's going on?
					</a>

				</div>

				<div id="faq-answer-choices-preview" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-choices-preview">
					<div class="panel-body">
						<p>Some documents can take longer than others to appear in the previewer. If your document is on the large side – over 5mb for example – please be patient and it will appear. Alternatively you can click on Refresh Preview to reload the preview. Thanks.</p>
					</div>
				</div>

			</div>

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-choices-design-tool">

					<a data-toggle="collapse" data-parent="#faq-choices" href="#faq-answer-choices-design-tool" aria-expanded="false" aria-controls="faq-answer-choices-design-tool">
						<?php echo $icon; ?> I am having trouble using the cover design tool. Help!
					</a>

				</div>

				<div id="faq-answer-choices-design-tool" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-choices-design-tool">
					<div class="panel-body">
						<p>The most effective way to solve this is by making sure your browser is updated with the latest version. The tool functions best in recent versions of Firefox, Chrome, Safari and Internet Explorer. If you are using the latest version of any of these browsers and still encounter problems, please clear your browser history or try rebooting your computer.</p>
					</div>
				</div>

			</div>

		</div>

	</div>

	<div class="col-sm-6 hidden-xs">

		<?php echo $this->Html->image(
			CLOUDFRONTUPLOADS . DS . 'Dox-confused.png',
			array(
				'Frequently Asked Questions'
			)
		); ?>

	</div>

</div>
