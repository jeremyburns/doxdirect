<?php $icon = '<i class="fa fa-question-circle"></i>'; ?>

<div class="row">

	<div class="col-sm-18">

		<h3>Frequently Asked Questions</h3>

		<div class="panel-group panel-faq" id="faq-review" role="tablist" aria-multiselectable="true">

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-review-times">

					<a data-toggle="collapse" data-parent="#faq-review" href="#faq-answer-review-times" aria-expanded="false" aria-controls="faq-answer-review-times">
						<?php echo $icon; ?> What are your minimum processing and delivery times?
					</a>

				</div>
				<div id="faq-answer-review-times" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-review-times">
					<div class="panel-body">
						<p>If we receive your order <strong>before 11am and you choose 'same day turnaround'</strong> we will process and produce your order that day. Delivery times are separate from turnaround time, so please choose a 'courier' option for next day delivery, or free UK delivery for Royal Mail first class postage (usually 1-2 working days).</p>
						<p><strong>Please note:</strong> Hardback and Paperback books take 2 days longer than the selected turnaround time. If we receive your order before 11am and you choose 'same day turnaround' we will process and produce your order that day. Delivery times are separate from turnaround time, so please choose a 'courier' option for next day delivery, or free UK delivery for Royal Mail first class postage (usually 1-2 working days). <strong>Please note:</strong> Hardback and Paperback books take 2 days longer than the selected turnaround time.</p>
					</div>
				</div>

			</div>


			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-review-standard-times">

					<a data-toggle="collapse" data-parent="#faq-review" href="#faq-answer-review-standard-times" aria-expanded="false" aria-controls="faq-answer-review-standard-times">
						<?php echo $icon; ?> What is your standard order processing time?
					</a>

				</div>
				<div id="faq-answer-review-standard-times" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-review-standard-times">
					<div class="panel-body">
						<p>If you choose the standard 'two day turnaround' option and free UK delivery, your oder will be processed, produced and despatched within 2 working days, and delivery will be by Royal Mail first class (usually 1-2 working days).</p>
					</div>
				</div>

			</div>


			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-review-holidays">

					<a data-toggle="collapse" data-parent="#faq-review" href="#faq-answer-review-holidays" aria-expanded="false" aria-controls="faq-answer-review-holidays">
						<?php echo $icon; ?> Do you process orders during public holidays and weekends?
					</a>

				</div>
				<div id="faq-answer-review-holidays" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-review-holidays">
					<div class="panel-body">
						<p>We process orders on weekdays only, and not on weekends or bank holidays. If you order on a Friday before 11am and choose 'same day turnaround' it will be processed that day. In all other cases it will be processed the following Monday (or Tuesday if there is a bank holiday).</p>
					</div>
				</div>

			</div>


			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-review-next-day">

					<a data-toggle="collapse" data-parent="#faq-review" href="#faq-answer-review-next-day" aria-expanded="false" aria-controls="faq-answer-review-next-day">
						<?php echo $icon; ?> What is the best delivery option for a next day delivery?
					</a>

				</div>
				<div id="faq-answer-review-next-day" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-review-next-day">
					<div class="panel-body">
						<p>We recommend that you choose 'same day turnaround' and one of our courier options.</p>
					</div>
				</div>

			</div>

		</div>

	</div>

	<div class="col-sm-6 hidden-xs">

		<?php echo $this->Html->image(
			CLOUDFRONTUPLOADS . DS . 'Dox-confused.png',
			array(
				'Frequently Asked Questions'
			)
		); ?>

	</div>

</div>
