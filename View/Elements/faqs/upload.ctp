<?php $icon = '<i class="fa fa-question-circle"></i>'; ?>

<div class="row">

	<div class="col-sm-18">

		<h3>Frequently Asked Questions</h3>

		<div class="panel-group panel-faq" id="faq-upload" role="tablist" aria-multiselectable="true">

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-upload-problem">

					<a data-toggle="collapse" data-parent="#faq-upload" href="#faq-answer-upload-problem" aria-expanded="false" aria-controls="faq-answer-upload-problem">
						<?php echo $icon; ?> Why am I having problems uploading my file?
					</a>

				</div>

				<div id="faq-answer-upload-problem" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-upload-problem">
					<div class="panel-body">
						<p>This could be to do with file size, uploading multiple files, your internet connection or your browser.</p>
						<p>The limit for file uploads on Doxdirect is 150mb, and larger files can be <a href="mailto:service@doxdirect.com">emailed to us</a>. If your file is smaller than 150mb it may have something to do with your internet connection or your browser. Checking your connection or using a different browser may solve the problem. Finally, uploading multiple documents to be merged into one can sometimes take additional time.</p>
						<p>Whatever the reason, if you need assistance with uploading your file please <a href="/contact-us/" target="_blank">Contact Us</a> and we'll help you out.</p>
					</div>
				</div>

			</div>

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-upload-type">

					<a data-toggle="collapse" data-parent="#faq-upload" href="#faq-answer-upload-type" aria-expanded="false" aria-controls="faq-answer-upload-type">
						<?php echo $icon; ?> What type of documents can I upload to Doxdirect?
					</a>

				</div>

				<div id="faq-answer-upload-type" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-upload-type">
					<div class="panel-body">
						<p>You can upload Microsoft Word, Microsoft PowerPoint or Adobe PDF files in portrait or landscape.</p>
						<p>We will convert Word and PowerPoint documents to PDF as best we can. To be sure you are getting exactly what you want, we encourage you to convert the document to PDF yourself and upload the correctly formatted PDF document.</p>
						<p>The 2007 and 2010 editions of Microsoft Office give you the option of saving a file as a PDF (simply click 'save as' and PDF should appear in the list of options).</p>
					</div>
				</div>

			</div>

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-upload-sizes">

					<a data-toggle="collapse" data-parent="#faq-upload" href="#faq-answer-upload-sizes" aria-expanded="false" aria-controls="faq-answer-upload-sizes">
						<?php echo $icon; ?> Can I upload files that are not standard A series sizes?
					</a>

				</div>

				<div id="faq-answer-upload-sizes" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-upload-sizes">
					<div class="panel-body">
						<p>Yes, but we recommend using standard A5-A0 document sides for the best results. If your artwork is a smaller, larger or custom size, please contact us and we will be happy to process your oder manually in the same time as it would take to order online.</p>
					</div>
				</div>

			</div>

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-upload-excel">

					<a data-toggle="collapse" data-parent="#faq-upload" href="#faq-answer-upload-excel" aria-expanded="false" aria-controls="faq-answer-upload-excel">
						<?php echo $icon; ?> I have a Microsoft Excel document that I would like to convert to a PDF. How do I do this?
					</a>

				</div>

				<div id="faq-answer-upload-excel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-upload-excel">
					<div class="panel-body">
						<p>The 2007 and 2010 editions of Microsoft Office give you the option of saving an Excel file as a PDF (simply click 'save as' and PDF should appear in the list of options).</p>
					</div>
				</div>

			</div>

			<div class="panel panel-faq">

				<div class="panel-heading" role="tab" id="faq-upload-error">

					<a data-toggle="collapse" data-parent="#faq-upload" href="#faq-answer-upload-error" aria-expanded="false" aria-controls="faq-answer-upload-error">
						<?php echo $icon; ?> I have converted my document into a PDF but it’s not uploading correctly. What do I do?
					</a>

				</div>

				<div id="faq-answer-upload-error" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-upload-error">
					<div class="panel-body">
						<p>The file may be corrupted, which occasionally happens when saving a Word or PowerPoint file as a PDF. Doxdirect can automatically convert these files to PDF upon upload, so please try uploading the original Word or PowerPoint file. If you still have any trouble, please contact us and we'll see what we can do.</p>
					</div>
				</div>

			</div>

		</div>

	</div>

	<div class="col-sm-6 hidden-xs">

		<?php echo $this->Html->image(
			CLOUDFRONTUPLOADS . DS . 'Dox-confused.png',
			array(
				'Frequently Asked Questions'
			)
		); ?>

	</div>

</div>
