<div class="steps mts mbs">
	<?php foreach ($steps as $step) { ?>

		<div class="<?php echo $step['class']; ?>">
			<i class="fa <?php echo $step['icon']; ?> fa-2x"></i>
			<div class="content">
				<div class="title"><?php echo $step['name']; ?></div>
				<div class="description"><?php echo $step['description']; ?></div>
			</div>
		</div>
	<?php } ?>
</div>