<h3>Add an address</h3>

<?php

	echo $this->Form->create(
		'UserAddress',
		array(
			'url' => [
				'admin' => false,
				'controller' => 'user_addresses',
				'action' => 'add'
			],
			'role' => 'form',
			'class' => 'form-horizontal'
		)
	);

		echo $this->Flash->render();

		if (!empty($result['code']) && $result['code'] == 'potential-duplicate' && !empty($result['existingUserAddresses'])) { ?>

			<div class="panel panel-primary">

				<div class="panel-heading">
					Potential duplicate address(es):
				</div>

				<div class="panel-body">

					<ul>

							<?php foreach ($result['existingUserAddresses'] as $existingUserAddress) { ?>

							<li><?php echo $existingUserAddress; ?></li>

						<?php } ?>

					</ul>

				</div>

				<div class="panel-footer">

					<ul class="list-unstyled list-inline mbn">

						<li>

							<?php if (!empty($result['code']) && $result['code'] == 'potential-duplicate') {

								echo $this->Form->button(
									'<i class="fa fa-check-circle"></i> Save anyway',
									[
										'type' => 'submit',
										'class' => 'btn btn-primary',
										'escape' => false,
										'name' => 'add-duplicate'
									]
								);

							} ?>

						</li>

						<li class="text-info">Or edit the new address and click 'Save' below...</li>

					</ul>

				</div>

			</div>

		<?php }

		echo $this->BootstrapForm->horizontalInputs(array(
			'options' => array(
				'divClass' => 'col-sm-'
			),
			'inputs' => array(
//				'UserAddress.house_number' => array(
//					'label' => 'House/building number',
//					'required' => false,
//					'placeHolder' => 'No',
//					'width-class-label' => THIRD,
//					'width-class-input' => SIXTH
//				),
				'UserAddress.address_1' => array(
					'label' => 'Address 1',
					'required' => true,
					'width-class-label' => THIRD,
					'width-class-input' => HALF
				),
				'UserAddress.address_2' => array(
					'label' => 'Address 2',
					'width-class-label' => THIRD,
					'width-class-input' => HALF
				),
				'UserAddress.city' => array(
					'label' => 'City',
					'width-class-label' => THIRD,
					'width-class-input' => THIRD
				),
				'UserAddress.county' => array(
					'label' => 'County',
					'width-class-label' => THIRD,
					'width-class-input' => THIRD
				),
				'UserAddress.post_code' => array(
					'label' => 'Post code',
					'required' => true,
					'width-class-label' => THIRD,
					'width-class-input' => QUARTER
				),
				'UserAddress.country_id' => array(
					'label' => 'Country',
					'required' => true,
					'width-class-label' => THIRD,
					'width-class-input' => THIRD,
					'type' => 'select',
					'selectOptions' => $countries
				)
			)
		));

	?>

	<div class="btn-group">

		<?php

			echo $this->Form->button(
				'<i class="fa fa-plus-circle"></i> Save',
				[
					'type' => 'submit',
					'class' => 'btn btn-success',
					'escape' => false,
					'name' => 'add'
				]
			);

			echo $this->Html->link(
				'<i class="fa fa-arrow-left"></i> Finished editing',
				[
					'admin' => false,
					'controller' => 'user_addresses',
					'action' => 'finish_edit'
				],
				[
					'class' => 'btn btn-default',
					'escape' => false
				]
			);

		?>

	</div>
