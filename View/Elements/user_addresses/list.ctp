<?php

	$userAddressIndexLink = $this->Html->url([
		'admin' => false,
		'controller' => 'user_addresses',
		'action' => 'index'
	]);

	$userAddressAddLink = $this->Html->url([
		'admin' => false,
		'controller' => 'user_addresses',
		'action' => 'add'
	]);

?>

<span id="user-addresses" data-url-index="<?php echo $userAddressIndexLink; ?>" data-url-add="<?php echo $userAddressAddLink; ?>">

	<h3>Your addresses</h3>

	<?php echo $this->Flash->render(); ?>

	<?php if (empty($userAddresses)) { ?>

		<p class="alert alert-warning">
			<i class="fa fa-warning"></i> We have no active addresses on record for you.
		</p>

	<?php } else { ?>

		<div class="row">

			<?php foreach ($userAddresses as $userAddress) { ?>

				<div class="col-sm-12 col-md-8">

					<div class="thumbnail">

						<div class="caption">

							<?php

								if (!empty($userAddress['UserAddress'])) {
									$userAddress = $userAddress['UserAddress'];
								}

								$userAddressId = $userAddress['id'];

								echo $this->Dox->address($userAddress);

							?>

							<div class="btn-group">

								<?php

									echo $this->Html->link(
										'<i class="fa fa-pencil"></i> Edit',
										[
											'admin' => false,
											'controller' => 'user_addresses',
											'action' => 'edit',
											$userAddressId
										],
										[
											'class' => 'btn btn-sm btn-default',
											'data-user-address' => 'edit',
											'data-user-address-id' => $userAddressId,
											'escape' => false
										]
									);

									echo $this->Html->link(
										'<i class="fa fa-trash-o"></i> Delete',
										[
											'admin' => false,
											'controller' => 'user_addresses',
											'action' => 'delete',
											$userAddressId
										],
										[
											'class' => 'btn btn-sm btn-danger',
											'data-user-address' => 'delete',
											'data-user-address-id' => $userAddressId,
											'escape' => false
										]
									);

								?>

							</div>

						</div>

					</div>

				</div>

			<?php } ?>

		</div>

	<?php } ?>

	<div class="row">

		<div class="col-sm-24">

			<div class="mbs">

				<?php

					echo $this->Html->link(
						'<i class="fa fa-plus-circle"></i> Add address',
						[
							'admin' => false,
							'controller' => 'user_addresses',
							'action' => 'add'
						],
						[
							'id' => 'add-user-address',
							'class' => 'btn btn-success',
							'escape' => false
						]
					);

					echo $this->Html->link(
						'<i class="fa fa-undo"></i> Cancel',
						[
							'admin' => false,
							'controller' => 'orders',
							'action' => 'resume'
						],
						array(
							'class' => 'btn btn-link hide',
							'escape' => false,
							'data-action' => 'resume'
						)
					);

				?>

			</div>

		</div>

	</div>

</span>
