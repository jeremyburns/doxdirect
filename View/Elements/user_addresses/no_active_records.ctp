<p class="alert alert-warning">
	<i class="fa fa-warning"></i> We have no active addresses on record for you. Please add one before continuing with your order.
</p>

<p>

	<?php

		echo $this->Html->link(
			'<i class="fa fa-plus-circle"></i> Add an address',
			[
				'admin' => false,
				'controller' => 'user_addresses',
				'action' => 'add'
			],
			[
				'escape' => false,
				'class' => 'btn btn-primary',
				'id' => 'add-user-address'
			]
		);

	?>

</p>
