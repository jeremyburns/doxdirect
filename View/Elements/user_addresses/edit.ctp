<h3>Edit your address</h3>

<?php

	echo $this->Form->create(
		'UserAddress',
		array(
			'url' => [
				'admin' => false,
				'controller' => 'user_addresses',
				'action' => 'edit'
			],
			'role' => 'form',
			'class' => 'form-horizontal'
		)
	);

		echo $this->BootstrapForm->horizontalInputs(array(
			'options' => array(
				'divClass' => 'col-sm-'
			),
			'inputs' => array(
				'referer' => [
					'type' => 'hidden',
					'value' => !empty($referer) ? $referer : null
				],
				'UserAddress.id' => array(
					'type' => 'hidden'
				),
				'UserAddress.house_number' => array(
					'label' => 'House/building number',
					'required' => false,
					'placeHolder' => 'No',
					'width-class-label' => THIRD,
					'width-class-input' => SIXTH
				),
				'UserAddress.address_1' => array(
					'label' => 'Address 1',
					'required' => true,
					'width-class-label' => THIRD,
					'width-class-input' => HALF
				),
				'UserAddress.address_2' => array(
					'label' => 'Address 2',
					'width-class-label' => THIRD,
					'width-class-input' => HALF
				),
				'UserAddress.city' => array(
					'label' => 'City',
					'width-class-label' => THIRD,
					'width-class-input' => THIRD
				),
				'UserAddress.county' => array(
					'label' => 'County',
					'width-class-label' => THIRD,
					'width-class-input' => THIRD
				),
				'UserAddress.post_code' => array(
					'label' => 'Post code',
					'required' => true,
					'width-class-label' => THIRD,
					'width-class-input' => QUARTER
				),
				'UserAddress.country_id' => array(
					'label' => 'Country',
					'required' => true,
					'width-class-label' => THIRD,
					'width-class-input' => THIRD,
					'type' => 'select',
					'selectOptions' => $countries
				)
			)
		));

		echo $this->Form->button(
			'<i class="fa fa-plus-circle"></i> Save',
			[
				'type' => 'submit',
				'class' => 'btn btn-success',
				'escape' => false
			]
		);

		echo $this->Html->link(
			'<i class="fa fa-undo"></i> Cancel',
			[
				'admin' => false,
				'controller' => 'users',
				'action' => 'view'
			],
			[
				'class' => 'btn btn-link',
				'escape' => false,
				'data-user-address' => 'index'
			]
		);
