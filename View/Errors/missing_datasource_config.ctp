<h2><?php echo __d('cake_dev', 'Missing Datasource Configuration'); ?></h2>
<p class="alert alert-error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', 'The datasource configuration %1$s was not found in database.php.', '<em>' . h($config) . '</em>'); ?>
</p>