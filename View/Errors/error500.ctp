<h2><?php echo $message; ?></h2>

<p class="alert alert-error">
	<i class="fa fa-warning"></i> <strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo h($error->getMessage()); ?>
</p>