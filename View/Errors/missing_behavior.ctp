<?php $pluginDot = empty($plugin) ? null : $plugin . '.'; ?>

<h2><?php echo __d('cake_dev', 'Missing Behavior'); ?></h2>
<p class="alert alert-error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', '%s could not be found.', '<em>' . h($pluginDot . $class) . '</em>'); ?>
</p>