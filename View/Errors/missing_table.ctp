<h2><?php echo __d('cake_dev', 'Missing Database Table'); ?></h2>
<p class="alert alert-error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', 'Table %1$s for model %2$s was not found in datasource %3$s.', '<em>' . h($table) . '</em>',  '<em>' . h($class) . '</em>', '<em>' . h($ds) . '</em>'); ?>
</p>