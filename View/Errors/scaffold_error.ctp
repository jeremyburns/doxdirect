<h2><?php echo __d('cake_dev', 'Scaffold Error'); ?></h2>
<p class="alert alert-error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', 'Method _scaffoldError in was not found in the controller'); ?>
</p>