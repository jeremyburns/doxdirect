<h2><?php echo __d('cake_dev', 'Missing Database Connection'); ?></h2>
<p class="alert alert-error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', 'A Database connection using "%s" was missing or unable to connect.', h($class)); ?>
	<br />
	<?php
	if (isset($message)):
		echo __d('cake_dev', 'The database server returned this error: %s', h($message));
	endif;
	?>
</p>
<?php if (!$enabled) : ?>
	<p class="alert alert-error">
		<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
		<?php echo __d('cake_dev', '%s driver is NOT enabled', h($class)); ?>
	</p>
<?php endif; ?>