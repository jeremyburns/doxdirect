<div class="drillingTypes view">

	<h2><?php echo __('Drilling type'); ?></h2>

	<div class="row">

		<div class="col-sm-12">

			<table class="table table-condensed">

				<caption>Details</caption>

				<tr>
					<th><?php echo __('Id'); ?></th>
					<td><?php echo h($drillingType['DrillingType']['id']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Name'); ?></th>
					<td><?php echo h($drillingType['DrillingType']['name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Pace name'); ?></th>
					<td><?php echo h($drillingType['DrillingType']['pace_name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Holes'); ?></th>
					<td><?php echo h($drillingType['DrillingType']['holes']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Leaves'); ?></th>
					<td><?php echo h($drillingType['DrillingType']['leaves']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Default'); ?></th>
					<td><?php echo $this->Presentation->yesNo($drillingType['DrillingType']['is_default']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Active'); ?></th>
					<td><?php echo $this->Presentation->yesNo($drillingType['DrillingType']['is_active']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Order items'); ?></th>
					<td><?php echo h($drillingType['DrillingType']['count_order_item']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Created'); ?></th>
					<td><?php echo h($this->Time->niceShort($drillingType['DrillingType']['created'])); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Modified'); ?></th>
					<td><?php echo h($this->Time->niceShort($drillingType['DrillingType']['modified'])); ?></td>
				</tr>
			</table>

		</div>

		<div class="col-sm-12">

			<?php
				echo $this->element(
					'product/admin/list',
					['products' => $drillingType['ProductDrillingType']]
				);
			?>

		</div>

	</div>

</div>

<div class="actions">
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('List drilling types'), array('action' => 'index'));
		?>
	</div>
</div>
