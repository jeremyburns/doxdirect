<div class="drillingTypes index">

	<h2><?php echo __('Drilling types'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('holes'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('leaves'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($drillingTypes as $drillingType): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($drillingType['DrillingType']['id']),
									array(
										'action' => 'view',
										$drillingType['DrillingType']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($drillingType['DrillingType']['name']),
									array(
										'action' => 'view',
										$drillingType['DrillingType']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($drillingType['DrillingType']['pace_name']); ?></td>
						<td class="text-right"><?php echo h($drillingType['DrillingType']['holes']); ?></td>
						<td class="text-right"><?php echo h($drillingType['DrillingType']['leaves']); ?></td>
						<td><?php echo $this->Presentation->yesNo($drillingType['DrillingType']['is_active']); ?></td>
						<td><?php echo $this->Presentation->yesNo($drillingType['DrillingType']['is_default']); ?></td>
						<td><?php echo h($drillingType['DrillingType']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $drillingType['DrillingType']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>