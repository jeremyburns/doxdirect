<div class="promoCodes index">

	<h2><?php echo __('Promo codes'); ?></h2>

	<table class="table table-condensed">

		<thead>
			<tr>
				<th>Live</th>
				<th><?php echo $this->Paginator->sort('id'); ?></th>
				<th><?php echo $this->Paginator->sort('name'); ?></th>
				<th><?php echo $this->Paginator->sort('code'); ?></th>
				<th><?php echo $this->Paginator->sort('is_active', 'Enabled'); ?></th>
				<th><?php echo $this->Paginator->sort('description'); ?></th>
				<th><?php echo $this->Paginator->sort('valid_from'); ?></th>
				<th><?php echo $this->Paginator->sort('valid_to'); ?></th>
				<th class="text-right"><?php echo $this->Paginator->sort('limit_per_user'); ?></th>
				<th class="text-right"><?php echo $this->Paginator->sort('limit_total'); ?></th>
				<th class="text-right"><?php echo $this->Paginator->sort('minimum_spend'); ?></th>
				<th class="text-right"><?php echo $this->Paginator->sort('discount_amount_limit'); ?></th>
				<th class="text-right"><?php echo $this->Paginator->sort('discount_type'); ?></th>
				<th class="text-right"><?php echo $this->Paginator->sort('apply_discount_to'); ?></th>
				<th class="text-right"><?php echo $this->Paginator->sort('discount_amount'); ?></th>
				<th><?php echo $this->Paginator->sort('user_id'); ?></th>
				<th><?php echo $this->Paginator->sort('count_order'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
		</thead>

		<tbody>

			<?php foreach ($promoCodes as $promoCode) { ?>

				<tr>
					<td>
						<?php if (!empty($promoCode['PromoCode']['is_live'])) { ?>
							<i class="fa fa-heartbeat color-success"></i>
						<?php } ?>
					</td>
					<td><?php echo h($promoCode['PromoCode']['id']); ?></td>
					<td><?php echo h($promoCode['PromoCode']['name']); ?></td>
					<td><?php echo h($promoCode['PromoCode']['code']); ?></td>
					<td><?php echo $this->Presentation->yesNo($promoCode['PromoCode']['is_active']); ?></td>
					<td><?php echo h($promoCode['PromoCode']['description']); ?></td>
					<td>
						<?php
							if ($this->Time->isFuture($promoCode['PromoCode']['valid_from'])) { ?>
								<span class="date future">
							<?php }

							echo h($this->Time->format($promoCode['PromoCode']['valid_from'], '%a %d %b %Y'));

							if ($this->Time->isFuture($promoCode['PromoCode']['valid_from'])) { ?>
								</span>
							<?php }
						?>
					</td>
					<td>
						<?php
							if ($this->Time->isPast($promoCode['PromoCode']['valid_to'])) { ?>
								<span class="date past">
							<?php }

							echo h($this->Time->format($promoCode['PromoCode']['valid_to'], '%a %d %b %Y'));

							if ($this->Time->isFuture($promoCode['PromoCode']['valid_to'])) { ?>
								</span>
							<?php }
						?>
					</td>
					<td class="text-right"><?php echo h($promoCode['PromoCode']['limit_per_user']); ?></td>
					<td class="text-right"><?php echo h($promoCode['PromoCode']['limit_total']); ?></td>
					<td class="text-right"><?php echo h($promoCode['PromoCode']['minimum_spend']); ?></td>
					<td class="text-right"><?php echo h($promoCode['PromoCode']['discount_amount_limit']); ?></td>
					<td><?php echo h($promoCode['PromoCode']['discount_type']); ?></td>
					<td><?php echo h($promoCode['PromoCode']['apply_discount_to']); ?></td>
					<td class="text-right"><?php echo h($promoCode['PromoCode']['discount_amount']); ?></td>
					<td><?php echo h($promoCode['PromoCode']['user_id']); ?></td>
					<td class="text-right"><?php echo h($promoCode['PromoCode']['count_order']); ?></td>
					<td class="actions">
						<div class="btn-group">
							<?php echo $this->Html->link(__('View'), array('action' => 'view', $promoCode['PromoCode']['id']), array('class' => 'btn btn-xs btn-default')); ?>
							<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $promoCode['PromoCode']['id']), array('class' => 'btn btn-xs btn-default')); ?>
							<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $promoCode['PromoCode']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $promoCode['PromoCode']['id'])); ?>
						</div>
					</td>
				</tr>

			<?php } ?>

		</tbody>

	</table>

	<?php

		echo $this->element('navigation/pagination');

		echo $this->element(
			'navigation/admin_actions',
			[
				'data' => $promoCode['PromoCode'],
				'controllerName' => $this->params->controller,
				'itemNameField' => 'name',
				'show' => [
					'add'
				]
			]
		);

	?>

</div>
