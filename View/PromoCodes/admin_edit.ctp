<div class="row">

	<div class="col-sm-16 col-md-12 col-lg-8">

		<div class="promoCodes form">
			<?php echo $this->Form->create(
				'PromoCode',
				[
					'inputDefaults' => [
						'div' => 'form-group',
						'class' => 'form-control'
					],
					'role' => 'form'
				]
			); ?>

				<legend><?php echo __('Edit promo code'); ?></legend>
				<?php
					echo $this->Form->input('id');
					echo $this->Form->input('name');
					echo $this->Form->input('code');
					echo $this->BootstrapForm->horizontalInputs([
						'options' => [
							'divClass' => 'col-sm-'
						],
						'inputs' => [
							'PromoCode.is_active' => [
								'type' => 'checkbox',
								'label' => 'Is active',
								'width-class-label' => 0,
								'width-class-input' => 24,
								'required' => true
							]
						]
					]);
					echo $this->Form->input('description'); ?>

					<label>Valid dates</label>
					<div class="input-daterange input-group" id="valid-dates">
						<?php echo $this->Form->input(
							'valid_from',
							[
								'label' => false,
								'type' => 'text',
								'class' => 'input-sm form-control datepicker'
							]
						); ?>
						<span class="input-group-addon">to</span>
						<?php echo $this->Form->input(
							'valid_to',
							[
								'label' => false,
								'type' => 'text',
								'class' => 'input-sm form-control datepicker'
							]
						); ?>
					</div>

					<?php
						echo $this->Form->inputs([
							'legend' => false,
							'limit_per_user',
							'limit_total',
							'minimum_spend',
							'discount_amount_limit',
							'discount_type' => ['empty' => true],
							'discount_amount',
							'apply_discount_to' => [
								'empty' => true,
								'options' => $applyDiscountToFieldNames
							],
							'user_id' => [
								'empty' => true,
								'after' => '<p class="help-block">Limits the use of this promo code to this user.</p>'
							]
						]);

				echo $this->element(
					'navigation/admin_actions',
					[
						'controllerName' => $this->params->controller,
						'itemNameField' => 'name',
						'show' => [
							'index',
							'save'
						]
					]
				);

			echo $this->Form->end(); ?>

		</div>

	</div>

</div>
