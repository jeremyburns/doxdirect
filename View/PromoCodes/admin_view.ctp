<div class="page-header">
    <h1>Promo code</h1>
    <?php echo $this->Flash->render(); ?>
</div>

    <div class="row">

    <div class="col-sm-16 col-md-12 col-lg-8">

        <div class="promoCodes view">

            <table class="table table-bordered table-condensed">
                <tr>
                    <th><?php echo __('Id'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['id']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['name']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Code'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['code']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Enabled'); ?></th>
                    <td><?php echo $this->Presentation->yesNo($promoCode['PromoCode']['is_active']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Type'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['description']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Valid from'); ?></th>
                    <td><?php echo h($this->Time->format($promoCode['PromoCode']['valid_from'], '%A %d %B %Y')); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Valid to'); ?></th>
                    <td><?php echo h($this->Time->format($promoCode['PromoCode']['valid_to'], '%A %d %B %Y')); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Limit per user'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['limit_per_user']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Limit total'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['limit_total']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Minimum spend'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['minimum_spend']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Discount amount limit'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['discount_amount_limit']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Discount type'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['discount_type']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Apply discount to'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['apply_discount_to']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Discount amount'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['discount_amount']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('User'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['user_id']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Orders'); ?></th>
                    <td><?php echo h($promoCode['PromoCode']['count_order']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td><?php echo h($this->Time->niceShort($promoCode['PromoCode']['created'])); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td><?php echo h($this->Time->niceShort($promoCode['PromoCode']['modified'])); ?></td>
                </tr>
            </table>
        </div>

    </div>

</div>

<?php

echo $this->element(
    'navigation/admin_actions',
    [
        'data' => $promoCode['PromoCode'],
        'controllerName' => $this->params->controller,
        'itemNameField' => 'name',
        'show' => [
            'index',
            'edit',
            'delete',
            'add'
        ]
    ]
);
