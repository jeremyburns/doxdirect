<div class="container">

	<div class="row">

		<div class="col col-sm-20 col-sm-offset-2 col-md-12 col-md-offset-6">

			<div class="users index">

				<h2><?php echo __d('users', 'Reset password request'); ?></h2>

				<?php echo $this->Flash->render();?>

				<p>Please enter your username (your email address) below and we'll send you a link to reset your password.</p>

				<?php echo $this->Flash->render(
					'auth',
					array('element' => ['element' => 'error'])
				);?>

				<fieldset>

					<?php
						echo $this->Form->create(
							'User',
							array(
								'controller' => 'users',
								'action' => 'request_password_reset',
								'class' => 'form-horizontal'
							)
						);

							echo $this->BootstrapForm->horizontalInputs(array(
								'options' => array(
									'divClass' => 'col-sm-'
								),
								'inputs' => array(
									'username' => array(
										'label' => __d('users', 'Username'),
										'type' => 'text',
										'placeHolder' => 'Username (your email)',
										'default' => null,
										'required' => true,
										'width-class-label' => 4,
										'width-class-input' => 12
									)
								)
							)); ?>

							<div class="col-sm-16 col-sm-offset-8">

								<?php
									echo $this->Form->button(
										'Reset password',
										array(
											'type' => 'submit',
											'class' => 'btn btn-success mts'
										)
									);
								?>

							</div>

						<?php echo $this->Form->end();

					?>

				</fieldset>

			</div>

		</div>

	</div>

</div>
