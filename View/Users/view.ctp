<div class="container">

	<div class="row">

		<div class="col-sm-24">

			<?php echo $this->Flash->render();?>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-8">

			<h2><?php echo __('Your profile'); ?></h2>

			<table class="table">

				<tr>
					<th><?php echo __('Username'); ?></th>
					<td><?php echo h($user['User']['username']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('First Name'); ?></th>
					<td><?php echo h($user['User']['first_name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Last Name'); ?></th>
					<td><?php echo h($user['User']['last_name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Phone'); ?></th>
					<td><?php echo h($user['User']['phone']); ?></td>
				</tr>
			</table>

			<div class="btn-group">

				<?php
					echo $this->Html->link(
						__('Edit'),
						array('action' => 'edit'),
						array('class' => 'btn btn-default')
					);
					echo $this->Html->link(
						__('Change password'),
						array('action' => 'change_password'),
						array('class' => 'btn btn-default')
					);
				?>

			</div>

		</div>

		<div class="col-sm-16">

			<?php echo $this->element(
				'users/order_history',
				array(
					'orders' => $user['Order']
				)
			); ?>

			<?php echo $this->element(
				'user_addresses/list',
				array(
					'userAddresses' => $user['UserAddress']
				)
			); ?>

		</div>

	</div>

</div>
