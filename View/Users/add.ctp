<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('salutation');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('phone');
		echo $this->Form->input('house_number');
		echo $this->Form->input('address_1');
		echo $this->Form->input('address_2');
		echo $this->Form->input('city');
		echo $this->Form->input('county');
		echo $this->Form->input('post_code');
		echo $this->Form->input('country');
		echo $this->Form->input('count_order');
	?>
	</fieldset>
	<?php

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">

		<?php
			echo $this->Html->link(__('List Users'), array('action' => 'index'));
			echo $this->Html->link(__('List Orders'), array('controller' => 'orders', 'action' => 'index'));
			echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add'));
		?>
	</div>
</div>
