<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>

		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>

		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>

		</dd>
		<dt><?php echo __('Salutation'); ?></dt>
		<dd>
			<?php echo h($user['User']['salutation']); ?>

		</dd>
		<dt><?php echo __('First Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['first_name']); ?>

		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['last_name']); ?>

		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($user['User']['phone']); ?>

		</dd>
		<dt><?php echo __('House Number'); ?></dt>
		<dd>
			<?php echo h($user['User']['house_number']); ?>

		</dd>
		<dt><?php echo __('Address 1'); ?></dt>
		<dd>
			<?php echo h($user['User']['address_1']); ?>

		</dd>
		<dt><?php echo __('Address 2'); ?></dt>
		<dd>
			<?php echo h($user['User']['address_2']); ?>

		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($user['User']['city']); ?>

		</dd>
		<dt><?php echo __('County'); ?></dt>
		<dd>
			<?php echo h($user['User']['county']); ?>

		</dd>
		<dt><?php echo __('Post Code'); ?></dt>
		<dd>
			<?php echo h($user['User']['post_code']); ?>

		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo h($user['User']['country']); ?>

		</dd>
		<dt><?php echo __('Count Order'); ?></dt>
		<dd>
			<?php echo h($user['User']['count_order']); ?>

		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>

		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>

		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id']));
			echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id']));
			echo $this->Html->link(__('List Users'), array('action' => 'index'));
			echo $this->Html->link(__('New User'), array('action' => 'add'));
			echo $this->Html->link(__('List Orders'), array('controller' => 'orders', 'action' => 'index'));
			echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add'));
		?>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Orders'); ?></h3>
	<?php if (!empty($user['Order'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User'); ?></th>
		<th><?php echo __('Promo Code'); ?></th>
		<th><?php echo __('Count Order Item'); ?></th>
		<th><?php echo __('Amount Net'); ?></th>
		<th><?php echo __('Amount Delivery'); ?></th>
		<th><?php echo __('Amount Vat'); ?></th>
		<th><?php echo __('Amount Total'); ?></th>
		<th><?php echo __('Order Status'); ?></th>
		<th><?php echo __('Delivery Option'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['Order'] as $order): ?>
		<tr>
			<td><?php echo $order['id']; ?></td>
			<td><?php echo $order['user_id']; ?></td>
			<td><?php echo $order['promo_code_id']; ?></td>
			<td><?php echo $order['count_order_item']; ?></td>
			<td><?php echo $order['amount_net']; ?></td>
			<td><?php echo $order['amount_delivery']; ?></td>
			<td><?php echo $order['amount_vat']; ?></td>
			<td><?php echo $order['amount_total']; ?></td>
			<td><?php echo $order['order_status_id']; ?></td>
			<td><?php echo $order['delivery_option_id']; ?></td>
			<td><?php echo $order['created']; ?></td>
			<td><?php echo $order['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'orders', 'action' => 'view', $order['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'orders', 'action' => 'edit', $order['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'orders', 'action' => 'delete', $order['id']), array(), __('Are you sure you want to delete # %s?', $order['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<div class="btn-group">
			<?php echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add')); ?>
		</div>
	</div>
</div>
