<div class="container">

	<div class="row">

		<div class="col col-sm-20 col-sm-offset-2 col-md-12 col-md-offset-6">

			<div class="users index">

				<h2><?php echo __d('users', 'Login'); ?></h2>

				<?php echo $this->element('users/login'); ?>

			</div>

		</div>

	</div>

</div>
