<div class="users index">

	<h2><?php echo __('Users'); ?></h2>

	<div class="table-responsive">

		<table class="table">

			<thead>

				<tr>
					<th><?php echo $this->Paginator->sort('username'); ?></th>
					<th><?php echo $this->Paginator->sort('first_name'); ?></th>
					<th><?php echo $this->Paginator->sort('last_name'); ?></th>
					<th><?php echo $this->Paginator->sort('phone'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order'); ?></th>
					<th><?php echo $this->Paginator->sort('created'); ?></th>
					<th><?php echo $this->Paginator->sort('modified'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>

				</tr>

			</thead>

			<tbody>

				<?php foreach ($users as $user) { ?>
					<tr>
						<td><?php echo h($user['User']['username']); ?></td>
						<td><?php echo h($user['User']['first_name']); ?></td>
						<td><?php echo h($user['User']['last_name']); ?></td>
						<td><?php echo h($user['User']['phone']); ?></td>
						<td><?php echo h($user['User']['count_order']); ?></td>
						<td><?php echo h($user['User']['created']); ?></td>
						<td><?php echo h($user['User']['modified']); ?></td>
						<td class="actions">
							<div class="btn-group">
								<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-xs btn-default')); ?>
								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-xs btn-default')); ?>
								<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
							</div>
						</td>
					</tr>

				<?php } ?>

			</tbody>

		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<div class="btn-group">
			<?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?>
		</div>
	</div>

</div>
