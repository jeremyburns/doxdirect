<div class="index admin-dashboard">

	<h2><?php echo __('<i class="fa fa-gear"></i> Admin dashboard'); ?></h2>

	<div class="row">

		<div class="col-sm-6">

			<h4>Orders</h4>

			<dl class="spaced">

				<dd>

					<?php echo $this->element(
						'navigation/admin_sub/order_search',
						[
							'class' => 'form'
						]
					); ?>

				</dd>

				<dt><?php echo $this->Html->link('List', ['admin' => true, 'controller' => 'orders', 'action' => 'index']); ?></dt>
				<dd>List all orders.</dd>

				<dt><?php echo $this->Html->link('Order statuses', ['admin' => true, 'controller' => 'order_statuses', 'action' => 'index']); ?></dt>
				<dd>Each order passes through a series of statuses.</dd>

				<dt><?php echo $this->Html->link('Job statuses', ['admin' => true, 'controller' => 'job_statuses', 'action' => 'index']); ?></dt>
				<dd>The status of an order with respect to Callas and Pace.</dd>

				<dt><?php echo $this->Html->link('Promo codes', ['admin' => true, 'controller' => 'promo_codes', 'action' => 'index']); ?></dt>
				<dd>View and amend promo codes.</dd>

				<dt><?php echo $this->Html->link('Users', ['admin' => true, 'controller' => 'users', 'action' => 'index']); ?></dt>
				<dd>Users who have placed orders and created user accounts.</dd>

			</dl>

		</div>

		<div class="col-sm-6">

			<h4>Quote options</h4>

			<dl class="spaced">
				<dt><?php echo $this->Html->link('Binding options', ['admin' => true, 'controller' => 'bindings', 'action' => 'index']); ?></dt>
				<dd>The binding options that can be applied to some products.</dd>

				<dt><?php echo $this->Html->link('Binding edges', ['admin' => true, 'controller' => 'binding_edge_types', 'action' => 'index']); ?></dt>
				<dd>Options for placing bindings on specified edges (e.g. long or short)</dd>

				<dt><?php echo $this->Html->link('Binding sides', ['admin' => true, 'controller' => 'binding_side_types', 'action' => 'index']); ?></dt>
				<dd>Options for placing bindings on specified sides (e.g. left or top)</dd>

				<dt><?php echo $this->Html->link('Binding types', ['admin' => true, 'controller' => 'binding_types', 'action' => 'index']); ?></dt>
				<dd>The types of bindings avaiable which, when combined with media, create a product.</dd>

				<dt><?php echo $this->Html->link('Clicks', ['admin' => true, 'controller' => 'clicks', 'action' => 'index']); ?></dt>
				<dd>Options for clicks (generally colour and black & white)</dd>

				<dt><?php echo $this->Html->link('Cover types', ['admin' => true, 'controller' => 'cover_types', 'action' => 'index']); ?></dt>
				<dd>The cover options available for certain products</dd>

				<dt><?php echo $this->Html->link('Delivery options', ['admin' => true, 'controller' => 'delivery_options', 'action' => 'index']); ?></dt>
				<dd>Delivery options</dd>

				<dt><?php echo $this->Html->link('Drilling types', ['admin' => true, 'controller' => 'drilling_types', 'action' => 'index']); ?></dt>
				<dd>Drilling options for ring binders etc.</dd>

				<dt><?php echo $this->Html->link('Media', ['admin' => true, 'controller' => 'media', 'action' => 'index']); ?></dt>
				<dd>The media options available which, when combined with a binding type, create a product.</dd>

				<dt><?php echo $this->Html->link('Paper sizes', ['admin' => true, 'controller' => 'paper_sizes', 'action' => 'index']); ?></dt>
				<dd>Data to recognise paper sizes. Used when examining uploaded files.</dd>

				<dt><?php echo $this->Html->link('Products', ['admin' => true, 'controller' => 'products', 'action' => 'index']); ?></dt>
				<dd>A product is a combination of a binding type and a media option.</dd>

				<dt><?php echo $this->Html->link('Sides', ['admin' => true, 'controller' => 'sides', 'action' => 'index']); ?></dt>
				<dd>Options to determine duplex printing.</dd>

				<dt><?php echo $this->Html->link('Tab types', ['admin' => true, 'controller' => 'tab_types', 'action' => 'index']); ?></dt>
				<dd>The types of tabs offered on specified products.</dd>

				<dt><?php echo $this->Html->link('Turnaround options', ['admin' => true, 'controller' => 'turnaround_options', 'action' => 'index']); ?></dt>
				<dd>Turnaround options allow the customer to specify priority processing.</dd>

			</dl>

		</div>

		<div class="col-sm-6">

			<h4>Configuration</h4>

			<dl class="spaced">
				<dt><?php echo $this->Html->link('Configuration', ['admin' => true, 'controller' => 'configurations', 'action' => 'index']); ?></dt>
				<dd>View and manage configuration options.</dd>

				<dt><?php echo $this->Html->link('Countries', ['admin' => true, 'controller' => 'countries', 'action' => 'index']); ?></dt>
				<dd>View and manage the list of countries.</dd>

				<dt><?php echo $this->Html->link('Holidays', ['admin' => true, 'controller' => 'holidays', 'action' => 'index']); ?></dt>
				<dd>Static (repeating) and variable holidays that change turnaround and delivery availability.</dd>

				<dt><?php echo $this->Html->link('Holiday alerts', ['admin' => true, 'controller' => 'holiday_alerts', 'action' => 'index']); ?></dt>
				<dd>View and manage hliday alert notices that appear in quote results around bank holidays.</dd>

				<dt><?php echo $this->Html->link('Order sources', ['admin' => true, 'controller' => 'order_sources', 'action' => 'index']); ?></dt>
				<dd>View and manage order sources (how people heard of us and what their order is for).</dd>

				<dt><?php echo $this->Html->link('Callas processes', ['admin' => true, 'controller' => 'processes', 'action' => 'index']); ?></dt>
				<dd>Callas profiles that documents are passed through after order payment and before sending to Pace.</dd>

				<dt><?php echo $this->Html->link('Page images', ['admin' => true, 'controller' => 'order_item_file_pages', 'action' => 'page_images']); ?></dt>
				<dd>View the thumbnail images that describe document options to customers during order configuration.</dd>

			</dl>

		</div>

		<div class="col-sm-6">

			<h4>Pace</h4>

			<dl class="spaced">
				<dt><?php echo $this->Html->link('Empty cache', ['admin' => true, 'controller' => 'products', 'action' => 'empty_cache']); ?></dt>
				<dd>Empties the cache, clearing down quote options. They will auto-refill as the system is used.</dd>

				<dt><?php echo $this->Html->link('Delivery options - list', ['admin' => true, 'controller' => 'delivery_options', 'action' => 'index']); ?></dt>
				<dd>List the delivery options, which are maintained in Pace.</dd>

				<dt><?php echo $this->Html->link('Delivery options - refresh', ['admin' => true, 'controller' => 'delivery_options', 'action' => 'refresh']); ?></dt>
				<dd>Refrsh the delivery options from Pace.</dd>

				<dt><?php echo $this->Html->link('Countries - list', ['admin' => true, 'controller' => 'countries', 'action' => 'index']); ?></dt>
				<dd>List the countries, which are maintained in Pace.</dd>

				<dt><?php echo $this->Html->link('Countries - refresh', ['admin' => true, 'controller' => 'countries', 'action' => 'refresh']); ?></dt>
				<dd>Refresh the countries from Pace.</dd>

				<dt><?php echo $this->Html->link('Product catalogue', ['admin' => true, 'controller' => 'products', 'action' => 'product_catalogue']); ?></dt>
				<dd>View and refresh the product catalogue imported from Pace.</dd>

				<dt><?php echo $this->Html->link('Product map - view', ['admin' => true, 'controller' => 'products', 'action' => 'product_map']); ?></dt>
				<dd>The product map is a pre-comiled view of products and their options, created by parsing the product catalogue.</dd>

				<dt><?php echo $this->Html->link('Product map - refresh', ['admin' => true, 'controller' => 'products', 'action' => 'refresh_product_map']); ?></dt>
				<dd>Rebuild the product map from the locally cached version of the product catalogue.</dd>
			</dl>

		</div>

	</div>

</div>
