<div class="container">

	<div class="row">

		<div class="col col-sm-20 col-sm-offset-2 col-md-12 col-md-offset-6">

			<div class="users index">

				<h2><?php echo __d('users', 'Reset password'); ?></h2>

				<?php echo $this->Flash->render();?>

				<p>Please enter your username (your email address) and choose your new password.</p>

				<?php echo $this->Flash->render(
					'auth',
					array('element' => ['element' => 'error'])
				);?>

				<fieldset>

					<?php
						echo $this->Form->create(
							'User',
							[
								'url' => [
									'admin' => false,
									'controller' => 'users',
									'action' => 'reset_password'
								],
								'class' => 'form-horizontal'
							]
						);

							echo $this->BootstrapForm->horizontalInputs(array(
								'options' => array(
									'divClass' => 'col-sm-'
								),
								'inputs' => array(
									'password_reset_code' => array(
										'type' => 'hidden',
										'value' => $passwordResetCode
									),
									'username' => array(
										'label' => __d('users', 'Username'),
										'type' => 'text',
										'placeHolder' => 'Username (your email)',
										'default' => null,
										'required' => true,
										'width-class-label' => 8,
										'width-class-input' => 12
									),
									'password_new' => array(
										'label' => __d('users', 'New password'),
										'type' => 'password',
										'placeHolder' => 'Your new password',
										'default' => null,
										'required' => true,
										'width-class-label' => 8,
										'width-class-input' => 12
									),
									'password_new_confirm' => array(
										'label' => __d('users', 'Confirm password'),
										'type' => 'password',
										'placeHolder' => 'Confirm your new password',
										'default' => null,
										'required' => true,
										'width-class-label' => 8,
										'width-class-input' => 12
									)
								)
							)); ?>

							<div class="col-sm-16 col-sm-offset-8">

								<?php
									echo $this->Form->button(
										'Reset password',
										array(
											'type' => 'submit',
											'class' => 'btn btn-success mts'
										)
									);

								?>

							</div>

						<?php echo $this->Form->end();

					?>

				</fieldset>

			</div>

		</div>

	</div>

</div>
