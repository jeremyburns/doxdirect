<div class="container">

	<div class="row">

		<div class="col col-sm-20 col-sm-offset-2 col-md-12 col-md-offset-6">

			<div class="users index">

				<h2><?php echo __d('users', 'Change your details'); ?></h2>

				<?php echo $this->Flash->render();?>

				<?php
					echo $this->element(
						'users/edit',
						[
							'user' => $user
						]
					);
				?>

			</div>

		</div>

	</div>

</div>
