<div class="page-header">
	<h1>Order status</h1>
	<?php echo $this->Flash->render(); ?>
</div>

<div class="orderStatuses view">

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($orderStatus['OrderStatus']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($orderStatus['OrderStatus']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Sequence'); ?></th>
			<td><?php echo h($orderStatus['OrderStatus']['sequence']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Default'); ?></th>
			<td><?php echo $this->Presentation->yesNo($orderStatus['OrderStatus']['is_default']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Show cart'); ?></th>
			<td><?php echo $this->Presentation->yesNo($orderStatus['OrderStatus']['show_cart']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Show tab'); ?></th>
			<td><?php echo $this->Presentation->yesNo($orderStatus['OrderStatus']['show_tab']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Icon'); ?></th>
			<td><i class="fa <?php echo h($orderStatus['OrderStatus']['icon']); ?>"></i></td>
		</tr>
		<tr>
			<th><?php echo __('Description'); ?></th>
			<td><?php echo h($orderStatus['OrderStatus']['description']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Link'); ?></th>
			<td><?php echo h($orderStatus['OrderStatus']['link']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Order count'); ?></th>
			<td><?php echo h($orderStatus['OrderStatus']['count_order']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Order date count'); ?></th>
			<td><?php echo h($orderStatus['OrderStatus']['count_order_date']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($orderStatus['OrderStatus']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($orderStatus['OrderStatus']['modified'])); ?></td>
		</tr>
	</table>

</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('Edit order status'), array('action' => 'edit', $orderStatus['OrderStatus']['id']));
			echo $this->Form->postLink(__('Delete order status'), array('action' => 'delete', $orderStatus['OrderStatus']['id']), array(), __('Are you sure you want to delete # %s?', $orderStatus['OrderStatus']['id']));
			echo $this->Html->link(__('List order statuses'), array('action' => 'index'));
			echo $this->Html->link(__('New order status'), array('action' => 'add'));
		?>
	</div>
</div>
