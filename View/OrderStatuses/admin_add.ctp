<div class="page-header">
	<h1>Add an order status</h1>
	<?php echo $this->Flash->render(); ?>
</div>

<div class="orderStatuses form">

	<?php
		echo $this->Form->create(
			'OrderStatus',
			array(
				'inputDefaults' => array(
					'div' => 'form-group',
					'class' => 'form-control'
				),
				'role' => 'form'
			)
		);

			echo $this->Form->inputs([
				'legend' => false,
				'name',
				'sequence',
				'is_default',
				'show_cart',
				'show_tab',
				'icon',
				'description',
				'link'
			]);

			echo $this->Form->button(
				__('Save'),
				array(
					'class' => 'btn btn-default btn-success'
				)
			);

		echo $this->Form->end();

	?>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php echo $this->Html->link(__('List order statuses'), array('action' => 'index')); ?>
	</div>
</div>
