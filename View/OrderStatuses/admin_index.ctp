<div class="orderStatuses index">

	<div class="page-header">
		<h1>Order statuses</h1>
		<?php echo $this->Flash->render(); ?>
	</div>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Icon</th>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('sequence'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('show_cart'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('show_tab'); ?></th>
					<th><?php echo $this->Paginator->sort('description'); ?></th>
					<th><?php echo $this->Paginator->sort('link'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_order', 'Orders'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_order_date', 'Order dates'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($orderStatuses as $orderStatus): ?>
					<tr>
						<td><i class="fa <?php echo h($orderStatus['OrderStatus']['icon']); ?>"></i></td>
						<td>
							<?php
								echo $this->Html->link(
									h($orderStatus['OrderStatus']['id']),
									array(
										'action' => 'view',
										$orderStatus['OrderStatus']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($orderStatus['OrderStatus']['name']),
									array(
										'action' => 'view',
										$orderStatus['OrderStatus']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($orderStatus['OrderStatus']['sequence']); ?></td>
						<td class="text-center"><?php echo $this->Presentation->yesNo($orderStatus['OrderStatus']['is_default']); ?></td>
						<td class="text-center"><?php echo $this->Presentation->yesNo($orderStatus['OrderStatus']['show_cart']); ?></td>
						<td class="text-center"><?php echo $this->Presentation->yesNo($orderStatus['OrderStatus']['show_tab']); ?></td>
						<td><?php echo h($orderStatus['OrderStatus']['description']); ?></td>
						<td><?php echo h($orderStatus['OrderStatus']['link']); ?></td>
						<td class="text-right"><?php echo h($orderStatus['OrderStatus']['count_order']); ?></td>
						<td class="text-right"><?php echo h($orderStatus['OrderStatus']['count_order_date']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $orderStatus['OrderStatus']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $orderStatus['OrderStatus']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $orderStatus['OrderStatus']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $orderStatus['OrderStatus']['id']));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<div class="btn-group">
			<?php echo $this->Html->link(__('New order status'), array('action' => 'add')); ?>
		</div>
	</div>

</div>
