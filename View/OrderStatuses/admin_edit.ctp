<div class="page-header">
	<h1>Edit an order status</h1>
	<?php echo $this->Flash->render(); ?>
</div>

<div class="orderStatuses form">
	<?php echo $this->Form->create(
		'OrderStatus',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Edit order status'); ?></legend>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('name');
			echo $this->Form->input('sequence');
			echo $this->Form->input('is_default');
			echo $this->Form->input('show_cart');
			echo $this->Form->input('show_tab');
			echo $this->Form->input('icon');
			echo $this->Form->input('description');
			echo $this->Form->input('link');
		?>
	</fieldset>
	<?php

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Form->postLink(
				__('Delete'),
				array(
					'action' => 'delete',
					$this->Form->value('OrderStatus.id')
				),
				array(),
				__('Are you sure you want to delete # %s?', $this->Form->value('OrderStatus.id'))
			);
			echo $this->Html->link(__('List order statuses'), array('action' => 'index'));
		?>
	</div>
</div>
