<div class="coverTypes view">

	<h2><?php echo __('Cover type'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($coverType['CoverType']['id']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($coverType['CoverType']['name']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Pace name'); ?></th>
			<td><?php echo h($coverType['CoverType']['pace_name']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Size'); ?></th>
			<td><?php echo h($coverType['CoverType']['size_name']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Front/Back'); ?></th>
			<td><?php echo h($coverType['CoverType']['front_back']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Inner/Outer'); ?></th>
			<td><?php echo h($coverType['CoverType']['inner_outer']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($coverType['CoverType']['is_active']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Order item count I/F'); ?></th>
			<td><?php echo h($coverType['CoverType']['count_order_item_inner_front']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Order item count I/B'); ?></th>
			<td><?php echo h($coverType['CoverType']['count_order_item_inner_back']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Order item count O/F'); ?></th>
			<td><?php echo h($coverType['CoverType']['count_order_item_outer_front']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Order item count O/B'); ?></th>
			<td><?php echo h($coverType['CoverType']['count_order_item_outer_back']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($coverType['CoverType']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($coverType['CoverType']['modified'])); ?></td>
		</tr>
	</table>

</div>

<?php echo $this->element(
	'navigation/admin_actions',
	[
		'controllerName' => $this->params->controller,
		'show' => [
			'index'
		]
	]
);
