<div class="coverTypes index">

	<h2><?php echo __('Cover types'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">

			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th><?php echo $this->Paginator->sort('size_name'); ?></th>
					<th><?php echo $this->Paginator->sort('front_back', 'F/B'); ?></th>
					<th><?php echo $this->Paginator->sort('inner_outer', 'I/O'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_order_item_inner_front'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_order_item_inner_back'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_order_item_outer_front'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_order_item_outer_back'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>

			<tbody>

				<?php foreach ($coverTypes as $coverType): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($coverType['CoverType']['id']),
									array(
										'action' => 'view',
										$coverType['CoverType']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($coverType['CoverType']['name']),
									array(
										'action' => 'view',
										$coverType['CoverType']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($coverType['CoverType']['pace_name']); ?></td>
						<td><?php echo h($coverType['CoverType']['size_name']); ?></td>
						<td><?php echo h($coverType['CoverType']['front_back']); ?></td>
						<td><?php echo h($coverType['CoverType']['inner_outer']); ?></td>
						<td><?php echo $this->Presentation->yesNo($coverType['CoverType']['is_active']); ?></td>
						<td class="text-right"><?php echo h($coverType['CoverType']['count_order_item_inner_front']); ?></td>
						<td class="text-right"><?php echo h($coverType['CoverType']['count_order_item_inner_back']); ?></td>
						<td class="text-right"><?php echo h($coverType['CoverType']['count_order_item_outer_front']); ?></td>
						<td class="text-right"><?php echo h($coverType['CoverType']['count_order_item_outer_back']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $coverType['CoverType']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>

				<?php endforeach; ?>

			</tbody>

		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>