<div class="tabTypes view">

	<h2><?php echo __('Tab type'); ?></h2>

	<div class="row">

		<div class="col-sm-12">

			<table class="table table-condensed">

				<caption>Details</caption>

				<tr>
					<th><?php echo __('Id'); ?></th>
					<td><?php echo h($tabType['TabType']['id']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Name'); ?></th>
					<td><?php echo h($tabType['TabType']['name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Pace name'); ?></th>
					<td><?php echo h($tabType['TabType']['pace_name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Thickness'); ?></th>
					<td><?php echo h($tabType['TabType']['thickness']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Weight'); ?></th>
					<td><?php echo h($tabType['TabType']['weight']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Default'); ?></th>
					<td><?php echo $this->Presentation->yesNo($tabType['TabType']['is_default']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Active'); ?></th>
					<td><?php echo $this->Presentation->yesNo($tabType['TabType']['is_active']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Order items'); ?></th>
					<td><?php echo h($tabType['TabType']['count_order_item']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Created'); ?></th>
					<td><?php echo h($this->Time->niceShort($tabType['TabType']['created'])); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Modified'); ?></th>
					<td><?php echo h($this->Time->niceShort($tabType['TabType']['modified'])); ?></td>
				</tr>
			</table>

		</div>

		<div class="col-sm-12">

			<?php
				echo $this->element(
					'product/admin/list',
					['products' => $tabType['ProductTabType']]
				);
			?>
		</div>

	</div>

</div>

<div class="actions">
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('List tab types'), array('action' => 'index'));
		?>
	</div>
</div>
