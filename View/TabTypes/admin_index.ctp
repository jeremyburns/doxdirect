<div class="tabTypes index">

	<h2><?php echo __('Tab types'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">

			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th><?php echo $this->Paginator->sort('thickness'); ?></th>
					<th><?php echo $this->Paginator->sort('weight'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>

			<tbody>

				<?php foreach ($tabTypes as $tabType): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($tabType['TabType']['id']),
									array(
										'action' => 'view',
										$tabType['TabType']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($tabType['TabType']['name']),
									array(
										'action' => 'view',
										$tabType['TabType']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($tabType['TabType']['pace_name']); ?></td>
						<td><?php echo h($tabType['TabType']['thickness']); ?></td>
						<td><?php echo h($tabType['TabType']['weight']); ?></td>
						<td><?php echo $this->Presentation->yesNo($tabType['TabType']['is_default']); ?></td>
						<td><?php echo $this->Presentation->yesNo($tabType['TabType']['is_active']); ?></td>
						<td><?php echo h($tabType['TabType']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $tabType['TabType']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>

				<?php endforeach; ?>

			</tbody>

		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>