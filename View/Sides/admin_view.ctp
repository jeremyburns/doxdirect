<div class="sides view">

	<h2><?php echo __('Side'); ?></h2>

	<div class="row">

		<div class="col-sm-12">

			<table class="table table-condensed">

				<caption>Details</caption>
				<tr>
					<th><?php echo __('Id'); ?></th>
					<td><?php echo h($side['Side']['id']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Name'); ?></th>
					<td><?php echo h($side['Side']['name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Pace name'); ?></th>
					<td><?php echo h($side['Side']['pace_name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Sides'); ?></th>
					<td><?php echo h($side['Side']['sides']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Default'); ?></th>
					<td><?php echo $this->Presentation->yesNo($side['Side']['is_default']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Active'); ?></th>
					<td><?php echo $this->Presentation->yesNo($side['Side']['is_active']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Order items'); ?></th>
					<td><?php echo h($side['Side']['count_order_item']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Created'); ?></th>
					<td><?php echo h($this->Time->niceShort($side['Side']['created'])); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Modified'); ?></th>
					<td><?php echo h($this->Time->niceShort($side['Side']['modified'])); ?></td>
				</tr>
			</table>

		</div>

		<div class="col-sm-12">

			<?php echo $this->element(
				'product/admin/list',
				['products' => $side['ProductSide']]
			); ?>

		</div>

	</div>

</div>

<div class="actions">
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('List sides'), array('action' => 'index'));
		?>
	</div>
</div>
