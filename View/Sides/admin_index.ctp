<div class="sides index">

	<h2><?php echo __('Sides'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">

			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('sides'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($sides as $side): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($side['Side']['id']),
									array(
										'action' => 'view',
										$side['Side']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($side['Side']['name']),
									array(
										'action' => 'view',
										$side['Side']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($side['Side']['pace_name']); ?></td>
						<td class="text-right"><?php echo h($side['Side']['sides']); ?></td>
						<td><?php echo $this->Presentation->yesNo($side['Side']['is_default']); ?></td>
						<td><?php echo $this->Presentation->yesNo($side['Side']['is_active']); ?></td>
						<td><?php echo h($side['Side']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $side['Side']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>

			</tbody>

		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>