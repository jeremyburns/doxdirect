<div class="bindingEdgeTypes view">

	<h2><?php echo __('Binding edge type'); ?></h2>

	<div class="row">

		<div class="col-sm-12">

			<table class="table table-condensed">

				<caption>Details</caption>

				<tr>
					<th><?php echo __('Id'); ?></th>
					<td><?php echo h($bindingEdgeType['BindingEdgeType']['id']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Name'); ?></th>
					<td><?php echo h($bindingEdgeType['BindingEdgeType']['name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Pace name'); ?></th>
					<td><?php echo h($bindingEdgeType['BindingEdgeType']['pace_name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Default'); ?></th>
					<td><?php echo $this->Presentation->yesNo($bindingEdgeType['BindingEdgeType']['is_default']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Active'); ?></th>
					<td><?php echo $this->Presentation->yesNo($bindingEdgeType['BindingEdgeType']['is_active']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Order items'); ?></th>
					<td><?php echo h($bindingEdgeType['BindingEdgeType']['count_order_item']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Created'); ?></th>
					<td><?php echo h($this->Time->niceShort($bindingEdgeType['BindingEdgeType']['created'])); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Modified'); ?></th>
					<td><?php echo h($this->Time->niceShort($bindingEdgeType['BindingEdgeType']['modified'])); ?></td>
				</tr>
			</table>

		</div>

		<div class="col-sm-12">

			<?php
				echo $this->element(
					'product/admin/list',
					['products' => $bindingEdgeType['ProductBindingEdgeType']]
				);
			?>

		</div>

	</div>

</div>

<?php

	echo $this->element(
		'navigation/admin_actions',
		[
			'data' => $bindingEdgeType['BindingEdgeType'],
			'controllerName' => $this->params->controller,
			'itemNameField' => 'name',
			'show' => [
				'index'
			]
		]
	);
