<div class="bindingEdgeTypes index">

	<h2><?php echo __('Binding edge types'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($bindingEdgeTypes as $bindingEdgeType): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($bindingEdgeType['BindingEdgeType']['id']),
									array(
										'action' => 'view',
										$bindingEdgeType['BindingEdgeType']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($bindingEdgeType['BindingEdgeType']['name']),
									array(
										'action' => 'view',
										$bindingEdgeType['BindingEdgeType']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($bindingEdgeType['BindingEdgeType']['pace_name']); ?></td>
						<td><?php echo $this->Presentation->yesNo($bindingEdgeType['BindingEdgeType']['is_active']); ?></td>
						<td><?php echo $this->Presentation->yesNo($bindingEdgeType['BindingEdgeType']['is_default']); ?></td>
						<td><?php echo h($bindingEdgeType['BindingEdgeType']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $bindingEdgeType['BindingEdgeType']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>
