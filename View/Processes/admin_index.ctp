<div class="processes index">

	<h2><?php echo __('Processes'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('code'); ?></th>
					<th><?php echo $this->Paginator->sort('type'); ?></th>
					<th><?php echo $this->Paginator->sort('profile_name'); ?></th>
					<th><?php echo $this->Paginator->sort('parameters'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($processes as $process): ?>
					<tr>
						<td><?php echo $this->Html->link($process['Process']['id'], array('action' => 'view', $process['Process']['id'])); ?></td>
						<td><?php echo $this->Html->link($process['Process']['name'], array('action' => 'view', $process['Process']['id'])); ?></td>
						<td><?php echo $this->Html->link($process['Process']['code'], array('action' => 'view', $process['Process']['id'])); ?></td>
						<td><?php echo h($process['Process']['type']); ?></td>
						<td><?php echo h($process['Process']['profile_name']); ?></td>
						<td><?php echo h($process['Process']['parameters']); ?></td>
						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $process['Process']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $process['Process']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $process['Process']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $process['Process']['id']));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>

<?php echo $this->element(
	'navigation/admin_actions',
	[
		'controllerName' => $this->params->controller,
		'itemNameField' => 'name',
		'show' => [
			'add'
		]
	]
);
