<div class="promoCodes form">
	<?php echo $this->Form->create(
		'Process',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>

		<legend><?php echo __('Add process'); ?></legend>
		<?php
			echo $this->Form->inputs([
				'legend' => false,
				'name',
				'code',
				'type' => ['empty' => true],
				'profile_name',
				'parameters',
				'description'
			]);

			echo $this->element(
				'navigation/admin_actions',
				[
					'controllerName' => $this->params->controller,
					'itemNameField' => 'name',
					'show' => [
						'index',
						'save'
					]
				]
			);

		echo $this->Form->end();

	?>

</div>
