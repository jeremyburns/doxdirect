<div class="processs view">
	<h2><?php echo __('Process'); ?></h2>
	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($process['Process']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($process['Process']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Code'); ?></th>
			<td><?php echo h($process['Process']['code']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Type'); ?></th>
			<td><?php echo h($process['Process']['type']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Profile name'); ?></th>
			<td><?php echo h($process['Process']['profile_name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Parameters'); ?></th>
			<td><?php echo h($process['Process']['parameters']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Description'); ?></th>
			<td><?php echo h($process['Process']['description']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($process['Process']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($process['Process']['modified'])); ?></td>
		</tr>
	</table>
</div>

<?php

echo $this->element(
	'navigation/admin_actions',
	[
		'data' => $process['Process'],
		'controllerName' => $this->params->controller,
		'itemNameField' => 'code',
		'show' => [
			'add',
			'delete',
			'edit',
			'index'
		]
	]
);
