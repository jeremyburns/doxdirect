<div class="promoCodes form">
	<?php echo $this->Form->create(
		'Process',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>

		<legend><?php echo __('Edit process'); ?></legend>
		<?php
			echo $this->Form->inputs([
				'legend' => false,
				'id',
				'name',
				'code',
				'type' => ['empty' => true],
				'profile_name',
				'parameters',
				'description'
			]);

			echo $this->element(
				'navigation/admin_actions',
				[
					'data' => $this->request->data['Process'],
					'controllerName' => $this->params->controller,
					'itemNameField' => 'name',
					'show' => [
						'delete',
						'view',
						'index',
						'save'
					]
				]
			);

	echo $this->Form->end();

?>
</div>
