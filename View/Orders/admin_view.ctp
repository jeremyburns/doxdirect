<div class="page-header">
	<h1>Order</h1>
	<?php echo $this->Flash->render(); ?>

</div>


<div class="row">

	<div class="col-sm-10">

		<?php echo $this->element(
			'orders/admin/order',
			array('order' => $order)
		); ?>

	</div>

	<div class="col-sm-8">

		<?php echo $this->element(
			'orders/admin/order_dates',
			array('orderDates' => $order['OrderDate'])
		); ?>

	</div>

	<div class="col-sm-6">

		<?php
			echo $this->element(
				'orders/admin/amounts',
				array('order' => $order)
			);

			echo $this->element(
				'orders/admin/promo_code',
				array('ordere' => $order)
			);

		?>

	</div>

</div>

<div class="row">

	<div class="col-sm-24">

		<?php echo $this->element(
			'orders/admin/order_paypal_payments',
			array('order' => $order)
		); ?>

	</div>

</div>

<?php if (!empty($order['OrderItem'])) { ?>

	<div class="row">

		<div class="col-sm-24">

			<?php echo $this->element(
				'orders/admin/order_items',
				array('orderItems' => $order['OrderItem'])
			); ?>

		</div>

	</div>

<?php } ?>

<div class="row">

	<div class="col-sm-24">

		<?php if ($latestQuote) { ?>
			<a class="btn btn-sm btn-link" role="button" data-toggle="collapse" href="#latest-quote" aria-expanded="false" aria-controls="collapseExample">Show/hide latest quote</a>
			<div class="collapse" id="latest-quote">
				<div class="well">
					<small><pre><?php print_r($latestQuote); ?></pre></small>
				</div>
			</div>
		<?php } else { ?>
			No quote
		<?php } ?>

	</div>

</div>
