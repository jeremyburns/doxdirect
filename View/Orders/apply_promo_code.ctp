<?php echo $this->element('orders/order_steps', array('currentStep' => $currentStep)); ?>

<div class="container">

	<div class="row">

		<div class="col-sm-24">
			<h1><i class="fa fa-qrcode"></i> Apply a promo code</h1>
		</div>

	</div>

	<div class="row">

		<div class="col-sm-16 col-md-push-8">

			<div class="panel panel-default">

				<div class="panel-heading"><i class="fa fa-check-circle"></i> Order confirmation</div>

				<div class="panel-body">

					<?php echo $this->Flash->render(); ?>

					<span id="promo-apply-code-ajax"></span>

					<?php

						echo $this->Form->create(
							'Order',
							[
								'url' => [
									'admin' => false,
									'controller' => 'orders',
									'action' => 'apply_promo_code'
								],
								'role' => 'form',
								'class' => 'form-horizontal'
							]
						);

							echo $this->Form->inputs(array(
								'legend' => false,
								'Order.id'
							));

							echo $this->element(
								'orders/checkout/panels/promo_code_input',
								array(
									'order' => $order
								)
							);

							echo $this->Form->button(
								'Apply <i class="fa fa-chevron-right"></i>',
								array(
									'type' => 'submit',
									'class' => 'btn btn-md btn-action pull-right mlx',
									'escape' => false,
									'data-user' => 'neutral'
								)
							);

							echo $this->Html->link(
								'<i class="fa fa-undo"></i> Cancel',
								[
									'admin' => false,
									'controller' => 'orders',
									'action' => 'resume'
								],
								[
									'escape' => false,
									'class' => 'btn btn-md btn-link',
								]
							);

						echo $this->Form->end();

					?>

				</div>

			</div>

		</div>

		<div class="col-sm-8 col-md-pull-16">

			<div class="panel panel-default">

				<div class="panel-heading">
					<i class="fa fa-calculator"></i> Your order
				</div>

				<div class="panel-body">

					<?php

						echo $this->element(
							'orders/quote/amounts',
							array(
								'order' => $order,
								'orderOptions' => array(
									'allow_edit' => false,
									'show_add_link' => false
								)
							)
						);

						echo $this->Html->link(
							'<i class="fa fa-pencil"></i> Edit your order',
							[
								'admin' => false,
								'controller' => 'orders',
								'action' => 'quote'
							],
							[
								'escape' => false
							]
						);

					?>

				</div>

			</div>

			<div class="panel panel-default">

				<div class="panel-heading">
					<i class="fa fa-truck fa-flip-horizontal"></i> Despatch details
				</div>

				<div class="panel-body">

					<?php
						if (!empty($order['Order'])) {
							echo $this->element(
								'orders/despatch/summary',
								array(
									'despatchDateFormatted' => $order['Order']['despatch_date_formatted'],
									'despatchMethod' => $order['TurnaroundOption']['name'],
									'deliveryMethod' => $order['DeliveryOption']['name'],
									'deliveryDateFormatted' => $order['Order']['delivery_date_formatted'],
									'latestDeliveryTime' => $order['DeliveryOption']['latest_delivery_time'],
									'isGuaranteed' => $order['DeliveryOption']['is_guaranteed']
								)
							);
						}
					?>

				</div>

			</div>

		</div>

	</div>

</div>
