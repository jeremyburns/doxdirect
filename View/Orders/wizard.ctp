<div class="row">

	<div class="small-12 medium-8 columns">

		<div class="row">

			<h5>Upload your document and let Daisie work it out</h5>

			<?php

				echo $this->Form->create(
					'Order',
					array (
						'type' => 'file',
						'id' => 'document-upload'
					)
				);

				echo $this->Form->input(
					'document',
					array(
						'type' => 'file',
						'label' => 'Upload your document'
					)
				);
			
				echo $this->Form->button(
					'Upload',
					array(
						'class' => 'button rounded success'
					)
				);
			
			echo $this->Form->end(); ?>

		</div>

	</div>

	<div class="small-12 medium-4 columns">

		<div class="panel blue price-calculator">
			<h4>How much?</h4>
		</div>

		<?php echo $this->element(
			'quote/calculator',
			array(
				'wizard' => true,
				'order' => !empty($order) ? $order : array()
			)
		); ?>

	</div>

</div>