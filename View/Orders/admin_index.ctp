<div class="orders index">

	<div class="page-header">

		<h1>Orders</h1>

		<?php if (!empty($adminOrderSummary)) {

			$th = $tdPercent = $tdCount = '';

			foreach ($adminOrderSummary as $status => $counts) {
				$th .= sprintf('<th class="text-right">%s</th>', $status);
				$tdCount .= sprintf('<td class="text-right">%s</td>', $counts['count']);
				$tdPercent .= sprintf('<td class="text-right">%s&#37;</td>', $counts['percent']);
			} ?>

			<table class="table table-condensed">
				<thead>
					<tr>
						<?php echo $th; ?>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php echo $tdCount; ?>
					</tr>
					<tr>
						<?php echo $tdPercent; ?>
					</tr>
				</tbody>
			</table>
		<?php } ?>

	</div>

	<div class="table-responsive">

		<table class="table table-condensed">

			<thead>

				<tr>
					<th><?php echo $this->Paginator->sort('pace_quote_id', 'Pace quote ID'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_job_number', 'Pace job No'); ?></th>
					<th><?php echo $this->Paginator->sort('OrderStatus.name', 'Status'); ?></th>
					<th><?php echo $this->Paginator->sort('JobStatus.name', 'Job status'); ?></th>
					<th><?php echo $this->Paginator->sort('User.username', 'User'); ?></th>
					<th><?php echo $this->Paginator->sort('modified'); ?></th>
					<th><?php echo $this->Paginator->sort('despatch_date', 'Despatch'); ?></th>
					<th><?php echo $this->Paginator->sort('delivery_date', 'Delivery'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('amount_net', 'Net'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('amount_total', 'Total'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('amount_due', 'Due'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_order_item', 'Items'); ?></th>
					<th class="actions"></th>
				</tr>

			</thead>

			<tbody>

				<?php foreach ($orders as $order) { ?>

					<tr>

						<td>
							<?php
								echo $this->Html->link(
									$order['Order']['pace_quote_id'],
									array(
										'admin' => true,
										'controller' => 'orders',
										'action' => 'view',
										$order['Order']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$order['Order']['pace_job_number'],
									array(
										'admin' => true,
										'controller' => 'orders',
										'action' => 'view',
										$order['Order']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$order['OrderStatus']['name'],
									array(
										'admin' => true,
										'controller' => 'order_statuses',
										'action' => 'view',
										$order['Order']['order_status_id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$order['JobStatus']['name'],
									array(
										'admin' => true,
										'controller' => 'job_statuses',
										'action' => 'view',
										$order['Order']['job_status_id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									$order['User']['username'],
									array(
										'admin' => true,
										'controller' => 'users',
										'action' => 'view',
										$order['Order']['user_id']
									)
								);
							?>
						</td>
						<td><?php echo $this->Time->niceShort($order['Order']['modified']); ?></td>
						<td><?php echo $this->Presentation->niceDate($order['Order']['despatch_date'], false); ?></td>
						<td><?php echo $this->Presentation->niceDate($order['Order']['delivery_date'], false); ?></td>
						<td class="text-right"><?php echo h($order['Order']['amount_net']); ?></td>
						<td class="text-right"><?php echo h($order['Order']['amount_total']); ?></td>
						<td class="text-right"><?php echo h($order['Order']['amount_due']); ?></td>
						<td class="text-right"><?php echo h($order['Order']['count_order_item']); ?></td>

						<td class="actions">
							<?php
								echo $this->Html->link(
									__('View'),
									array(
										'admin' => true,
										'controller' => 'orders',
										'action' => 'view',
										$order['Order']['id']
									),
									array(
										'class' => 'btn btn-xs btn-default'
									)
								);
							?>
						</td>
					</tr>

				<?php } ?>

			</tbody>

		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>
