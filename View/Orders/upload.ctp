<?php echo $this->element('orders/order_steps', array('currentStep' => $currentStep)); ?>

<div class="container">

	<div id="upload-dialog">

		<div class="row">

			<div class="col-sm-24">

				<h1><i class="fa fa-cloud-upload"></i> Upload your files</h1>

				<?php echo $this->Flash->render(); ?>

			</div>

		</div>

		<div class="row">

			<div class="col-sm-16 col-sm-push-8">

				<?php echo $this->element(
					'orders/order_items/order_item_files/choose_order_item',
					array(
						'orderItems' => $orderItems
						// 'orderItemKey' => $orderItemKey
					)
				); ?>

				<h3>Choose a file you want us to print</h3>

				<p class="text-info"><small>Maximum file size 150MB. PDF, Microsoft Word and PowerPoint only.</small></p>

				<?php
					echo $this->element(
						'orders/order_items/order_item_files/upload_dialogue',
						[
							'maximumFileUploadSize' => $maximumFileUploadSize
						]
					);

					$uploadedFilesUrl = $this->Html->url(
						array(
							'controller' => 'orders',
							'action' => 'order_item_files'
						)
					);

					$orderSummaryUrl = $this->Html->url(
						array(
							'controller' => 'orders',
							'action' => 'order_summary'
						)
					);

				?>

				<input id="uploaded-order-item-files-url" value="<?php echo $uploadedFilesUrl; ?>" type="hidden">

				<input id="order-summary-url" value="<?php echo $orderSummaryUrl; ?>" type="hidden">

				<span id="uploaded-order-item-files" >
					<?php
						echo $this->element(
							'orders/order_items/order_item_files/list',
							array('order' => $order)
						);
					?>
				</span>

				<?php
					echo $this->Html->link(
						'Continue <i class="fa fa-chevron-right"></i>',
						array(
							'controller' => 'orders',
							'action' => 'process_files'
						),
						array(
							'id' => 'link-upload-complete',
							'class' => 'btn btn-lg btn-action pull-right hide',
							'escape' => false
						)
					);

					echo $this->element('structure/hr', array('colClass' => 'xs-24'));
				?>

				<div id="large-file-alert" class="panel panel-info">

					<div class="panel-heading">
						File over 150mb?
					</div>

					<div class="panel-body">

						<p>If so, you can send the file to us at 'service@doxdirect.com' using a third party file transfer service such as:</p>

						<ul class="list-unstyled">
							<li><a href="http://www.mailbigfile.com" target="_blank">www.mailbigfile.com</a></li>
							<li><a href="http://www.wesend.com" target="_blank">www.wesend.com</a></li>
							<li><a href="http://www.wetransfer.com" target="_blank">www.wetransfer.com</a></li>
						</ul>

						<p>Tell us what binding you are choosing and we will email you back a link that will take you straight to the website with your PDF already uploaded ready for you to proceed with your order. The maximum file size we can accept is 750mb.</p>

					</div>

				</div>

			</div>

			<div class="col-sm-8 col-sm-pull-16">

				<div class="panel panel-info mbx">

					<div class="panel-body">

						<?php echo $this->element('global_content/file_type_images'); ?>

						<p class="mtx">You can upload PDF, Microsoft Word or Microsoft PowerPoint files. To get the best results we recommend you upload PDF files where possible. <?php echo $this->Html->link('Here\'s why...', ['admin' => false, 'controller' => 'pages', 'action' => 'display', 'pdf'], array('class' => 'popup')); ?></p>

					</div>

				</div>

				<span id="order-summary">

					<?php
						echo $this->element(
							'orders/summary/print_summary',
							array(
								'order' => $order
							)
						);
					?>

				</span>

				<?php echo $this->element('global_content/our_promise'); ?>

			</div>

		</div>

	</div>

	<?php
		if (!empty($order['Order'])) {

			echo $this->element('structure/hr', array('colClass' => 'xs-24')); ?>

			<div id="quote-details">

				<div class="row">

					<div class="col-sm-24">

						<h3><i class="fa fa-thumbs-o-up"></i> Here's how your order is shaping up</h3>
						<p>This might change once we've examined your uploaded file but don't worry&mdash;you can change all that in the next step.</p>

					</div>

				</div>

				<span id="order-progress">

					<?php
						if (!empty($order['Order'])) {
							echo $this->element(
								'orders/summary/progress',
								array(
									'order' => $order
								)
							);
						}
					?>

				</span>

			</div>

		<?php }

		echo $this->element('structure/hr', array('colClass' => 'xs-24'));

		echo $this->element('faqs/upload');

	?>

</div>
