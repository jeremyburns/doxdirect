<span id="checkout-content">

	<?php echo $this->element('orders/order_steps', array('currentStep' => $currentStep)); ?>

	<div class="container">

		<div class="row">

			<div class="col-sm-24">
				<h1><i class="fa fa-shopping-cart"></i> Checkout <small>Fields marked with an asterisk (*) are mandatory</small></h1>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-16 col-md-push-8">

				<div class="panel panel-default">

					<div class="panel-heading"><i class="fa fa-check-circle"></i> Order confirmation</div>

					<div class="panel-body">

						<?php echo $this->Flash->render(); ?>

						<span id="checkout-alert" style="display: none;">
							<p id="checkout-message" class="alert"></p>
						</span>

						<?php if (empty($loggedInUser)) { ?>

							<p class="alert alert-info-pale" id="returning-user-notice" data-user="new">
								<i class="fa fa-sign-in"></i> Returning user? <?php echo $this->Html->link('Click here to log in', ['admin' => false, 'controller' => 'users', 'action' => 'login'], array('id' => 'checkout-returning-user-login-link')); ?>.
							</p>

						<?php } ?>

						<span id="checkout-ajax"></span>

						<?php echo $this->Form->create(
							'Order',
							[
								'url' => [
									'admin' => false,
									'controller' => 'orders',
									'action' => 'checkout'
								],
								'role' => 'form',
								'class' => 'form-horizontal',
								'data-refresh-link' => $this->Html->url([
									'admin' => false,
									'controller' => 'orders',
									'action' => 'checkout'
								])
							]
						);

							// This input is used to tell the code that checks tunraound options if this is a quote (value = false) or an order (value = 1)
							echo $this->Form->hidden(
								'Order.is_order',
								array(
									'id' => 'IsOrder',
									'value' => 1
								)
							);

							// When checking whether an order can have same day turnaround we need to examine the products in the order items. This field aggregates those into a single variable for ease.

							echo $this->Form->hidden(
								'Order.qualifies_for_same_day_turnaround',
								array(
									'id' => 'QualifiesForSameDayTurnaround',
									'value' => intval($qualifiesForSameDayTurnaround)
								)
							);

							echo $this->Form->inputs(array(
								'legend' => false,
								'Order.id'
//								'Order.promo_code_id' => ['type' => 'text']
							));

							if (!empty($loggedInUser)) { ?>

								<span data-user="returned">

									<?php echo $this->element(
										'orders/checkout/panels/returned_user',
										[
											'loggedInUser' => $loggedInUser,
											'userAddresses' => $userAddresses
										]
									); ?>

								</span>

							<?php } else { ?>

								<span data-user="new">

									<?php
										echo $this->element(
											'orders/checkout/panels/about_you',
											array(
												'order' => $order
											)
										);

										echo $this->element(
											'orders/checkout/panels/shipping_address',
											array(
												'order' => $order
											)
										);

										echo $this->element(
											'orders/checkout/panels/billing_address',
											array(
												'order' => $order
											)
										);
									?>

								</span>

							<?php } ?>

							<span id="user-details" data-user="returned"></span>

							<span id="checkout-options" data-user="neutral">

								<?php
									echo $this->element(
										'orders/checkout/panels/options',
										array(
											'order' => $order,
											'turnaroundOptions' => $turnaroundOptions,
											'deliveryOptions' => $deliveryOptions
										)
									);

								?>

							</span>

							<span id="checkout-promo-code" data-user="neutral">

								<?php

									echo $this->element(
										'orders/checkout/panels/promo_code',
										array(
											'order' => $order
										)
									);

								?>

							</span>

							<span id="checkout-small-print">

								<?php

									echo $this->element(
										'orders/checkout/panels/small_print',
										array(
											'order' => $order
										)
									);

								?>

							</span>

							<span id="checkout-button">

								<?php

									echo $this->Form->button(
										'Save & continue <i class="fa fa-chevron-right"></i>',
										array(
											'type' => 'submit',
											'class' => 'btn btn-lg btn-action pull-right mlx',
											'escape' => false,
											'data-user' => 'neutral'
										)
									); ?>

							</span>

						<?php echo $this->Form->end(); ?>

					</div>

				</div>

			</div>

			<div class="col-sm-8 col-md-pull-16">

				<div class="panel panel-default">

					<div class="panel-heading">
						<i class="fa fa-calculator"></i> Your order
					</div>

					<div id="your-order-summary" class="panel-body">

						<?php
							echo $this->element(
								'orders/quote/amounts',
								array(
									'order' => $order,
									'orderOptions' => array(
										'allow_edit' => false,
										'show_add_link' => false
									)
								)
							);

							echo $this->Html->link(
								'<i class="fa fa-pencil"></i> Edit your order',
								[
									'admin' => false,
									'controller' => 'orders',
									'action' => 'quote'
								],
								[
									'escape' => false
								]
							);

						?>

					</div>

				</div>

				<div class="panel panel-default">

					<div class="panel-heading">
						<i class="fa fa-truck fa-flip-horizontal"></i> Despatch details
					</div>

					<div class="panel-body">

						<?php
							if (!empty($order['Order'])) {
								echo $this->element(
									'orders/despatch/summary',
									array(
										'despatchDateFormatted' => $order['Order']['despatch_date_formatted'],
										'despatchMethod' => $order['TurnaroundOption']['name'],
										'deliveryMethod' => $order['DeliveryOption']['name'],
										'deliveryDateFormatted' => $order['Order']['delivery_date_formatted'],
										'latestDeliveryTime' => $order['DeliveryOption']['latest_delivery_time'],
										'isGuaranteed' => $order['DeliveryOption']['is_guaranteed']
									)
								);
							}
						?>

					</div>

				</div>

			</div>

		</div>

	</div>

</span>

<?php
	echo $this->Form->input(
		'returned-user-url',
		[
			'value' => $this->Html->url([
				'controller' => 'users',
				'action' => 'returned_user'
			]),
			'type' => 'hidden'
		]
	);

	echo $this->Form->input(
		'change-my-details-url',
		[
			'value' => $this->Html->url([
				'controller' => 'users',
				'action' => 'edit'
			]),
			'type' => 'hidden'
		]
	);
