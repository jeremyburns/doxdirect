<div class="panel-group" id="calc_accordion" role="tablist" aria-multiselectable="false">

	<div class="panel panel-default">

		<div class="panel-heading" role="tab">

			<h4 class="panel-title"><a data-toggle="collapse" data-parent="#calc_accordion" href="#calc" aria-expanded="true" aria-controls="calc">Your Document</a></h4>

		</div>

		<div id="calc" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="calc">

			<div class="panel-body">

				<?php echo $this->element(
					'orders/quote/calculator',
					array(
						'data' => !empty($this->request->data) ? $this->request->data : array(),
						'order' => !empty($order) ? $order : array(),
						'orderItemKey' => $orderItemKey,
						'renderForWordpress' => true
					)
				); ?>

			</div>

		</div>

	</div>

	<div id="quote_container" class="panel panel-default<?php if(!$order['Order']['pace_quote_id']): ?> hidden<?php endif; ?>">

		<div class="panel-heading" role="tab">

			<h4 class="panel-title"><a data-toggle="collapse" data-parent="#calc_accordion" href="#quote" aria-expanded="true" aria-controls="quote">Your Quote</a></h4>

		</div>

		<div id="quote" class="panel-collapse collapse" role="tabpanel" aria-labelledby="quote">

			<div class="panel-body">

				<span id="quote-result">

					<?php
						echo $this->element(
							'orders/quote/quote_result',
							array(
								'order' => !empty($order) ? $order : array(),
								'renderForWordpress' => true
							)
						);
					?>

				</span>

			</div>

		</div>

	</div>

</div>
