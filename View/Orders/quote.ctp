<div class="container mts">

    <div class="row">

        <div class="col-xs-24">

            <?php
                echo $this->Flash->render();

                echo $this->Flash->render('auth');

            ?>

        </div>

    </div>

    <div class="row">


        <div class="col-sm-12 col-md-8 col-md-push-8">

            <?php

                echo $this->element(
                     'orders/quote/calculator',
                    [
                        'data' => !empty($this->request->data) ? $this->request->data : [],
                        'order' => !empty($order) ? $order : [],
                        'orderItemKey' => $orderItemKey,
                        'renderForWordpress' => false
                    ]
                );

            ?>

        </div>

        <div class="col-sm-12 col-md-8 col-md-push-8">

            <span id="quote-result">

                <?php
                    echo $this->element(
                        'orders/quote/quote_result',
                        [
                            'order' => !empty($order) ? $order : []
                        ]
                    );
                ?>

            </span>

        </div>

        <div class="col-sm-24 col-md-8 col-md-pull-16">

            <?php echo $this->element('global_content/reviews/widget'); ?>

        </div>

    </div>

</div>
