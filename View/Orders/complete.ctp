<div class="container">

	<div class="row">
		
		<div class="col-sm-24">

			<?php
				echo $this->element(
					'global/steps',
					array('steps' => $steps)
				);
			?>

		</div>

	</div>

</div>