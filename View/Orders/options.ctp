<div class="container">

	<div class="row">

		<div class="col-sm-24 col-md-16">

			<?php
				echo $this->element(
					'global/steps',
					array('steps' => $steps)
				);

				echo $this->Html->link(
					'Checkout',
					[
						'admin' => false,
						'controller' => 'orders',
						'action' => 'checkout'
					],
				);
			?>

		</div>

		<div class="col-sm-24 col-md-8">

			<div class="panel blue tight price-calculator">
				<h4>How much?</h4>
				<p>Try our instant price calculator</p>
			</div>

			<?php echo $this->element(
				 'orders/quote/calculator',
				array(
					'wizard' => false,
					'order' => !empty($order) ? $order : array()
				)
			); ?>

		</div>

	</div>

</div>