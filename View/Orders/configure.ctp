<?php echo $this->element('orders/order_steps', array('currentStep' => $currentStep)); ?>

<div class="container">

	<div class="row">

		<div class="col-sm-24">
			<h1><i class="fa fa-wrench"></i> Configure your document</h1>

			<?php echo $this->Flash->render(); ?>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-8">

			<?php echo $this->element('orders/summary/print_summary'); ?>

		</div>

		<div class="col-sm-16">

			<?php
				echo $this->element(
					'previewer/preview',
					array(
						'order' => $order
					)
				);

				if (!empty($orderItem['Product']['ProductTabType'])) {

//					echo $this->element(
//						'orders/configure/tabs',
//						array(
//							'order_item_tabs' => $order,
//							'tabs' => $tabs
//						)
//					);

				}
			?>

		</div>

	</div>

	<?php if (in_array($orderItem['Product']['BindingType']['name'], array('Hardback','Paperback'))) { ?>

		<div class="row">

			<div class="col-sm-24">

				<?php
					echo $this->element(
						'orders/configure/cover',
						array(
							'orderItem' => $orderItem
						)
					);
				?>

			</div>

		</div>

	<?php } ?>

	<div class="row">

		<div class="col-sm-8 col-sm-push-8">

			<?php // echo $this->element('orders/configure/extra_copies'); ?>

			<div class="panel panel-info">

				<div class="panel-heading">
					<i class="fa fa-truck fa-flip-horizontal"></i> Despatch details
				</div>

				<div class="panel-body">

					<?php
						if (!empty($order['Order'])) {
							echo $this->element(
								'orders/despatch/summary',
								array(
									'despatchDateFormatted' => $order['Order']['despatch_date_formatted'],
									'despatchMethod' => $order['TurnaroundOption']['name'],
									'deliveryMethod' => $order['DeliveryOption']['name'],
									'deliveryDateFormatted' => $order['Order']['delivery_date_formatted'],
									'latestDeliveryTime' => $order['DeliveryOption']['latest_delivery_time'],
									'isGuaranteed' => $order['DeliveryOption']['is_guaranteed']
								)
							);
						}
					?>

				</div>

			</div>

		</div>

		<div class="col-sm-8 col-sm-push-8">

			<div class="panel panel-info">

				<div class="panel-heading">
					<i class="fa fa-calculator"></i> Your quote
				</div>

				<div class="panel-body">

					<?php
						echo $this->element(
							'orders/quote/amounts',
							array(
								'order' => $order,
								'orderOptions' => array(
									'allow_edit' => false,
									'show_add_link' => true
								)
							)
						);

						echo $this->Form->create(
							null,
							array(
								'url' => [
									'admin' => false,
									'controller' => 'orders',
									'action' => 'checkout'
								],
								'class' => 'text-right'
							)
						);

							echo $this->Form->button(
								'Checkout <i class="fa fa-chevron-right"></i>',
								array(
									'type' => 'submit',
									'class' => 'btn btn-action btn-lg' . (in_array($orderItem['Product']['BindingType']['name'], ['Hardback','Paperback']) && !$orderItem['printed_cover_job_id'] && $orderItem['printed_cover_front_options'] != "blank_cover" ? ' disabled' : ''),
									'id' => 'btn_checkout'
								)
							);

						echo $this->Form->end();
					?>

				</div>

			</div>

		</div>

		<div class="col-sm-8 col-sm-pull-16">

			<?php echo $this->element('global_content/our_promise'); ?>

		</div>

	</div>

	<?php echo $this->element('structure/hr', array('colClass' => 'xs-24')); ?>

	<?php echo $this->element('faqs/configure'); ?>

</div>

<?php
	$completionPreviewsUrl = $this->Html->url(
		[
			'controller' => 'orders',
			'action' => 'complete_previews'
		]
	);
	$coverDesignerUrl = $this->Html->url(
		[
			'controller' => 'orders',
			'action' => 'cover_designer'
		]
	);
	$updateCoverUrl = $this->Html->url(
		[
			'controller' => 'orders',
			'action' => 'update_printed_cover'
		]
	);
	$deleteCoverUrl = $this->Html->url(
		[
			'controller' => 'orders',
			'action' => 'delete_printed_cover'
		]
	);
?>
<input id="callas-complete-previews-url" value="<?php echo $completionPreviewsUrl; ?>" type="hidden">
<input id="cover-designer-url" value="<?php echo $coverDesignerUrl; ?>" type="hidden">
<input id="update-cover-url" value="<?php echo $updateCoverUrl; ?>" type="hidden">
<input id="delete-cover-url" value="<?php echo $deleteCoverUrl; ?>" type="hidden">
<input id="cover-designer-in-use" value="<?php if (in_array($orderItem['Product']['BindingType']['name'], array('Hardback','Paperback'))): ?>1<?php else: ?>0<?php endif; ?>" type="hidden">
