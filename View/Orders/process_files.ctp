<?php echo $this->element('orders/order_steps', array('currentStep' => $currentStep)); ?>

<div class="container">

	<div class="row">

		<div class="col-sm-24">
			<h1><i class="fa fa-cog fa-spin"></i> Processing your files</h1>
		</div>

	</div>

	<div class="row">

		<div class="col-sm-24">

			<h5>Processing...</h5>

		</div>

	</div>

	<?php if (!empty($order['Order'])) { ?>

		<?php echo $this->element('structure/hr', array('colClass' => 'xs-24')); ?>

		<div id="quote-details">

			<div class="row">

				<div class="col-sm-24">

					<h3><i class="fa fa-thumbs-o-up"></i> Here's how your order is shaping up</h3>
					<p>This might change once we've examined your uploaded file but don't worry&mdash;you can change all that in the next step.</p>

				</div>

			</div>

			<?php
				if (!empty($order['Order'])) {
					echo $this->element(
						'orders/summary/progress',
						array(
							'order' => $order
						)
					);
				}
			?>

		</div>

	<?php } ?>

	<?php
		echo $this->element('structure/hr', array('colClass' => 'xs-24'));

		echo $this->element('faqs/upload');

	?>

</div>