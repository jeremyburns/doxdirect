<?php echo $this->element('orders/order_steps', array('currentStep' => $currentStep)); ?>

<div class="container">

	<div class="row">

		<div class="col-sm-24">
			<h1><i class="fa fa-wrench"></i> Please configure your document</h1>

			<?php echo $this->Flash->render(); ?>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-24">

			<div class="row">

				<div class="col-sm-8">

					<img src="<?php echo $failure['invalid_order_item']['OrderItemFile'][0]['OrderItemFilePage'][0]['preview_image_path']; ?>" class="img-thumbnail">

				</div>

				<div class="col-sm-16">

					<div class="row">

						<div class="col-sm-24">

							<?php echo $this->element(
								'Flash' . DS . $failure['alert']['class'],
								['message' => $failure['alert']]
							); ?>

							<div id="working-alert" class="alert alert-info-pale hidden"></div>

						</div>

					</div>

					<div class="row">

						<div class="col-sm-24">

							<div class="list-group">

								<?php
									$countOptions = count($failure['elements']['options']);

									foreach ($failure['elements']['options'] as $configurationOptionNumber => $option) {

										echo $this->element(
											'orders/configure/fix_option',
											array(
												'optionNumber' => $configurationOptionNumber + 1,
												'orderItem' => $failure['invalid_order_item'],
												'heading' => $option['heading'],
												'message' => $option['message'],
												'image' => $option['image'],
												'actions' => $option['actions']
											)
										);

									}

								?>

								<div class="list-group-item fix-option" data-class="fix-option-panel">

									<div class="media ">

										<div class="media-body">

											<?php
												echo $this->Html->link(
													'<i class="fa fa-chevron-left"></i> Return to the uploads page',
													[
														'admin' => false,
														'controller' => 'orders',
														'action' => 'upload'
													],
													[
														'escape' => false,
														'data-button' => 'fix-option'
													]
												);

											?>

										</div>

									</div>

								</div>


							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php
		echo $this->element('structure/hr', array('colClass' => 'xs-24'));

		echo $this->element('faqs/configure');

	?>

</div>
