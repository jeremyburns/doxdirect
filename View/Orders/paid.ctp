<div class="container">

	<div class="row">

		<div class="col-sm-24">

			<h1>Thanks! You've paid.</h1>

			<?php echo $this->Flash->render(); ?>

			<div class="alert alert-info-pale">

				<p class="lead">Your order number is: <?php echo $order['Order']['pace_job_number']; ?></p>

				<p>Please include your order number in any communications with us.</p>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-16">

			<h2>What next?</h2>

			<p class="lead">We've got your order and The Doxtors are getting ready to produce it.</p>

			<p>We'll print it with care and send it out with haste. Meanwhile, please check your inbox for an order confirmation email.</p>

		</div>

		<div class="col-sm-8">

			<div class="panel panel-info">

				<div class="panel-body">

					<h2>Questions?</h2>

					<p>You can call and speak to us on:</p>
					<p class="lead"><strong>0333 200 7272</strong></p>

				</div>

			</div>

		</div>

	</div>

	<?php if (!empty($order['Order'])) { ?>
	
		<?php echo $this->element(
			'analytics/ecommerce/paid',
			array('order' => $order)
		); ?>

		<div class="row">

			<div class="col-sm-8">

				<h3>Your order</h3>

				<?php echo $this->element(
					'orders/paid/order_details',
					array('order' => $order)
				); ?>

			</div>

			<div class="col-sm-8">

				<h3>Despatch and delivery</h3>

				<?php echo $this->element(
					'orders/paid/delivery_details',
					array('order' => $order)
				); ?>

			</div>

			<div class="col-sm-8">

				<h3>Payment</h3>

				<?php echo $this->element(
					'orders/paid/payment_details',
					array('order' => $order)
				); ?>

			</div>

		</div>

	<?php } ?>

</div>
