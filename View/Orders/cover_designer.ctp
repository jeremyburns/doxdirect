<?php
	echo $this->element(
		'orders/cover_designer/js',
		array('coverJobId' => $printedCoverJobId)
	);

	echo $this->element('orders/order_steps', array('currentStep' => $currentStep));

	$addCoverFilesUrl = $this->Html->url(
		[
			'controller' => 'orders',
			'action' => 'add_cover_files'
		]
	);
?>

<input id="add-cover-files-url" value="<?php echo $addCoverFilesUrl; ?>" type="hidden">

<div class="container mts">

	<div class="row">

		<div class="col-sm-24">

			<h1><i class="fa fa-paint-brush"></i> Design Your Book Cover</h1>

			<?php echo $this->Flash->render(); ?>

			<p>If you chose to use your first and/or last pages as starting points, these have been loaded into place below. To replace either of these, click the relevant "Edit" button on the canvas. Check to see how your cover will look (without the on-screen guidelines) with the Preview, and once you are happy with your cover design, please click the "Finished" button <?php echo $this->Html->image('doxdirect/graphics/cover-finished.png', ['width' => 50]); ?> to finalise your cover.</p>

			<p>Feel free to use our tutorial videos below to guide you through a number of key tasks you may wish to perform on your cover design.</p>

			<p><strong>NOTE: Please do not use the "back" button on your browser, as it may result in unexpected behaviour. The "Finished" button will take you back to the document configuration page.</strong></p>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-24">

			<div id="flashContent">

				<p>
					<i class="fa fa-circle-o-notch fa-spin"></i>
					<br>
					Please wait while we prepare your Book Cover template...
				</p>

			</div>

			<div class="row">

				<div class="col-sm-24 col-md-12">

					<h3>How to Add an Image</h3>

					<div class="videowrapper">
						<iframe src="https://www.youtube.com/embed/sRghpxnJDjY" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-sm-24 col-md-12">

					<h3>How to Add Text</h3>

					<div class="videowrapper">
						<iframe src="https://www.youtube.com/embed/rx_uhG5BOVk" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-sm-24 col-md-12">

					<h3>How to Add a Background</h3>

					<div class="videowrapper">
						<iframe src="https://www.youtube.com/embed/dwwauRmS9Yk" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-sm-24 col-md-12">

					<h3>How to Undo a Mistake</h3>

					<div class="videowrapper">
						<iframe src="https://www.youtube.com/embed/boaVawSI5Rc" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

			</div>

		</div>

	</div>

</div>
