<?php echo $this->element('orders/order_steps', array('currentStep' => $currentStep)); ?>

<div class="container">

	<div class="row">

		<div class="col-sm-24">
			<h1><i class="fa fa-shopping-cart"></i> Checkout</h1>
		</div>

	</div>

	<div class="row">

		<div class="col-sm-16">

			<?php echo $this->Flash->render(); ?>

			<div class="panel panel-info">

				<div class="panel-heading"><i class="fa fa-credit-card"></i> Please choose a payment method</div>

				<div class="panel-body">

					<div class="row">

						<?php $colClass = 24 / count($paymentMethods); ?>

						<?php foreach($paymentMethods as $paymentMethod) { ?>

							<div class="col-xs-<?php echo $colClass; ?>">

								<?php

									$config = $paymentConfig[$paymentMethod['PaymentMethod']['code']];

									echo $this->element(
										'payment_methods/' . $paymentMethod['PaymentMethod']['code'],
										array(
											'paymentMethod' => $paymentMethod,
											'cart' => $cart,
											'config' => $config
										)
									);

								?>

							</div>

						<?php } ?>

					</div>

				</div>

			</div>

			<?php echo $this->Html->link(
				'<i class="fa fa-chevron-left"></i> Edit my details',
				[
					'admin' => false,
					'controller' => 'orders',
					'action' => 'checkout'
				],
				array(
					'class' => 'btn btn-md btn-default',
					'escape' => false
				)
			); ?>

		</div>

		<div class="col-sm-8">

			<div class="panel panel-default">

				<div class="panel-heading">
					<i class="fa fa-calculator"></i> Your order
				</div>

				<div class="panel-body">

					<?php echo $this->element(
						'orders/quote/amounts',
						array(
							'order' => $order,
							'orderOptions' => array(
								'allow_edit' => false,
								'show_add_link' => false
							)
						)
					); ?>

					<?php echo $this->element(
						'analytics/ecommerce/pay_now',
						array(
							'order' => $order
						)
					); ?>

				</div>

			</div>

		</div>

	</div>

</div>
