<div class="medias index">

	<h2><?php echo __('Media'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th><?php echo $this->Paginator->sort('paper_size_id'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('thickness'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('weight'); ?></th>
					<th><?php echo $this->Paginator->sort('type'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($media as $thisMedia): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($thisMedia['Media']['id']),
									array(
										'action' => 'view',
										$thisMedia['Media']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($thisMedia['Media']['name']),
									array(
										'action' => 'view',
										$thisMedia['Media']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($thisMedia['Media']['pace_name']); ?></td>
						<td><?php echo h($thisMedia['Media']['paper_size_id']); ?></td>
						<td class="text-right"><?php echo h($thisMedia['Media']['thickness']); ?></td>
						<td class="text-right"><?php echo h($thisMedia['Media']['weight']); ?></td>
						<td><?php echo h($thisMedia['Media']['type']); ?></td>
						<td><?php echo $this->Presentation->yesNo($thisMedia['Media']['is_active']); ?></td>
						<td><?php echo h($thisMedia['Media']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $thisMedia['Media']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>
