<div class="medias view">

	<h2><?php echo __('Media'); ?></h2>

	<div class="row">

		<div class="col-sm-12">

			<table class="table table-condensed">

				<caption>Details</caption>

				<tr>
					<th><?php echo __('Id'); ?></th>
					<td><?php echo h($media['Media']['id']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Name'); ?></th>
					<td><?php echo h($media['Media']['name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Pace name'); ?></th>
					<td><?php echo h($media['Media']['pace_name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Paper size'); ?></th>
					<td>
						<?php
							echo $this->Html->link(
								$media['Media']['paper_size_id'],
								[
									'admin' => true,
									'controller' => 'paper_sizes',
									'action' => 'view',
									$media['Media']['paper_size_id']
								]
							);
						?>
					</td>
				</tr>
				<tr>
					<th><?php echo __('Thickness'); ?></th>
					<td><?php echo h($media['Media']['thickness']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Weight'); ?></th>
					<td><?php echo h($media['Media']['weight']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Type'); ?></th>
					<td><?php echo h($media['Media']['type']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Active'); ?></th>
					<td><?php echo $this->Presentation->yesNo($media['Media']['is_active']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Order items'); ?></th>
					<td><?php echo h($media['Media']['count_order_item']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Created'); ?></th>
					<td><?php echo h($this->Time->niceShort($media['Media']['created'])); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Modified'); ?></th>
					<td><?php echo h($this->Time->niceShort($media['Media']['modified'])); ?></td>
				</tr>
			</table>

		</div>

		<div class="col-sm-12">

			<?php echo $this->element(
				'product/admin/list',
				['products' => $media['ProductMedia']]
			); ?>
		</div>

	</div>

</div>

<div class="actions">
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('List media'), array('action' => 'index'));
		?>
	</div>
</div>
