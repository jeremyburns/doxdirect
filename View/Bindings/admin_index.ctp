<div class="bindings index">

	<h2><?php echo __('Binding options'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($bindings as $binding): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($binding['Binding']['id']),
									array(
										'action' => 'view',
										$binding['Binding']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($binding['Binding']['name']),
									array(
										'action' => 'view',
										$binding['Binding']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($binding['Binding']['pace_name']); ?></td>
						<td><?php echo $this->Presentation->yesNo($binding['Binding']['is_active']); ?></td>
						<td><?php echo h($binding['Binding']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $binding['Binding']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>
