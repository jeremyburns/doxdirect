<div class="bindings view">

	<h2><?php echo __('Binding option'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($binding['Binding']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($binding['Binding']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Pace name'); ?></th>
			<td><?php echo h($binding['Binding']['pace_name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($binding['Binding']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Order items'); ?></th>
			<td><?php echo h($binding['Binding']['count_order_item']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($binding['Binding']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($binding['Binding']['modified'])); ?></td>
		</tr>
	</table>

</div>

<?php

	echo $this->element(
		'navigation/admin_actions',
		[
			'data' => $binding['Binding'],
			'controllerName' => $this->params->controller,
			'itemNameField' => 'name',
			'show' => [
				'index'
			]
		]
);
