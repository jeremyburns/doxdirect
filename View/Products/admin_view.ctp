<div class="products view">

	<div class="page-header">
		<h1>Product</h1>
		<?php echo $this->Flash->render(); ?>
	</div>

	<div class="row">

		<div class="col-sm-12">

			<table class="table table-bordered table-condensed">

				<caption>Details</caption>

				<tbody>

					<tr>
						<th><?php echo __('Id'); ?></th>
						<td><?php echo h($product['Product']['id']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Name'); ?></th>
						<td><?php echo h($product['Product']['name']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Size'); ?></th>
						<td>
							<?php echo $this->Html->link(
								$product['Product']['paper_size_id'],
								[
									'admin' => true,
									'controller' => 'paper_sizes',
									'action' => 'view',
									$product['Product']['paper_size_id']
								]
							); ?>
						</td>
					</tr>
					<tr>
						<th><?php echo __('Binding'); ?></th>
						<td>
							<?php echo $this->Html->link(
								$product['Product']['binding_name'],
								[
									'admin' => true,
									'controller' => 'binding_types',
									'action' => 'view',
									$product['Product']['binding_type_id']
								]
							); ?>
						</td>
					</tr>
					<tr>
						<th><?php echo __('Default'); ?></th>
						<td><?php echo $this->Presentation->yesNo($product['Product']['is_default']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Active'); ?></th>
						<td><?php echo $this->Presentation->yesNo($product['Product']['is_active']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Order items'); ?></th>
						<td><?php echo h($product['Product']['count_order_item']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Created'); ?></th>
						<td><?php echo h($this->Time->niceShort($product['Product']['created'])); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Modified'); ?></th>
						<td><?php echo h($this->Time->niceShort($product['Product']['modified'])); ?></td>
					</tr>

				</tbody>

			</table>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-12">

			<?php

				echo $this->element(
					'product/admin/binding',
					['productBindings' => $product['ProductBinding']]
				);

				echo $this->element(
					'product/admin/click',
					['productClicks' => $product['ProductClick']]
				);

				echo $this->element(
					'product/admin/drilling_type',
					['productDrillingTypes' => $product['ProductDrillingType']]
				);

				echo $this->element(
					'product/admin/side',
					['productSides' => $product['ProductSide']]
				);

			?>

		</div>

		<div class="col-sm-12">

			<?php

				echo $this->element(
					'product/admin/binding_edge_type',
					['productBindingEdgeTypes' => $product['ProductBindingEdgeType']]
				);

				echo $this->element(
					'product/admin/binding_side_type',
					['productBindingSideTypes' => $product['ProductBindingSideType']]
				);

				echo $this->element(
					'product/admin/tab_type',
					['productTabTypes' => $product['ProductTabType']]
				);

			?>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-24">

			<?php

				echo $this->element(
					'product/admin/cover_type',
					['productCoverTypes' => $product['ProductCoverType']]
				);

				echo $this->element(
					'product/admin/media',
					['productMedias' => $product['ProductMedia']]
				);

			?>

		</div>

	</div>



</div>

<?php echo $this->element(
	'navigation/admin_actions',
	[
		'controllerName' => $this->params->controller,
		'itemNameField' => 'name',
		'show' => [
			'index'
		]
	]
);
