<div class="products index">

	<h2><?php echo __('Products'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">

			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('paper_size_id', 'Size'); ?></th>
					<th><?php echo $this->Paginator->sort('binding_name', 'Binding'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($products as $product): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($product['Product']['id']),
									array(
										'action' => 'view',
										$product['Product']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($product['Product']['name']),
									array(
										'action' => 'view',
										$product['Product']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($product['Product']['paper_size_id']),
									array(
										'admin' => true,
										'controller' => 'paper_sizes',
										'action' => 'view',
										$product['Product']['paper_size_id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($product['Product']['binding_name']),
									array(
										'admin' => true,
										'controller' => 'binding_types',
										'action' => 'view',
										$product['Product']['binding_type_id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo $this->Presentation->yesNo($product['Product']['is_default']); ?></td>
						<td><?php echo $this->Presentation->yesNo($product['Product']['is_active']); ?></td>
						<td><?php echo h($product['Product']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $product['Product']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>

			</tbody>

		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>