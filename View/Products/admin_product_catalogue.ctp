<h3>Product catalogue</h3>

<div>

	<div class="row">

		<div class="col-sm-24">

			<span id="flash" style="display: none;"></span>

			<?php

				echo $this->Form->create(
					null,
					[
						'url' => [
							'admin' => true,
							'controller' => 'products',
							'action' => 'product_catalogue'
						],
						'id' => 'FormRefreshProductCatalogue',
						'inputDefaults' => [
							'div' => 'form-group',
							'class' => 'form-control'
						],
						'role' => 'form',
						'class' => 'form-inline shaded'
					]
				); ?>

				<legend>Refresh and rebuild the product catalogue. <span class="text-info font-weight-normal"><i class="fa fa-info-circle"></i> It is advisable to check both options.</span></legend>

				<?php echo $this->BootstrapForm->horizontalInputs([
					'options' => [
						'divClass' => 'col-sm-',
					],
					'inputs' => [
						'clear_cache' => [
							'label' => 'Clear cache first',
							'type' => 'checkbox',
							'default' => true,
							'width-class-label' => 0,
							'width-class-input' => 24
						],
						'rebuild_product_map' => [
							'label' => 'Rebuild product map',
							'type' => 'checkbox',
							'default' => true,
							'width-class-label' => 0,
							'width-class-input' => 24
						]
					]
				]);

				echo $this->Form->button(
					'<i class="fa fa-refresh"></i> Refresh',
					array(
						'id' => 'btn-submit-product-catalogue-refresh',
						'type' => 'submit',
						'class' => 'btn btn-primary',
						'confirm' => 'Are you sure you want to refresh the product catalogue from Pace?',
						'div' => false
					)
				);

				echo $this->Form->end();

			?>

		</div>

	</div>

	<span id="product-catalogue">
		<?php
			echo $this->element(
				'product/admin/product_catalogue',
				['productCatalogue' => $productCatalogue]
			);
		?>
	</span>

</div>
