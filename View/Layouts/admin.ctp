<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			Admin: <?php echo $this->fetch('title') ?>
		</title>
		<?php
			echo $this->Html->meta(
				'favicon.png',
				'/favicon.png',
				array('type' => 'icon')
			);

			echo $this->Html->css('//fonts.googleapis.com/css?family=Lato:300,400,700');
			echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
			echo $this->Html->css('app.css');

			echo $this->fetch('meta');
			echo $this->fetch('css');

		?>

		<!--[if lt IE 9]>
		  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	</head>
	<body>
		<?php echo $this->element('structure/admin_header'); ?>

		<div class="container">
			<?php echo $this->Flash->render(); ?>
			<?php echo $this->fetch('content'); ?>
		</div>

		<?php

			echo $this->element('structure/admin_footer');

			echo $this->element(
				'structure/footer_js',
				array(
					'js' => empty($js) ? array() : $js,
					'jsVariables' => empty($jsVariables) ? array() : $jsVariables
				)
			);

		?>

	</body>

</html>
