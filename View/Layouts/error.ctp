<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			<?php echo $this->fetch('title') ?>
		</title>
		<?php
			echo $this->Html->meta(
				'favicon.png',
				'/favicon.png',
				array('type' => 'icon')
			);

			echo $this->Html->css('//fonts.googleapis.com/css?family=Lato:300,400,700');
			echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
			echo $this->Html->css('app.css');

			echo $this->fetch('meta');
			echo $this->fetch('css');

		?>

		<!--[if lt IE 9]>
		  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	</head>
	<body>
		<?php echo $this->element('structure/header'); ?>

		<div class="container">

			<div class="row">

				<div class="col-sm-18">

					<?php echo $this->fetch('content'); ?>

					<p class="lead"><i class="fa fa-home color-green"></i> <?php echo $this->Html->link('Click here', '/'); ?> to go to the home page.</p>

					<?php

						if (Configure::read('debug') > 0)  {
							echo $this->element('exception_stack_trace');
						}

					?>

				</div>

				<div class="col-sm-6">

					<?php echo $this->Html->image(CLOUDFRONTUPLOADS . DS . 'Dox-confused.png'); ?>

				</div>

			</div>

			<?php if (Configure::read('debug') > 0) { ?>

				<div class="row">

					<div class="col-sm-24">

						<?php

							echo $this->element('sql_dump');

							if (extension_loaded('xdebug')) {

								xdebug_print_function_stack();

							}

						?>

					</div>

				</div>

			<?php } ?>

		</div>

		<?php
			echo $this->element('structure/footer');

			echo $this->element(
				'structure/footer_js',
				array(
					'js' => !empty($js) ? $js : [],
					'jsVariables' => !empty($jsVariables) ? $jsVariables : []
				)
			);

		?>

	</body>

</html>
