<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			<?php echo $this->fetch('title') ?>
		</title>
		<?php
			echo $this->Html->meta(
				'favicon.png',
				'/favicon.png',
				array('type' => 'icon')
			);

			echo $this->Html->css('//fonts.googleapis.com/css?family=Lato:300,400,700');
			echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
			echo $this->Html->css('app.css');

			echo $this->fetch('meta');
			echo $this->fetch('css');

		?>

		<!--[if lt IE 9]>
		  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics<?php if(Configure::read('environment') == "dev"): echo "_debug"; endif; ?>.js','ga');

			ga('create', 'UA-11751675-6', <?php if(Configure::read('environment') == "live"): ?>'auto'<?php else: ?>{ 'cookieDomain' : 'none' }<?php endif; ?>);
		</script>

	</head>
	<body>
		<?php
			echo $this->element('structure/header');

				if (!empty($orderInProgress)) {
					echo $this->element('orders/return_to_order');
				}
				echo $this->fetch('content');
			echo $this->element('structure/footer');

			echo $this->element(
				'structure/footer_js',
				array(
					'js' => empty($js) ? array() : $js,
					'jsVariables' => empty($jsVariables) ? array() : $jsVariables,
					'jsScripts' => empty($jsScripts) ? array() : $jsScripts
				)
			);

		?>

		<script>
			ga('send', 'pageview');
		</script>

	</body>

</html>
