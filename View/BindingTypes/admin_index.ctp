<div class="bindingTypes index">

	<h2><?php echo __('Binding  types'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('extra_processing_day'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('leaves_min'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('leaves_max'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('pages_min'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('pages_max'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('thickness_min'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('thickness_max'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('format_width_min'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('format_width_max'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('format_height_min'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('format_height_max'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('page_divisor'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($bindingTypes as $bindingType): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($bindingType['BindingType']['id']),
									array(
										'action' => 'view',
										$bindingType['BindingType']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($bindingType['BindingType']['name']),
									array(
										'action' => 'view',
										$bindingType['BindingType']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['extra_processing_day']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['leaves_min']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['leaves_max']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['pages_min']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['pages_max']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['thickness_min']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['thickness_max']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['format_width_min']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['format_width_max']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['format_height_min']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['format_height_max']); ?></td>
						<td class="text-right"><?php echo h($bindingType['BindingType']['page_divisor']); ?></td>

						<td><?php echo $this->Presentation->yesNo($bindingType['BindingType']['is_active']); ?></td>
						<td><?php echo $this->Presentation->yesNo($bindingType['BindingType']['is_default']); ?></td>
						<td><?php echo h($bindingType['BindingType']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $bindingType['BindingType']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $bindingType['BindingType']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>