<div class="bindingTypes view">

	<h2><?php echo __('Binding  type'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($bindingType['BindingType']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($bindingType['BindingType']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Extra processing day'); ?></th>
			<td><?php echo h($bindingType['BindingType']['extra_processing_day']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Leaves min'); ?></th>
			<td><?php echo h($bindingType['BindingType']['leaves_min']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Leaves max'); ?></th>
			<td><?php echo h($bindingType['BindingType']['leaves_max']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Pages min'); ?></th>
			<td><?php echo h($bindingType['BindingType']['pages_min']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Pages max'); ?></th>
			<td><?php echo h($bindingType['BindingType']['pages_max']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Thickness min'); ?></th>
			<td><?php echo h($bindingType['BindingType']['thickness_min']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Thickness max'); ?></th>
			<td><?php echo h($bindingType['BindingType']['thickness_max']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Format width min'); ?></th>
			<td><?php echo h($bindingType['BindingType']['format_width_min']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Format width max'); ?></th>
			<td><?php echo h($bindingType['BindingType']['format_width_max']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Format height min'); ?></th>
			<td><?php echo h($bindingType['BindingType']['format_height_min']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Format height max'); ?></th>
			<td><?php echo h($bindingType['BindingType']['format_height_max']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Page divisor'); ?></th>
			<td><?php echo h($bindingType['BindingType']['page_divisor']); ?></td>
		</tr>

		<tr>
			<th><?php echo __('Default'); ?></th>
			<td><?php echo $this->Presentation->yesNo($bindingType['BindingType']['is_default']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($bindingType['BindingType']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Order items'); ?></th>
			<td><?php echo h($bindingType['BindingType']['count_order_item']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($bindingType['BindingType']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($bindingType['BindingType']['modified'])); ?></td>
		</tr>
	</table>

</div>

<?php echo $this->element(
	'navigation/admin_actions',
	[
		'controllerName' => $this->params->controller,
		'itemNameField' => 'name',
		'show' => [
			'index'
		]
	]
);
