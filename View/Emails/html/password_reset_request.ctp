<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Emails.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<table>
  <thead>
      <tr>
          <th>Password reset request</th>
      </tr>
  </thead>
   <tfoot>
       <tr>
           <td>Doxdirect</td>
       </tr>
   </tfoot>
   <tbody>
        <tr>
            <td>You have asked to reset your password. Please click <a href="<?php echo $this->Html->url(['admin' => false, 'controller' => 'users', 'action' => 'reset_password', $passwordResetCode], true); ?>">here</a> to choose a new password.</td>
        </tr>
    </tbody>

</table>
