<div class="deliveryOptions view">

	<h2><?php echo __('Delivery option'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($deliveryOption['DeliveryOption']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($deliveryOption['DeliveryOption']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Description'); ?></th>
			<td><?php echo h($deliveryOption['DeliveryOption']['description']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Pace Code'); ?></th>
			<td><?php echo h($deliveryOption['DeliveryOption']['pace_code']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Days'); ?></th>
			<td><?php echo h($deliveryOption['DeliveryOption']['days']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Is guaranteed'); ?></th>
			<td><?php echo $this->Presentation->yesNo($deliveryOption['DeliveryOption']['is_guaranteed']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Latest delivery time'); ?></th>
			<td><?php echo h($deliveryOption['DeliveryOption']['latest_delivery_time']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Weekdays only'); ?></th>
			<td><?php echo $this->Presentation->yesNo($deliveryOption['DeliveryOption']['weekdays_only']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Minimum order value'); ?></th>
			<td><?php echo h($deliveryOption['DeliveryOption']['minimum_order_value']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Order count'); ?></th>
			<td><?php echo h($deliveryOption['DeliveryOption']['count_order']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Is default'); ?></th>
			<td><?php echo $this->Presentation->yesNo($deliveryOption['DeliveryOption']['is_default']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Is active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($deliveryOption['DeliveryOption']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($deliveryOption['DeliveryOption']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($deliveryOption['DeliveryOption']['modified'])); ?></td>
		</tr>
	</table>

</div>

<?php

echo $this->element(
	'navigation/admin_actions',
	[
		'data' => $deliveryOption['DeliveryOption'],
		'controllerName' => $this->params->controller,
		'itemNameField' => 'name',
		'show' => [
			'index'
		]
	]
);
