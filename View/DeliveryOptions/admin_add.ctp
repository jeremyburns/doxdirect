<div class="deliveryOptions form">
	<?php echo $this->Form->create(
		'DeliveryOption',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Add delivery option'); ?></legend>
		<?php
			echo $this->Form->input('name');
			echo $this->Form->input('description');
			echo $this->Form->input('pace_code');
			echo $this->Form->input('days');
			echo $this->Form->input('is_guaranteed');
			echo $this->Form->input('latest_delivery_time');
			echo $this->Form->input('weekdays_only');
			echo $this->Form->input('minimum_order_value');
			echo $this->Form->input('is_default');
			echo $this->Form->input('is_active');
		?>
	</fieldset>

	<?php echo $this->element(
		'navigation/admin_actions',
		[
			'data' => $deliveryOption['DeliveryOption'],
			'controllerName' => $this->params->controller,
			'itemNameField' => 'name',
			'show' => [
				'save',
				'index'
			]
		]
	);

	echo $this->Form->end();

?>
</div>


