<div class="deliveryOptions index">

    <h2><?php echo __('Delivery options'); ?></h2>

    <div class="table-responsive">

        <table class="table table-condensed">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                    <th>Description</th>
                    <th><?php echo $this->Paginator->sort('pace_code'); ?></th>
                    <th class="text-right"><?php echo $this->Paginator->sort('days'); ?></th>
                    <th><?php echo $this->Paginator->sort('is_guaranteed'); ?></th>
                    <th><?php echo $this->Paginator->sort('latest_delivery_time'); ?></th>
                    <th><?php echo $this->Paginator->sort('weekdays_only'); ?></th>
                    <th class="text-right"><?php echo $this->Paginator->sort('minimum_order_value', 'Min order val'); ?></th>
                    <th class="text-right"><?php echo $this->Paginator->sort('count_order', 'Orders'); ?></th>
                    <th class="text-center"><?php echo $this->Paginator->sort('is_default'); ?></th>
                    <th class="text-center"><?php echo $this->Paginator->sort('is_active'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($deliveryOptions as $deliveryOption): ?>
                    <tr>
                        <td>
                            <?php
                                echo $this->Html->link(
                                    h($deliveryOption['DeliveryOption']['id']),
                                    array(
                                        'action' => 'view',
                                        $deliveryOption['DeliveryOption']['id']
                                    )
                                );
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $this->Html->link(
                                    h($deliveryOption['DeliveryOption']['name']),
                                    array(
                                        'action' => 'view',
                                        $deliveryOption['DeliveryOption']['id']
                                    ),
                                    array(
                                        'escape' => false
                                    )
                                );
                            ?>
                        </td>
                        <td><?php echo h($deliveryOption['DeliveryOption']['description']); ?></td>
                        <td><?php echo h($deliveryOption['DeliveryOption']['pace_code']); ?></td>
                        <td class="text-right"><?php echo h($deliveryOption['DeliveryOption']['days']); ?></td>
                        <td><?php echo $this->Presentation->yesNo($deliveryOption['DeliveryOption']['is_guaranteed']); ?></td>
                        <td><?php echo h($deliveryOption['DeliveryOption']['latest_delivery_time']); ?></td>
                        <td><?php echo $this->Presentation->yesNo($deliveryOption['DeliveryOption']['weekdays_only']); ?></td>
                        <td class="text-right"><?php echo h($deliveryOption['DeliveryOption']['minimum_order_value']); ?></td>
                        <td class="text-right"><?php echo h($deliveryOption['DeliveryOption']['count_order']); ?></td>
                        <td class="text-center"><?php echo $this->Presentation->yesNo($deliveryOption['DeliveryOption']['is_default']); ?></td>
                        <td class="text-center"><?php echo $this->Presentation->yesNo($deliveryOption['DeliveryOption']['is_active']); ?></td>
                        <td class="actions">
                            <div class="btn-group">
                                <?php
                                    echo $this->Html->link(__('View'), array('action' => 'view', $deliveryOption['DeliveryOption']['id']), array('class' => 'btn btn-xs btn-default'));
                                    echo $this->Html->link(__('Edit'), array('action' => 'edit', $deliveryOption['DeliveryOption']['id']), array('class' => 'btn btn-xs btn-default'));
                                ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>

    <?php echo $this->element('navigation/pagination'); ?>

</div>
