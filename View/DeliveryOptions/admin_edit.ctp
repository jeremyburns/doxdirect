<div class="deliveryOptions form">
	<?php echo $this->Form->create(
		'DeliveryOption',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Edit delivery option'); ?></legend>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('name');
			echo $this->Form->input('description');
			echo $this->Form->input('pace_code');
			echo $this->Form->input('days');
			echo $this->Form->input('is_guaranteed');
			echo $this->Form->input('latest_delivery_time');
			echo $this->Form->input('weekdays_only');
			echo $this->Form->input('minimum_order_value');
			echo $this->Form->input('is_default');
			echo $this->Form->input('is_active');
		?>
	</fieldset>
	<?php

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Form->postLink(
				__('Delete'),
				array(
					'action' => 'delete',
					$this->Form->value('DeliveryOption.id')
				),
				array(),
				__('Are you sure you want to delete # %s?', $this->Form->value('DeliveryOption.id'))
			);
			echo $this->Html->link(__('List delivery options'), array('action' => 'index'));
		?>
	</div>
</div>
