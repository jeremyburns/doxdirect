<?php

App::uses('AppHelper', 'View/Helper');

class BootstrapFormHelper extends AppHelper {

	public $helpers = ['Form'];

	public function labelWithPopover($label = '', $popoverTitle = '', $popoverContent = '' ) {

		return $label .'&nbsp;<i class="fa fa-question-circle help-icon" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="auto" title="' . $popoverTitle . '" data-content="' . $popoverContent . '"></i>';

	}

	public function horizontalInputs($elements = []) {

		$inputs = $elements['inputs'];
		$elementOptions = $elements['options'];

		$this->divClass = $elementOptions['divClass'];

		$output = '';

		foreach ($inputs as $fieldName => $options) {

			$output .= $this->horizontalInput($fieldName, $options);

		}

		return $output;

	}

	public function horizontalInput($fieldName = null, $options = []) {

		$defaults = [
			'empty' => true,
			'type' => 'text',
			'required' => false,
			'default' => null,
			'width-class-label' => 8,
			'width-class-input' => 16
		];

		$options = array_merge(
			$defaults,
			$options
		);

		if ($options['type'] == 'hidden') {

			$output = $this->hidden($fieldName, $options);

			return $output;

		}

		if (!isset($options['placeHolder'])) {
			$options['placeHolder'] = $options['label'];
		}

		$this->wrapperDivClass = 'form-group';

		$divClass = $this->divClass;

		if ($options['required']) {
			$this->wrapperDivClass .= ' required';
		}

		if ($options['type'] == 'checkbox') {

			$output = $this->checkbox($fieldName, $options);

		} elseif ($options['type'] == 'select') {

			$output = $this->select($fieldName, $options);

		} else {

			$output = $this->input($fieldName, $options);

		}

		if (!empty($options['help-block'])) {
			$output .= '<p class="help-block">' . $options['help-block'] . '</p>';
		}

		return $output;

	}

	public function input($fieldName = null, $options = []) {

		$output = '<div class="' . $this->wrapperDivClass . '">';

			$output .= $this->Form->label(
				$fieldName,
				$options['label'],
				['class' => 'control-label ' . $this->divClass . $options['width-class-label']]
			);

			$inputOptions = [
				'type' => $options['type'],
				'class' => 'form-control',
				'placeHolder' => $options['placeHolder'],
				'div' => $this->divClass . $options['width-class-input'],
				'label' => false,
				'required' => $options['required'],
				'default' => $options['default']
			];

			$optionKeys = [
				'value',
				'before',
				'after'
			];

			foreach ($optionKeys as $optionKey) {
				if (isset($options[$optionKey])) {
					$inputOptions[$optionKey] = $options[$optionKey];
				}
			}

			if (isset($options['wrap'])) {
				$inputOptions['div'] .= ' ' . $options['wrap'];
			}
			$output .= $this->Form->input(
				$fieldName,
				$inputOptions
			);


		$output .= '</div>';

		return $output;


	}

	public function checkbox($fieldName = null, $options = []) {

		$output = '<div class="form-group">';

		$output .= '<div class="' . $this->divClass . 'offset-' . $options['width-class-label'] . ' ' . $this->divClass . $options['width-class-input'] . '">';

		$output .= '<div class="checkbox"><label class="required">';

			$output .= $this->Form->checkbox($fieldName, $options);
			$output .= $options['label'];

		$output .= "</label></div></div></div>";

		return $output;
	}

	public function select($fieldName = null, $options = []) {

		$output = '<div class="' . $this->wrapperDivClass . '">';

			if ($options['label']) {

				$output .= $this->Form->label(
					$fieldName,
					$options['label'],
					['class' => 'control-label ' . $this->divClass . $options['width-class-label']]
				);

			}

			$output .= '<div class="' . $this->divClass . $options['width-class-input'] . '">';

				$controlOptions = [
					'empty' => $options['empty'],
					'class' => 'form-control',
					'label' => false,
					'required' => $options['required'],
					'label' => $options['label'],
					'default' => $options['default']
				];

				if (!empty($options['attributes'])) {
					$controlOptions = array_merge(
						$controlOptions,
						$options['attributes']
					);
				}

				$output .= $this->Form->select(
					$fieldName,
					$options['selectOptions'],
					$controlOptions
				);

			$output .= '</div>';
		$output .= '</div>';

		return $output;
	}

	public function hidden($fieldName, $options) {

		$output = $this->Form->input($fieldName, $options);

		return $output;

	}

}
