<?php
class PresentationHelper extends AppHelper {

	var $helpers = array('Form', 'Time');

	function niceDate($dateString = null, $includeTime = true, $userOffset = null) {

		if (!$dateString) {
			return '';
		}

		$date = $dateString
			? $this->Time->fromString($dateString, $userOffset)
			: time()
		;

		$y = $this->Time->isThisYear($date)
			? ''
			: ' %Y'
		;



		if ($this->Time->isToday($dateString, $userOffset)) {

			if ($includeTime) {

				return $this->Time->format($dateString, '%H:%M') . ' today';

			} else {

				return 'Today';

			}

		} elseif ($this->Time->wasYesterday($dateString, $userOffset)) {

			if ($includeTime) {

				return $this->Time->format($dateString, '%H:%M') . ' yesterday';

			} else {


				return 'Yesterday';

			}

		} else {

			if ($includeTime) {

				$format = $this->Time->convertSpecifiers("%b %eS{$y}, %H:%M", $date);

			} else {

				$format = $this->Time->convertSpecifiers("%b %eS{$y}", $date);

			}

			return strftime($format, $date);

		}

		return null;

	}

	function gender($gender = null) {

		if ($gender == 'm') {
			return 'Male';
		} elseif  ($gender == 'f') {
			return 'Female';
		} elseif  ($gender == 'b') {
			return 'Both';
		} else {
			return '';
		}
	}

	function genderPronoun($gender = null) {

		if ($gender == 'm') {
			return 'he';
		} elseif  ($gender == 'f') {
			return 'she';
		} else {
			return 'they';
		}
	}

	function genderPossessivePronoun($gender = null) {

		if ($gender == 'm') {
			return 'his';
		} elseif  ($gender == 'f') {
			return 'her';
		} else {
			return 'their';
		}
	}

	function currency($amount = 0, $customOptions = array()) {

		$options = array(
			'symbol' => '&pound;',
			'smallSymbol' => 'p',
			'decimalPlaces' => 2,
			'nilValue' => 0
		);

		$options = array_merge($options, $customOptions);

		extract($options);

		if (!$amount || $amount == 0) {
			return $nilValue;
		} elseif  ($amount < 1) {
			return number_format(($amount * 100), 0) . $smallSymbol;
		} else {
			return $symbol . number_format($amount, $decimalPlaces);
		}
	}

	function accounting($amount = 0, $symbol = '&pound;', $decimalPlaces = 2, $zeroValue = null) {

		if (in_array($amount, array(0, '0', '0.00'))) {
			if ($zeroValue || $zeroValue === 0) {
				return $symbol . number_format($zeroValue, $decimalPlaces);
			} else {
				return $zeroValue;
			}
		} else {
			return $symbol . number_format($amount, $decimalPlaces);
		}

	}

	function yesNo($value = 0, $options = array()) {

		$defaults = array(
			'tick' => true,
			'cross' => true
		);

		$options = array_merge(
			$defaults,
			$options
		);

		if (in_array($value, [1, 'y'])) {
			if (isset($options['tick'])) {
				return '<i  class="fa fa-check color-green"></i>';
			} else {
				if (isset($options['yes'])) {
					return $options['yes'];
				} else {
					return 'Yes';
				}
			}
		} else {
			if (isset($options['minus'])) {
				return '<i  class="fa fa-minus color-red"></i>';
			} elseif (isset($options['cross'])) {
				return '<i  class="fa fa-times color-gray-lighter"></i>';
			} else {
				if (isset($options['no'])) {
					return $options['no'];
				} else {
					return 'No';
				}
			}
		}
	}

	function ordinal($value = 0) {

		switch ( $value ) {
			case 1:
				return 'first';
				break;
			case 2:
				return 'second';
				break;
			case 3:
				return 'third';
				break;
			case 4:
				return 'fourth';
				break;
			case 5:
				return 'fifth';
				break;
			case 6:
				return 'sixth';
				break;
			case 7:
				return 'seventh';
				break;
			case 8:
				return 'eighth';
				break;
			case 9:
				return 'ninth';
				break;
			case 10:
				return 'tenth';
				break;
			default:
				return '';
		}
	}

	function pluralize($count, $value, $highlightCount = true, $possessive = false) {

		// If we need to highlight the text count, then wrap the count in <strong> tags
		$countText = ($highlightCount ? '<strong>' : '').$count.($highlightCount ? '</strong>' : '');

		if ($possessive) {
			$verb = 'has';
		} elseif ($count == 1) {
			$verb = 'is';
		} else {
			$verb = 'are';
		}

		if ($count == 1) {
			return $verb.' '.$countText.' '.$value;
		} else {
			return $verb.' '.$countText.' '.Inflector::pluralize($value);
		}
	}

	function getAddress($clinic = array(), $separator = ', ') {

		$address = '';

		$fields = array('address_1', 'address_2', 'city', 'postcode');

		foreach($fields as $field) {
			if (isset($clinic[$field]) && strlen($clinic[$field])) {
				if ($address != '') $address .= $separator;
				$address .= $clinic[$field];
			}
		}

		return $address;
	}

	function multiSelect($inputName = '', $label = null, $tall = false) {

		if (!$label) $label = Inflector::pluralize($inputName);

		$output = '<div class="input multi-select '.($tall ? 'tall' : '').'"><label for="'.$inputName.$inputName.'">'.$label.'</label>';
		$output .= '<div class="multi-select-check">';
		$output .= $this->Form->input($inputName, array('multiple' => 'checkbox', 'label' => false, 'div' => false));
		$output .= '</div></div>';

		return $output;

	}

	function getUrl($url) {

		$protocol = 'http://';

		if (substr($url, 0, strlen($protocol)) == $protocol) {
			return $url;
		} else {
			return $protocol . $url;
		}
	}

	function stringToSlug($str, $replacement = "-") {

		// turn into slug
		$str = Inflector::slug($str, $replacement);

		// to lowercase
		$str = strtolower($str);

		return $str;
	}

	function denied() {
		return '<i class="icon-ban-circle denied" title="You do not have access to this information"></i>';
	}

	public function formDimensions() {

		$narrow = 8;
		$wide = 24 - $narrow;
		$breakPoint = 'xs';

		$inputDivClassNarrow = 'col-' . $breakPoint . '-' . $narrow;
		$inputDivClassWide = 'col-' . $breakPoint . '-' . $wide;
		$inputDivClassOffsetNarrow = 'col-' . $breakPoint . '-offset-' . $narrow;
		$numberInputWidth = 'col-' . $breakPoint . '-' . $narrow;

		$labelClass = $inputDivClassNarrow . ' control-label';

		$formDimensions = array(
			'inputDivClassNarrow' => $inputDivClassNarrow,
			'inputDivClassWide' => $inputDivClassWide,
			'inputDivClassOffsetNarrow' => $inputDivClassOffsetNarrow,
			'numberInputWidth' => $numberInputWidth,
			'labelClass' => $labelClass
		);

		return $formDimensions;

	}

}
?>
