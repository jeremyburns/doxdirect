<?php

App::uses('AppHelper', 'View/Helper');

class DoxHelper extends AppHelper {

	public function address($address = array()) {

		if (!$address) {
			return null;
		}

		$output = '';

		if (!empty($address['house_number']) && !empty($address['address_1'])) {

			$output .= $address['house_number'] . ' ' . $address['address_1'] . '<br>';

		} elseif (!empty($address['house_number'])) {

			$output .= '<strong>' . $address['house_number'] . '</strong><br>';

		} elseif (!empty($address['address_1'])) {

			$output .= '<strong>' . $address['address_1'] . '</strong><br>';

		}

		$fields = array(
			'address_2',
			'address_3',
			'city',
			'post_code'
		);

		foreach ($fields as $field) {

			if (!empty($address[$field])) {

				$output .= $address[$field] . '<br>';

			}

		}

		if (!empty($address['Country']['name'])) {

			$output .= $address['Country']['name'];

		}

		if ($output) {

			$output = '<address>' . $output . '</address>';
		}

		return $output;

	}

}
