<div class="clicks index">

	<h2><?php echo __('Clicks'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active', 'Active'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default', 'Default'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($clicks as $click): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($click['Click']['id']),
									array(
										'action' => 'view',
										$click['Click']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($click['Click']['name']),
									array(
										'action' => 'view',
										$click['Click']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($click['Click']['pace_name']); ?></td>
						<td><?php echo $this->Presentation->yesNo($click['Click']['is_active']); ?></td>
						<td><?php echo $this->Presentation->yesNo($click['Click']['is_default']); ?></td>
						<td><?php echo h($click['Click']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $click['Click']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>