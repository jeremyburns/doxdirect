<div class="holidays view">

	<h2><?php echo __('Holiday'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($holiday['Holiday']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($holiday['Holiday']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Day'); ?></th>
			<td><?php echo h($holiday['Holiday']['day']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Month'); ?></th>
			<td><?php echo h($holiday['Holiday']['month']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Year'); ?></th>
			<td><?php echo h($holiday['Holiday']['year']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Will despatch'); ?></th>
			<td><?php echo $this->Presentation->yesNo($holiday['Holiday']['will_despatch']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Will collect'); ?></th>
			<td><?php echo $this->Presentation->yesNo($holiday['Holiday']['will_collect']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Will deliver'); ?></th>
			<td><?php echo $this->Presentation->yesNo($holiday['Holiday']['will_deliver']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($holiday['Holiday']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($holiday['Holiday']['modified'])); ?></td>
		</tr>
	</table>

</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('Edit holiday'), array('action' => 'edit', $holiday['Holiday']['id']));
			echo $this->Form->postLink(__('Delete holiday'), array('action' => 'delete', $holiday['Holiday']['id']), array(), __('Are you sure you want to delete # %s?', $holiday['Holiday']['id']));
			echo $this->Html->link(__('List holidays'), array('action' => 'index'));
			echo $this->Html->link(__('New holiday'), array('action' => 'add'));
		?>
	</div>
</div>
