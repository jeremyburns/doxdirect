<div class="holidays index">

	<h2><?php echo __('Holidays'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('day'); ?></th>
					<th><?php echo $this->Paginator->sort('month'); ?></th>
					<th><?php echo $this->Paginator->sort('year'); ?></th>
					<th><?php echo $this->Paginator->sort('will_despatch'); ?></th>
					<th><?php echo $this->Paginator->sort('will_collect'); ?></th>
					<th><?php echo $this->Paginator->sort('will_deliver'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($holidays as $holiday): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($holiday['Holiday']['id']),
									array(
										'action' => 'view',
										$holiday['Holiday']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($holiday['Holiday']['name']),
									array(
										'action' => 'view',
										$holiday['Holiday']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($holiday['Holiday']['day']); ?></td>
						<td><?php echo h($holiday['Holiday']['month']); ?></td>
						<td><?php echo h($holiday['Holiday']['year']); ?></td>
						<td><?php echo $this->Presentation->yesNo($holiday['Holiday']['will_despatch']); ?></td>
						<td><?php echo $this->Presentation->yesNo($holiday['Holiday']['will_collect']); ?></td>
						<td><?php echo $this->Presentation->yesNo($holiday['Holiday']['will_deliver']); ?></td>
						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $holiday['Holiday']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $holiday['Holiday']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $holiday['Holiday']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $holiday['Holiday']['id']));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<div class="btn-group">
			<?php echo $this->Html->link(__('New holiday'), array('action' => 'add')); ?>
		</div>
	</div>

</div>
