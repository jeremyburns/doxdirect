<div class="holidays form">
	<?php echo $this->Form->create(
		'Holiday',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Add holiday'); ?></legend>
		<?php
			echo $this->Form->input('name');
			echo $this->Form->input('day');
			echo $this->Form->input('month');
			echo $this->Form->input('year');
			echo $this->Form->input('will_despatch');
			echo $this->Form->input('will_collect');
			echo $this->Form->input('will_deliver');
		?>
	</fieldset>
	<?php

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php echo $this->Html->link(__('List Holidays'), array('action' => 'index')); ?>
	</div>
</div>
