<div class="bindingSideTypes view">

	<h2><?php echo __('Binding side type'); ?></h2>

	<div class="row">

		<div class="col-sm-12">

			<table class="table table-condensed">

				<caption>Details</caption>

				<tr>
					<th><?php echo __('Id'); ?></th>
					<td><?php echo h($bindingSideType['BindingSideType']['id']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Name'); ?></th>
					<td><?php echo h($bindingSideType['BindingSideType']['name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Pace name'); ?></th>
					<td><?php echo h($bindingSideType['BindingSideType']['pace_name']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Default'); ?></th>
					<td><?php echo $this->Presentation->yesNo($bindingSideType['BindingSideType']['is_default']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Active'); ?></th>
					<td><?php echo $this->Presentation->yesNo($bindingSideType['BindingSideType']['is_active']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Order items'); ?></th>
					<td><?php echo h($bindingSideType['BindingSideType']['count_order_item']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Created'); ?></th>
					<td><?php echo h($this->Time->niceShort($bindingSideType['BindingSideType']['created'])); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Modified'); ?></th>
					<td><?php echo h($this->Time->niceShort($bindingSideType['BindingSideType']['modified'])); ?></td>
				</tr>
			</table>

		</div>

		<div class="col-sm-12">

			<?php
				echo $this->element(
					'product/admin/list',
					['products' => $bindingSideType['ProductBindingSideType']]
				);
			?>

		</div>

	</div>

</div>

<?php echo $this->element(
	'navigation/admin_actions',
	[
		'controllerName' => $this->params->controller,
		'itemNameField' => 'name',
		'show' => [
			'index'
		]
	]
);
