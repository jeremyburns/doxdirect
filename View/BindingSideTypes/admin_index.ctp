<div class="bindingSideTypes index">

	<h2><?php echo __('Binding side types'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('pace_name'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($bindingSideTypes as $bindingSideType): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($bindingSideType['BindingSideType']['id']),
									array(
										'action' => 'view',
										$bindingSideType['BindingSideType']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($bindingSideType['BindingSideType']['name']),
									array(
										'action' => 'view',
										$bindingSideType['BindingSideType']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($bindingSideType['BindingSideType']['pace_name']); ?></td>
						<td><?php echo $this->Presentation->yesNo($bindingSideType['BindingSideType']['is_active']); ?></td>
						<td><?php echo $this->Presentation->yesNo($bindingSideType['BindingSideType']['is_default']); ?></td>
						<td><?php echo h($bindingSideType['BindingSideType']['count_order_item']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $bindingSideType['BindingSideType']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>