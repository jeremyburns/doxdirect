<div class="sources view">

	<h2><?php echo __('Source'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($source['Source']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($source['Source']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Sequence'); ?></th>
			<td><?php echo h($source['Source']['sequence']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($source['Source']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Orders'); ?></th>
			<td><?php echo h($source['Source']['count_order']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($source['Source']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($source['Source']['modified'])); ?></td>
		</tr>
	</table>

</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('Edit source'), array('action' => 'edit', $source['Source']['id']));
			echo $this->Form->postLink(__('Delete source'), array('action' => 'delete', $source['Source']['id']), array(), __('Are you sure you want to delete # %s?', $source['Source']['id']));
			echo $this->Html->link(__('List sources'), array('action' => 'index'));
			echo $this->Html->link(__('New source'), array('action' => 'add'));
		?>
	</div>
</div>
