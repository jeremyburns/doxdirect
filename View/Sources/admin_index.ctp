<div class="sources index">

	<h2><?php echo __('Sources'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('sequence'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($sources as $source): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($source['Source']['id']),
									array(
										'action' => 'view',
										$source['Source']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($source['Source']['name']),
									array(
										'action' => 'view',
										$source['Source']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($source['Source']['sequence']); ?></td>
						<td><?php echo $this->Presentation->yesNo($source['Source']['is_active']); ?></td>
						<td><?php echo h($source['Source']['count_order']); ?></td>
						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $source['Source']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $source['Source']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $source['Source']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $source['Source']['id']));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<div class="btn-group">
			<?php echo $this->Html->link(__('New source'), array('action' => 'add')); ?>
		</div>
	</div>

</div>
