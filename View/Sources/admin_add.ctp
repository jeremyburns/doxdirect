<div class="sources form">
	<?php echo $this->Form->create(
		'Source',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Add source'); ?></legend>
		<?php
			echo $this->Form->input('name');
			echo $this->Form->input('sequence');
			echo $this->Form->input('is_active');
		?>
	</fieldset>
	<?php

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php echo $this->Html->link(__('List Sources'), array('action' => 'index')); ?>
	</div>
</div>
