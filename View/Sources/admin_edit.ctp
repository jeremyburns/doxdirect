<div class="sources form">
	<?php echo $this->Form->create(
		'Source',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Edit source'); ?></legend>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('name');
			echo $this->Form->input('sequence');
			echo $this->Form->input('is_active');
		?>
	</fieldset>
	<?php

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Form->postLink(
				__('Delete'),
				array(
					'action' => 'delete',
					$this->Form->value('Source.id')
				),
				array(),
				__('Are you sure you want to delete # %s?', $this->Form->value('Source.id'))
			);
			echo $this->Html->link(__('List sources'), array('action' => 'index'));
		?>
	</div>
</div>
