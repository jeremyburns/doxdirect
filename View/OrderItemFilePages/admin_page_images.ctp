<div class="page-header">
	<h1>File upload fix images</h1>
</div>

<?php

	$counter = 1;
	$imagesPerRow = 4;
	$colClass = 24 / $imagesPerRow;

	foreach ($pageImages as $pageImage) {

		if ($counter % $imagesPerRow == 1) { ?>

			<div class="row">

		<?php } ?>

		<div class="col-sm-<?php echo $colClass; ?>">

			<div class="thumbnail">

				<?php

					foreach ($pageImage['images'] as $imageName) {
						echo $this->Html->image(
							'pages' . DS . $imageName
						);
					}
				?>


				<div class="caption">
					<h4><?php echo $pageImage['caption']; ?></h4>
					<p><?php echo $pageImage['description']; ?></p>
				</div>

			</div>

		</div>

		<?php if ($counter % $imagesPerRow == 0) { ?>

			</div>

		<?php }

		$counter++;

	}

if ($counter % $imagesPerRow !== 0) { ?>

	</div>

<?php }
