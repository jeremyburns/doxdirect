<div class="turnaroundOptions index">

	<h2><?php echo __('Turnaround options'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('code'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('days'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>

			<tbody>

				<?php foreach ($turnaroundOptions as $turnaroundOption): ?>

					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($turnaroundOption['TurnaroundOption']['id']),
									array(
										'action' => 'view',
										$turnaroundOption['TurnaroundOption']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($turnaroundOption['TurnaroundOption']['name']),
									array(
										'action' => 'view',
										$turnaroundOption['TurnaroundOption']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($turnaroundOption['TurnaroundOption']['code']); ?></td>
						<td class="text-right"><?php echo h($turnaroundOption['TurnaroundOption']['days']); ?></td>
						<td><?php echo $this->Presentation->yesNo($turnaroundOption['TurnaroundOption']['is_active']); ?></td>
						<td><?php echo $this->Presentation->yesNo($turnaroundOption['TurnaroundOption']['is_default']); ?></td>
						<td><?php echo h($turnaroundOption['TurnaroundOption']['count_order']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $turnaroundOption['TurnaroundOption']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>

				<?php endforeach; ?>

			</tbody>

		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>