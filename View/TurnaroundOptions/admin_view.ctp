<div class="turnaroundOptions view">

	<h2><?php echo __('Turnaround option'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($turnaroundOption['TurnaroundOption']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($turnaroundOption['TurnaroundOption']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Code'); ?></th>
			<td><?php echo h($turnaroundOption['TurnaroundOption']['code']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Days'); ?></th>
			<td><?php echo h($turnaroundOption['TurnaroundOption']['days']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Default'); ?></th>
			<td><?php echo $this->Presentation->yesNo($turnaroundOption['TurnaroundOption']['is_default']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($turnaroundOption['TurnaroundOption']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Orders'); ?></th>
			<td><?php echo h($turnaroundOption['TurnaroundOption']['count_order']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($turnaroundOption['TurnaroundOption']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($turnaroundOption['TurnaroundOption']['modified'])); ?></td>
		</tr>
	</table>

</div>

<div class="actions">
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('List turnaround options'), array('action' => 'index'));
		?>
	</div>
</div>
