<div class="container">

	<div class="row">

		<div class="col col-sm-20 col-sm-offset-2 col-md-12 col-md-offset-6">

			<div class="user_addresses index">

				<?php echo $this->element(
					'user_addresses/add',
					[
						'countries' => $countries,
						'result' => $result
					]
				); ?>

			</div>

		</div>

	</div>

</div>
