<div class="container">

	<div class="row">

		<div class="col col-sm-20 col-sm-offset-2 col-md-12 col-md-offset-6">

			<div class="user_addresses index">

				<h3>These are the addresses we have on record for you</h3>

				<?php echo $this->element(
					'user_addresses/list',
					['userAddresses' => $userAddresses]
				); ?>

			</div>

		</div>

	</div>

</div>
