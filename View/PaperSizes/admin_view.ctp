<div class="paperSizes view">

	<div class="page-header">
		<h1>Paper size</h1>
		<?php echo $this->Flash->render(); ?>
	</div>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($paperSize['PaperSize']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Size rank'); ?></th>
			<td><?php echo h($paperSize['PaperSize']['size_rank']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Width (mm)'); ?></th>
			<td><?php echo h($paperSize['PaperSize']['width']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Height (mm)'); ?></th>
			<td><?php echo h($paperSize['PaperSize']['height']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Width (points)'); ?></th>
			<td><?php echo h($paperSize['PaperSize']['width_points']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Height (points)'); ?></th>
			<td><?php echo h($paperSize['PaperSize']['height_points']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Default'); ?></th>
			<td><?php echo $this->Presentation->yesNo($paperSize['PaperSize']['is_default']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($paperSize['PaperSize']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Order items'); ?></th>
			<td><?php echo h($paperSize['PaperSize']['count_order_item']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Order item files'); ?></th>
			<td><?php echo h($paperSize['PaperSize']['count_order_item_file']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($paperSize['PaperSize']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($paperSize['PaperSize']['modified'])); ?></td>
		</tr>
	</table>

</div>

<div class="actions">
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('List paper sizes'), array('action' => 'index'));
		?>
	</div>
</div>
