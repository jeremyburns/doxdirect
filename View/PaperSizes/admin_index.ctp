<div class="paperSizes index">

	<div class="page-header">
		<h1>Paper sizes</h1>
		<?php echo $this->Flash->render(); ?>
	</div>

	<div class="table-responsive">

		<table class="table table-condensed">

			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('size_rank'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('width', 'Width (mm)'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('height', 'Height (mm)'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('width_points', 'Width (pts)'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('height_points', 'Height (pts)'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item'); ?></th>
					<th><?php echo $this->Paginator->sort('count_order_item_file'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($paperSizes as $paperSize): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($paperSize['PaperSize']['id']),
									array(
										'action' => 'view',
										$paperSize['PaperSize']['id']
									)
								);
							?>
						</td>
						<td class="text-right"><?php echo h($paperSize['PaperSize']['size_rank']); ?></td>
						<td class="text-right"><?php echo h($paperSize['PaperSize']['width']); ?></td>
						<td class="text-right"><?php echo h($paperSize['PaperSize']['height']); ?></td>
						<td class="text-right"><?php echo h($paperSize['PaperSize']['width_points']); ?></td>
						<td class="text-right"><?php echo h($paperSize['PaperSize']['height_points']); ?></td>
						<td class="text-center"><?php echo $this->Presentation->yesNo($paperSize['PaperSize']['is_default']); ?></td>
						<td class="text-center"><?php echo $this->Presentation->yesNo($paperSize['PaperSize']['is_active']); ?></td>
						<td><?php echo h($paperSize['PaperSize']['count_order_item']); ?></td>
						<td><?php echo h($paperSize['PaperSize']['count_order_item_file']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $paperSize['PaperSize']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $paperSize['PaperSize']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $paperSize['PaperSize']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $paperSize['PaperSize']['id']));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<div class="btn-group">
			<?php echo $this->Html->link(__('New paper size'), array('action' => 'add')); ?>
		</div>
	</div>

</div>
