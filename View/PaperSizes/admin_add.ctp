<div class="page-header">
	<h1>Add a paper size</h1>
	<?php echo $this->Flash->render(); ?>
</div>

<div class="paperSizes form">

	<?php echo $this->Form->create(
		'PaperSize',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	);

		echo $this->Form->inputs([
			'legend' => false,
			'id' => ['type' => 'text'],
			'size_rank',
			'width',
			'height',
			'width_points',
			'height_points',
			'is_default',
			'is_active',
			'is_a_size'
		]);

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php echo $this->Html->link(__('List paper sizes'), array('action' => 'index')); ?>
	</div>
</div>
