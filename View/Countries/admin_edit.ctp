<div class="countries form">
	<?php echo $this->Form->create(
		'Country',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Edit country'); ?></legend>
		<?php
			echo $this->Form->input('id', ['type' => 'text']);
			echo $this->Form->input('name');
			echo $this->Form->input('iso_code');
			echo $this->Form->input('tier');
			echo $this->Form->input('is_default');
			echo $this->Form->input('is_active');
		?>
	</fieldset>
	<?php

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Form->postLink(
				__('Delete'),
				array(
					'action' => 'delete',
					$this->Form->value('Country.id')
				),
				array(),
				__('Are you sure you want to delete # %s?', $this->Form->value('Country.name'))
			);
			echo $this->Html->link(__('List countries'), array('action' => 'index'));
		?>
	</div>
</div>
