<div class="countries form">
	<?php echo $this->Form->create(
		'Country',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Add country'); ?></legend>
		<?php
			echo $this->Form->input('id', ['type' => 'text']);
			echo $this->Form->input('name');
			echo $this->Form->input('iso_code');
			echo $this->Form->input('tier');
			echo $this->Form->input('is_default');
			echo $this->Form->input('is_active');
		?>
	</fieldset>
	<?php

		echo $this->element(
			'navigation/admin_actions',
			[
				'controllerName' => $this->params->controller,
				'itemNameField' => 'name',
				'show' => [
					'save',
					'index'
				]
			]
		);

	echo $this->Form->end();

?>
</div>
