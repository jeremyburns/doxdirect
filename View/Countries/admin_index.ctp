<div class="countries index">

	<h2><?php echo __('Countries'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('iso_code'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_user_addresses', 'Addresses'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('tier'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($countries as $country): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($country['Country']['id']),
									array(
										'action' => 'view',
										$country['Country']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($country['Country']['name']),
									array(
										'action' => 'view',
										$country['Country']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($country['Country']['iso_code']); ?></td>
						<td class="text-right"><?php echo h($country['Country']['count_user_address']); ?></td>
						<td class="text-right"><?php echo $country['Country']['tier']; ?></td>
						<td class="text-center"><?php echo $this->Presentation->yesNo($country['Country']['is_default']); ?></td>
						<td class="text-center"><?php echo $this->Presentation->yesNo($country['Country']['is_active']); ?></td>
						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $country['Country']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $country['Country']['id']), array('class' => 'btn btn-xs btn-default'));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>


<?php echo $this->element(
	'navigation/admin_actions',
	[
		'controllerName' => $this->params->controller,
		'itemNameField' => 'name',
		'show' => [
			'add'
		]
	]
);
