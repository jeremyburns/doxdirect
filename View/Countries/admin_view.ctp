<div class="countries view">

	<h2><?php echo __('Country'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($country['Country']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($country['Country']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('ISO Code'); ?></th>
			<td><?php echo h($country['Country']['iso_code']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Address count'); ?></th>
			<td><?php echo h($country['Country']['count_user_address']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Tier'); ?></th>
			<td><?php echo $country['Country']['tier']; ?></td>
		</tr>
		<tr>
			<th><?php echo __('Is default'); ?></th>
			<td><?php echo $this->Presentation->yesNo($country['Country']['is_default']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Is active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($country['Country']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($country['Country']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($country['Country']['modified'])); ?></td>
		</tr>
	</table>

</div>

<?php echo $this->element(
	'navigation/admin_actions',
	[
		'data' => $country['Country'],
		'controllerName' => $this->params->controller,
		'itemNameField' => 'name',
		'show' => [
			'add',
			'delete',
			'edit',
			'index'
		]
	]
);
