<div class="jobStatuses index">

	<div class="page-header">
		<h1>Job statuses</h1>
		<?php echo $this->Flash->render(); ?>
	</div>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th>id</th>
					<th>Icon</th>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('sequence'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('is_default'); ?></th>
					<th><?php echo $this->Paginator->sort('description'); ?></th>
					<th class="text-right"><?php echo $this->Paginator->sort('count_order', 'Orders'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (empty($jobStatuses)) { ?>

					<tr class="info">

						<td colspan="9">There are no job statuses set up.</td>
					</tr>

				<?php } else {

					foreach ($jobStatuses as $jobStatus) { ?>

						<tr>
							<td><?php echo h($jobStatus['JobStatus']['id']); ?></td>
							<td><i class="fa <?php echo h($jobStatus['JobStatus']['icon']); ?>"></i></td>
							<td>
								<?php
									echo $this->Html->link(
										h($jobStatus['JobStatus']['id']),
										array(
											'action' => 'view',
											$jobStatus['JobStatus']['id']
										)
									);
								?>
							</td>
							<td>
								<?php
									echo $this->Html->link(
										h($jobStatus['JobStatus']['name']),
										array(
											'action' => 'view',
											$jobStatus['JobStatus']['id']
										),
										array(
											'escape' => false
										)
									);
								?>
							</td>
							<td><?php echo h($jobStatus['JobStatus']['sequence']); ?></td>
							<td class="text-center"><?php echo $this->Presentation->yesNo($jobStatus['JobStatus']['is_default']); ?></td>
							<td><?php echo h($jobStatus['JobStatus']['description']); ?></td>
							<td class="text-right"><?php echo h($jobStatus['JobStatus']['count_order']); ?></td>

							<td class="actions">
								<div class="btn-group">
									<?php
										echo $this->Html->link(__('View'), array('action' => 'view', $jobStatus['JobStatus']['id']), array('class' => 'btn btn-xs btn-default'));
										echo $this->Html->link(__('Edit'), array('action' => 'edit', $jobStatus['JobStatus']['id']), array('class' => 'btn btn-xs btn-default'));
										echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $jobStatus['JobStatus']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $jobStatus['JobStatus']['id']));
									?>
								</div>
							</td>
						</tr>

					<?php }

				} ?>

			</tbody>

		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

	<?php echo $this->element(
		'navigation/admin_actions',
		[
			'controllerName' => $this->params->controller,
			'itemNameField' => 'name',
			'show' => [
				'add'
			]
		]
	); ?>

</div>
