<div class="page-header">
	<h1>Add an job status</h1>
	<?php echo $this->Flash->render(); ?>
</div>

<div class="orderStatuses form">

	<?php
		echo $this->Form->create(
			'JobStatus',
			array(
				'inputDefaults' => array(
					'div' => 'form-group',
					'class' => 'form-control'
				),
				'role' => 'form'
			)
		);

			echo $this->Form->inputs([
				'legend' => false,
				'id' => ['type' => 'text'],
				'name',
				'sequence',
				'is_default',
				'icon',
				'description'
			]);

			echo $this->element(
				'navigation/admin_actions',
				[
					'controllerName' => $this->params->controller,
					'itemNameField' => 'name',
					'show' => [
						'index',
						'save'
					]
				]
			);

		echo $this->Form->end();

	?>
</div>
