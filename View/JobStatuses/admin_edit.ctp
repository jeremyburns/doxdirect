<div class="page-header">
	<h1>Edit an job status</h1>
	<?php echo $this->Flash->render(); ?>
</div>

<div class="orderStatuses form">
	<?php echo $this->Form->create(
		'JobStatus',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>

		<legend><?php echo __('Edit job status'); ?></legend>
		<?php
			echo $this->Form->inputs([
				'legend' => false,
				'id' => ['type' => 'text'],
				'name',
				'sequence',
				'is_default',
				'icon',
				'description'
			]);

			echo $this->element(
				'navigation/admin_actions',
				[
					'data' => $this->request->data['JobStatus'],
					'controllerName' => $this->params->controller,
					'itemNameField' => 'name',
					'show' => [
						'delete',
						'view',
						'index',
						'save'
					]
				]
			);

		echo $this->Form->end();

	?>
</div>
