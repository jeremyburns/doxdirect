<div class="page-header">
	<h1>Order status</h1>
	<?php echo $this->Flash->render(); ?>
</div>

<div class="jobStatus view">

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($jobStatus['JobStatus']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($jobStatus['JobStatus']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Sequence'); ?></th>
			<td><?php echo h($jobStatus['JobStatus']['sequence']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Default'); ?></th>
			<td><?php echo $this->Presentation->yesNo($jobStatus['JobStatus']['is_default']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Icon'); ?></th>
			<td><i class="fa <?php echo h($jobStatus['JobStatus']['icon']); ?>"></i></td>
		</tr>
		<tr>
			<th><?php echo __('Description'); ?></th>
			<td><?php echo h($jobStatus['JobStatus']['description']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Order count'); ?></th>
			<td><?php echo h($jobStatus['JobStatus']['count_order']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($jobStatus['JobStatus']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($jobStatus['JobStatus']['modified'])); ?></td>
		</tr>
	</table>

</div>

<?php

	echo $this->element(
		'navigation/admin_actions',
		[
			'data' => $jobStatus['JobStatus'],
			'controllerName' => $this->params->controller,
			'itemNameField' => 'name',
			'show' => [
				'add',
				'delete',
				'edit',
				'index'
			]
		]
	);
