<div class="configurations form">
	<?php echo $this->Form->create(
		'Configuration',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>

		<legend><?php echo __('Edit configuration'); ?></legend>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('category');
			echo $this->Form->input('code');
			echo $this->Form->input('name', array('label' => 'Value'));
			echo $this->Form->input('description', array('type' => 'textarea'));
			echo $this->Form->input(
				'is_active',
				array(
					'label' => 'Is active',
					'type' => 'checkbox'
				)
			);

		echo $this->element(
			'navigation/admin_actions',
			[
				'data' => $this->request->data['Configuration'],
				'controllerName' => $this->params->controller,
				'itemNameField' => 'name',
				'show' => [
					'delete',
					'view',
					'index',
					'save'
				]
			]
		);

	echo $this->Form->end();

?>
</div>

