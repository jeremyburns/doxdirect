<div class="configurations index">

	<h2><?php echo __('Configurations'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('category'); ?></th>
					<th><?php echo $this->Paginator->sort('code'); ?></th>
					<th><?php echo $this->Paginator->sort('name', 'Value'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($configurations as $configuration): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($configuration['Configuration']['id']),
									array(
										'action' => 'view',
										$configuration['Configuration']['id']
									)
								);
							?>
						</td>
						<td><?php echo h($configuration['Configuration']['category']); ?></td>
						<td>
							<?php
								echo $this->Html->link(
									h($configuration['Configuration']['code']),
									array(
										'action' => 'view',
										$configuration['Configuration']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($configuration['Configuration']['name']),
									array(
										'action' => 'view',
										$configuration['Configuration']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo $this->Presentation->yesNo($configuration['Configuration']['is_active']); ?></td>

						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $configuration['Configuration']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $configuration['Configuration']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $configuration['Configuration']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $configuration['Configuration']['id']));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

</div>

<?php echo $this->element(
	'navigation/admin_actions',
	[
		'controllerName' => $this->params->controller,
		'itemNameField' => 'name',
		'show' => [
			'add'
		]
	]
);
