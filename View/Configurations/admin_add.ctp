<div class="configurations form">
	<?php echo $this->Form->create(
		'Configuration',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>

		<legend><?php echo __('Add configuration'); ?></legend>
		<?php
			echo $this->Form->inputs([
				'category',
				'code',
				'name' => ['label' => 'Value'],
				'description' => ['type' => 'textarea'],
				'is_active'
			]);

			echo $this->element(
				'navigation/admin_actions',
				[
					'controllerName' => $this->params->controller,
					'itemNameField' => 'name',
					'show' => [
						'index',
						'save'
					]
				]
			);

		echo $this->Form->end();

	?>
</div>
