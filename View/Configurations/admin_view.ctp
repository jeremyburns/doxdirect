<div class="configurations view">

	<h2><?php echo __('Configuration'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($configuration['Configuration']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Category'); ?></th>
			<td><?php echo h($configuration['Configuration']['category']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Code'); ?></th>
			<td><?php echo h($configuration['Configuration']['code']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Value'); ?></th>
			<td><?php echo h($configuration['Configuration']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Description'); ?></th>
			<td><?php echo h($configuration['Configuration']['description']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Is active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($configuration['Configuration']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($configuration['Configuration']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($configuration['Configuration']['modified'])); ?></td>
		</tr>
	</table>

</div>

<?php

	echo $this->element(
		'navigation/admin_actions',
		[
			'data' => $configuration['Configuration'],
			'controllerName' => $this->params->controller,
			'itemNameField' => 'code',
			'show' => [
				'add',
				'delete',
				'edit',
				'index'
			]
		]
	);
