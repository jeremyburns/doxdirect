<div class="holidayAlerts index">

	<h2><?php echo __('Holiday alerts'); ?></h2>

	<div class="table-responsive">

		<table class="table table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('heading'); ?></th>
					<th><?php echo $this->Paginator->sort('text'); ?></th>
					<th><?php echo $this->Paginator->sort('display_from_day'); ?></th>
					<th><?php echo $this->Paginator->sort('display_from_month'); ?></th>
					<th><?php echo $this->Paginator->sort('display_from_year'); ?></th>
					<th><?php echo $this->Paginator->sort('display_to_day'); ?></th>
					<th><?php echo $this->Paginator->sort('display_to_month'); ?></th>
					<th><?php echo $this->Paginator->sort('display_to_year'); ?></th>
					<th><?php echo $this->Paginator->sort('display_from_date'); ?></th>
					<th><?php echo $this->Paginator->sort('display_to_date'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($holidayAlerts as $holidayAlert): ?>
					<tr>
						<td>
							<?php
								echo $this->Html->link(
									h($holidayAlert['HolidayAlert']['id']),
									array(
										'action' => 'view',
										$holidayAlert['HolidayAlert']['id']
									)
								);
							?>
						</td>
						<td>
							<?php
								echo $this->Html->link(
									h($holidayAlert['HolidayAlert']['name']),
									array(
										'action' => 'view',
										$holidayAlert['HolidayAlert']['id']
									),
									array(
										'escape' => false
									)
								);
							?>
						</td>
						<td><?php echo h($holidayAlert['HolidayAlert']['heading']); ?></td>
						<td><?php echo h($holidayAlert['HolidayAlert']['text']); ?></td>
						<td><?php echo h($holidayAlert['HolidayAlert']['display_from_day']); ?></td>
						<td><?php echo h($holidayAlert['HolidayAlert']['display_from_month']); ?></td>
						<td><?php echo h($holidayAlert['HolidayAlert']['display_from_year']); ?></td>
						<td><?php echo h($holidayAlert['HolidayAlert']['display_to_day']); ?></td>
						<td><?php echo h($holidayAlert['HolidayAlert']['display_to_month']); ?></td>
						<td><?php echo h($holidayAlert['HolidayAlert']['display_to_year']); ?></td>
						<td><?php echo $this->Presentation->niceDate($holidayAlert['HolidayAlert']['display_from_date'], false); ?></td>
						<td><?php echo $this->Presentation->niceDate($holidayAlert['HolidayAlert']['display_to_date'], false); ?></td>
						<td><?php echo $this->Presentation->yesNo($holidayAlert['HolidayAlert']['is_active']); ?></td>
						<td class="actions">
							<div class="btn-group">
								<?php
									echo $this->Html->link(__('View'), array('action' => 'view', $holidayAlert['HolidayAlert']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Html->link(__('Edit'), array('action' => 'edit', $holidayAlert['HolidayAlert']['id']), array('class' => 'btn btn-xs btn-default'));
									echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $holidayAlert['HolidayAlert']['id']), array('class' => 'btn btn-xs btn-default'), __('Are you sure you want to delete # %s?', $holidayAlert['HolidayAlert']['id']));
								?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>

	<?php echo $this->element('navigation/pagination'); ?>

	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<div class="btn-group">
			<?php echo $this->Html->link(__('New holiday alert'), array('action' => 'add')); ?>
		</div>
	</div>

</div>
