<div class="holidayAlerts form">
	<?php echo $this->Form->create(
		'HolidayAlert',
		array(
			'inputDefaults' => array(
				'div' => 'form-group',
				'class' => 'form-control'
			),
			'role' => 'form'
		)
	); ?>
	<fieldset>
		<legend><?php echo __('Edit holiday alert'); ?></legend>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('name');
			echo $this->Form->input('heading');
			echo $this->Form->input('text');
			echo $this->Form->input('display_from_day');
			echo $this->Form->input('display_from_month');
			echo $this->Form->input('display_from_year');
			echo $this->Form->input('display_to_day');
			echo $this->Form->input('display_to_month');
			echo $this->Form->input('display_to_year');
			echo $this->Form->input('display_from_date');
			echo $this->Form->input('display_to_date');
			echo $this->Form->input('is_active');
		?>
	</fieldset>
	<?php

		echo $this->Form->button(
			__('Save'),
			array(
				'class' => 'btn btn-default btn-success'
			)
		);

	echo $this->Form->end();

?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Form->postLink(
				__('Delete'),
				array(
					'action' => 'delete',
					$this->Form->value('HolidayAlert.id')
				),
				array(),
				__('Are you sure you want to delete # %s?', $this->Form->value('HolidayAlert.id'))
			);
			echo $this->Html->link(__('List holiday alerts'), array('action' => 'index'));
		?>
	</div>
</div>
