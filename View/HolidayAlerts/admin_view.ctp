<div class="holidayAlerts view">

	<h2><?php echo __('Holiday alert'); ?></h2>

	<table class="table table-condensed">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Name'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['name']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Heading'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['heading']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Text'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['text']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Display from day'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['display_from_day']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Display from month'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['display_from_month']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Display from year'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['display_from_year']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Display to day'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['display_to_day']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Display to month'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['display_to_month']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Display to year'); ?></th>
			<td><?php echo h($holidayAlert['HolidayAlert']['display_to_year']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Display from date'); ?></th>
			<td><?php echo $this->Presentation->niceDate($holidayAlert['HolidayAlert']['display_from_date'], false); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Display to date'); ?></th>
			<td><?php echo $this->Presentation->niceDate($holidayAlert['HolidayAlert']['display_to_date'], false); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Active'); ?></th>
			<td><?php echo $this->Presentation->yesNo($holidayAlert['HolidayAlert']['is_active']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($this->Time->niceShort($holidayAlert['HolidayAlert']['created'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($this->Time->niceShort($holidayAlert['HolidayAlert']['modified'])); ?></td>
		</tr>
	</table>

</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<div class="btn-group">
		<?php
			echo $this->Html->link(__('Edit holiday alert'), array('action' => 'edit', $holidayAlert['HolidayAlert']['id']));
			echo $this->Form->postLink(__('Delete holiday alert'), array('action' => 'delete', $holidayAlert['HolidayAlert']['id']), array(), __('Are you sure you want to delete # %s?', $holidayAlert['HolidayAlert']['id']));
			echo $this->Html->link(__('List holiday alerts'), array('action' => 'index'));
			echo $this->Html->link(__('New holiday alert'), array('action' => 'add'));
		?>
	</div>
</div>
