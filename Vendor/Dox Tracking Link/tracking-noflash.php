<head>
<style>

body {
	margin-top:10px;
	margin-bottom:0px;
	margin-left:0px;
	margin-right:0px;
	padding:0px;
	background-color:#ffffff;
	align:center;
}

#footer {
	background-image:url(images/tracking_footer.gif);
	background-repeat:repeat-x;
	background-position:bottom;
	height:100px;
}

table {
	border-style: solid;
	border-width: 1px;
	border-color: #252282;
}
table.noborder {
	border-style: none;
}
td.right {
	border-right-style: dotted;
	border-right-width: 1px;
	border-right-color: #252282;
}
td.rightbottom {
	border-right-style: dotted;
	border-right-width: 1px;
	border-right-color: #252282;
	border-bottom-style: dotted;
	border-bottom-width: 1px;
	border-bottom-color: #252282;
}
td.bottom {
	border-bottom-style: dotted;
	border-bottom-width: 1px;
	border-bottom-color: #252282;
}
</style>
<title>Track your DoxBox</title>
    <script type="text/javascript" charset="utf-8" src="doxdirect-ups_animation_edgePreload.js"></script>
    <style>
        .edgeLoad-EDGE-25410347 { visibility:hidden; }
		#Stage { height:100px; }
		#Poster { margin-left:auto; margin-right:auto;}
    </style>
</head>

<body>
<div align="center">

<div id="Stage" class="EDGE-25410347">
</div>

     <!-- begin main content -->
     <?php
//////////// begin tracking script "track"  //////////// 
 if($_POST['action'] == ""){
$userid_pass = "simon21";  /// The username and password from UPS (username and password are the same)
$access_key = "6C3A39F497E8D6EE";  //// license key from UPS
$upsURL = "https://wwwcie.ups.com/ups.app/xml/Track"; /// This will be provided to you by UPS
$activity = "activity"; /// UPS activity code

///// The below variable is the query string to be posted. /////
$y = "<?xml version=\"1.0\"?><AccessRequest xml:lang=\"en-US\"><AccessLicenseNumber>".$access_key."</AccessLicenseNumber><UserId>".$userid_pass."</UserId><Password>".$userid_pass."</Password></AccessRequest><?xml version=\"1.0\"?><TrackRequest xml:lang=\"en-US\"><Request><TransactionReference><CustomerContext>Example 1</CustomerContext><XpciVersion>1.0001</XpciVersion></TransactionReference><RequestAction>Track</RequestAction><RequestOption>".$activity."</RequestOption></Request><TrackingNumber>".$tracking_number."</TrackingNumber></TrackRequest>";
//////
 /////////////////// begin the cURL engine /////////////////////
///////////////////////////////////////////////////////////////
//////////////////// What a powerful utility! /////////////////
///////////////////////////////////////////////////////////////
 $ch = curl_init(); /// initialize a cURL session
 curl_setopt ($ch, CURLOPT_URL,$upsURL); /// set the post-to url (do not include the ?query+string here!)
 curl_setopt ($ch, CURLOPT_HEADER, 0); /// Header control
 curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, TRUE);/// Use this to prevent PHP from verifying the host (later versions of PHP including 5)
 /// If the script you were using with cURL has stopped working. Likely adding the line above will solve it.
 curl_setopt($ch, CURLOPT_POST, 1);  /// tell it to make a POST, not a GET
 curl_setopt($ch, CURLOPT_POSTFIELDS, $y);  /// put the query string here starting with "?" 
 curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); /// This allows the output to be set into a variable $xyz
 $upsResponse = curl_exec ($ch); /// execute the curl session and return the output to a variable $xyz
 curl_close ($ch); /// close the curl session
////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
///////////////////  end the cURL Engine  /////////////////
 
 
 //////////// begin xml parser Class function ////////////
/////////////////////////////////////////////
/////// class function taken from http://www.hansanderson.com/php/xml/class.xml.php.txt

 class xml_container {
 
 	function store($k,$v) {
 		$this->{$k}[] = $v;
 	}
 
 }
 class xml { 
 
 	var $current_tag=array();
 	var $xml_parser;
 	var $Version = 1.0;
 	var $tagtracker = array();
 
 	function startElement($parser, $name, $attrs) {
 
 		array_push($this->current_tag, $name);
 
 		$curtag = implode("_",$this->current_tag);
 
 		if(isset($this->tagtracker["$curtag"])) {
 			$this->tagtracker["$curtag"]++;
 		} else {
 			$this->tagtracker["$curtag"]=0;
 		}
 
 
 		if(count($attrs)>0) {
 			$j = $this->tagtracker["$curtag"];
 			if(!$j) $j = 0;
 
 			if(!is_object($GLOBALS[$this->identifier]["$curtag"][$j])) {
 				$GLOBALS[$this->identifier]["$curtag"][$j] = new xml_container;
 			}
 
 			$GLOBALS[$this->identifier]["$curtag"][$j]->store("attributes",$attrs);
                 }
 
 	} // end function startElement
 
 
 
 	/* when expat hits a closing tag, it fires up this function */
 
 	function endElement($parser, $name) {
 
 		$curtag = implode("_",$this->current_tag); 	// piece together tag
 								// before we pop it off,
 								// so we can get the correct
 								// cdata
 
 		if(!$this->tagdata["$curtag"]) {
 			$popped = array_pop($this->current_tag); // or else we screw up where we are
 			return; 	// if we have no data for the tag
 		} else {
 			$TD = $this->tagdata["$curtag"];
 			unset($this->tagdata["$curtag"]);
 		}
 
 		$popped = array_pop($this->current_tag);
 								// we want the tag name for
 								// the tag above this, it 
 								// allows us to group the
 								// tags together in a more
 								// intuitive way.
 
 		if(sizeof($this->current_tag) == 0) return; 	// if we aren't in a tag
 
 		$curtag = implode("_",$this->current_tag); 	// piece together tag
 								// this time for the arrays
 
 		$j = $this->tagtracker["$curtag"];
 		if(!$j) $j = 0;
 
 		if(!is_object($GLOBALS[$this->identifier]["$curtag"][$j])) {
 			$GLOBALS[$this->identifier]["$curtag"][$j] = new xml_container;
 		}
 
 		$GLOBALS[$this->identifier]["$curtag"][$j]->store($name,$TD); #$this->tagdata["$curtag"]);
 		unset($TD);
 		return TRUE;
 	}
 
 
 
 	/* when expat finds some internal tag character data,
 	   it fires up this function */
 
 	function characterData($parser, $cdata) {
 		$curtag = implode("_",$this->current_tag); // piece together tag		
 		$this->tagdata["$curtag"] .= $cdata;
 	}
 
 
 	/* this is the constructor: automatically called when the class is initialized */
 
 	function xml($data,$identifier='xml') {  
 
 		$this->identifier = $identifier;
 
 		// create parser object
 		$this->xml_parser = xml_parser_create();
 
 		// set up some options and handlers
 		xml_set_object($this->xml_parser,$this);
 		xml_parser_set_option($this->xml_parser,XML_OPTION_CASE_FOLDING,0);
 		xml_set_element_handler($this->xml_parser, "startElement", "endElement");
 		xml_set_character_data_handler($this->xml_parser, "characterData");
 
 		if (!xml_parse($this->xml_parser, $data, TRUE)) {
 			sprintf("XML error: %s at line %d",
 			xml_error_string(xml_get_error_code($this->xml_parser)),
 			xml_get_current_line_number($this->xml_parser));
 		}
 
 		// we are done with the parser, so let's free it
 		xml_parser_free($this->xml_parser);
 
 	}  // end constructor: function xml()
 
 
 } // thus, we end our class xml
///////////////////////////////////////////////////
/////////// end XML Class  //////////////////////// 

$obj = new xml($upsResponse,"xml"); /// create the object
$nine = trim($xml["TrackResponse_Response"][0]->ResponseStatusCode[0]);
////////////////////////
if($nine == "1"){
			$tracknumber = $xml["TrackResponse_Shipment_Package"][0]->TrackingNumber[0] . "\n";
			$pieces = $xml["TrackResponse_Shipment"][0]->NumberOfPieces[0] . "\n";
			$seven = $xml["TrackResponse_Shipment_ShipTo_Address"][0]->AddressLine1[0] . "\n";
			$six = $xml["TrackResponse_Shipment_ShipTo_Address"][0]->AddressLine2[0] . "\n";
			$five = $xml["TrackResponse_Shipment_ShipTo_Address"][0]->City[0] . "\n";
			$four = $xml["TrackResponse_Shipment_ShipTo_Address"][0]->StateProvinceCode[0] . "\n";
			$three = $xml["TrackResponse_Shipment_ShipTo_Address"][0]->PostalCode[0] . "\n";
			$two = $xml["TrackResponse_Shipment_ShipTo_Address"][0]->CountryCode[0] . "\n";
			$twelve = $xml["TrackResponse_Shipment_Package_PackageWeight_UnitOfMeasurement"][0]->Code[0] . "\n";
			$eleven = $xml["TrackResponse_Shipment_Package_PackageWeight"][0]->Weight[0] . "\n";
			$thirteen = $xml["TrackResponse_Shipment_Service"][0]->Description[0] . "\n";
///current location
			$fourteen = $xml["TrackResponse_Shipment_Package_Activity_ActivityLocation"][0]->Description[0] . "\n";
			$eighteen = $xml["TrackResponse_Shipment_Package_Activity_ActivityLocation_Address"][0]->City[0] . "\n";
			$nineteen = $xml["TrackResponse_Shipment_Package_Activity_ActivityLocation_Address"][0]->CountryCode[0] . "\n";
			$twenty = $xml["TrackResponse_Shipment_Package_Activity_ActivityLocation_Address"][0]->StateProvinceCode[0] . "\n";
			$fifteen = $xml["TrackResponse_Shipment_Package_Activity_ActivityLocation"][0]->SignedForByName[0] . "\n";
// end location
			$sixteen = $xml["TrackResponse_Shipment_Package_Activity_Status_StatusType"][0]->Description[0] . "\n";
			$seventeen = $xml["TrackResponse_Shipment_Package_Activity_Status_StatusType"][0]->Code[0] . "\n";
			$twentyfour = $xml["TrackResponse_Shipment_Package_Activity"][0]->Date[0] . "\n";
			$twentyfive = $xml["TrackResponse_Shipment_Package_Activity"][0]->Time[0] . "\n";
$yearx = substr("$twentyfour", 0, 4);
$monthx = substr("$twentyfour", 4, 2);
$dayx = substr("$twentyfour", 6, 2); 
$hhx = substr("$twentyfive", 0, 2);
$mmx = substr("$twentyfive", 2, 2);
$ssx = substr("$twentyfive", 4, 2);
$seventeen = trim($seventeen);
switch($seventeen){
case I:
$stat = "In transit";
BREAK;
case D:
$stat = "Delivered";
BREAK;
case X:
$stat = "Exception";
BREAK;
case P:
$stat = "Pickup";
BREAK;
case M:
$stat = "Manifest Pickup";
BREAK;
}
?>
  </p>
</div>
<table width="556" border="0" align="center" cellpadding="5" cellspacing="0" id="main">
  <tr>
    <td bgcolor="#ffffff" class="right"><font color="#000000" size="2" face="Arial">Tracking Number:</font></td>
    <td bgcolor="#ffffff"><font color="#000000" size="2" face="Arial"><?php echo $tracknumber; ?></font></td>
  </tr>
  <tr>
    <td width="25%" bgcolor="#002e62"><font size="4" face="Arial" color="#ffffff"><strong>Status:</strong></font></td>
    <td bgcolor="#002e62"><b><font color="#ffffff" size="4" face="Arial"><?php echo $stat ?></font><font color="#ffffff" size="4" face="Arial"></font><font color="#ffffff" size="4" face="Arial"><?php if($seventeen == "D"){echo(", left at: $fourteen");} ?>
    </font></b></td>
  </tr>
  <tr>
    <td width="25%" bgcolor="#ffffff" class="rightbottom"><font face="Arial" size="2" color="#000000">Shipping Method:</font></td>
    <td bgcolor="#ffffff" class="bottom"><font face="Arial" size="2" color="#000000"><?php echo $thirteen ?></font></td>
  </tr>
  <tr>
    <td width="25%" bgcolor="#ffffff" class="rightbottom"><font face="Arial" size="2" color="#000000">Weight:</font></td>
    <td bgcolor="#ffffff" class="bottom"><font face="Arial" size="2" color="#000000"><?php echo "$eleven $twelve" ?></font></td>
  </tr>
  <tr>
    <td width="25%" bgcolor="#ffffff" class="rightbottom"><font face="Arial" size="2" color="#000000">Delivery date/time:</font></td>
    <td bgcolor="#ffffff" class="bottom"><font face="Arial" size="2" color="#000000"><?php echo "$dayx/$monthx/$yearx" ?> - <?php echo "$hhx:$mmx:$ssx" ?></font></td>
  </tr>
  <tr>
    <td width="25%" bgcolor="#ffffff" class="rightbottom"><font face="Arial" size="2" color="#000000">
      <?php if($stat == "I"){$sixteen = trim($sixteen);echo("$sixteen");}else{$sixteen = trim($sixteen);echo("$sixteen");} ?>
    </font></td>
    <td bgcolor="#ffffff" class="bottom"><font face="Arial" size="2" color="#000000">
      <?php if($eighteen){echo("$eighteen, ");}if($twenty){echo("$twenty ");}if($nineteen){echo("$nineteen");} ?>
    </font></td>
  </tr>
  <tr>
    <td bgcolor="#0097d7" class="right"><font face="Arial" size="2" color="#ffffff"><strong><?php if($seventeen == "D"){echo("Signed for by:");} ?>
    </strongb></font></td>
    <td bgcolor="#0097d7"><font face="Arial" size="2" color="#ffffff"><strong><?php if($seventeen == "D"){echo("$fifteen");} ?></strong></font></td>
  </tr>
</table>
<div align="center">
  <center>
  </center>
</div>


<div align="center">
  <?php
}else{
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
echo "<center><b><font face='Arial' color='#FF0000'>This shipment has not been despatched yet</font></b></center>";
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
}  /// end if nine is 1 or 0
/////////////////
/////////// end xml parser  /////
}    ///// end if action is track
?>
  <BR>
  <?php
if($nine == "1"){
$bgcolor = "#eeeeee";
echo("<center><b><font face=\"Arial\" color=\"#0097d7\" size=\"2\">Package History</b></center>
<font face=\"Arial\" size=\"1\"><br>
<table class=\"noborder\" border='0' align=\"center\" width=\"556\"><tr><td nowrap width='20%' bgcolor='#0097d7'><B><font face=\"Arial\" color=\"#ffffff\" size=\"1\">Date</font></b></td><td nowrap width='20%' bgcolor='#0097d7'><B><font face=\"Arial\" color=\"#ffffff\" size=\"1\">Time</font></b></td><td nowrap width='20%' bgcolor='#0097d7'><B><font face=\"Arial\" color=\"#ffffff\" size=\"1\">Location</font></b></td><td width='40%' width='20%' bgcolor='#0097d7'><B><font face=\"Arial\" color=\"#ffffff\" size=\"1\">Activity</font></b></td></tr>");
for($i=0;$i<count($xml["TrackResponse_Shipment_Package_Activity"]);$i++) {
			$twentyone = $xml["TrackResponse_Shipment_Package_Activity_Status_StatusType"][$i]->Description[0] . "\n";
			$twentytwo = $xml["TrackResponse_Shipment_Package_Activity"][$i]->Date[0] . "\n";
			$twentythree = $xml["TrackResponse_Shipment_Package_Activity"][$i]->Time[0] . "\n";
			$twentyfour = $xml["TrackResponse_Shipment_Package_Activity_ActivityLocation_Address"][$i]->City[0] . "\n";
			$twentyfive = $xml["TrackResponse_Shipment_Package_Activity_ActivityLocation_Address"][$i]->StateProvinceCode[0] . "\n";
			$twentysix = $xml["TrackResponse_Shipment_Package_Activity_ActivityLocation_Address"][$i]->CountryCode[0] . "\n";


$year = substr("$twentytwo", 0, 4);
$month = substr("$twentytwo", 4, 2);
$day = substr("$twentytwo", 6, 2); 

$hh = substr("$twentythree", 0, 2);
$mm = substr("$twentythree", 2, 2);
$ss = substr("$twentythree", 4, 2);
echo("<tr>");
if($xday != $day){

if($bgcolor == "#eeeeee"){
$bgcolor = "#dddddd";
///echo("E1E1E1");
}else{
$bgcolor = "#eeeeee";
///echo("EFEEED");
}

echo("<td nowrap width='20%' bgcolor='".$bgcolor."'><B><font face=\"Arial\" color=\"#000000\" size=\"1\">".$day."/".$month."/".$year."</font></b></td>");

}else{
echo("<td nowrap width='20%' bgcolor='".$bgcolor."'>&nbsp;</td>");
}

$xmonth = $month;
$xday = $day;
$xyear = $xyear;

echo("<td nowrap width='20%' bgcolor='".$bgcolor."'><font face=\"Arial\" color=\"#000000\" size=\"1\"><i>".$hh.":".$mm.":".$ss."</i></font></td><td nowrap width='20%' bgcolor='".$bgcolor."'><font face=\"Arial\" color=\"#000000\" size=\"1\">".$twentyfour." ".$twentyfive." ".$twentysix."</font></td><td width='40%' bgcolor='".$bgcolor."'><font face=\"Arial\" color=\"#000000\" size=\"1\">".$twentyone."</font></td></tr>");
}

echo("
</table>
</font>
<BR>");

echo("<table width=\"556\" class=\"noborder\";><tr><td><font face=\"Arial\" color=\"#333333\" size=\"1\">Results provided by UPS ".date("F j, Y, g:i a T").".<BR><BR>
<b>NOTICE:</b> UPS authorizes you to use UPS tracking systems solely to track shipments tendered by or for you to UPS for delivery and for no other purpose. Any other use of UPS tracking systems and information is strictly prohibited.
</font></td></tr></table>
");
}  /// end if 17 is X
 ?>
  <BR>
  <!-- end main content -->  
  </div>
  
  <div id="footer"></div>
</body>