<?php

	if (isset ($_POST['file']) && isset ($_POST['actions'])) {

		$file = base64_decode($_POST['file']);

		$actions = explode(',', $_POST['actions']);

		foreach ($actions as $action) {

			$targetPath = $action . '\\In\\' . $_POST['file_name'];

			file_put_contents(
				$targetPath,
				$file
			);

		}

	}