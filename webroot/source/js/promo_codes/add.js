// @codekit-prepend '../_includes/datepicker/bootstrap-datepicker.js';
$('.datepicker').datepicker({
	format: 'yyyy-mm-dd',
	todayBtn: "linked",
	clearBtn: true,
	autoclose: true,
	todayHighlight: true
})