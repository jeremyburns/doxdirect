/*global $ */

/*global document */

/*global clearMessage */

/*global showMessage */

/*global renderElement */

/*global clearElement */

// @codekit-prepend "../global/global_functions.js";

// @codekit-prepend "same_day_processing.js";

function toggleBillingAddress() {

	'use strict';

	var
		$checkbox = $('#OrderBillingSameAsShipping'),
		$billingAddress = $('#checkout-billing-address'),
		checked = $checkbox.prop('checked');

	if (checked) {
		$billingAddress.hide();
	} else {
		$billingAddress.show();
	}

}

function userNew() {
	$('[data-user=returning]').hide();
	$('[data-user=new]').show();
	$('[data-user=returned]').hide();
	$('[data-user=neutral]').show();
}

function editingUserDetails() {

	$('[data-user=returning]').hide();
	$('[data-user=new]').hide();
	$('[data-user=returned]').hide();
	$('[data-user=neutral]').hide();
	$('#checkout-ajax').show();
	$('[data-action=resume').removeClass('hide');

}

function userLoggingIn() {

	'use strict';

	var element = {};

	element.elementName = 'users/login';

	element.variables = {
		showCancelLink: true
	};

	renderElement(element, '#checkout-ajax');

	$('[data-user=returning]').show();
	$('[data-user=new]').hide();
	$('[data-user=returned]').hide();
	$('[data-user=neutral]').hide();


}

function userReturned() {

	clearElement('#checkout-ajax');
	$('[data-user=returning]').remove();
	$('[data-user=new]').remove();
	$('[data-user=returned]').show();
	$('[data-user=neutral]').show();
}

function refreshUserDetails() {

	'use strict';

	var returnedUserUrl = $('#returned-user-url').val();

	$.ajax({
		type: "GET",
		url: returnedUserUrl,
		dataType: 'json',
		cache: false,
		success: function (response) {

			$('*[data-content="login-link"]').html(response.loginLink);
			$('#user-details').html(response.userDetails);

			userReturned();

		},
		error: function (xhr, status, errorThrown) {
			console.log("Error: " + errorThrown);
			console.log("Status: " + status);
			console.dir(xhr);
		}

	});

}

$('#OrderBillingSameAsShipping').on(
	'change',
	function () {
		toggleBillingAddress();
	}
);

$(document).on(
	'click',
	'#checkout-returning-user-login-link',
	function(e) {

		e.preventDefault();

		userLoggingIn();

	}
);

$(document).on(
	'click',
	'#cancel-login-link',
	function(e) {

		e.preventDefault();

		clearMessage('#checkout-alert');
		clearElement('#checkout-ajax');

		userNew();

	}
);

function loginStart() {

	'use strict';

	$('#login-flash-message')
		.removeClass('alert-error')
		.html('');

	clearMessage('#checkout-alert');

	$('#login-btn').addClass('disabled');
	$('#login-btn i').addClass('fa-spin');

}

function loginStop() {

	$('#login-btn i').removeClass('fa-spin');
	$('#login-btn').removeClass('disabled');

}

//function refresh() {
//
//	'use strict';
//
//	var
//		form = $("#OrderCheckoutForm"),
//		action = form.attr("action");
//
//	$.ajax({
//		type: 'GET',
//		url: action,
//		dataType: 'html',
//		cache: false,
//		beforeSend: function () {
//
//		},
//		success: function (response) {
//
//
//		},
//		error: function (error) {
//		},
//		complete: function (response) {
//			$('#checkout-content').html(response);
//		}
//	});
//
//}

$(document).on(
	'click',
	'#change-my-details',
	function(e) {

		e.preventDefault();

		clearMessage('#checkout-alert');

		var changeMyDetailsUrl = $('#change-my-details-url').val();

		$.ajax({
			type: "GET",
			url: changeMyDetailsUrl,
			dataType: 'json',
			success: function (response) {

				$('#checkout-ajax')
					.html(response.html)
					.show()
				;

				editingUserDetails();

			},
			error: function (xhr, status, errorThrown) {
				console.log("Error: " + errorThrown);
				console.log("Status: " + status);
				console.dir(xhr);
			}
		});
	}
);

$(document).on(
	'submit',
	'#UserEditForm',
	function(e) {

		'use strict';

		e.preventDefault();

		clearMessage('#checkout-alert');

		var
			form = $(this),
			action = form.attr("action"),
			data = form.serialize();

		$.ajax({
			type: 'POST',
			url: action,
			data: data,
			dataType: 'json',
			cache: false,
			success: function (response) {
				refreshUserDetails();
//				if (response.success) {
//
//					showMessage(response.message, 'success', '#checkout-alert');
//
//					refreshUserDetails();
//
//				} else {
//
//					showMessage(response.message, 'error', '#checkout-alert');
//
//				}

			},
			error: function (xhr, status, errorThrown) {
				console.log("Error: " + errorThrown);
				console.log("Status: " + status);
				console.dir(xhr);
			}
		});

	}
);

$(document).on(
	'click',
	'[data-action="resume"]',
	function(e) {

		e.preventDefault();

		clearMessage('#checkout-alert');

//		refreshUserDetails();

		userReturned();
	}
);

$(document).on(
	'click',
	'#user-addresses-edit-link',
	function(e) {

		'use strict';

		e.preventDefault();

		var
			link = $(this),
			url;

		clearMessage('#checkout-alert');

		url = link.attr('href');

		$.ajax({
			type: "GET",
			url: url,
			dataType: 'json',
			cache: false,
			success: function (response) {
				console.log(response);
				$('#checkout-ajax').html(response.html);

				editingUserDetails();

			},
			error: function (xhr, status, errorThrown) {
				console.log("Error: " + errorThrown);
				console.log("Status: " + status);
				console.dir(xhr);
			}
		});

	}
);

$(document).on(
	"change",
	"*[data-check-turnaround='yes']",
	function () {
		// Enable or disable same day turnaround
		checkSameDayTurnaround();
	}
);

$(document).on(
	'click',
	'[data-user-address="edit"]',
	function(e) {

		'use strict';

		e.preventDefault();

		var
			link = $(this),
			url,
			userAddressId;

		clearMessage('#checkout-alert');

		url = link.attr('href');

		userAddressId = link.data('user-address-id');

		$.ajax({
			type: "GET",
			url: url,
			dataType: 'json',
			cache: false,
			success: function (response) {

				$('#checkout-form').html(response.html);

			},
			error: function (xhr, status, errorThrown) {
				console.log("Error: " + errorThrown);
				console.log("Status: " + status);
				console.dir(xhr);
			}
		});

	}
);

$(document).on(
	'click',
	'#user-address-finish-edit',
	function(e) {

		'use strict';

		e.preventDefault();

		userReturned();

	}

);

$(document).on(
	'click',
	'#checkput-promo-code-link',
	function(e) {

		e.preventDefault();

		$('#checkout-promo-code-input').show();
		$('#checkput-promo-code-link-text').remove();

	}
);

function login() {

	'use strict';

	clearMessage('#checkout-alert');

	var
		form = $("#LoginForm"),
		action = form.attr("action"),
		data = form.serialize();

	$.ajax({
		type: 'POST',
		url: action,
		data: data,
		dataType: 'json',
		cache: false,
		beforeSend: function () {
			loginStart();
		},
		success: function (response) {

			if (!response.success) {

				showMessage(response.message, 'error', '#checkout-alert');

				userLoggingIn();

			} else {

				showMessage(response.message, 'success', '#checkout-alert');

				refreshUserDetails();

				userReturned();

				clearElement('#checkout-ajax');

			}

		},
		error: function (xhr, status, errorThrown) {
			console.log("Error: " + errorThrown);
			console.log("Status: " + status);
			console.dir(xhr);
		},
		complete: function () {
			loginStop();
		}
	});

}

$(document).on(
	'submit',
	'#LoginForm',
	function(e) {

		e.preventDefault();

		clearMessage('#checkout-alert');

		login();

	}
);

function removePromoCode() {

	'use strict';

	clearMessage('#checkout-alert');

	var
		link = $('#remove-promo-code'),
		url;

	link.html('<i class="fa fa-circle-o-notch fa-spin"></i> Removing promo code...');

	url = link.attr('href');

	$.ajax({
		type: "GET",
		url: url,
		dataType: 'json',
		cache: false,
		success: function (response) {

			if (response.success) {

				showMessage(response.message, 'success', '#checkout-alert');

				$('#your-order-summary').html(response.html.order_summary);
				$('#checkout-promo-code').html(response.html.promo_code);

			} else {

				showMessage(response.message, 'error', '#checkout-alert');

			}

		},
		error: function (xhr, status, errorThrown) {
			console.log("Error: " + errorThrown);
			console.log("Status: " + status);
			console.dir(xhr);
		}
	});

}

function applyPromoCode() {

	'use strict';

	clearMessage('#promo-code-alert');

	var
		promoCodeInput = $('#PromoCodeCode'),
		promoCode = promoCodeInput.val(),
		promoCodeApplyButton = $('#btn-apply-promo-code'),
		url = promoCodeApplyButton.attr('href');

	$.ajax({
		type: "POST",
		url: url,
		data: {
			promo_code: promoCode
		},
		dataType: 'json',
		success: function (response) {

			if (!response.success) {
				showMessage(response.message, 'error', '#promo-code-alert');
			} else {

				$('#checkout-promo-code').html(response.html.promo_code);
				$('#your-order-summary').html(response.html.order_summary);
			}

		},
		error: function (xhr, status, errorThrown) {
			console.log("Error: " + errorThrown);
			console.log("Status: " + status);
			console.dir(xhr);
		}
	});

}

$(document).on(
	'click',
	'#btn-apply-promo-code',
	function(e) {

		e.preventDefault();
		applyPromoCode();

	}
);

$(document).on(
	'click',
	'#remove-promo-code',
	function(e) {

		e.preventDefault();
		removePromoCode();

	}
);

toggleBillingAddress();
