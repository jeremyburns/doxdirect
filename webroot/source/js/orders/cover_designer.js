/*global $ */

// cover_designer.js
$(document).ready(function () {

	'use strict';

//	console.log("Testing");
	$.ajax({
		type: 'get',
		dataType: 'json',
		url: $('#add-cover-files-url').val(),
		success: function (json) {
			if (json.status === "done") {
				showDesignTool();
			}
		}
	});
});
