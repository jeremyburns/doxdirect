/*global $ */

/*global window */

/*global document */

function updatePrintedCover(changed_dropdown_id) {

	'use strict';

	var
		front_options = $('#front_cover_options').val(),
		back_options = $('#back_cover_options').val();

	if (changed_dropdown_id === "front_cover_options" && front_options === "blank_cover") {

		// If the front cover has been changed to "blank cover", set the back cover option to "blank cover" as well, and disable the button
		$('#back_cover_options option:selected').removeAttr('selected');
		$('#back_cover_options').find('option[value="blank_cover"]').attr('selected', 'true');
		$('#cover_button').addClass('disabled hidden');
		$('#cover_designer_info').removeClass('hidden');
		back_options = $('#back_cover_options').val();

	} else if (changed_dropdown_id === "front_cover_options" && front_options !== "blank_cover") {

		// If the front cover has been changed, but the value is NOT "blank cover", enable the button.
		// If the back cover is currently "blank cover", change it so it matches the front
		$('#cover_button').removeClass('disabled').removeClass('hidden');
		$('#cover_designer_info').addClass('hidden');

		if (back_options === "blank_cover") {

			$('#back_cover_options option:selected').removeAttr('selected');
			$('#back_cover_options').find('option[value="' + front_options + '"]').attr('selected', 'true');
			back_options = front_options;

		}
	} else if (changed_dropdown_id === "back_cover_options" && back_options === "blank_cover") {

		// If the back cover has been changed to "blank cover", set the front cover option to "blank cover" as well, and disable the button
		$('#front_cover_options option:selected').removeAttr('selected');
		$('#front_cover_options').find('option[value="blank_cover"]').attr('selected', 'true');
		$('#cover_button').addClass('disabled hidden');
		$('#cover_designer_info').removeClass('hidden');
		front_options = $('#front_cover_options').val();

	} else if (changed_dropdown_id === "back_cover_options" && back_options !== "blank_cover") {

		// If the back cover has been changed, but the value is NOT "blank cover", enable the button.
		// If the front cover is currently "blank cover", change it so it matches the back
		$('#cover_button').removeClass('disabled').removeClass('hidden');
		$('#cover_designer_info').addClass('hidden');

		if (front_options === "blank_cover") {

			$('#front_cover_options option:selected').removeAttr('selected');
			$('#front_cover_options').find('option[value="' + back_options + '"]').attr('selected', 'true');
			front_options = back_options;
		}
	}

	if(front_options === "use_and_discard" && back_options === "use_and_discard") {
		$('#cover_button').html('Apply &amp; Preview');
	}
	else if(front_options === "start_from_blank" || back_options === "start_from_blank") {
		$('#cover_button').html('Design My Cover');
	}

	if(front_options !== "blank_cover" && back_options !== "blank_cover") {
		if($('#printed_cover_job_id').val() !== "") {
			$('#btn_checkout').removeClass('disabled');
		}
		else {
			$('#btn_checkout').addClass('disabled');
		}
	}
	else {
		$('#btn_checkout').removeClass('disabled');
	}

	$.ajax({
		type: 'get',
		url: $('#update-cover-url').val() + '/' + front_options + '/' + back_options
	});
}

function deletePrintedCover() {

	'use strict';

	$.ajax({
		type: 'get',
		url: $('#delete-cover-url').val(),
		success: function () {
			var
				cover_button = $('#cover_button'),
				replace_block = '<p id="cover_button_paragraph"></p>';
			$('#delete_cover_block').replaceWith(replace_block);
			$('#cover_button_paragraph').append(cover_button);
			$('#front_cover_options').removeAttr('disabled');
			$('#back_cover_options').removeAttr('disabled');
			$('#front_cover_options option:selected').removeAttr('selected');
			$('#front_cover_options').find('option[value="blank_cover"]').attr('selected', 'true');
			$('#back_cover_options option:selected').removeAttr('selected');
			$('#back_cover_options').find('option[value="blank_cover"]').attr('selected', 'true');
			$('#cover_button').addClass('hidden');

			$.ajax({
				type: 'get',
				url: $('#update-cover-url').val() + '/use_and_discard/use_and_discard'
			});

			$('#cover_button').on(
				'click',
				function () {
					window.location = $('#cover-designer-url').val();
				}
			);
		}
	});

}





$('form[data-class=fix-option-form]').on(
	'submit',
	function () {

		'use strict';

		var
			form = $(this),
			fixOptionPanel = form.closest('[data-class=fix-option-panel]'),
			fixOptionButton = form.find('[data-class=fix-option-button]');

		$('[data-class=fix-option-button]')
			.not(fixOptionButton)
			.addClass('disabled');

		fixOptionButton
			.html('<i class="fa fa-circle-o-notch fa-spin"></i> Updating your document...')
			.addClass('disabled');

		$('[data-class=fix-option-panel]')
			.not(fixOptionPanel)
			.addClass('disabled')
			.hide();

	}
);



$(document).on(
	"submit",
	"#OrderConfigureForm",
	function () {

		$('#btn_checkout')
			.html('Going to checkout...')
			.addClass('disabled');

	}
);

$(document).on(
	'change',
	'#front_cover_options',
	function () {
		updatePrintedCover(this.id);
	}
);

$(document).on(
	'change',
	'#back_cover_options',
	function () {
		updatePrintedCover(this.id);
	}
);

$(document).on(
	'click',
	'#cover_button',
	function () {
		window.location = $('#cover-designer-url').val();
	}
);

$(document).on(
	'click',
	'#delete_cover_button',
	function () {
		deletePrintedCover();
	}
);

// Set up Cover Design <select> functionality where applicable
$(document).ready(
	function () {

		// When a large document is uploaded to Callas via the api it's possible that not all pages have preview files
		// This ajax action starts a background process to complete them
//		completePreviews();

	}
);
