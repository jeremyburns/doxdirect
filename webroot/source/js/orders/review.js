/*global $ */

$('#promotional-code-link').on(
	'click',
	function (e) {

		'use strict';

		e.preventDefault();
		$('.promotional-code').show();
	}
);

$('.promotional-code').hide();
