/*global $ */
/*global document */
/*global productMap */

// @codekit-prepend "../global/global_functions.js";

// @codekit-prepend "same_day_processing.js";

function updateCart() {

	'use strict';

	var
	headerCart = $('#header-cart'),
		url = headerCart.data('refresh-link');

	$.get(
		url,
		function (html) {

			headerCart.html(html);

		}

	);

}

function invalidateQuote() {

	'use strict';

	var recalculateMessage;

	$('#quote-price')
		.removeClass('panel-default')
		.addClass('panel-error');

	$('#link-order-now').hide();

	recalculateMessage = 'Please recalculate to get an updated quote.';

	$('#quote-note').html('<p class="alert alert-warning">This quote is out of date as you have changed the criteria. ' + recalculateMessage + '</p>');

	$('#quote-update-notice').html('<p class="alert alert-info-pale">' + recalculateMessage + '</p>');

	$('#order-now-note').html(recalculateMessage);

	$('#another-quote-text').hide();

	$('#quote-submit-button').show();

}

function calculateStart() {

	'use strict';

	$('#another-quote-text').hide();

	var
	workingText,
		calculatingText,
		quoteText;

	calculatingText = 'Calculating your quote...';
	workingText = '<i class="fa fa-circle-o-notch fa-spin color-success"></i> ' + calculatingText;
	quoteText = '<i class="fa fa-circle-o-notch fa-spin color-white"></i> ' + calculatingText;

	quoteText = '<p class="alert alert-info px mn">' + quoteText + '</p>';

	$("#quote-price").html(quoteText);
	$('#quote-update-notice').html(quoteText);
	//	$('#quote-submit-button').addClass('disabled');
	$('#quote-submit-button').hide();
	//	$('#quote-submit-button').html('<i class="fa fa-circle-o-notch fa-spin color-white"></i> ' + calculatingText);
	//	$('#quote-submit-button i').addClass('fa-spin');

}

function calculateStop() {

	$('#quote-update-notice').html('<p class="alert alert-success"><i class="fa fa-check-circle color-white"></i> <a href="#quote-result">Your quote is ready</a></p>');

	$('#quote-submit-button i').removeClass('fa-spin');
	$('#quote-submit-button').hide();
	$('#another-quote-text').show();

	// $('#link-add-another-order-item').show();

}

function getQuote() {

	'use strict';

	var
	form = $("#OrderQuoteForm"),
		url = form.attr("action"),
		data = form.serialize();

	$.ajax({
		type: 'POST',
		url: url,
		data: data,
		dataType: 'json',
		cache: false,
		beforeSend: function () {
			calculateStart();
		},
		success: function (result) {
			$('#quote-result').html(result.html);
			$('#OrderId').val(result.order.Order.id);
			$('#OrderPaceQuoteId').val(result.order.Order.pace_quote_id);
			$('#OrderItem0Id').val(result.order['OrderItem'][0]['id']);
			$('#OrderItem0PaceQuoteProductId').val(result.order['OrderItem'][0]['pace_quote_product_id']);
			$('#quote_container').removeClass('hidden');
			$('#calc').collapse('hide');
			$('#quote').collapse('show');
			updateCart();
		},
		complete: function () {
			calculateStop();
		}
	});

}



function setPageLimits() {

	'use strict';

	clearHelpText();

	var
	product = $('*[data-refresh="product"]'),
		productId = product.val(),
		productName = product.find(":selected").text(),
		media = $('*[data-update-model="media"]'),
		mediaId = media.val(),
		mediaName = media.find(":selected").text(),
		sideId = $('*[data-update-model="side"]').val(),
		pages = $('*[data-update-model="pages"]'),
		currentPages = parseInt(pages.val(), 10),
		pageLimits,
		mediaPageLimits,
		sidesMediaPageLimits,
		minPages = 1,
		maxPages,
		divisor,
		surplus,
		addPages,
		newTotalPages,
		helpText,
		sides;

	// Convert the side id into an actual number of sides
	sides = productMap['sides'][sideId];

	if (productMap['selectOptions'][productId].hasOwnProperty('pageLimits')) {

		pageLimits = productMap['selectOptions'][productId]['pageLimits'];

		if (pageLimits.hasOwnProperty(mediaId)) {

			mediaPageLimits = pageLimits[mediaId];

			if (mediaPageLimits.hasOwnProperty(sides)) {

				sidesMediaPageLimits = mediaPageLimits[sides];

				if (sidesMediaPageLimits.hasOwnProperty('pages_min')) {
					minPages = parseInt(sidesMediaPageLimits.pages_min, 10);
				}

				pages.attr('min', minPages);

				if (currentPages < minPages) {
					pages.val(minPages);
					helpText = productName + ' must have at least ' + minPages + ' pages (you had chosen ' + currentPages + ').';
				}

				if (sidesMediaPageLimits.hasOwnProperty('pages_max')) {

					maxPages = parseInt(sidesMediaPageLimits.pages_max, 10);

					pages.attr('max', maxPages);

					if (currentPages > maxPages) {
						pages.val(maxPages);
						helpText = productName + ' printed on ' + mediaName + ' cannot have more than ' + maxPages + ' pages (you had chosen ' + currentPages + ').';
					}

				} else {

					pages.removeAttr('max');

				}

			}

		}

	}

	if (productMap.products[productId].page_divisor > 0) {
		divisor = productMap.products[productId].page_divisor;
		surplus = (currentPages % divisor);
		addPages = divisor - surplus;
		newTotalPages = currentPages + addPages;
		if (surplus) {
			pages.val(newTotalPages);
			helpText = 'The number of pages in an ' + productName + ' document must be divisible by ' + divisor + '. You had chosen ' + currentPages + ' - we will add ' + addPages + ' to make a total of ' + newTotalPages + ' pages.';
		}

	}

	if (helpText) {
		showHelpText($('#form-group-pages'), 'help-page-count', helpText);
	}

}

function refreshSelectOptions() {

	'use strict';

	// Which product has been selected?
	var
		productId = $('*[data-refresh="product"]').val(),
		selectedId,
		selectedText,
		newSelectedId,
		output,
		targetControl,
		targetControlType,
		selectOptions,
		modelHasOptions = false,
		hideWhenSingle,
		updateModel,
		optionGroup,
		orderItemKey;

	if (productId) {

		// Loop through the productMap settings key
		$.each(
			productMap.settings,
			function (modelName, setting) {

				modelHasOptions = false;

				// Pass in the modelName and the settings for it
				updateModel = setting.updateModel;

				// Which is the target control we have to change?
				targetControl = $('*[data-update-model="' + updateModel + '"]');

				// What type is it?
				targetControlType = targetControl.data('type');

				// What is its selected id?
				selectedId = targetControl.val();

				newSelectedId = null;

				// Put the select options into a variable
				selectOptions = productMap['selectOptions'][productId];
				hideWhenSingle = selectOptions['hideWhenSingle'][modelName];

				optionGroup = setting.optionGroup;

				if (targetControlType !== 'undefined') {
					switch (targetControlType) {

						case 'radio':

							// Hide all options to begin with
							$('.' + updateModel).hide();

							// Get the label text so we can match it later
							selectedText = targetControl.closest('label').text();
							orderItemKey = targetControl.data('order-item-key');

							// Are there options?
							if (selectOptions['options'].hasOwnProperty(modelName)) {

								// Now loop through the options for this modelName
								$.each(selectOptions['options'][modelName], function (value, label) {

									// Show this option
									$('#OrderItem' + orderItemKey + modelName + 'Id' + value).closest('div.' + updateModel).show();

									// Does this option have the same text as the previously selected text?
									if (label === selectedText) {
										newSelectedId = value;
									}

								});

							}

							break;

						case 'select':

							// Get the text of the selected option
							selectedText = targetControl.find(":selected").text();

							// Let's do this...
							// First empty it and then append the new values
							targetControl
								.empty()
								.html(function () {

								// If there's no options set, just hide the control
								if (!selectOptions['options'].hasOwnProperty(modelName)) {

									$('.' + optionGroup).hide();

								} else {

									// Start with an empty output
									output = '';

									// Do we have a placeholder entry?
									if (setting.hasOwnProperty('placeHolderText') && !hideWhenSingle) {
										// Yes, so make it the first entry with an empty value
										output += '<option value="">' + setting.placeHolderText + '</option>';
									}

									// Now loop through the options for this modelName
									$.each(selectOptions['options'][modelName], function (value, label) {

										modelHasOptions = true;

										// And add the select option to the output
										output += '<option value="' + value + '">' + label + '</option>';

										// Does this option have the same text as the previously selected text?
										if (label === selectedText) {
											newSelectedId = value;
										}

									});

									// Sometimes, when there's just a single option, we want to hide the control
									if (selectOptions['hideWhenSingle'][modelName] === true) {
										$('.' + optionGroup).hide();
									} else {
										// Otherewise make sure it's shown
										$('.' + optionGroup).show();
									}

									// Return the output
									return output;

								}
							});

							break;

					}
				}

				if (setting.hasOwnProperty('default') && !hideWhenSingle) {
					// There's a default
					newSelectedId = setting.default;
				}

				if (newSelectedId) {

					switch (targetControlType) {

						case 'radio':

							break;

						case 'select':

							targetControl.val(newSelectedId);

							break;

					}

				}

			}

		);

		setPageLimits();

	}

}

$(document).on(
	"change",
	"*[data-update-quote='yes']",
	function () {
		invalidateQuote();
	}
);

$(document).on(
	"change",
	"*[data-check-turnaround='yes']",
	function () {
		// Enable or disable same day turnaround
		checkSameDayTurnaround();
	}
);

// To make the quote calculator work but not submit by ajax (perhaps to trace an error) comment out these lines
$(document).on(
	"submit",
	"#OrderQuoteForm",
	function (event) {
		event.preventDefault();
		getQuote();
	}
);

$('#link-add-another-order-item').hide();

$(document).on(
	"change",
	"*[data-page-limit]",
	function () {
		refreshSelectOptions();
	}
);

// Have to do this on document ready as productMap is added after this script is written
$(document).ready(
	function () {
		refreshSelectOptions();
	}
);
