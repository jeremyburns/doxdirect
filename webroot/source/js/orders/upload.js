/*global $ */
/*global console */
/*global document */

function toggleElement(element, showHide) {

	'use strict';

	if (showHide === 'show') {
		$(element)
			.show()
			.removeClass('hide');
	} else if (showHide === 'disable') {
		$(element)
			.addClass('disabled')
			.show();
	} else if (showHide === 'enable') {
		$(element)
			.removeClass('disabled')
			.removeClass('hide')
			.show();
	} else if (showHide === 'hide') {
		$(element)
			.addClass('hide');
	} else if (showHide === 'fadeOut') {
		$(element)
			.fadeOut(1000);
	}

}

function showStatus(statusCode, additionalMessage) {

	'use strict';

	var
		uploadStatus = $('#upload-status'),
		icons,
		statusMessages,
		statusSets,
		statusSet,
		messageText;

	icons = {
		'ready': '<i class="fa fa-check-circle-o color-green"></i>',
		'working': '<i class="fa fa-circle-o-notch fa-spin color-blue"></i>',
		'complete': '<i class="fa fa-check color-success"></i>',
		'waiting': '<i class="fa fa-genderless color-disabled"></i>',
		'error': '<i class="fa fa-exclamation-circle color-error"></i>'
	};

	statusMessages = {
		'ready': 'Ready to upload',
		'uploading': 'Uploading, checking and previewing your files. This could take a minute or two...',
		'uploaded': 'File upload complete',
		'check': 'Checking your files',
		'checking': 'Checking your files; this could take a minute or two...',
		'checked': 'Checking complete',
		'complete': 'Your files are ready. You can upload more files or continue with your order.',
		'error': 'There was a problem uploading your files.'
	};

	statusSets = {
		'ready': [
			{
				'message': 'ready',
				'icon': 'ready'
			}
		],
		'uploading': [
			{
				'message': 'uploading',
				'icon': 'working'
			}
//			{
//				'message': 'check',
//				'icon': 'waiting'
//			}
		],
		'checking': [
			{
				'message': 'uploaded',
				'icon': 'complete'
			},
			{
				'message': 'checking',
				'icon': 'working'
			}
		],
		'complete': [
			{
				'message': 'complete',
				'icon': 'complete'
			}
		],
		'clear': [],
		'error': [
			{
				'message': 'error',
				'icon': 'error'
			}
		]
	};

	statusSet = statusSets[statusCode];

	uploadStatus.empty();

	$.each(statusSet, function(key, status) {

		messageText = statusMessages[status.message];

		if (additionalMessage) {
			messageText = messageText + ' ' + additionalMessage;
		}
		uploadStatus.append('<li>' + icons[status.icon] + ' ' + messageText + '</li>' );
	});

	uploadStatus.show();

}

function setProgressBar(percentComplete) {
	'use strict';

	var progressBar = $('#upload-progress-bar');

	if (percentComplete > 0) {
		toggleElement('#upload-progress-bar-container', 'show');
	}

	progressBar
		.css('width', percentComplete + '%')
//		.html(percentComplete + '%')
		.data('percent', percentComplete);

//	if (percentComplete == 100) {
//		toggleElement('#upload-progress-bar-container', 'fadeOut');
//	}

//	if (percentComplete > 90) {
//		showStatus('checking');
//	}

}

function toggleUploadAlert(showHide) {
	'use strict';
	if (showHide) {
		toggleElement('#no-files', 'hide');
		toggleElement('#link-upload-complete', 'show');
//		toggleElement('#uploaded-order-item-files', 'show');
	} else {
		toggleElement('#no-files', 'show');
		toggleElement('#link-upload-complete', 'hide');
//		toggleElement('#uploaded-order-item-files', 'hide');
	}

	if ($('#page_count_validation').length) {
		toggleElement('#link-upload-complete', 'hide');
	}

}

//function updateOrderSummary() {
//	'use strict';
//	var
//		span = $('#order-summary'),
//		url = $('#order-summary-url').val();
//
//	if (url.length) {
//
//		$.get(
//			url,
//			function (html) {
//				span.html(html);
//
//			}
//		);
//
//	}
//
//}

function checkIsReady() {

	'use strict';

	var fileCount = $('#order-item-files-list-tbody').children('tr.order-item-file').length;

	if (fileCount > 0) {
		toggleUploadAlert(true);
	} else {
		toggleUploadAlert(false);
	}

}

function resequence(tableId) {

	var
	orderItemFileIds = [],
		orderItemFileId,
		table = $(tableId),
		url = table.data('resequence-url'),
		tr,
		td;

	$('td.sequence').html('<i class="fa fa-circle-o-notch fa-spin color-blue"></i>');

	$(tableId + " tr.order-item-file").each(function () {
		orderItemFileId = $(this).data('order-item-file-id');
		orderItemFileIds.push(orderItemFileId);
	});

	$.ajax({
		type: "POST",
		url: url,
		data: {
			orderItemFileIds: orderItemFileIds
		},
		dataType: 'json',
		success: function (response) {

			$.each(
				response,
				function (index, value) {

					tr = $('#order-item-file-' + index);
					td = tr.find('td.sequence');
					td.html(value);

				}
			);

			makeOrderItemFilesSortable();

		}

	});

}

function makeOrderItemFilesSortable() {
	$('table#order-item-files-list tbody').sortable({
		helper: fixWidthHelper,
		items: "tr.order-item-file",
		//	handle: '.ui-sortable-handle',
		stop: function () {
			resequence('#order-item-files-list');
		}
	}).disableSelection();
}

function showDocuments() {

	'use strict';

	var
		span = $('#uploaded-order-item-files'),
		url = $('#uploaded-order-item-files-url').val();

	if (url.length) {

		toggleElement('#link-upload-complete', false);

		$.get(
			url,
			function (data) {
				span.html(data);
				toggleElement('.delete-order-item-file-confirm', 'hide');
				toggleUploadAlert(true);
				toggleElement('#upload-alert-panel', 'show');
//				updateOrderSummary();
				checkIsReady();
				// Make the new list of order item files sortable
				makeOrderItemFilesSortable();
			}
		);

	}

}

function closeAlerts() {
	'use strict';
	$('.alert-dismissible').alert('close');
}

function humanFileSize(bytes, si) {
	'use strict';

	var
		thresh = si ? 1000 : 1024,
		units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'],
		unitKey = -1;

	if (bytes < thresh) {
		return bytes + ' B';
	}

	do {
		bytes /= thresh;
		unitKey = unitKey + 1;
	} while (bytes >= thresh);

	return bytes.toFixed(1) + ' ' + units[unitKey];


}

function toggleDeleteForm(orderItemFileId, showHide) {

	'use strict';

	if (showHide === true) {
		toggleElement('#delete-order-item-file-link-' + orderItemFileId, 'hide');
		toggleElement('#delete-order-item-file-confirm-' + orderItemFileId, 'show');
	} else {
		toggleElement('#delete-order-item-file-link-' + orderItemFileId, 'show');
		toggleElement('#delete-order-item-file-confirm-' + orderItemFileId, 'hide');
	}
}

function uploadFile(form) {

	'use strict';

	closeAlerts();

	var uploadFileName = $('#upload-file-name'),
		continueLink = $('#link-upload-complete'),
		uploadLabel = $('#upload-label'),
		defaultLabelText = '<i class="fa fa-folder-open-o"></i> <span>Choose a file...</span>';

	form.ajaxSubmit({
		dataType: 'json',
		beforeSubmit: function () {

			continueLink.hide();

			toggleElement('#btn-upload', 'disable');

			$('#btn-upload').html('<i class="fa fa-hourglass-end"></i>&nbsp;Uploading...');

			setProgressBar(0);

			showStatus('uploading');

		},
		uploadProgress: function (event, position, total, percentComplete) {
			setProgressBar(percentComplete);
		},
		success: function (response) {

			showDocuments();

			uploadFileName
				.html('')
				.hide();

			toggleUploadAlert(true);

			$('#btn-upload').html('<i class="fa fa-cloud-upload"></i>&nbsp;Upload');
			toggleElement('#btn-upload', 'hide');
			toggleElement('#upload-label', 'enable');
			uploadLabel.html(defaultLabelText);

			showStatus('uploading');

			if (response.success === true) {

				var orderItemId = response.orderItemId;

				$('#order-item-files-list-none-' + orderItemId).remove();

				continueLink.show();

				showStatus('complete');

				toggleElement('#upload-progress-bar-container', 'fadeOut');

				$('#order-summary').html(response.html.orderItemSummary);

				$('#order-progress').html(response.html.orderProgress);


			} else {

				showStatus('error', response.message);
				toggleElement('#upload-progress-bar-container', 'hide');
				uploadLabel.html(defaultLabelText);

			}

		},
		error: function (xhr, status, errorThrown) {
			console.log("Error: " + errorThrown);
			console.log("Status: " + status);
			console.dir(xhr);
			toggleElement('#upload-progress-bar-container', 'hide');
			toggleUploadAlert(true);
		},
		resetForm: true
	});

}

function deleteOrderItemFile(form) {

	'use strict';

	showStatus('clear');

	var
		continueLink = $('#link-upload-complete'),
		confirmButton = form.find('[data-action=confirm]'),
		orderItemId,
		tr;

	orderItemId = form.data('order-item-file-id');

	tr = $('#order-item-file-' + orderItemId);

	tr.hide();
	continueLink.hide();
	confirmButton.html('<i class="fa fa-circle-o-notch fa-spin"></i> Deleting...');

	$.ajax({
		type: "POST",
		url: form.attr('action'),
		data: form.serialize(),
		dataType: 'json',
		success: function (response) {

			$('#order-summary').html(response.html.orderItemSummary);
			$('#order-progress').html(response.html.orderProgress);
			tr.remove();
			showDocuments();
			continueLink.show();
			// makeOrderItemFilesSortable();

		}

	});

}

//Comment this block out to disable ajax uploads
$(document).on(
	'submit',
	'#OrderUploadForm',
	function (e) {

		e.preventDefault();

		uploadFile($(this));

	}
);

$(document).on(
	'click',
	'.delete-order-item-file-link',
	function (e) {

		'use strict';

		e.preventDefault();

		toggleElement('#link-upload-complete', 'hide');

		var orderItemFileId = $(this).data('order-item-file-id');

		toggleDeleteForm(orderItemFileId, true);

	}

);

$(document).on(
	'reset',
	'.delete-order-item-file-form',
	function (e) {

		'use strict';

		e.preventDefault();

		toggleElement('#link-upload-complete', 'show');

		var orderItemFileId = $(this).data('order-item-file-id');
		toggleDeleteForm(orderItemFileId, false);
	}
);

$(document).on(
	'submit',
	'.delete-order-item-file-form',
	function (e) {

		e.preventDefault();

		deleteOrderItemFile($(this));

	}
);

function fixWidthHelper(e, tr) {

	tr.children().each(function() {
		$(this).width($(this).width());
	});
	return tr;
}

$('#link-upload-complete').on(
	'click',
	function () {

		'use strict';

		$(this)
			.addClass('disabled')
			.removeClass('btn-sm')
			.removeClass('btn-lg')
			.html('<i class="fa fa-circle-o-notch fa-spin"></i> Analysing your files...');
	}
);

function getInput() {
	'use strict';

	;( function( $, window, document, undefined )
	  {
		$( '.inputfile' ).each( function() {

			var $input	 = $(this),
				$label	 = $input.next('label'),
				labelVal = $label.html();

			$input.on( 'change', function( e ) {

				var fileName = '';

				if (this.files && this.files.length > 1 ) {
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				} else if( e.target.value ) {
					fileName = e.target.value.split( '\\' ).pop();
				}

				if ( fileName ) {
					$label.find( 'span' ).html( fileName );
					toggleElement('#btn-upload', 'enable');
				} else {
					$label.html( labelVal );
				}

			});

			// Firefox bug fix
			$input
				.on(
					'focus',
					function() {
						$input.addClass('has-focus');
					}
				)
				.on(
					'blur',
					function() {
						$input.removeClass('has-focus');
					}
				);

		});

	})( jQuery, window, document );

}


// Make the list of order item files sortable
makeOrderItemFilesSortable();

// Hide the normal upload button
toggleElement('.delete-order-item-file-confirm', 'hide');
toggleElement('#upload-progress-bar-container', 'hide');

checkIsReady();

getInput();

toggleElement('#btn-upload', 'hide');
