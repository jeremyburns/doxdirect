// checks to see if same day delivery is available for given product and order time
function checkSameDayTurnaround() {

	'use strict';

	clearMessage('.help-block');

	var
		// Find the turnaround options
		turnaroundOptions = $("#OrderTurnaroundOptionId"),
		// Get the same day option
//		sameDayOption = turnaroundOptions.find('option[value=1]'),
		// get the currently selected option
		selectedOption = turnaroundOptions.val(),
		// The url we use to check availability
		url = turnaroundOptions.data('check-url'),
		message = false,
		is_order = $('#IsOrder').val(),
		nextDayOption,
		newOption,
		postData;

	postData = {
		turnaround_option_id: selectedOption,
		is_order: is_order
	};

	if ($('*[data-refresh="product"]').length) {
		postData['product_id'] = $('*[data-refresh="product"]').val();
	}

	if ($('#QualifiesForSameDayTurnaround').length) {
		postData['qualifies_for_same_day_turnaround'] = $('#QualifiesForSameDayTurnaround').val();
	}

	// Run the check
	$.ajax(
		{
			dataType: 'json',
			type: 'post',
			url: url,
			data: postData,
			success: function (sameDayAvailable) {

				if ('message' in sameDayAvailable) {
					message = sameDayAvailable.message;
					showHelpText($('#form-group-turnaround-option-id'), 'help-turnaround-option', message);
					showHelpText($('#OrderTurnaroundOptionId'), 'help-turnaround-option', message);
					showHelpText($('#form-group-product'), 'help-product-id', message);
				}

				if (sameDayAvailable.success === true) {

					if (sameDayAvailable.available === false) {

						if ('new_option' in sameDayAvailable) {

							newOption = sameDayAvailable.new_option;

							// Get the next day option
							nextDayOption = turnaroundOptions.find('option[value=' + newOption + ']');
							// Select it
							nextDayOption.attr('selected', true);

							// Set the value to 2
							turnaroundOptions.val(newOption);

						}

					}

				}

			},
			error: function (xhr, status, errorThrown) {
				console.log("Error: " + errorThrown);
				console.log("Status: " + status);
				console.dir(xhr);
			}

		}

	);

}
