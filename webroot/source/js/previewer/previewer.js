/*global $ */

/*global orderItemPages */

/*global Image */

/*global setTimeout */

/*global window */

/*global document */

function completePreviews(pageNumber) {

	'use strict';

	var url = $('#callas-complete-previews-url').val();

	if (typeof pageNumber !== 'undefined') {
		url += '/' + pageNumber;
	}

	$.ajax({
		type: 'get',
		url: url
	});

}

function loadPageImage(page, imagePath, pageNumber, loader) {

	'use strict';

	var imageData = new Image();

	imageData.onload = function () {
		page
			.attr('src', imagePath)
			.data('page-number', pageNumber)
			.show();

	};

	imageData.onerror = function () {

		page.hide();
		loader.show();

		completePreviews(pageNumber);

		setTimeout(function () {
			loadPageImage(page, imagePath, pageNumber, loader);
		}, 10);

		loader.hide();

	};

	imageData.src = imagePath;

}

function setHeight() {

	'use strict';

	// This is the object we are going to get the height from
	// It'll be the right or single page

	var
		height,
		orientation = $('#previewer').data('orientation');


	if (orientation === 'portrait') {

		if ($('#page-single').length !== 0) {
			height = $('#page-single').height();
		} else if ($('#page-right').length !== 0) {
			height = $('#page-right').height();
		}

		if (height) {
			if ($('#page-blank').length !== 0) {
				$('#page-blank').height(height);
			}

			if ($('img.spine').length !== 0) {
				$('img.spine').height(height);
			}
		}

	}

}

function clearPage(side, direction) {

	'use strict';

	var
		page = $('#page-' + side),
		pageNumberText = $('#page-number-' + side),
		arrow = $('#page-link-' + direction);

	page
		.attr('src', null)
		.hide()
		.data('page-number', null);

	pageNumberText.text('');

	arrow.hide();

}

function setPage(side, pageNumber) {

	'use strict';

	var
		loader = $('#page-loader-' + side),
		page = $('#page-' + side),
		pageNumberText = $('#page-number-' + side),
		pageNumberKey = pageNumber - 1,
		imagePath;

	if (typeof orderItemPages !== 'undefined') {

		if (typeof orderItemPages[pageNumberKey] !== 'undefined') {

			imagePath = orderItemPages[pageNumberKey]['preview_image_path'];

			loadPageImage(page, imagePath, pageNumber, loader);

			if ($('#page-number-single').length) {
				$('#page-number-single').text('Page ' + pageNumber);
			} else {
				pageNumberText.text('Page ' + pageNumber);
			}

			page.show();

		} else {

			if (side !== 'single') {

				clearPage(side);

			}

			$('.page-loader').hide();

		}

	}

	$('.page-loader').hide();

	setHeight();

}

function side(pageNumber) {

	if (pageNumber % 2 === 1) {
		return 'right';
	} else {
		return 'left';
	}

}

function goToPage(pageNumber) { //, direct) {

	'use strict';

	var
		previewer = $('#previewer'),
		bound = previewer.data('bound'),
		duplex = previewer.data('duplex'),
		leftPageNumber,
		rightPageNumber,
		singlePageNumber;

	$('.page-number-link').show();

	if (duplex === 'single') {

		if (bound === 'unbound') {

			setPage('single', pageNumber);

		} else {

			if (side(pageNumber) === 'left') {

				singlePageNumber = pageNumber;

			} else {

				singlePageNumber = pageNumber;
			}

			setPage('right', singlePageNumber);

		}

	} else {

		if (side(pageNumber) === 'left') {

			leftPageNumber = pageNumber;
			rightPageNumber = leftPageNumber + 1;

		} else {

			rightPageNumber = pageNumber;
			leftPageNumber = rightPageNumber - 1;

		}

		setPage('left', leftPageNumber);
		setPage('right', rightPageNumber);

	}

}

function turn(direction) {

	'use strict';

	$('.page-number-link')
		.show()
		.removeClass('disabled');

	var
		previewer = $('#previewer'),
		bound = previewer.data('bound'),
		duplex = previewer.data('duplex'),
		leftPage = $('#page-left'),
		rightPage = $('#page-right'),
		singlePage = $('#page-single'),
		leftPageNumber,
		rightPageNumber,
		singlePageNumber,
		pageCount = $('#previewer').data('page-count');

	if (duplex === 'single') {

		if (bound === 'unbound') {

			singlePageNumber = singlePage.data('page-number');

			if (direction === 'previous') {
				singlePageNumber -= 1;
			} else {
				singlePageNumber += 1;
			}

			if (singlePageNumber > 0 && singlePageNumber <= pageCount) {
				setPage('single', singlePageNumber);
			}

			if (singlePageNumber <= 0) {
				$('#page-link-previous').addClass('disabled');
			}

			if (singlePageNumber >= pageCount) {
				$('#page-link-next').addClass('disabled');
			}

		} else {

			if (direction === 'previous') {

				leftPageNumber = leftPage.data('page-number');

				leftPageNumber -= 1;
				rightPageNumber = leftPageNumber + 1;

			} else {

				rightPageNumber = rightPage.data('page-number');
				rightPageNumber += 1;
				leftPageNumber = rightPageNumber - 1;
			}


			if (leftPageNumber > 0) {
				setPage('left', leftPageNumber);
			} else {
				clearPage('left', direction);
			}

			if (rightPageNumber <= pageCount) {
				setPage('right', rightPageNumber);
			} else {
				clearPage('right', direction);
			}



		}

	} else {

		if (direction === 'previous') {

			leftPageNumber = leftPage.data('page-number');

			leftPageNumber -= 2;
			rightPageNumber = leftPageNumber + 1;

		} else {

			rightPageNumber = rightPage.data('page-number');

			rightPageNumber += 2;
			leftPageNumber = rightPageNumber - 1;

		}

		if (leftPageNumber > 0) {
			setPage('left', leftPageNumber);
		} else {
			clearPage('left', direction);
		}

		if (rightPageNumber <= pageCount) {
			setPage('right', rightPageNumber);
		} else {
			clearPage('right', direction);
		}

	}

}

$('[data-turn]').on(
	'click',
	function (event) {

		event.preventDefault();

		turn($(this).data('turn'));

	}
);

$(window).resize(function () {

	setHeight();

});

// Have to do this on document ready as orderItemPages is added after this script is written
$(document).ready(

	function () {

		'use strict';

		$('.page-loader').show();
		$('.page').hide();
		$('.page-blank').hide();

		goToPage(1);

		$('#page-link-previous')
			.hide()
			.addClass('disabled');

		if ($('#previewer').data('page-count') === 1) {
			$('#page-link-next')
				.hide()
				.addClass('disabled');
		}

		setHeight();

	}

);
