/*global $ */

/*global document */

$(document).on(
	'submit',
	'#FormRefreshProductCatalogue',
	function(e) {

		'use strict';

		e.preventDefault();

		var
			form = $(this),
			flash = $('#flash'),
			productCatalogue = $('#product-catalogue');

		form.hide();
		flash
			.html('<p class="alert alert-info"><i class="fa fa-circle-o-notch fa-spin"></i> Refreshing product catalogue...</p>').show();

		productCatalogue.hide();

		flash.html();

		$.ajax({
			type: "POST",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: 'json',
			success: function (response) {

				flash.html(response.flash).show();

				productCatalogue.html(response.productCatalogue).show();

			},
			error: function (xhr, status, errorThrown) {
				console.log("Error: " + errorThrown);
				console.log("Status: " + status);
				console.dir(xhr);
			},
			complete: function() {
				form.show();
			}

		});

	}
);
