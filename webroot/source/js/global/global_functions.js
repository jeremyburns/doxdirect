/*global $ */

function clearMessage(domTargetName) {
	$(domTargetName)
		.html('')
		.hide();
}

function showMessage(message, element, domTargetName) {

	'use strict';

	var flashUrl = $('#flash-path').val();

	$.ajax({
		type: "POST",
		url: flashUrl,
		dataType: 'json',
		data: {
			message: message,
			element: element
		},
		success: function (response) {
			$(domTargetName)
				.html(response)
				.show();
		}
	});

}

function clearElement(domTargetName) {

	$(domTargetName)
		.html('')
		.hide();
}

function renderElement(data, domTargetName) {

	'use strict';

	var
		elementUrl = $('#element-path').val(),
		domTarget = $(domTargetName);

	domTarget.html('<p class="alert alert-gray"><i class="fa fa-spinner fa-pulse"></i> Loading...</p>');

	$.ajax({
		type: "POST",
		url: elementUrl,
		dataType: 'json',
		data: data,
		success: function (response) {
			domTarget
				.html(response)
				.show()
				.removeClass('hide');
		}
	});
}

function clearHelpText() {
	$('.help-block').remove();
}

function showHelpText(targetControl, id, text) {

	$('#' + id).remove();

	targetControl.after(
		'<span id="' + id + '" class="help-block"><i class="fa fa-info-circle"></i> ' + text + '</span>'
	);

}
