/*global $ */
/*global document */

$(document).on (
	'click',
	'[data-user-address="index"]',
	function (e) {

		'use strict';

		e.preventDefault();

		var
			userAddressesSpan = $('#user-addresses'),
			url = userAddressesSpan.data('url-index');

		$.ajax({
			type: "GET",
			url: url,
			dataType: 'json',
			success: function (response) {

				userAddressesSpan.html(response.html);

			}

		});


	}

);

$(document).on (
	'click',
	'#add-user-address',
	function (e) {

		'use strict';

		e.preventDefault();

		var
			userAddressesSpan = $('#user-addresses'),
			url = userAddressesSpan.data('url-add');

		$.ajax({
			type: "GET",
			url: url,
			dataType: 'json',
			success: function (response) {

				userAddressesSpan.html(response.html);

			}

		});


	}

);

$(document).on (
	'click',
	'[data-user-address="edit"]',
	function (e) {

		'use strict';

		e.preventDefault();

		var
			userAddressesSpan = $('#user-addresses'),
			url = $(this).attr('href');

		$.ajax({
			type: "GET",
			url: url,
			dataType: 'json',
			success: function (response) {

				userAddressesSpan.html(response.html);

			}

		});


	}

);

$(document).on(
	'submit',
	'#UserAddressAddForm',
	function (e) {

		'use strict';

		var form = $(this);

		e.preventDefault();

		$.ajax({
			type: "POST",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: 'json',
			success: function (response) {

				$('#user-addresses').html(response.html);

			}

		});

	}
);

$(document).on(
	'click',
	'[data-user-address="delete"]',
	function (e) {

		'use strict';

		e.preventDefault();

		var link = $(this),
			url = link.attr('href'),
			userAddressId = link.data('user-address-id');

		if (confirm('Are you sure you want to delete this address?')) {

			$.ajax({
				type: "POST",
				url: url,
				data: {
					id: userAddressId
				},
				dataType: 'json',
				success: function (response) {

					$('#user-addresses').html(response.html);

				}

			});

		}

	}
);
